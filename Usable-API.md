# API liên quan đến tiếp nhận
### Request/Response Body
```javascript
	{
    "benhAnId": Long,
    "benhNhanCu": Boolean not null,
    "canhBao": Boolean not null,
    "capCuu": Boolean not null,
    "chiCoNamSinh": Boolean not null,
    "cmndBenhNhan": String,
    "cmndNguoiNha": String,
    "coBaoHiem": Boolean not null,
    "diaChi": String not null,
    "email": String,
    "gioiTinh": Boolean not null,
    "khangThe": String,
    "lanKhamTrongNgay": Boolean,
    "loaiGiayToTreEm": Boolean,
    "ngayCapCmnd": Date format(yyyy-mm-dd),
    "ngayMienCungChiTra": Date format(yyyy-mm-dd),
    "ngaySinh": Date format(yyyy-mm-dd) not null,
    "nhomMau": String,
    "noiCapCmnd": String,
    "noiLamViec": String,
    "phone": String,
    "phoneNguoiNha": String,
    "quocTich": String,
    "quocTichId": Long,
    "tenBenhNhan": String not null,
    "tenNguoiNha": String,
    "thangTuoi": Integer,
    "thoiGianTiepNhan": Date format(yyyy-mm-dd) not null,
    "tuoi": Integer,
    "uuTien": Integer not null,
    "uuid": String,
    "trangThai": Integer not null,
    "soBenhAn": String,
    "soBenhAnKhoa": String,
    "nam": Integer not null,
    "benhNhanId": Long,
    "donViId": Long,
    "nhanVienTiepNhanId": Long,
    "phongTiepNhanId": Long,
    "dotDieuTriId": Long,
    "soThuTu": String,
    "bhytCanTren": Integer,
    "bhytCanTrenKtc": Integer,
    "bhytMaKhuVuc": Integer,
    "bhytNgayBatDau": Date format(yyyy-mm-dd),
    "bhytNgayDuNamNam": Date format(yyyy-mm-dd),
    "bhytNgayHetHan": Date format(yyyy-mm-dd),
    "bhytNoiTinh": Boolean not null,
    "bhytSoThe": String,
    "bhytTyLeMienGiam": Integer,
    "bhytTyLeMienGiamKtc": Integer,
    "bhytDiaChi": String not null,
    "doiTuongBhytTen": String,
    "dungTuyen": Boolean not null,
    "giayToTreEm": String,
    "theTreEm": Boolean not null,
    "thongTuyenBhxhXml4210": Boolean not null,
    "loai": Integer,
    "bhytNoiDkKcbbd": String,
    "enabled": Boolean not null,
    "maSoThue": String,
    "soNhaXom": String,
    "apThon": String,
    "diaPhuongId": Long,
    "diaChiThuongTru": String not null,
    "danTocId": Long,
    "ngheNghiepId": Long,
    "phuongXaId": Long,
    "quanHuyenId": Long,
    "tinhThanhPhoId": Long,
    "userExtraId": Long,
    "bhytId": Long,
    "bhytEnabled": Integer not null,
    "maSoBhxh": String,
    "maTheCu": String,
    "qr": String,
    "doiTuongBhytId": Long,
    "thongTinBhxhId": Long
}

````
### 1. Tạo tiếp nhận mới
* Method: POST
* Url: /api/tiep-nhans

  #### 1.1 Tạo mới hoàn toàn 
Ràng buộc: các giá trị benhAnId, dotDieuTriId, benhNhanId, theBhytId bằng null.

  #### 1.2 Bệnh nhân cũ, thẻ BHYT cũ
Ràng buộc: các giá trị benhAnId, dotDieuTriId bằng null.

  #### 1.3 Bệnh nhân cũ, thẻ Bhyt mới
Ràng buộc: các giá trị theBhytId, dotDieuTriId bằng null.

  #### 1.4 Cập nhật 
Khi benhAnId, dotDieuTriId, benhNhanId, theBhytId đều khác null thì xem như đang cập nhật.

### 2. Lấy nhiều tiếp nhận theo điều kiện
* Method: GET
* Url: /api/tiep-nhans?{condition}
* Các {condition} 

  #### 2.1. Lấy theo bệnh án 
Condition: 
* id.equals={id bệnh án} : lấy 1 thông tin tiếp nhận theo id bệnh án.
* id.greaterThan={id bệnh án} : lấy những thông tin tiếp nhận có id bệnh án lớn hơn {id bệnh án}.
* id.greaterThanOrEqual={id bệnh án} : lấy những thông tin tiếp nhận có id bệnh án lớn hơn hoặc bằng {id bệnh án}.
* id.lessThan={id bệnh án} : lấy thông tin tiếp nhận có id bệnh án nhỏ hơn {id bệnh án}.
* id.lessThanOrEqual={id bệnh án} : lấy thông tin tiếp nhận có id bệnh án nhỏ hơn hoặc bằng {id bệnh án}.

  #### 2.2. Lấy theo bệnh nhân 
Condition: 
* id.equals={id bệnh nhân} : lấy 1 thông tin tiếp nhận theo id bệnh nhân.
* id.greaterThan={id bệnh nhân} : lấy những thông tin tiếp nhận có id bệnh nhân lớn hơn {id bệnh nhân}.
* id.greaterThanOrEqual={id bệnh nhân} : lấy những thông tin tiếp nhận có id bệnh nhân lớn hơn hoặc bằng {id bệnh nhân}.
* id.lessThan={id bệnh nhân} : lấy thông tin tiếp nhận có id bệnh nhân nhỏ hơn {id bệnh nhân}.
* id.lessThanOrEqual={id bệnh nhân} : lấy thông tin tiếp nhận có id bệnh nhân nhỏ hơn hoặc bằng {id bệnh nhân}.

  #### 2.3. Lấy theo tình trạng tiếp nhận
Condition:
* tinhTrang.equals={opt} : lấy những thông tinh tiếp nhận theo tình trạng {opt}.
* tinhTrang.greaterThan={opt} : lấy những thông tin tiếp nhận có opt lớn hơn {opt}.
* tinhTrang.greaterThanOrEqual={opt} : lấy những thông tin tiếp nhận có opt lớn hơn hoặc bằng {opt}.
* tinhTrang.lessThan={opt} : lấy thông tin tiếp nhận có opt nhỏ hơn {opt}.
* tinhTrang.lessThanOrEqual={opt} : lấy thông tin tiếp nhận có opt nhỏ hơn hoặc bằng {opt}.

Với {opt} là các giá trị 
{1}: đang chờ khám.
{2}: đang khám.
{3}: đã khám.
{4}: chuyển khoa/ chuyển phòng.
{5}: chuyển viện/ chuyển tuyến.
{6}: nhập viện.
{7}: tử vong.
{8}: trốn viện.

  #### 2.4. Lấy theo ngày tiếp nhận
Condition:
* thoiGianTiepNhan.equals={date} : lấy nghững thông tin tiếp nhận theo ngày tiêp nhận {date}.
* thoiGianTiepNhan.greaterThan={date} : lấy những thông tin tiếp nhận có ngày tiêp nhận lớn hơn {date}.
* thoiGianTiepNhan.greaterThanOrEqual={date} : lấy những thông tin tiếp nhận có ngày tiếp nhận lớn hơn hoặc bằng {date}.
* thoiGianTiepNhan.lessThan={date} : lấy thông tin tiếp nhận có ngày tiếp nhận nhỏ hơn {date}.
* thoiGianTiepNhan.lessThanOrEqual={date} : lấy thông tin tiếp nhận có ngày tiếp nhận nhỏ hơn hoặc bằng {date}.
* thoiGianTiepNhan.in[0].year={year}: lấy thông tin tiếp nhận theo năn {year}.

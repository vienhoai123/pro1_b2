package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.TBenhLyKhamBenh;
import vn.vnpt.domain.ThongTinKhamBenh;
import vn.vnpt.domain.BenhLy;
import vn.vnpt.repository.TBenhLyKhamBenhRepository;
import vn.vnpt.service.TBenhLyKhamBenhService;
import vn.vnpt.service.dto.TBenhLyKhamBenhDTO;
import vn.vnpt.service.mapper.TBenhLyKhamBenhMapper;
import vn.vnpt.service.dto.TBenhLyKhamBenhCriteria;
import vn.vnpt.service.TBenhLyKhamBenhQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TBenhLyKhamBenhResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })
@AutoConfigureMockMvc
@WithMockUser
public class TBenhLyKhamBenhResourceIT {

    private static final Boolean DEFAULT_LOAI_BENH = false;
    private static final Boolean UPDATED_LOAI_BENH = true;

    @Autowired
    private TBenhLyKhamBenhRepository tBenhLyKhamBenhRepository;

    @Autowired
    private TBenhLyKhamBenhMapper tBenhLyKhamBenhMapper;

    @Autowired
    private TBenhLyKhamBenhService tBenhLyKhamBenhService;

    @Autowired
    private TBenhLyKhamBenhQueryService tBenhLyKhamBenhQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restTBenhLyKhamBenhMockMvc;

    private TBenhLyKhamBenh tBenhLyKhamBenh;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TBenhLyKhamBenh createEntity(EntityManager em) {
        TBenhLyKhamBenh tBenhLyKhamBenh = new TBenhLyKhamBenh()
            .loaiBenh(DEFAULT_LOAI_BENH);
        // Add required entity
        ThongTinKhamBenh thongTinKhamBenh;
        if (TestUtil.findAll(em, ThongTinKhamBenh.class).isEmpty()) {
            thongTinKhamBenh = ThongTinKhamBenhResourceIT.createEntity(em);
            em.persist(thongTinKhamBenh);
            em.flush();
        } else {
            thongTinKhamBenh = TestUtil.findAll(em, ThongTinKhamBenh.class).get(0);
        }
        tBenhLyKhamBenh.setBakb(thongTinKhamBenh);
        // Add required entity
        tBenhLyKhamBenh.setDonVi(thongTinKhamBenh);
        // Add required entity
        tBenhLyKhamBenh.setBenhNhan(thongTinKhamBenh);
        // Add required entity
        tBenhLyKhamBenh.setDotDieuTri(thongTinKhamBenh);
        // Add required entity
        tBenhLyKhamBenh.setThongTinKhoa(thongTinKhamBenh);
        // Add required entity
        tBenhLyKhamBenh.setThongTinKhamBenh(thongTinKhamBenh);
        // Add required entity
        BenhLy benhLy;
        if (TestUtil.findAll(em, BenhLy.class).isEmpty()) {
            benhLy = BenhLyResourceIT.createEntity(em);
            em.persist(benhLy);
            em.flush();
        } else {
            benhLy = TestUtil.findAll(em, BenhLy.class).get(0);
        }
        tBenhLyKhamBenh.setBenhLy(benhLy);
        return tBenhLyKhamBenh;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TBenhLyKhamBenh createUpdatedEntity(EntityManager em) {
        TBenhLyKhamBenh tBenhLyKhamBenh = new TBenhLyKhamBenh()
            .loaiBenh(UPDATED_LOAI_BENH);
        // Add required entity
        ThongTinKhamBenh thongTinKhamBenh;
        if (TestUtil.findAll(em, ThongTinKhamBenh.class).isEmpty()) {
            thongTinKhamBenh = ThongTinKhamBenhResourceIT.createUpdatedEntity(em);
            em.persist(thongTinKhamBenh);
            em.flush();
        } else {
            thongTinKhamBenh = TestUtil.findAll(em, ThongTinKhamBenh.class).get(0);
        }
        tBenhLyKhamBenh.setBakb(thongTinKhamBenh);
        // Add required entity
        tBenhLyKhamBenh.setDonVi(thongTinKhamBenh);
        // Add required entity
        tBenhLyKhamBenh.setBenhNhan(thongTinKhamBenh);
        // Add required entity
        tBenhLyKhamBenh.setDotDieuTri(thongTinKhamBenh);
        // Add required entity
        tBenhLyKhamBenh.setThongTinKhoa(thongTinKhamBenh);
        // Add required entity
        tBenhLyKhamBenh.setThongTinKhamBenh(thongTinKhamBenh);
        // Add required entity
        BenhLy benhLy;
        if (TestUtil.findAll(em, BenhLy.class).isEmpty()) {
            benhLy = BenhLyResourceIT.createUpdatedEntity(em);
            em.persist(benhLy);
            em.flush();
        } else {
            benhLy = TestUtil.findAll(em, BenhLy.class).get(0);
        }
        tBenhLyKhamBenh.setBenhLy(benhLy);
        return tBenhLyKhamBenh;
    }

    @BeforeEach
    public void initTest() {
        tBenhLyKhamBenh = createEntity(em);
    }

    @Test
    @Transactional
    public void createTBenhLyKhamBenh() throws Exception {
        int databaseSizeBeforeCreate = tBenhLyKhamBenhRepository.findAll().size();
        // Create the TBenhLyKhamBenh
        TBenhLyKhamBenhDTO tBenhLyKhamBenhDTO = tBenhLyKhamBenhMapper.toDto(tBenhLyKhamBenh);
        restTBenhLyKhamBenhMockMvc.perform(post("/api/t-benh-ly-kham-benhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tBenhLyKhamBenhDTO)))
            .andExpect(status().isCreated());

        // Validate the TBenhLyKhamBenh in the database
        List<TBenhLyKhamBenh> tBenhLyKhamBenhList = tBenhLyKhamBenhRepository.findAll();
        assertThat(tBenhLyKhamBenhList).hasSize(databaseSizeBeforeCreate + 1);
        TBenhLyKhamBenh testTBenhLyKhamBenh = tBenhLyKhamBenhList.get(tBenhLyKhamBenhList.size() - 1);
        assertThat(testTBenhLyKhamBenh.isLoaiBenh()).isEqualTo(DEFAULT_LOAI_BENH);
    }

    @Test
    @Transactional
    public void createTBenhLyKhamBenhWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tBenhLyKhamBenhRepository.findAll().size();

        // Create the TBenhLyKhamBenh with an existing ID
        tBenhLyKhamBenh.setId(1L);
        TBenhLyKhamBenhDTO tBenhLyKhamBenhDTO = tBenhLyKhamBenhMapper.toDto(tBenhLyKhamBenh);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTBenhLyKhamBenhMockMvc.perform(post("/api/t-benh-ly-kham-benhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tBenhLyKhamBenhDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TBenhLyKhamBenh in the database
        List<TBenhLyKhamBenh> tBenhLyKhamBenhList = tBenhLyKhamBenhRepository.findAll();
        assertThat(tBenhLyKhamBenhList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkLoaiBenhIsRequired() throws Exception {
        int databaseSizeBeforeTest = tBenhLyKhamBenhRepository.findAll().size();
        // set the field null
        tBenhLyKhamBenh.setLoaiBenh(null);

        // Create the TBenhLyKhamBenh, which fails.
        TBenhLyKhamBenhDTO tBenhLyKhamBenhDTO = tBenhLyKhamBenhMapper.toDto(tBenhLyKhamBenh);


        restTBenhLyKhamBenhMockMvc.perform(post("/api/t-benh-ly-kham-benhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tBenhLyKhamBenhDTO)))
            .andExpect(status().isBadRequest());

        List<TBenhLyKhamBenh> tBenhLyKhamBenhList = tBenhLyKhamBenhRepository.findAll();
        assertThat(tBenhLyKhamBenhList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTBenhLyKhamBenhs() throws Exception {
        // Initialize the database
        tBenhLyKhamBenhRepository.saveAndFlush(tBenhLyKhamBenh);

        // Get all the tBenhLyKhamBenhList
        restTBenhLyKhamBenhMockMvc.perform(get("/api/t-benh-ly-kham-benhs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tBenhLyKhamBenh.getId().intValue())))
            .andExpect(jsonPath("$.[*].loaiBenh").value(hasItem(DEFAULT_LOAI_BENH.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getTBenhLyKhamBenh() throws Exception {
        // Initialize the database
        tBenhLyKhamBenhRepository.saveAndFlush(tBenhLyKhamBenh);

        // Get the tBenhLyKhamBenh
        restTBenhLyKhamBenhMockMvc.perform(get("/api/t-benh-ly-kham-benhs/{id}", tBenhLyKhamBenh.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(tBenhLyKhamBenh.getId().intValue()))
            .andExpect(jsonPath("$.loaiBenh").value(DEFAULT_LOAI_BENH.booleanValue()));
    }


    @Test
    @Transactional
    public void getTBenhLyKhamBenhsByIdFiltering() throws Exception {
        // Initialize the database
        tBenhLyKhamBenhRepository.saveAndFlush(tBenhLyKhamBenh);

        Long id = tBenhLyKhamBenh.getId();

        defaultTBenhLyKhamBenhShouldBeFound("id.equals=" + id);
        defaultTBenhLyKhamBenhShouldNotBeFound("id.notEquals=" + id);

        defaultTBenhLyKhamBenhShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultTBenhLyKhamBenhShouldNotBeFound("id.greaterThan=" + id);

        defaultTBenhLyKhamBenhShouldBeFound("id.lessThanOrEqual=" + id);
        defaultTBenhLyKhamBenhShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllTBenhLyKhamBenhsByLoaiBenhIsEqualToSomething() throws Exception {
        // Initialize the database
        tBenhLyKhamBenhRepository.saveAndFlush(tBenhLyKhamBenh);

        // Get all the tBenhLyKhamBenhList where loaiBenh equals to DEFAULT_LOAI_BENH
        defaultTBenhLyKhamBenhShouldBeFound("loaiBenh.equals=" + DEFAULT_LOAI_BENH);

        // Get all the tBenhLyKhamBenhList where loaiBenh equals to UPDATED_LOAI_BENH
        defaultTBenhLyKhamBenhShouldNotBeFound("loaiBenh.equals=" + UPDATED_LOAI_BENH);
    }

    @Test
    @Transactional
    public void getAllTBenhLyKhamBenhsByLoaiBenhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        tBenhLyKhamBenhRepository.saveAndFlush(tBenhLyKhamBenh);

        // Get all the tBenhLyKhamBenhList where loaiBenh not equals to DEFAULT_LOAI_BENH
        defaultTBenhLyKhamBenhShouldNotBeFound("loaiBenh.notEquals=" + DEFAULT_LOAI_BENH);

        // Get all the tBenhLyKhamBenhList where loaiBenh not equals to UPDATED_LOAI_BENH
        defaultTBenhLyKhamBenhShouldBeFound("loaiBenh.notEquals=" + UPDATED_LOAI_BENH);
    }

    @Test
    @Transactional
    public void getAllTBenhLyKhamBenhsByLoaiBenhIsInShouldWork() throws Exception {
        // Initialize the database
        tBenhLyKhamBenhRepository.saveAndFlush(tBenhLyKhamBenh);

        // Get all the tBenhLyKhamBenhList where loaiBenh in DEFAULT_LOAI_BENH or UPDATED_LOAI_BENH
        defaultTBenhLyKhamBenhShouldBeFound("loaiBenh.in=" + DEFAULT_LOAI_BENH + "," + UPDATED_LOAI_BENH);

        // Get all the tBenhLyKhamBenhList where loaiBenh equals to UPDATED_LOAI_BENH
        defaultTBenhLyKhamBenhShouldNotBeFound("loaiBenh.in=" + UPDATED_LOAI_BENH);
    }

    @Test
    @Transactional
    public void getAllTBenhLyKhamBenhsByLoaiBenhIsNullOrNotNull() throws Exception {
        // Initialize the database
        tBenhLyKhamBenhRepository.saveAndFlush(tBenhLyKhamBenh);

        // Get all the tBenhLyKhamBenhList where loaiBenh is not null
        defaultTBenhLyKhamBenhShouldBeFound("loaiBenh.specified=true");

        // Get all the tBenhLyKhamBenhList where loaiBenh is null
        defaultTBenhLyKhamBenhShouldNotBeFound("loaiBenh.specified=false");
    }

    @Test
    @Transactional
    public void getAllTBenhLyKhamBenhsByBakbIsEqualToSomething() throws Exception {
        // Get already existing entity
        ThongTinKhamBenh bakb = tBenhLyKhamBenh.getBakb();
        tBenhLyKhamBenhRepository.saveAndFlush(tBenhLyKhamBenh);
        Long bakbId = bakb.getId();

        // Get all the tBenhLyKhamBenhList where bakb equals to bakbId
        defaultTBenhLyKhamBenhShouldBeFound("bakbId.equals=" + bakbId);

        // Get all the tBenhLyKhamBenhList where bakb equals to bakbId + 1
        defaultTBenhLyKhamBenhShouldNotBeFound("bakbId.equals=" + (bakbId + 1));
    }


    @Test
    @Transactional
    public void getAllTBenhLyKhamBenhsByDonViIsEqualToSomething() throws Exception {
        // Get already existing entity
        ThongTinKhamBenh donVi = tBenhLyKhamBenh.getDonVi();
        tBenhLyKhamBenhRepository.saveAndFlush(tBenhLyKhamBenh);
        Long donViId = donVi.getId();

        // Get all the tBenhLyKhamBenhList where donVi equals to donViId
        defaultTBenhLyKhamBenhShouldBeFound("donViId.equals=" + donViId);

        // Get all the tBenhLyKhamBenhList where donVi equals to donViId + 1
        defaultTBenhLyKhamBenhShouldNotBeFound("donViId.equals=" + (donViId + 1));
    }


    @Test
    @Transactional
    public void getAllTBenhLyKhamBenhsByBenhNhanIsEqualToSomething() throws Exception {
        // Get already existing entity
        ThongTinKhamBenh benhNhan = tBenhLyKhamBenh.getBenhNhan();
        tBenhLyKhamBenhRepository.saveAndFlush(tBenhLyKhamBenh);
        Long benhNhanId = benhNhan.getId();

        // Get all the tBenhLyKhamBenhList where benhNhan equals to benhNhanId
        defaultTBenhLyKhamBenhShouldBeFound("benhNhanId.equals=" + benhNhanId);

        // Get all the tBenhLyKhamBenhList where benhNhan equals to benhNhanId + 1
        defaultTBenhLyKhamBenhShouldNotBeFound("benhNhanId.equals=" + (benhNhanId + 1));
    }


    @Test
    @Transactional
    public void getAllTBenhLyKhamBenhsByDotDieuTriIsEqualToSomething() throws Exception {
        // Get already existing entity
        ThongTinKhamBenh dotDieuTri = tBenhLyKhamBenh.getDotDieuTri();
        tBenhLyKhamBenhRepository.saveAndFlush(tBenhLyKhamBenh);
        Long dotDieuTriId = dotDieuTri.getId();

        // Get all the tBenhLyKhamBenhList where dotDieuTri equals to dotDieuTriId
        defaultTBenhLyKhamBenhShouldBeFound("dotDieuTriId.equals=" + dotDieuTriId);

        // Get all the tBenhLyKhamBenhList where dotDieuTri equals to dotDieuTriId + 1
        defaultTBenhLyKhamBenhShouldNotBeFound("dotDieuTriId.equals=" + (dotDieuTriId + 1));
    }


    @Test
    @Transactional
    public void getAllTBenhLyKhamBenhsByThongTinKhoaIsEqualToSomething() throws Exception {
        // Get already existing entity
        ThongTinKhamBenh thongTinKhoa = tBenhLyKhamBenh.getThongTinKhoa();
        tBenhLyKhamBenhRepository.saveAndFlush(tBenhLyKhamBenh);
        Long thongTinKhoaId = thongTinKhoa.getId();

        // Get all the tBenhLyKhamBenhList where thongTinKhoa equals to thongTinKhoaId
        defaultTBenhLyKhamBenhShouldBeFound("thongTinKhoaId.equals=" + thongTinKhoaId);

        // Get all the tBenhLyKhamBenhList where thongTinKhoa equals to thongTinKhoaId + 1
        defaultTBenhLyKhamBenhShouldNotBeFound("thongTinKhoaId.equals=" + (thongTinKhoaId + 1));
    }


    @Test
    @Transactional
    public void getAllTBenhLyKhamBenhsByThongTinKhamBenhIsEqualToSomething() throws Exception {
        // Get already existing entity
        ThongTinKhamBenh thongTinKhamBenh = tBenhLyKhamBenh.getThongTinKhamBenh();
        tBenhLyKhamBenhRepository.saveAndFlush(tBenhLyKhamBenh);
        Long thongTinKhamBenhId = thongTinKhamBenh.getId();

        // Get all the tBenhLyKhamBenhList where thongTinKhamBenh equals to thongTinKhamBenhId
        defaultTBenhLyKhamBenhShouldBeFound("thongTinKhamBenhId.equals=" + thongTinKhamBenhId);

        // Get all the tBenhLyKhamBenhList where thongTinKhamBenh equals to thongTinKhamBenhId + 1
        defaultTBenhLyKhamBenhShouldNotBeFound("thongTinKhamBenhId.equals=" + (thongTinKhamBenhId + 1));
    }


    @Test
    @Transactional
    public void getAllTBenhLyKhamBenhsByBenhLyIsEqualToSomething() throws Exception {
        // Get already existing entity
        BenhLy benhLy = tBenhLyKhamBenh.getBenhLy();
        tBenhLyKhamBenhRepository.saveAndFlush(tBenhLyKhamBenh);
        Long benhLyId = benhLy.getId();

        // Get all the tBenhLyKhamBenhList where benhLy equals to benhLyId
        defaultTBenhLyKhamBenhShouldBeFound("benhLyId.equals=" + benhLyId);

        // Get all the tBenhLyKhamBenhList where benhLy equals to benhLyId + 1
        defaultTBenhLyKhamBenhShouldNotBeFound("benhLyId.equals=" + (benhLyId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultTBenhLyKhamBenhShouldBeFound(String filter) throws Exception {
        restTBenhLyKhamBenhMockMvc.perform(get("/api/t-benh-ly-kham-benhs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tBenhLyKhamBenh.getId().intValue())))
            .andExpect(jsonPath("$.[*].loaiBenh").value(hasItem(DEFAULT_LOAI_BENH.booleanValue())));

        // Check, that the count call also returns 1
        restTBenhLyKhamBenhMockMvc.perform(get("/api/t-benh-ly-kham-benhs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultTBenhLyKhamBenhShouldNotBeFound(String filter) throws Exception {
        restTBenhLyKhamBenhMockMvc.perform(get("/api/t-benh-ly-kham-benhs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restTBenhLyKhamBenhMockMvc.perform(get("/api/t-benh-ly-kham-benhs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingTBenhLyKhamBenh() throws Exception {
        // Get the tBenhLyKhamBenh
        restTBenhLyKhamBenhMockMvc.perform(get("/api/t-benh-ly-kham-benhs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTBenhLyKhamBenh() throws Exception {
        // Initialize the database
        tBenhLyKhamBenhRepository.saveAndFlush(tBenhLyKhamBenh);

        int databaseSizeBeforeUpdate = tBenhLyKhamBenhRepository.findAll().size();

        // Update the tBenhLyKhamBenh
        TBenhLyKhamBenh updatedTBenhLyKhamBenh = tBenhLyKhamBenhRepository.findById(tBenhLyKhamBenh.getId()).get();
        // Disconnect from session so that the updates on updatedTBenhLyKhamBenh are not directly saved in db
        em.detach(updatedTBenhLyKhamBenh);
        updatedTBenhLyKhamBenh
            .loaiBenh(UPDATED_LOAI_BENH);
        TBenhLyKhamBenhDTO tBenhLyKhamBenhDTO = tBenhLyKhamBenhMapper.toDto(updatedTBenhLyKhamBenh);

        restTBenhLyKhamBenhMockMvc.perform(put("/api/t-benh-ly-kham-benhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tBenhLyKhamBenhDTO)))
            .andExpect(status().isOk());

        // Validate the TBenhLyKhamBenh in the database
        List<TBenhLyKhamBenh> tBenhLyKhamBenhList = tBenhLyKhamBenhRepository.findAll();
        assertThat(tBenhLyKhamBenhList).hasSize(databaseSizeBeforeUpdate);
        TBenhLyKhamBenh testTBenhLyKhamBenh = tBenhLyKhamBenhList.get(tBenhLyKhamBenhList.size() - 1);
        assertThat(testTBenhLyKhamBenh.isLoaiBenh()).isEqualTo(UPDATED_LOAI_BENH);
    }

    @Test
    @Transactional
    public void updateNonExistingTBenhLyKhamBenh() throws Exception {
        int databaseSizeBeforeUpdate = tBenhLyKhamBenhRepository.findAll().size();

        // Create the TBenhLyKhamBenh
        TBenhLyKhamBenhDTO tBenhLyKhamBenhDTO = tBenhLyKhamBenhMapper.toDto(tBenhLyKhamBenh);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTBenhLyKhamBenhMockMvc.perform(put("/api/t-benh-ly-kham-benhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tBenhLyKhamBenhDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TBenhLyKhamBenh in the database
        List<TBenhLyKhamBenh> tBenhLyKhamBenhList = tBenhLyKhamBenhRepository.findAll();
        assertThat(tBenhLyKhamBenhList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTBenhLyKhamBenh() throws Exception {
        // Initialize the database
        tBenhLyKhamBenhRepository.saveAndFlush(tBenhLyKhamBenh);

        int databaseSizeBeforeDelete = tBenhLyKhamBenhRepository.findAll().size();

        // Delete the tBenhLyKhamBenh
        restTBenhLyKhamBenhMockMvc.perform(delete("/api/t-benh-ly-kham-benhs/{id}", tBenhLyKhamBenh.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TBenhLyKhamBenh> tBenhLyKhamBenhList = tBenhLyKhamBenhRepository.findAll();
        assertThat(tBenhLyKhamBenhList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

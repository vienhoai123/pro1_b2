package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.HuongDieuTri;
import vn.vnpt.repository.HuongDieuTriRepository;
import vn.vnpt.service.HuongDieuTriService;
import vn.vnpt.service.dto.HuongDieuTriDTO;
import vn.vnpt.service.mapper.HuongDieuTriMapper;
import vn.vnpt.service.dto.HuongDieuTriCriteria;
import vn.vnpt.service.HuongDieuTriQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link HuongDieuTriResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })
@AutoConfigureMockMvc
@WithMockUser
public class HuongDieuTriResourceIT {

    private static final Integer DEFAULT_NOI_TRU = 1;
    private static final Integer UPDATED_NOI_TRU = 2;
    private static final Integer SMALLER_NOI_TRU = 1 - 1;

    private static final String DEFAULT_TEN = "AAAAAAAAAA";
    private static final String UPDATED_TEN = "BBBBBBBBBB";

    private static final Integer DEFAULT_MA = 1;
    private static final Integer UPDATED_MA = 2;
    private static final Integer SMALLER_MA = 1 - 1;

    @Autowired
    private HuongDieuTriRepository huongDieuTriRepository;

    @Autowired
    private HuongDieuTriMapper huongDieuTriMapper;

    @Autowired
    private HuongDieuTriService huongDieuTriService;

    @Autowired
    private HuongDieuTriQueryService huongDieuTriQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restHuongDieuTriMockMvc;

    private HuongDieuTri huongDieuTri;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static HuongDieuTri createEntity(EntityManager em) {
        HuongDieuTri huongDieuTri = new HuongDieuTri()
            .noiTru(DEFAULT_NOI_TRU)
            .ten(DEFAULT_TEN)
            .ma(DEFAULT_MA);
        return huongDieuTri;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static HuongDieuTri createUpdatedEntity(EntityManager em) {
        HuongDieuTri huongDieuTri = new HuongDieuTri()
            .noiTru(UPDATED_NOI_TRU)
            .ten(UPDATED_TEN)
            .ma(UPDATED_MA);
        return huongDieuTri;
    }

    @BeforeEach
    public void initTest() {
        huongDieuTri = createEntity(em);
    }

    @Test
    @Transactional
    public void createHuongDieuTri() throws Exception {
        int databaseSizeBeforeCreate = huongDieuTriRepository.findAll().size();
        // Create the HuongDieuTri
        HuongDieuTriDTO huongDieuTriDTO = huongDieuTriMapper.toDto(huongDieuTri);
        restHuongDieuTriMockMvc.perform(post("/api/huong-dieu-tris").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(huongDieuTriDTO)))
            .andExpect(status().isCreated());

        // Validate the HuongDieuTri in the database
        List<HuongDieuTri> huongDieuTriList = huongDieuTriRepository.findAll();
        assertThat(huongDieuTriList).hasSize(databaseSizeBeforeCreate + 1);
        HuongDieuTri testHuongDieuTri = huongDieuTriList.get(huongDieuTriList.size() - 1);
        assertThat(testHuongDieuTri.getNoiTru()).isEqualTo(DEFAULT_NOI_TRU);
        assertThat(testHuongDieuTri.getTen()).isEqualTo(DEFAULT_TEN);
        assertThat(testHuongDieuTri.getMa()).isEqualTo(DEFAULT_MA);
    }

    @Test
    @Transactional
    public void createHuongDieuTriWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = huongDieuTriRepository.findAll().size();

        // Create the HuongDieuTri with an existing ID
        huongDieuTri.setId(1L);
        HuongDieuTriDTO huongDieuTriDTO = huongDieuTriMapper.toDto(huongDieuTri);

        // An entity with an existing ID cannot be created, so this API call must fail
        restHuongDieuTriMockMvc.perform(post("/api/huong-dieu-tris").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(huongDieuTriDTO)))
            .andExpect(status().isBadRequest());

        // Validate the HuongDieuTri in the database
        List<HuongDieuTri> huongDieuTriList = huongDieuTriRepository.findAll();
        assertThat(huongDieuTriList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNoiTruIsRequired() throws Exception {
        int databaseSizeBeforeTest = huongDieuTriRepository.findAll().size();
        // set the field null
        huongDieuTri.setNoiTru(null);

        // Create the HuongDieuTri, which fails.
        HuongDieuTriDTO huongDieuTriDTO = huongDieuTriMapper.toDto(huongDieuTri);


        restHuongDieuTriMockMvc.perform(post("/api/huong-dieu-tris").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(huongDieuTriDTO)))
            .andExpect(status().isBadRequest());

        List<HuongDieuTri> huongDieuTriList = huongDieuTriRepository.findAll();
        assertThat(huongDieuTriList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkMaIsRequired() throws Exception {
        int databaseSizeBeforeTest = huongDieuTriRepository.findAll().size();
        // set the field null
        huongDieuTri.setMa(null);

        // Create the HuongDieuTri, which fails.
        HuongDieuTriDTO huongDieuTriDTO = huongDieuTriMapper.toDto(huongDieuTri);


        restHuongDieuTriMockMvc.perform(post("/api/huong-dieu-tris").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(huongDieuTriDTO)))
            .andExpect(status().isBadRequest());

        List<HuongDieuTri> huongDieuTriList = huongDieuTriRepository.findAll();
        assertThat(huongDieuTriList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllHuongDieuTris() throws Exception {
        // Initialize the database
        huongDieuTriRepository.saveAndFlush(huongDieuTri);

        // Get all the huongDieuTriList
        restHuongDieuTriMockMvc.perform(get("/api/huong-dieu-tris?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(huongDieuTri.getId().intValue())))
            .andExpect(jsonPath("$.[*].noiTru").value(hasItem(DEFAULT_NOI_TRU)))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].ma").value(hasItem(DEFAULT_MA)));
    }
    
    @Test
    @Transactional
    public void getHuongDieuTri() throws Exception {
        // Initialize the database
        huongDieuTriRepository.saveAndFlush(huongDieuTri);

        // Get the huongDieuTri
        restHuongDieuTriMockMvc.perform(get("/api/huong-dieu-tris/{id}", huongDieuTri.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(huongDieuTri.getId().intValue()))
            .andExpect(jsonPath("$.noiTru").value(DEFAULT_NOI_TRU))
            .andExpect(jsonPath("$.ten").value(DEFAULT_TEN))
            .andExpect(jsonPath("$.ma").value(DEFAULT_MA));
    }


    @Test
    @Transactional
    public void getHuongDieuTrisByIdFiltering() throws Exception {
        // Initialize the database
        huongDieuTriRepository.saveAndFlush(huongDieuTri);

        Long id = huongDieuTri.getId();

        defaultHuongDieuTriShouldBeFound("id.equals=" + id);
        defaultHuongDieuTriShouldNotBeFound("id.notEquals=" + id);

        defaultHuongDieuTriShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultHuongDieuTriShouldNotBeFound("id.greaterThan=" + id);

        defaultHuongDieuTriShouldBeFound("id.lessThanOrEqual=" + id);
        defaultHuongDieuTriShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllHuongDieuTrisByNoiTruIsEqualToSomething() throws Exception {
        // Initialize the database
        huongDieuTriRepository.saveAndFlush(huongDieuTri);

        // Get all the huongDieuTriList where noiTru equals to DEFAULT_NOI_TRU
        defaultHuongDieuTriShouldBeFound("noiTru.equals=" + DEFAULT_NOI_TRU);

        // Get all the huongDieuTriList where noiTru equals to UPDATED_NOI_TRU
        defaultHuongDieuTriShouldNotBeFound("noiTru.equals=" + UPDATED_NOI_TRU);
    }

    @Test
    @Transactional
    public void getAllHuongDieuTrisByNoiTruIsNotEqualToSomething() throws Exception {
        // Initialize the database
        huongDieuTriRepository.saveAndFlush(huongDieuTri);

        // Get all the huongDieuTriList where noiTru not equals to DEFAULT_NOI_TRU
        defaultHuongDieuTriShouldNotBeFound("noiTru.notEquals=" + DEFAULT_NOI_TRU);

        // Get all the huongDieuTriList where noiTru not equals to UPDATED_NOI_TRU
        defaultHuongDieuTriShouldBeFound("noiTru.notEquals=" + UPDATED_NOI_TRU);
    }

    @Test
    @Transactional
    public void getAllHuongDieuTrisByNoiTruIsInShouldWork() throws Exception {
        // Initialize the database
        huongDieuTriRepository.saveAndFlush(huongDieuTri);

        // Get all the huongDieuTriList where noiTru in DEFAULT_NOI_TRU or UPDATED_NOI_TRU
        defaultHuongDieuTriShouldBeFound("noiTru.in=" + DEFAULT_NOI_TRU + "," + UPDATED_NOI_TRU);

        // Get all the huongDieuTriList where noiTru equals to UPDATED_NOI_TRU
        defaultHuongDieuTriShouldNotBeFound("noiTru.in=" + UPDATED_NOI_TRU);
    }

    @Test
    @Transactional
    public void getAllHuongDieuTrisByNoiTruIsNullOrNotNull() throws Exception {
        // Initialize the database
        huongDieuTriRepository.saveAndFlush(huongDieuTri);

        // Get all the huongDieuTriList where noiTru is not null
        defaultHuongDieuTriShouldBeFound("noiTru.specified=true");

        // Get all the huongDieuTriList where noiTru is null
        defaultHuongDieuTriShouldNotBeFound("noiTru.specified=false");
    }

    @Test
    @Transactional
    public void getAllHuongDieuTrisByNoiTruIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        huongDieuTriRepository.saveAndFlush(huongDieuTri);

        // Get all the huongDieuTriList where noiTru is greater than or equal to DEFAULT_NOI_TRU
        defaultHuongDieuTriShouldBeFound("noiTru.greaterThanOrEqual=" + DEFAULT_NOI_TRU);

        // Get all the huongDieuTriList where noiTru is greater than or equal to UPDATED_NOI_TRU
        defaultHuongDieuTriShouldNotBeFound("noiTru.greaterThanOrEqual=" + UPDATED_NOI_TRU);
    }

    @Test
    @Transactional
    public void getAllHuongDieuTrisByNoiTruIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        huongDieuTriRepository.saveAndFlush(huongDieuTri);

        // Get all the huongDieuTriList where noiTru is less than or equal to DEFAULT_NOI_TRU
        defaultHuongDieuTriShouldBeFound("noiTru.lessThanOrEqual=" + DEFAULT_NOI_TRU);

        // Get all the huongDieuTriList where noiTru is less than or equal to SMALLER_NOI_TRU
        defaultHuongDieuTriShouldNotBeFound("noiTru.lessThanOrEqual=" + SMALLER_NOI_TRU);
    }

    @Test
    @Transactional
    public void getAllHuongDieuTrisByNoiTruIsLessThanSomething() throws Exception {
        // Initialize the database
        huongDieuTriRepository.saveAndFlush(huongDieuTri);

        // Get all the huongDieuTriList where noiTru is less than DEFAULT_NOI_TRU
        defaultHuongDieuTriShouldNotBeFound("noiTru.lessThan=" + DEFAULT_NOI_TRU);

        // Get all the huongDieuTriList where noiTru is less than UPDATED_NOI_TRU
        defaultHuongDieuTriShouldBeFound("noiTru.lessThan=" + UPDATED_NOI_TRU);
    }

    @Test
    @Transactional
    public void getAllHuongDieuTrisByNoiTruIsGreaterThanSomething() throws Exception {
        // Initialize the database
        huongDieuTriRepository.saveAndFlush(huongDieuTri);

        // Get all the huongDieuTriList where noiTru is greater than DEFAULT_NOI_TRU
        defaultHuongDieuTriShouldNotBeFound("noiTru.greaterThan=" + DEFAULT_NOI_TRU);

        // Get all the huongDieuTriList where noiTru is greater than SMALLER_NOI_TRU
        defaultHuongDieuTriShouldBeFound("noiTru.greaterThan=" + SMALLER_NOI_TRU);
    }


    @Test
    @Transactional
    public void getAllHuongDieuTrisByTenIsEqualToSomething() throws Exception {
        // Initialize the database
        huongDieuTriRepository.saveAndFlush(huongDieuTri);

        // Get all the huongDieuTriList where ten equals to DEFAULT_TEN
        defaultHuongDieuTriShouldBeFound("ten.equals=" + DEFAULT_TEN);

        // Get all the huongDieuTriList where ten equals to UPDATED_TEN
        defaultHuongDieuTriShouldNotBeFound("ten.equals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllHuongDieuTrisByTenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        huongDieuTriRepository.saveAndFlush(huongDieuTri);

        // Get all the huongDieuTriList where ten not equals to DEFAULT_TEN
        defaultHuongDieuTriShouldNotBeFound("ten.notEquals=" + DEFAULT_TEN);

        // Get all the huongDieuTriList where ten not equals to UPDATED_TEN
        defaultHuongDieuTriShouldBeFound("ten.notEquals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllHuongDieuTrisByTenIsInShouldWork() throws Exception {
        // Initialize the database
        huongDieuTriRepository.saveAndFlush(huongDieuTri);

        // Get all the huongDieuTriList where ten in DEFAULT_TEN or UPDATED_TEN
        defaultHuongDieuTriShouldBeFound("ten.in=" + DEFAULT_TEN + "," + UPDATED_TEN);

        // Get all the huongDieuTriList where ten equals to UPDATED_TEN
        defaultHuongDieuTriShouldNotBeFound("ten.in=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllHuongDieuTrisByTenIsNullOrNotNull() throws Exception {
        // Initialize the database
        huongDieuTriRepository.saveAndFlush(huongDieuTri);

        // Get all the huongDieuTriList where ten is not null
        defaultHuongDieuTriShouldBeFound("ten.specified=true");

        // Get all the huongDieuTriList where ten is null
        defaultHuongDieuTriShouldNotBeFound("ten.specified=false");
    }
                @Test
    @Transactional
    public void getAllHuongDieuTrisByTenContainsSomething() throws Exception {
        // Initialize the database
        huongDieuTriRepository.saveAndFlush(huongDieuTri);

        // Get all the huongDieuTriList where ten contains DEFAULT_TEN
        defaultHuongDieuTriShouldBeFound("ten.contains=" + DEFAULT_TEN);

        // Get all the huongDieuTriList where ten contains UPDATED_TEN
        defaultHuongDieuTriShouldNotBeFound("ten.contains=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllHuongDieuTrisByTenNotContainsSomething() throws Exception {
        // Initialize the database
        huongDieuTriRepository.saveAndFlush(huongDieuTri);

        // Get all the huongDieuTriList where ten does not contain DEFAULT_TEN
        defaultHuongDieuTriShouldNotBeFound("ten.doesNotContain=" + DEFAULT_TEN);

        // Get all the huongDieuTriList where ten does not contain UPDATED_TEN
        defaultHuongDieuTriShouldBeFound("ten.doesNotContain=" + UPDATED_TEN);
    }


    @Test
    @Transactional
    public void getAllHuongDieuTrisByMaIsEqualToSomething() throws Exception {
        // Initialize the database
        huongDieuTriRepository.saveAndFlush(huongDieuTri);

        // Get all the huongDieuTriList where ma equals to DEFAULT_MA
        defaultHuongDieuTriShouldBeFound("ma.equals=" + DEFAULT_MA);

        // Get all the huongDieuTriList where ma equals to UPDATED_MA
        defaultHuongDieuTriShouldNotBeFound("ma.equals=" + UPDATED_MA);
    }

    @Test
    @Transactional
    public void getAllHuongDieuTrisByMaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        huongDieuTriRepository.saveAndFlush(huongDieuTri);

        // Get all the huongDieuTriList where ma not equals to DEFAULT_MA
        defaultHuongDieuTriShouldNotBeFound("ma.notEquals=" + DEFAULT_MA);

        // Get all the huongDieuTriList where ma not equals to UPDATED_MA
        defaultHuongDieuTriShouldBeFound("ma.notEquals=" + UPDATED_MA);
    }

    @Test
    @Transactional
    public void getAllHuongDieuTrisByMaIsInShouldWork() throws Exception {
        // Initialize the database
        huongDieuTriRepository.saveAndFlush(huongDieuTri);

        // Get all the huongDieuTriList where ma in DEFAULT_MA or UPDATED_MA
        defaultHuongDieuTriShouldBeFound("ma.in=" + DEFAULT_MA + "," + UPDATED_MA);

        // Get all the huongDieuTriList where ma equals to UPDATED_MA
        defaultHuongDieuTriShouldNotBeFound("ma.in=" + UPDATED_MA);
    }

    @Test
    @Transactional
    public void getAllHuongDieuTrisByMaIsNullOrNotNull() throws Exception {
        // Initialize the database
        huongDieuTriRepository.saveAndFlush(huongDieuTri);

        // Get all the huongDieuTriList where ma is not null
        defaultHuongDieuTriShouldBeFound("ma.specified=true");

        // Get all the huongDieuTriList where ma is null
        defaultHuongDieuTriShouldNotBeFound("ma.specified=false");
    }

    @Test
    @Transactional
    public void getAllHuongDieuTrisByMaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        huongDieuTriRepository.saveAndFlush(huongDieuTri);

        // Get all the huongDieuTriList where ma is greater than or equal to DEFAULT_MA
        defaultHuongDieuTriShouldBeFound("ma.greaterThanOrEqual=" + DEFAULT_MA);

        // Get all the huongDieuTriList where ma is greater than or equal to UPDATED_MA
        defaultHuongDieuTriShouldNotBeFound("ma.greaterThanOrEqual=" + UPDATED_MA);
    }

    @Test
    @Transactional
    public void getAllHuongDieuTrisByMaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        huongDieuTriRepository.saveAndFlush(huongDieuTri);

        // Get all the huongDieuTriList where ma is less than or equal to DEFAULT_MA
        defaultHuongDieuTriShouldBeFound("ma.lessThanOrEqual=" + DEFAULT_MA);

        // Get all the huongDieuTriList where ma is less than or equal to SMALLER_MA
        defaultHuongDieuTriShouldNotBeFound("ma.lessThanOrEqual=" + SMALLER_MA);
    }

    @Test
    @Transactional
    public void getAllHuongDieuTrisByMaIsLessThanSomething() throws Exception {
        // Initialize the database
        huongDieuTriRepository.saveAndFlush(huongDieuTri);

        // Get all the huongDieuTriList where ma is less than DEFAULT_MA
        defaultHuongDieuTriShouldNotBeFound("ma.lessThan=" + DEFAULT_MA);

        // Get all the huongDieuTriList where ma is less than UPDATED_MA
        defaultHuongDieuTriShouldBeFound("ma.lessThan=" + UPDATED_MA);
    }

    @Test
    @Transactional
    public void getAllHuongDieuTrisByMaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        huongDieuTriRepository.saveAndFlush(huongDieuTri);

        // Get all the huongDieuTriList where ma is greater than DEFAULT_MA
        defaultHuongDieuTriShouldNotBeFound("ma.greaterThan=" + DEFAULT_MA);

        // Get all the huongDieuTriList where ma is greater than SMALLER_MA
        defaultHuongDieuTriShouldBeFound("ma.greaterThan=" + SMALLER_MA);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultHuongDieuTriShouldBeFound(String filter) throws Exception {
        restHuongDieuTriMockMvc.perform(get("/api/huong-dieu-tris?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(huongDieuTri.getId().intValue())))
            .andExpect(jsonPath("$.[*].noiTru").value(hasItem(DEFAULT_NOI_TRU)))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].ma").value(hasItem(DEFAULT_MA)));

        // Check, that the count call also returns 1
        restHuongDieuTriMockMvc.perform(get("/api/huong-dieu-tris/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultHuongDieuTriShouldNotBeFound(String filter) throws Exception {
        restHuongDieuTriMockMvc.perform(get("/api/huong-dieu-tris?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restHuongDieuTriMockMvc.perform(get("/api/huong-dieu-tris/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingHuongDieuTri() throws Exception {
        // Get the huongDieuTri
        restHuongDieuTriMockMvc.perform(get("/api/huong-dieu-tris/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateHuongDieuTri() throws Exception {
        // Initialize the database
        huongDieuTriRepository.saveAndFlush(huongDieuTri);

        int databaseSizeBeforeUpdate = huongDieuTriRepository.findAll().size();

        // Update the huongDieuTri
        HuongDieuTri updatedHuongDieuTri = huongDieuTriRepository.findById(huongDieuTri.getId()).get();
        // Disconnect from session so that the updates on updatedHuongDieuTri are not directly saved in db
        em.detach(updatedHuongDieuTri);
        updatedHuongDieuTri
            .noiTru(UPDATED_NOI_TRU)
            .ten(UPDATED_TEN)
            .ma(UPDATED_MA);
        HuongDieuTriDTO huongDieuTriDTO = huongDieuTriMapper.toDto(updatedHuongDieuTri);

        restHuongDieuTriMockMvc.perform(put("/api/huong-dieu-tris").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(huongDieuTriDTO)))
            .andExpect(status().isOk());

        // Validate the HuongDieuTri in the database
        List<HuongDieuTri> huongDieuTriList = huongDieuTriRepository.findAll();
        assertThat(huongDieuTriList).hasSize(databaseSizeBeforeUpdate);
        HuongDieuTri testHuongDieuTri = huongDieuTriList.get(huongDieuTriList.size() - 1);
        assertThat(testHuongDieuTri.getNoiTru()).isEqualTo(UPDATED_NOI_TRU);
        assertThat(testHuongDieuTri.getTen()).isEqualTo(UPDATED_TEN);
        assertThat(testHuongDieuTri.getMa()).isEqualTo(UPDATED_MA);
    }

    @Test
    @Transactional
    public void updateNonExistingHuongDieuTri() throws Exception {
        int databaseSizeBeforeUpdate = huongDieuTriRepository.findAll().size();

        // Create the HuongDieuTri
        HuongDieuTriDTO huongDieuTriDTO = huongDieuTriMapper.toDto(huongDieuTri);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restHuongDieuTriMockMvc.perform(put("/api/huong-dieu-tris").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(huongDieuTriDTO)))
            .andExpect(status().isBadRequest());

        // Validate the HuongDieuTri in the database
        List<HuongDieuTri> huongDieuTriList = huongDieuTriRepository.findAll();
        assertThat(huongDieuTriList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteHuongDieuTri() throws Exception {
        // Initialize the database
        huongDieuTriRepository.saveAndFlush(huongDieuTri);

        int databaseSizeBeforeDelete = huongDieuTriRepository.findAll().size();

        // Delete the huongDieuTri
        restHuongDieuTriMockMvc.perform(delete("/api/huong-dieu-tris/{id}", huongDieuTri.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<HuongDieuTri> huongDieuTriList = huongDieuTriRepository.findAll();
        assertThat(huongDieuTriList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

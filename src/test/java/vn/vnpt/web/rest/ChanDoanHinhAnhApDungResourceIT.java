package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.ChanDoanHinhAnhApDung;
import vn.vnpt.domain.DotGiaDichVuBhxh;
import vn.vnpt.domain.ChanDoanHinhAnh;
import vn.vnpt.repository.ChanDoanHinhAnhApDungRepository;
import vn.vnpt.service.ChanDoanHinhAnhApDungService;
import vn.vnpt.service.dto.ChanDoanHinhAnhApDungDTO;
import vn.vnpt.service.mapper.ChanDoanHinhAnhApDungMapper;
import vn.vnpt.service.dto.ChanDoanHinhAnhApDungCriteria;
import vn.vnpt.service.ChanDoanHinhAnhApDungQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ChanDoanHinhAnhApDungResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class ChanDoanHinhAnhApDungResourceIT {

    private static final String DEFAULT_MA_BAO_CAO_BHXH = "AAAAAAAAAA";
    private static final String UPDATED_MA_BAO_CAO_BHXH = "BBBBBBBBBB";

    private static final String DEFAULT_MA_BAO_CAO_BHYT = "AAAAAAAAAA";
    private static final String UPDATED_MA_BAO_CAO_BHYT = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_NGAY_AP_DUNG = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_AP_DUNG = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_NGAY_AP_DUNG = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_SO_CONG_VAN_BHXH = "AAAAAAAAAA";
    private static final String UPDATED_SO_CONG_VAN_BHXH = "BBBBBBBBBB";

    private static final String DEFAULT_SO_QUYET_DINH = "AAAAAAAAAA";
    private static final String UPDATED_SO_QUYET_DINH = "BBBBBBBBBB";

    private static final String DEFAULT_TEN_BAO_CAO_BHXH = "AAAAAAAAAA";
    private static final String UPDATED_TEN_BAO_CAO_BHXH = "BBBBBBBBBB";

    private static final String DEFAULT_TEN_DICH_VU_KHONG_BHYT = "AAAAAAAAAA";
    private static final String UPDATED_TEN_DICH_VU_KHONG_BHYT = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_TIEN_BENH_NHAN_CHI = new BigDecimal(1);
    private static final BigDecimal UPDATED_TIEN_BENH_NHAN_CHI = new BigDecimal(2);
    private static final BigDecimal SMALLER_TIEN_BENH_NHAN_CHI = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_TIEN_BHXH_CHI = new BigDecimal(1);
    private static final BigDecimal UPDATED_TIEN_BHXH_CHI = new BigDecimal(2);
    private static final BigDecimal SMALLER_TIEN_BHXH_CHI = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_TIEN_NGOAI_BHYT = new BigDecimal(1);
    private static final BigDecimal UPDATED_TIEN_NGOAI_BHYT = new BigDecimal(2);
    private static final BigDecimal SMALLER_TIEN_NGOAI_BHYT = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_TONG_TIEN_THANH_TOAN = new BigDecimal(1);
    private static final BigDecimal UPDATED_TONG_TIEN_THANH_TOAN = new BigDecimal(2);
    private static final BigDecimal SMALLER_TONG_TIEN_THANH_TOAN = new BigDecimal(1 - 1);

    private static final Integer DEFAULT_TY_LE_BHXH_THANH_TOAN = 1;
    private static final Integer UPDATED_TY_LE_BHXH_THANH_TOAN = 2;
    private static final Integer SMALLER_TY_LE_BHXH_THANH_TOAN = 1 - 1;

    private static final BigDecimal DEFAULT_GIA_BHYT = new BigDecimal(1);
    private static final BigDecimal UPDATED_GIA_BHYT = new BigDecimal(2);
    private static final BigDecimal SMALLER_GIA_BHYT = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_GIA_KHONG_BHYT = new BigDecimal(1);
    private static final BigDecimal UPDATED_GIA_KHONG_BHYT = new BigDecimal(2);
    private static final BigDecimal SMALLER_GIA_KHONG_BHYT = new BigDecimal(1 - 1);

    private static final Boolean DEFAULT_DOI_TUONG_DAC_BIET = false;
    private static final Boolean UPDATED_DOI_TUONG_DAC_BIET = true;

    private static final Integer DEFAULT_NGUON_CHI = 1;
    private static final Integer UPDATED_NGUON_CHI = 2;
    private static final Integer SMALLER_NGUON_CHI = 1 - 1;

    private static final Boolean DEFAULT_ENABLE = false;
    private static final Boolean UPDATED_ENABLE = true;

    @Autowired
    private ChanDoanHinhAnhApDungRepository chanDoanHinhAnhApDungRepository;

    @Autowired
    private ChanDoanHinhAnhApDungMapper chanDoanHinhAnhApDungMapper;

    @Autowired
    private ChanDoanHinhAnhApDungService chanDoanHinhAnhApDungService;

    @Autowired
    private ChanDoanHinhAnhApDungQueryService chanDoanHinhAnhApDungQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restChanDoanHinhAnhApDungMockMvc;

    private ChanDoanHinhAnhApDung chanDoanHinhAnhApDung;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ChanDoanHinhAnhApDung createEntity(EntityManager em) {
        ChanDoanHinhAnhApDung chanDoanHinhAnhApDung = new ChanDoanHinhAnhApDung()
            .maBaoCaoBhxh(DEFAULT_MA_BAO_CAO_BHXH)
            .maBaoCaoBhyt(DEFAULT_MA_BAO_CAO_BHYT)
            .ngayApDung(DEFAULT_NGAY_AP_DUNG)
            .soCongVanBhxh(DEFAULT_SO_CONG_VAN_BHXH)
            .soQuyetDinh(DEFAULT_SO_QUYET_DINH)
            .tenBaoCaoBhxh(DEFAULT_TEN_BAO_CAO_BHXH)
            .tenDichVuKhongBhyt(DEFAULT_TEN_DICH_VU_KHONG_BHYT)
            .tienBenhNhanChi(DEFAULT_TIEN_BENH_NHAN_CHI)
            .tienBhxhChi(DEFAULT_TIEN_BHXH_CHI)
            .tienNgoaiBhyt(DEFAULT_TIEN_NGOAI_BHYT)
            .tongTienThanhToan(DEFAULT_TONG_TIEN_THANH_TOAN)
            .tyLeBhxhThanhToan(DEFAULT_TY_LE_BHXH_THANH_TOAN)
            .giaBhyt(DEFAULT_GIA_BHYT)
            .giaKhongBhyt(DEFAULT_GIA_KHONG_BHYT)
            .doiTuongDacBiet(DEFAULT_DOI_TUONG_DAC_BIET)
            .nguonChi(DEFAULT_NGUON_CHI)
            .enable(DEFAULT_ENABLE);
        // Add required entity
        DotGiaDichVuBhxh dotGiaDichVuBhxh;
        if (TestUtil.findAll(em, DotGiaDichVuBhxh.class).isEmpty()) {
            dotGiaDichVuBhxh = DotGiaDichVuBhxhResourceIT.createEntity(em);
            em.persist(dotGiaDichVuBhxh);
            em.flush();
        } else {
            dotGiaDichVuBhxh = TestUtil.findAll(em, DotGiaDichVuBhxh.class).get(0);
        }
        chanDoanHinhAnhApDung.setDotGia(dotGiaDichVuBhxh);
        // Add required entity
        ChanDoanHinhAnh chanDoanHinhAnh;
        if (TestUtil.findAll(em, ChanDoanHinhAnh.class).isEmpty()) {
            chanDoanHinhAnh = ChanDoanHinhAnhResourceIT.createEntity(em);
            em.persist(chanDoanHinhAnh);
            em.flush();
        } else {
            chanDoanHinhAnh = TestUtil.findAll(em, ChanDoanHinhAnh.class).get(0);
        }
        chanDoanHinhAnhApDung.setCdha(chanDoanHinhAnh);
        return chanDoanHinhAnhApDung;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ChanDoanHinhAnhApDung createUpdatedEntity(EntityManager em) {
        ChanDoanHinhAnhApDung chanDoanHinhAnhApDung = new ChanDoanHinhAnhApDung()
            .maBaoCaoBhxh(UPDATED_MA_BAO_CAO_BHXH)
            .maBaoCaoBhyt(UPDATED_MA_BAO_CAO_BHYT)
            .ngayApDung(UPDATED_NGAY_AP_DUNG)
            .soCongVanBhxh(UPDATED_SO_CONG_VAN_BHXH)
            .soQuyetDinh(UPDATED_SO_QUYET_DINH)
            .tenBaoCaoBhxh(UPDATED_TEN_BAO_CAO_BHXH)
            .tenDichVuKhongBhyt(UPDATED_TEN_DICH_VU_KHONG_BHYT)
            .tienBenhNhanChi(UPDATED_TIEN_BENH_NHAN_CHI)
            .tienBhxhChi(UPDATED_TIEN_BHXH_CHI)
            .tienNgoaiBhyt(UPDATED_TIEN_NGOAI_BHYT)
            .tongTienThanhToan(UPDATED_TONG_TIEN_THANH_TOAN)
            .tyLeBhxhThanhToan(UPDATED_TY_LE_BHXH_THANH_TOAN)
            .giaBhyt(UPDATED_GIA_BHYT)
            .giaKhongBhyt(UPDATED_GIA_KHONG_BHYT)
            .doiTuongDacBiet(UPDATED_DOI_TUONG_DAC_BIET)
            .nguonChi(UPDATED_NGUON_CHI)
            .enable(UPDATED_ENABLE);
        // Add required entity
        DotGiaDichVuBhxh dotGiaDichVuBhxh;
        if (TestUtil.findAll(em, DotGiaDichVuBhxh.class).isEmpty()) {
            dotGiaDichVuBhxh = DotGiaDichVuBhxhResourceIT.createUpdatedEntity(em);
            em.persist(dotGiaDichVuBhxh);
            em.flush();
        } else {
            dotGiaDichVuBhxh = TestUtil.findAll(em, DotGiaDichVuBhxh.class).get(0);
        }
        chanDoanHinhAnhApDung.setDotGia(dotGiaDichVuBhxh);
        // Add required entity
        ChanDoanHinhAnh chanDoanHinhAnh;
        if (TestUtil.findAll(em, ChanDoanHinhAnh.class).isEmpty()) {
            chanDoanHinhAnh = ChanDoanHinhAnhResourceIT.createUpdatedEntity(em);
            em.persist(chanDoanHinhAnh);
            em.flush();
        } else {
            chanDoanHinhAnh = TestUtil.findAll(em, ChanDoanHinhAnh.class).get(0);
        }
        chanDoanHinhAnhApDung.setCdha(chanDoanHinhAnh);
        return chanDoanHinhAnhApDung;
    }

    @BeforeEach
    public void initTest() {
        chanDoanHinhAnhApDung = createEntity(em);
    }

    @Test
    @Transactional
    public void createChanDoanHinhAnhApDung() throws Exception {
        int databaseSizeBeforeCreate = chanDoanHinhAnhApDungRepository.findAll().size();

        // Create the ChanDoanHinhAnhApDung
        ChanDoanHinhAnhApDungDTO chanDoanHinhAnhApDungDTO = chanDoanHinhAnhApDungMapper.toDto(chanDoanHinhAnhApDung);
        restChanDoanHinhAnhApDungMockMvc.perform(post("/api/chan-doan-hinh-anh-ap-dungs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chanDoanHinhAnhApDungDTO)))
            .andExpect(status().isCreated());

        // Validate the ChanDoanHinhAnhApDung in the database
        List<ChanDoanHinhAnhApDung> chanDoanHinhAnhApDungList = chanDoanHinhAnhApDungRepository.findAll();
        assertThat(chanDoanHinhAnhApDungList).hasSize(databaseSizeBeforeCreate + 1);
        ChanDoanHinhAnhApDung testChanDoanHinhAnhApDung = chanDoanHinhAnhApDungList.get(chanDoanHinhAnhApDungList.size() - 1);
        assertThat(testChanDoanHinhAnhApDung.getMaBaoCaoBhxh()).isEqualTo(DEFAULT_MA_BAO_CAO_BHXH);
        assertThat(testChanDoanHinhAnhApDung.getMaBaoCaoBhyt()).isEqualTo(DEFAULT_MA_BAO_CAO_BHYT);
        assertThat(testChanDoanHinhAnhApDung.getNgayApDung()).isEqualTo(DEFAULT_NGAY_AP_DUNG);
        assertThat(testChanDoanHinhAnhApDung.getSoCongVanBhxh()).isEqualTo(DEFAULT_SO_CONG_VAN_BHXH);
        assertThat(testChanDoanHinhAnhApDung.getSoQuyetDinh()).isEqualTo(DEFAULT_SO_QUYET_DINH);
        assertThat(testChanDoanHinhAnhApDung.getTenBaoCaoBhxh()).isEqualTo(DEFAULT_TEN_BAO_CAO_BHXH);
        assertThat(testChanDoanHinhAnhApDung.getTenDichVuKhongBhyt()).isEqualTo(DEFAULT_TEN_DICH_VU_KHONG_BHYT);
        assertThat(testChanDoanHinhAnhApDung.getTienBenhNhanChi()).isEqualTo(DEFAULT_TIEN_BENH_NHAN_CHI);
        assertThat(testChanDoanHinhAnhApDung.getTienBhxhChi()).isEqualTo(DEFAULT_TIEN_BHXH_CHI);
        assertThat(testChanDoanHinhAnhApDung.getTienNgoaiBhyt()).isEqualTo(DEFAULT_TIEN_NGOAI_BHYT);
        assertThat(testChanDoanHinhAnhApDung.getTongTienThanhToan()).isEqualTo(DEFAULT_TONG_TIEN_THANH_TOAN);
        assertThat(testChanDoanHinhAnhApDung.getTyLeBhxhThanhToan()).isEqualTo(DEFAULT_TY_LE_BHXH_THANH_TOAN);
        assertThat(testChanDoanHinhAnhApDung.getGiaBhyt()).isEqualTo(DEFAULT_GIA_BHYT);
        assertThat(testChanDoanHinhAnhApDung.getGiaKhongBhyt()).isEqualTo(DEFAULT_GIA_KHONG_BHYT);
        assertThat(testChanDoanHinhAnhApDung.isDoiTuongDacBiet()).isEqualTo(DEFAULT_DOI_TUONG_DAC_BIET);
        assertThat(testChanDoanHinhAnhApDung.getNguonChi()).isEqualTo(DEFAULT_NGUON_CHI);
        assertThat(testChanDoanHinhAnhApDung.isEnable()).isEqualTo(DEFAULT_ENABLE);
    }

    @Test
    @Transactional
    public void createChanDoanHinhAnhApDungWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = chanDoanHinhAnhApDungRepository.findAll().size();

        // Create the ChanDoanHinhAnhApDung with an existing ID
        chanDoanHinhAnhApDung.setId(1L);
        ChanDoanHinhAnhApDungDTO chanDoanHinhAnhApDungDTO = chanDoanHinhAnhApDungMapper.toDto(chanDoanHinhAnhApDung);

        // An entity with an existing ID cannot be created, so this API call must fail
        restChanDoanHinhAnhApDungMockMvc.perform(post("/api/chan-doan-hinh-anh-ap-dungs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chanDoanHinhAnhApDungDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ChanDoanHinhAnhApDung in the database
        List<ChanDoanHinhAnhApDung> chanDoanHinhAnhApDungList = chanDoanHinhAnhApDungRepository.findAll();
        assertThat(chanDoanHinhAnhApDungList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNgayApDungIsRequired() throws Exception {
        int databaseSizeBeforeTest = chanDoanHinhAnhApDungRepository.findAll().size();
        // set the field null
        chanDoanHinhAnhApDung.setNgayApDung(null);

        // Create the ChanDoanHinhAnhApDung, which fails.
        ChanDoanHinhAnhApDungDTO chanDoanHinhAnhApDungDTO = chanDoanHinhAnhApDungMapper.toDto(chanDoanHinhAnhApDung);

        restChanDoanHinhAnhApDungMockMvc.perform(post("/api/chan-doan-hinh-anh-ap-dungs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chanDoanHinhAnhApDungDTO)))
            .andExpect(status().isBadRequest());

        List<ChanDoanHinhAnhApDung> chanDoanHinhAnhApDungList = chanDoanHinhAnhApDungRepository.findAll();
        assertThat(chanDoanHinhAnhApDungList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkGiaBhytIsRequired() throws Exception {
        int databaseSizeBeforeTest = chanDoanHinhAnhApDungRepository.findAll().size();
        // set the field null
        chanDoanHinhAnhApDung.setGiaBhyt(null);

        // Create the ChanDoanHinhAnhApDung, which fails.
        ChanDoanHinhAnhApDungDTO chanDoanHinhAnhApDungDTO = chanDoanHinhAnhApDungMapper.toDto(chanDoanHinhAnhApDung);

        restChanDoanHinhAnhApDungMockMvc.perform(post("/api/chan-doan-hinh-anh-ap-dungs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chanDoanHinhAnhApDungDTO)))
            .andExpect(status().isBadRequest());

        List<ChanDoanHinhAnhApDung> chanDoanHinhAnhApDungList = chanDoanHinhAnhApDungRepository.findAll();
        assertThat(chanDoanHinhAnhApDungList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkGiaKhongBhytIsRequired() throws Exception {
        int databaseSizeBeforeTest = chanDoanHinhAnhApDungRepository.findAll().size();
        // set the field null
        chanDoanHinhAnhApDung.setGiaKhongBhyt(null);

        // Create the ChanDoanHinhAnhApDung, which fails.
        ChanDoanHinhAnhApDungDTO chanDoanHinhAnhApDungDTO = chanDoanHinhAnhApDungMapper.toDto(chanDoanHinhAnhApDung);

        restChanDoanHinhAnhApDungMockMvc.perform(post("/api/chan-doan-hinh-anh-ap-dungs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chanDoanHinhAnhApDungDTO)))
            .andExpect(status().isBadRequest());

        List<ChanDoanHinhAnhApDung> chanDoanHinhAnhApDungList = chanDoanHinhAnhApDungRepository.findAll();
        assertThat(chanDoanHinhAnhApDungList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungs() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList
        restChanDoanHinhAnhApDungMockMvc.perform(get("/api/chan-doan-hinh-anh-ap-dungs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(chanDoanHinhAnhApDung.getId().intValue())))
            .andExpect(jsonPath("$.[*].maBaoCaoBhxh").value(hasItem(DEFAULT_MA_BAO_CAO_BHXH)))
            .andExpect(jsonPath("$.[*].maBaoCaoBhyt").value(hasItem(DEFAULT_MA_BAO_CAO_BHYT)))
            .andExpect(jsonPath("$.[*].ngayApDung").value(hasItem(DEFAULT_NGAY_AP_DUNG.toString())))
            .andExpect(jsonPath("$.[*].soCongVanBhxh").value(hasItem(DEFAULT_SO_CONG_VAN_BHXH)))
            .andExpect(jsonPath("$.[*].soQuyetDinh").value(hasItem(DEFAULT_SO_QUYET_DINH)))
            .andExpect(jsonPath("$.[*].tenBaoCaoBhxh").value(hasItem(DEFAULT_TEN_BAO_CAO_BHXH)))
            .andExpect(jsonPath("$.[*].tenDichVuKhongBhyt").value(hasItem(DEFAULT_TEN_DICH_VU_KHONG_BHYT)))
            .andExpect(jsonPath("$.[*].tienBenhNhanChi").value(hasItem(DEFAULT_TIEN_BENH_NHAN_CHI.intValue())))
            .andExpect(jsonPath("$.[*].tienBhxhChi").value(hasItem(DEFAULT_TIEN_BHXH_CHI.intValue())))
            .andExpect(jsonPath("$.[*].tienNgoaiBhyt").value(hasItem(DEFAULT_TIEN_NGOAI_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].tongTienThanhToan").value(hasItem(DEFAULT_TONG_TIEN_THANH_TOAN.intValue())))
            .andExpect(jsonPath("$.[*].tyLeBhxhThanhToan").value(hasItem(DEFAULT_TY_LE_BHXH_THANH_TOAN)))
            .andExpect(jsonPath("$.[*].giaBhyt").value(hasItem(DEFAULT_GIA_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].giaKhongBhyt").value(hasItem(DEFAULT_GIA_KHONG_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].doiTuongDacBiet").value(hasItem(DEFAULT_DOI_TUONG_DAC_BIET.booleanValue())))
            .andExpect(jsonPath("$.[*].nguonChi").value(hasItem(DEFAULT_NGUON_CHI)))
            .andExpect(jsonPath("$.[*].enable").value(hasItem(DEFAULT_ENABLE.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getChanDoanHinhAnhApDung() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get the chanDoanHinhAnhApDung
        restChanDoanHinhAnhApDungMockMvc.perform(get("/api/chan-doan-hinh-anh-ap-dungs/{id}", chanDoanHinhAnhApDung.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(chanDoanHinhAnhApDung.getId().intValue()))
            .andExpect(jsonPath("$.maBaoCaoBhxh").value(DEFAULT_MA_BAO_CAO_BHXH))
            .andExpect(jsonPath("$.maBaoCaoBhyt").value(DEFAULT_MA_BAO_CAO_BHYT))
            .andExpect(jsonPath("$.ngayApDung").value(DEFAULT_NGAY_AP_DUNG.toString()))
            .andExpect(jsonPath("$.soCongVanBhxh").value(DEFAULT_SO_CONG_VAN_BHXH))
            .andExpect(jsonPath("$.soQuyetDinh").value(DEFAULT_SO_QUYET_DINH))
            .andExpect(jsonPath("$.tenBaoCaoBhxh").value(DEFAULT_TEN_BAO_CAO_BHXH))
            .andExpect(jsonPath("$.tenDichVuKhongBhyt").value(DEFAULT_TEN_DICH_VU_KHONG_BHYT))
            .andExpect(jsonPath("$.tienBenhNhanChi").value(DEFAULT_TIEN_BENH_NHAN_CHI.intValue()))
            .andExpect(jsonPath("$.tienBhxhChi").value(DEFAULT_TIEN_BHXH_CHI.intValue()))
            .andExpect(jsonPath("$.tienNgoaiBhyt").value(DEFAULT_TIEN_NGOAI_BHYT.intValue()))
            .andExpect(jsonPath("$.tongTienThanhToan").value(DEFAULT_TONG_TIEN_THANH_TOAN.intValue()))
            .andExpect(jsonPath("$.tyLeBhxhThanhToan").value(DEFAULT_TY_LE_BHXH_THANH_TOAN))
            .andExpect(jsonPath("$.giaBhyt").value(DEFAULT_GIA_BHYT.intValue()))
            .andExpect(jsonPath("$.giaKhongBhyt").value(DEFAULT_GIA_KHONG_BHYT.intValue()))
            .andExpect(jsonPath("$.doiTuongDacBiet").value(DEFAULT_DOI_TUONG_DAC_BIET.booleanValue()))
            .andExpect(jsonPath("$.nguonChi").value(DEFAULT_NGUON_CHI))
            .andExpect(jsonPath("$.enable").value(DEFAULT_ENABLE.booleanValue()));
    }


    @Test
    @Transactional
    public void getChanDoanHinhAnhApDungsByIdFiltering() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        Long id = chanDoanHinhAnhApDung.getId();

        defaultChanDoanHinhAnhApDungShouldBeFound("id.equals=" + id);
        defaultChanDoanHinhAnhApDungShouldNotBeFound("id.notEquals=" + id);

        defaultChanDoanHinhAnhApDungShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultChanDoanHinhAnhApDungShouldNotBeFound("id.greaterThan=" + id);

        defaultChanDoanHinhAnhApDungShouldBeFound("id.lessThanOrEqual=" + id);
        defaultChanDoanHinhAnhApDungShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByMaBaoCaoBhxhIsEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where maBaoCaoBhxh equals to DEFAULT_MA_BAO_CAO_BHXH
        defaultChanDoanHinhAnhApDungShouldBeFound("maBaoCaoBhxh.equals=" + DEFAULT_MA_BAO_CAO_BHXH);

        // Get all the chanDoanHinhAnhApDungList where maBaoCaoBhxh equals to UPDATED_MA_BAO_CAO_BHXH
        defaultChanDoanHinhAnhApDungShouldNotBeFound("maBaoCaoBhxh.equals=" + UPDATED_MA_BAO_CAO_BHXH);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByMaBaoCaoBhxhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where maBaoCaoBhxh not equals to DEFAULT_MA_BAO_CAO_BHXH
        defaultChanDoanHinhAnhApDungShouldNotBeFound("maBaoCaoBhxh.notEquals=" + DEFAULT_MA_BAO_CAO_BHXH);

        // Get all the chanDoanHinhAnhApDungList where maBaoCaoBhxh not equals to UPDATED_MA_BAO_CAO_BHXH
        defaultChanDoanHinhAnhApDungShouldBeFound("maBaoCaoBhxh.notEquals=" + UPDATED_MA_BAO_CAO_BHXH);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByMaBaoCaoBhxhIsInShouldWork() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where maBaoCaoBhxh in DEFAULT_MA_BAO_CAO_BHXH or UPDATED_MA_BAO_CAO_BHXH
        defaultChanDoanHinhAnhApDungShouldBeFound("maBaoCaoBhxh.in=" + DEFAULT_MA_BAO_CAO_BHXH + "," + UPDATED_MA_BAO_CAO_BHXH);

        // Get all the chanDoanHinhAnhApDungList where maBaoCaoBhxh equals to UPDATED_MA_BAO_CAO_BHXH
        defaultChanDoanHinhAnhApDungShouldNotBeFound("maBaoCaoBhxh.in=" + UPDATED_MA_BAO_CAO_BHXH);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByMaBaoCaoBhxhIsNullOrNotNull() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where maBaoCaoBhxh is not null
        defaultChanDoanHinhAnhApDungShouldBeFound("maBaoCaoBhxh.specified=true");

        // Get all the chanDoanHinhAnhApDungList where maBaoCaoBhxh is null
        defaultChanDoanHinhAnhApDungShouldNotBeFound("maBaoCaoBhxh.specified=false");
    }
                @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByMaBaoCaoBhxhContainsSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where maBaoCaoBhxh contains DEFAULT_MA_BAO_CAO_BHXH
        defaultChanDoanHinhAnhApDungShouldBeFound("maBaoCaoBhxh.contains=" + DEFAULT_MA_BAO_CAO_BHXH);

        // Get all the chanDoanHinhAnhApDungList where maBaoCaoBhxh contains UPDATED_MA_BAO_CAO_BHXH
        defaultChanDoanHinhAnhApDungShouldNotBeFound("maBaoCaoBhxh.contains=" + UPDATED_MA_BAO_CAO_BHXH);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByMaBaoCaoBhxhNotContainsSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where maBaoCaoBhxh does not contain DEFAULT_MA_BAO_CAO_BHXH
        defaultChanDoanHinhAnhApDungShouldNotBeFound("maBaoCaoBhxh.doesNotContain=" + DEFAULT_MA_BAO_CAO_BHXH);

        // Get all the chanDoanHinhAnhApDungList where maBaoCaoBhxh does not contain UPDATED_MA_BAO_CAO_BHXH
        defaultChanDoanHinhAnhApDungShouldBeFound("maBaoCaoBhxh.doesNotContain=" + UPDATED_MA_BAO_CAO_BHXH);
    }


    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByMaBaoCaoBhytIsEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where maBaoCaoBhyt equals to DEFAULT_MA_BAO_CAO_BHYT
        defaultChanDoanHinhAnhApDungShouldBeFound("maBaoCaoBhyt.equals=" + DEFAULT_MA_BAO_CAO_BHYT);

        // Get all the chanDoanHinhAnhApDungList where maBaoCaoBhyt equals to UPDATED_MA_BAO_CAO_BHYT
        defaultChanDoanHinhAnhApDungShouldNotBeFound("maBaoCaoBhyt.equals=" + UPDATED_MA_BAO_CAO_BHYT);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByMaBaoCaoBhytIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where maBaoCaoBhyt not equals to DEFAULT_MA_BAO_CAO_BHYT
        defaultChanDoanHinhAnhApDungShouldNotBeFound("maBaoCaoBhyt.notEquals=" + DEFAULT_MA_BAO_CAO_BHYT);

        // Get all the chanDoanHinhAnhApDungList where maBaoCaoBhyt not equals to UPDATED_MA_BAO_CAO_BHYT
        defaultChanDoanHinhAnhApDungShouldBeFound("maBaoCaoBhyt.notEquals=" + UPDATED_MA_BAO_CAO_BHYT);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByMaBaoCaoBhytIsInShouldWork() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where maBaoCaoBhyt in DEFAULT_MA_BAO_CAO_BHYT or UPDATED_MA_BAO_CAO_BHYT
        defaultChanDoanHinhAnhApDungShouldBeFound("maBaoCaoBhyt.in=" + DEFAULT_MA_BAO_CAO_BHYT + "," + UPDATED_MA_BAO_CAO_BHYT);

        // Get all the chanDoanHinhAnhApDungList where maBaoCaoBhyt equals to UPDATED_MA_BAO_CAO_BHYT
        defaultChanDoanHinhAnhApDungShouldNotBeFound("maBaoCaoBhyt.in=" + UPDATED_MA_BAO_CAO_BHYT);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByMaBaoCaoBhytIsNullOrNotNull() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where maBaoCaoBhyt is not null
        defaultChanDoanHinhAnhApDungShouldBeFound("maBaoCaoBhyt.specified=true");

        // Get all the chanDoanHinhAnhApDungList where maBaoCaoBhyt is null
        defaultChanDoanHinhAnhApDungShouldNotBeFound("maBaoCaoBhyt.specified=false");
    }
                @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByMaBaoCaoBhytContainsSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where maBaoCaoBhyt contains DEFAULT_MA_BAO_CAO_BHYT
        defaultChanDoanHinhAnhApDungShouldBeFound("maBaoCaoBhyt.contains=" + DEFAULT_MA_BAO_CAO_BHYT);

        // Get all the chanDoanHinhAnhApDungList where maBaoCaoBhyt contains UPDATED_MA_BAO_CAO_BHYT
        defaultChanDoanHinhAnhApDungShouldNotBeFound("maBaoCaoBhyt.contains=" + UPDATED_MA_BAO_CAO_BHYT);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByMaBaoCaoBhytNotContainsSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where maBaoCaoBhyt does not contain DEFAULT_MA_BAO_CAO_BHYT
        defaultChanDoanHinhAnhApDungShouldNotBeFound("maBaoCaoBhyt.doesNotContain=" + DEFAULT_MA_BAO_CAO_BHYT);

        // Get all the chanDoanHinhAnhApDungList where maBaoCaoBhyt does not contain UPDATED_MA_BAO_CAO_BHYT
        defaultChanDoanHinhAnhApDungShouldBeFound("maBaoCaoBhyt.doesNotContain=" + UPDATED_MA_BAO_CAO_BHYT);
    }


    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByNgayApDungIsEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where ngayApDung equals to DEFAULT_NGAY_AP_DUNG
        defaultChanDoanHinhAnhApDungShouldBeFound("ngayApDung.equals=" + DEFAULT_NGAY_AP_DUNG);

        // Get all the chanDoanHinhAnhApDungList where ngayApDung equals to UPDATED_NGAY_AP_DUNG
        defaultChanDoanHinhAnhApDungShouldNotBeFound("ngayApDung.equals=" + UPDATED_NGAY_AP_DUNG);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByNgayApDungIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where ngayApDung not equals to DEFAULT_NGAY_AP_DUNG
        defaultChanDoanHinhAnhApDungShouldNotBeFound("ngayApDung.notEquals=" + DEFAULT_NGAY_AP_DUNG);

        // Get all the chanDoanHinhAnhApDungList where ngayApDung not equals to UPDATED_NGAY_AP_DUNG
        defaultChanDoanHinhAnhApDungShouldBeFound("ngayApDung.notEquals=" + UPDATED_NGAY_AP_DUNG);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByNgayApDungIsInShouldWork() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where ngayApDung in DEFAULT_NGAY_AP_DUNG or UPDATED_NGAY_AP_DUNG
        defaultChanDoanHinhAnhApDungShouldBeFound("ngayApDung.in=" + DEFAULT_NGAY_AP_DUNG + "," + UPDATED_NGAY_AP_DUNG);

        // Get all the chanDoanHinhAnhApDungList where ngayApDung equals to UPDATED_NGAY_AP_DUNG
        defaultChanDoanHinhAnhApDungShouldNotBeFound("ngayApDung.in=" + UPDATED_NGAY_AP_DUNG);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByNgayApDungIsNullOrNotNull() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where ngayApDung is not null
        defaultChanDoanHinhAnhApDungShouldBeFound("ngayApDung.specified=true");

        // Get all the chanDoanHinhAnhApDungList where ngayApDung is null
        defaultChanDoanHinhAnhApDungShouldNotBeFound("ngayApDung.specified=false");
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByNgayApDungIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where ngayApDung is greater than or equal to DEFAULT_NGAY_AP_DUNG
        defaultChanDoanHinhAnhApDungShouldBeFound("ngayApDung.greaterThanOrEqual=" + DEFAULT_NGAY_AP_DUNG);

        // Get all the chanDoanHinhAnhApDungList where ngayApDung is greater than or equal to UPDATED_NGAY_AP_DUNG
        defaultChanDoanHinhAnhApDungShouldNotBeFound("ngayApDung.greaterThanOrEqual=" + UPDATED_NGAY_AP_DUNG);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByNgayApDungIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where ngayApDung is less than or equal to DEFAULT_NGAY_AP_DUNG
        defaultChanDoanHinhAnhApDungShouldBeFound("ngayApDung.lessThanOrEqual=" + DEFAULT_NGAY_AP_DUNG);

        // Get all the chanDoanHinhAnhApDungList where ngayApDung is less than or equal to SMALLER_NGAY_AP_DUNG
        defaultChanDoanHinhAnhApDungShouldNotBeFound("ngayApDung.lessThanOrEqual=" + SMALLER_NGAY_AP_DUNG);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByNgayApDungIsLessThanSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where ngayApDung is less than DEFAULT_NGAY_AP_DUNG
        defaultChanDoanHinhAnhApDungShouldNotBeFound("ngayApDung.lessThan=" + DEFAULT_NGAY_AP_DUNG);

        // Get all the chanDoanHinhAnhApDungList where ngayApDung is less than UPDATED_NGAY_AP_DUNG
        defaultChanDoanHinhAnhApDungShouldBeFound("ngayApDung.lessThan=" + UPDATED_NGAY_AP_DUNG);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByNgayApDungIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where ngayApDung is greater than DEFAULT_NGAY_AP_DUNG
        defaultChanDoanHinhAnhApDungShouldNotBeFound("ngayApDung.greaterThan=" + DEFAULT_NGAY_AP_DUNG);

        // Get all the chanDoanHinhAnhApDungList where ngayApDung is greater than SMALLER_NGAY_AP_DUNG
        defaultChanDoanHinhAnhApDungShouldBeFound("ngayApDung.greaterThan=" + SMALLER_NGAY_AP_DUNG);
    }


    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsBySoCongVanBhxhIsEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where soCongVanBhxh equals to DEFAULT_SO_CONG_VAN_BHXH
        defaultChanDoanHinhAnhApDungShouldBeFound("soCongVanBhxh.equals=" + DEFAULT_SO_CONG_VAN_BHXH);

        // Get all the chanDoanHinhAnhApDungList where soCongVanBhxh equals to UPDATED_SO_CONG_VAN_BHXH
        defaultChanDoanHinhAnhApDungShouldNotBeFound("soCongVanBhxh.equals=" + UPDATED_SO_CONG_VAN_BHXH);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsBySoCongVanBhxhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where soCongVanBhxh not equals to DEFAULT_SO_CONG_VAN_BHXH
        defaultChanDoanHinhAnhApDungShouldNotBeFound("soCongVanBhxh.notEquals=" + DEFAULT_SO_CONG_VAN_BHXH);

        // Get all the chanDoanHinhAnhApDungList where soCongVanBhxh not equals to UPDATED_SO_CONG_VAN_BHXH
        defaultChanDoanHinhAnhApDungShouldBeFound("soCongVanBhxh.notEquals=" + UPDATED_SO_CONG_VAN_BHXH);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsBySoCongVanBhxhIsInShouldWork() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where soCongVanBhxh in DEFAULT_SO_CONG_VAN_BHXH or UPDATED_SO_CONG_VAN_BHXH
        defaultChanDoanHinhAnhApDungShouldBeFound("soCongVanBhxh.in=" + DEFAULT_SO_CONG_VAN_BHXH + "," + UPDATED_SO_CONG_VAN_BHXH);

        // Get all the chanDoanHinhAnhApDungList where soCongVanBhxh equals to UPDATED_SO_CONG_VAN_BHXH
        defaultChanDoanHinhAnhApDungShouldNotBeFound("soCongVanBhxh.in=" + UPDATED_SO_CONG_VAN_BHXH);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsBySoCongVanBhxhIsNullOrNotNull() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where soCongVanBhxh is not null
        defaultChanDoanHinhAnhApDungShouldBeFound("soCongVanBhxh.specified=true");

        // Get all the chanDoanHinhAnhApDungList where soCongVanBhxh is null
        defaultChanDoanHinhAnhApDungShouldNotBeFound("soCongVanBhxh.specified=false");
    }
                @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsBySoCongVanBhxhContainsSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where soCongVanBhxh contains DEFAULT_SO_CONG_VAN_BHXH
        defaultChanDoanHinhAnhApDungShouldBeFound("soCongVanBhxh.contains=" + DEFAULT_SO_CONG_VAN_BHXH);

        // Get all the chanDoanHinhAnhApDungList where soCongVanBhxh contains UPDATED_SO_CONG_VAN_BHXH
        defaultChanDoanHinhAnhApDungShouldNotBeFound("soCongVanBhxh.contains=" + UPDATED_SO_CONG_VAN_BHXH);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsBySoCongVanBhxhNotContainsSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where soCongVanBhxh does not contain DEFAULT_SO_CONG_VAN_BHXH
        defaultChanDoanHinhAnhApDungShouldNotBeFound("soCongVanBhxh.doesNotContain=" + DEFAULT_SO_CONG_VAN_BHXH);

        // Get all the chanDoanHinhAnhApDungList where soCongVanBhxh does not contain UPDATED_SO_CONG_VAN_BHXH
        defaultChanDoanHinhAnhApDungShouldBeFound("soCongVanBhxh.doesNotContain=" + UPDATED_SO_CONG_VAN_BHXH);
    }


    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsBySoQuyetDinhIsEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where soQuyetDinh equals to DEFAULT_SO_QUYET_DINH
        defaultChanDoanHinhAnhApDungShouldBeFound("soQuyetDinh.equals=" + DEFAULT_SO_QUYET_DINH);

        // Get all the chanDoanHinhAnhApDungList where soQuyetDinh equals to UPDATED_SO_QUYET_DINH
        defaultChanDoanHinhAnhApDungShouldNotBeFound("soQuyetDinh.equals=" + UPDATED_SO_QUYET_DINH);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsBySoQuyetDinhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where soQuyetDinh not equals to DEFAULT_SO_QUYET_DINH
        defaultChanDoanHinhAnhApDungShouldNotBeFound("soQuyetDinh.notEquals=" + DEFAULT_SO_QUYET_DINH);

        // Get all the chanDoanHinhAnhApDungList where soQuyetDinh not equals to UPDATED_SO_QUYET_DINH
        defaultChanDoanHinhAnhApDungShouldBeFound("soQuyetDinh.notEquals=" + UPDATED_SO_QUYET_DINH);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsBySoQuyetDinhIsInShouldWork() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where soQuyetDinh in DEFAULT_SO_QUYET_DINH or UPDATED_SO_QUYET_DINH
        defaultChanDoanHinhAnhApDungShouldBeFound("soQuyetDinh.in=" + DEFAULT_SO_QUYET_DINH + "," + UPDATED_SO_QUYET_DINH);

        // Get all the chanDoanHinhAnhApDungList where soQuyetDinh equals to UPDATED_SO_QUYET_DINH
        defaultChanDoanHinhAnhApDungShouldNotBeFound("soQuyetDinh.in=" + UPDATED_SO_QUYET_DINH);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsBySoQuyetDinhIsNullOrNotNull() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where soQuyetDinh is not null
        defaultChanDoanHinhAnhApDungShouldBeFound("soQuyetDinh.specified=true");

        // Get all the chanDoanHinhAnhApDungList where soQuyetDinh is null
        defaultChanDoanHinhAnhApDungShouldNotBeFound("soQuyetDinh.specified=false");
    }
                @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsBySoQuyetDinhContainsSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where soQuyetDinh contains DEFAULT_SO_QUYET_DINH
        defaultChanDoanHinhAnhApDungShouldBeFound("soQuyetDinh.contains=" + DEFAULT_SO_QUYET_DINH);

        // Get all the chanDoanHinhAnhApDungList where soQuyetDinh contains UPDATED_SO_QUYET_DINH
        defaultChanDoanHinhAnhApDungShouldNotBeFound("soQuyetDinh.contains=" + UPDATED_SO_QUYET_DINH);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsBySoQuyetDinhNotContainsSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where soQuyetDinh does not contain DEFAULT_SO_QUYET_DINH
        defaultChanDoanHinhAnhApDungShouldNotBeFound("soQuyetDinh.doesNotContain=" + DEFAULT_SO_QUYET_DINH);

        // Get all the chanDoanHinhAnhApDungList where soQuyetDinh does not contain UPDATED_SO_QUYET_DINH
        defaultChanDoanHinhAnhApDungShouldBeFound("soQuyetDinh.doesNotContain=" + UPDATED_SO_QUYET_DINH);
    }


    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTenBaoCaoBhxhIsEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tenBaoCaoBhxh equals to DEFAULT_TEN_BAO_CAO_BHXH
        defaultChanDoanHinhAnhApDungShouldBeFound("tenBaoCaoBhxh.equals=" + DEFAULT_TEN_BAO_CAO_BHXH);

        // Get all the chanDoanHinhAnhApDungList where tenBaoCaoBhxh equals to UPDATED_TEN_BAO_CAO_BHXH
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tenBaoCaoBhxh.equals=" + UPDATED_TEN_BAO_CAO_BHXH);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTenBaoCaoBhxhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tenBaoCaoBhxh not equals to DEFAULT_TEN_BAO_CAO_BHXH
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tenBaoCaoBhxh.notEquals=" + DEFAULT_TEN_BAO_CAO_BHXH);

        // Get all the chanDoanHinhAnhApDungList where tenBaoCaoBhxh not equals to UPDATED_TEN_BAO_CAO_BHXH
        defaultChanDoanHinhAnhApDungShouldBeFound("tenBaoCaoBhxh.notEquals=" + UPDATED_TEN_BAO_CAO_BHXH);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTenBaoCaoBhxhIsInShouldWork() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tenBaoCaoBhxh in DEFAULT_TEN_BAO_CAO_BHXH or UPDATED_TEN_BAO_CAO_BHXH
        defaultChanDoanHinhAnhApDungShouldBeFound("tenBaoCaoBhxh.in=" + DEFAULT_TEN_BAO_CAO_BHXH + "," + UPDATED_TEN_BAO_CAO_BHXH);

        // Get all the chanDoanHinhAnhApDungList where tenBaoCaoBhxh equals to UPDATED_TEN_BAO_CAO_BHXH
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tenBaoCaoBhxh.in=" + UPDATED_TEN_BAO_CAO_BHXH);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTenBaoCaoBhxhIsNullOrNotNull() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tenBaoCaoBhxh is not null
        defaultChanDoanHinhAnhApDungShouldBeFound("tenBaoCaoBhxh.specified=true");

        // Get all the chanDoanHinhAnhApDungList where tenBaoCaoBhxh is null
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tenBaoCaoBhxh.specified=false");
    }
                @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTenBaoCaoBhxhContainsSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tenBaoCaoBhxh contains DEFAULT_TEN_BAO_CAO_BHXH
        defaultChanDoanHinhAnhApDungShouldBeFound("tenBaoCaoBhxh.contains=" + DEFAULT_TEN_BAO_CAO_BHXH);

        // Get all the chanDoanHinhAnhApDungList where tenBaoCaoBhxh contains UPDATED_TEN_BAO_CAO_BHXH
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tenBaoCaoBhxh.contains=" + UPDATED_TEN_BAO_CAO_BHXH);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTenBaoCaoBhxhNotContainsSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tenBaoCaoBhxh does not contain DEFAULT_TEN_BAO_CAO_BHXH
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tenBaoCaoBhxh.doesNotContain=" + DEFAULT_TEN_BAO_CAO_BHXH);

        // Get all the chanDoanHinhAnhApDungList where tenBaoCaoBhxh does not contain UPDATED_TEN_BAO_CAO_BHXH
        defaultChanDoanHinhAnhApDungShouldBeFound("tenBaoCaoBhxh.doesNotContain=" + UPDATED_TEN_BAO_CAO_BHXH);
    }


    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTenDichVuKhongBhytIsEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tenDichVuKhongBhyt equals to DEFAULT_TEN_DICH_VU_KHONG_BHYT
        defaultChanDoanHinhAnhApDungShouldBeFound("tenDichVuKhongBhyt.equals=" + DEFAULT_TEN_DICH_VU_KHONG_BHYT);

        // Get all the chanDoanHinhAnhApDungList where tenDichVuKhongBhyt equals to UPDATED_TEN_DICH_VU_KHONG_BHYT
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tenDichVuKhongBhyt.equals=" + UPDATED_TEN_DICH_VU_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTenDichVuKhongBhytIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tenDichVuKhongBhyt not equals to DEFAULT_TEN_DICH_VU_KHONG_BHYT
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tenDichVuKhongBhyt.notEquals=" + DEFAULT_TEN_DICH_VU_KHONG_BHYT);

        // Get all the chanDoanHinhAnhApDungList where tenDichVuKhongBhyt not equals to UPDATED_TEN_DICH_VU_KHONG_BHYT
        defaultChanDoanHinhAnhApDungShouldBeFound("tenDichVuKhongBhyt.notEquals=" + UPDATED_TEN_DICH_VU_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTenDichVuKhongBhytIsInShouldWork() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tenDichVuKhongBhyt in DEFAULT_TEN_DICH_VU_KHONG_BHYT or UPDATED_TEN_DICH_VU_KHONG_BHYT
        defaultChanDoanHinhAnhApDungShouldBeFound("tenDichVuKhongBhyt.in=" + DEFAULT_TEN_DICH_VU_KHONG_BHYT + "," + UPDATED_TEN_DICH_VU_KHONG_BHYT);

        // Get all the chanDoanHinhAnhApDungList where tenDichVuKhongBhyt equals to UPDATED_TEN_DICH_VU_KHONG_BHYT
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tenDichVuKhongBhyt.in=" + UPDATED_TEN_DICH_VU_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTenDichVuKhongBhytIsNullOrNotNull() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tenDichVuKhongBhyt is not null
        defaultChanDoanHinhAnhApDungShouldBeFound("tenDichVuKhongBhyt.specified=true");

        // Get all the chanDoanHinhAnhApDungList where tenDichVuKhongBhyt is null
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tenDichVuKhongBhyt.specified=false");
    }
                @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTenDichVuKhongBhytContainsSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tenDichVuKhongBhyt contains DEFAULT_TEN_DICH_VU_KHONG_BHYT
        defaultChanDoanHinhAnhApDungShouldBeFound("tenDichVuKhongBhyt.contains=" + DEFAULT_TEN_DICH_VU_KHONG_BHYT);

        // Get all the chanDoanHinhAnhApDungList where tenDichVuKhongBhyt contains UPDATED_TEN_DICH_VU_KHONG_BHYT
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tenDichVuKhongBhyt.contains=" + UPDATED_TEN_DICH_VU_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTenDichVuKhongBhytNotContainsSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tenDichVuKhongBhyt does not contain DEFAULT_TEN_DICH_VU_KHONG_BHYT
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tenDichVuKhongBhyt.doesNotContain=" + DEFAULT_TEN_DICH_VU_KHONG_BHYT);

        // Get all the chanDoanHinhAnhApDungList where tenDichVuKhongBhyt does not contain UPDATED_TEN_DICH_VU_KHONG_BHYT
        defaultChanDoanHinhAnhApDungShouldBeFound("tenDichVuKhongBhyt.doesNotContain=" + UPDATED_TEN_DICH_VU_KHONG_BHYT);
    }


    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTienBenhNhanChiIsEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tienBenhNhanChi equals to DEFAULT_TIEN_BENH_NHAN_CHI
        defaultChanDoanHinhAnhApDungShouldBeFound("tienBenhNhanChi.equals=" + DEFAULT_TIEN_BENH_NHAN_CHI);

        // Get all the chanDoanHinhAnhApDungList where tienBenhNhanChi equals to UPDATED_TIEN_BENH_NHAN_CHI
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tienBenhNhanChi.equals=" + UPDATED_TIEN_BENH_NHAN_CHI);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTienBenhNhanChiIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tienBenhNhanChi not equals to DEFAULT_TIEN_BENH_NHAN_CHI
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tienBenhNhanChi.notEquals=" + DEFAULT_TIEN_BENH_NHAN_CHI);

        // Get all the chanDoanHinhAnhApDungList where tienBenhNhanChi not equals to UPDATED_TIEN_BENH_NHAN_CHI
        defaultChanDoanHinhAnhApDungShouldBeFound("tienBenhNhanChi.notEquals=" + UPDATED_TIEN_BENH_NHAN_CHI);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTienBenhNhanChiIsInShouldWork() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tienBenhNhanChi in DEFAULT_TIEN_BENH_NHAN_CHI or UPDATED_TIEN_BENH_NHAN_CHI
        defaultChanDoanHinhAnhApDungShouldBeFound("tienBenhNhanChi.in=" + DEFAULT_TIEN_BENH_NHAN_CHI + "," + UPDATED_TIEN_BENH_NHAN_CHI);

        // Get all the chanDoanHinhAnhApDungList where tienBenhNhanChi equals to UPDATED_TIEN_BENH_NHAN_CHI
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tienBenhNhanChi.in=" + UPDATED_TIEN_BENH_NHAN_CHI);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTienBenhNhanChiIsNullOrNotNull() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tienBenhNhanChi is not null
        defaultChanDoanHinhAnhApDungShouldBeFound("tienBenhNhanChi.specified=true");

        // Get all the chanDoanHinhAnhApDungList where tienBenhNhanChi is null
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tienBenhNhanChi.specified=false");
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTienBenhNhanChiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tienBenhNhanChi is greater than or equal to DEFAULT_TIEN_BENH_NHAN_CHI
        defaultChanDoanHinhAnhApDungShouldBeFound("tienBenhNhanChi.greaterThanOrEqual=" + DEFAULT_TIEN_BENH_NHAN_CHI);

        // Get all the chanDoanHinhAnhApDungList where tienBenhNhanChi is greater than or equal to UPDATED_TIEN_BENH_NHAN_CHI
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tienBenhNhanChi.greaterThanOrEqual=" + UPDATED_TIEN_BENH_NHAN_CHI);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTienBenhNhanChiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tienBenhNhanChi is less than or equal to DEFAULT_TIEN_BENH_NHAN_CHI
        defaultChanDoanHinhAnhApDungShouldBeFound("tienBenhNhanChi.lessThanOrEqual=" + DEFAULT_TIEN_BENH_NHAN_CHI);

        // Get all the chanDoanHinhAnhApDungList where tienBenhNhanChi is less than or equal to SMALLER_TIEN_BENH_NHAN_CHI
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tienBenhNhanChi.lessThanOrEqual=" + SMALLER_TIEN_BENH_NHAN_CHI);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTienBenhNhanChiIsLessThanSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tienBenhNhanChi is less than DEFAULT_TIEN_BENH_NHAN_CHI
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tienBenhNhanChi.lessThan=" + DEFAULT_TIEN_BENH_NHAN_CHI);

        // Get all the chanDoanHinhAnhApDungList where tienBenhNhanChi is less than UPDATED_TIEN_BENH_NHAN_CHI
        defaultChanDoanHinhAnhApDungShouldBeFound("tienBenhNhanChi.lessThan=" + UPDATED_TIEN_BENH_NHAN_CHI);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTienBenhNhanChiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tienBenhNhanChi is greater than DEFAULT_TIEN_BENH_NHAN_CHI
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tienBenhNhanChi.greaterThan=" + DEFAULT_TIEN_BENH_NHAN_CHI);

        // Get all the chanDoanHinhAnhApDungList where tienBenhNhanChi is greater than SMALLER_TIEN_BENH_NHAN_CHI
        defaultChanDoanHinhAnhApDungShouldBeFound("tienBenhNhanChi.greaterThan=" + SMALLER_TIEN_BENH_NHAN_CHI);
    }


    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTienBhxhChiIsEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tienBhxhChi equals to DEFAULT_TIEN_BHXH_CHI
        defaultChanDoanHinhAnhApDungShouldBeFound("tienBhxhChi.equals=" + DEFAULT_TIEN_BHXH_CHI);

        // Get all the chanDoanHinhAnhApDungList where tienBhxhChi equals to UPDATED_TIEN_BHXH_CHI
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tienBhxhChi.equals=" + UPDATED_TIEN_BHXH_CHI);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTienBhxhChiIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tienBhxhChi not equals to DEFAULT_TIEN_BHXH_CHI
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tienBhxhChi.notEquals=" + DEFAULT_TIEN_BHXH_CHI);

        // Get all the chanDoanHinhAnhApDungList where tienBhxhChi not equals to UPDATED_TIEN_BHXH_CHI
        defaultChanDoanHinhAnhApDungShouldBeFound("tienBhxhChi.notEquals=" + UPDATED_TIEN_BHXH_CHI);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTienBhxhChiIsInShouldWork() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tienBhxhChi in DEFAULT_TIEN_BHXH_CHI or UPDATED_TIEN_BHXH_CHI
        defaultChanDoanHinhAnhApDungShouldBeFound("tienBhxhChi.in=" + DEFAULT_TIEN_BHXH_CHI + "," + UPDATED_TIEN_BHXH_CHI);

        // Get all the chanDoanHinhAnhApDungList where tienBhxhChi equals to UPDATED_TIEN_BHXH_CHI
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tienBhxhChi.in=" + UPDATED_TIEN_BHXH_CHI);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTienBhxhChiIsNullOrNotNull() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tienBhxhChi is not null
        defaultChanDoanHinhAnhApDungShouldBeFound("tienBhxhChi.specified=true");

        // Get all the chanDoanHinhAnhApDungList where tienBhxhChi is null
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tienBhxhChi.specified=false");
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTienBhxhChiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tienBhxhChi is greater than or equal to DEFAULT_TIEN_BHXH_CHI
        defaultChanDoanHinhAnhApDungShouldBeFound("tienBhxhChi.greaterThanOrEqual=" + DEFAULT_TIEN_BHXH_CHI);

        // Get all the chanDoanHinhAnhApDungList where tienBhxhChi is greater than or equal to UPDATED_TIEN_BHXH_CHI
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tienBhxhChi.greaterThanOrEqual=" + UPDATED_TIEN_BHXH_CHI);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTienBhxhChiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tienBhxhChi is less than or equal to DEFAULT_TIEN_BHXH_CHI
        defaultChanDoanHinhAnhApDungShouldBeFound("tienBhxhChi.lessThanOrEqual=" + DEFAULT_TIEN_BHXH_CHI);

        // Get all the chanDoanHinhAnhApDungList where tienBhxhChi is less than or equal to SMALLER_TIEN_BHXH_CHI
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tienBhxhChi.lessThanOrEqual=" + SMALLER_TIEN_BHXH_CHI);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTienBhxhChiIsLessThanSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tienBhxhChi is less than DEFAULT_TIEN_BHXH_CHI
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tienBhxhChi.lessThan=" + DEFAULT_TIEN_BHXH_CHI);

        // Get all the chanDoanHinhAnhApDungList where tienBhxhChi is less than UPDATED_TIEN_BHXH_CHI
        defaultChanDoanHinhAnhApDungShouldBeFound("tienBhxhChi.lessThan=" + UPDATED_TIEN_BHXH_CHI);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTienBhxhChiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tienBhxhChi is greater than DEFAULT_TIEN_BHXH_CHI
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tienBhxhChi.greaterThan=" + DEFAULT_TIEN_BHXH_CHI);

        // Get all the chanDoanHinhAnhApDungList where tienBhxhChi is greater than SMALLER_TIEN_BHXH_CHI
        defaultChanDoanHinhAnhApDungShouldBeFound("tienBhxhChi.greaterThan=" + SMALLER_TIEN_BHXH_CHI);
    }


    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTienNgoaiBhytIsEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tienNgoaiBhyt equals to DEFAULT_TIEN_NGOAI_BHYT
        defaultChanDoanHinhAnhApDungShouldBeFound("tienNgoaiBhyt.equals=" + DEFAULT_TIEN_NGOAI_BHYT);

        // Get all the chanDoanHinhAnhApDungList where tienNgoaiBhyt equals to UPDATED_TIEN_NGOAI_BHYT
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tienNgoaiBhyt.equals=" + UPDATED_TIEN_NGOAI_BHYT);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTienNgoaiBhytIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tienNgoaiBhyt not equals to DEFAULT_TIEN_NGOAI_BHYT
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tienNgoaiBhyt.notEquals=" + DEFAULT_TIEN_NGOAI_BHYT);

        // Get all the chanDoanHinhAnhApDungList where tienNgoaiBhyt not equals to UPDATED_TIEN_NGOAI_BHYT
        defaultChanDoanHinhAnhApDungShouldBeFound("tienNgoaiBhyt.notEquals=" + UPDATED_TIEN_NGOAI_BHYT);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTienNgoaiBhytIsInShouldWork() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tienNgoaiBhyt in DEFAULT_TIEN_NGOAI_BHYT or UPDATED_TIEN_NGOAI_BHYT
        defaultChanDoanHinhAnhApDungShouldBeFound("tienNgoaiBhyt.in=" + DEFAULT_TIEN_NGOAI_BHYT + "," + UPDATED_TIEN_NGOAI_BHYT);

        // Get all the chanDoanHinhAnhApDungList where tienNgoaiBhyt equals to UPDATED_TIEN_NGOAI_BHYT
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tienNgoaiBhyt.in=" + UPDATED_TIEN_NGOAI_BHYT);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTienNgoaiBhytIsNullOrNotNull() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tienNgoaiBhyt is not null
        defaultChanDoanHinhAnhApDungShouldBeFound("tienNgoaiBhyt.specified=true");

        // Get all the chanDoanHinhAnhApDungList where tienNgoaiBhyt is null
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tienNgoaiBhyt.specified=false");
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTienNgoaiBhytIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tienNgoaiBhyt is greater than or equal to DEFAULT_TIEN_NGOAI_BHYT
        defaultChanDoanHinhAnhApDungShouldBeFound("tienNgoaiBhyt.greaterThanOrEqual=" + DEFAULT_TIEN_NGOAI_BHYT);

        // Get all the chanDoanHinhAnhApDungList where tienNgoaiBhyt is greater than or equal to UPDATED_TIEN_NGOAI_BHYT
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tienNgoaiBhyt.greaterThanOrEqual=" + UPDATED_TIEN_NGOAI_BHYT);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTienNgoaiBhytIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tienNgoaiBhyt is less than or equal to DEFAULT_TIEN_NGOAI_BHYT
        defaultChanDoanHinhAnhApDungShouldBeFound("tienNgoaiBhyt.lessThanOrEqual=" + DEFAULT_TIEN_NGOAI_BHYT);

        // Get all the chanDoanHinhAnhApDungList where tienNgoaiBhyt is less than or equal to SMALLER_TIEN_NGOAI_BHYT
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tienNgoaiBhyt.lessThanOrEqual=" + SMALLER_TIEN_NGOAI_BHYT);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTienNgoaiBhytIsLessThanSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tienNgoaiBhyt is less than DEFAULT_TIEN_NGOAI_BHYT
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tienNgoaiBhyt.lessThan=" + DEFAULT_TIEN_NGOAI_BHYT);

        // Get all the chanDoanHinhAnhApDungList where tienNgoaiBhyt is less than UPDATED_TIEN_NGOAI_BHYT
        defaultChanDoanHinhAnhApDungShouldBeFound("tienNgoaiBhyt.lessThan=" + UPDATED_TIEN_NGOAI_BHYT);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTienNgoaiBhytIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tienNgoaiBhyt is greater than DEFAULT_TIEN_NGOAI_BHYT
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tienNgoaiBhyt.greaterThan=" + DEFAULT_TIEN_NGOAI_BHYT);

        // Get all the chanDoanHinhAnhApDungList where tienNgoaiBhyt is greater than SMALLER_TIEN_NGOAI_BHYT
        defaultChanDoanHinhAnhApDungShouldBeFound("tienNgoaiBhyt.greaterThan=" + SMALLER_TIEN_NGOAI_BHYT);
    }


    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTongTienThanhToanIsEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tongTienThanhToan equals to DEFAULT_TONG_TIEN_THANH_TOAN
        defaultChanDoanHinhAnhApDungShouldBeFound("tongTienThanhToan.equals=" + DEFAULT_TONG_TIEN_THANH_TOAN);

        // Get all the chanDoanHinhAnhApDungList where tongTienThanhToan equals to UPDATED_TONG_TIEN_THANH_TOAN
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tongTienThanhToan.equals=" + UPDATED_TONG_TIEN_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTongTienThanhToanIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tongTienThanhToan not equals to DEFAULT_TONG_TIEN_THANH_TOAN
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tongTienThanhToan.notEquals=" + DEFAULT_TONG_TIEN_THANH_TOAN);

        // Get all the chanDoanHinhAnhApDungList where tongTienThanhToan not equals to UPDATED_TONG_TIEN_THANH_TOAN
        defaultChanDoanHinhAnhApDungShouldBeFound("tongTienThanhToan.notEquals=" + UPDATED_TONG_TIEN_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTongTienThanhToanIsInShouldWork() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tongTienThanhToan in DEFAULT_TONG_TIEN_THANH_TOAN or UPDATED_TONG_TIEN_THANH_TOAN
        defaultChanDoanHinhAnhApDungShouldBeFound("tongTienThanhToan.in=" + DEFAULT_TONG_TIEN_THANH_TOAN + "," + UPDATED_TONG_TIEN_THANH_TOAN);

        // Get all the chanDoanHinhAnhApDungList where tongTienThanhToan equals to UPDATED_TONG_TIEN_THANH_TOAN
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tongTienThanhToan.in=" + UPDATED_TONG_TIEN_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTongTienThanhToanIsNullOrNotNull() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tongTienThanhToan is not null
        defaultChanDoanHinhAnhApDungShouldBeFound("tongTienThanhToan.specified=true");

        // Get all the chanDoanHinhAnhApDungList where tongTienThanhToan is null
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tongTienThanhToan.specified=false");
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTongTienThanhToanIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tongTienThanhToan is greater than or equal to DEFAULT_TONG_TIEN_THANH_TOAN
        defaultChanDoanHinhAnhApDungShouldBeFound("tongTienThanhToan.greaterThanOrEqual=" + DEFAULT_TONG_TIEN_THANH_TOAN);

        // Get all the chanDoanHinhAnhApDungList where tongTienThanhToan is greater than or equal to UPDATED_TONG_TIEN_THANH_TOAN
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tongTienThanhToan.greaterThanOrEqual=" + UPDATED_TONG_TIEN_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTongTienThanhToanIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tongTienThanhToan is less than or equal to DEFAULT_TONG_TIEN_THANH_TOAN
        defaultChanDoanHinhAnhApDungShouldBeFound("tongTienThanhToan.lessThanOrEqual=" + DEFAULT_TONG_TIEN_THANH_TOAN);

        // Get all the chanDoanHinhAnhApDungList where tongTienThanhToan is less than or equal to SMALLER_TONG_TIEN_THANH_TOAN
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tongTienThanhToan.lessThanOrEqual=" + SMALLER_TONG_TIEN_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTongTienThanhToanIsLessThanSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tongTienThanhToan is less than DEFAULT_TONG_TIEN_THANH_TOAN
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tongTienThanhToan.lessThan=" + DEFAULT_TONG_TIEN_THANH_TOAN);

        // Get all the chanDoanHinhAnhApDungList where tongTienThanhToan is less than UPDATED_TONG_TIEN_THANH_TOAN
        defaultChanDoanHinhAnhApDungShouldBeFound("tongTienThanhToan.lessThan=" + UPDATED_TONG_TIEN_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTongTienThanhToanIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tongTienThanhToan is greater than DEFAULT_TONG_TIEN_THANH_TOAN
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tongTienThanhToan.greaterThan=" + DEFAULT_TONG_TIEN_THANH_TOAN);

        // Get all the chanDoanHinhAnhApDungList where tongTienThanhToan is greater than SMALLER_TONG_TIEN_THANH_TOAN
        defaultChanDoanHinhAnhApDungShouldBeFound("tongTienThanhToan.greaterThan=" + SMALLER_TONG_TIEN_THANH_TOAN);
    }


    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTyLeBhxhThanhToanIsEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tyLeBhxhThanhToan equals to DEFAULT_TY_LE_BHXH_THANH_TOAN
        defaultChanDoanHinhAnhApDungShouldBeFound("tyLeBhxhThanhToan.equals=" + DEFAULT_TY_LE_BHXH_THANH_TOAN);

        // Get all the chanDoanHinhAnhApDungList where tyLeBhxhThanhToan equals to UPDATED_TY_LE_BHXH_THANH_TOAN
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tyLeBhxhThanhToan.equals=" + UPDATED_TY_LE_BHXH_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTyLeBhxhThanhToanIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tyLeBhxhThanhToan not equals to DEFAULT_TY_LE_BHXH_THANH_TOAN
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tyLeBhxhThanhToan.notEquals=" + DEFAULT_TY_LE_BHXH_THANH_TOAN);

        // Get all the chanDoanHinhAnhApDungList where tyLeBhxhThanhToan not equals to UPDATED_TY_LE_BHXH_THANH_TOAN
        defaultChanDoanHinhAnhApDungShouldBeFound("tyLeBhxhThanhToan.notEquals=" + UPDATED_TY_LE_BHXH_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTyLeBhxhThanhToanIsInShouldWork() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tyLeBhxhThanhToan in DEFAULT_TY_LE_BHXH_THANH_TOAN or UPDATED_TY_LE_BHXH_THANH_TOAN
        defaultChanDoanHinhAnhApDungShouldBeFound("tyLeBhxhThanhToan.in=" + DEFAULT_TY_LE_BHXH_THANH_TOAN + "," + UPDATED_TY_LE_BHXH_THANH_TOAN);

        // Get all the chanDoanHinhAnhApDungList where tyLeBhxhThanhToan equals to UPDATED_TY_LE_BHXH_THANH_TOAN
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tyLeBhxhThanhToan.in=" + UPDATED_TY_LE_BHXH_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTyLeBhxhThanhToanIsNullOrNotNull() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tyLeBhxhThanhToan is not null
        defaultChanDoanHinhAnhApDungShouldBeFound("tyLeBhxhThanhToan.specified=true");

        // Get all the chanDoanHinhAnhApDungList where tyLeBhxhThanhToan is null
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tyLeBhxhThanhToan.specified=false");
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTyLeBhxhThanhToanIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tyLeBhxhThanhToan is greater than or equal to DEFAULT_TY_LE_BHXH_THANH_TOAN
        defaultChanDoanHinhAnhApDungShouldBeFound("tyLeBhxhThanhToan.greaterThanOrEqual=" + DEFAULT_TY_LE_BHXH_THANH_TOAN);

        // Get all the chanDoanHinhAnhApDungList where tyLeBhxhThanhToan is greater than or equal to UPDATED_TY_LE_BHXH_THANH_TOAN
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tyLeBhxhThanhToan.greaterThanOrEqual=" + UPDATED_TY_LE_BHXH_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTyLeBhxhThanhToanIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tyLeBhxhThanhToan is less than or equal to DEFAULT_TY_LE_BHXH_THANH_TOAN
        defaultChanDoanHinhAnhApDungShouldBeFound("tyLeBhxhThanhToan.lessThanOrEqual=" + DEFAULT_TY_LE_BHXH_THANH_TOAN);

        // Get all the chanDoanHinhAnhApDungList where tyLeBhxhThanhToan is less than or equal to SMALLER_TY_LE_BHXH_THANH_TOAN
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tyLeBhxhThanhToan.lessThanOrEqual=" + SMALLER_TY_LE_BHXH_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTyLeBhxhThanhToanIsLessThanSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tyLeBhxhThanhToan is less than DEFAULT_TY_LE_BHXH_THANH_TOAN
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tyLeBhxhThanhToan.lessThan=" + DEFAULT_TY_LE_BHXH_THANH_TOAN);

        // Get all the chanDoanHinhAnhApDungList where tyLeBhxhThanhToan is less than UPDATED_TY_LE_BHXH_THANH_TOAN
        defaultChanDoanHinhAnhApDungShouldBeFound("tyLeBhxhThanhToan.lessThan=" + UPDATED_TY_LE_BHXH_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByTyLeBhxhThanhToanIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where tyLeBhxhThanhToan is greater than DEFAULT_TY_LE_BHXH_THANH_TOAN
        defaultChanDoanHinhAnhApDungShouldNotBeFound("tyLeBhxhThanhToan.greaterThan=" + DEFAULT_TY_LE_BHXH_THANH_TOAN);

        // Get all the chanDoanHinhAnhApDungList where tyLeBhxhThanhToan is greater than SMALLER_TY_LE_BHXH_THANH_TOAN
        defaultChanDoanHinhAnhApDungShouldBeFound("tyLeBhxhThanhToan.greaterThan=" + SMALLER_TY_LE_BHXH_THANH_TOAN);
    }


    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByGiaBhytIsEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where giaBhyt equals to DEFAULT_GIA_BHYT
        defaultChanDoanHinhAnhApDungShouldBeFound("giaBhyt.equals=" + DEFAULT_GIA_BHYT);

        // Get all the chanDoanHinhAnhApDungList where giaBhyt equals to UPDATED_GIA_BHYT
        defaultChanDoanHinhAnhApDungShouldNotBeFound("giaBhyt.equals=" + UPDATED_GIA_BHYT);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByGiaBhytIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where giaBhyt not equals to DEFAULT_GIA_BHYT
        defaultChanDoanHinhAnhApDungShouldNotBeFound("giaBhyt.notEquals=" + DEFAULT_GIA_BHYT);

        // Get all the chanDoanHinhAnhApDungList where giaBhyt not equals to UPDATED_GIA_BHYT
        defaultChanDoanHinhAnhApDungShouldBeFound("giaBhyt.notEquals=" + UPDATED_GIA_BHYT);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByGiaBhytIsInShouldWork() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where giaBhyt in DEFAULT_GIA_BHYT or UPDATED_GIA_BHYT
        defaultChanDoanHinhAnhApDungShouldBeFound("giaBhyt.in=" + DEFAULT_GIA_BHYT + "," + UPDATED_GIA_BHYT);

        // Get all the chanDoanHinhAnhApDungList where giaBhyt equals to UPDATED_GIA_BHYT
        defaultChanDoanHinhAnhApDungShouldNotBeFound("giaBhyt.in=" + UPDATED_GIA_BHYT);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByGiaBhytIsNullOrNotNull() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where giaBhyt is not null
        defaultChanDoanHinhAnhApDungShouldBeFound("giaBhyt.specified=true");

        // Get all the chanDoanHinhAnhApDungList where giaBhyt is null
        defaultChanDoanHinhAnhApDungShouldNotBeFound("giaBhyt.specified=false");
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByGiaBhytIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where giaBhyt is greater than or equal to DEFAULT_GIA_BHYT
        defaultChanDoanHinhAnhApDungShouldBeFound("giaBhyt.greaterThanOrEqual=" + DEFAULT_GIA_BHYT);

        // Get all the chanDoanHinhAnhApDungList where giaBhyt is greater than or equal to UPDATED_GIA_BHYT
        defaultChanDoanHinhAnhApDungShouldNotBeFound("giaBhyt.greaterThanOrEqual=" + UPDATED_GIA_BHYT);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByGiaBhytIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where giaBhyt is less than or equal to DEFAULT_GIA_BHYT
        defaultChanDoanHinhAnhApDungShouldBeFound("giaBhyt.lessThanOrEqual=" + DEFAULT_GIA_BHYT);

        // Get all the chanDoanHinhAnhApDungList where giaBhyt is less than or equal to SMALLER_GIA_BHYT
        defaultChanDoanHinhAnhApDungShouldNotBeFound("giaBhyt.lessThanOrEqual=" + SMALLER_GIA_BHYT);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByGiaBhytIsLessThanSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where giaBhyt is less than DEFAULT_GIA_BHYT
        defaultChanDoanHinhAnhApDungShouldNotBeFound("giaBhyt.lessThan=" + DEFAULT_GIA_BHYT);

        // Get all the chanDoanHinhAnhApDungList where giaBhyt is less than UPDATED_GIA_BHYT
        defaultChanDoanHinhAnhApDungShouldBeFound("giaBhyt.lessThan=" + UPDATED_GIA_BHYT);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByGiaBhytIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where giaBhyt is greater than DEFAULT_GIA_BHYT
        defaultChanDoanHinhAnhApDungShouldNotBeFound("giaBhyt.greaterThan=" + DEFAULT_GIA_BHYT);

        // Get all the chanDoanHinhAnhApDungList where giaBhyt is greater than SMALLER_GIA_BHYT
        defaultChanDoanHinhAnhApDungShouldBeFound("giaBhyt.greaterThan=" + SMALLER_GIA_BHYT);
    }


    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByGiaKhongBhytIsEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where giaKhongBhyt equals to DEFAULT_GIA_KHONG_BHYT
        defaultChanDoanHinhAnhApDungShouldBeFound("giaKhongBhyt.equals=" + DEFAULT_GIA_KHONG_BHYT);

        // Get all the chanDoanHinhAnhApDungList where giaKhongBhyt equals to UPDATED_GIA_KHONG_BHYT
        defaultChanDoanHinhAnhApDungShouldNotBeFound("giaKhongBhyt.equals=" + UPDATED_GIA_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByGiaKhongBhytIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where giaKhongBhyt not equals to DEFAULT_GIA_KHONG_BHYT
        defaultChanDoanHinhAnhApDungShouldNotBeFound("giaKhongBhyt.notEquals=" + DEFAULT_GIA_KHONG_BHYT);

        // Get all the chanDoanHinhAnhApDungList where giaKhongBhyt not equals to UPDATED_GIA_KHONG_BHYT
        defaultChanDoanHinhAnhApDungShouldBeFound("giaKhongBhyt.notEquals=" + UPDATED_GIA_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByGiaKhongBhytIsInShouldWork() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where giaKhongBhyt in DEFAULT_GIA_KHONG_BHYT or UPDATED_GIA_KHONG_BHYT
        defaultChanDoanHinhAnhApDungShouldBeFound("giaKhongBhyt.in=" + DEFAULT_GIA_KHONG_BHYT + "," + UPDATED_GIA_KHONG_BHYT);

        // Get all the chanDoanHinhAnhApDungList where giaKhongBhyt equals to UPDATED_GIA_KHONG_BHYT
        defaultChanDoanHinhAnhApDungShouldNotBeFound("giaKhongBhyt.in=" + UPDATED_GIA_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByGiaKhongBhytIsNullOrNotNull() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where giaKhongBhyt is not null
        defaultChanDoanHinhAnhApDungShouldBeFound("giaKhongBhyt.specified=true");

        // Get all the chanDoanHinhAnhApDungList where giaKhongBhyt is null
        defaultChanDoanHinhAnhApDungShouldNotBeFound("giaKhongBhyt.specified=false");
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByGiaKhongBhytIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where giaKhongBhyt is greater than or equal to DEFAULT_GIA_KHONG_BHYT
        defaultChanDoanHinhAnhApDungShouldBeFound("giaKhongBhyt.greaterThanOrEqual=" + DEFAULT_GIA_KHONG_BHYT);

        // Get all the chanDoanHinhAnhApDungList where giaKhongBhyt is greater than or equal to UPDATED_GIA_KHONG_BHYT
        defaultChanDoanHinhAnhApDungShouldNotBeFound("giaKhongBhyt.greaterThanOrEqual=" + UPDATED_GIA_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByGiaKhongBhytIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where giaKhongBhyt is less than or equal to DEFAULT_GIA_KHONG_BHYT
        defaultChanDoanHinhAnhApDungShouldBeFound("giaKhongBhyt.lessThanOrEqual=" + DEFAULT_GIA_KHONG_BHYT);

        // Get all the chanDoanHinhAnhApDungList where giaKhongBhyt is less than or equal to SMALLER_GIA_KHONG_BHYT
        defaultChanDoanHinhAnhApDungShouldNotBeFound("giaKhongBhyt.lessThanOrEqual=" + SMALLER_GIA_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByGiaKhongBhytIsLessThanSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where giaKhongBhyt is less than DEFAULT_GIA_KHONG_BHYT
        defaultChanDoanHinhAnhApDungShouldNotBeFound("giaKhongBhyt.lessThan=" + DEFAULT_GIA_KHONG_BHYT);

        // Get all the chanDoanHinhAnhApDungList where giaKhongBhyt is less than UPDATED_GIA_KHONG_BHYT
        defaultChanDoanHinhAnhApDungShouldBeFound("giaKhongBhyt.lessThan=" + UPDATED_GIA_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByGiaKhongBhytIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where giaKhongBhyt is greater than DEFAULT_GIA_KHONG_BHYT
        defaultChanDoanHinhAnhApDungShouldNotBeFound("giaKhongBhyt.greaterThan=" + DEFAULT_GIA_KHONG_BHYT);

        // Get all the chanDoanHinhAnhApDungList where giaKhongBhyt is greater than SMALLER_GIA_KHONG_BHYT
        defaultChanDoanHinhAnhApDungShouldBeFound("giaKhongBhyt.greaterThan=" + SMALLER_GIA_KHONG_BHYT);
    }


    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByDoiTuongDacBietIsEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where doiTuongDacBiet equals to DEFAULT_DOI_TUONG_DAC_BIET
        defaultChanDoanHinhAnhApDungShouldBeFound("doiTuongDacBiet.equals=" + DEFAULT_DOI_TUONG_DAC_BIET);

        // Get all the chanDoanHinhAnhApDungList where doiTuongDacBiet equals to UPDATED_DOI_TUONG_DAC_BIET
        defaultChanDoanHinhAnhApDungShouldNotBeFound("doiTuongDacBiet.equals=" + UPDATED_DOI_TUONG_DAC_BIET);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByDoiTuongDacBietIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where doiTuongDacBiet not equals to DEFAULT_DOI_TUONG_DAC_BIET
        defaultChanDoanHinhAnhApDungShouldNotBeFound("doiTuongDacBiet.notEquals=" + DEFAULT_DOI_TUONG_DAC_BIET);

        // Get all the chanDoanHinhAnhApDungList where doiTuongDacBiet not equals to UPDATED_DOI_TUONG_DAC_BIET
        defaultChanDoanHinhAnhApDungShouldBeFound("doiTuongDacBiet.notEquals=" + UPDATED_DOI_TUONG_DAC_BIET);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByDoiTuongDacBietIsInShouldWork() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where doiTuongDacBiet in DEFAULT_DOI_TUONG_DAC_BIET or UPDATED_DOI_TUONG_DAC_BIET
        defaultChanDoanHinhAnhApDungShouldBeFound("doiTuongDacBiet.in=" + DEFAULT_DOI_TUONG_DAC_BIET + "," + UPDATED_DOI_TUONG_DAC_BIET);

        // Get all the chanDoanHinhAnhApDungList where doiTuongDacBiet equals to UPDATED_DOI_TUONG_DAC_BIET
        defaultChanDoanHinhAnhApDungShouldNotBeFound("doiTuongDacBiet.in=" + UPDATED_DOI_TUONG_DAC_BIET);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByDoiTuongDacBietIsNullOrNotNull() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where doiTuongDacBiet is not null
        defaultChanDoanHinhAnhApDungShouldBeFound("doiTuongDacBiet.specified=true");

        // Get all the chanDoanHinhAnhApDungList where doiTuongDacBiet is null
        defaultChanDoanHinhAnhApDungShouldNotBeFound("doiTuongDacBiet.specified=false");
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByNguonChiIsEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where nguonChi equals to DEFAULT_NGUON_CHI
        defaultChanDoanHinhAnhApDungShouldBeFound("nguonChi.equals=" + DEFAULT_NGUON_CHI);

        // Get all the chanDoanHinhAnhApDungList where nguonChi equals to UPDATED_NGUON_CHI
        defaultChanDoanHinhAnhApDungShouldNotBeFound("nguonChi.equals=" + UPDATED_NGUON_CHI);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByNguonChiIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where nguonChi not equals to DEFAULT_NGUON_CHI
        defaultChanDoanHinhAnhApDungShouldNotBeFound("nguonChi.notEquals=" + DEFAULT_NGUON_CHI);

        // Get all the chanDoanHinhAnhApDungList where nguonChi not equals to UPDATED_NGUON_CHI
        defaultChanDoanHinhAnhApDungShouldBeFound("nguonChi.notEquals=" + UPDATED_NGUON_CHI);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByNguonChiIsInShouldWork() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where nguonChi in DEFAULT_NGUON_CHI or UPDATED_NGUON_CHI
        defaultChanDoanHinhAnhApDungShouldBeFound("nguonChi.in=" + DEFAULT_NGUON_CHI + "," + UPDATED_NGUON_CHI);

        // Get all the chanDoanHinhAnhApDungList where nguonChi equals to UPDATED_NGUON_CHI
        defaultChanDoanHinhAnhApDungShouldNotBeFound("nguonChi.in=" + UPDATED_NGUON_CHI);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByNguonChiIsNullOrNotNull() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where nguonChi is not null
        defaultChanDoanHinhAnhApDungShouldBeFound("nguonChi.specified=true");

        // Get all the chanDoanHinhAnhApDungList where nguonChi is null
        defaultChanDoanHinhAnhApDungShouldNotBeFound("nguonChi.specified=false");
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByNguonChiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where nguonChi is greater than or equal to DEFAULT_NGUON_CHI
        defaultChanDoanHinhAnhApDungShouldBeFound("nguonChi.greaterThanOrEqual=" + DEFAULT_NGUON_CHI);

        // Get all the chanDoanHinhAnhApDungList where nguonChi is greater than or equal to UPDATED_NGUON_CHI
        defaultChanDoanHinhAnhApDungShouldNotBeFound("nguonChi.greaterThanOrEqual=" + UPDATED_NGUON_CHI);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByNguonChiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where nguonChi is less than or equal to DEFAULT_NGUON_CHI
        defaultChanDoanHinhAnhApDungShouldBeFound("nguonChi.lessThanOrEqual=" + DEFAULT_NGUON_CHI);

        // Get all the chanDoanHinhAnhApDungList where nguonChi is less than or equal to SMALLER_NGUON_CHI
        defaultChanDoanHinhAnhApDungShouldNotBeFound("nguonChi.lessThanOrEqual=" + SMALLER_NGUON_CHI);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByNguonChiIsLessThanSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where nguonChi is less than DEFAULT_NGUON_CHI
        defaultChanDoanHinhAnhApDungShouldNotBeFound("nguonChi.lessThan=" + DEFAULT_NGUON_CHI);

        // Get all the chanDoanHinhAnhApDungList where nguonChi is less than UPDATED_NGUON_CHI
        defaultChanDoanHinhAnhApDungShouldBeFound("nguonChi.lessThan=" + UPDATED_NGUON_CHI);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByNguonChiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where nguonChi is greater than DEFAULT_NGUON_CHI
        defaultChanDoanHinhAnhApDungShouldNotBeFound("nguonChi.greaterThan=" + DEFAULT_NGUON_CHI);

        // Get all the chanDoanHinhAnhApDungList where nguonChi is greater than SMALLER_NGUON_CHI
        defaultChanDoanHinhAnhApDungShouldBeFound("nguonChi.greaterThan=" + SMALLER_NGUON_CHI);
    }


    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByEnableIsEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where enable equals to DEFAULT_ENABLE
        defaultChanDoanHinhAnhApDungShouldBeFound("enable.equals=" + DEFAULT_ENABLE);

        // Get all the chanDoanHinhAnhApDungList where enable equals to UPDATED_ENABLE
        defaultChanDoanHinhAnhApDungShouldNotBeFound("enable.equals=" + UPDATED_ENABLE);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByEnableIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where enable not equals to DEFAULT_ENABLE
        defaultChanDoanHinhAnhApDungShouldNotBeFound("enable.notEquals=" + DEFAULT_ENABLE);

        // Get all the chanDoanHinhAnhApDungList where enable not equals to UPDATED_ENABLE
        defaultChanDoanHinhAnhApDungShouldBeFound("enable.notEquals=" + UPDATED_ENABLE);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByEnableIsInShouldWork() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where enable in DEFAULT_ENABLE or UPDATED_ENABLE
        defaultChanDoanHinhAnhApDungShouldBeFound("enable.in=" + DEFAULT_ENABLE + "," + UPDATED_ENABLE);

        // Get all the chanDoanHinhAnhApDungList where enable equals to UPDATED_ENABLE
        defaultChanDoanHinhAnhApDungShouldNotBeFound("enable.in=" + UPDATED_ENABLE);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByEnableIsNullOrNotNull() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        // Get all the chanDoanHinhAnhApDungList where enable is not null
        defaultChanDoanHinhAnhApDungShouldBeFound("enable.specified=true");

        // Get all the chanDoanHinhAnhApDungList where enable is null
        defaultChanDoanHinhAnhApDungShouldNotBeFound("enable.specified=false");
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByDotGiaIsEqualToSomething() throws Exception {
        // Get already existing entity
        DotGiaDichVuBhxh dotGia = chanDoanHinhAnhApDung.getDotGia();
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);
        Long dotGiaId = dotGia.getId();

        // Get all the chanDoanHinhAnhApDungList where dotGia equals to dotGiaId
        defaultChanDoanHinhAnhApDungShouldBeFound("dotGiaId.equals=" + dotGiaId);

        // Get all the chanDoanHinhAnhApDungList where dotGia equals to dotGiaId + 1
        defaultChanDoanHinhAnhApDungShouldNotBeFound("dotGiaId.equals=" + (dotGiaId + 1));
    }


    @Test
    @Transactional
    public void getAllChanDoanHinhAnhApDungsByCdhaIsEqualToSomething() throws Exception {
        // Get already existing entity
        ChanDoanHinhAnh cdha = chanDoanHinhAnhApDung.getCdha();
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);
        Long cdhaId = cdha.getId();

        // Get all the chanDoanHinhAnhApDungList where cdha equals to cdhaId
        defaultChanDoanHinhAnhApDungShouldBeFound("cdhaId.equals=" + cdhaId);

        // Get all the chanDoanHinhAnhApDungList where cdha equals to cdhaId + 1
        defaultChanDoanHinhAnhApDungShouldNotBeFound("cdhaId.equals=" + (cdhaId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultChanDoanHinhAnhApDungShouldBeFound(String filter) throws Exception {
        restChanDoanHinhAnhApDungMockMvc.perform(get("/api/chan-doan-hinh-anh-ap-dungs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(chanDoanHinhAnhApDung.getId().intValue())))
            .andExpect(jsonPath("$.[*].maBaoCaoBhxh").value(hasItem(DEFAULT_MA_BAO_CAO_BHXH)))
            .andExpect(jsonPath("$.[*].maBaoCaoBhyt").value(hasItem(DEFAULT_MA_BAO_CAO_BHYT)))
            .andExpect(jsonPath("$.[*].ngayApDung").value(hasItem(DEFAULT_NGAY_AP_DUNG.toString())))
            .andExpect(jsonPath("$.[*].soCongVanBhxh").value(hasItem(DEFAULT_SO_CONG_VAN_BHXH)))
            .andExpect(jsonPath("$.[*].soQuyetDinh").value(hasItem(DEFAULT_SO_QUYET_DINH)))
            .andExpect(jsonPath("$.[*].tenBaoCaoBhxh").value(hasItem(DEFAULT_TEN_BAO_CAO_BHXH)))
            .andExpect(jsonPath("$.[*].tenDichVuKhongBhyt").value(hasItem(DEFAULT_TEN_DICH_VU_KHONG_BHYT)))
            .andExpect(jsonPath("$.[*].tienBenhNhanChi").value(hasItem(DEFAULT_TIEN_BENH_NHAN_CHI.intValue())))
            .andExpect(jsonPath("$.[*].tienBhxhChi").value(hasItem(DEFAULT_TIEN_BHXH_CHI.intValue())))
            .andExpect(jsonPath("$.[*].tienNgoaiBhyt").value(hasItem(DEFAULT_TIEN_NGOAI_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].tongTienThanhToan").value(hasItem(DEFAULT_TONG_TIEN_THANH_TOAN.intValue())))
            .andExpect(jsonPath("$.[*].tyLeBhxhThanhToan").value(hasItem(DEFAULT_TY_LE_BHXH_THANH_TOAN)))
            .andExpect(jsonPath("$.[*].giaBhyt").value(hasItem(DEFAULT_GIA_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].giaKhongBhyt").value(hasItem(DEFAULT_GIA_KHONG_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].doiTuongDacBiet").value(hasItem(DEFAULT_DOI_TUONG_DAC_BIET.booleanValue())))
            .andExpect(jsonPath("$.[*].nguonChi").value(hasItem(DEFAULT_NGUON_CHI)))
            .andExpect(jsonPath("$.[*].enable").value(hasItem(DEFAULT_ENABLE.booleanValue())));

        // Check, that the count call also returns 1
        restChanDoanHinhAnhApDungMockMvc.perform(get("/api/chan-doan-hinh-anh-ap-dungs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultChanDoanHinhAnhApDungShouldNotBeFound(String filter) throws Exception {
        restChanDoanHinhAnhApDungMockMvc.perform(get("/api/chan-doan-hinh-anh-ap-dungs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restChanDoanHinhAnhApDungMockMvc.perform(get("/api/chan-doan-hinh-anh-ap-dungs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingChanDoanHinhAnhApDung() throws Exception {
        // Get the chanDoanHinhAnhApDung
        restChanDoanHinhAnhApDungMockMvc.perform(get("/api/chan-doan-hinh-anh-ap-dungs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateChanDoanHinhAnhApDung() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        int databaseSizeBeforeUpdate = chanDoanHinhAnhApDungRepository.findAll().size();

        // Update the chanDoanHinhAnhApDung
        ChanDoanHinhAnhApDung updatedChanDoanHinhAnhApDung = chanDoanHinhAnhApDungRepository.findById(chanDoanHinhAnhApDung.getId()).get();
        // Disconnect from session so that the updates on updatedChanDoanHinhAnhApDung are not directly saved in db
        em.detach(updatedChanDoanHinhAnhApDung);
        updatedChanDoanHinhAnhApDung
            .maBaoCaoBhxh(UPDATED_MA_BAO_CAO_BHXH)
            .maBaoCaoBhyt(UPDATED_MA_BAO_CAO_BHYT)
            .ngayApDung(UPDATED_NGAY_AP_DUNG)
            .soCongVanBhxh(UPDATED_SO_CONG_VAN_BHXH)
            .soQuyetDinh(UPDATED_SO_QUYET_DINH)
            .tenBaoCaoBhxh(UPDATED_TEN_BAO_CAO_BHXH)
            .tenDichVuKhongBhyt(UPDATED_TEN_DICH_VU_KHONG_BHYT)
            .tienBenhNhanChi(UPDATED_TIEN_BENH_NHAN_CHI)
            .tienBhxhChi(UPDATED_TIEN_BHXH_CHI)
            .tienNgoaiBhyt(UPDATED_TIEN_NGOAI_BHYT)
            .tongTienThanhToan(UPDATED_TONG_TIEN_THANH_TOAN)
            .tyLeBhxhThanhToan(UPDATED_TY_LE_BHXH_THANH_TOAN)
            .giaBhyt(UPDATED_GIA_BHYT)
            .giaKhongBhyt(UPDATED_GIA_KHONG_BHYT)
            .doiTuongDacBiet(UPDATED_DOI_TUONG_DAC_BIET)
            .nguonChi(UPDATED_NGUON_CHI)
            .enable(UPDATED_ENABLE);
        ChanDoanHinhAnhApDungDTO chanDoanHinhAnhApDungDTO = chanDoanHinhAnhApDungMapper.toDto(updatedChanDoanHinhAnhApDung);

        restChanDoanHinhAnhApDungMockMvc.perform(put("/api/chan-doan-hinh-anh-ap-dungs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chanDoanHinhAnhApDungDTO)))
            .andExpect(status().isOk());

        // Validate the ChanDoanHinhAnhApDung in the database
        List<ChanDoanHinhAnhApDung> chanDoanHinhAnhApDungList = chanDoanHinhAnhApDungRepository.findAll();
        assertThat(chanDoanHinhAnhApDungList).hasSize(databaseSizeBeforeUpdate);
        ChanDoanHinhAnhApDung testChanDoanHinhAnhApDung = chanDoanHinhAnhApDungList.get(chanDoanHinhAnhApDungList.size() - 1);
        assertThat(testChanDoanHinhAnhApDung.getMaBaoCaoBhxh()).isEqualTo(UPDATED_MA_BAO_CAO_BHXH);
        assertThat(testChanDoanHinhAnhApDung.getMaBaoCaoBhyt()).isEqualTo(UPDATED_MA_BAO_CAO_BHYT);
        assertThat(testChanDoanHinhAnhApDung.getNgayApDung()).isEqualTo(UPDATED_NGAY_AP_DUNG);
        assertThat(testChanDoanHinhAnhApDung.getSoCongVanBhxh()).isEqualTo(UPDATED_SO_CONG_VAN_BHXH);
        assertThat(testChanDoanHinhAnhApDung.getSoQuyetDinh()).isEqualTo(UPDATED_SO_QUYET_DINH);
        assertThat(testChanDoanHinhAnhApDung.getTenBaoCaoBhxh()).isEqualTo(UPDATED_TEN_BAO_CAO_BHXH);
        assertThat(testChanDoanHinhAnhApDung.getTenDichVuKhongBhyt()).isEqualTo(UPDATED_TEN_DICH_VU_KHONG_BHYT);
        assertThat(testChanDoanHinhAnhApDung.getTienBenhNhanChi()).isEqualTo(UPDATED_TIEN_BENH_NHAN_CHI);
        assertThat(testChanDoanHinhAnhApDung.getTienBhxhChi()).isEqualTo(UPDATED_TIEN_BHXH_CHI);
        assertThat(testChanDoanHinhAnhApDung.getTienNgoaiBhyt()).isEqualTo(UPDATED_TIEN_NGOAI_BHYT);
        assertThat(testChanDoanHinhAnhApDung.getTongTienThanhToan()).isEqualTo(UPDATED_TONG_TIEN_THANH_TOAN);
        assertThat(testChanDoanHinhAnhApDung.getTyLeBhxhThanhToan()).isEqualTo(UPDATED_TY_LE_BHXH_THANH_TOAN);
        assertThat(testChanDoanHinhAnhApDung.getGiaBhyt()).isEqualTo(UPDATED_GIA_BHYT);
        assertThat(testChanDoanHinhAnhApDung.getGiaKhongBhyt()).isEqualTo(UPDATED_GIA_KHONG_BHYT);
        assertThat(testChanDoanHinhAnhApDung.isDoiTuongDacBiet()).isEqualTo(UPDATED_DOI_TUONG_DAC_BIET);
        assertThat(testChanDoanHinhAnhApDung.getNguonChi()).isEqualTo(UPDATED_NGUON_CHI);
        assertThat(testChanDoanHinhAnhApDung.isEnable()).isEqualTo(UPDATED_ENABLE);
    }

    @Test
    @Transactional
    public void updateNonExistingChanDoanHinhAnhApDung() throws Exception {
        int databaseSizeBeforeUpdate = chanDoanHinhAnhApDungRepository.findAll().size();

        // Create the ChanDoanHinhAnhApDung
        ChanDoanHinhAnhApDungDTO chanDoanHinhAnhApDungDTO = chanDoanHinhAnhApDungMapper.toDto(chanDoanHinhAnhApDung);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restChanDoanHinhAnhApDungMockMvc.perform(put("/api/chan-doan-hinh-anh-ap-dungs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chanDoanHinhAnhApDungDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ChanDoanHinhAnhApDung in the database
        List<ChanDoanHinhAnhApDung> chanDoanHinhAnhApDungList = chanDoanHinhAnhApDungRepository.findAll();
        assertThat(chanDoanHinhAnhApDungList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteChanDoanHinhAnhApDung() throws Exception {
        // Initialize the database
        chanDoanHinhAnhApDungRepository.saveAndFlush(chanDoanHinhAnhApDung);

        int databaseSizeBeforeDelete = chanDoanHinhAnhApDungRepository.findAll().size();

        // Delete the chanDoanHinhAnhApDung
        restChanDoanHinhAnhApDungMockMvc.perform(delete("/api/chan-doan-hinh-anh-ap-dungs/{id}", chanDoanHinhAnhApDung.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ChanDoanHinhAnhApDung> chanDoanHinhAnhApDungList = chanDoanHinhAnhApDungRepository.findAll();
        assertThat(chanDoanHinhAnhApDungList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

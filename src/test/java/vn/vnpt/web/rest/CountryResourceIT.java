package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.Country;
import vn.vnpt.repository.CountryRepository;
import vn.vnpt.service.CountryService;
import vn.vnpt.service.dto.CountryDTO;
import vn.vnpt.service.mapper.CountryMapper;
import vn.vnpt.service.dto.CountryCriteria;
import vn.vnpt.service.CountryQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CountryResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class CountryResourceIT {

    private static final String DEFAULT_ISO_CODE = "AAAAAAAAAA";
    private static final String UPDATED_ISO_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_ISO_NUMERIC = "AAAAAAAAAA";
    private static final String UPDATED_ISO_NUMERIC = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Integer DEFAULT_MA_CTK = 1;
    private static final Integer UPDATED_MA_CTK = 2;
    private static final Integer SMALLER_MA_CTK = 1 - 1;

    private static final String DEFAULT_TEN_CTK = "AAAAAAAAAA";
    private static final String UPDATED_TEN_CTK = "BBBBBBBBBB";

    @Autowired
    private CountryRepository countryRepository;

    @Autowired
    private CountryMapper countryMapper;

    @Autowired
    private CountryService countryService;

    @Autowired
    private CountryQueryService countryQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCountryMockMvc;

    private Country country;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Country createEntity(EntityManager em) {
        Country country = new Country()
            .isoCode(DEFAULT_ISO_CODE)
            .isoNumeric(DEFAULT_ISO_NUMERIC)
            .name(DEFAULT_NAME)
            .maCtk(DEFAULT_MA_CTK)
            .tenCtk(DEFAULT_TEN_CTK);
        return country;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Country createUpdatedEntity(EntityManager em) {
        Country country = new Country()
            .isoCode(UPDATED_ISO_CODE)
            .isoNumeric(UPDATED_ISO_NUMERIC)
            .name(UPDATED_NAME)
            .maCtk(UPDATED_MA_CTK)
            .tenCtk(UPDATED_TEN_CTK);
        return country;
    }

    @BeforeEach
    public void initTest() {
        country = createEntity(em);
    }

    @Test
    @Transactional
    public void createCountry() throws Exception {
        int databaseSizeBeforeCreate = countryRepository.findAll().size();

        // Create the Country
        CountryDTO countryDTO = countryMapper.toDto(country);
        restCountryMockMvc.perform(post("/api/countries").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(countryDTO)))
            .andExpect(status().isCreated());

        // Validate the Country in the database
        List<Country> countryList = countryRepository.findAll();
        assertThat(countryList).hasSize(databaseSizeBeforeCreate + 1);
        Country testCountry = countryList.get(countryList.size() - 1);
        assertThat(testCountry.getIsoCode()).isEqualTo(DEFAULT_ISO_CODE);
        assertThat(testCountry.getIsoNumeric()).isEqualTo(DEFAULT_ISO_NUMERIC);
        assertThat(testCountry.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCountry.getMaCtk()).isEqualTo(DEFAULT_MA_CTK);
        assertThat(testCountry.getTenCtk()).isEqualTo(DEFAULT_TEN_CTK);
    }

    @Test
    @Transactional
    public void createCountryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = countryRepository.findAll().size();

        // Create the Country with an existing ID
        country.setId(1L);
        CountryDTO countryDTO = countryMapper.toDto(country);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCountryMockMvc.perform(post("/api/countries").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(countryDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Country in the database
        List<Country> countryList = countryRepository.findAll();
        assertThat(countryList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllCountries() throws Exception {
        // Initialize the database
        countryRepository.saveAndFlush(country);

        // Get all the countryList
        restCountryMockMvc.perform(get("/api/countries?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(country.getId().intValue())))
            .andExpect(jsonPath("$.[*].isoCode").value(hasItem(DEFAULT_ISO_CODE)))
            .andExpect(jsonPath("$.[*].isoNumeric").value(hasItem(DEFAULT_ISO_NUMERIC)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].maCtk").value(hasItem(DEFAULT_MA_CTK)))
            .andExpect(jsonPath("$.[*].tenCtk").value(hasItem(DEFAULT_TEN_CTK)));
    }
    
    @Test
    @Transactional
    public void getCountry() throws Exception {
        // Initialize the database
        countryRepository.saveAndFlush(country);

        // Get the country
        restCountryMockMvc.perform(get("/api/countries/{id}", country.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(country.getId().intValue()))
            .andExpect(jsonPath("$.isoCode").value(DEFAULT_ISO_CODE))
            .andExpect(jsonPath("$.isoNumeric").value(DEFAULT_ISO_NUMERIC))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.maCtk").value(DEFAULT_MA_CTK))
            .andExpect(jsonPath("$.tenCtk").value(DEFAULT_TEN_CTK));
    }


    @Test
    @Transactional
    public void getCountriesByIdFiltering() throws Exception {
        // Initialize the database
        countryRepository.saveAndFlush(country);

        Long id = country.getId();

        defaultCountryShouldBeFound("id.equals=" + id);
        defaultCountryShouldNotBeFound("id.notEquals=" + id);

        defaultCountryShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultCountryShouldNotBeFound("id.greaterThan=" + id);

        defaultCountryShouldBeFound("id.lessThanOrEqual=" + id);
        defaultCountryShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllCountriesByIsoCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        countryRepository.saveAndFlush(country);

        // Get all the countryList where isoCode equals to DEFAULT_ISO_CODE
        defaultCountryShouldBeFound("isoCode.equals=" + DEFAULT_ISO_CODE);

        // Get all the countryList where isoCode equals to UPDATED_ISO_CODE
        defaultCountryShouldNotBeFound("isoCode.equals=" + UPDATED_ISO_CODE);
    }

    @Test
    @Transactional
    public void getAllCountriesByIsoCodeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        countryRepository.saveAndFlush(country);

        // Get all the countryList where isoCode not equals to DEFAULT_ISO_CODE
        defaultCountryShouldNotBeFound("isoCode.notEquals=" + DEFAULT_ISO_CODE);

        // Get all the countryList where isoCode not equals to UPDATED_ISO_CODE
        defaultCountryShouldBeFound("isoCode.notEquals=" + UPDATED_ISO_CODE);
    }

    @Test
    @Transactional
    public void getAllCountriesByIsoCodeIsInShouldWork() throws Exception {
        // Initialize the database
        countryRepository.saveAndFlush(country);

        // Get all the countryList where isoCode in DEFAULT_ISO_CODE or UPDATED_ISO_CODE
        defaultCountryShouldBeFound("isoCode.in=" + DEFAULT_ISO_CODE + "," + UPDATED_ISO_CODE);

        // Get all the countryList where isoCode equals to UPDATED_ISO_CODE
        defaultCountryShouldNotBeFound("isoCode.in=" + UPDATED_ISO_CODE);
    }

    @Test
    @Transactional
    public void getAllCountriesByIsoCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        countryRepository.saveAndFlush(country);

        // Get all the countryList where isoCode is not null
        defaultCountryShouldBeFound("isoCode.specified=true");

        // Get all the countryList where isoCode is null
        defaultCountryShouldNotBeFound("isoCode.specified=false");
    }
                @Test
    @Transactional
    public void getAllCountriesByIsoCodeContainsSomething() throws Exception {
        // Initialize the database
        countryRepository.saveAndFlush(country);

        // Get all the countryList where isoCode contains DEFAULT_ISO_CODE
        defaultCountryShouldBeFound("isoCode.contains=" + DEFAULT_ISO_CODE);

        // Get all the countryList where isoCode contains UPDATED_ISO_CODE
        defaultCountryShouldNotBeFound("isoCode.contains=" + UPDATED_ISO_CODE);
    }

    @Test
    @Transactional
    public void getAllCountriesByIsoCodeNotContainsSomething() throws Exception {
        // Initialize the database
        countryRepository.saveAndFlush(country);

        // Get all the countryList where isoCode does not contain DEFAULT_ISO_CODE
        defaultCountryShouldNotBeFound("isoCode.doesNotContain=" + DEFAULT_ISO_CODE);

        // Get all the countryList where isoCode does not contain UPDATED_ISO_CODE
        defaultCountryShouldBeFound("isoCode.doesNotContain=" + UPDATED_ISO_CODE);
    }


    @Test
    @Transactional
    public void getAllCountriesByIsoNumericIsEqualToSomething() throws Exception {
        // Initialize the database
        countryRepository.saveAndFlush(country);

        // Get all the countryList where isoNumeric equals to DEFAULT_ISO_NUMERIC
        defaultCountryShouldBeFound("isoNumeric.equals=" + DEFAULT_ISO_NUMERIC);

        // Get all the countryList where isoNumeric equals to UPDATED_ISO_NUMERIC
        defaultCountryShouldNotBeFound("isoNumeric.equals=" + UPDATED_ISO_NUMERIC);
    }

    @Test
    @Transactional
    public void getAllCountriesByIsoNumericIsNotEqualToSomething() throws Exception {
        // Initialize the database
        countryRepository.saveAndFlush(country);

        // Get all the countryList where isoNumeric not equals to DEFAULT_ISO_NUMERIC
        defaultCountryShouldNotBeFound("isoNumeric.notEquals=" + DEFAULT_ISO_NUMERIC);

        // Get all the countryList where isoNumeric not equals to UPDATED_ISO_NUMERIC
        defaultCountryShouldBeFound("isoNumeric.notEquals=" + UPDATED_ISO_NUMERIC);
    }

    @Test
    @Transactional
    public void getAllCountriesByIsoNumericIsInShouldWork() throws Exception {
        // Initialize the database
        countryRepository.saveAndFlush(country);

        // Get all the countryList where isoNumeric in DEFAULT_ISO_NUMERIC or UPDATED_ISO_NUMERIC
        defaultCountryShouldBeFound("isoNumeric.in=" + DEFAULT_ISO_NUMERIC + "," + UPDATED_ISO_NUMERIC);

        // Get all the countryList where isoNumeric equals to UPDATED_ISO_NUMERIC
        defaultCountryShouldNotBeFound("isoNumeric.in=" + UPDATED_ISO_NUMERIC);
    }

    @Test
    @Transactional
    public void getAllCountriesByIsoNumericIsNullOrNotNull() throws Exception {
        // Initialize the database
        countryRepository.saveAndFlush(country);

        // Get all the countryList where isoNumeric is not null
        defaultCountryShouldBeFound("isoNumeric.specified=true");

        // Get all the countryList where isoNumeric is null
        defaultCountryShouldNotBeFound("isoNumeric.specified=false");
    }
                @Test
    @Transactional
    public void getAllCountriesByIsoNumericContainsSomething() throws Exception {
        // Initialize the database
        countryRepository.saveAndFlush(country);

        // Get all the countryList where isoNumeric contains DEFAULT_ISO_NUMERIC
        defaultCountryShouldBeFound("isoNumeric.contains=" + DEFAULT_ISO_NUMERIC);

        // Get all the countryList where isoNumeric contains UPDATED_ISO_NUMERIC
        defaultCountryShouldNotBeFound("isoNumeric.contains=" + UPDATED_ISO_NUMERIC);
    }

    @Test
    @Transactional
    public void getAllCountriesByIsoNumericNotContainsSomething() throws Exception {
        // Initialize the database
        countryRepository.saveAndFlush(country);

        // Get all the countryList where isoNumeric does not contain DEFAULT_ISO_NUMERIC
        defaultCountryShouldNotBeFound("isoNumeric.doesNotContain=" + DEFAULT_ISO_NUMERIC);

        // Get all the countryList where isoNumeric does not contain UPDATED_ISO_NUMERIC
        defaultCountryShouldBeFound("isoNumeric.doesNotContain=" + UPDATED_ISO_NUMERIC);
    }


    @Test
    @Transactional
    public void getAllCountriesByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        countryRepository.saveAndFlush(country);

        // Get all the countryList where name equals to DEFAULT_NAME
        defaultCountryShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the countryList where name equals to UPDATED_NAME
        defaultCountryShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllCountriesByNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        countryRepository.saveAndFlush(country);

        // Get all the countryList where name not equals to DEFAULT_NAME
        defaultCountryShouldNotBeFound("name.notEquals=" + DEFAULT_NAME);

        // Get all the countryList where name not equals to UPDATED_NAME
        defaultCountryShouldBeFound("name.notEquals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllCountriesByNameIsInShouldWork() throws Exception {
        // Initialize the database
        countryRepository.saveAndFlush(country);

        // Get all the countryList where name in DEFAULT_NAME or UPDATED_NAME
        defaultCountryShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the countryList where name equals to UPDATED_NAME
        defaultCountryShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllCountriesByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        countryRepository.saveAndFlush(country);

        // Get all the countryList where name is not null
        defaultCountryShouldBeFound("name.specified=true");

        // Get all the countryList where name is null
        defaultCountryShouldNotBeFound("name.specified=false");
    }
                @Test
    @Transactional
    public void getAllCountriesByNameContainsSomething() throws Exception {
        // Initialize the database
        countryRepository.saveAndFlush(country);

        // Get all the countryList where name contains DEFAULT_NAME
        defaultCountryShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the countryList where name contains UPDATED_NAME
        defaultCountryShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllCountriesByNameNotContainsSomething() throws Exception {
        // Initialize the database
        countryRepository.saveAndFlush(country);

        // Get all the countryList where name does not contain DEFAULT_NAME
        defaultCountryShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the countryList where name does not contain UPDATED_NAME
        defaultCountryShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }


    @Test
    @Transactional
    public void getAllCountriesByMaCtkIsEqualToSomething() throws Exception {
        // Initialize the database
        countryRepository.saveAndFlush(country);

        // Get all the countryList where maCtk equals to DEFAULT_MA_CTK
        defaultCountryShouldBeFound("maCtk.equals=" + DEFAULT_MA_CTK);

        // Get all the countryList where maCtk equals to UPDATED_MA_CTK
        defaultCountryShouldNotBeFound("maCtk.equals=" + UPDATED_MA_CTK);
    }

    @Test
    @Transactional
    public void getAllCountriesByMaCtkIsNotEqualToSomething() throws Exception {
        // Initialize the database
        countryRepository.saveAndFlush(country);

        // Get all the countryList where maCtk not equals to DEFAULT_MA_CTK
        defaultCountryShouldNotBeFound("maCtk.notEquals=" + DEFAULT_MA_CTK);

        // Get all the countryList where maCtk not equals to UPDATED_MA_CTK
        defaultCountryShouldBeFound("maCtk.notEquals=" + UPDATED_MA_CTK);
    }

    @Test
    @Transactional
    public void getAllCountriesByMaCtkIsInShouldWork() throws Exception {
        // Initialize the database
        countryRepository.saveAndFlush(country);

        // Get all the countryList where maCtk in DEFAULT_MA_CTK or UPDATED_MA_CTK
        defaultCountryShouldBeFound("maCtk.in=" + DEFAULT_MA_CTK + "," + UPDATED_MA_CTK);

        // Get all the countryList where maCtk equals to UPDATED_MA_CTK
        defaultCountryShouldNotBeFound("maCtk.in=" + UPDATED_MA_CTK);
    }

    @Test
    @Transactional
    public void getAllCountriesByMaCtkIsNullOrNotNull() throws Exception {
        // Initialize the database
        countryRepository.saveAndFlush(country);

        // Get all the countryList where maCtk is not null
        defaultCountryShouldBeFound("maCtk.specified=true");

        // Get all the countryList where maCtk is null
        defaultCountryShouldNotBeFound("maCtk.specified=false");
    }

    @Test
    @Transactional
    public void getAllCountriesByMaCtkIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        countryRepository.saveAndFlush(country);

        // Get all the countryList where maCtk is greater than or equal to DEFAULT_MA_CTK
        defaultCountryShouldBeFound("maCtk.greaterThanOrEqual=" + DEFAULT_MA_CTK);

        // Get all the countryList where maCtk is greater than or equal to UPDATED_MA_CTK
        defaultCountryShouldNotBeFound("maCtk.greaterThanOrEqual=" + UPDATED_MA_CTK);
    }

    @Test
    @Transactional
    public void getAllCountriesByMaCtkIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        countryRepository.saveAndFlush(country);

        // Get all the countryList where maCtk is less than or equal to DEFAULT_MA_CTK
        defaultCountryShouldBeFound("maCtk.lessThanOrEqual=" + DEFAULT_MA_CTK);

        // Get all the countryList where maCtk is less than or equal to SMALLER_MA_CTK
        defaultCountryShouldNotBeFound("maCtk.lessThanOrEqual=" + SMALLER_MA_CTK);
    }

    @Test
    @Transactional
    public void getAllCountriesByMaCtkIsLessThanSomething() throws Exception {
        // Initialize the database
        countryRepository.saveAndFlush(country);

        // Get all the countryList where maCtk is less than DEFAULT_MA_CTK
        defaultCountryShouldNotBeFound("maCtk.lessThan=" + DEFAULT_MA_CTK);

        // Get all the countryList where maCtk is less than UPDATED_MA_CTK
        defaultCountryShouldBeFound("maCtk.lessThan=" + UPDATED_MA_CTK);
    }

    @Test
    @Transactional
    public void getAllCountriesByMaCtkIsGreaterThanSomething() throws Exception {
        // Initialize the database
        countryRepository.saveAndFlush(country);

        // Get all the countryList where maCtk is greater than DEFAULT_MA_CTK
        defaultCountryShouldNotBeFound("maCtk.greaterThan=" + DEFAULT_MA_CTK);

        // Get all the countryList where maCtk is greater than SMALLER_MA_CTK
        defaultCountryShouldBeFound("maCtk.greaterThan=" + SMALLER_MA_CTK);
    }


    @Test
    @Transactional
    public void getAllCountriesByTenCtkIsEqualToSomething() throws Exception {
        // Initialize the database
        countryRepository.saveAndFlush(country);

        // Get all the countryList where tenCtk equals to DEFAULT_TEN_CTK
        defaultCountryShouldBeFound("tenCtk.equals=" + DEFAULT_TEN_CTK);

        // Get all the countryList where tenCtk equals to UPDATED_TEN_CTK
        defaultCountryShouldNotBeFound("tenCtk.equals=" + UPDATED_TEN_CTK);
    }

    @Test
    @Transactional
    public void getAllCountriesByTenCtkIsNotEqualToSomething() throws Exception {
        // Initialize the database
        countryRepository.saveAndFlush(country);

        // Get all the countryList where tenCtk not equals to DEFAULT_TEN_CTK
        defaultCountryShouldNotBeFound("tenCtk.notEquals=" + DEFAULT_TEN_CTK);

        // Get all the countryList where tenCtk not equals to UPDATED_TEN_CTK
        defaultCountryShouldBeFound("tenCtk.notEquals=" + UPDATED_TEN_CTK);
    }

    @Test
    @Transactional
    public void getAllCountriesByTenCtkIsInShouldWork() throws Exception {
        // Initialize the database
        countryRepository.saveAndFlush(country);

        // Get all the countryList where tenCtk in DEFAULT_TEN_CTK or UPDATED_TEN_CTK
        defaultCountryShouldBeFound("tenCtk.in=" + DEFAULT_TEN_CTK + "," + UPDATED_TEN_CTK);

        // Get all the countryList where tenCtk equals to UPDATED_TEN_CTK
        defaultCountryShouldNotBeFound("tenCtk.in=" + UPDATED_TEN_CTK);
    }

    @Test
    @Transactional
    public void getAllCountriesByTenCtkIsNullOrNotNull() throws Exception {
        // Initialize the database
        countryRepository.saveAndFlush(country);

        // Get all the countryList where tenCtk is not null
        defaultCountryShouldBeFound("tenCtk.specified=true");

        // Get all the countryList where tenCtk is null
        defaultCountryShouldNotBeFound("tenCtk.specified=false");
    }
                @Test
    @Transactional
    public void getAllCountriesByTenCtkContainsSomething() throws Exception {
        // Initialize the database
        countryRepository.saveAndFlush(country);

        // Get all the countryList where tenCtk contains DEFAULT_TEN_CTK
        defaultCountryShouldBeFound("tenCtk.contains=" + DEFAULT_TEN_CTK);

        // Get all the countryList where tenCtk contains UPDATED_TEN_CTK
        defaultCountryShouldNotBeFound("tenCtk.contains=" + UPDATED_TEN_CTK);
    }

    @Test
    @Transactional
    public void getAllCountriesByTenCtkNotContainsSomething() throws Exception {
        // Initialize the database
        countryRepository.saveAndFlush(country);

        // Get all the countryList where tenCtk does not contain DEFAULT_TEN_CTK
        defaultCountryShouldNotBeFound("tenCtk.doesNotContain=" + DEFAULT_TEN_CTK);

        // Get all the countryList where tenCtk does not contain UPDATED_TEN_CTK
        defaultCountryShouldBeFound("tenCtk.doesNotContain=" + UPDATED_TEN_CTK);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultCountryShouldBeFound(String filter) throws Exception {
        restCountryMockMvc.perform(get("/api/countries?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(country.getId().intValue())))
            .andExpect(jsonPath("$.[*].isoCode").value(hasItem(DEFAULT_ISO_CODE)))
            .andExpect(jsonPath("$.[*].isoNumeric").value(hasItem(DEFAULT_ISO_NUMERIC)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].maCtk").value(hasItem(DEFAULT_MA_CTK)))
            .andExpect(jsonPath("$.[*].tenCtk").value(hasItem(DEFAULT_TEN_CTK)));

        // Check, that the count call also returns 1
        restCountryMockMvc.perform(get("/api/countries/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultCountryShouldNotBeFound(String filter) throws Exception {
        restCountryMockMvc.perform(get("/api/countries?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restCountryMockMvc.perform(get("/api/countries/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingCountry() throws Exception {
        // Get the country
        restCountryMockMvc.perform(get("/api/countries/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCountry() throws Exception {
        // Initialize the database
        countryRepository.saveAndFlush(country);

        int databaseSizeBeforeUpdate = countryRepository.findAll().size();

        // Update the country
        Country updatedCountry = countryRepository.findById(country.getId()).get();
        // Disconnect from session so that the updates on updatedCountry are not directly saved in db
        em.detach(updatedCountry);
        updatedCountry
            .isoCode(UPDATED_ISO_CODE)
            .isoNumeric(UPDATED_ISO_NUMERIC)
            .name(UPDATED_NAME)
            .maCtk(UPDATED_MA_CTK)
            .tenCtk(UPDATED_TEN_CTK);
        CountryDTO countryDTO = countryMapper.toDto(updatedCountry);

        restCountryMockMvc.perform(put("/api/countries").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(countryDTO)))
            .andExpect(status().isOk());

        // Validate the Country in the database
        List<Country> countryList = countryRepository.findAll();
        assertThat(countryList).hasSize(databaseSizeBeforeUpdate);
        Country testCountry = countryList.get(countryList.size() - 1);
        assertThat(testCountry.getIsoCode()).isEqualTo(UPDATED_ISO_CODE);
        assertThat(testCountry.getIsoNumeric()).isEqualTo(UPDATED_ISO_NUMERIC);
        assertThat(testCountry.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCountry.getMaCtk()).isEqualTo(UPDATED_MA_CTK);
        assertThat(testCountry.getTenCtk()).isEqualTo(UPDATED_TEN_CTK);
    }

    @Test
    @Transactional
    public void updateNonExistingCountry() throws Exception {
        int databaseSizeBeforeUpdate = countryRepository.findAll().size();

        // Create the Country
        CountryDTO countryDTO = countryMapper.toDto(country);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCountryMockMvc.perform(put("/api/countries").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(countryDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Country in the database
        List<Country> countryList = countryRepository.findAll();
        assertThat(countryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCountry() throws Exception {
        // Initialize the database
        countryRepository.saveAndFlush(country);

        int databaseSizeBeforeDelete = countryRepository.findAll().size();

        // Delete the country
        restCountryMockMvc.perform(delete("/api/countries/{id}", country.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Country> countryList = countryRepository.findAll();
        assertThat(countryList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

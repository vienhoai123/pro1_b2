package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.ThongTinKhamBenh;
import vn.vnpt.domain.ThongTinKhoa;
import vn.vnpt.domain.NhanVien;
import vn.vnpt.domain.HuongDieuTri;
import vn.vnpt.domain.Phong;
import vn.vnpt.repository.ThongTinKhamBenhRepository;
import vn.vnpt.service.ThongTinKhamBenhService;
import vn.vnpt.service.dto.ThongTinKhamBenhDTO;
import vn.vnpt.service.mapper.ThongTinKhamBenhMapper;
import vn.vnpt.service.dto.ThongTinKhamBenhCriteria;
import vn.vnpt.service.ThongTinKhamBenhQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ThongTinKhamBenhResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })
@AutoConfigureMockMvc
@WithMockUser
public class ThongTinKhamBenhResourceIT {

    private static final BigDecimal DEFAULT_BMI = new BigDecimal(1);
    private static final BigDecimal UPDATED_BMI = new BigDecimal(2);
    private static final BigDecimal SMALLER_BMI = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_CAN_NANG = new BigDecimal(1);
    private static final BigDecimal UPDATED_CAN_NANG = new BigDecimal(2);
    private static final BigDecimal SMALLER_CAN_NANG = new BigDecimal(1 - 1);

    private static final Integer DEFAULT_CHIEU_CAO = 1;
    private static final Integer UPDATED_CHIEU_CAO = 2;
    private static final Integer SMALLER_CHIEU_CAO = 1 - 1;

    private static final BigDecimal DEFAULT_CREATININ = new BigDecimal(1);
    private static final BigDecimal UPDATED_CREATININ = new BigDecimal(2);
    private static final BigDecimal SMALLER_CREATININ = new BigDecimal(1 - 1);

    private static final String DEFAULT_DAU_HIEU_LAM_SANG = "AAAAAAAAAA";
    private static final String UPDATED_DAU_HIEU_LAM_SANG = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_DO_THANH_THAI = new BigDecimal(1);
    private static final BigDecimal UPDATED_DO_THANH_THAI = new BigDecimal(2);
    private static final BigDecimal SMALLER_DO_THANH_THAI = new BigDecimal(1 - 1);

    private static final Integer DEFAULT_HUYET_AP_CAO = 1;
    private static final Integer UPDATED_HUYET_AP_CAO = 2;
    private static final Integer SMALLER_HUYET_AP_CAO = 1 - 1;

    private static final Integer DEFAULT_HUYET_AP_THAP = 1;
    private static final Integer UPDATED_HUYET_AP_THAP = 2;
    private static final Integer SMALLER_HUYET_AP_THAP = 1 - 1;

    private static final String DEFAULT_ICD = "AAAAAAAAAA";
    private static final String UPDATED_ICD = "BBBBBBBBBB";

    private static final Integer DEFAULT_LAN_HEN = 1;
    private static final Integer UPDATED_LAN_HEN = 2;
    private static final Integer SMALLER_LAN_HEN = 1 - 1;

    private static final String DEFAULT_LOI_DAN = "AAAAAAAAAA";
    private static final String UPDATED_LOI_DAN = "BBBBBBBBBB";

    private static final String DEFAULT_LY_DO_CHUYEN = "AAAAAAAAAA";
    private static final String UPDATED_LY_DO_CHUYEN = "BBBBBBBBBB";

    private static final String DEFAULT_MA_BENH_YHCT = "AAAAAAAAAA";
    private static final String UPDATED_MA_BENH_YHCT = "BBBBBBBBBB";

    private static final Integer DEFAULT_MACH = 1;
    private static final Integer UPDATED_MACH = 2;
    private static final Integer SMALLER_MACH = 1 - 1;

    private static final Integer DEFAULT_MAX_NGAY_RA_TOA = 1;
    private static final Integer UPDATED_MAX_NGAY_RA_TOA = 2;
    private static final Integer SMALLER_MAX_NGAY_RA_TOA = 1 - 1;

    private static final LocalDate DEFAULT_NGAY_HEN = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_HEN = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_NGAY_HEN = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_NHAN_DINH_BMI = "AAAAAAAAAA";
    private static final String UPDATED_NHAN_DINH_BMI = "BBBBBBBBBB";

    private static final String DEFAULT_NHAN_DINH_DO_THANH_THAI = "AAAAAAAAAA";
    private static final String UPDATED_NHAN_DINH_DO_THANH_THAI = "BBBBBBBBBB";

    private static final Boolean DEFAULT_NHAP_VIEN = false;
    private static final Boolean UPDATED_NHAP_VIEN = true;

    private static final BigDecimal DEFAULT_NHIET_DO = new BigDecimal(1);
    private static final BigDecimal UPDATED_NHIET_DO = new BigDecimal(2);
    private static final BigDecimal SMALLER_NHIET_DO = new BigDecimal(1 - 1);

    private static final Integer DEFAULT_NHIP_THO = 1;
    private static final Integer UPDATED_NHIP_THO = 2;
    private static final Integer SMALLER_NHIP_THO = 1 - 1;

    private static final String DEFAULT_PP_DIEU_TRI_YTCT = "AAAAAAAAAA";
    private static final String UPDATED_PP_DIEU_TRI_YTCT = "BBBBBBBBBB";

    private static final Integer DEFAULT_SO_LAN_IN_BANG_KE = 1;
    private static final Integer UPDATED_SO_LAN_IN_BANG_KE = 2;
    private static final Integer SMALLER_SO_LAN_IN_BANG_KE = 1 - 1;

    private static final Integer DEFAULT_SO_LAN_IN_TOA_THUOC = 1;
    private static final Integer UPDATED_SO_LAN_IN_TOA_THUOC = 2;
    private static final Integer SMALLER_SO_LAN_IN_TOA_THUOC = 1 - 1;

    private static final String DEFAULT_TEN_BENH_ICD = "AAAAAAAAAA";
    private static final String UPDATED_TEN_BENH_ICD = "BBBBBBBBBB";

    private static final String DEFAULT_TEN_BENH_THEO_BAC_SI = "AAAAAAAAAA";
    private static final String UPDATED_TEN_BENH_THEO_BAC_SI = "BBBBBBBBBB";

    private static final String DEFAULT_TEN_BENH_YHCT = "AAAAAAAAAA";
    private static final String UPDATED_TEN_BENH_YHCT = "BBBBBBBBBB";

    private static final Boolean DEFAULT_TRANG_THAI_CDHA = false;
    private static final Boolean UPDATED_TRANG_THAI_CDHA = true;

    private static final Boolean DEFAULT_TRANG_THAI_TTPT = false;
    private static final Boolean UPDATED_TRANG_THAI_TTPT = true;

    private static final Boolean DEFAULT_TRANG_THAI_XET_NGHIEM = false;
    private static final Boolean UPDATED_TRANG_THAI_XET_NGHIEM = true;

    private static final String DEFAULT_TRIEU_CHUNG_LAM_SANG = "AAAAAAAAAA";
    private static final String UPDATED_TRIEU_CHUNG_LAM_SANG = "BBBBBBBBBB";

    private static final Integer DEFAULT_VONG_BUNG = 1;
    private static final Integer UPDATED_VONG_BUNG = 2;
    private static final Integer SMALLER_VONG_BUNG = 1 - 1;

    private static final Integer DEFAULT_TRANG_THAI = 1;
    private static final Integer UPDATED_TRANG_THAI = 2;
    private static final Integer SMALLER_TRANG_THAI = 1 - 1;

    private static final LocalDate DEFAULT_THOI_GIAN_KET_THUC_KHAM = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_THOI_GIAN_KET_THUC_KHAM = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_THOI_GIAN_KET_THUC_KHAM = LocalDate.ofEpochDay(-1L);

    private static final Long DEFAULT_PHONG_ID_CHUYEN_TU = 1L;
    private static final Long UPDATED_PHONG_ID_CHUYEN_TU = 2L;
    private static final Long SMALLER_PHONG_ID_CHUYEN_TU = 1L - 1L;

    private static final LocalDate DEFAULT_THOI_GIAN_KHAM_BENH = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_THOI_GIAN_KHAM_BENH = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_THOI_GIAN_KHAM_BENH = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_Y_LENH = "AAAAAAAAAA";
    private static final String UPDATED_Y_LENH = "BBBBBBBBBB";

    private static final Integer DEFAULT_LOAI_Y_LENH = 1;
    private static final Integer UPDATED_LOAI_Y_LENH = 2;
    private static final Integer SMALLER_LOAI_Y_LENH = 1 - 1;

    private static final String DEFAULT_CHAN_DOAN = "AAAAAAAAAA";
    private static final String UPDATED_CHAN_DOAN = "BBBBBBBBBB";

    private static final Integer DEFAULT_LOAI_CHAN_DOAN = 1;
    private static final Integer UPDATED_LOAI_CHAN_DOAN = 2;
    private static final Integer SMALLER_LOAI_CHAN_DOAN = 1 - 1;

    private static final Integer DEFAULT_NAM = 1;
    private static final Integer UPDATED_NAM = 2;
    private static final Integer SMALLER_NAM = 1 - 1;

    @Autowired
    private ThongTinKhamBenhRepository thongTinKhamBenhRepository;

    @Autowired
    private ThongTinKhamBenhMapper thongTinKhamBenhMapper;

    @Autowired
    private ThongTinKhamBenhService thongTinKhamBenhService;

    @Autowired
    private ThongTinKhamBenhQueryService thongTinKhamBenhQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restThongTinKhamBenhMockMvc;

    private ThongTinKhamBenh thongTinKhamBenh;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ThongTinKhamBenh createEntity(EntityManager em) {
        ThongTinKhamBenh thongTinKhamBenh = new ThongTinKhamBenh()
            .bmi(DEFAULT_BMI)
            .canNang(DEFAULT_CAN_NANG)
            .chieuCao(DEFAULT_CHIEU_CAO)
            .creatinin(DEFAULT_CREATININ)
            .dauHieuLamSang(DEFAULT_DAU_HIEU_LAM_SANG)
            .doThanhThai(DEFAULT_DO_THANH_THAI)
            .huyetApCao(DEFAULT_HUYET_AP_CAO)
            .huyetApThap(DEFAULT_HUYET_AP_THAP)
            .icd(DEFAULT_ICD)
            .lanHen(DEFAULT_LAN_HEN)
            .loiDan(DEFAULT_LOI_DAN)
            .lyDoChuyen(DEFAULT_LY_DO_CHUYEN)
            .maBenhYhct(DEFAULT_MA_BENH_YHCT)
            .mach(DEFAULT_MACH)
            .maxNgayRaToa(DEFAULT_MAX_NGAY_RA_TOA)
            .ngayHen(DEFAULT_NGAY_HEN)
            .nhanDinhBmi(DEFAULT_NHAN_DINH_BMI)
            .nhanDinhDoThanhThai(DEFAULT_NHAN_DINH_DO_THANH_THAI)
            .nhapVien(DEFAULT_NHAP_VIEN)
            .nhietDo(DEFAULT_NHIET_DO)
            .nhipTho(DEFAULT_NHIP_THO)
            .ppDieuTriYtct(DEFAULT_PP_DIEU_TRI_YTCT)
            .soLanInBangKe(DEFAULT_SO_LAN_IN_BANG_KE)
            .soLanInToaThuoc(DEFAULT_SO_LAN_IN_TOA_THUOC)
            .tenBenhIcd(DEFAULT_TEN_BENH_ICD)
            .tenBenhTheoBacSi(DEFAULT_TEN_BENH_THEO_BAC_SI)
            .tenBenhYhct(DEFAULT_TEN_BENH_YHCT)
            .trangThaiCdha(DEFAULT_TRANG_THAI_CDHA)
            .trangThaiTtpt(DEFAULT_TRANG_THAI_TTPT)
            .trangThaiXetNghiem(DEFAULT_TRANG_THAI_XET_NGHIEM)
            .trieuChungLamSang(DEFAULT_TRIEU_CHUNG_LAM_SANG)
            .vongBung(DEFAULT_VONG_BUNG)
            .trangThai(DEFAULT_TRANG_THAI)
            .thoiGianKetThucKham(DEFAULT_THOI_GIAN_KET_THUC_KHAM)
            .phongIdChuyenTu(DEFAULT_PHONG_ID_CHUYEN_TU)
            .thoiGianKhamBenh(DEFAULT_THOI_GIAN_KHAM_BENH)
            .yLenh(DEFAULT_Y_LENH)
            .loaiYLenh(DEFAULT_LOAI_Y_LENH)
            .chanDoan(DEFAULT_CHAN_DOAN)
            .loaiChanDoan(DEFAULT_LOAI_CHAN_DOAN)
            .nam(DEFAULT_NAM);
        // Add required entity
        ThongTinKhoa thongTinKhoa;
        if (TestUtil.findAll(em, ThongTinKhoa.class).isEmpty()) {
            thongTinKhoa = ThongTinKhoaResourceIT.createEntity(em);
            em.persist(thongTinKhoa);
            em.flush();
        } else {
            thongTinKhoa = TestUtil.findAll(em, ThongTinKhoa.class).get(0);
        }
        thongTinKhamBenh.setBakb(thongTinKhoa);
        // Add required entity
        thongTinKhamBenh.setDonVi(thongTinKhoa);
        // Add required entity
        thongTinKhamBenh.setBenhNhan(thongTinKhoa);
        // Add required entity
        thongTinKhamBenh.setDotDieuTri(thongTinKhoa);
        // Add required entity
        thongTinKhamBenh.setThongTinKhoa(thongTinKhoa);
        // Add required entity
        Phong phong;
        if (TestUtil.findAll(em, Phong.class).isEmpty()) {
            phong = PhongResourceIT.createEntity(em);
            em.persist(phong);
            em.flush();
        } else {
            phong = TestUtil.findAll(em, Phong.class).get(0);
        }
        thongTinKhamBenh.setPhong(phong);
        return thongTinKhamBenh;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ThongTinKhamBenh createUpdatedEntity(EntityManager em) {
        ThongTinKhamBenh thongTinKhamBenh = new ThongTinKhamBenh()
            .bmi(UPDATED_BMI)
            .canNang(UPDATED_CAN_NANG)
            .chieuCao(UPDATED_CHIEU_CAO)
            .creatinin(UPDATED_CREATININ)
            .dauHieuLamSang(UPDATED_DAU_HIEU_LAM_SANG)
            .doThanhThai(UPDATED_DO_THANH_THAI)
            .huyetApCao(UPDATED_HUYET_AP_CAO)
            .huyetApThap(UPDATED_HUYET_AP_THAP)
            .icd(UPDATED_ICD)
            .lanHen(UPDATED_LAN_HEN)
            .loiDan(UPDATED_LOI_DAN)
            .lyDoChuyen(UPDATED_LY_DO_CHUYEN)
            .maBenhYhct(UPDATED_MA_BENH_YHCT)
            .mach(UPDATED_MACH)
            .maxNgayRaToa(UPDATED_MAX_NGAY_RA_TOA)
            .ngayHen(UPDATED_NGAY_HEN)
            .nhanDinhBmi(UPDATED_NHAN_DINH_BMI)
            .nhanDinhDoThanhThai(UPDATED_NHAN_DINH_DO_THANH_THAI)
            .nhapVien(UPDATED_NHAP_VIEN)
            .nhietDo(UPDATED_NHIET_DO)
            .nhipTho(UPDATED_NHIP_THO)
            .ppDieuTriYtct(UPDATED_PP_DIEU_TRI_YTCT)
            .soLanInBangKe(UPDATED_SO_LAN_IN_BANG_KE)
            .soLanInToaThuoc(UPDATED_SO_LAN_IN_TOA_THUOC)
            .tenBenhIcd(UPDATED_TEN_BENH_ICD)
            .tenBenhTheoBacSi(UPDATED_TEN_BENH_THEO_BAC_SI)
            .tenBenhYhct(UPDATED_TEN_BENH_YHCT)
            .trangThaiCdha(UPDATED_TRANG_THAI_CDHA)
            .trangThaiTtpt(UPDATED_TRANG_THAI_TTPT)
            .trangThaiXetNghiem(UPDATED_TRANG_THAI_XET_NGHIEM)
            .trieuChungLamSang(UPDATED_TRIEU_CHUNG_LAM_SANG)
            .vongBung(UPDATED_VONG_BUNG)
            .trangThai(UPDATED_TRANG_THAI)
            .thoiGianKetThucKham(UPDATED_THOI_GIAN_KET_THUC_KHAM)
            .phongIdChuyenTu(UPDATED_PHONG_ID_CHUYEN_TU)
            .thoiGianKhamBenh(UPDATED_THOI_GIAN_KHAM_BENH)
            .yLenh(UPDATED_Y_LENH)
            .loaiYLenh(UPDATED_LOAI_Y_LENH)
            .chanDoan(UPDATED_CHAN_DOAN)
            .loaiChanDoan(UPDATED_LOAI_CHAN_DOAN)
            .nam(UPDATED_NAM);
        // Add required entity
        ThongTinKhoa thongTinKhoa;
        if (TestUtil.findAll(em, ThongTinKhoa.class).isEmpty()) {
            thongTinKhoa = ThongTinKhoaResourceIT.createUpdatedEntity(em);
            em.persist(thongTinKhoa);
            em.flush();
        } else {
            thongTinKhoa = TestUtil.findAll(em, ThongTinKhoa.class).get(0);
        }
        thongTinKhamBenh.setBakb(thongTinKhoa);
        // Add required entity
        thongTinKhamBenh.setDonVi(thongTinKhoa);
        // Add required entity
        thongTinKhamBenh.setBenhNhan(thongTinKhoa);
        // Add required entity
        thongTinKhamBenh.setDotDieuTri(thongTinKhoa);
        // Add required entity
        thongTinKhamBenh.setThongTinKhoa(thongTinKhoa);
        // Add required entity
        Phong phong;
        if (TestUtil.findAll(em, Phong.class).isEmpty()) {
            phong = PhongResourceIT.createUpdatedEntity(em);
            em.persist(phong);
            em.flush();
        } else {
            phong = TestUtil.findAll(em, Phong.class).get(0);
        }
        thongTinKhamBenh.setPhong(phong);
        return thongTinKhamBenh;
    }

    @BeforeEach
    public void initTest() {
        thongTinKhamBenh = createEntity(em);
    }

    @Test
    @Transactional
    public void createThongTinKhamBenh() throws Exception {
        int databaseSizeBeforeCreate = thongTinKhamBenhRepository.findAll().size();
        // Create the ThongTinKhamBenh
        ThongTinKhamBenhDTO thongTinKhamBenhDTO = thongTinKhamBenhMapper.toDto(thongTinKhamBenh);
        restThongTinKhamBenhMockMvc.perform(post("/api/thong-tin-kham-benhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(thongTinKhamBenhDTO)))
            .andExpect(status().isCreated());

        // Validate the ThongTinKhamBenh in the database
        List<ThongTinKhamBenh> thongTinKhamBenhList = thongTinKhamBenhRepository.findAll();
        assertThat(thongTinKhamBenhList).hasSize(databaseSizeBeforeCreate + 1);
        ThongTinKhamBenh testThongTinKhamBenh = thongTinKhamBenhList.get(thongTinKhamBenhList.size() - 1);
        assertThat(testThongTinKhamBenh.getBmi()).isEqualTo(DEFAULT_BMI);
        assertThat(testThongTinKhamBenh.getCanNang()).isEqualTo(DEFAULT_CAN_NANG);
        assertThat(testThongTinKhamBenh.getChieuCao()).isEqualTo(DEFAULT_CHIEU_CAO);
        assertThat(testThongTinKhamBenh.getCreatinin()).isEqualTo(DEFAULT_CREATININ);
        assertThat(testThongTinKhamBenh.getDauHieuLamSang()).isEqualTo(DEFAULT_DAU_HIEU_LAM_SANG);
        assertThat(testThongTinKhamBenh.getDoThanhThai()).isEqualTo(DEFAULT_DO_THANH_THAI);
        assertThat(testThongTinKhamBenh.getHuyetApCao()).isEqualTo(DEFAULT_HUYET_AP_CAO);
        assertThat(testThongTinKhamBenh.getHuyetApThap()).isEqualTo(DEFAULT_HUYET_AP_THAP);
        assertThat(testThongTinKhamBenh.getIcd()).isEqualTo(DEFAULT_ICD);
        assertThat(testThongTinKhamBenh.getLanHen()).isEqualTo(DEFAULT_LAN_HEN);
        assertThat(testThongTinKhamBenh.getLoiDan()).isEqualTo(DEFAULT_LOI_DAN);
        assertThat(testThongTinKhamBenh.getLyDoChuyen()).isEqualTo(DEFAULT_LY_DO_CHUYEN);
        assertThat(testThongTinKhamBenh.getMaBenhYhct()).isEqualTo(DEFAULT_MA_BENH_YHCT);
        assertThat(testThongTinKhamBenh.getMach()).isEqualTo(DEFAULT_MACH);
        assertThat(testThongTinKhamBenh.getMaxNgayRaToa()).isEqualTo(DEFAULT_MAX_NGAY_RA_TOA);
        assertThat(testThongTinKhamBenh.getNgayHen()).isEqualTo(DEFAULT_NGAY_HEN);
        assertThat(testThongTinKhamBenh.getNhanDinhBmi()).isEqualTo(DEFAULT_NHAN_DINH_BMI);
        assertThat(testThongTinKhamBenh.getNhanDinhDoThanhThai()).isEqualTo(DEFAULT_NHAN_DINH_DO_THANH_THAI);
        assertThat(testThongTinKhamBenh.isNhapVien()).isEqualTo(DEFAULT_NHAP_VIEN);
        assertThat(testThongTinKhamBenh.getNhietDo()).isEqualTo(DEFAULT_NHIET_DO);
        assertThat(testThongTinKhamBenh.getNhipTho()).isEqualTo(DEFAULT_NHIP_THO);
        assertThat(testThongTinKhamBenh.getPpDieuTriYtct()).isEqualTo(DEFAULT_PP_DIEU_TRI_YTCT);
        assertThat(testThongTinKhamBenh.getSoLanInBangKe()).isEqualTo(DEFAULT_SO_LAN_IN_BANG_KE);
        assertThat(testThongTinKhamBenh.getSoLanInToaThuoc()).isEqualTo(DEFAULT_SO_LAN_IN_TOA_THUOC);
        assertThat(testThongTinKhamBenh.getTenBenhIcd()).isEqualTo(DEFAULT_TEN_BENH_ICD);
        assertThat(testThongTinKhamBenh.getTenBenhTheoBacSi()).isEqualTo(DEFAULT_TEN_BENH_THEO_BAC_SI);
        assertThat(testThongTinKhamBenh.getTenBenhYhct()).isEqualTo(DEFAULT_TEN_BENH_YHCT);
        assertThat(testThongTinKhamBenh.isTrangThaiCdha()).isEqualTo(DEFAULT_TRANG_THAI_CDHA);
        assertThat(testThongTinKhamBenh.isTrangThaiTtpt()).isEqualTo(DEFAULT_TRANG_THAI_TTPT);
        assertThat(testThongTinKhamBenh.isTrangThaiXetNghiem()).isEqualTo(DEFAULT_TRANG_THAI_XET_NGHIEM);
        assertThat(testThongTinKhamBenh.getTrieuChungLamSang()).isEqualTo(DEFAULT_TRIEU_CHUNG_LAM_SANG);
        assertThat(testThongTinKhamBenh.getVongBung()).isEqualTo(DEFAULT_VONG_BUNG);
        assertThat(testThongTinKhamBenh.getTrangThai()).isEqualTo(DEFAULT_TRANG_THAI);
        assertThat(testThongTinKhamBenh.getThoiGianKetThucKham()).isEqualTo(DEFAULT_THOI_GIAN_KET_THUC_KHAM);
        assertThat(testThongTinKhamBenh.getPhongIdChuyenTu()).isEqualTo(DEFAULT_PHONG_ID_CHUYEN_TU);
        assertThat(testThongTinKhamBenh.getThoiGianKhamBenh()).isEqualTo(DEFAULT_THOI_GIAN_KHAM_BENH);
        assertThat(testThongTinKhamBenh.getyLenh()).isEqualTo(DEFAULT_Y_LENH);
        assertThat(testThongTinKhamBenh.getLoaiYLenh()).isEqualTo(DEFAULT_LOAI_Y_LENH);
        assertThat(testThongTinKhamBenh.getChanDoan()).isEqualTo(DEFAULT_CHAN_DOAN);
        assertThat(testThongTinKhamBenh.getLoaiChanDoan()).isEqualTo(DEFAULT_LOAI_CHAN_DOAN);
        assertThat(testThongTinKhamBenh.getNam()).isEqualTo(DEFAULT_NAM);
    }

    @Test
    @Transactional
    public void createThongTinKhamBenhWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = thongTinKhamBenhRepository.findAll().size();

        // Create the ThongTinKhamBenh with an existing ID
        thongTinKhamBenh.setId(1L);
        ThongTinKhamBenhDTO thongTinKhamBenhDTO = thongTinKhamBenhMapper.toDto(thongTinKhamBenh);

        // An entity with an existing ID cannot be created, so this API call must fail
        restThongTinKhamBenhMockMvc.perform(post("/api/thong-tin-kham-benhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(thongTinKhamBenhDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ThongTinKhamBenh in the database
        List<ThongTinKhamBenh> thongTinKhamBenhList = thongTinKhamBenhRepository.findAll();
        assertThat(thongTinKhamBenhList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNhapVienIsRequired() throws Exception {
        int databaseSizeBeforeTest = thongTinKhamBenhRepository.findAll().size();
        // set the field null
        thongTinKhamBenh.setNhapVien(null);

        // Create the ThongTinKhamBenh, which fails.
        ThongTinKhamBenhDTO thongTinKhamBenhDTO = thongTinKhamBenhMapper.toDto(thongTinKhamBenh);


        restThongTinKhamBenhMockMvc.perform(post("/api/thong-tin-kham-benhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(thongTinKhamBenhDTO)))
            .andExpect(status().isBadRequest());

        List<ThongTinKhamBenh> thongTinKhamBenhList = thongTinKhamBenhRepository.findAll();
        assertThat(thongTinKhamBenhList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTrangThaiCdhaIsRequired() throws Exception {
        int databaseSizeBeforeTest = thongTinKhamBenhRepository.findAll().size();
        // set the field null
        thongTinKhamBenh.setTrangThaiCdha(null);

        // Create the ThongTinKhamBenh, which fails.
        ThongTinKhamBenhDTO thongTinKhamBenhDTO = thongTinKhamBenhMapper.toDto(thongTinKhamBenh);


        restThongTinKhamBenhMockMvc.perform(post("/api/thong-tin-kham-benhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(thongTinKhamBenhDTO)))
            .andExpect(status().isBadRequest());

        List<ThongTinKhamBenh> thongTinKhamBenhList = thongTinKhamBenhRepository.findAll();
        assertThat(thongTinKhamBenhList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTrangThaiTtptIsRequired() throws Exception {
        int databaseSizeBeforeTest = thongTinKhamBenhRepository.findAll().size();
        // set the field null
        thongTinKhamBenh.setTrangThaiTtpt(null);

        // Create the ThongTinKhamBenh, which fails.
        ThongTinKhamBenhDTO thongTinKhamBenhDTO = thongTinKhamBenhMapper.toDto(thongTinKhamBenh);


        restThongTinKhamBenhMockMvc.perform(post("/api/thong-tin-kham-benhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(thongTinKhamBenhDTO)))
            .andExpect(status().isBadRequest());

        List<ThongTinKhamBenh> thongTinKhamBenhList = thongTinKhamBenhRepository.findAll();
        assertThat(thongTinKhamBenhList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTrangThaiXetNghiemIsRequired() throws Exception {
        int databaseSizeBeforeTest = thongTinKhamBenhRepository.findAll().size();
        // set the field null
        thongTinKhamBenh.setTrangThaiXetNghiem(null);

        // Create the ThongTinKhamBenh, which fails.
        ThongTinKhamBenhDTO thongTinKhamBenhDTO = thongTinKhamBenhMapper.toDto(thongTinKhamBenh);


        restThongTinKhamBenhMockMvc.perform(post("/api/thong-tin-kham-benhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(thongTinKhamBenhDTO)))
            .andExpect(status().isBadRequest());

        List<ThongTinKhamBenh> thongTinKhamBenhList = thongTinKhamBenhRepository.findAll();
        assertThat(thongTinKhamBenhList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNamIsRequired() throws Exception {
        int databaseSizeBeforeTest = thongTinKhamBenhRepository.findAll().size();
        // set the field null
        thongTinKhamBenh.setNam(null);

        // Create the ThongTinKhamBenh, which fails.
        ThongTinKhamBenhDTO thongTinKhamBenhDTO = thongTinKhamBenhMapper.toDto(thongTinKhamBenh);


        restThongTinKhamBenhMockMvc.perform(post("/api/thong-tin-kham-benhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(thongTinKhamBenhDTO)))
            .andExpect(status().isBadRequest());

        List<ThongTinKhamBenh> thongTinKhamBenhList = thongTinKhamBenhRepository.findAll();
        assertThat(thongTinKhamBenhList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhs() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList
        restThongTinKhamBenhMockMvc.perform(get("/api/thong-tin-kham-benhs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(thongTinKhamBenh.getId().intValue())))
            .andExpect(jsonPath("$.[*].bmi").value(hasItem(DEFAULT_BMI.intValue())))
            .andExpect(jsonPath("$.[*].canNang").value(hasItem(DEFAULT_CAN_NANG.intValue())))
            .andExpect(jsonPath("$.[*].chieuCao").value(hasItem(DEFAULT_CHIEU_CAO)))
            .andExpect(jsonPath("$.[*].creatinin").value(hasItem(DEFAULT_CREATININ.intValue())))
            .andExpect(jsonPath("$.[*].dauHieuLamSang").value(hasItem(DEFAULT_DAU_HIEU_LAM_SANG)))
            .andExpect(jsonPath("$.[*].doThanhThai").value(hasItem(DEFAULT_DO_THANH_THAI.intValue())))
            .andExpect(jsonPath("$.[*].huyetApCao").value(hasItem(DEFAULT_HUYET_AP_CAO)))
            .andExpect(jsonPath("$.[*].huyetApThap").value(hasItem(DEFAULT_HUYET_AP_THAP)))
            .andExpect(jsonPath("$.[*].icd").value(hasItem(DEFAULT_ICD)))
            .andExpect(jsonPath("$.[*].lanHen").value(hasItem(DEFAULT_LAN_HEN)))
            .andExpect(jsonPath("$.[*].loiDan").value(hasItem(DEFAULT_LOI_DAN)))
            .andExpect(jsonPath("$.[*].lyDoChuyen").value(hasItem(DEFAULT_LY_DO_CHUYEN)))
            .andExpect(jsonPath("$.[*].maBenhYhct").value(hasItem(DEFAULT_MA_BENH_YHCT)))
            .andExpect(jsonPath("$.[*].mach").value(hasItem(DEFAULT_MACH)))
            .andExpect(jsonPath("$.[*].maxNgayRaToa").value(hasItem(DEFAULT_MAX_NGAY_RA_TOA)))
            .andExpect(jsonPath("$.[*].ngayHen").value(hasItem(DEFAULT_NGAY_HEN.toString())))
            .andExpect(jsonPath("$.[*].nhanDinhBmi").value(hasItem(DEFAULT_NHAN_DINH_BMI)))
            .andExpect(jsonPath("$.[*].nhanDinhDoThanhThai").value(hasItem(DEFAULT_NHAN_DINH_DO_THANH_THAI)))
            .andExpect(jsonPath("$.[*].nhapVien").value(hasItem(DEFAULT_NHAP_VIEN.booleanValue())))
            .andExpect(jsonPath("$.[*].nhietDo").value(hasItem(DEFAULT_NHIET_DO.intValue())))
            .andExpect(jsonPath("$.[*].nhipTho").value(hasItem(DEFAULT_NHIP_THO)))
            .andExpect(jsonPath("$.[*].ppDieuTriYtct").value(hasItem(DEFAULT_PP_DIEU_TRI_YTCT)))
            .andExpect(jsonPath("$.[*].soLanInBangKe").value(hasItem(DEFAULT_SO_LAN_IN_BANG_KE)))
            .andExpect(jsonPath("$.[*].soLanInToaThuoc").value(hasItem(DEFAULT_SO_LAN_IN_TOA_THUOC)))
            .andExpect(jsonPath("$.[*].tenBenhIcd").value(hasItem(DEFAULT_TEN_BENH_ICD)))
            .andExpect(jsonPath("$.[*].tenBenhTheoBacSi").value(hasItem(DEFAULT_TEN_BENH_THEO_BAC_SI)))
            .andExpect(jsonPath("$.[*].tenBenhYhct").value(hasItem(DEFAULT_TEN_BENH_YHCT)))
            .andExpect(jsonPath("$.[*].trangThaiCdha").value(hasItem(DEFAULT_TRANG_THAI_CDHA.booleanValue())))
            .andExpect(jsonPath("$.[*].trangThaiTtpt").value(hasItem(DEFAULT_TRANG_THAI_TTPT.booleanValue())))
            .andExpect(jsonPath("$.[*].trangThaiXetNghiem").value(hasItem(DEFAULT_TRANG_THAI_XET_NGHIEM.booleanValue())))
            .andExpect(jsonPath("$.[*].trieuChungLamSang").value(hasItem(DEFAULT_TRIEU_CHUNG_LAM_SANG)))
            .andExpect(jsonPath("$.[*].vongBung").value(hasItem(DEFAULT_VONG_BUNG)))
            .andExpect(jsonPath("$.[*].trangThai").value(hasItem(DEFAULT_TRANG_THAI)))
            .andExpect(jsonPath("$.[*].thoiGianKetThucKham").value(hasItem(DEFAULT_THOI_GIAN_KET_THUC_KHAM.toString())))
            .andExpect(jsonPath("$.[*].phongIdChuyenTu").value(hasItem(DEFAULT_PHONG_ID_CHUYEN_TU.intValue())))
            .andExpect(jsonPath("$.[*].thoiGianKhamBenh").value(hasItem(DEFAULT_THOI_GIAN_KHAM_BENH.toString())))
            .andExpect(jsonPath("$.[*].yLenh").value(hasItem(DEFAULT_Y_LENH)))
            .andExpect(jsonPath("$.[*].loaiYLenh").value(hasItem(DEFAULT_LOAI_Y_LENH)))
            .andExpect(jsonPath("$.[*].chanDoan").value(hasItem(DEFAULT_CHAN_DOAN)))
            .andExpect(jsonPath("$.[*].loaiChanDoan").value(hasItem(DEFAULT_LOAI_CHAN_DOAN)))
            .andExpect(jsonPath("$.[*].nam").value(hasItem(DEFAULT_NAM)));
    }
    
    @Test
    @Transactional
    public void getThongTinKhamBenh() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get the thongTinKhamBenh
        restThongTinKhamBenhMockMvc.perform(get("/api/thong-tin-kham-benhs/{id}", thongTinKhamBenh.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(thongTinKhamBenh.getId().intValue()))
            .andExpect(jsonPath("$.bmi").value(DEFAULT_BMI.intValue()))
            .andExpect(jsonPath("$.canNang").value(DEFAULT_CAN_NANG.intValue()))
            .andExpect(jsonPath("$.chieuCao").value(DEFAULT_CHIEU_CAO))
            .andExpect(jsonPath("$.creatinin").value(DEFAULT_CREATININ.intValue()))
            .andExpect(jsonPath("$.dauHieuLamSang").value(DEFAULT_DAU_HIEU_LAM_SANG))
            .andExpect(jsonPath("$.doThanhThai").value(DEFAULT_DO_THANH_THAI.intValue()))
            .andExpect(jsonPath("$.huyetApCao").value(DEFAULT_HUYET_AP_CAO))
            .andExpect(jsonPath("$.huyetApThap").value(DEFAULT_HUYET_AP_THAP))
            .andExpect(jsonPath("$.icd").value(DEFAULT_ICD))
            .andExpect(jsonPath("$.lanHen").value(DEFAULT_LAN_HEN))
            .andExpect(jsonPath("$.loiDan").value(DEFAULT_LOI_DAN))
            .andExpect(jsonPath("$.lyDoChuyen").value(DEFAULT_LY_DO_CHUYEN))
            .andExpect(jsonPath("$.maBenhYhct").value(DEFAULT_MA_BENH_YHCT))
            .andExpect(jsonPath("$.mach").value(DEFAULT_MACH))
            .andExpect(jsonPath("$.maxNgayRaToa").value(DEFAULT_MAX_NGAY_RA_TOA))
            .andExpect(jsonPath("$.ngayHen").value(DEFAULT_NGAY_HEN.toString()))
            .andExpect(jsonPath("$.nhanDinhBmi").value(DEFAULT_NHAN_DINH_BMI))
            .andExpect(jsonPath("$.nhanDinhDoThanhThai").value(DEFAULT_NHAN_DINH_DO_THANH_THAI))
            .andExpect(jsonPath("$.nhapVien").value(DEFAULT_NHAP_VIEN.booleanValue()))
            .andExpect(jsonPath("$.nhietDo").value(DEFAULT_NHIET_DO.intValue()))
            .andExpect(jsonPath("$.nhipTho").value(DEFAULT_NHIP_THO))
            .andExpect(jsonPath("$.ppDieuTriYtct").value(DEFAULT_PP_DIEU_TRI_YTCT))
            .andExpect(jsonPath("$.soLanInBangKe").value(DEFAULT_SO_LAN_IN_BANG_KE))
            .andExpect(jsonPath("$.soLanInToaThuoc").value(DEFAULT_SO_LAN_IN_TOA_THUOC))
            .andExpect(jsonPath("$.tenBenhIcd").value(DEFAULT_TEN_BENH_ICD))
            .andExpect(jsonPath("$.tenBenhTheoBacSi").value(DEFAULT_TEN_BENH_THEO_BAC_SI))
            .andExpect(jsonPath("$.tenBenhYhct").value(DEFAULT_TEN_BENH_YHCT))
            .andExpect(jsonPath("$.trangThaiCdha").value(DEFAULT_TRANG_THAI_CDHA.booleanValue()))
            .andExpect(jsonPath("$.trangThaiTtpt").value(DEFAULT_TRANG_THAI_TTPT.booleanValue()))
            .andExpect(jsonPath("$.trangThaiXetNghiem").value(DEFAULT_TRANG_THAI_XET_NGHIEM.booleanValue()))
            .andExpect(jsonPath("$.trieuChungLamSang").value(DEFAULT_TRIEU_CHUNG_LAM_SANG))
            .andExpect(jsonPath("$.vongBung").value(DEFAULT_VONG_BUNG))
            .andExpect(jsonPath("$.trangThai").value(DEFAULT_TRANG_THAI))
            .andExpect(jsonPath("$.thoiGianKetThucKham").value(DEFAULT_THOI_GIAN_KET_THUC_KHAM.toString()))
            .andExpect(jsonPath("$.phongIdChuyenTu").value(DEFAULT_PHONG_ID_CHUYEN_TU.intValue()))
            .andExpect(jsonPath("$.thoiGianKhamBenh").value(DEFAULT_THOI_GIAN_KHAM_BENH.toString()))
            .andExpect(jsonPath("$.yLenh").value(DEFAULT_Y_LENH))
            .andExpect(jsonPath("$.loaiYLenh").value(DEFAULT_LOAI_Y_LENH))
            .andExpect(jsonPath("$.chanDoan").value(DEFAULT_CHAN_DOAN))
            .andExpect(jsonPath("$.loaiChanDoan").value(DEFAULT_LOAI_CHAN_DOAN))
            .andExpect(jsonPath("$.nam").value(DEFAULT_NAM));
    }


    @Test
    @Transactional
    public void getThongTinKhamBenhsByIdFiltering() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        Long id = thongTinKhamBenh.getId();

        defaultThongTinKhamBenhShouldBeFound("id.equals=" + id);
        defaultThongTinKhamBenhShouldNotBeFound("id.notEquals=" + id);

        defaultThongTinKhamBenhShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultThongTinKhamBenhShouldNotBeFound("id.greaterThan=" + id);

        defaultThongTinKhamBenhShouldBeFound("id.lessThanOrEqual=" + id);
        defaultThongTinKhamBenhShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByBmiIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where bmi equals to DEFAULT_BMI
        defaultThongTinKhamBenhShouldBeFound("bmi.equals=" + DEFAULT_BMI);

        // Get all the thongTinKhamBenhList where bmi equals to UPDATED_BMI
        defaultThongTinKhamBenhShouldNotBeFound("bmi.equals=" + UPDATED_BMI);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByBmiIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where bmi not equals to DEFAULT_BMI
        defaultThongTinKhamBenhShouldNotBeFound("bmi.notEquals=" + DEFAULT_BMI);

        // Get all the thongTinKhamBenhList where bmi not equals to UPDATED_BMI
        defaultThongTinKhamBenhShouldBeFound("bmi.notEquals=" + UPDATED_BMI);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByBmiIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where bmi in DEFAULT_BMI or UPDATED_BMI
        defaultThongTinKhamBenhShouldBeFound("bmi.in=" + DEFAULT_BMI + "," + UPDATED_BMI);

        // Get all the thongTinKhamBenhList where bmi equals to UPDATED_BMI
        defaultThongTinKhamBenhShouldNotBeFound("bmi.in=" + UPDATED_BMI);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByBmiIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where bmi is not null
        defaultThongTinKhamBenhShouldBeFound("bmi.specified=true");

        // Get all the thongTinKhamBenhList where bmi is null
        defaultThongTinKhamBenhShouldNotBeFound("bmi.specified=false");
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByBmiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where bmi is greater than or equal to DEFAULT_BMI
        defaultThongTinKhamBenhShouldBeFound("bmi.greaterThanOrEqual=" + DEFAULT_BMI);

        // Get all the thongTinKhamBenhList where bmi is greater than or equal to UPDATED_BMI
        defaultThongTinKhamBenhShouldNotBeFound("bmi.greaterThanOrEqual=" + UPDATED_BMI);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByBmiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where bmi is less than or equal to DEFAULT_BMI
        defaultThongTinKhamBenhShouldBeFound("bmi.lessThanOrEqual=" + DEFAULT_BMI);

        // Get all the thongTinKhamBenhList where bmi is less than or equal to SMALLER_BMI
        defaultThongTinKhamBenhShouldNotBeFound("bmi.lessThanOrEqual=" + SMALLER_BMI);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByBmiIsLessThanSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where bmi is less than DEFAULT_BMI
        defaultThongTinKhamBenhShouldNotBeFound("bmi.lessThan=" + DEFAULT_BMI);

        // Get all the thongTinKhamBenhList where bmi is less than UPDATED_BMI
        defaultThongTinKhamBenhShouldBeFound("bmi.lessThan=" + UPDATED_BMI);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByBmiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where bmi is greater than DEFAULT_BMI
        defaultThongTinKhamBenhShouldNotBeFound("bmi.greaterThan=" + DEFAULT_BMI);

        // Get all the thongTinKhamBenhList where bmi is greater than SMALLER_BMI
        defaultThongTinKhamBenhShouldBeFound("bmi.greaterThan=" + SMALLER_BMI);
    }


    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByCanNangIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where canNang equals to DEFAULT_CAN_NANG
        defaultThongTinKhamBenhShouldBeFound("canNang.equals=" + DEFAULT_CAN_NANG);

        // Get all the thongTinKhamBenhList where canNang equals to UPDATED_CAN_NANG
        defaultThongTinKhamBenhShouldNotBeFound("canNang.equals=" + UPDATED_CAN_NANG);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByCanNangIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where canNang not equals to DEFAULT_CAN_NANG
        defaultThongTinKhamBenhShouldNotBeFound("canNang.notEquals=" + DEFAULT_CAN_NANG);

        // Get all the thongTinKhamBenhList where canNang not equals to UPDATED_CAN_NANG
        defaultThongTinKhamBenhShouldBeFound("canNang.notEquals=" + UPDATED_CAN_NANG);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByCanNangIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where canNang in DEFAULT_CAN_NANG or UPDATED_CAN_NANG
        defaultThongTinKhamBenhShouldBeFound("canNang.in=" + DEFAULT_CAN_NANG + "," + UPDATED_CAN_NANG);

        // Get all the thongTinKhamBenhList where canNang equals to UPDATED_CAN_NANG
        defaultThongTinKhamBenhShouldNotBeFound("canNang.in=" + UPDATED_CAN_NANG);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByCanNangIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where canNang is not null
        defaultThongTinKhamBenhShouldBeFound("canNang.specified=true");

        // Get all the thongTinKhamBenhList where canNang is null
        defaultThongTinKhamBenhShouldNotBeFound("canNang.specified=false");
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByCanNangIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where canNang is greater than or equal to DEFAULT_CAN_NANG
        defaultThongTinKhamBenhShouldBeFound("canNang.greaterThanOrEqual=" + DEFAULT_CAN_NANG);

        // Get all the thongTinKhamBenhList where canNang is greater than or equal to UPDATED_CAN_NANG
        defaultThongTinKhamBenhShouldNotBeFound("canNang.greaterThanOrEqual=" + UPDATED_CAN_NANG);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByCanNangIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where canNang is less than or equal to DEFAULT_CAN_NANG
        defaultThongTinKhamBenhShouldBeFound("canNang.lessThanOrEqual=" + DEFAULT_CAN_NANG);

        // Get all the thongTinKhamBenhList where canNang is less than or equal to SMALLER_CAN_NANG
        defaultThongTinKhamBenhShouldNotBeFound("canNang.lessThanOrEqual=" + SMALLER_CAN_NANG);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByCanNangIsLessThanSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where canNang is less than DEFAULT_CAN_NANG
        defaultThongTinKhamBenhShouldNotBeFound("canNang.lessThan=" + DEFAULT_CAN_NANG);

        // Get all the thongTinKhamBenhList where canNang is less than UPDATED_CAN_NANG
        defaultThongTinKhamBenhShouldBeFound("canNang.lessThan=" + UPDATED_CAN_NANG);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByCanNangIsGreaterThanSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where canNang is greater than DEFAULT_CAN_NANG
        defaultThongTinKhamBenhShouldNotBeFound("canNang.greaterThan=" + DEFAULT_CAN_NANG);

        // Get all the thongTinKhamBenhList where canNang is greater than SMALLER_CAN_NANG
        defaultThongTinKhamBenhShouldBeFound("canNang.greaterThan=" + SMALLER_CAN_NANG);
    }


    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByChieuCaoIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where chieuCao equals to DEFAULT_CHIEU_CAO
        defaultThongTinKhamBenhShouldBeFound("chieuCao.equals=" + DEFAULT_CHIEU_CAO);

        // Get all the thongTinKhamBenhList where chieuCao equals to UPDATED_CHIEU_CAO
        defaultThongTinKhamBenhShouldNotBeFound("chieuCao.equals=" + UPDATED_CHIEU_CAO);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByChieuCaoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where chieuCao not equals to DEFAULT_CHIEU_CAO
        defaultThongTinKhamBenhShouldNotBeFound("chieuCao.notEquals=" + DEFAULT_CHIEU_CAO);

        // Get all the thongTinKhamBenhList where chieuCao not equals to UPDATED_CHIEU_CAO
        defaultThongTinKhamBenhShouldBeFound("chieuCao.notEquals=" + UPDATED_CHIEU_CAO);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByChieuCaoIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where chieuCao in DEFAULT_CHIEU_CAO or UPDATED_CHIEU_CAO
        defaultThongTinKhamBenhShouldBeFound("chieuCao.in=" + DEFAULT_CHIEU_CAO + "," + UPDATED_CHIEU_CAO);

        // Get all the thongTinKhamBenhList where chieuCao equals to UPDATED_CHIEU_CAO
        defaultThongTinKhamBenhShouldNotBeFound("chieuCao.in=" + UPDATED_CHIEU_CAO);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByChieuCaoIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where chieuCao is not null
        defaultThongTinKhamBenhShouldBeFound("chieuCao.specified=true");

        // Get all the thongTinKhamBenhList where chieuCao is null
        defaultThongTinKhamBenhShouldNotBeFound("chieuCao.specified=false");
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByChieuCaoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where chieuCao is greater than or equal to DEFAULT_CHIEU_CAO
        defaultThongTinKhamBenhShouldBeFound("chieuCao.greaterThanOrEqual=" + DEFAULT_CHIEU_CAO);

        // Get all the thongTinKhamBenhList where chieuCao is greater than or equal to UPDATED_CHIEU_CAO
        defaultThongTinKhamBenhShouldNotBeFound("chieuCao.greaterThanOrEqual=" + UPDATED_CHIEU_CAO);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByChieuCaoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where chieuCao is less than or equal to DEFAULT_CHIEU_CAO
        defaultThongTinKhamBenhShouldBeFound("chieuCao.lessThanOrEqual=" + DEFAULT_CHIEU_CAO);

        // Get all the thongTinKhamBenhList where chieuCao is less than or equal to SMALLER_CHIEU_CAO
        defaultThongTinKhamBenhShouldNotBeFound("chieuCao.lessThanOrEqual=" + SMALLER_CHIEU_CAO);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByChieuCaoIsLessThanSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where chieuCao is less than DEFAULT_CHIEU_CAO
        defaultThongTinKhamBenhShouldNotBeFound("chieuCao.lessThan=" + DEFAULT_CHIEU_CAO);

        // Get all the thongTinKhamBenhList where chieuCao is less than UPDATED_CHIEU_CAO
        defaultThongTinKhamBenhShouldBeFound("chieuCao.lessThan=" + UPDATED_CHIEU_CAO);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByChieuCaoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where chieuCao is greater than DEFAULT_CHIEU_CAO
        defaultThongTinKhamBenhShouldNotBeFound("chieuCao.greaterThan=" + DEFAULT_CHIEU_CAO);

        // Get all the thongTinKhamBenhList where chieuCao is greater than SMALLER_CHIEU_CAO
        defaultThongTinKhamBenhShouldBeFound("chieuCao.greaterThan=" + SMALLER_CHIEU_CAO);
    }


    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByCreatininIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where creatinin equals to DEFAULT_CREATININ
        defaultThongTinKhamBenhShouldBeFound("creatinin.equals=" + DEFAULT_CREATININ);

        // Get all the thongTinKhamBenhList where creatinin equals to UPDATED_CREATININ
        defaultThongTinKhamBenhShouldNotBeFound("creatinin.equals=" + UPDATED_CREATININ);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByCreatininIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where creatinin not equals to DEFAULT_CREATININ
        defaultThongTinKhamBenhShouldNotBeFound("creatinin.notEquals=" + DEFAULT_CREATININ);

        // Get all the thongTinKhamBenhList where creatinin not equals to UPDATED_CREATININ
        defaultThongTinKhamBenhShouldBeFound("creatinin.notEquals=" + UPDATED_CREATININ);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByCreatininIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where creatinin in DEFAULT_CREATININ or UPDATED_CREATININ
        defaultThongTinKhamBenhShouldBeFound("creatinin.in=" + DEFAULT_CREATININ + "," + UPDATED_CREATININ);

        // Get all the thongTinKhamBenhList where creatinin equals to UPDATED_CREATININ
        defaultThongTinKhamBenhShouldNotBeFound("creatinin.in=" + UPDATED_CREATININ);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByCreatininIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where creatinin is not null
        defaultThongTinKhamBenhShouldBeFound("creatinin.specified=true");

        // Get all the thongTinKhamBenhList where creatinin is null
        defaultThongTinKhamBenhShouldNotBeFound("creatinin.specified=false");
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByCreatininIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where creatinin is greater than or equal to DEFAULT_CREATININ
        defaultThongTinKhamBenhShouldBeFound("creatinin.greaterThanOrEqual=" + DEFAULT_CREATININ);

        // Get all the thongTinKhamBenhList where creatinin is greater than or equal to UPDATED_CREATININ
        defaultThongTinKhamBenhShouldNotBeFound("creatinin.greaterThanOrEqual=" + UPDATED_CREATININ);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByCreatininIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where creatinin is less than or equal to DEFAULT_CREATININ
        defaultThongTinKhamBenhShouldBeFound("creatinin.lessThanOrEqual=" + DEFAULT_CREATININ);

        // Get all the thongTinKhamBenhList where creatinin is less than or equal to SMALLER_CREATININ
        defaultThongTinKhamBenhShouldNotBeFound("creatinin.lessThanOrEqual=" + SMALLER_CREATININ);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByCreatininIsLessThanSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where creatinin is less than DEFAULT_CREATININ
        defaultThongTinKhamBenhShouldNotBeFound("creatinin.lessThan=" + DEFAULT_CREATININ);

        // Get all the thongTinKhamBenhList where creatinin is less than UPDATED_CREATININ
        defaultThongTinKhamBenhShouldBeFound("creatinin.lessThan=" + UPDATED_CREATININ);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByCreatininIsGreaterThanSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where creatinin is greater than DEFAULT_CREATININ
        defaultThongTinKhamBenhShouldNotBeFound("creatinin.greaterThan=" + DEFAULT_CREATININ);

        // Get all the thongTinKhamBenhList where creatinin is greater than SMALLER_CREATININ
        defaultThongTinKhamBenhShouldBeFound("creatinin.greaterThan=" + SMALLER_CREATININ);
    }


    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByDauHieuLamSangIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where dauHieuLamSang equals to DEFAULT_DAU_HIEU_LAM_SANG
        defaultThongTinKhamBenhShouldBeFound("dauHieuLamSang.equals=" + DEFAULT_DAU_HIEU_LAM_SANG);

        // Get all the thongTinKhamBenhList where dauHieuLamSang equals to UPDATED_DAU_HIEU_LAM_SANG
        defaultThongTinKhamBenhShouldNotBeFound("dauHieuLamSang.equals=" + UPDATED_DAU_HIEU_LAM_SANG);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByDauHieuLamSangIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where dauHieuLamSang not equals to DEFAULT_DAU_HIEU_LAM_SANG
        defaultThongTinKhamBenhShouldNotBeFound("dauHieuLamSang.notEquals=" + DEFAULT_DAU_HIEU_LAM_SANG);

        // Get all the thongTinKhamBenhList where dauHieuLamSang not equals to UPDATED_DAU_HIEU_LAM_SANG
        defaultThongTinKhamBenhShouldBeFound("dauHieuLamSang.notEquals=" + UPDATED_DAU_HIEU_LAM_SANG);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByDauHieuLamSangIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where dauHieuLamSang in DEFAULT_DAU_HIEU_LAM_SANG or UPDATED_DAU_HIEU_LAM_SANG
        defaultThongTinKhamBenhShouldBeFound("dauHieuLamSang.in=" + DEFAULT_DAU_HIEU_LAM_SANG + "," + UPDATED_DAU_HIEU_LAM_SANG);

        // Get all the thongTinKhamBenhList where dauHieuLamSang equals to UPDATED_DAU_HIEU_LAM_SANG
        defaultThongTinKhamBenhShouldNotBeFound("dauHieuLamSang.in=" + UPDATED_DAU_HIEU_LAM_SANG);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByDauHieuLamSangIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where dauHieuLamSang is not null
        defaultThongTinKhamBenhShouldBeFound("dauHieuLamSang.specified=true");

        // Get all the thongTinKhamBenhList where dauHieuLamSang is null
        defaultThongTinKhamBenhShouldNotBeFound("dauHieuLamSang.specified=false");
    }
                @Test
    @Transactional
    public void getAllThongTinKhamBenhsByDauHieuLamSangContainsSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where dauHieuLamSang contains DEFAULT_DAU_HIEU_LAM_SANG
        defaultThongTinKhamBenhShouldBeFound("dauHieuLamSang.contains=" + DEFAULT_DAU_HIEU_LAM_SANG);

        // Get all the thongTinKhamBenhList where dauHieuLamSang contains UPDATED_DAU_HIEU_LAM_SANG
        defaultThongTinKhamBenhShouldNotBeFound("dauHieuLamSang.contains=" + UPDATED_DAU_HIEU_LAM_SANG);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByDauHieuLamSangNotContainsSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where dauHieuLamSang does not contain DEFAULT_DAU_HIEU_LAM_SANG
        defaultThongTinKhamBenhShouldNotBeFound("dauHieuLamSang.doesNotContain=" + DEFAULT_DAU_HIEU_LAM_SANG);

        // Get all the thongTinKhamBenhList where dauHieuLamSang does not contain UPDATED_DAU_HIEU_LAM_SANG
        defaultThongTinKhamBenhShouldBeFound("dauHieuLamSang.doesNotContain=" + UPDATED_DAU_HIEU_LAM_SANG);
    }


    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByDoThanhThaiIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where doThanhThai equals to DEFAULT_DO_THANH_THAI
        defaultThongTinKhamBenhShouldBeFound("doThanhThai.equals=" + DEFAULT_DO_THANH_THAI);

        // Get all the thongTinKhamBenhList where doThanhThai equals to UPDATED_DO_THANH_THAI
        defaultThongTinKhamBenhShouldNotBeFound("doThanhThai.equals=" + UPDATED_DO_THANH_THAI);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByDoThanhThaiIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where doThanhThai not equals to DEFAULT_DO_THANH_THAI
        defaultThongTinKhamBenhShouldNotBeFound("doThanhThai.notEquals=" + DEFAULT_DO_THANH_THAI);

        // Get all the thongTinKhamBenhList where doThanhThai not equals to UPDATED_DO_THANH_THAI
        defaultThongTinKhamBenhShouldBeFound("doThanhThai.notEquals=" + UPDATED_DO_THANH_THAI);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByDoThanhThaiIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where doThanhThai in DEFAULT_DO_THANH_THAI or UPDATED_DO_THANH_THAI
        defaultThongTinKhamBenhShouldBeFound("doThanhThai.in=" + DEFAULT_DO_THANH_THAI + "," + UPDATED_DO_THANH_THAI);

        // Get all the thongTinKhamBenhList where doThanhThai equals to UPDATED_DO_THANH_THAI
        defaultThongTinKhamBenhShouldNotBeFound("doThanhThai.in=" + UPDATED_DO_THANH_THAI);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByDoThanhThaiIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where doThanhThai is not null
        defaultThongTinKhamBenhShouldBeFound("doThanhThai.specified=true");

        // Get all the thongTinKhamBenhList where doThanhThai is null
        defaultThongTinKhamBenhShouldNotBeFound("doThanhThai.specified=false");
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByDoThanhThaiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where doThanhThai is greater than or equal to DEFAULT_DO_THANH_THAI
        defaultThongTinKhamBenhShouldBeFound("doThanhThai.greaterThanOrEqual=" + DEFAULT_DO_THANH_THAI);

        // Get all the thongTinKhamBenhList where doThanhThai is greater than or equal to UPDATED_DO_THANH_THAI
        defaultThongTinKhamBenhShouldNotBeFound("doThanhThai.greaterThanOrEqual=" + UPDATED_DO_THANH_THAI);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByDoThanhThaiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where doThanhThai is less than or equal to DEFAULT_DO_THANH_THAI
        defaultThongTinKhamBenhShouldBeFound("doThanhThai.lessThanOrEqual=" + DEFAULT_DO_THANH_THAI);

        // Get all the thongTinKhamBenhList where doThanhThai is less than or equal to SMALLER_DO_THANH_THAI
        defaultThongTinKhamBenhShouldNotBeFound("doThanhThai.lessThanOrEqual=" + SMALLER_DO_THANH_THAI);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByDoThanhThaiIsLessThanSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where doThanhThai is less than DEFAULT_DO_THANH_THAI
        defaultThongTinKhamBenhShouldNotBeFound("doThanhThai.lessThan=" + DEFAULT_DO_THANH_THAI);

        // Get all the thongTinKhamBenhList where doThanhThai is less than UPDATED_DO_THANH_THAI
        defaultThongTinKhamBenhShouldBeFound("doThanhThai.lessThan=" + UPDATED_DO_THANH_THAI);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByDoThanhThaiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where doThanhThai is greater than DEFAULT_DO_THANH_THAI
        defaultThongTinKhamBenhShouldNotBeFound("doThanhThai.greaterThan=" + DEFAULT_DO_THANH_THAI);

        // Get all the thongTinKhamBenhList where doThanhThai is greater than SMALLER_DO_THANH_THAI
        defaultThongTinKhamBenhShouldBeFound("doThanhThai.greaterThan=" + SMALLER_DO_THANH_THAI);
    }


    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByHuyetApCaoIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where huyetApCao equals to DEFAULT_HUYET_AP_CAO
        defaultThongTinKhamBenhShouldBeFound("huyetApCao.equals=" + DEFAULT_HUYET_AP_CAO);

        // Get all the thongTinKhamBenhList where huyetApCao equals to UPDATED_HUYET_AP_CAO
        defaultThongTinKhamBenhShouldNotBeFound("huyetApCao.equals=" + UPDATED_HUYET_AP_CAO);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByHuyetApCaoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where huyetApCao not equals to DEFAULT_HUYET_AP_CAO
        defaultThongTinKhamBenhShouldNotBeFound("huyetApCao.notEquals=" + DEFAULT_HUYET_AP_CAO);

        // Get all the thongTinKhamBenhList where huyetApCao not equals to UPDATED_HUYET_AP_CAO
        defaultThongTinKhamBenhShouldBeFound("huyetApCao.notEquals=" + UPDATED_HUYET_AP_CAO);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByHuyetApCaoIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where huyetApCao in DEFAULT_HUYET_AP_CAO or UPDATED_HUYET_AP_CAO
        defaultThongTinKhamBenhShouldBeFound("huyetApCao.in=" + DEFAULT_HUYET_AP_CAO + "," + UPDATED_HUYET_AP_CAO);

        // Get all the thongTinKhamBenhList where huyetApCao equals to UPDATED_HUYET_AP_CAO
        defaultThongTinKhamBenhShouldNotBeFound("huyetApCao.in=" + UPDATED_HUYET_AP_CAO);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByHuyetApCaoIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where huyetApCao is not null
        defaultThongTinKhamBenhShouldBeFound("huyetApCao.specified=true");

        // Get all the thongTinKhamBenhList where huyetApCao is null
        defaultThongTinKhamBenhShouldNotBeFound("huyetApCao.specified=false");
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByHuyetApCaoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where huyetApCao is greater than or equal to DEFAULT_HUYET_AP_CAO
        defaultThongTinKhamBenhShouldBeFound("huyetApCao.greaterThanOrEqual=" + DEFAULT_HUYET_AP_CAO);

        // Get all the thongTinKhamBenhList where huyetApCao is greater than or equal to UPDATED_HUYET_AP_CAO
        defaultThongTinKhamBenhShouldNotBeFound("huyetApCao.greaterThanOrEqual=" + UPDATED_HUYET_AP_CAO);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByHuyetApCaoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where huyetApCao is less than or equal to DEFAULT_HUYET_AP_CAO
        defaultThongTinKhamBenhShouldBeFound("huyetApCao.lessThanOrEqual=" + DEFAULT_HUYET_AP_CAO);

        // Get all the thongTinKhamBenhList where huyetApCao is less than or equal to SMALLER_HUYET_AP_CAO
        defaultThongTinKhamBenhShouldNotBeFound("huyetApCao.lessThanOrEqual=" + SMALLER_HUYET_AP_CAO);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByHuyetApCaoIsLessThanSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where huyetApCao is less than DEFAULT_HUYET_AP_CAO
        defaultThongTinKhamBenhShouldNotBeFound("huyetApCao.lessThan=" + DEFAULT_HUYET_AP_CAO);

        // Get all the thongTinKhamBenhList where huyetApCao is less than UPDATED_HUYET_AP_CAO
        defaultThongTinKhamBenhShouldBeFound("huyetApCao.lessThan=" + UPDATED_HUYET_AP_CAO);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByHuyetApCaoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where huyetApCao is greater than DEFAULT_HUYET_AP_CAO
        defaultThongTinKhamBenhShouldNotBeFound("huyetApCao.greaterThan=" + DEFAULT_HUYET_AP_CAO);

        // Get all the thongTinKhamBenhList where huyetApCao is greater than SMALLER_HUYET_AP_CAO
        defaultThongTinKhamBenhShouldBeFound("huyetApCao.greaterThan=" + SMALLER_HUYET_AP_CAO);
    }


    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByHuyetApThapIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where huyetApThap equals to DEFAULT_HUYET_AP_THAP
        defaultThongTinKhamBenhShouldBeFound("huyetApThap.equals=" + DEFAULT_HUYET_AP_THAP);

        // Get all the thongTinKhamBenhList where huyetApThap equals to UPDATED_HUYET_AP_THAP
        defaultThongTinKhamBenhShouldNotBeFound("huyetApThap.equals=" + UPDATED_HUYET_AP_THAP);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByHuyetApThapIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where huyetApThap not equals to DEFAULT_HUYET_AP_THAP
        defaultThongTinKhamBenhShouldNotBeFound("huyetApThap.notEquals=" + DEFAULT_HUYET_AP_THAP);

        // Get all the thongTinKhamBenhList where huyetApThap not equals to UPDATED_HUYET_AP_THAP
        defaultThongTinKhamBenhShouldBeFound("huyetApThap.notEquals=" + UPDATED_HUYET_AP_THAP);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByHuyetApThapIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where huyetApThap in DEFAULT_HUYET_AP_THAP or UPDATED_HUYET_AP_THAP
        defaultThongTinKhamBenhShouldBeFound("huyetApThap.in=" + DEFAULT_HUYET_AP_THAP + "," + UPDATED_HUYET_AP_THAP);

        // Get all the thongTinKhamBenhList where huyetApThap equals to UPDATED_HUYET_AP_THAP
        defaultThongTinKhamBenhShouldNotBeFound("huyetApThap.in=" + UPDATED_HUYET_AP_THAP);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByHuyetApThapIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where huyetApThap is not null
        defaultThongTinKhamBenhShouldBeFound("huyetApThap.specified=true");

        // Get all the thongTinKhamBenhList where huyetApThap is null
        defaultThongTinKhamBenhShouldNotBeFound("huyetApThap.specified=false");
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByHuyetApThapIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where huyetApThap is greater than or equal to DEFAULT_HUYET_AP_THAP
        defaultThongTinKhamBenhShouldBeFound("huyetApThap.greaterThanOrEqual=" + DEFAULT_HUYET_AP_THAP);

        // Get all the thongTinKhamBenhList where huyetApThap is greater than or equal to UPDATED_HUYET_AP_THAP
        defaultThongTinKhamBenhShouldNotBeFound("huyetApThap.greaterThanOrEqual=" + UPDATED_HUYET_AP_THAP);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByHuyetApThapIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where huyetApThap is less than or equal to DEFAULT_HUYET_AP_THAP
        defaultThongTinKhamBenhShouldBeFound("huyetApThap.lessThanOrEqual=" + DEFAULT_HUYET_AP_THAP);

        // Get all the thongTinKhamBenhList where huyetApThap is less than or equal to SMALLER_HUYET_AP_THAP
        defaultThongTinKhamBenhShouldNotBeFound("huyetApThap.lessThanOrEqual=" + SMALLER_HUYET_AP_THAP);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByHuyetApThapIsLessThanSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where huyetApThap is less than DEFAULT_HUYET_AP_THAP
        defaultThongTinKhamBenhShouldNotBeFound("huyetApThap.lessThan=" + DEFAULT_HUYET_AP_THAP);

        // Get all the thongTinKhamBenhList where huyetApThap is less than UPDATED_HUYET_AP_THAP
        defaultThongTinKhamBenhShouldBeFound("huyetApThap.lessThan=" + UPDATED_HUYET_AP_THAP);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByHuyetApThapIsGreaterThanSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where huyetApThap is greater than DEFAULT_HUYET_AP_THAP
        defaultThongTinKhamBenhShouldNotBeFound("huyetApThap.greaterThan=" + DEFAULT_HUYET_AP_THAP);

        // Get all the thongTinKhamBenhList where huyetApThap is greater than SMALLER_HUYET_AP_THAP
        defaultThongTinKhamBenhShouldBeFound("huyetApThap.greaterThan=" + SMALLER_HUYET_AP_THAP);
    }


    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByIcdIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where icd equals to DEFAULT_ICD
        defaultThongTinKhamBenhShouldBeFound("icd.equals=" + DEFAULT_ICD);

        // Get all the thongTinKhamBenhList where icd equals to UPDATED_ICD
        defaultThongTinKhamBenhShouldNotBeFound("icd.equals=" + UPDATED_ICD);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByIcdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where icd not equals to DEFAULT_ICD
        defaultThongTinKhamBenhShouldNotBeFound("icd.notEquals=" + DEFAULT_ICD);

        // Get all the thongTinKhamBenhList where icd not equals to UPDATED_ICD
        defaultThongTinKhamBenhShouldBeFound("icd.notEquals=" + UPDATED_ICD);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByIcdIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where icd in DEFAULT_ICD or UPDATED_ICD
        defaultThongTinKhamBenhShouldBeFound("icd.in=" + DEFAULT_ICD + "," + UPDATED_ICD);

        // Get all the thongTinKhamBenhList where icd equals to UPDATED_ICD
        defaultThongTinKhamBenhShouldNotBeFound("icd.in=" + UPDATED_ICD);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByIcdIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where icd is not null
        defaultThongTinKhamBenhShouldBeFound("icd.specified=true");

        // Get all the thongTinKhamBenhList where icd is null
        defaultThongTinKhamBenhShouldNotBeFound("icd.specified=false");
    }
                @Test
    @Transactional
    public void getAllThongTinKhamBenhsByIcdContainsSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where icd contains DEFAULT_ICD
        defaultThongTinKhamBenhShouldBeFound("icd.contains=" + DEFAULT_ICD);

        // Get all the thongTinKhamBenhList where icd contains UPDATED_ICD
        defaultThongTinKhamBenhShouldNotBeFound("icd.contains=" + UPDATED_ICD);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByIcdNotContainsSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where icd does not contain DEFAULT_ICD
        defaultThongTinKhamBenhShouldNotBeFound("icd.doesNotContain=" + DEFAULT_ICD);

        // Get all the thongTinKhamBenhList where icd does not contain UPDATED_ICD
        defaultThongTinKhamBenhShouldBeFound("icd.doesNotContain=" + UPDATED_ICD);
    }


    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByLanHenIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where lanHen equals to DEFAULT_LAN_HEN
        defaultThongTinKhamBenhShouldBeFound("lanHen.equals=" + DEFAULT_LAN_HEN);

        // Get all the thongTinKhamBenhList where lanHen equals to UPDATED_LAN_HEN
        defaultThongTinKhamBenhShouldNotBeFound("lanHen.equals=" + UPDATED_LAN_HEN);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByLanHenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where lanHen not equals to DEFAULT_LAN_HEN
        defaultThongTinKhamBenhShouldNotBeFound("lanHen.notEquals=" + DEFAULT_LAN_HEN);

        // Get all the thongTinKhamBenhList where lanHen not equals to UPDATED_LAN_HEN
        defaultThongTinKhamBenhShouldBeFound("lanHen.notEquals=" + UPDATED_LAN_HEN);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByLanHenIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where lanHen in DEFAULT_LAN_HEN or UPDATED_LAN_HEN
        defaultThongTinKhamBenhShouldBeFound("lanHen.in=" + DEFAULT_LAN_HEN + "," + UPDATED_LAN_HEN);

        // Get all the thongTinKhamBenhList where lanHen equals to UPDATED_LAN_HEN
        defaultThongTinKhamBenhShouldNotBeFound("lanHen.in=" + UPDATED_LAN_HEN);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByLanHenIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where lanHen is not null
        defaultThongTinKhamBenhShouldBeFound("lanHen.specified=true");

        // Get all the thongTinKhamBenhList where lanHen is null
        defaultThongTinKhamBenhShouldNotBeFound("lanHen.specified=false");
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByLanHenIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where lanHen is greater than or equal to DEFAULT_LAN_HEN
        defaultThongTinKhamBenhShouldBeFound("lanHen.greaterThanOrEqual=" + DEFAULT_LAN_HEN);

        // Get all the thongTinKhamBenhList where lanHen is greater than or equal to UPDATED_LAN_HEN
        defaultThongTinKhamBenhShouldNotBeFound("lanHen.greaterThanOrEqual=" + UPDATED_LAN_HEN);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByLanHenIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where lanHen is less than or equal to DEFAULT_LAN_HEN
        defaultThongTinKhamBenhShouldBeFound("lanHen.lessThanOrEqual=" + DEFAULT_LAN_HEN);

        // Get all the thongTinKhamBenhList where lanHen is less than or equal to SMALLER_LAN_HEN
        defaultThongTinKhamBenhShouldNotBeFound("lanHen.lessThanOrEqual=" + SMALLER_LAN_HEN);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByLanHenIsLessThanSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where lanHen is less than DEFAULT_LAN_HEN
        defaultThongTinKhamBenhShouldNotBeFound("lanHen.lessThan=" + DEFAULT_LAN_HEN);

        // Get all the thongTinKhamBenhList where lanHen is less than UPDATED_LAN_HEN
        defaultThongTinKhamBenhShouldBeFound("lanHen.lessThan=" + UPDATED_LAN_HEN);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByLanHenIsGreaterThanSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where lanHen is greater than DEFAULT_LAN_HEN
        defaultThongTinKhamBenhShouldNotBeFound("lanHen.greaterThan=" + DEFAULT_LAN_HEN);

        // Get all the thongTinKhamBenhList where lanHen is greater than SMALLER_LAN_HEN
        defaultThongTinKhamBenhShouldBeFound("lanHen.greaterThan=" + SMALLER_LAN_HEN);
    }


    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByLoiDanIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where loiDan equals to DEFAULT_LOI_DAN
        defaultThongTinKhamBenhShouldBeFound("loiDan.equals=" + DEFAULT_LOI_DAN);

        // Get all the thongTinKhamBenhList where loiDan equals to UPDATED_LOI_DAN
        defaultThongTinKhamBenhShouldNotBeFound("loiDan.equals=" + UPDATED_LOI_DAN);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByLoiDanIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where loiDan not equals to DEFAULT_LOI_DAN
        defaultThongTinKhamBenhShouldNotBeFound("loiDan.notEquals=" + DEFAULT_LOI_DAN);

        // Get all the thongTinKhamBenhList where loiDan not equals to UPDATED_LOI_DAN
        defaultThongTinKhamBenhShouldBeFound("loiDan.notEquals=" + UPDATED_LOI_DAN);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByLoiDanIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where loiDan in DEFAULT_LOI_DAN or UPDATED_LOI_DAN
        defaultThongTinKhamBenhShouldBeFound("loiDan.in=" + DEFAULT_LOI_DAN + "," + UPDATED_LOI_DAN);

        // Get all the thongTinKhamBenhList where loiDan equals to UPDATED_LOI_DAN
        defaultThongTinKhamBenhShouldNotBeFound("loiDan.in=" + UPDATED_LOI_DAN);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByLoiDanIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where loiDan is not null
        defaultThongTinKhamBenhShouldBeFound("loiDan.specified=true");

        // Get all the thongTinKhamBenhList where loiDan is null
        defaultThongTinKhamBenhShouldNotBeFound("loiDan.specified=false");
    }
                @Test
    @Transactional
    public void getAllThongTinKhamBenhsByLoiDanContainsSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where loiDan contains DEFAULT_LOI_DAN
        defaultThongTinKhamBenhShouldBeFound("loiDan.contains=" + DEFAULT_LOI_DAN);

        // Get all the thongTinKhamBenhList where loiDan contains UPDATED_LOI_DAN
        defaultThongTinKhamBenhShouldNotBeFound("loiDan.contains=" + UPDATED_LOI_DAN);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByLoiDanNotContainsSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where loiDan does not contain DEFAULT_LOI_DAN
        defaultThongTinKhamBenhShouldNotBeFound("loiDan.doesNotContain=" + DEFAULT_LOI_DAN);

        // Get all the thongTinKhamBenhList where loiDan does not contain UPDATED_LOI_DAN
        defaultThongTinKhamBenhShouldBeFound("loiDan.doesNotContain=" + UPDATED_LOI_DAN);
    }


    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByLyDoChuyenIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where lyDoChuyen equals to DEFAULT_LY_DO_CHUYEN
        defaultThongTinKhamBenhShouldBeFound("lyDoChuyen.equals=" + DEFAULT_LY_DO_CHUYEN);

        // Get all the thongTinKhamBenhList where lyDoChuyen equals to UPDATED_LY_DO_CHUYEN
        defaultThongTinKhamBenhShouldNotBeFound("lyDoChuyen.equals=" + UPDATED_LY_DO_CHUYEN);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByLyDoChuyenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where lyDoChuyen not equals to DEFAULT_LY_DO_CHUYEN
        defaultThongTinKhamBenhShouldNotBeFound("lyDoChuyen.notEquals=" + DEFAULT_LY_DO_CHUYEN);

        // Get all the thongTinKhamBenhList where lyDoChuyen not equals to UPDATED_LY_DO_CHUYEN
        defaultThongTinKhamBenhShouldBeFound("lyDoChuyen.notEquals=" + UPDATED_LY_DO_CHUYEN);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByLyDoChuyenIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where lyDoChuyen in DEFAULT_LY_DO_CHUYEN or UPDATED_LY_DO_CHUYEN
        defaultThongTinKhamBenhShouldBeFound("lyDoChuyen.in=" + DEFAULT_LY_DO_CHUYEN + "," + UPDATED_LY_DO_CHUYEN);

        // Get all the thongTinKhamBenhList where lyDoChuyen equals to UPDATED_LY_DO_CHUYEN
        defaultThongTinKhamBenhShouldNotBeFound("lyDoChuyen.in=" + UPDATED_LY_DO_CHUYEN);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByLyDoChuyenIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where lyDoChuyen is not null
        defaultThongTinKhamBenhShouldBeFound("lyDoChuyen.specified=true");

        // Get all the thongTinKhamBenhList where lyDoChuyen is null
        defaultThongTinKhamBenhShouldNotBeFound("lyDoChuyen.specified=false");
    }
                @Test
    @Transactional
    public void getAllThongTinKhamBenhsByLyDoChuyenContainsSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where lyDoChuyen contains DEFAULT_LY_DO_CHUYEN
        defaultThongTinKhamBenhShouldBeFound("lyDoChuyen.contains=" + DEFAULT_LY_DO_CHUYEN);

        // Get all the thongTinKhamBenhList where lyDoChuyen contains UPDATED_LY_DO_CHUYEN
        defaultThongTinKhamBenhShouldNotBeFound("lyDoChuyen.contains=" + UPDATED_LY_DO_CHUYEN);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByLyDoChuyenNotContainsSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where lyDoChuyen does not contain DEFAULT_LY_DO_CHUYEN
        defaultThongTinKhamBenhShouldNotBeFound("lyDoChuyen.doesNotContain=" + DEFAULT_LY_DO_CHUYEN);

        // Get all the thongTinKhamBenhList where lyDoChuyen does not contain UPDATED_LY_DO_CHUYEN
        defaultThongTinKhamBenhShouldBeFound("lyDoChuyen.doesNotContain=" + UPDATED_LY_DO_CHUYEN);
    }


    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByMaBenhYhctIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where maBenhYhct equals to DEFAULT_MA_BENH_YHCT
        defaultThongTinKhamBenhShouldBeFound("maBenhYhct.equals=" + DEFAULT_MA_BENH_YHCT);

        // Get all the thongTinKhamBenhList where maBenhYhct equals to UPDATED_MA_BENH_YHCT
        defaultThongTinKhamBenhShouldNotBeFound("maBenhYhct.equals=" + UPDATED_MA_BENH_YHCT);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByMaBenhYhctIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where maBenhYhct not equals to DEFAULT_MA_BENH_YHCT
        defaultThongTinKhamBenhShouldNotBeFound("maBenhYhct.notEquals=" + DEFAULT_MA_BENH_YHCT);

        // Get all the thongTinKhamBenhList where maBenhYhct not equals to UPDATED_MA_BENH_YHCT
        defaultThongTinKhamBenhShouldBeFound("maBenhYhct.notEquals=" + UPDATED_MA_BENH_YHCT);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByMaBenhYhctIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where maBenhYhct in DEFAULT_MA_BENH_YHCT or UPDATED_MA_BENH_YHCT
        defaultThongTinKhamBenhShouldBeFound("maBenhYhct.in=" + DEFAULT_MA_BENH_YHCT + "," + UPDATED_MA_BENH_YHCT);

        // Get all the thongTinKhamBenhList where maBenhYhct equals to UPDATED_MA_BENH_YHCT
        defaultThongTinKhamBenhShouldNotBeFound("maBenhYhct.in=" + UPDATED_MA_BENH_YHCT);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByMaBenhYhctIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where maBenhYhct is not null
        defaultThongTinKhamBenhShouldBeFound("maBenhYhct.specified=true");

        // Get all the thongTinKhamBenhList where maBenhYhct is null
        defaultThongTinKhamBenhShouldNotBeFound("maBenhYhct.specified=false");
    }
                @Test
    @Transactional
    public void getAllThongTinKhamBenhsByMaBenhYhctContainsSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where maBenhYhct contains DEFAULT_MA_BENH_YHCT
        defaultThongTinKhamBenhShouldBeFound("maBenhYhct.contains=" + DEFAULT_MA_BENH_YHCT);

        // Get all the thongTinKhamBenhList where maBenhYhct contains UPDATED_MA_BENH_YHCT
        defaultThongTinKhamBenhShouldNotBeFound("maBenhYhct.contains=" + UPDATED_MA_BENH_YHCT);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByMaBenhYhctNotContainsSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where maBenhYhct does not contain DEFAULT_MA_BENH_YHCT
        defaultThongTinKhamBenhShouldNotBeFound("maBenhYhct.doesNotContain=" + DEFAULT_MA_BENH_YHCT);

        // Get all the thongTinKhamBenhList where maBenhYhct does not contain UPDATED_MA_BENH_YHCT
        defaultThongTinKhamBenhShouldBeFound("maBenhYhct.doesNotContain=" + UPDATED_MA_BENH_YHCT);
    }


    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByMachIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where mach equals to DEFAULT_MACH
        defaultThongTinKhamBenhShouldBeFound("mach.equals=" + DEFAULT_MACH);

        // Get all the thongTinKhamBenhList where mach equals to UPDATED_MACH
        defaultThongTinKhamBenhShouldNotBeFound("mach.equals=" + UPDATED_MACH);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByMachIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where mach not equals to DEFAULT_MACH
        defaultThongTinKhamBenhShouldNotBeFound("mach.notEquals=" + DEFAULT_MACH);

        // Get all the thongTinKhamBenhList where mach not equals to UPDATED_MACH
        defaultThongTinKhamBenhShouldBeFound("mach.notEquals=" + UPDATED_MACH);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByMachIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where mach in DEFAULT_MACH or UPDATED_MACH
        defaultThongTinKhamBenhShouldBeFound("mach.in=" + DEFAULT_MACH + "," + UPDATED_MACH);

        // Get all the thongTinKhamBenhList where mach equals to UPDATED_MACH
        defaultThongTinKhamBenhShouldNotBeFound("mach.in=" + UPDATED_MACH);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByMachIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where mach is not null
        defaultThongTinKhamBenhShouldBeFound("mach.specified=true");

        // Get all the thongTinKhamBenhList where mach is null
        defaultThongTinKhamBenhShouldNotBeFound("mach.specified=false");
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByMachIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where mach is greater than or equal to DEFAULT_MACH
        defaultThongTinKhamBenhShouldBeFound("mach.greaterThanOrEqual=" + DEFAULT_MACH);

        // Get all the thongTinKhamBenhList where mach is greater than or equal to UPDATED_MACH
        defaultThongTinKhamBenhShouldNotBeFound("mach.greaterThanOrEqual=" + UPDATED_MACH);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByMachIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where mach is less than or equal to DEFAULT_MACH
        defaultThongTinKhamBenhShouldBeFound("mach.lessThanOrEqual=" + DEFAULT_MACH);

        // Get all the thongTinKhamBenhList where mach is less than or equal to SMALLER_MACH
        defaultThongTinKhamBenhShouldNotBeFound("mach.lessThanOrEqual=" + SMALLER_MACH);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByMachIsLessThanSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where mach is less than DEFAULT_MACH
        defaultThongTinKhamBenhShouldNotBeFound("mach.lessThan=" + DEFAULT_MACH);

        // Get all the thongTinKhamBenhList where mach is less than UPDATED_MACH
        defaultThongTinKhamBenhShouldBeFound("mach.lessThan=" + UPDATED_MACH);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByMachIsGreaterThanSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where mach is greater than DEFAULT_MACH
        defaultThongTinKhamBenhShouldNotBeFound("mach.greaterThan=" + DEFAULT_MACH);

        // Get all the thongTinKhamBenhList where mach is greater than SMALLER_MACH
        defaultThongTinKhamBenhShouldBeFound("mach.greaterThan=" + SMALLER_MACH);
    }


    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByMaxNgayRaToaIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where maxNgayRaToa equals to DEFAULT_MAX_NGAY_RA_TOA
        defaultThongTinKhamBenhShouldBeFound("maxNgayRaToa.equals=" + DEFAULT_MAX_NGAY_RA_TOA);

        // Get all the thongTinKhamBenhList where maxNgayRaToa equals to UPDATED_MAX_NGAY_RA_TOA
        defaultThongTinKhamBenhShouldNotBeFound("maxNgayRaToa.equals=" + UPDATED_MAX_NGAY_RA_TOA);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByMaxNgayRaToaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where maxNgayRaToa not equals to DEFAULT_MAX_NGAY_RA_TOA
        defaultThongTinKhamBenhShouldNotBeFound("maxNgayRaToa.notEquals=" + DEFAULT_MAX_NGAY_RA_TOA);

        // Get all the thongTinKhamBenhList where maxNgayRaToa not equals to UPDATED_MAX_NGAY_RA_TOA
        defaultThongTinKhamBenhShouldBeFound("maxNgayRaToa.notEquals=" + UPDATED_MAX_NGAY_RA_TOA);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByMaxNgayRaToaIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where maxNgayRaToa in DEFAULT_MAX_NGAY_RA_TOA or UPDATED_MAX_NGAY_RA_TOA
        defaultThongTinKhamBenhShouldBeFound("maxNgayRaToa.in=" + DEFAULT_MAX_NGAY_RA_TOA + "," + UPDATED_MAX_NGAY_RA_TOA);

        // Get all the thongTinKhamBenhList where maxNgayRaToa equals to UPDATED_MAX_NGAY_RA_TOA
        defaultThongTinKhamBenhShouldNotBeFound("maxNgayRaToa.in=" + UPDATED_MAX_NGAY_RA_TOA);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByMaxNgayRaToaIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where maxNgayRaToa is not null
        defaultThongTinKhamBenhShouldBeFound("maxNgayRaToa.specified=true");

        // Get all the thongTinKhamBenhList where maxNgayRaToa is null
        defaultThongTinKhamBenhShouldNotBeFound("maxNgayRaToa.specified=false");
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByMaxNgayRaToaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where maxNgayRaToa is greater than or equal to DEFAULT_MAX_NGAY_RA_TOA
        defaultThongTinKhamBenhShouldBeFound("maxNgayRaToa.greaterThanOrEqual=" + DEFAULT_MAX_NGAY_RA_TOA);

        // Get all the thongTinKhamBenhList where maxNgayRaToa is greater than or equal to UPDATED_MAX_NGAY_RA_TOA
        defaultThongTinKhamBenhShouldNotBeFound("maxNgayRaToa.greaterThanOrEqual=" + UPDATED_MAX_NGAY_RA_TOA);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByMaxNgayRaToaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where maxNgayRaToa is less than or equal to DEFAULT_MAX_NGAY_RA_TOA
        defaultThongTinKhamBenhShouldBeFound("maxNgayRaToa.lessThanOrEqual=" + DEFAULT_MAX_NGAY_RA_TOA);

        // Get all the thongTinKhamBenhList where maxNgayRaToa is less than or equal to SMALLER_MAX_NGAY_RA_TOA
        defaultThongTinKhamBenhShouldNotBeFound("maxNgayRaToa.lessThanOrEqual=" + SMALLER_MAX_NGAY_RA_TOA);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByMaxNgayRaToaIsLessThanSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where maxNgayRaToa is less than DEFAULT_MAX_NGAY_RA_TOA
        defaultThongTinKhamBenhShouldNotBeFound("maxNgayRaToa.lessThan=" + DEFAULT_MAX_NGAY_RA_TOA);

        // Get all the thongTinKhamBenhList where maxNgayRaToa is less than UPDATED_MAX_NGAY_RA_TOA
        defaultThongTinKhamBenhShouldBeFound("maxNgayRaToa.lessThan=" + UPDATED_MAX_NGAY_RA_TOA);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByMaxNgayRaToaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where maxNgayRaToa is greater than DEFAULT_MAX_NGAY_RA_TOA
        defaultThongTinKhamBenhShouldNotBeFound("maxNgayRaToa.greaterThan=" + DEFAULT_MAX_NGAY_RA_TOA);

        // Get all the thongTinKhamBenhList where maxNgayRaToa is greater than SMALLER_MAX_NGAY_RA_TOA
        defaultThongTinKhamBenhShouldBeFound("maxNgayRaToa.greaterThan=" + SMALLER_MAX_NGAY_RA_TOA);
    }


    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByNgayHenIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where ngayHen equals to DEFAULT_NGAY_HEN
        defaultThongTinKhamBenhShouldBeFound("ngayHen.equals=" + DEFAULT_NGAY_HEN);

        // Get all the thongTinKhamBenhList where ngayHen equals to UPDATED_NGAY_HEN
        defaultThongTinKhamBenhShouldNotBeFound("ngayHen.equals=" + UPDATED_NGAY_HEN);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByNgayHenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where ngayHen not equals to DEFAULT_NGAY_HEN
        defaultThongTinKhamBenhShouldNotBeFound("ngayHen.notEquals=" + DEFAULT_NGAY_HEN);

        // Get all the thongTinKhamBenhList where ngayHen not equals to UPDATED_NGAY_HEN
        defaultThongTinKhamBenhShouldBeFound("ngayHen.notEquals=" + UPDATED_NGAY_HEN);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByNgayHenIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where ngayHen in DEFAULT_NGAY_HEN or UPDATED_NGAY_HEN
        defaultThongTinKhamBenhShouldBeFound("ngayHen.in=" + DEFAULT_NGAY_HEN + "," + UPDATED_NGAY_HEN);

        // Get all the thongTinKhamBenhList where ngayHen equals to UPDATED_NGAY_HEN
        defaultThongTinKhamBenhShouldNotBeFound("ngayHen.in=" + UPDATED_NGAY_HEN);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByNgayHenIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where ngayHen is not null
        defaultThongTinKhamBenhShouldBeFound("ngayHen.specified=true");

        // Get all the thongTinKhamBenhList where ngayHen is null
        defaultThongTinKhamBenhShouldNotBeFound("ngayHen.specified=false");
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByNgayHenIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where ngayHen is greater than or equal to DEFAULT_NGAY_HEN
        defaultThongTinKhamBenhShouldBeFound("ngayHen.greaterThanOrEqual=" + DEFAULT_NGAY_HEN);

        // Get all the thongTinKhamBenhList where ngayHen is greater than or equal to UPDATED_NGAY_HEN
        defaultThongTinKhamBenhShouldNotBeFound("ngayHen.greaterThanOrEqual=" + UPDATED_NGAY_HEN);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByNgayHenIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where ngayHen is less than or equal to DEFAULT_NGAY_HEN
        defaultThongTinKhamBenhShouldBeFound("ngayHen.lessThanOrEqual=" + DEFAULT_NGAY_HEN);

        // Get all the thongTinKhamBenhList where ngayHen is less than or equal to SMALLER_NGAY_HEN
        defaultThongTinKhamBenhShouldNotBeFound("ngayHen.lessThanOrEqual=" + SMALLER_NGAY_HEN);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByNgayHenIsLessThanSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where ngayHen is less than DEFAULT_NGAY_HEN
        defaultThongTinKhamBenhShouldNotBeFound("ngayHen.lessThan=" + DEFAULT_NGAY_HEN);

        // Get all the thongTinKhamBenhList where ngayHen is less than UPDATED_NGAY_HEN
        defaultThongTinKhamBenhShouldBeFound("ngayHen.lessThan=" + UPDATED_NGAY_HEN);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByNgayHenIsGreaterThanSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where ngayHen is greater than DEFAULT_NGAY_HEN
        defaultThongTinKhamBenhShouldNotBeFound("ngayHen.greaterThan=" + DEFAULT_NGAY_HEN);

        // Get all the thongTinKhamBenhList where ngayHen is greater than SMALLER_NGAY_HEN
        defaultThongTinKhamBenhShouldBeFound("ngayHen.greaterThan=" + SMALLER_NGAY_HEN);
    }


    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByNhanDinhBmiIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where nhanDinhBmi equals to DEFAULT_NHAN_DINH_BMI
        defaultThongTinKhamBenhShouldBeFound("nhanDinhBmi.equals=" + DEFAULT_NHAN_DINH_BMI);

        // Get all the thongTinKhamBenhList where nhanDinhBmi equals to UPDATED_NHAN_DINH_BMI
        defaultThongTinKhamBenhShouldNotBeFound("nhanDinhBmi.equals=" + UPDATED_NHAN_DINH_BMI);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByNhanDinhBmiIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where nhanDinhBmi not equals to DEFAULT_NHAN_DINH_BMI
        defaultThongTinKhamBenhShouldNotBeFound("nhanDinhBmi.notEquals=" + DEFAULT_NHAN_DINH_BMI);

        // Get all the thongTinKhamBenhList where nhanDinhBmi not equals to UPDATED_NHAN_DINH_BMI
        defaultThongTinKhamBenhShouldBeFound("nhanDinhBmi.notEquals=" + UPDATED_NHAN_DINH_BMI);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByNhanDinhBmiIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where nhanDinhBmi in DEFAULT_NHAN_DINH_BMI or UPDATED_NHAN_DINH_BMI
        defaultThongTinKhamBenhShouldBeFound("nhanDinhBmi.in=" + DEFAULT_NHAN_DINH_BMI + "," + UPDATED_NHAN_DINH_BMI);

        // Get all the thongTinKhamBenhList where nhanDinhBmi equals to UPDATED_NHAN_DINH_BMI
        defaultThongTinKhamBenhShouldNotBeFound("nhanDinhBmi.in=" + UPDATED_NHAN_DINH_BMI);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByNhanDinhBmiIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where nhanDinhBmi is not null
        defaultThongTinKhamBenhShouldBeFound("nhanDinhBmi.specified=true");

        // Get all the thongTinKhamBenhList where nhanDinhBmi is null
        defaultThongTinKhamBenhShouldNotBeFound("nhanDinhBmi.specified=false");
    }
                @Test
    @Transactional
    public void getAllThongTinKhamBenhsByNhanDinhBmiContainsSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where nhanDinhBmi contains DEFAULT_NHAN_DINH_BMI
        defaultThongTinKhamBenhShouldBeFound("nhanDinhBmi.contains=" + DEFAULT_NHAN_DINH_BMI);

        // Get all the thongTinKhamBenhList where nhanDinhBmi contains UPDATED_NHAN_DINH_BMI
        defaultThongTinKhamBenhShouldNotBeFound("nhanDinhBmi.contains=" + UPDATED_NHAN_DINH_BMI);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByNhanDinhBmiNotContainsSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where nhanDinhBmi does not contain DEFAULT_NHAN_DINH_BMI
        defaultThongTinKhamBenhShouldNotBeFound("nhanDinhBmi.doesNotContain=" + DEFAULT_NHAN_DINH_BMI);

        // Get all the thongTinKhamBenhList where nhanDinhBmi does not contain UPDATED_NHAN_DINH_BMI
        defaultThongTinKhamBenhShouldBeFound("nhanDinhBmi.doesNotContain=" + UPDATED_NHAN_DINH_BMI);
    }


    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByNhanDinhDoThanhThaiIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where nhanDinhDoThanhThai equals to DEFAULT_NHAN_DINH_DO_THANH_THAI
        defaultThongTinKhamBenhShouldBeFound("nhanDinhDoThanhThai.equals=" + DEFAULT_NHAN_DINH_DO_THANH_THAI);

        // Get all the thongTinKhamBenhList where nhanDinhDoThanhThai equals to UPDATED_NHAN_DINH_DO_THANH_THAI
        defaultThongTinKhamBenhShouldNotBeFound("nhanDinhDoThanhThai.equals=" + UPDATED_NHAN_DINH_DO_THANH_THAI);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByNhanDinhDoThanhThaiIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where nhanDinhDoThanhThai not equals to DEFAULT_NHAN_DINH_DO_THANH_THAI
        defaultThongTinKhamBenhShouldNotBeFound("nhanDinhDoThanhThai.notEquals=" + DEFAULT_NHAN_DINH_DO_THANH_THAI);

        // Get all the thongTinKhamBenhList where nhanDinhDoThanhThai not equals to UPDATED_NHAN_DINH_DO_THANH_THAI
        defaultThongTinKhamBenhShouldBeFound("nhanDinhDoThanhThai.notEquals=" + UPDATED_NHAN_DINH_DO_THANH_THAI);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByNhanDinhDoThanhThaiIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where nhanDinhDoThanhThai in DEFAULT_NHAN_DINH_DO_THANH_THAI or UPDATED_NHAN_DINH_DO_THANH_THAI
        defaultThongTinKhamBenhShouldBeFound("nhanDinhDoThanhThai.in=" + DEFAULT_NHAN_DINH_DO_THANH_THAI + "," + UPDATED_NHAN_DINH_DO_THANH_THAI);

        // Get all the thongTinKhamBenhList where nhanDinhDoThanhThai equals to UPDATED_NHAN_DINH_DO_THANH_THAI
        defaultThongTinKhamBenhShouldNotBeFound("nhanDinhDoThanhThai.in=" + UPDATED_NHAN_DINH_DO_THANH_THAI);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByNhanDinhDoThanhThaiIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where nhanDinhDoThanhThai is not null
        defaultThongTinKhamBenhShouldBeFound("nhanDinhDoThanhThai.specified=true");

        // Get all the thongTinKhamBenhList where nhanDinhDoThanhThai is null
        defaultThongTinKhamBenhShouldNotBeFound("nhanDinhDoThanhThai.specified=false");
    }
                @Test
    @Transactional
    public void getAllThongTinKhamBenhsByNhanDinhDoThanhThaiContainsSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where nhanDinhDoThanhThai contains DEFAULT_NHAN_DINH_DO_THANH_THAI
        defaultThongTinKhamBenhShouldBeFound("nhanDinhDoThanhThai.contains=" + DEFAULT_NHAN_DINH_DO_THANH_THAI);

        // Get all the thongTinKhamBenhList where nhanDinhDoThanhThai contains UPDATED_NHAN_DINH_DO_THANH_THAI
        defaultThongTinKhamBenhShouldNotBeFound("nhanDinhDoThanhThai.contains=" + UPDATED_NHAN_DINH_DO_THANH_THAI);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByNhanDinhDoThanhThaiNotContainsSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where nhanDinhDoThanhThai does not contain DEFAULT_NHAN_DINH_DO_THANH_THAI
        defaultThongTinKhamBenhShouldNotBeFound("nhanDinhDoThanhThai.doesNotContain=" + DEFAULT_NHAN_DINH_DO_THANH_THAI);

        // Get all the thongTinKhamBenhList where nhanDinhDoThanhThai does not contain UPDATED_NHAN_DINH_DO_THANH_THAI
        defaultThongTinKhamBenhShouldBeFound("nhanDinhDoThanhThai.doesNotContain=" + UPDATED_NHAN_DINH_DO_THANH_THAI);
    }


    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByNhapVienIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where nhapVien equals to DEFAULT_NHAP_VIEN
        defaultThongTinKhamBenhShouldBeFound("nhapVien.equals=" + DEFAULT_NHAP_VIEN);

        // Get all the thongTinKhamBenhList where nhapVien equals to UPDATED_NHAP_VIEN
        defaultThongTinKhamBenhShouldNotBeFound("nhapVien.equals=" + UPDATED_NHAP_VIEN);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByNhapVienIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where nhapVien not equals to DEFAULT_NHAP_VIEN
        defaultThongTinKhamBenhShouldNotBeFound("nhapVien.notEquals=" + DEFAULT_NHAP_VIEN);

        // Get all the thongTinKhamBenhList where nhapVien not equals to UPDATED_NHAP_VIEN
        defaultThongTinKhamBenhShouldBeFound("nhapVien.notEquals=" + UPDATED_NHAP_VIEN);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByNhapVienIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where nhapVien in DEFAULT_NHAP_VIEN or UPDATED_NHAP_VIEN
        defaultThongTinKhamBenhShouldBeFound("nhapVien.in=" + DEFAULT_NHAP_VIEN + "," + UPDATED_NHAP_VIEN);

        // Get all the thongTinKhamBenhList where nhapVien equals to UPDATED_NHAP_VIEN
        defaultThongTinKhamBenhShouldNotBeFound("nhapVien.in=" + UPDATED_NHAP_VIEN);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByNhapVienIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where nhapVien is not null
        defaultThongTinKhamBenhShouldBeFound("nhapVien.specified=true");

        // Get all the thongTinKhamBenhList where nhapVien is null
        defaultThongTinKhamBenhShouldNotBeFound("nhapVien.specified=false");
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByNhietDoIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where nhietDo equals to DEFAULT_NHIET_DO
        defaultThongTinKhamBenhShouldBeFound("nhietDo.equals=" + DEFAULT_NHIET_DO);

        // Get all the thongTinKhamBenhList where nhietDo equals to UPDATED_NHIET_DO
        defaultThongTinKhamBenhShouldNotBeFound("nhietDo.equals=" + UPDATED_NHIET_DO);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByNhietDoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where nhietDo not equals to DEFAULT_NHIET_DO
        defaultThongTinKhamBenhShouldNotBeFound("nhietDo.notEquals=" + DEFAULT_NHIET_DO);

        // Get all the thongTinKhamBenhList where nhietDo not equals to UPDATED_NHIET_DO
        defaultThongTinKhamBenhShouldBeFound("nhietDo.notEquals=" + UPDATED_NHIET_DO);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByNhietDoIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where nhietDo in DEFAULT_NHIET_DO or UPDATED_NHIET_DO
        defaultThongTinKhamBenhShouldBeFound("nhietDo.in=" + DEFAULT_NHIET_DO + "," + UPDATED_NHIET_DO);

        // Get all the thongTinKhamBenhList where nhietDo equals to UPDATED_NHIET_DO
        defaultThongTinKhamBenhShouldNotBeFound("nhietDo.in=" + UPDATED_NHIET_DO);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByNhietDoIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where nhietDo is not null
        defaultThongTinKhamBenhShouldBeFound("nhietDo.specified=true");

        // Get all the thongTinKhamBenhList where nhietDo is null
        defaultThongTinKhamBenhShouldNotBeFound("nhietDo.specified=false");
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByNhietDoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where nhietDo is greater than or equal to DEFAULT_NHIET_DO
        defaultThongTinKhamBenhShouldBeFound("nhietDo.greaterThanOrEqual=" + DEFAULT_NHIET_DO);

        // Get all the thongTinKhamBenhList where nhietDo is greater than or equal to UPDATED_NHIET_DO
        defaultThongTinKhamBenhShouldNotBeFound("nhietDo.greaterThanOrEqual=" + UPDATED_NHIET_DO);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByNhietDoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where nhietDo is less than or equal to DEFAULT_NHIET_DO
        defaultThongTinKhamBenhShouldBeFound("nhietDo.lessThanOrEqual=" + DEFAULT_NHIET_DO);

        // Get all the thongTinKhamBenhList where nhietDo is less than or equal to SMALLER_NHIET_DO
        defaultThongTinKhamBenhShouldNotBeFound("nhietDo.lessThanOrEqual=" + SMALLER_NHIET_DO);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByNhietDoIsLessThanSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where nhietDo is less than DEFAULT_NHIET_DO
        defaultThongTinKhamBenhShouldNotBeFound("nhietDo.lessThan=" + DEFAULT_NHIET_DO);

        // Get all the thongTinKhamBenhList where nhietDo is less than UPDATED_NHIET_DO
        defaultThongTinKhamBenhShouldBeFound("nhietDo.lessThan=" + UPDATED_NHIET_DO);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByNhietDoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where nhietDo is greater than DEFAULT_NHIET_DO
        defaultThongTinKhamBenhShouldNotBeFound("nhietDo.greaterThan=" + DEFAULT_NHIET_DO);

        // Get all the thongTinKhamBenhList where nhietDo is greater than SMALLER_NHIET_DO
        defaultThongTinKhamBenhShouldBeFound("nhietDo.greaterThan=" + SMALLER_NHIET_DO);
    }


    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByNhipThoIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where nhipTho equals to DEFAULT_NHIP_THO
        defaultThongTinKhamBenhShouldBeFound("nhipTho.equals=" + DEFAULT_NHIP_THO);

        // Get all the thongTinKhamBenhList where nhipTho equals to UPDATED_NHIP_THO
        defaultThongTinKhamBenhShouldNotBeFound("nhipTho.equals=" + UPDATED_NHIP_THO);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByNhipThoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where nhipTho not equals to DEFAULT_NHIP_THO
        defaultThongTinKhamBenhShouldNotBeFound("nhipTho.notEquals=" + DEFAULT_NHIP_THO);

        // Get all the thongTinKhamBenhList where nhipTho not equals to UPDATED_NHIP_THO
        defaultThongTinKhamBenhShouldBeFound("nhipTho.notEquals=" + UPDATED_NHIP_THO);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByNhipThoIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where nhipTho in DEFAULT_NHIP_THO or UPDATED_NHIP_THO
        defaultThongTinKhamBenhShouldBeFound("nhipTho.in=" + DEFAULT_NHIP_THO + "," + UPDATED_NHIP_THO);

        // Get all the thongTinKhamBenhList where nhipTho equals to UPDATED_NHIP_THO
        defaultThongTinKhamBenhShouldNotBeFound("nhipTho.in=" + UPDATED_NHIP_THO);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByNhipThoIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where nhipTho is not null
        defaultThongTinKhamBenhShouldBeFound("nhipTho.specified=true");

        // Get all the thongTinKhamBenhList where nhipTho is null
        defaultThongTinKhamBenhShouldNotBeFound("nhipTho.specified=false");
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByNhipThoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where nhipTho is greater than or equal to DEFAULT_NHIP_THO
        defaultThongTinKhamBenhShouldBeFound("nhipTho.greaterThanOrEqual=" + DEFAULT_NHIP_THO);

        // Get all the thongTinKhamBenhList where nhipTho is greater than or equal to UPDATED_NHIP_THO
        defaultThongTinKhamBenhShouldNotBeFound("nhipTho.greaterThanOrEqual=" + UPDATED_NHIP_THO);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByNhipThoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where nhipTho is less than or equal to DEFAULT_NHIP_THO
        defaultThongTinKhamBenhShouldBeFound("nhipTho.lessThanOrEqual=" + DEFAULT_NHIP_THO);

        // Get all the thongTinKhamBenhList where nhipTho is less than or equal to SMALLER_NHIP_THO
        defaultThongTinKhamBenhShouldNotBeFound("nhipTho.lessThanOrEqual=" + SMALLER_NHIP_THO);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByNhipThoIsLessThanSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where nhipTho is less than DEFAULT_NHIP_THO
        defaultThongTinKhamBenhShouldNotBeFound("nhipTho.lessThan=" + DEFAULT_NHIP_THO);

        // Get all the thongTinKhamBenhList where nhipTho is less than UPDATED_NHIP_THO
        defaultThongTinKhamBenhShouldBeFound("nhipTho.lessThan=" + UPDATED_NHIP_THO);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByNhipThoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where nhipTho is greater than DEFAULT_NHIP_THO
        defaultThongTinKhamBenhShouldNotBeFound("nhipTho.greaterThan=" + DEFAULT_NHIP_THO);

        // Get all the thongTinKhamBenhList where nhipTho is greater than SMALLER_NHIP_THO
        defaultThongTinKhamBenhShouldBeFound("nhipTho.greaterThan=" + SMALLER_NHIP_THO);
    }


    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByPpDieuTriYtctIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where ppDieuTriYtct equals to DEFAULT_PP_DIEU_TRI_YTCT
        defaultThongTinKhamBenhShouldBeFound("ppDieuTriYtct.equals=" + DEFAULT_PP_DIEU_TRI_YTCT);

        // Get all the thongTinKhamBenhList where ppDieuTriYtct equals to UPDATED_PP_DIEU_TRI_YTCT
        defaultThongTinKhamBenhShouldNotBeFound("ppDieuTriYtct.equals=" + UPDATED_PP_DIEU_TRI_YTCT);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByPpDieuTriYtctIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where ppDieuTriYtct not equals to DEFAULT_PP_DIEU_TRI_YTCT
        defaultThongTinKhamBenhShouldNotBeFound("ppDieuTriYtct.notEquals=" + DEFAULT_PP_DIEU_TRI_YTCT);

        // Get all the thongTinKhamBenhList where ppDieuTriYtct not equals to UPDATED_PP_DIEU_TRI_YTCT
        defaultThongTinKhamBenhShouldBeFound("ppDieuTriYtct.notEquals=" + UPDATED_PP_DIEU_TRI_YTCT);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByPpDieuTriYtctIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where ppDieuTriYtct in DEFAULT_PP_DIEU_TRI_YTCT or UPDATED_PP_DIEU_TRI_YTCT
        defaultThongTinKhamBenhShouldBeFound("ppDieuTriYtct.in=" + DEFAULT_PP_DIEU_TRI_YTCT + "," + UPDATED_PP_DIEU_TRI_YTCT);

        // Get all the thongTinKhamBenhList where ppDieuTriYtct equals to UPDATED_PP_DIEU_TRI_YTCT
        defaultThongTinKhamBenhShouldNotBeFound("ppDieuTriYtct.in=" + UPDATED_PP_DIEU_TRI_YTCT);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByPpDieuTriYtctIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where ppDieuTriYtct is not null
        defaultThongTinKhamBenhShouldBeFound("ppDieuTriYtct.specified=true");

        // Get all the thongTinKhamBenhList where ppDieuTriYtct is null
        defaultThongTinKhamBenhShouldNotBeFound("ppDieuTriYtct.specified=false");
    }
                @Test
    @Transactional
    public void getAllThongTinKhamBenhsByPpDieuTriYtctContainsSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where ppDieuTriYtct contains DEFAULT_PP_DIEU_TRI_YTCT
        defaultThongTinKhamBenhShouldBeFound("ppDieuTriYtct.contains=" + DEFAULT_PP_DIEU_TRI_YTCT);

        // Get all the thongTinKhamBenhList where ppDieuTriYtct contains UPDATED_PP_DIEU_TRI_YTCT
        defaultThongTinKhamBenhShouldNotBeFound("ppDieuTriYtct.contains=" + UPDATED_PP_DIEU_TRI_YTCT);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByPpDieuTriYtctNotContainsSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where ppDieuTriYtct does not contain DEFAULT_PP_DIEU_TRI_YTCT
        defaultThongTinKhamBenhShouldNotBeFound("ppDieuTriYtct.doesNotContain=" + DEFAULT_PP_DIEU_TRI_YTCT);

        // Get all the thongTinKhamBenhList where ppDieuTriYtct does not contain UPDATED_PP_DIEU_TRI_YTCT
        defaultThongTinKhamBenhShouldBeFound("ppDieuTriYtct.doesNotContain=" + UPDATED_PP_DIEU_TRI_YTCT);
    }


    @Test
    @Transactional
    public void getAllThongTinKhamBenhsBySoLanInBangKeIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where soLanInBangKe equals to DEFAULT_SO_LAN_IN_BANG_KE
        defaultThongTinKhamBenhShouldBeFound("soLanInBangKe.equals=" + DEFAULT_SO_LAN_IN_BANG_KE);

        // Get all the thongTinKhamBenhList where soLanInBangKe equals to UPDATED_SO_LAN_IN_BANG_KE
        defaultThongTinKhamBenhShouldNotBeFound("soLanInBangKe.equals=" + UPDATED_SO_LAN_IN_BANG_KE);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsBySoLanInBangKeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where soLanInBangKe not equals to DEFAULT_SO_LAN_IN_BANG_KE
        defaultThongTinKhamBenhShouldNotBeFound("soLanInBangKe.notEquals=" + DEFAULT_SO_LAN_IN_BANG_KE);

        // Get all the thongTinKhamBenhList where soLanInBangKe not equals to UPDATED_SO_LAN_IN_BANG_KE
        defaultThongTinKhamBenhShouldBeFound("soLanInBangKe.notEquals=" + UPDATED_SO_LAN_IN_BANG_KE);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsBySoLanInBangKeIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where soLanInBangKe in DEFAULT_SO_LAN_IN_BANG_KE or UPDATED_SO_LAN_IN_BANG_KE
        defaultThongTinKhamBenhShouldBeFound("soLanInBangKe.in=" + DEFAULT_SO_LAN_IN_BANG_KE + "," + UPDATED_SO_LAN_IN_BANG_KE);

        // Get all the thongTinKhamBenhList where soLanInBangKe equals to UPDATED_SO_LAN_IN_BANG_KE
        defaultThongTinKhamBenhShouldNotBeFound("soLanInBangKe.in=" + UPDATED_SO_LAN_IN_BANG_KE);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsBySoLanInBangKeIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where soLanInBangKe is not null
        defaultThongTinKhamBenhShouldBeFound("soLanInBangKe.specified=true");

        // Get all the thongTinKhamBenhList where soLanInBangKe is null
        defaultThongTinKhamBenhShouldNotBeFound("soLanInBangKe.specified=false");
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsBySoLanInBangKeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where soLanInBangKe is greater than or equal to DEFAULT_SO_LAN_IN_BANG_KE
        defaultThongTinKhamBenhShouldBeFound("soLanInBangKe.greaterThanOrEqual=" + DEFAULT_SO_LAN_IN_BANG_KE);

        // Get all the thongTinKhamBenhList where soLanInBangKe is greater than or equal to UPDATED_SO_LAN_IN_BANG_KE
        defaultThongTinKhamBenhShouldNotBeFound("soLanInBangKe.greaterThanOrEqual=" + UPDATED_SO_LAN_IN_BANG_KE);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsBySoLanInBangKeIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where soLanInBangKe is less than or equal to DEFAULT_SO_LAN_IN_BANG_KE
        defaultThongTinKhamBenhShouldBeFound("soLanInBangKe.lessThanOrEqual=" + DEFAULT_SO_LAN_IN_BANG_KE);

        // Get all the thongTinKhamBenhList where soLanInBangKe is less than or equal to SMALLER_SO_LAN_IN_BANG_KE
        defaultThongTinKhamBenhShouldNotBeFound("soLanInBangKe.lessThanOrEqual=" + SMALLER_SO_LAN_IN_BANG_KE);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsBySoLanInBangKeIsLessThanSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where soLanInBangKe is less than DEFAULT_SO_LAN_IN_BANG_KE
        defaultThongTinKhamBenhShouldNotBeFound("soLanInBangKe.lessThan=" + DEFAULT_SO_LAN_IN_BANG_KE);

        // Get all the thongTinKhamBenhList where soLanInBangKe is less than UPDATED_SO_LAN_IN_BANG_KE
        defaultThongTinKhamBenhShouldBeFound("soLanInBangKe.lessThan=" + UPDATED_SO_LAN_IN_BANG_KE);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsBySoLanInBangKeIsGreaterThanSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where soLanInBangKe is greater than DEFAULT_SO_LAN_IN_BANG_KE
        defaultThongTinKhamBenhShouldNotBeFound("soLanInBangKe.greaterThan=" + DEFAULT_SO_LAN_IN_BANG_KE);

        // Get all the thongTinKhamBenhList where soLanInBangKe is greater than SMALLER_SO_LAN_IN_BANG_KE
        defaultThongTinKhamBenhShouldBeFound("soLanInBangKe.greaterThan=" + SMALLER_SO_LAN_IN_BANG_KE);
    }


    @Test
    @Transactional
    public void getAllThongTinKhamBenhsBySoLanInToaThuocIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where soLanInToaThuoc equals to DEFAULT_SO_LAN_IN_TOA_THUOC
        defaultThongTinKhamBenhShouldBeFound("soLanInToaThuoc.equals=" + DEFAULT_SO_LAN_IN_TOA_THUOC);

        // Get all the thongTinKhamBenhList where soLanInToaThuoc equals to UPDATED_SO_LAN_IN_TOA_THUOC
        defaultThongTinKhamBenhShouldNotBeFound("soLanInToaThuoc.equals=" + UPDATED_SO_LAN_IN_TOA_THUOC);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsBySoLanInToaThuocIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where soLanInToaThuoc not equals to DEFAULT_SO_LAN_IN_TOA_THUOC
        defaultThongTinKhamBenhShouldNotBeFound("soLanInToaThuoc.notEquals=" + DEFAULT_SO_LAN_IN_TOA_THUOC);

        // Get all the thongTinKhamBenhList where soLanInToaThuoc not equals to UPDATED_SO_LAN_IN_TOA_THUOC
        defaultThongTinKhamBenhShouldBeFound("soLanInToaThuoc.notEquals=" + UPDATED_SO_LAN_IN_TOA_THUOC);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsBySoLanInToaThuocIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where soLanInToaThuoc in DEFAULT_SO_LAN_IN_TOA_THUOC or UPDATED_SO_LAN_IN_TOA_THUOC
        defaultThongTinKhamBenhShouldBeFound("soLanInToaThuoc.in=" + DEFAULT_SO_LAN_IN_TOA_THUOC + "," + UPDATED_SO_LAN_IN_TOA_THUOC);

        // Get all the thongTinKhamBenhList where soLanInToaThuoc equals to UPDATED_SO_LAN_IN_TOA_THUOC
        defaultThongTinKhamBenhShouldNotBeFound("soLanInToaThuoc.in=" + UPDATED_SO_LAN_IN_TOA_THUOC);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsBySoLanInToaThuocIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where soLanInToaThuoc is not null
        defaultThongTinKhamBenhShouldBeFound("soLanInToaThuoc.specified=true");

        // Get all the thongTinKhamBenhList where soLanInToaThuoc is null
        defaultThongTinKhamBenhShouldNotBeFound("soLanInToaThuoc.specified=false");
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsBySoLanInToaThuocIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where soLanInToaThuoc is greater than or equal to DEFAULT_SO_LAN_IN_TOA_THUOC
        defaultThongTinKhamBenhShouldBeFound("soLanInToaThuoc.greaterThanOrEqual=" + DEFAULT_SO_LAN_IN_TOA_THUOC);

        // Get all the thongTinKhamBenhList where soLanInToaThuoc is greater than or equal to UPDATED_SO_LAN_IN_TOA_THUOC
        defaultThongTinKhamBenhShouldNotBeFound("soLanInToaThuoc.greaterThanOrEqual=" + UPDATED_SO_LAN_IN_TOA_THUOC);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsBySoLanInToaThuocIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where soLanInToaThuoc is less than or equal to DEFAULT_SO_LAN_IN_TOA_THUOC
        defaultThongTinKhamBenhShouldBeFound("soLanInToaThuoc.lessThanOrEqual=" + DEFAULT_SO_LAN_IN_TOA_THUOC);

        // Get all the thongTinKhamBenhList where soLanInToaThuoc is less than or equal to SMALLER_SO_LAN_IN_TOA_THUOC
        defaultThongTinKhamBenhShouldNotBeFound("soLanInToaThuoc.lessThanOrEqual=" + SMALLER_SO_LAN_IN_TOA_THUOC);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsBySoLanInToaThuocIsLessThanSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where soLanInToaThuoc is less than DEFAULT_SO_LAN_IN_TOA_THUOC
        defaultThongTinKhamBenhShouldNotBeFound("soLanInToaThuoc.lessThan=" + DEFAULT_SO_LAN_IN_TOA_THUOC);

        // Get all the thongTinKhamBenhList where soLanInToaThuoc is less than UPDATED_SO_LAN_IN_TOA_THUOC
        defaultThongTinKhamBenhShouldBeFound("soLanInToaThuoc.lessThan=" + UPDATED_SO_LAN_IN_TOA_THUOC);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsBySoLanInToaThuocIsGreaterThanSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where soLanInToaThuoc is greater than DEFAULT_SO_LAN_IN_TOA_THUOC
        defaultThongTinKhamBenhShouldNotBeFound("soLanInToaThuoc.greaterThan=" + DEFAULT_SO_LAN_IN_TOA_THUOC);

        // Get all the thongTinKhamBenhList where soLanInToaThuoc is greater than SMALLER_SO_LAN_IN_TOA_THUOC
        defaultThongTinKhamBenhShouldBeFound("soLanInToaThuoc.greaterThan=" + SMALLER_SO_LAN_IN_TOA_THUOC);
    }


    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByTenBenhIcdIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where tenBenhIcd equals to DEFAULT_TEN_BENH_ICD
        defaultThongTinKhamBenhShouldBeFound("tenBenhIcd.equals=" + DEFAULT_TEN_BENH_ICD);

        // Get all the thongTinKhamBenhList where tenBenhIcd equals to UPDATED_TEN_BENH_ICD
        defaultThongTinKhamBenhShouldNotBeFound("tenBenhIcd.equals=" + UPDATED_TEN_BENH_ICD);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByTenBenhIcdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where tenBenhIcd not equals to DEFAULT_TEN_BENH_ICD
        defaultThongTinKhamBenhShouldNotBeFound("tenBenhIcd.notEquals=" + DEFAULT_TEN_BENH_ICD);

        // Get all the thongTinKhamBenhList where tenBenhIcd not equals to UPDATED_TEN_BENH_ICD
        defaultThongTinKhamBenhShouldBeFound("tenBenhIcd.notEquals=" + UPDATED_TEN_BENH_ICD);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByTenBenhIcdIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where tenBenhIcd in DEFAULT_TEN_BENH_ICD or UPDATED_TEN_BENH_ICD
        defaultThongTinKhamBenhShouldBeFound("tenBenhIcd.in=" + DEFAULT_TEN_BENH_ICD + "," + UPDATED_TEN_BENH_ICD);

        // Get all the thongTinKhamBenhList where tenBenhIcd equals to UPDATED_TEN_BENH_ICD
        defaultThongTinKhamBenhShouldNotBeFound("tenBenhIcd.in=" + UPDATED_TEN_BENH_ICD);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByTenBenhIcdIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where tenBenhIcd is not null
        defaultThongTinKhamBenhShouldBeFound("tenBenhIcd.specified=true");

        // Get all the thongTinKhamBenhList where tenBenhIcd is null
        defaultThongTinKhamBenhShouldNotBeFound("tenBenhIcd.specified=false");
    }
                @Test
    @Transactional
    public void getAllThongTinKhamBenhsByTenBenhIcdContainsSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where tenBenhIcd contains DEFAULT_TEN_BENH_ICD
        defaultThongTinKhamBenhShouldBeFound("tenBenhIcd.contains=" + DEFAULT_TEN_BENH_ICD);

        // Get all the thongTinKhamBenhList where tenBenhIcd contains UPDATED_TEN_BENH_ICD
        defaultThongTinKhamBenhShouldNotBeFound("tenBenhIcd.contains=" + UPDATED_TEN_BENH_ICD);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByTenBenhIcdNotContainsSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where tenBenhIcd does not contain DEFAULT_TEN_BENH_ICD
        defaultThongTinKhamBenhShouldNotBeFound("tenBenhIcd.doesNotContain=" + DEFAULT_TEN_BENH_ICD);

        // Get all the thongTinKhamBenhList where tenBenhIcd does not contain UPDATED_TEN_BENH_ICD
        defaultThongTinKhamBenhShouldBeFound("tenBenhIcd.doesNotContain=" + UPDATED_TEN_BENH_ICD);
    }


    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByTenBenhTheoBacSiIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where tenBenhTheoBacSi equals to DEFAULT_TEN_BENH_THEO_BAC_SI
        defaultThongTinKhamBenhShouldBeFound("tenBenhTheoBacSi.equals=" + DEFAULT_TEN_BENH_THEO_BAC_SI);

        // Get all the thongTinKhamBenhList where tenBenhTheoBacSi equals to UPDATED_TEN_BENH_THEO_BAC_SI
        defaultThongTinKhamBenhShouldNotBeFound("tenBenhTheoBacSi.equals=" + UPDATED_TEN_BENH_THEO_BAC_SI);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByTenBenhTheoBacSiIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where tenBenhTheoBacSi not equals to DEFAULT_TEN_BENH_THEO_BAC_SI
        defaultThongTinKhamBenhShouldNotBeFound("tenBenhTheoBacSi.notEquals=" + DEFAULT_TEN_BENH_THEO_BAC_SI);

        // Get all the thongTinKhamBenhList where tenBenhTheoBacSi not equals to UPDATED_TEN_BENH_THEO_BAC_SI
        defaultThongTinKhamBenhShouldBeFound("tenBenhTheoBacSi.notEquals=" + UPDATED_TEN_BENH_THEO_BAC_SI);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByTenBenhTheoBacSiIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where tenBenhTheoBacSi in DEFAULT_TEN_BENH_THEO_BAC_SI or UPDATED_TEN_BENH_THEO_BAC_SI
        defaultThongTinKhamBenhShouldBeFound("tenBenhTheoBacSi.in=" + DEFAULT_TEN_BENH_THEO_BAC_SI + "," + UPDATED_TEN_BENH_THEO_BAC_SI);

        // Get all the thongTinKhamBenhList where tenBenhTheoBacSi equals to UPDATED_TEN_BENH_THEO_BAC_SI
        defaultThongTinKhamBenhShouldNotBeFound("tenBenhTheoBacSi.in=" + UPDATED_TEN_BENH_THEO_BAC_SI);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByTenBenhTheoBacSiIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where tenBenhTheoBacSi is not null
        defaultThongTinKhamBenhShouldBeFound("tenBenhTheoBacSi.specified=true");

        // Get all the thongTinKhamBenhList where tenBenhTheoBacSi is null
        defaultThongTinKhamBenhShouldNotBeFound("tenBenhTheoBacSi.specified=false");
    }
                @Test
    @Transactional
    public void getAllThongTinKhamBenhsByTenBenhTheoBacSiContainsSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where tenBenhTheoBacSi contains DEFAULT_TEN_BENH_THEO_BAC_SI
        defaultThongTinKhamBenhShouldBeFound("tenBenhTheoBacSi.contains=" + DEFAULT_TEN_BENH_THEO_BAC_SI);

        // Get all the thongTinKhamBenhList where tenBenhTheoBacSi contains UPDATED_TEN_BENH_THEO_BAC_SI
        defaultThongTinKhamBenhShouldNotBeFound("tenBenhTheoBacSi.contains=" + UPDATED_TEN_BENH_THEO_BAC_SI);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByTenBenhTheoBacSiNotContainsSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where tenBenhTheoBacSi does not contain DEFAULT_TEN_BENH_THEO_BAC_SI
        defaultThongTinKhamBenhShouldNotBeFound("tenBenhTheoBacSi.doesNotContain=" + DEFAULT_TEN_BENH_THEO_BAC_SI);

        // Get all the thongTinKhamBenhList where tenBenhTheoBacSi does not contain UPDATED_TEN_BENH_THEO_BAC_SI
        defaultThongTinKhamBenhShouldBeFound("tenBenhTheoBacSi.doesNotContain=" + UPDATED_TEN_BENH_THEO_BAC_SI);
    }


    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByTenBenhYhctIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where tenBenhYhct equals to DEFAULT_TEN_BENH_YHCT
        defaultThongTinKhamBenhShouldBeFound("tenBenhYhct.equals=" + DEFAULT_TEN_BENH_YHCT);

        // Get all the thongTinKhamBenhList where tenBenhYhct equals to UPDATED_TEN_BENH_YHCT
        defaultThongTinKhamBenhShouldNotBeFound("tenBenhYhct.equals=" + UPDATED_TEN_BENH_YHCT);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByTenBenhYhctIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where tenBenhYhct not equals to DEFAULT_TEN_BENH_YHCT
        defaultThongTinKhamBenhShouldNotBeFound("tenBenhYhct.notEquals=" + DEFAULT_TEN_BENH_YHCT);

        // Get all the thongTinKhamBenhList where tenBenhYhct not equals to UPDATED_TEN_BENH_YHCT
        defaultThongTinKhamBenhShouldBeFound("tenBenhYhct.notEquals=" + UPDATED_TEN_BENH_YHCT);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByTenBenhYhctIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where tenBenhYhct in DEFAULT_TEN_BENH_YHCT or UPDATED_TEN_BENH_YHCT
        defaultThongTinKhamBenhShouldBeFound("tenBenhYhct.in=" + DEFAULT_TEN_BENH_YHCT + "," + UPDATED_TEN_BENH_YHCT);

        // Get all the thongTinKhamBenhList where tenBenhYhct equals to UPDATED_TEN_BENH_YHCT
        defaultThongTinKhamBenhShouldNotBeFound("tenBenhYhct.in=" + UPDATED_TEN_BENH_YHCT);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByTenBenhYhctIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where tenBenhYhct is not null
        defaultThongTinKhamBenhShouldBeFound("tenBenhYhct.specified=true");

        // Get all the thongTinKhamBenhList where tenBenhYhct is null
        defaultThongTinKhamBenhShouldNotBeFound("tenBenhYhct.specified=false");
    }
                @Test
    @Transactional
    public void getAllThongTinKhamBenhsByTenBenhYhctContainsSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where tenBenhYhct contains DEFAULT_TEN_BENH_YHCT
        defaultThongTinKhamBenhShouldBeFound("tenBenhYhct.contains=" + DEFAULT_TEN_BENH_YHCT);

        // Get all the thongTinKhamBenhList where tenBenhYhct contains UPDATED_TEN_BENH_YHCT
        defaultThongTinKhamBenhShouldNotBeFound("tenBenhYhct.contains=" + UPDATED_TEN_BENH_YHCT);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByTenBenhYhctNotContainsSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where tenBenhYhct does not contain DEFAULT_TEN_BENH_YHCT
        defaultThongTinKhamBenhShouldNotBeFound("tenBenhYhct.doesNotContain=" + DEFAULT_TEN_BENH_YHCT);

        // Get all the thongTinKhamBenhList where tenBenhYhct does not contain UPDATED_TEN_BENH_YHCT
        defaultThongTinKhamBenhShouldBeFound("tenBenhYhct.doesNotContain=" + UPDATED_TEN_BENH_YHCT);
    }


    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByTrangThaiCdhaIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where trangThaiCdha equals to DEFAULT_TRANG_THAI_CDHA
        defaultThongTinKhamBenhShouldBeFound("trangThaiCdha.equals=" + DEFAULT_TRANG_THAI_CDHA);

        // Get all the thongTinKhamBenhList where trangThaiCdha equals to UPDATED_TRANG_THAI_CDHA
        defaultThongTinKhamBenhShouldNotBeFound("trangThaiCdha.equals=" + UPDATED_TRANG_THAI_CDHA);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByTrangThaiCdhaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where trangThaiCdha not equals to DEFAULT_TRANG_THAI_CDHA
        defaultThongTinKhamBenhShouldNotBeFound("trangThaiCdha.notEquals=" + DEFAULT_TRANG_THAI_CDHA);

        // Get all the thongTinKhamBenhList where trangThaiCdha not equals to UPDATED_TRANG_THAI_CDHA
        defaultThongTinKhamBenhShouldBeFound("trangThaiCdha.notEquals=" + UPDATED_TRANG_THAI_CDHA);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByTrangThaiCdhaIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where trangThaiCdha in DEFAULT_TRANG_THAI_CDHA or UPDATED_TRANG_THAI_CDHA
        defaultThongTinKhamBenhShouldBeFound("trangThaiCdha.in=" + DEFAULT_TRANG_THAI_CDHA + "," + UPDATED_TRANG_THAI_CDHA);

        // Get all the thongTinKhamBenhList where trangThaiCdha equals to UPDATED_TRANG_THAI_CDHA
        defaultThongTinKhamBenhShouldNotBeFound("trangThaiCdha.in=" + UPDATED_TRANG_THAI_CDHA);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByTrangThaiCdhaIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where trangThaiCdha is not null
        defaultThongTinKhamBenhShouldBeFound("trangThaiCdha.specified=true");

        // Get all the thongTinKhamBenhList where trangThaiCdha is null
        defaultThongTinKhamBenhShouldNotBeFound("trangThaiCdha.specified=false");
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByTrangThaiTtptIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where trangThaiTtpt equals to DEFAULT_TRANG_THAI_TTPT
        defaultThongTinKhamBenhShouldBeFound("trangThaiTtpt.equals=" + DEFAULT_TRANG_THAI_TTPT);

        // Get all the thongTinKhamBenhList where trangThaiTtpt equals to UPDATED_TRANG_THAI_TTPT
        defaultThongTinKhamBenhShouldNotBeFound("trangThaiTtpt.equals=" + UPDATED_TRANG_THAI_TTPT);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByTrangThaiTtptIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where trangThaiTtpt not equals to DEFAULT_TRANG_THAI_TTPT
        defaultThongTinKhamBenhShouldNotBeFound("trangThaiTtpt.notEquals=" + DEFAULT_TRANG_THAI_TTPT);

        // Get all the thongTinKhamBenhList where trangThaiTtpt not equals to UPDATED_TRANG_THAI_TTPT
        defaultThongTinKhamBenhShouldBeFound("trangThaiTtpt.notEquals=" + UPDATED_TRANG_THAI_TTPT);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByTrangThaiTtptIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where trangThaiTtpt in DEFAULT_TRANG_THAI_TTPT or UPDATED_TRANG_THAI_TTPT
        defaultThongTinKhamBenhShouldBeFound("trangThaiTtpt.in=" + DEFAULT_TRANG_THAI_TTPT + "," + UPDATED_TRANG_THAI_TTPT);

        // Get all the thongTinKhamBenhList where trangThaiTtpt equals to UPDATED_TRANG_THAI_TTPT
        defaultThongTinKhamBenhShouldNotBeFound("trangThaiTtpt.in=" + UPDATED_TRANG_THAI_TTPT);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByTrangThaiTtptIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where trangThaiTtpt is not null
        defaultThongTinKhamBenhShouldBeFound("trangThaiTtpt.specified=true");

        // Get all the thongTinKhamBenhList where trangThaiTtpt is null
        defaultThongTinKhamBenhShouldNotBeFound("trangThaiTtpt.specified=false");
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByTrangThaiXetNghiemIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where trangThaiXetNghiem equals to DEFAULT_TRANG_THAI_XET_NGHIEM
        defaultThongTinKhamBenhShouldBeFound("trangThaiXetNghiem.equals=" + DEFAULT_TRANG_THAI_XET_NGHIEM);

        // Get all the thongTinKhamBenhList where trangThaiXetNghiem equals to UPDATED_TRANG_THAI_XET_NGHIEM
        defaultThongTinKhamBenhShouldNotBeFound("trangThaiXetNghiem.equals=" + UPDATED_TRANG_THAI_XET_NGHIEM);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByTrangThaiXetNghiemIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where trangThaiXetNghiem not equals to DEFAULT_TRANG_THAI_XET_NGHIEM
        defaultThongTinKhamBenhShouldNotBeFound("trangThaiXetNghiem.notEquals=" + DEFAULT_TRANG_THAI_XET_NGHIEM);

        // Get all the thongTinKhamBenhList where trangThaiXetNghiem not equals to UPDATED_TRANG_THAI_XET_NGHIEM
        defaultThongTinKhamBenhShouldBeFound("trangThaiXetNghiem.notEquals=" + UPDATED_TRANG_THAI_XET_NGHIEM);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByTrangThaiXetNghiemIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where trangThaiXetNghiem in DEFAULT_TRANG_THAI_XET_NGHIEM or UPDATED_TRANG_THAI_XET_NGHIEM
        defaultThongTinKhamBenhShouldBeFound("trangThaiXetNghiem.in=" + DEFAULT_TRANG_THAI_XET_NGHIEM + "," + UPDATED_TRANG_THAI_XET_NGHIEM);

        // Get all the thongTinKhamBenhList where trangThaiXetNghiem equals to UPDATED_TRANG_THAI_XET_NGHIEM
        defaultThongTinKhamBenhShouldNotBeFound("trangThaiXetNghiem.in=" + UPDATED_TRANG_THAI_XET_NGHIEM);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByTrangThaiXetNghiemIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where trangThaiXetNghiem is not null
        defaultThongTinKhamBenhShouldBeFound("trangThaiXetNghiem.specified=true");

        // Get all the thongTinKhamBenhList where trangThaiXetNghiem is null
        defaultThongTinKhamBenhShouldNotBeFound("trangThaiXetNghiem.specified=false");
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByTrieuChungLamSangIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where trieuChungLamSang equals to DEFAULT_TRIEU_CHUNG_LAM_SANG
        defaultThongTinKhamBenhShouldBeFound("trieuChungLamSang.equals=" + DEFAULT_TRIEU_CHUNG_LAM_SANG);

        // Get all the thongTinKhamBenhList where trieuChungLamSang equals to UPDATED_TRIEU_CHUNG_LAM_SANG
        defaultThongTinKhamBenhShouldNotBeFound("trieuChungLamSang.equals=" + UPDATED_TRIEU_CHUNG_LAM_SANG);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByTrieuChungLamSangIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where trieuChungLamSang not equals to DEFAULT_TRIEU_CHUNG_LAM_SANG
        defaultThongTinKhamBenhShouldNotBeFound("trieuChungLamSang.notEquals=" + DEFAULT_TRIEU_CHUNG_LAM_SANG);

        // Get all the thongTinKhamBenhList where trieuChungLamSang not equals to UPDATED_TRIEU_CHUNG_LAM_SANG
        defaultThongTinKhamBenhShouldBeFound("trieuChungLamSang.notEquals=" + UPDATED_TRIEU_CHUNG_LAM_SANG);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByTrieuChungLamSangIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where trieuChungLamSang in DEFAULT_TRIEU_CHUNG_LAM_SANG or UPDATED_TRIEU_CHUNG_LAM_SANG
        defaultThongTinKhamBenhShouldBeFound("trieuChungLamSang.in=" + DEFAULT_TRIEU_CHUNG_LAM_SANG + "," + UPDATED_TRIEU_CHUNG_LAM_SANG);

        // Get all the thongTinKhamBenhList where trieuChungLamSang equals to UPDATED_TRIEU_CHUNG_LAM_SANG
        defaultThongTinKhamBenhShouldNotBeFound("trieuChungLamSang.in=" + UPDATED_TRIEU_CHUNG_LAM_SANG);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByTrieuChungLamSangIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where trieuChungLamSang is not null
        defaultThongTinKhamBenhShouldBeFound("trieuChungLamSang.specified=true");

        // Get all the thongTinKhamBenhList where trieuChungLamSang is null
        defaultThongTinKhamBenhShouldNotBeFound("trieuChungLamSang.specified=false");
    }
                @Test
    @Transactional
    public void getAllThongTinKhamBenhsByTrieuChungLamSangContainsSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where trieuChungLamSang contains DEFAULT_TRIEU_CHUNG_LAM_SANG
        defaultThongTinKhamBenhShouldBeFound("trieuChungLamSang.contains=" + DEFAULT_TRIEU_CHUNG_LAM_SANG);

        // Get all the thongTinKhamBenhList where trieuChungLamSang contains UPDATED_TRIEU_CHUNG_LAM_SANG
        defaultThongTinKhamBenhShouldNotBeFound("trieuChungLamSang.contains=" + UPDATED_TRIEU_CHUNG_LAM_SANG);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByTrieuChungLamSangNotContainsSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where trieuChungLamSang does not contain DEFAULT_TRIEU_CHUNG_LAM_SANG
        defaultThongTinKhamBenhShouldNotBeFound("trieuChungLamSang.doesNotContain=" + DEFAULT_TRIEU_CHUNG_LAM_SANG);

        // Get all the thongTinKhamBenhList where trieuChungLamSang does not contain UPDATED_TRIEU_CHUNG_LAM_SANG
        defaultThongTinKhamBenhShouldBeFound("trieuChungLamSang.doesNotContain=" + UPDATED_TRIEU_CHUNG_LAM_SANG);
    }


    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByVongBungIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where vongBung equals to DEFAULT_VONG_BUNG
        defaultThongTinKhamBenhShouldBeFound("vongBung.equals=" + DEFAULT_VONG_BUNG);

        // Get all the thongTinKhamBenhList where vongBung equals to UPDATED_VONG_BUNG
        defaultThongTinKhamBenhShouldNotBeFound("vongBung.equals=" + UPDATED_VONG_BUNG);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByVongBungIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where vongBung not equals to DEFAULT_VONG_BUNG
        defaultThongTinKhamBenhShouldNotBeFound("vongBung.notEquals=" + DEFAULT_VONG_BUNG);

        // Get all the thongTinKhamBenhList where vongBung not equals to UPDATED_VONG_BUNG
        defaultThongTinKhamBenhShouldBeFound("vongBung.notEquals=" + UPDATED_VONG_BUNG);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByVongBungIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where vongBung in DEFAULT_VONG_BUNG or UPDATED_VONG_BUNG
        defaultThongTinKhamBenhShouldBeFound("vongBung.in=" + DEFAULT_VONG_BUNG + "," + UPDATED_VONG_BUNG);

        // Get all the thongTinKhamBenhList where vongBung equals to UPDATED_VONG_BUNG
        defaultThongTinKhamBenhShouldNotBeFound("vongBung.in=" + UPDATED_VONG_BUNG);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByVongBungIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where vongBung is not null
        defaultThongTinKhamBenhShouldBeFound("vongBung.specified=true");

        // Get all the thongTinKhamBenhList where vongBung is null
        defaultThongTinKhamBenhShouldNotBeFound("vongBung.specified=false");
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByVongBungIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where vongBung is greater than or equal to DEFAULT_VONG_BUNG
        defaultThongTinKhamBenhShouldBeFound("vongBung.greaterThanOrEqual=" + DEFAULT_VONG_BUNG);

        // Get all the thongTinKhamBenhList where vongBung is greater than or equal to UPDATED_VONG_BUNG
        defaultThongTinKhamBenhShouldNotBeFound("vongBung.greaterThanOrEqual=" + UPDATED_VONG_BUNG);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByVongBungIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where vongBung is less than or equal to DEFAULT_VONG_BUNG
        defaultThongTinKhamBenhShouldBeFound("vongBung.lessThanOrEqual=" + DEFAULT_VONG_BUNG);

        // Get all the thongTinKhamBenhList where vongBung is less than or equal to SMALLER_VONG_BUNG
        defaultThongTinKhamBenhShouldNotBeFound("vongBung.lessThanOrEqual=" + SMALLER_VONG_BUNG);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByVongBungIsLessThanSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where vongBung is less than DEFAULT_VONG_BUNG
        defaultThongTinKhamBenhShouldNotBeFound("vongBung.lessThan=" + DEFAULT_VONG_BUNG);

        // Get all the thongTinKhamBenhList where vongBung is less than UPDATED_VONG_BUNG
        defaultThongTinKhamBenhShouldBeFound("vongBung.lessThan=" + UPDATED_VONG_BUNG);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByVongBungIsGreaterThanSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where vongBung is greater than DEFAULT_VONG_BUNG
        defaultThongTinKhamBenhShouldNotBeFound("vongBung.greaterThan=" + DEFAULT_VONG_BUNG);

        // Get all the thongTinKhamBenhList where vongBung is greater than SMALLER_VONG_BUNG
        defaultThongTinKhamBenhShouldBeFound("vongBung.greaterThan=" + SMALLER_VONG_BUNG);
    }


    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByTrangThaiIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where trangThai equals to DEFAULT_TRANG_THAI
        defaultThongTinKhamBenhShouldBeFound("trangThai.equals=" + DEFAULT_TRANG_THAI);

        // Get all the thongTinKhamBenhList where trangThai equals to UPDATED_TRANG_THAI
        defaultThongTinKhamBenhShouldNotBeFound("trangThai.equals=" + UPDATED_TRANG_THAI);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByTrangThaiIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where trangThai not equals to DEFAULT_TRANG_THAI
        defaultThongTinKhamBenhShouldNotBeFound("trangThai.notEquals=" + DEFAULT_TRANG_THAI);

        // Get all the thongTinKhamBenhList where trangThai not equals to UPDATED_TRANG_THAI
        defaultThongTinKhamBenhShouldBeFound("trangThai.notEquals=" + UPDATED_TRANG_THAI);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByTrangThaiIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where trangThai in DEFAULT_TRANG_THAI or UPDATED_TRANG_THAI
        defaultThongTinKhamBenhShouldBeFound("trangThai.in=" + DEFAULT_TRANG_THAI + "," + UPDATED_TRANG_THAI);

        // Get all the thongTinKhamBenhList where trangThai equals to UPDATED_TRANG_THAI
        defaultThongTinKhamBenhShouldNotBeFound("trangThai.in=" + UPDATED_TRANG_THAI);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByTrangThaiIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where trangThai is not null
        defaultThongTinKhamBenhShouldBeFound("trangThai.specified=true");

        // Get all the thongTinKhamBenhList where trangThai is null
        defaultThongTinKhamBenhShouldNotBeFound("trangThai.specified=false");
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByTrangThaiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where trangThai is greater than or equal to DEFAULT_TRANG_THAI
        defaultThongTinKhamBenhShouldBeFound("trangThai.greaterThanOrEqual=" + DEFAULT_TRANG_THAI);

        // Get all the thongTinKhamBenhList where trangThai is greater than or equal to UPDATED_TRANG_THAI
        defaultThongTinKhamBenhShouldNotBeFound("trangThai.greaterThanOrEqual=" + UPDATED_TRANG_THAI);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByTrangThaiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where trangThai is less than or equal to DEFAULT_TRANG_THAI
        defaultThongTinKhamBenhShouldBeFound("trangThai.lessThanOrEqual=" + DEFAULT_TRANG_THAI);

        // Get all the thongTinKhamBenhList where trangThai is less than or equal to SMALLER_TRANG_THAI
        defaultThongTinKhamBenhShouldNotBeFound("trangThai.lessThanOrEqual=" + SMALLER_TRANG_THAI);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByTrangThaiIsLessThanSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where trangThai is less than DEFAULT_TRANG_THAI
        defaultThongTinKhamBenhShouldNotBeFound("trangThai.lessThan=" + DEFAULT_TRANG_THAI);

        // Get all the thongTinKhamBenhList where trangThai is less than UPDATED_TRANG_THAI
        defaultThongTinKhamBenhShouldBeFound("trangThai.lessThan=" + UPDATED_TRANG_THAI);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByTrangThaiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where trangThai is greater than DEFAULT_TRANG_THAI
        defaultThongTinKhamBenhShouldNotBeFound("trangThai.greaterThan=" + DEFAULT_TRANG_THAI);

        // Get all the thongTinKhamBenhList where trangThai is greater than SMALLER_TRANG_THAI
        defaultThongTinKhamBenhShouldBeFound("trangThai.greaterThan=" + SMALLER_TRANG_THAI);
    }


    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByThoiGianKetThucKhamIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where thoiGianKetThucKham equals to DEFAULT_THOI_GIAN_KET_THUC_KHAM
        defaultThongTinKhamBenhShouldBeFound("thoiGianKetThucKham.equals=" + DEFAULT_THOI_GIAN_KET_THUC_KHAM);

        // Get all the thongTinKhamBenhList where thoiGianKetThucKham equals to UPDATED_THOI_GIAN_KET_THUC_KHAM
        defaultThongTinKhamBenhShouldNotBeFound("thoiGianKetThucKham.equals=" + UPDATED_THOI_GIAN_KET_THUC_KHAM);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByThoiGianKetThucKhamIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where thoiGianKetThucKham not equals to DEFAULT_THOI_GIAN_KET_THUC_KHAM
        defaultThongTinKhamBenhShouldNotBeFound("thoiGianKetThucKham.notEquals=" + DEFAULT_THOI_GIAN_KET_THUC_KHAM);

        // Get all the thongTinKhamBenhList where thoiGianKetThucKham not equals to UPDATED_THOI_GIAN_KET_THUC_KHAM
        defaultThongTinKhamBenhShouldBeFound("thoiGianKetThucKham.notEquals=" + UPDATED_THOI_GIAN_KET_THUC_KHAM);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByThoiGianKetThucKhamIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where thoiGianKetThucKham in DEFAULT_THOI_GIAN_KET_THUC_KHAM or UPDATED_THOI_GIAN_KET_THUC_KHAM
        defaultThongTinKhamBenhShouldBeFound("thoiGianKetThucKham.in=" + DEFAULT_THOI_GIAN_KET_THUC_KHAM + "," + UPDATED_THOI_GIAN_KET_THUC_KHAM);

        // Get all the thongTinKhamBenhList where thoiGianKetThucKham equals to UPDATED_THOI_GIAN_KET_THUC_KHAM
        defaultThongTinKhamBenhShouldNotBeFound("thoiGianKetThucKham.in=" + UPDATED_THOI_GIAN_KET_THUC_KHAM);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByThoiGianKetThucKhamIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where thoiGianKetThucKham is not null
        defaultThongTinKhamBenhShouldBeFound("thoiGianKetThucKham.specified=true");

        // Get all the thongTinKhamBenhList where thoiGianKetThucKham is null
        defaultThongTinKhamBenhShouldNotBeFound("thoiGianKetThucKham.specified=false");
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByThoiGianKetThucKhamIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where thoiGianKetThucKham is greater than or equal to DEFAULT_THOI_GIAN_KET_THUC_KHAM
        defaultThongTinKhamBenhShouldBeFound("thoiGianKetThucKham.greaterThanOrEqual=" + DEFAULT_THOI_GIAN_KET_THUC_KHAM);

        // Get all the thongTinKhamBenhList where thoiGianKetThucKham is greater than or equal to UPDATED_THOI_GIAN_KET_THUC_KHAM
        defaultThongTinKhamBenhShouldNotBeFound("thoiGianKetThucKham.greaterThanOrEqual=" + UPDATED_THOI_GIAN_KET_THUC_KHAM);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByThoiGianKetThucKhamIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where thoiGianKetThucKham is less than or equal to DEFAULT_THOI_GIAN_KET_THUC_KHAM
        defaultThongTinKhamBenhShouldBeFound("thoiGianKetThucKham.lessThanOrEqual=" + DEFAULT_THOI_GIAN_KET_THUC_KHAM);

        // Get all the thongTinKhamBenhList where thoiGianKetThucKham is less than or equal to SMALLER_THOI_GIAN_KET_THUC_KHAM
        defaultThongTinKhamBenhShouldNotBeFound("thoiGianKetThucKham.lessThanOrEqual=" + SMALLER_THOI_GIAN_KET_THUC_KHAM);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByThoiGianKetThucKhamIsLessThanSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where thoiGianKetThucKham is less than DEFAULT_THOI_GIAN_KET_THUC_KHAM
        defaultThongTinKhamBenhShouldNotBeFound("thoiGianKetThucKham.lessThan=" + DEFAULT_THOI_GIAN_KET_THUC_KHAM);

        // Get all the thongTinKhamBenhList where thoiGianKetThucKham is less than UPDATED_THOI_GIAN_KET_THUC_KHAM
        defaultThongTinKhamBenhShouldBeFound("thoiGianKetThucKham.lessThan=" + UPDATED_THOI_GIAN_KET_THUC_KHAM);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByThoiGianKetThucKhamIsGreaterThanSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where thoiGianKetThucKham is greater than DEFAULT_THOI_GIAN_KET_THUC_KHAM
        defaultThongTinKhamBenhShouldNotBeFound("thoiGianKetThucKham.greaterThan=" + DEFAULT_THOI_GIAN_KET_THUC_KHAM);

        // Get all the thongTinKhamBenhList where thoiGianKetThucKham is greater than SMALLER_THOI_GIAN_KET_THUC_KHAM
        defaultThongTinKhamBenhShouldBeFound("thoiGianKetThucKham.greaterThan=" + SMALLER_THOI_GIAN_KET_THUC_KHAM);
    }


    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByPhongIdChuyenTuIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where phongIdChuyenTu equals to DEFAULT_PHONG_ID_CHUYEN_TU
        defaultThongTinKhamBenhShouldBeFound("phongIdChuyenTu.equals=" + DEFAULT_PHONG_ID_CHUYEN_TU);

        // Get all the thongTinKhamBenhList where phongIdChuyenTu equals to UPDATED_PHONG_ID_CHUYEN_TU
        defaultThongTinKhamBenhShouldNotBeFound("phongIdChuyenTu.equals=" + UPDATED_PHONG_ID_CHUYEN_TU);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByPhongIdChuyenTuIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where phongIdChuyenTu not equals to DEFAULT_PHONG_ID_CHUYEN_TU
        defaultThongTinKhamBenhShouldNotBeFound("phongIdChuyenTu.notEquals=" + DEFAULT_PHONG_ID_CHUYEN_TU);

        // Get all the thongTinKhamBenhList where phongIdChuyenTu not equals to UPDATED_PHONG_ID_CHUYEN_TU
        defaultThongTinKhamBenhShouldBeFound("phongIdChuyenTu.notEquals=" + UPDATED_PHONG_ID_CHUYEN_TU);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByPhongIdChuyenTuIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where phongIdChuyenTu in DEFAULT_PHONG_ID_CHUYEN_TU or UPDATED_PHONG_ID_CHUYEN_TU
        defaultThongTinKhamBenhShouldBeFound("phongIdChuyenTu.in=" + DEFAULT_PHONG_ID_CHUYEN_TU + "," + UPDATED_PHONG_ID_CHUYEN_TU);

        // Get all the thongTinKhamBenhList where phongIdChuyenTu equals to UPDATED_PHONG_ID_CHUYEN_TU
        defaultThongTinKhamBenhShouldNotBeFound("phongIdChuyenTu.in=" + UPDATED_PHONG_ID_CHUYEN_TU);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByPhongIdChuyenTuIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where phongIdChuyenTu is not null
        defaultThongTinKhamBenhShouldBeFound("phongIdChuyenTu.specified=true");

        // Get all the thongTinKhamBenhList where phongIdChuyenTu is null
        defaultThongTinKhamBenhShouldNotBeFound("phongIdChuyenTu.specified=false");
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByPhongIdChuyenTuIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where phongIdChuyenTu is greater than or equal to DEFAULT_PHONG_ID_CHUYEN_TU
        defaultThongTinKhamBenhShouldBeFound("phongIdChuyenTu.greaterThanOrEqual=" + DEFAULT_PHONG_ID_CHUYEN_TU);

        // Get all the thongTinKhamBenhList where phongIdChuyenTu is greater than or equal to UPDATED_PHONG_ID_CHUYEN_TU
        defaultThongTinKhamBenhShouldNotBeFound("phongIdChuyenTu.greaterThanOrEqual=" + UPDATED_PHONG_ID_CHUYEN_TU);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByPhongIdChuyenTuIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where phongIdChuyenTu is less than or equal to DEFAULT_PHONG_ID_CHUYEN_TU
        defaultThongTinKhamBenhShouldBeFound("phongIdChuyenTu.lessThanOrEqual=" + DEFAULT_PHONG_ID_CHUYEN_TU);

        // Get all the thongTinKhamBenhList where phongIdChuyenTu is less than or equal to SMALLER_PHONG_ID_CHUYEN_TU
        defaultThongTinKhamBenhShouldNotBeFound("phongIdChuyenTu.lessThanOrEqual=" + SMALLER_PHONG_ID_CHUYEN_TU);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByPhongIdChuyenTuIsLessThanSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where phongIdChuyenTu is less than DEFAULT_PHONG_ID_CHUYEN_TU
        defaultThongTinKhamBenhShouldNotBeFound("phongIdChuyenTu.lessThan=" + DEFAULT_PHONG_ID_CHUYEN_TU);

        // Get all the thongTinKhamBenhList where phongIdChuyenTu is less than UPDATED_PHONG_ID_CHUYEN_TU
        defaultThongTinKhamBenhShouldBeFound("phongIdChuyenTu.lessThan=" + UPDATED_PHONG_ID_CHUYEN_TU);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByPhongIdChuyenTuIsGreaterThanSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where phongIdChuyenTu is greater than DEFAULT_PHONG_ID_CHUYEN_TU
        defaultThongTinKhamBenhShouldNotBeFound("phongIdChuyenTu.greaterThan=" + DEFAULT_PHONG_ID_CHUYEN_TU);

        // Get all the thongTinKhamBenhList where phongIdChuyenTu is greater than SMALLER_PHONG_ID_CHUYEN_TU
        defaultThongTinKhamBenhShouldBeFound("phongIdChuyenTu.greaterThan=" + SMALLER_PHONG_ID_CHUYEN_TU);
    }


    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByThoiGianKhamBenhIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where thoiGianKhamBenh equals to DEFAULT_THOI_GIAN_KHAM_BENH
        defaultThongTinKhamBenhShouldBeFound("thoiGianKhamBenh.equals=" + DEFAULT_THOI_GIAN_KHAM_BENH);

        // Get all the thongTinKhamBenhList where thoiGianKhamBenh equals to UPDATED_THOI_GIAN_KHAM_BENH
        defaultThongTinKhamBenhShouldNotBeFound("thoiGianKhamBenh.equals=" + UPDATED_THOI_GIAN_KHAM_BENH);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByThoiGianKhamBenhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where thoiGianKhamBenh not equals to DEFAULT_THOI_GIAN_KHAM_BENH
        defaultThongTinKhamBenhShouldNotBeFound("thoiGianKhamBenh.notEquals=" + DEFAULT_THOI_GIAN_KHAM_BENH);

        // Get all the thongTinKhamBenhList where thoiGianKhamBenh not equals to UPDATED_THOI_GIAN_KHAM_BENH
        defaultThongTinKhamBenhShouldBeFound("thoiGianKhamBenh.notEquals=" + UPDATED_THOI_GIAN_KHAM_BENH);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByThoiGianKhamBenhIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where thoiGianKhamBenh in DEFAULT_THOI_GIAN_KHAM_BENH or UPDATED_THOI_GIAN_KHAM_BENH
        defaultThongTinKhamBenhShouldBeFound("thoiGianKhamBenh.in=" + DEFAULT_THOI_GIAN_KHAM_BENH + "," + UPDATED_THOI_GIAN_KHAM_BENH);

        // Get all the thongTinKhamBenhList where thoiGianKhamBenh equals to UPDATED_THOI_GIAN_KHAM_BENH
        defaultThongTinKhamBenhShouldNotBeFound("thoiGianKhamBenh.in=" + UPDATED_THOI_GIAN_KHAM_BENH);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByThoiGianKhamBenhIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where thoiGianKhamBenh is not null
        defaultThongTinKhamBenhShouldBeFound("thoiGianKhamBenh.specified=true");

        // Get all the thongTinKhamBenhList where thoiGianKhamBenh is null
        defaultThongTinKhamBenhShouldNotBeFound("thoiGianKhamBenh.specified=false");
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByThoiGianKhamBenhIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where thoiGianKhamBenh is greater than or equal to DEFAULT_THOI_GIAN_KHAM_BENH
        defaultThongTinKhamBenhShouldBeFound("thoiGianKhamBenh.greaterThanOrEqual=" + DEFAULT_THOI_GIAN_KHAM_BENH);

        // Get all the thongTinKhamBenhList where thoiGianKhamBenh is greater than or equal to UPDATED_THOI_GIAN_KHAM_BENH
        defaultThongTinKhamBenhShouldNotBeFound("thoiGianKhamBenh.greaterThanOrEqual=" + UPDATED_THOI_GIAN_KHAM_BENH);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByThoiGianKhamBenhIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where thoiGianKhamBenh is less than or equal to DEFAULT_THOI_GIAN_KHAM_BENH
        defaultThongTinKhamBenhShouldBeFound("thoiGianKhamBenh.lessThanOrEqual=" + DEFAULT_THOI_GIAN_KHAM_BENH);

        // Get all the thongTinKhamBenhList where thoiGianKhamBenh is less than or equal to SMALLER_THOI_GIAN_KHAM_BENH
        defaultThongTinKhamBenhShouldNotBeFound("thoiGianKhamBenh.lessThanOrEqual=" + SMALLER_THOI_GIAN_KHAM_BENH);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByThoiGianKhamBenhIsLessThanSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where thoiGianKhamBenh is less than DEFAULT_THOI_GIAN_KHAM_BENH
        defaultThongTinKhamBenhShouldNotBeFound("thoiGianKhamBenh.lessThan=" + DEFAULT_THOI_GIAN_KHAM_BENH);

        // Get all the thongTinKhamBenhList where thoiGianKhamBenh is less than UPDATED_THOI_GIAN_KHAM_BENH
        defaultThongTinKhamBenhShouldBeFound("thoiGianKhamBenh.lessThan=" + UPDATED_THOI_GIAN_KHAM_BENH);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByThoiGianKhamBenhIsGreaterThanSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where thoiGianKhamBenh is greater than DEFAULT_THOI_GIAN_KHAM_BENH
        defaultThongTinKhamBenhShouldNotBeFound("thoiGianKhamBenh.greaterThan=" + DEFAULT_THOI_GIAN_KHAM_BENH);

        // Get all the thongTinKhamBenhList where thoiGianKhamBenh is greater than SMALLER_THOI_GIAN_KHAM_BENH
        defaultThongTinKhamBenhShouldBeFound("thoiGianKhamBenh.greaterThan=" + SMALLER_THOI_GIAN_KHAM_BENH);
    }


    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByyLenhIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where yLenh equals to DEFAULT_Y_LENH
        defaultThongTinKhamBenhShouldBeFound("yLenh.equals=" + DEFAULT_Y_LENH);

        // Get all the thongTinKhamBenhList where yLenh equals to UPDATED_Y_LENH
        defaultThongTinKhamBenhShouldNotBeFound("yLenh.equals=" + UPDATED_Y_LENH);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByyLenhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where yLenh not equals to DEFAULT_Y_LENH
        defaultThongTinKhamBenhShouldNotBeFound("yLenh.notEquals=" + DEFAULT_Y_LENH);

        // Get all the thongTinKhamBenhList where yLenh not equals to UPDATED_Y_LENH
        defaultThongTinKhamBenhShouldBeFound("yLenh.notEquals=" + UPDATED_Y_LENH);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByyLenhIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where yLenh in DEFAULT_Y_LENH or UPDATED_Y_LENH
        defaultThongTinKhamBenhShouldBeFound("yLenh.in=" + DEFAULT_Y_LENH + "," + UPDATED_Y_LENH);

        // Get all the thongTinKhamBenhList where yLenh equals to UPDATED_Y_LENH
        defaultThongTinKhamBenhShouldNotBeFound("yLenh.in=" + UPDATED_Y_LENH);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByyLenhIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where yLenh is not null
        defaultThongTinKhamBenhShouldBeFound("yLenh.specified=true");

        // Get all the thongTinKhamBenhList where yLenh is null
        defaultThongTinKhamBenhShouldNotBeFound("yLenh.specified=false");
    }
                @Test
    @Transactional
    public void getAllThongTinKhamBenhsByyLenhContainsSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where yLenh contains DEFAULT_Y_LENH
        defaultThongTinKhamBenhShouldBeFound("yLenh.contains=" + DEFAULT_Y_LENH);

        // Get all the thongTinKhamBenhList where yLenh contains UPDATED_Y_LENH
        defaultThongTinKhamBenhShouldNotBeFound("yLenh.contains=" + UPDATED_Y_LENH);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByyLenhNotContainsSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where yLenh does not contain DEFAULT_Y_LENH
        defaultThongTinKhamBenhShouldNotBeFound("yLenh.doesNotContain=" + DEFAULT_Y_LENH);

        // Get all the thongTinKhamBenhList where yLenh does not contain UPDATED_Y_LENH
        defaultThongTinKhamBenhShouldBeFound("yLenh.doesNotContain=" + UPDATED_Y_LENH);
    }


    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByLoaiYLenhIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where loaiYLenh equals to DEFAULT_LOAI_Y_LENH
        defaultThongTinKhamBenhShouldBeFound("loaiYLenh.equals=" + DEFAULT_LOAI_Y_LENH);

        // Get all the thongTinKhamBenhList where loaiYLenh equals to UPDATED_LOAI_Y_LENH
        defaultThongTinKhamBenhShouldNotBeFound("loaiYLenh.equals=" + UPDATED_LOAI_Y_LENH);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByLoaiYLenhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where loaiYLenh not equals to DEFAULT_LOAI_Y_LENH
        defaultThongTinKhamBenhShouldNotBeFound("loaiYLenh.notEquals=" + DEFAULT_LOAI_Y_LENH);

        // Get all the thongTinKhamBenhList where loaiYLenh not equals to UPDATED_LOAI_Y_LENH
        defaultThongTinKhamBenhShouldBeFound("loaiYLenh.notEquals=" + UPDATED_LOAI_Y_LENH);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByLoaiYLenhIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where loaiYLenh in DEFAULT_LOAI_Y_LENH or UPDATED_LOAI_Y_LENH
        defaultThongTinKhamBenhShouldBeFound("loaiYLenh.in=" + DEFAULT_LOAI_Y_LENH + "," + UPDATED_LOAI_Y_LENH);

        // Get all the thongTinKhamBenhList where loaiYLenh equals to UPDATED_LOAI_Y_LENH
        defaultThongTinKhamBenhShouldNotBeFound("loaiYLenh.in=" + UPDATED_LOAI_Y_LENH);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByLoaiYLenhIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where loaiYLenh is not null
        defaultThongTinKhamBenhShouldBeFound("loaiYLenh.specified=true");

        // Get all the thongTinKhamBenhList where loaiYLenh is null
        defaultThongTinKhamBenhShouldNotBeFound("loaiYLenh.specified=false");
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByLoaiYLenhIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where loaiYLenh is greater than or equal to DEFAULT_LOAI_Y_LENH
        defaultThongTinKhamBenhShouldBeFound("loaiYLenh.greaterThanOrEqual=" + DEFAULT_LOAI_Y_LENH);

        // Get all the thongTinKhamBenhList where loaiYLenh is greater than or equal to UPDATED_LOAI_Y_LENH
        defaultThongTinKhamBenhShouldNotBeFound("loaiYLenh.greaterThanOrEqual=" + UPDATED_LOAI_Y_LENH);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByLoaiYLenhIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where loaiYLenh is less than or equal to DEFAULT_LOAI_Y_LENH
        defaultThongTinKhamBenhShouldBeFound("loaiYLenh.lessThanOrEqual=" + DEFAULT_LOAI_Y_LENH);

        // Get all the thongTinKhamBenhList where loaiYLenh is less than or equal to SMALLER_LOAI_Y_LENH
        defaultThongTinKhamBenhShouldNotBeFound("loaiYLenh.lessThanOrEqual=" + SMALLER_LOAI_Y_LENH);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByLoaiYLenhIsLessThanSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where loaiYLenh is less than DEFAULT_LOAI_Y_LENH
        defaultThongTinKhamBenhShouldNotBeFound("loaiYLenh.lessThan=" + DEFAULT_LOAI_Y_LENH);

        // Get all the thongTinKhamBenhList where loaiYLenh is less than UPDATED_LOAI_Y_LENH
        defaultThongTinKhamBenhShouldBeFound("loaiYLenh.lessThan=" + UPDATED_LOAI_Y_LENH);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByLoaiYLenhIsGreaterThanSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where loaiYLenh is greater than DEFAULT_LOAI_Y_LENH
        defaultThongTinKhamBenhShouldNotBeFound("loaiYLenh.greaterThan=" + DEFAULT_LOAI_Y_LENH);

        // Get all the thongTinKhamBenhList where loaiYLenh is greater than SMALLER_LOAI_Y_LENH
        defaultThongTinKhamBenhShouldBeFound("loaiYLenh.greaterThan=" + SMALLER_LOAI_Y_LENH);
    }


    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByChanDoanIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where chanDoan equals to DEFAULT_CHAN_DOAN
        defaultThongTinKhamBenhShouldBeFound("chanDoan.equals=" + DEFAULT_CHAN_DOAN);

        // Get all the thongTinKhamBenhList where chanDoan equals to UPDATED_CHAN_DOAN
        defaultThongTinKhamBenhShouldNotBeFound("chanDoan.equals=" + UPDATED_CHAN_DOAN);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByChanDoanIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where chanDoan not equals to DEFAULT_CHAN_DOAN
        defaultThongTinKhamBenhShouldNotBeFound("chanDoan.notEquals=" + DEFAULT_CHAN_DOAN);

        // Get all the thongTinKhamBenhList where chanDoan not equals to UPDATED_CHAN_DOAN
        defaultThongTinKhamBenhShouldBeFound("chanDoan.notEquals=" + UPDATED_CHAN_DOAN);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByChanDoanIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where chanDoan in DEFAULT_CHAN_DOAN or UPDATED_CHAN_DOAN
        defaultThongTinKhamBenhShouldBeFound("chanDoan.in=" + DEFAULT_CHAN_DOAN + "," + UPDATED_CHAN_DOAN);

        // Get all the thongTinKhamBenhList where chanDoan equals to UPDATED_CHAN_DOAN
        defaultThongTinKhamBenhShouldNotBeFound("chanDoan.in=" + UPDATED_CHAN_DOAN);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByChanDoanIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where chanDoan is not null
        defaultThongTinKhamBenhShouldBeFound("chanDoan.specified=true");

        // Get all the thongTinKhamBenhList where chanDoan is null
        defaultThongTinKhamBenhShouldNotBeFound("chanDoan.specified=false");
    }
                @Test
    @Transactional
    public void getAllThongTinKhamBenhsByChanDoanContainsSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where chanDoan contains DEFAULT_CHAN_DOAN
        defaultThongTinKhamBenhShouldBeFound("chanDoan.contains=" + DEFAULT_CHAN_DOAN);

        // Get all the thongTinKhamBenhList where chanDoan contains UPDATED_CHAN_DOAN
        defaultThongTinKhamBenhShouldNotBeFound("chanDoan.contains=" + UPDATED_CHAN_DOAN);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByChanDoanNotContainsSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where chanDoan does not contain DEFAULT_CHAN_DOAN
        defaultThongTinKhamBenhShouldNotBeFound("chanDoan.doesNotContain=" + DEFAULT_CHAN_DOAN);

        // Get all the thongTinKhamBenhList where chanDoan does not contain UPDATED_CHAN_DOAN
        defaultThongTinKhamBenhShouldBeFound("chanDoan.doesNotContain=" + UPDATED_CHAN_DOAN);
    }


    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByLoaiChanDoanIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where loaiChanDoan equals to DEFAULT_LOAI_CHAN_DOAN
        defaultThongTinKhamBenhShouldBeFound("loaiChanDoan.equals=" + DEFAULT_LOAI_CHAN_DOAN);

        // Get all the thongTinKhamBenhList where loaiChanDoan equals to UPDATED_LOAI_CHAN_DOAN
        defaultThongTinKhamBenhShouldNotBeFound("loaiChanDoan.equals=" + UPDATED_LOAI_CHAN_DOAN);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByLoaiChanDoanIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where loaiChanDoan not equals to DEFAULT_LOAI_CHAN_DOAN
        defaultThongTinKhamBenhShouldNotBeFound("loaiChanDoan.notEquals=" + DEFAULT_LOAI_CHAN_DOAN);

        // Get all the thongTinKhamBenhList where loaiChanDoan not equals to UPDATED_LOAI_CHAN_DOAN
        defaultThongTinKhamBenhShouldBeFound("loaiChanDoan.notEquals=" + UPDATED_LOAI_CHAN_DOAN);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByLoaiChanDoanIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where loaiChanDoan in DEFAULT_LOAI_CHAN_DOAN or UPDATED_LOAI_CHAN_DOAN
        defaultThongTinKhamBenhShouldBeFound("loaiChanDoan.in=" + DEFAULT_LOAI_CHAN_DOAN + "," + UPDATED_LOAI_CHAN_DOAN);

        // Get all the thongTinKhamBenhList where loaiChanDoan equals to UPDATED_LOAI_CHAN_DOAN
        defaultThongTinKhamBenhShouldNotBeFound("loaiChanDoan.in=" + UPDATED_LOAI_CHAN_DOAN);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByLoaiChanDoanIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where loaiChanDoan is not null
        defaultThongTinKhamBenhShouldBeFound("loaiChanDoan.specified=true");

        // Get all the thongTinKhamBenhList where loaiChanDoan is null
        defaultThongTinKhamBenhShouldNotBeFound("loaiChanDoan.specified=false");
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByLoaiChanDoanIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where loaiChanDoan is greater than or equal to DEFAULT_LOAI_CHAN_DOAN
        defaultThongTinKhamBenhShouldBeFound("loaiChanDoan.greaterThanOrEqual=" + DEFAULT_LOAI_CHAN_DOAN);

        // Get all the thongTinKhamBenhList where loaiChanDoan is greater than or equal to UPDATED_LOAI_CHAN_DOAN
        defaultThongTinKhamBenhShouldNotBeFound("loaiChanDoan.greaterThanOrEqual=" + UPDATED_LOAI_CHAN_DOAN);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByLoaiChanDoanIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where loaiChanDoan is less than or equal to DEFAULT_LOAI_CHAN_DOAN
        defaultThongTinKhamBenhShouldBeFound("loaiChanDoan.lessThanOrEqual=" + DEFAULT_LOAI_CHAN_DOAN);

        // Get all the thongTinKhamBenhList where loaiChanDoan is less than or equal to SMALLER_LOAI_CHAN_DOAN
        defaultThongTinKhamBenhShouldNotBeFound("loaiChanDoan.lessThanOrEqual=" + SMALLER_LOAI_CHAN_DOAN);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByLoaiChanDoanIsLessThanSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where loaiChanDoan is less than DEFAULT_LOAI_CHAN_DOAN
        defaultThongTinKhamBenhShouldNotBeFound("loaiChanDoan.lessThan=" + DEFAULT_LOAI_CHAN_DOAN);

        // Get all the thongTinKhamBenhList where loaiChanDoan is less than UPDATED_LOAI_CHAN_DOAN
        defaultThongTinKhamBenhShouldBeFound("loaiChanDoan.lessThan=" + UPDATED_LOAI_CHAN_DOAN);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByLoaiChanDoanIsGreaterThanSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where loaiChanDoan is greater than DEFAULT_LOAI_CHAN_DOAN
        defaultThongTinKhamBenhShouldNotBeFound("loaiChanDoan.greaterThan=" + DEFAULT_LOAI_CHAN_DOAN);

        // Get all the thongTinKhamBenhList where loaiChanDoan is greater than SMALLER_LOAI_CHAN_DOAN
        defaultThongTinKhamBenhShouldBeFound("loaiChanDoan.greaterThan=" + SMALLER_LOAI_CHAN_DOAN);
    }


    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByNamIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where nam equals to DEFAULT_NAM
        defaultThongTinKhamBenhShouldBeFound("nam.equals=" + DEFAULT_NAM);

        // Get all the thongTinKhamBenhList where nam equals to UPDATED_NAM
        defaultThongTinKhamBenhShouldNotBeFound("nam.equals=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByNamIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where nam not equals to DEFAULT_NAM
        defaultThongTinKhamBenhShouldNotBeFound("nam.notEquals=" + DEFAULT_NAM);

        // Get all the thongTinKhamBenhList where nam not equals to UPDATED_NAM
        defaultThongTinKhamBenhShouldBeFound("nam.notEquals=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByNamIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where nam in DEFAULT_NAM or UPDATED_NAM
        defaultThongTinKhamBenhShouldBeFound("nam.in=" + DEFAULT_NAM + "," + UPDATED_NAM);

        // Get all the thongTinKhamBenhList where nam equals to UPDATED_NAM
        defaultThongTinKhamBenhShouldNotBeFound("nam.in=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByNamIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where nam is not null
        defaultThongTinKhamBenhShouldBeFound("nam.specified=true");

        // Get all the thongTinKhamBenhList where nam is null
        defaultThongTinKhamBenhShouldNotBeFound("nam.specified=false");
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByNamIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where nam is greater than or equal to DEFAULT_NAM
        defaultThongTinKhamBenhShouldBeFound("nam.greaterThanOrEqual=" + DEFAULT_NAM);

        // Get all the thongTinKhamBenhList where nam is greater than or equal to UPDATED_NAM
        defaultThongTinKhamBenhShouldNotBeFound("nam.greaterThanOrEqual=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByNamIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where nam is less than or equal to DEFAULT_NAM
        defaultThongTinKhamBenhShouldBeFound("nam.lessThanOrEqual=" + DEFAULT_NAM);

        // Get all the thongTinKhamBenhList where nam is less than or equal to SMALLER_NAM
        defaultThongTinKhamBenhShouldNotBeFound("nam.lessThanOrEqual=" + SMALLER_NAM);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByNamIsLessThanSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where nam is less than DEFAULT_NAM
        defaultThongTinKhamBenhShouldNotBeFound("nam.lessThan=" + DEFAULT_NAM);

        // Get all the thongTinKhamBenhList where nam is less than UPDATED_NAM
        defaultThongTinKhamBenhShouldBeFound("nam.lessThan=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByNamIsGreaterThanSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        // Get all the thongTinKhamBenhList where nam is greater than DEFAULT_NAM
        defaultThongTinKhamBenhShouldNotBeFound("nam.greaterThan=" + DEFAULT_NAM);

        // Get all the thongTinKhamBenhList where nam is greater than SMALLER_NAM
        defaultThongTinKhamBenhShouldBeFound("nam.greaterThan=" + SMALLER_NAM);
    }


    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByBakbIsEqualToSomething() throws Exception {
        // Get already existing entity
        ThongTinKhoa bakb = thongTinKhamBenh.getBakb();
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);
        Long bakbId = bakb.getId();

        // Get all the thongTinKhamBenhList where bakb equals to bakbId
        defaultThongTinKhamBenhShouldBeFound("bakbId.equals=" + bakbId);

        // Get all the thongTinKhamBenhList where bakb equals to bakbId + 1
        defaultThongTinKhamBenhShouldNotBeFound("bakbId.equals=" + (bakbId + 1));
    }


    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByDonViIsEqualToSomething() throws Exception {
        // Get already existing entity
        ThongTinKhoa donVi = thongTinKhamBenh.getDonVi();
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);
        Long donViId = donVi.getId();

        // Get all the thongTinKhamBenhList where donVi equals to donViId
        defaultThongTinKhamBenhShouldBeFound("donViId.equals=" + donViId);

        // Get all the thongTinKhamBenhList where donVi equals to donViId + 1
        defaultThongTinKhamBenhShouldNotBeFound("donViId.equals=" + (donViId + 1));
    }


    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByBenhNhanIsEqualToSomething() throws Exception {
        // Get already existing entity
        ThongTinKhoa benhNhan = thongTinKhamBenh.getBenhNhan();
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);
        Long benhNhanId = benhNhan.getId();

        // Get all the thongTinKhamBenhList where benhNhan equals to benhNhanId
        defaultThongTinKhamBenhShouldBeFound("benhNhanId.equals=" + benhNhanId);

        // Get all the thongTinKhamBenhList where benhNhan equals to benhNhanId + 1
        defaultThongTinKhamBenhShouldNotBeFound("benhNhanId.equals=" + (benhNhanId + 1));
    }


    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByDotDieuTriIsEqualToSomething() throws Exception {
        // Get already existing entity
        ThongTinKhoa dotDieuTri = thongTinKhamBenh.getDotDieuTri();
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);
        Long dotDieuTriId = dotDieuTri.getId();

        // Get all the thongTinKhamBenhList where dotDieuTri equals to dotDieuTriId
        defaultThongTinKhamBenhShouldBeFound("dotDieuTriId.equals=" + dotDieuTriId);

        // Get all the thongTinKhamBenhList where dotDieuTri equals to dotDieuTriId + 1
        defaultThongTinKhamBenhShouldNotBeFound("dotDieuTriId.equals=" + (dotDieuTriId + 1));
    }


    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByThongTinKhoaIsEqualToSomething() throws Exception {
        // Get already existing entity
        ThongTinKhoa thongTinKhoa = thongTinKhamBenh.getThongTinKhoa();
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);
        Long thongTinKhoaId = thongTinKhoa.getId();

        // Get all the thongTinKhamBenhList where thongTinKhoa equals to thongTinKhoaId
        defaultThongTinKhamBenhShouldBeFound("thongTinKhoaId.equals=" + thongTinKhoaId);

        // Get all the thongTinKhamBenhList where thongTinKhoa equals to thongTinKhoaId + 1
        defaultThongTinKhamBenhShouldNotBeFound("thongTinKhoaId.equals=" + (thongTinKhoaId + 1));
    }


    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByNhanVienIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);
        NhanVien nhanVien = NhanVienResourceIT.createEntity(em);
        em.persist(nhanVien);
        em.flush();
        thongTinKhamBenh.setNhanVien(nhanVien);
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);
        Long nhanVienId = nhanVien.getId();

        // Get all the thongTinKhamBenhList where nhanVien equals to nhanVienId
        defaultThongTinKhamBenhShouldBeFound("nhanVienId.equals=" + nhanVienId);

        // Get all the thongTinKhamBenhList where nhanVien equals to nhanVienId + 1
        defaultThongTinKhamBenhShouldNotBeFound("nhanVienId.equals=" + (nhanVienId + 1));
    }


    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByHuongDieuTriIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);
        HuongDieuTri huongDieuTri = HuongDieuTriResourceIT.createEntity(em);
        em.persist(huongDieuTri);
        em.flush();
        thongTinKhamBenh.setHuongDieuTri(huongDieuTri);
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);
        Long huongDieuTriId = huongDieuTri.getId();

        // Get all the thongTinKhamBenhList where huongDieuTri equals to huongDieuTriId
        defaultThongTinKhamBenhShouldBeFound("huongDieuTriId.equals=" + huongDieuTriId);

        // Get all the thongTinKhamBenhList where huongDieuTri equals to huongDieuTriId + 1
        defaultThongTinKhamBenhShouldNotBeFound("huongDieuTriId.equals=" + (huongDieuTriId + 1));
    }


    @Test
    @Transactional
    public void getAllThongTinKhamBenhsByPhongIsEqualToSomething() throws Exception {
        // Get already existing entity
        Phong phong = thongTinKhamBenh.getPhong();
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);
        Long phongId = phong.getId();

        // Get all the thongTinKhamBenhList where phong equals to phongId
        defaultThongTinKhamBenhShouldBeFound("phongId.equals=" + phongId);

        // Get all the thongTinKhamBenhList where phong equals to phongId + 1
        defaultThongTinKhamBenhShouldNotBeFound("phongId.equals=" + (phongId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultThongTinKhamBenhShouldBeFound(String filter) throws Exception {
        restThongTinKhamBenhMockMvc.perform(get("/api/thong-tin-kham-benhs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(thongTinKhamBenh.getId().intValue())))
            .andExpect(jsonPath("$.[*].bmi").value(hasItem(DEFAULT_BMI.intValue())))
            .andExpect(jsonPath("$.[*].canNang").value(hasItem(DEFAULT_CAN_NANG.intValue())))
            .andExpect(jsonPath("$.[*].chieuCao").value(hasItem(DEFAULT_CHIEU_CAO)))
            .andExpect(jsonPath("$.[*].creatinin").value(hasItem(DEFAULT_CREATININ.intValue())))
            .andExpect(jsonPath("$.[*].dauHieuLamSang").value(hasItem(DEFAULT_DAU_HIEU_LAM_SANG)))
            .andExpect(jsonPath("$.[*].doThanhThai").value(hasItem(DEFAULT_DO_THANH_THAI.intValue())))
            .andExpect(jsonPath("$.[*].huyetApCao").value(hasItem(DEFAULT_HUYET_AP_CAO)))
            .andExpect(jsonPath("$.[*].huyetApThap").value(hasItem(DEFAULT_HUYET_AP_THAP)))
            .andExpect(jsonPath("$.[*].icd").value(hasItem(DEFAULT_ICD)))
            .andExpect(jsonPath("$.[*].lanHen").value(hasItem(DEFAULT_LAN_HEN)))
            .andExpect(jsonPath("$.[*].loiDan").value(hasItem(DEFAULT_LOI_DAN)))
            .andExpect(jsonPath("$.[*].lyDoChuyen").value(hasItem(DEFAULT_LY_DO_CHUYEN)))
            .andExpect(jsonPath("$.[*].maBenhYhct").value(hasItem(DEFAULT_MA_BENH_YHCT)))
            .andExpect(jsonPath("$.[*].mach").value(hasItem(DEFAULT_MACH)))
            .andExpect(jsonPath("$.[*].maxNgayRaToa").value(hasItem(DEFAULT_MAX_NGAY_RA_TOA)))
            .andExpect(jsonPath("$.[*].ngayHen").value(hasItem(DEFAULT_NGAY_HEN.toString())))
            .andExpect(jsonPath("$.[*].nhanDinhBmi").value(hasItem(DEFAULT_NHAN_DINH_BMI)))
            .andExpect(jsonPath("$.[*].nhanDinhDoThanhThai").value(hasItem(DEFAULT_NHAN_DINH_DO_THANH_THAI)))
            .andExpect(jsonPath("$.[*].nhapVien").value(hasItem(DEFAULT_NHAP_VIEN.booleanValue())))
            .andExpect(jsonPath("$.[*].nhietDo").value(hasItem(DEFAULT_NHIET_DO.intValue())))
            .andExpect(jsonPath("$.[*].nhipTho").value(hasItem(DEFAULT_NHIP_THO)))
            .andExpect(jsonPath("$.[*].ppDieuTriYtct").value(hasItem(DEFAULT_PP_DIEU_TRI_YTCT)))
            .andExpect(jsonPath("$.[*].soLanInBangKe").value(hasItem(DEFAULT_SO_LAN_IN_BANG_KE)))
            .andExpect(jsonPath("$.[*].soLanInToaThuoc").value(hasItem(DEFAULT_SO_LAN_IN_TOA_THUOC)))
            .andExpect(jsonPath("$.[*].tenBenhIcd").value(hasItem(DEFAULT_TEN_BENH_ICD)))
            .andExpect(jsonPath("$.[*].tenBenhTheoBacSi").value(hasItem(DEFAULT_TEN_BENH_THEO_BAC_SI)))
            .andExpect(jsonPath("$.[*].tenBenhYhct").value(hasItem(DEFAULT_TEN_BENH_YHCT)))
            .andExpect(jsonPath("$.[*].trangThaiCdha").value(hasItem(DEFAULT_TRANG_THAI_CDHA.booleanValue())))
            .andExpect(jsonPath("$.[*].trangThaiTtpt").value(hasItem(DEFAULT_TRANG_THAI_TTPT.booleanValue())))
            .andExpect(jsonPath("$.[*].trangThaiXetNghiem").value(hasItem(DEFAULT_TRANG_THAI_XET_NGHIEM.booleanValue())))
            .andExpect(jsonPath("$.[*].trieuChungLamSang").value(hasItem(DEFAULT_TRIEU_CHUNG_LAM_SANG)))
            .andExpect(jsonPath("$.[*].vongBung").value(hasItem(DEFAULT_VONG_BUNG)))
            .andExpect(jsonPath("$.[*].trangThai").value(hasItem(DEFAULT_TRANG_THAI)))
            .andExpect(jsonPath("$.[*].thoiGianKetThucKham").value(hasItem(DEFAULT_THOI_GIAN_KET_THUC_KHAM.toString())))
            .andExpect(jsonPath("$.[*].phongIdChuyenTu").value(hasItem(DEFAULT_PHONG_ID_CHUYEN_TU.intValue())))
            .andExpect(jsonPath("$.[*].thoiGianKhamBenh").value(hasItem(DEFAULT_THOI_GIAN_KHAM_BENH.toString())))
            .andExpect(jsonPath("$.[*].yLenh").value(hasItem(DEFAULT_Y_LENH)))
            .andExpect(jsonPath("$.[*].loaiYLenh").value(hasItem(DEFAULT_LOAI_Y_LENH)))
            .andExpect(jsonPath("$.[*].chanDoan").value(hasItem(DEFAULT_CHAN_DOAN)))
            .andExpect(jsonPath("$.[*].loaiChanDoan").value(hasItem(DEFAULT_LOAI_CHAN_DOAN)))
            .andExpect(jsonPath("$.[*].nam").value(hasItem(DEFAULT_NAM)));

        // Check, that the count call also returns 1
        restThongTinKhamBenhMockMvc.perform(get("/api/thong-tin-kham-benhs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultThongTinKhamBenhShouldNotBeFound(String filter) throws Exception {
        restThongTinKhamBenhMockMvc.perform(get("/api/thong-tin-kham-benhs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restThongTinKhamBenhMockMvc.perform(get("/api/thong-tin-kham-benhs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingThongTinKhamBenh() throws Exception {
        // Get the thongTinKhamBenh
        restThongTinKhamBenhMockMvc.perform(get("/api/thong-tin-kham-benhs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateThongTinKhamBenh() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        int databaseSizeBeforeUpdate = thongTinKhamBenhRepository.findAll().size();

        // Update the thongTinKhamBenh
        ThongTinKhamBenh updatedThongTinKhamBenh = thongTinKhamBenhRepository.findById(thongTinKhamBenh.getId()).get();
        // Disconnect from session so that the updates on updatedThongTinKhamBenh are not directly saved in db
        em.detach(updatedThongTinKhamBenh);
        updatedThongTinKhamBenh
            .bmi(UPDATED_BMI)
            .canNang(UPDATED_CAN_NANG)
            .chieuCao(UPDATED_CHIEU_CAO)
            .creatinin(UPDATED_CREATININ)
            .dauHieuLamSang(UPDATED_DAU_HIEU_LAM_SANG)
            .doThanhThai(UPDATED_DO_THANH_THAI)
            .huyetApCao(UPDATED_HUYET_AP_CAO)
            .huyetApThap(UPDATED_HUYET_AP_THAP)
            .icd(UPDATED_ICD)
            .lanHen(UPDATED_LAN_HEN)
            .loiDan(UPDATED_LOI_DAN)
            .lyDoChuyen(UPDATED_LY_DO_CHUYEN)
            .maBenhYhct(UPDATED_MA_BENH_YHCT)
            .mach(UPDATED_MACH)
            .maxNgayRaToa(UPDATED_MAX_NGAY_RA_TOA)
            .ngayHen(UPDATED_NGAY_HEN)
            .nhanDinhBmi(UPDATED_NHAN_DINH_BMI)
            .nhanDinhDoThanhThai(UPDATED_NHAN_DINH_DO_THANH_THAI)
            .nhapVien(UPDATED_NHAP_VIEN)
            .nhietDo(UPDATED_NHIET_DO)
            .nhipTho(UPDATED_NHIP_THO)
            .ppDieuTriYtct(UPDATED_PP_DIEU_TRI_YTCT)
            .soLanInBangKe(UPDATED_SO_LAN_IN_BANG_KE)
            .soLanInToaThuoc(UPDATED_SO_LAN_IN_TOA_THUOC)
            .tenBenhIcd(UPDATED_TEN_BENH_ICD)
            .tenBenhTheoBacSi(UPDATED_TEN_BENH_THEO_BAC_SI)
            .tenBenhYhct(UPDATED_TEN_BENH_YHCT)
            .trangThaiCdha(UPDATED_TRANG_THAI_CDHA)
            .trangThaiTtpt(UPDATED_TRANG_THAI_TTPT)
            .trangThaiXetNghiem(UPDATED_TRANG_THAI_XET_NGHIEM)
            .trieuChungLamSang(UPDATED_TRIEU_CHUNG_LAM_SANG)
            .vongBung(UPDATED_VONG_BUNG)
            .trangThai(UPDATED_TRANG_THAI)
            .thoiGianKetThucKham(UPDATED_THOI_GIAN_KET_THUC_KHAM)
            .phongIdChuyenTu(UPDATED_PHONG_ID_CHUYEN_TU)
            .thoiGianKhamBenh(UPDATED_THOI_GIAN_KHAM_BENH)
            .yLenh(UPDATED_Y_LENH)
            .loaiYLenh(UPDATED_LOAI_Y_LENH)
            .chanDoan(UPDATED_CHAN_DOAN)
            .loaiChanDoan(UPDATED_LOAI_CHAN_DOAN)
            .nam(UPDATED_NAM);
        ThongTinKhamBenhDTO thongTinKhamBenhDTO = thongTinKhamBenhMapper.toDto(updatedThongTinKhamBenh);

        restThongTinKhamBenhMockMvc.perform(put("/api/thong-tin-kham-benhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(thongTinKhamBenhDTO)))
            .andExpect(status().isOk());

        // Validate the ThongTinKhamBenh in the database
        List<ThongTinKhamBenh> thongTinKhamBenhList = thongTinKhamBenhRepository.findAll();
        assertThat(thongTinKhamBenhList).hasSize(databaseSizeBeforeUpdate);
        ThongTinKhamBenh testThongTinKhamBenh = thongTinKhamBenhList.get(thongTinKhamBenhList.size() - 1);
        assertThat(testThongTinKhamBenh.getBmi()).isEqualTo(UPDATED_BMI);
        assertThat(testThongTinKhamBenh.getCanNang()).isEqualTo(UPDATED_CAN_NANG);
        assertThat(testThongTinKhamBenh.getChieuCao()).isEqualTo(UPDATED_CHIEU_CAO);
        assertThat(testThongTinKhamBenh.getCreatinin()).isEqualTo(UPDATED_CREATININ);
        assertThat(testThongTinKhamBenh.getDauHieuLamSang()).isEqualTo(UPDATED_DAU_HIEU_LAM_SANG);
        assertThat(testThongTinKhamBenh.getDoThanhThai()).isEqualTo(UPDATED_DO_THANH_THAI);
        assertThat(testThongTinKhamBenh.getHuyetApCao()).isEqualTo(UPDATED_HUYET_AP_CAO);
        assertThat(testThongTinKhamBenh.getHuyetApThap()).isEqualTo(UPDATED_HUYET_AP_THAP);
        assertThat(testThongTinKhamBenh.getIcd()).isEqualTo(UPDATED_ICD);
        assertThat(testThongTinKhamBenh.getLanHen()).isEqualTo(UPDATED_LAN_HEN);
        assertThat(testThongTinKhamBenh.getLoiDan()).isEqualTo(UPDATED_LOI_DAN);
        assertThat(testThongTinKhamBenh.getLyDoChuyen()).isEqualTo(UPDATED_LY_DO_CHUYEN);
        assertThat(testThongTinKhamBenh.getMaBenhYhct()).isEqualTo(UPDATED_MA_BENH_YHCT);
        assertThat(testThongTinKhamBenh.getMach()).isEqualTo(UPDATED_MACH);
        assertThat(testThongTinKhamBenh.getMaxNgayRaToa()).isEqualTo(UPDATED_MAX_NGAY_RA_TOA);
        assertThat(testThongTinKhamBenh.getNgayHen()).isEqualTo(UPDATED_NGAY_HEN);
        assertThat(testThongTinKhamBenh.getNhanDinhBmi()).isEqualTo(UPDATED_NHAN_DINH_BMI);
        assertThat(testThongTinKhamBenh.getNhanDinhDoThanhThai()).isEqualTo(UPDATED_NHAN_DINH_DO_THANH_THAI);
        assertThat(testThongTinKhamBenh.isNhapVien()).isEqualTo(UPDATED_NHAP_VIEN);
        assertThat(testThongTinKhamBenh.getNhietDo()).isEqualTo(UPDATED_NHIET_DO);
        assertThat(testThongTinKhamBenh.getNhipTho()).isEqualTo(UPDATED_NHIP_THO);
        assertThat(testThongTinKhamBenh.getPpDieuTriYtct()).isEqualTo(UPDATED_PP_DIEU_TRI_YTCT);
        assertThat(testThongTinKhamBenh.getSoLanInBangKe()).isEqualTo(UPDATED_SO_LAN_IN_BANG_KE);
        assertThat(testThongTinKhamBenh.getSoLanInToaThuoc()).isEqualTo(UPDATED_SO_LAN_IN_TOA_THUOC);
        assertThat(testThongTinKhamBenh.getTenBenhIcd()).isEqualTo(UPDATED_TEN_BENH_ICD);
        assertThat(testThongTinKhamBenh.getTenBenhTheoBacSi()).isEqualTo(UPDATED_TEN_BENH_THEO_BAC_SI);
        assertThat(testThongTinKhamBenh.getTenBenhYhct()).isEqualTo(UPDATED_TEN_BENH_YHCT);
        assertThat(testThongTinKhamBenh.isTrangThaiCdha()).isEqualTo(UPDATED_TRANG_THAI_CDHA);
        assertThat(testThongTinKhamBenh.isTrangThaiTtpt()).isEqualTo(UPDATED_TRANG_THAI_TTPT);
        assertThat(testThongTinKhamBenh.isTrangThaiXetNghiem()).isEqualTo(UPDATED_TRANG_THAI_XET_NGHIEM);
        assertThat(testThongTinKhamBenh.getTrieuChungLamSang()).isEqualTo(UPDATED_TRIEU_CHUNG_LAM_SANG);
        assertThat(testThongTinKhamBenh.getVongBung()).isEqualTo(UPDATED_VONG_BUNG);
        assertThat(testThongTinKhamBenh.getTrangThai()).isEqualTo(UPDATED_TRANG_THAI);
        assertThat(testThongTinKhamBenh.getThoiGianKetThucKham()).isEqualTo(UPDATED_THOI_GIAN_KET_THUC_KHAM);
        assertThat(testThongTinKhamBenh.getPhongIdChuyenTu()).isEqualTo(UPDATED_PHONG_ID_CHUYEN_TU);
        assertThat(testThongTinKhamBenh.getThoiGianKhamBenh()).isEqualTo(UPDATED_THOI_GIAN_KHAM_BENH);
        assertThat(testThongTinKhamBenh.getyLenh()).isEqualTo(UPDATED_Y_LENH);
        assertThat(testThongTinKhamBenh.getLoaiYLenh()).isEqualTo(UPDATED_LOAI_Y_LENH);
        assertThat(testThongTinKhamBenh.getChanDoan()).isEqualTo(UPDATED_CHAN_DOAN);
        assertThat(testThongTinKhamBenh.getLoaiChanDoan()).isEqualTo(UPDATED_LOAI_CHAN_DOAN);
        assertThat(testThongTinKhamBenh.getNam()).isEqualTo(UPDATED_NAM);
    }

    @Test
    @Transactional
    public void updateNonExistingThongTinKhamBenh() throws Exception {
        int databaseSizeBeforeUpdate = thongTinKhamBenhRepository.findAll().size();

        // Create the ThongTinKhamBenh
        ThongTinKhamBenhDTO thongTinKhamBenhDTO = thongTinKhamBenhMapper.toDto(thongTinKhamBenh);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restThongTinKhamBenhMockMvc.perform(put("/api/thong-tin-kham-benhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(thongTinKhamBenhDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ThongTinKhamBenh in the database
        List<ThongTinKhamBenh> thongTinKhamBenhList = thongTinKhamBenhRepository.findAll();
        assertThat(thongTinKhamBenhList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteThongTinKhamBenh() throws Exception {
        // Initialize the database
        thongTinKhamBenhRepository.saveAndFlush(thongTinKhamBenh);

        int databaseSizeBeforeDelete = thongTinKhamBenhRepository.findAll().size();

        // Delete the thongTinKhamBenh
        restThongTinKhamBenhMockMvc.perform(delete("/api/thong-tin-kham-benhs/{id}", thongTinKhamBenh.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ThongTinKhamBenh> thongTinKhamBenhList = thongTinKhamBenhRepository.findAll();
        assertThat(thongTinKhamBenhList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

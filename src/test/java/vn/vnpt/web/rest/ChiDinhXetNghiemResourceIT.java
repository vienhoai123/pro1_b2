package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.ChiDinhXetNghiem;
import vn.vnpt.domain.PhieuChiDinhXetNghiem;
import vn.vnpt.domain.Phong;
import vn.vnpt.domain.XetNghiem;
import vn.vnpt.repository.ChiDinhXetNghiemRepository;
import vn.vnpt.service.ChiDinhXetNghiemService;
import vn.vnpt.service.dto.ChiDinhXetNghiemDTO;
import vn.vnpt.service.mapper.ChiDinhXetNghiemMapper;
import vn.vnpt.service.dto.ChiDinhXetNghiemCriteria;
import vn.vnpt.service.ChiDinhXetNghiemQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ChiDinhXetNghiemResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class ChiDinhXetNghiemResourceIT {

    private static final Boolean DEFAULT_CO_BAO_HIEM = false;
    private static final Boolean UPDATED_CO_BAO_HIEM = true;

    private static final Boolean DEFAULT_CO_KET_QUA = false;
    private static final Boolean UPDATED_CO_KET_QUA = true;

    private static final Integer DEFAULT_DA_THANH_TOAN = 1;
    private static final Integer UPDATED_DA_THANH_TOAN = 2;
    private static final Integer SMALLER_DA_THANH_TOAN = 1 - 1;

    private static final Integer DEFAULT_DA_THANH_TOAN_CHENH_LECH = 1;
    private static final Integer UPDATED_DA_THANH_TOAN_CHENH_LECH = 2;
    private static final Integer SMALLER_DA_THANH_TOAN_CHENH_LECH = 1 - 1;

    private static final Integer DEFAULT_DA_THUC_HIEN = 1;
    private static final Integer UPDATED_DA_THUC_HIEN = 2;
    private static final Integer SMALLER_DA_THUC_HIEN = 1 - 1;

    private static final BigDecimal DEFAULT_DON_GIA = new BigDecimal(1);
    private static final BigDecimal UPDATED_DON_GIA = new BigDecimal(2);
    private static final BigDecimal SMALLER_DON_GIA = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_DON_GIA_BHYT = new BigDecimal(1);
    private static final BigDecimal UPDATED_DON_GIA_BHYT = new BigDecimal(2);
    private static final BigDecimal SMALLER_DON_GIA_BHYT = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_DON_GIA_KHONG_BHYT = new BigDecimal(1);
    private static final BigDecimal UPDATED_DON_GIA_KHONG_BHYT = new BigDecimal(2);
    private static final BigDecimal SMALLER_DON_GIA_KHONG_BHYT = new BigDecimal(1 - 1);

    private static final String DEFAULT_GHI_CHU_CHI_DINH = "AAAAAAAAAA";
    private static final String UPDATED_GHI_CHU_CHI_DINH = "BBBBBBBBBB";

    private static final String DEFAULT_MO_TA = "AAAAAAAAAA";
    private static final String UPDATED_MO_TA = "BBBBBBBBBB";

    private static final Long DEFAULT_NGUOI_CHI_DINH_ID = 1L;
    private static final Long UPDATED_NGUOI_CHI_DINH_ID = 2L;
    private static final Long SMALLER_NGUOI_CHI_DINH_ID = 1L - 1L;

    private static final Integer DEFAULT_SO_LUONG = 1;
    private static final Integer UPDATED_SO_LUONG = 2;
    private static final Integer SMALLER_SO_LUONG = 1 - 1;

    private static final String DEFAULT_TEN = "AAAAAAAAAA";
    private static final String UPDATED_TEN = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_THANH_TIEN = new BigDecimal(1);
    private static final BigDecimal UPDATED_THANH_TIEN = new BigDecimal(2);
    private static final BigDecimal SMALLER_THANH_TIEN = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_THANH_TIEN_BHYT = new BigDecimal(1);
    private static final BigDecimal UPDATED_THANH_TIEN_BHYT = new BigDecimal(2);
    private static final BigDecimal SMALLER_THANH_TIEN_BHYT = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_THANH_TIEN_KHONG_BHYT = new BigDecimal(1);
    private static final BigDecimal UPDATED_THANH_TIEN_KHONG_BHYT = new BigDecimal(2);
    private static final BigDecimal SMALLER_THANH_TIEN_KHONG_BHYT = new BigDecimal(1 - 1);

    private static final LocalDate DEFAULT_THOI_GIAN_CHI_DINH = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_THOI_GIAN_CHI_DINH = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_THOI_GIAN_CHI_DINH = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_THOI_GIAN_TAO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_THOI_GIAN_TAO = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_THOI_GIAN_TAO = LocalDate.ofEpochDay(-1L);

    private static final BigDecimal DEFAULT_TIEN_NGOAI_BHYT = new BigDecimal(1);
    private static final BigDecimal UPDATED_TIEN_NGOAI_BHYT = new BigDecimal(2);
    private static final BigDecimal SMALLER_TIEN_NGOAI_BHYT = new BigDecimal(1 - 1);

    private static final Boolean DEFAULT_THANH_TOAN_CHENH_LECH = false;
    private static final Boolean UPDATED_THANH_TOAN_CHENH_LECH = true;

    private static final Integer DEFAULT_TY_LE_THANH_TOAN = 1;
    private static final Integer UPDATED_TY_LE_THANH_TOAN = 2;
    private static final Integer SMALLER_TY_LE_THANH_TOAN = 1 - 1;

    private static final String DEFAULT_MA_DUNG_CHUNG = "AAAAAAAAAA";
    private static final String UPDATED_MA_DUNG_CHUNG = "BBBBBBBBBB";

    private static final Boolean DEFAULT_DICH_VU_YEU_CAU = false;
    private static final Boolean UPDATED_DICH_VU_YEU_CAU = true;

    private static final Integer DEFAULT_NAM = 1;
    private static final Integer UPDATED_NAM = 2;
    private static final Integer SMALLER_NAM = 1 - 1;

    @Autowired
    private ChiDinhXetNghiemRepository chiDinhXetNghiemRepository;

    @Autowired
    private ChiDinhXetNghiemMapper chiDinhXetNghiemMapper;

    @Autowired
    private ChiDinhXetNghiemService chiDinhXetNghiemService;

    @Autowired
    private ChiDinhXetNghiemQueryService chiDinhXetNghiemQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restChiDinhXetNghiemMockMvc;

    private ChiDinhXetNghiem chiDinhXetNghiem;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ChiDinhXetNghiem createEntity(EntityManager em) {
        ChiDinhXetNghiem chiDinhXetNghiem = new ChiDinhXetNghiem()
            .coBaoHiem(DEFAULT_CO_BAO_HIEM)
            .coKetQua(DEFAULT_CO_KET_QUA)
            .daThanhToan(DEFAULT_DA_THANH_TOAN)
            .daThanhToanChenhLech(DEFAULT_DA_THANH_TOAN_CHENH_LECH)
            .daThucHien(DEFAULT_DA_THUC_HIEN)
            .donGia(DEFAULT_DON_GIA)
            .donGiaBhyt(DEFAULT_DON_GIA_BHYT)
            .donGiaKhongBhyt(DEFAULT_DON_GIA_KHONG_BHYT)
            .ghiChuChiDinh(DEFAULT_GHI_CHU_CHI_DINH)
            .moTa(DEFAULT_MO_TA)
            .nguoiChiDinhId(DEFAULT_NGUOI_CHI_DINH_ID)
            .soLuong(DEFAULT_SO_LUONG)
            .ten(DEFAULT_TEN)
            .thanhTien(DEFAULT_THANH_TIEN)
            .thanhTienBhyt(DEFAULT_THANH_TIEN_BHYT)
            .thanhTienKhongBHYT(DEFAULT_THANH_TIEN_KHONG_BHYT)
            .thoiGianChiDinh(DEFAULT_THOI_GIAN_CHI_DINH)
            .thoiGianTao(DEFAULT_THOI_GIAN_TAO)
            .tienNgoaiBHYT(DEFAULT_TIEN_NGOAI_BHYT)
            .thanhToanChenhLech(DEFAULT_THANH_TOAN_CHENH_LECH)
            .tyLeThanhToan(DEFAULT_TY_LE_THANH_TOAN)
            .maDungChung(DEFAULT_MA_DUNG_CHUNG)
            .dichVuYeuCau(DEFAULT_DICH_VU_YEU_CAU)
            .nam(DEFAULT_NAM);
        // Add required entity
        PhieuChiDinhXetNghiem phieuChiDinhXetNghiem;
        if (TestUtil.findAll(em, PhieuChiDinhXetNghiem.class).isEmpty()) {
            phieuChiDinhXetNghiem = PhieuChiDinhXetNghiemResourceIT.createEntity(em);
            em.persist(phieuChiDinhXetNghiem);
            em.flush();
        } else {
            phieuChiDinhXetNghiem = TestUtil.findAll(em, PhieuChiDinhXetNghiem.class).get(0);
        }
        chiDinhXetNghiem.setDonVi(phieuChiDinhXetNghiem);
        // Add required entity
        chiDinhXetNghiem.setBenhNhan(phieuChiDinhXetNghiem);
        // Add required entity
        chiDinhXetNghiem.setBakb(phieuChiDinhXetNghiem);
        // Add required entity
        chiDinhXetNghiem.setPhieuCD(phieuChiDinhXetNghiem);
        // Add required entity
        Phong phong;
        if (TestUtil.findAll(em, Phong.class).isEmpty()) {
            phong = PhongResourceIT.createEntity(em);
            em.persist(phong);
            em.flush();
        } else {
            phong = TestUtil.findAll(em, Phong.class).get(0);
        }
        chiDinhXetNghiem.setPhong(phong);
        // Add required entity
        XetNghiem xetNghiem;
        if (TestUtil.findAll(em, XetNghiem.class).isEmpty()) {
            xetNghiem = XetNghiemResourceIT.createEntity(em);
            em.persist(xetNghiem);
            em.flush();
        } else {
            xetNghiem = TestUtil.findAll(em, XetNghiem.class).get(0);
        }
        chiDinhXetNghiem.setXetNghiem(xetNghiem);
        return chiDinhXetNghiem;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ChiDinhXetNghiem createUpdatedEntity(EntityManager em) {
        ChiDinhXetNghiem chiDinhXetNghiem = new ChiDinhXetNghiem()
            .coBaoHiem(UPDATED_CO_BAO_HIEM)
            .coKetQua(UPDATED_CO_KET_QUA)
            .daThanhToan(UPDATED_DA_THANH_TOAN)
            .daThanhToanChenhLech(UPDATED_DA_THANH_TOAN_CHENH_LECH)
            .daThucHien(UPDATED_DA_THUC_HIEN)
            .donGia(UPDATED_DON_GIA)
            .donGiaBhyt(UPDATED_DON_GIA_BHYT)
            .donGiaKhongBhyt(UPDATED_DON_GIA_KHONG_BHYT)
            .ghiChuChiDinh(UPDATED_GHI_CHU_CHI_DINH)
            .moTa(UPDATED_MO_TA)
            .nguoiChiDinhId(UPDATED_NGUOI_CHI_DINH_ID)
            .soLuong(UPDATED_SO_LUONG)
            .ten(UPDATED_TEN)
            .thanhTien(UPDATED_THANH_TIEN)
            .thanhTienBhyt(UPDATED_THANH_TIEN_BHYT)
            .thanhTienKhongBHYT(UPDATED_THANH_TIEN_KHONG_BHYT)
            .thoiGianChiDinh(UPDATED_THOI_GIAN_CHI_DINH)
            .thoiGianTao(UPDATED_THOI_GIAN_TAO)
            .tienNgoaiBHYT(UPDATED_TIEN_NGOAI_BHYT)
            .thanhToanChenhLech(UPDATED_THANH_TOAN_CHENH_LECH)
            .tyLeThanhToan(UPDATED_TY_LE_THANH_TOAN)
            .maDungChung(UPDATED_MA_DUNG_CHUNG)
            .dichVuYeuCau(UPDATED_DICH_VU_YEU_CAU)
            .nam(UPDATED_NAM);
        // Add required entity
        PhieuChiDinhXetNghiem phieuChiDinhXetNghiem;
        if (TestUtil.findAll(em, PhieuChiDinhXetNghiem.class).isEmpty()) {
            phieuChiDinhXetNghiem = PhieuChiDinhXetNghiemResourceIT.createUpdatedEntity(em);
            em.persist(phieuChiDinhXetNghiem);
            em.flush();
        } else {
            phieuChiDinhXetNghiem = TestUtil.findAll(em, PhieuChiDinhXetNghiem.class).get(0);
        }
        chiDinhXetNghiem.setDonVi(phieuChiDinhXetNghiem);
        // Add required entity
        chiDinhXetNghiem.setBenhNhan(phieuChiDinhXetNghiem);
        // Add required entity
        chiDinhXetNghiem.setBakb(phieuChiDinhXetNghiem);
        // Add required entity
        chiDinhXetNghiem.setPhieuCD(phieuChiDinhXetNghiem);
        // Add required entity
        Phong phong;
        if (TestUtil.findAll(em, Phong.class).isEmpty()) {
            phong = PhongResourceIT.createUpdatedEntity(em);
            em.persist(phong);
            em.flush();
        } else {
            phong = TestUtil.findAll(em, Phong.class).get(0);
        }
        chiDinhXetNghiem.setPhong(phong);
        // Add required entity
        XetNghiem xetNghiem;
        if (TestUtil.findAll(em, XetNghiem.class).isEmpty()) {
            xetNghiem = XetNghiemResourceIT.createUpdatedEntity(em);
            em.persist(xetNghiem);
            em.flush();
        } else {
            xetNghiem = TestUtil.findAll(em, XetNghiem.class).get(0);
        }
        chiDinhXetNghiem.setXetNghiem(xetNghiem);
        return chiDinhXetNghiem;
    }

    @BeforeEach
    public void initTest() {
        chiDinhXetNghiem = createEntity(em);
    }

    @Test
    @Transactional
    public void createChiDinhXetNghiem() throws Exception {
        int databaseSizeBeforeCreate = chiDinhXetNghiemRepository.findAll().size();

        // Create the ChiDinhXetNghiem
        ChiDinhXetNghiemDTO chiDinhXetNghiemDTO = chiDinhXetNghiemMapper.toDto(chiDinhXetNghiem);
        restChiDinhXetNghiemMockMvc.perform(post("/api/chi-dinh-xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhXetNghiemDTO)))
            .andExpect(status().isCreated());

        // Validate the ChiDinhXetNghiem in the database
        List<ChiDinhXetNghiem> chiDinhXetNghiemList = chiDinhXetNghiemRepository.findAll();
        assertThat(chiDinhXetNghiemList).hasSize(databaseSizeBeforeCreate + 1);
        ChiDinhXetNghiem testChiDinhXetNghiem = chiDinhXetNghiemList.get(chiDinhXetNghiemList.size() - 1);
        assertThat(testChiDinhXetNghiem.isCoBaoHiem()).isEqualTo(DEFAULT_CO_BAO_HIEM);
        assertThat(testChiDinhXetNghiem.isCoKetQua()).isEqualTo(DEFAULT_CO_KET_QUA);
        assertThat(testChiDinhXetNghiem.getDaThanhToan()).isEqualTo(DEFAULT_DA_THANH_TOAN);
        assertThat(testChiDinhXetNghiem.getDaThanhToanChenhLech()).isEqualTo(DEFAULT_DA_THANH_TOAN_CHENH_LECH);
        assertThat(testChiDinhXetNghiem.getDaThucHien()).isEqualTo(DEFAULT_DA_THUC_HIEN);
        assertThat(testChiDinhXetNghiem.getDonGia()).isEqualTo(DEFAULT_DON_GIA);
        assertThat(testChiDinhXetNghiem.getDonGiaBhyt()).isEqualTo(DEFAULT_DON_GIA_BHYT);
        assertThat(testChiDinhXetNghiem.getDonGiaKhongBhyt()).isEqualTo(DEFAULT_DON_GIA_KHONG_BHYT);
        assertThat(testChiDinhXetNghiem.getGhiChuChiDinh()).isEqualTo(DEFAULT_GHI_CHU_CHI_DINH);
        assertThat(testChiDinhXetNghiem.getMoTa()).isEqualTo(DEFAULT_MO_TA);
        assertThat(testChiDinhXetNghiem.getNguoiChiDinhId()).isEqualTo(DEFAULT_NGUOI_CHI_DINH_ID);
        assertThat(testChiDinhXetNghiem.getSoLuong()).isEqualTo(DEFAULT_SO_LUONG);
        assertThat(testChiDinhXetNghiem.getTen()).isEqualTo(DEFAULT_TEN);
        assertThat(testChiDinhXetNghiem.getThanhTien()).isEqualTo(DEFAULT_THANH_TIEN);
        assertThat(testChiDinhXetNghiem.getThanhTienBhyt()).isEqualTo(DEFAULT_THANH_TIEN_BHYT);
        assertThat(testChiDinhXetNghiem.getThanhTienKhongBHYT()).isEqualTo(DEFAULT_THANH_TIEN_KHONG_BHYT);
        assertThat(testChiDinhXetNghiem.getThoiGianChiDinh()).isEqualTo(DEFAULT_THOI_GIAN_CHI_DINH);
        assertThat(testChiDinhXetNghiem.getThoiGianTao()).isEqualTo(DEFAULT_THOI_GIAN_TAO);
        assertThat(testChiDinhXetNghiem.getTienNgoaiBHYT()).isEqualTo(DEFAULT_TIEN_NGOAI_BHYT);
        assertThat(testChiDinhXetNghiem.isThanhToanChenhLech()).isEqualTo(DEFAULT_THANH_TOAN_CHENH_LECH);
        assertThat(testChiDinhXetNghiem.getTyLeThanhToan()).isEqualTo(DEFAULT_TY_LE_THANH_TOAN);
        assertThat(testChiDinhXetNghiem.getMaDungChung()).isEqualTo(DEFAULT_MA_DUNG_CHUNG);
        assertThat(testChiDinhXetNghiem.isDichVuYeuCau()).isEqualTo(DEFAULT_DICH_VU_YEU_CAU);
        assertThat(testChiDinhXetNghiem.getNam()).isEqualTo(DEFAULT_NAM);
    }

    @Test
    @Transactional
    public void createChiDinhXetNghiemWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = chiDinhXetNghiemRepository.findAll().size();

        // Create the ChiDinhXetNghiem with an existing ID
        chiDinhXetNghiem.setId(1L);
        ChiDinhXetNghiemDTO chiDinhXetNghiemDTO = chiDinhXetNghiemMapper.toDto(chiDinhXetNghiem);

        // An entity with an existing ID cannot be created, so this API call must fail
        restChiDinhXetNghiemMockMvc.perform(post("/api/chi-dinh-xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhXetNghiemDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ChiDinhXetNghiem in the database
        List<ChiDinhXetNghiem> chiDinhXetNghiemList = chiDinhXetNghiemRepository.findAll();
        assertThat(chiDinhXetNghiemList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkCoBaoHiemIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiDinhXetNghiemRepository.findAll().size();
        // set the field null
        chiDinhXetNghiem.setCoBaoHiem(null);

        // Create the ChiDinhXetNghiem, which fails.
        ChiDinhXetNghiemDTO chiDinhXetNghiemDTO = chiDinhXetNghiemMapper.toDto(chiDinhXetNghiem);

        restChiDinhXetNghiemMockMvc.perform(post("/api/chi-dinh-xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhXetNghiemDTO)))
            .andExpect(status().isBadRequest());

        List<ChiDinhXetNghiem> chiDinhXetNghiemList = chiDinhXetNghiemRepository.findAll();
        assertThat(chiDinhXetNghiemList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCoKetQuaIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiDinhXetNghiemRepository.findAll().size();
        // set the field null
        chiDinhXetNghiem.setCoKetQua(null);

        // Create the ChiDinhXetNghiem, which fails.
        ChiDinhXetNghiemDTO chiDinhXetNghiemDTO = chiDinhXetNghiemMapper.toDto(chiDinhXetNghiem);

        restChiDinhXetNghiemMockMvc.perform(post("/api/chi-dinh-xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhXetNghiemDTO)))
            .andExpect(status().isBadRequest());

        List<ChiDinhXetNghiem> chiDinhXetNghiemList = chiDinhXetNghiemRepository.findAll();
        assertThat(chiDinhXetNghiemList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDaThanhToanIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiDinhXetNghiemRepository.findAll().size();
        // set the field null
        chiDinhXetNghiem.setDaThanhToan(null);

        // Create the ChiDinhXetNghiem, which fails.
        ChiDinhXetNghiemDTO chiDinhXetNghiemDTO = chiDinhXetNghiemMapper.toDto(chiDinhXetNghiem);

        restChiDinhXetNghiemMockMvc.perform(post("/api/chi-dinh-xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhXetNghiemDTO)))
            .andExpect(status().isBadRequest());

        List<ChiDinhXetNghiem> chiDinhXetNghiemList = chiDinhXetNghiemRepository.findAll();
        assertThat(chiDinhXetNghiemList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDaThanhToanChenhLechIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiDinhXetNghiemRepository.findAll().size();
        // set the field null
        chiDinhXetNghiem.setDaThanhToanChenhLech(null);

        // Create the ChiDinhXetNghiem, which fails.
        ChiDinhXetNghiemDTO chiDinhXetNghiemDTO = chiDinhXetNghiemMapper.toDto(chiDinhXetNghiem);

        restChiDinhXetNghiemMockMvc.perform(post("/api/chi-dinh-xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhXetNghiemDTO)))
            .andExpect(status().isBadRequest());

        List<ChiDinhXetNghiem> chiDinhXetNghiemList = chiDinhXetNghiemRepository.findAll();
        assertThat(chiDinhXetNghiemList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDaThucHienIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiDinhXetNghiemRepository.findAll().size();
        // set the field null
        chiDinhXetNghiem.setDaThucHien(null);

        // Create the ChiDinhXetNghiem, which fails.
        ChiDinhXetNghiemDTO chiDinhXetNghiemDTO = chiDinhXetNghiemMapper.toDto(chiDinhXetNghiem);

        restChiDinhXetNghiemMockMvc.perform(post("/api/chi-dinh-xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhXetNghiemDTO)))
            .andExpect(status().isBadRequest());

        List<ChiDinhXetNghiem> chiDinhXetNghiemList = chiDinhXetNghiemRepository.findAll();
        assertThat(chiDinhXetNghiemList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDonGiaIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiDinhXetNghiemRepository.findAll().size();
        // set the field null
        chiDinhXetNghiem.setDonGia(null);

        // Create the ChiDinhXetNghiem, which fails.
        ChiDinhXetNghiemDTO chiDinhXetNghiemDTO = chiDinhXetNghiemMapper.toDto(chiDinhXetNghiem);

        restChiDinhXetNghiemMockMvc.perform(post("/api/chi-dinh-xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhXetNghiemDTO)))
            .andExpect(status().isBadRequest());

        List<ChiDinhXetNghiem> chiDinhXetNghiemList = chiDinhXetNghiemRepository.findAll();
        assertThat(chiDinhXetNghiemList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDonGiaBhytIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiDinhXetNghiemRepository.findAll().size();
        // set the field null
        chiDinhXetNghiem.setDonGiaBhyt(null);

        // Create the ChiDinhXetNghiem, which fails.
        ChiDinhXetNghiemDTO chiDinhXetNghiemDTO = chiDinhXetNghiemMapper.toDto(chiDinhXetNghiem);

        restChiDinhXetNghiemMockMvc.perform(post("/api/chi-dinh-xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhXetNghiemDTO)))
            .andExpect(status().isBadRequest());

        List<ChiDinhXetNghiem> chiDinhXetNghiemList = chiDinhXetNghiemRepository.findAll();
        assertThat(chiDinhXetNghiemList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkThanhTienIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiDinhXetNghiemRepository.findAll().size();
        // set the field null
        chiDinhXetNghiem.setThanhTien(null);

        // Create the ChiDinhXetNghiem, which fails.
        ChiDinhXetNghiemDTO chiDinhXetNghiemDTO = chiDinhXetNghiemMapper.toDto(chiDinhXetNghiem);

        restChiDinhXetNghiemMockMvc.perform(post("/api/chi-dinh-xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhXetNghiemDTO)))
            .andExpect(status().isBadRequest());

        List<ChiDinhXetNghiem> chiDinhXetNghiemList = chiDinhXetNghiemRepository.findAll();
        assertThat(chiDinhXetNghiemList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkThoiGianTaoIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiDinhXetNghiemRepository.findAll().size();
        // set the field null
        chiDinhXetNghiem.setThoiGianTao(null);

        // Create the ChiDinhXetNghiem, which fails.
        ChiDinhXetNghiemDTO chiDinhXetNghiemDTO = chiDinhXetNghiemMapper.toDto(chiDinhXetNghiem);

        restChiDinhXetNghiemMockMvc.perform(post("/api/chi-dinh-xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhXetNghiemDTO)))
            .andExpect(status().isBadRequest());

        List<ChiDinhXetNghiem> chiDinhXetNghiemList = chiDinhXetNghiemRepository.findAll();
        assertThat(chiDinhXetNghiemList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTienNgoaiBHYTIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiDinhXetNghiemRepository.findAll().size();
        // set the field null
        chiDinhXetNghiem.setTienNgoaiBHYT(null);

        // Create the ChiDinhXetNghiem, which fails.
        ChiDinhXetNghiemDTO chiDinhXetNghiemDTO = chiDinhXetNghiemMapper.toDto(chiDinhXetNghiem);

        restChiDinhXetNghiemMockMvc.perform(post("/api/chi-dinh-xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhXetNghiemDTO)))
            .andExpect(status().isBadRequest());

        List<ChiDinhXetNghiem> chiDinhXetNghiemList = chiDinhXetNghiemRepository.findAll();
        assertThat(chiDinhXetNghiemList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkThanhToanChenhLechIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiDinhXetNghiemRepository.findAll().size();
        // set the field null
        chiDinhXetNghiem.setThanhToanChenhLech(null);

        // Create the ChiDinhXetNghiem, which fails.
        ChiDinhXetNghiemDTO chiDinhXetNghiemDTO = chiDinhXetNghiemMapper.toDto(chiDinhXetNghiem);

        restChiDinhXetNghiemMockMvc.perform(post("/api/chi-dinh-xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhXetNghiemDTO)))
            .andExpect(status().isBadRequest());

        List<ChiDinhXetNghiem> chiDinhXetNghiemList = chiDinhXetNghiemRepository.findAll();
        assertThat(chiDinhXetNghiemList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNamIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiDinhXetNghiemRepository.findAll().size();
        // set the field null
        chiDinhXetNghiem.setNam(null);

        // Create the ChiDinhXetNghiem, which fails.
        ChiDinhXetNghiemDTO chiDinhXetNghiemDTO = chiDinhXetNghiemMapper.toDto(chiDinhXetNghiem);

        restChiDinhXetNghiemMockMvc.perform(post("/api/chi-dinh-xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhXetNghiemDTO)))
            .andExpect(status().isBadRequest());

        List<ChiDinhXetNghiem> chiDinhXetNghiemList = chiDinhXetNghiemRepository.findAll();
        assertThat(chiDinhXetNghiemList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiems() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList
        restChiDinhXetNghiemMockMvc.perform(get("/api/chi-dinh-xet-nghiems?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(chiDinhXetNghiem.getId().intValue())))
            .andExpect(jsonPath("$.[*].coBaoHiem").value(hasItem(DEFAULT_CO_BAO_HIEM.booleanValue())))
            .andExpect(jsonPath("$.[*].coKetQua").value(hasItem(DEFAULT_CO_KET_QUA.booleanValue())))
            .andExpect(jsonPath("$.[*].daThanhToan").value(hasItem(DEFAULT_DA_THANH_TOAN)))
            .andExpect(jsonPath("$.[*].daThanhToanChenhLech").value(hasItem(DEFAULT_DA_THANH_TOAN_CHENH_LECH)))
            .andExpect(jsonPath("$.[*].daThucHien").value(hasItem(DEFAULT_DA_THUC_HIEN)))
            .andExpect(jsonPath("$.[*].donGia").value(hasItem(DEFAULT_DON_GIA.intValue())))
            .andExpect(jsonPath("$.[*].donGiaBhyt").value(hasItem(DEFAULT_DON_GIA_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].donGiaKhongBhyt").value(hasItem(DEFAULT_DON_GIA_KHONG_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].ghiChuChiDinh").value(hasItem(DEFAULT_GHI_CHU_CHI_DINH)))
            .andExpect(jsonPath("$.[*].moTa").value(hasItem(DEFAULT_MO_TA)))
            .andExpect(jsonPath("$.[*].nguoiChiDinhId").value(hasItem(DEFAULT_NGUOI_CHI_DINH_ID.intValue())))
            .andExpect(jsonPath("$.[*].soLuong").value(hasItem(DEFAULT_SO_LUONG)))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].thanhTien").value(hasItem(DEFAULT_THANH_TIEN.intValue())))
            .andExpect(jsonPath("$.[*].thanhTienBhyt").value(hasItem(DEFAULT_THANH_TIEN_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].thanhTienKhongBHYT").value(hasItem(DEFAULT_THANH_TIEN_KHONG_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].thoiGianChiDinh").value(hasItem(DEFAULT_THOI_GIAN_CHI_DINH.toString())))
            .andExpect(jsonPath("$.[*].thoiGianTao").value(hasItem(DEFAULT_THOI_GIAN_TAO.toString())))
            .andExpect(jsonPath("$.[*].tienNgoaiBHYT").value(hasItem(DEFAULT_TIEN_NGOAI_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].thanhToanChenhLech").value(hasItem(DEFAULT_THANH_TOAN_CHENH_LECH.booleanValue())))
            .andExpect(jsonPath("$.[*].tyLeThanhToan").value(hasItem(DEFAULT_TY_LE_THANH_TOAN)))
            .andExpect(jsonPath("$.[*].maDungChung").value(hasItem(DEFAULT_MA_DUNG_CHUNG)))
            .andExpect(jsonPath("$.[*].dichVuYeuCau").value(hasItem(DEFAULT_DICH_VU_YEU_CAU.booleanValue())))
            .andExpect(jsonPath("$.[*].nam").value(hasItem(DEFAULT_NAM)));
    }
    
    @Test
    @Transactional
    public void getChiDinhXetNghiem() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get the chiDinhXetNghiem
        restChiDinhXetNghiemMockMvc.perform(get("/api/chi-dinh-xet-nghiems/{id}", chiDinhXetNghiem.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(chiDinhXetNghiem.getId().intValue()))
            .andExpect(jsonPath("$.coBaoHiem").value(DEFAULT_CO_BAO_HIEM.booleanValue()))
            .andExpect(jsonPath("$.coKetQua").value(DEFAULT_CO_KET_QUA.booleanValue()))
            .andExpect(jsonPath("$.daThanhToan").value(DEFAULT_DA_THANH_TOAN))
            .andExpect(jsonPath("$.daThanhToanChenhLech").value(DEFAULT_DA_THANH_TOAN_CHENH_LECH))
            .andExpect(jsonPath("$.daThucHien").value(DEFAULT_DA_THUC_HIEN))
            .andExpect(jsonPath("$.donGia").value(DEFAULT_DON_GIA.intValue()))
            .andExpect(jsonPath("$.donGiaBhyt").value(DEFAULT_DON_GIA_BHYT.intValue()))
            .andExpect(jsonPath("$.donGiaKhongBhyt").value(DEFAULT_DON_GIA_KHONG_BHYT.intValue()))
            .andExpect(jsonPath("$.ghiChuChiDinh").value(DEFAULT_GHI_CHU_CHI_DINH))
            .andExpect(jsonPath("$.moTa").value(DEFAULT_MO_TA))
            .andExpect(jsonPath("$.nguoiChiDinhId").value(DEFAULT_NGUOI_CHI_DINH_ID.intValue()))
            .andExpect(jsonPath("$.soLuong").value(DEFAULT_SO_LUONG))
            .andExpect(jsonPath("$.ten").value(DEFAULT_TEN))
            .andExpect(jsonPath("$.thanhTien").value(DEFAULT_THANH_TIEN.intValue()))
            .andExpect(jsonPath("$.thanhTienBhyt").value(DEFAULT_THANH_TIEN_BHYT.intValue()))
            .andExpect(jsonPath("$.thanhTienKhongBHYT").value(DEFAULT_THANH_TIEN_KHONG_BHYT.intValue()))
            .andExpect(jsonPath("$.thoiGianChiDinh").value(DEFAULT_THOI_GIAN_CHI_DINH.toString()))
            .andExpect(jsonPath("$.thoiGianTao").value(DEFAULT_THOI_GIAN_TAO.toString()))
            .andExpect(jsonPath("$.tienNgoaiBHYT").value(DEFAULT_TIEN_NGOAI_BHYT.intValue()))
            .andExpect(jsonPath("$.thanhToanChenhLech").value(DEFAULT_THANH_TOAN_CHENH_LECH.booleanValue()))
            .andExpect(jsonPath("$.tyLeThanhToan").value(DEFAULT_TY_LE_THANH_TOAN))
            .andExpect(jsonPath("$.maDungChung").value(DEFAULT_MA_DUNG_CHUNG))
            .andExpect(jsonPath("$.dichVuYeuCau").value(DEFAULT_DICH_VU_YEU_CAU.booleanValue()))
            .andExpect(jsonPath("$.nam").value(DEFAULT_NAM));
    }


    @Test
    @Transactional
    public void getChiDinhXetNghiemsByIdFiltering() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        Long id = chiDinhXetNghiem.getId();

        defaultChiDinhXetNghiemShouldBeFound("id.equals=" + id);
        defaultChiDinhXetNghiemShouldNotBeFound("id.notEquals=" + id);

        defaultChiDinhXetNghiemShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultChiDinhXetNghiemShouldNotBeFound("id.greaterThan=" + id);

        defaultChiDinhXetNghiemShouldBeFound("id.lessThanOrEqual=" + id);
        defaultChiDinhXetNghiemShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByCoBaoHiemIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where coBaoHiem equals to DEFAULT_CO_BAO_HIEM
        defaultChiDinhXetNghiemShouldBeFound("coBaoHiem.equals=" + DEFAULT_CO_BAO_HIEM);

        // Get all the chiDinhXetNghiemList where coBaoHiem equals to UPDATED_CO_BAO_HIEM
        defaultChiDinhXetNghiemShouldNotBeFound("coBaoHiem.equals=" + UPDATED_CO_BAO_HIEM);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByCoBaoHiemIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where coBaoHiem not equals to DEFAULT_CO_BAO_HIEM
        defaultChiDinhXetNghiemShouldNotBeFound("coBaoHiem.notEquals=" + DEFAULT_CO_BAO_HIEM);

        // Get all the chiDinhXetNghiemList where coBaoHiem not equals to UPDATED_CO_BAO_HIEM
        defaultChiDinhXetNghiemShouldBeFound("coBaoHiem.notEquals=" + UPDATED_CO_BAO_HIEM);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByCoBaoHiemIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where coBaoHiem in DEFAULT_CO_BAO_HIEM or UPDATED_CO_BAO_HIEM
        defaultChiDinhXetNghiemShouldBeFound("coBaoHiem.in=" + DEFAULT_CO_BAO_HIEM + "," + UPDATED_CO_BAO_HIEM);

        // Get all the chiDinhXetNghiemList where coBaoHiem equals to UPDATED_CO_BAO_HIEM
        defaultChiDinhXetNghiemShouldNotBeFound("coBaoHiem.in=" + UPDATED_CO_BAO_HIEM);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByCoBaoHiemIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where coBaoHiem is not null
        defaultChiDinhXetNghiemShouldBeFound("coBaoHiem.specified=true");

        // Get all the chiDinhXetNghiemList where coBaoHiem is null
        defaultChiDinhXetNghiemShouldNotBeFound("coBaoHiem.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByCoKetQuaIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where coKetQua equals to DEFAULT_CO_KET_QUA
        defaultChiDinhXetNghiemShouldBeFound("coKetQua.equals=" + DEFAULT_CO_KET_QUA);

        // Get all the chiDinhXetNghiemList where coKetQua equals to UPDATED_CO_KET_QUA
        defaultChiDinhXetNghiemShouldNotBeFound("coKetQua.equals=" + UPDATED_CO_KET_QUA);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByCoKetQuaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where coKetQua not equals to DEFAULT_CO_KET_QUA
        defaultChiDinhXetNghiemShouldNotBeFound("coKetQua.notEquals=" + DEFAULT_CO_KET_QUA);

        // Get all the chiDinhXetNghiemList where coKetQua not equals to UPDATED_CO_KET_QUA
        defaultChiDinhXetNghiemShouldBeFound("coKetQua.notEquals=" + UPDATED_CO_KET_QUA);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByCoKetQuaIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where coKetQua in DEFAULT_CO_KET_QUA or UPDATED_CO_KET_QUA
        defaultChiDinhXetNghiemShouldBeFound("coKetQua.in=" + DEFAULT_CO_KET_QUA + "," + UPDATED_CO_KET_QUA);

        // Get all the chiDinhXetNghiemList where coKetQua equals to UPDATED_CO_KET_QUA
        defaultChiDinhXetNghiemShouldNotBeFound("coKetQua.in=" + UPDATED_CO_KET_QUA);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByCoKetQuaIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where coKetQua is not null
        defaultChiDinhXetNghiemShouldBeFound("coKetQua.specified=true");

        // Get all the chiDinhXetNghiemList where coKetQua is null
        defaultChiDinhXetNghiemShouldNotBeFound("coKetQua.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDaThanhToanIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where daThanhToan equals to DEFAULT_DA_THANH_TOAN
        defaultChiDinhXetNghiemShouldBeFound("daThanhToan.equals=" + DEFAULT_DA_THANH_TOAN);

        // Get all the chiDinhXetNghiemList where daThanhToan equals to UPDATED_DA_THANH_TOAN
        defaultChiDinhXetNghiemShouldNotBeFound("daThanhToan.equals=" + UPDATED_DA_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDaThanhToanIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where daThanhToan not equals to DEFAULT_DA_THANH_TOAN
        defaultChiDinhXetNghiemShouldNotBeFound("daThanhToan.notEquals=" + DEFAULT_DA_THANH_TOAN);

        // Get all the chiDinhXetNghiemList where daThanhToan not equals to UPDATED_DA_THANH_TOAN
        defaultChiDinhXetNghiemShouldBeFound("daThanhToan.notEquals=" + UPDATED_DA_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDaThanhToanIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where daThanhToan in DEFAULT_DA_THANH_TOAN or UPDATED_DA_THANH_TOAN
        defaultChiDinhXetNghiemShouldBeFound("daThanhToan.in=" + DEFAULT_DA_THANH_TOAN + "," + UPDATED_DA_THANH_TOAN);

        // Get all the chiDinhXetNghiemList where daThanhToan equals to UPDATED_DA_THANH_TOAN
        defaultChiDinhXetNghiemShouldNotBeFound("daThanhToan.in=" + UPDATED_DA_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDaThanhToanIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where daThanhToan is not null
        defaultChiDinhXetNghiemShouldBeFound("daThanhToan.specified=true");

        // Get all the chiDinhXetNghiemList where daThanhToan is null
        defaultChiDinhXetNghiemShouldNotBeFound("daThanhToan.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDaThanhToanIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where daThanhToan is greater than or equal to DEFAULT_DA_THANH_TOAN
        defaultChiDinhXetNghiemShouldBeFound("daThanhToan.greaterThanOrEqual=" + DEFAULT_DA_THANH_TOAN);

        // Get all the chiDinhXetNghiemList where daThanhToan is greater than or equal to UPDATED_DA_THANH_TOAN
        defaultChiDinhXetNghiemShouldNotBeFound("daThanhToan.greaterThanOrEqual=" + UPDATED_DA_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDaThanhToanIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where daThanhToan is less than or equal to DEFAULT_DA_THANH_TOAN
        defaultChiDinhXetNghiemShouldBeFound("daThanhToan.lessThanOrEqual=" + DEFAULT_DA_THANH_TOAN);

        // Get all the chiDinhXetNghiemList where daThanhToan is less than or equal to SMALLER_DA_THANH_TOAN
        defaultChiDinhXetNghiemShouldNotBeFound("daThanhToan.lessThanOrEqual=" + SMALLER_DA_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDaThanhToanIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where daThanhToan is less than DEFAULT_DA_THANH_TOAN
        defaultChiDinhXetNghiemShouldNotBeFound("daThanhToan.lessThan=" + DEFAULT_DA_THANH_TOAN);

        // Get all the chiDinhXetNghiemList where daThanhToan is less than UPDATED_DA_THANH_TOAN
        defaultChiDinhXetNghiemShouldBeFound("daThanhToan.lessThan=" + UPDATED_DA_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDaThanhToanIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where daThanhToan is greater than DEFAULT_DA_THANH_TOAN
        defaultChiDinhXetNghiemShouldNotBeFound("daThanhToan.greaterThan=" + DEFAULT_DA_THANH_TOAN);

        // Get all the chiDinhXetNghiemList where daThanhToan is greater than SMALLER_DA_THANH_TOAN
        defaultChiDinhXetNghiemShouldBeFound("daThanhToan.greaterThan=" + SMALLER_DA_THANH_TOAN);
    }


    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDaThanhToanChenhLechIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where daThanhToanChenhLech equals to DEFAULT_DA_THANH_TOAN_CHENH_LECH
        defaultChiDinhXetNghiemShouldBeFound("daThanhToanChenhLech.equals=" + DEFAULT_DA_THANH_TOAN_CHENH_LECH);

        // Get all the chiDinhXetNghiemList where daThanhToanChenhLech equals to UPDATED_DA_THANH_TOAN_CHENH_LECH
        defaultChiDinhXetNghiemShouldNotBeFound("daThanhToanChenhLech.equals=" + UPDATED_DA_THANH_TOAN_CHENH_LECH);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDaThanhToanChenhLechIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where daThanhToanChenhLech not equals to DEFAULT_DA_THANH_TOAN_CHENH_LECH
        defaultChiDinhXetNghiemShouldNotBeFound("daThanhToanChenhLech.notEquals=" + DEFAULT_DA_THANH_TOAN_CHENH_LECH);

        // Get all the chiDinhXetNghiemList where daThanhToanChenhLech not equals to UPDATED_DA_THANH_TOAN_CHENH_LECH
        defaultChiDinhXetNghiemShouldBeFound("daThanhToanChenhLech.notEquals=" + UPDATED_DA_THANH_TOAN_CHENH_LECH);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDaThanhToanChenhLechIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where daThanhToanChenhLech in DEFAULT_DA_THANH_TOAN_CHENH_LECH or UPDATED_DA_THANH_TOAN_CHENH_LECH
        defaultChiDinhXetNghiemShouldBeFound("daThanhToanChenhLech.in=" + DEFAULT_DA_THANH_TOAN_CHENH_LECH + "," + UPDATED_DA_THANH_TOAN_CHENH_LECH);

        // Get all the chiDinhXetNghiemList where daThanhToanChenhLech equals to UPDATED_DA_THANH_TOAN_CHENH_LECH
        defaultChiDinhXetNghiemShouldNotBeFound("daThanhToanChenhLech.in=" + UPDATED_DA_THANH_TOAN_CHENH_LECH);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDaThanhToanChenhLechIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where daThanhToanChenhLech is not null
        defaultChiDinhXetNghiemShouldBeFound("daThanhToanChenhLech.specified=true");

        // Get all the chiDinhXetNghiemList where daThanhToanChenhLech is null
        defaultChiDinhXetNghiemShouldNotBeFound("daThanhToanChenhLech.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDaThanhToanChenhLechIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where daThanhToanChenhLech is greater than or equal to DEFAULT_DA_THANH_TOAN_CHENH_LECH
        defaultChiDinhXetNghiemShouldBeFound("daThanhToanChenhLech.greaterThanOrEqual=" + DEFAULT_DA_THANH_TOAN_CHENH_LECH);

        // Get all the chiDinhXetNghiemList where daThanhToanChenhLech is greater than or equal to UPDATED_DA_THANH_TOAN_CHENH_LECH
        defaultChiDinhXetNghiemShouldNotBeFound("daThanhToanChenhLech.greaterThanOrEqual=" + UPDATED_DA_THANH_TOAN_CHENH_LECH);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDaThanhToanChenhLechIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where daThanhToanChenhLech is less than or equal to DEFAULT_DA_THANH_TOAN_CHENH_LECH
        defaultChiDinhXetNghiemShouldBeFound("daThanhToanChenhLech.lessThanOrEqual=" + DEFAULT_DA_THANH_TOAN_CHENH_LECH);

        // Get all the chiDinhXetNghiemList where daThanhToanChenhLech is less than or equal to SMALLER_DA_THANH_TOAN_CHENH_LECH
        defaultChiDinhXetNghiemShouldNotBeFound("daThanhToanChenhLech.lessThanOrEqual=" + SMALLER_DA_THANH_TOAN_CHENH_LECH);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDaThanhToanChenhLechIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where daThanhToanChenhLech is less than DEFAULT_DA_THANH_TOAN_CHENH_LECH
        defaultChiDinhXetNghiemShouldNotBeFound("daThanhToanChenhLech.lessThan=" + DEFAULT_DA_THANH_TOAN_CHENH_LECH);

        // Get all the chiDinhXetNghiemList where daThanhToanChenhLech is less than UPDATED_DA_THANH_TOAN_CHENH_LECH
        defaultChiDinhXetNghiemShouldBeFound("daThanhToanChenhLech.lessThan=" + UPDATED_DA_THANH_TOAN_CHENH_LECH);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDaThanhToanChenhLechIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where daThanhToanChenhLech is greater than DEFAULT_DA_THANH_TOAN_CHENH_LECH
        defaultChiDinhXetNghiemShouldNotBeFound("daThanhToanChenhLech.greaterThan=" + DEFAULT_DA_THANH_TOAN_CHENH_LECH);

        // Get all the chiDinhXetNghiemList where daThanhToanChenhLech is greater than SMALLER_DA_THANH_TOAN_CHENH_LECH
        defaultChiDinhXetNghiemShouldBeFound("daThanhToanChenhLech.greaterThan=" + SMALLER_DA_THANH_TOAN_CHENH_LECH);
    }


    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDaThucHienIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where daThucHien equals to DEFAULT_DA_THUC_HIEN
        defaultChiDinhXetNghiemShouldBeFound("daThucHien.equals=" + DEFAULT_DA_THUC_HIEN);

        // Get all the chiDinhXetNghiemList where daThucHien equals to UPDATED_DA_THUC_HIEN
        defaultChiDinhXetNghiemShouldNotBeFound("daThucHien.equals=" + UPDATED_DA_THUC_HIEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDaThucHienIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where daThucHien not equals to DEFAULT_DA_THUC_HIEN
        defaultChiDinhXetNghiemShouldNotBeFound("daThucHien.notEquals=" + DEFAULT_DA_THUC_HIEN);

        // Get all the chiDinhXetNghiemList where daThucHien not equals to UPDATED_DA_THUC_HIEN
        defaultChiDinhXetNghiemShouldBeFound("daThucHien.notEquals=" + UPDATED_DA_THUC_HIEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDaThucHienIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where daThucHien in DEFAULT_DA_THUC_HIEN or UPDATED_DA_THUC_HIEN
        defaultChiDinhXetNghiemShouldBeFound("daThucHien.in=" + DEFAULT_DA_THUC_HIEN + "," + UPDATED_DA_THUC_HIEN);

        // Get all the chiDinhXetNghiemList where daThucHien equals to UPDATED_DA_THUC_HIEN
        defaultChiDinhXetNghiemShouldNotBeFound("daThucHien.in=" + UPDATED_DA_THUC_HIEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDaThucHienIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where daThucHien is not null
        defaultChiDinhXetNghiemShouldBeFound("daThucHien.specified=true");

        // Get all the chiDinhXetNghiemList where daThucHien is null
        defaultChiDinhXetNghiemShouldNotBeFound("daThucHien.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDaThucHienIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where daThucHien is greater than or equal to DEFAULT_DA_THUC_HIEN
        defaultChiDinhXetNghiemShouldBeFound("daThucHien.greaterThanOrEqual=" + DEFAULT_DA_THUC_HIEN);

        // Get all the chiDinhXetNghiemList where daThucHien is greater than or equal to UPDATED_DA_THUC_HIEN
        defaultChiDinhXetNghiemShouldNotBeFound("daThucHien.greaterThanOrEqual=" + UPDATED_DA_THUC_HIEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDaThucHienIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where daThucHien is less than or equal to DEFAULT_DA_THUC_HIEN
        defaultChiDinhXetNghiemShouldBeFound("daThucHien.lessThanOrEqual=" + DEFAULT_DA_THUC_HIEN);

        // Get all the chiDinhXetNghiemList where daThucHien is less than or equal to SMALLER_DA_THUC_HIEN
        defaultChiDinhXetNghiemShouldNotBeFound("daThucHien.lessThanOrEqual=" + SMALLER_DA_THUC_HIEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDaThucHienIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where daThucHien is less than DEFAULT_DA_THUC_HIEN
        defaultChiDinhXetNghiemShouldNotBeFound("daThucHien.lessThan=" + DEFAULT_DA_THUC_HIEN);

        // Get all the chiDinhXetNghiemList where daThucHien is less than UPDATED_DA_THUC_HIEN
        defaultChiDinhXetNghiemShouldBeFound("daThucHien.lessThan=" + UPDATED_DA_THUC_HIEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDaThucHienIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where daThucHien is greater than DEFAULT_DA_THUC_HIEN
        defaultChiDinhXetNghiemShouldNotBeFound("daThucHien.greaterThan=" + DEFAULT_DA_THUC_HIEN);

        // Get all the chiDinhXetNghiemList where daThucHien is greater than SMALLER_DA_THUC_HIEN
        defaultChiDinhXetNghiemShouldBeFound("daThucHien.greaterThan=" + SMALLER_DA_THUC_HIEN);
    }


    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDonGiaIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where donGia equals to DEFAULT_DON_GIA
        defaultChiDinhXetNghiemShouldBeFound("donGia.equals=" + DEFAULT_DON_GIA);

        // Get all the chiDinhXetNghiemList where donGia equals to UPDATED_DON_GIA
        defaultChiDinhXetNghiemShouldNotBeFound("donGia.equals=" + UPDATED_DON_GIA);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDonGiaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where donGia not equals to DEFAULT_DON_GIA
        defaultChiDinhXetNghiemShouldNotBeFound("donGia.notEquals=" + DEFAULT_DON_GIA);

        // Get all the chiDinhXetNghiemList where donGia not equals to UPDATED_DON_GIA
        defaultChiDinhXetNghiemShouldBeFound("donGia.notEquals=" + UPDATED_DON_GIA);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDonGiaIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where donGia in DEFAULT_DON_GIA or UPDATED_DON_GIA
        defaultChiDinhXetNghiemShouldBeFound("donGia.in=" + DEFAULT_DON_GIA + "," + UPDATED_DON_GIA);

        // Get all the chiDinhXetNghiemList where donGia equals to UPDATED_DON_GIA
        defaultChiDinhXetNghiemShouldNotBeFound("donGia.in=" + UPDATED_DON_GIA);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDonGiaIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where donGia is not null
        defaultChiDinhXetNghiemShouldBeFound("donGia.specified=true");

        // Get all the chiDinhXetNghiemList where donGia is null
        defaultChiDinhXetNghiemShouldNotBeFound("donGia.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDonGiaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where donGia is greater than or equal to DEFAULT_DON_GIA
        defaultChiDinhXetNghiemShouldBeFound("donGia.greaterThanOrEqual=" + DEFAULT_DON_GIA);

        // Get all the chiDinhXetNghiemList where donGia is greater than or equal to UPDATED_DON_GIA
        defaultChiDinhXetNghiemShouldNotBeFound("donGia.greaterThanOrEqual=" + UPDATED_DON_GIA);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDonGiaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where donGia is less than or equal to DEFAULT_DON_GIA
        defaultChiDinhXetNghiemShouldBeFound("donGia.lessThanOrEqual=" + DEFAULT_DON_GIA);

        // Get all the chiDinhXetNghiemList where donGia is less than or equal to SMALLER_DON_GIA
        defaultChiDinhXetNghiemShouldNotBeFound("donGia.lessThanOrEqual=" + SMALLER_DON_GIA);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDonGiaIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where donGia is less than DEFAULT_DON_GIA
        defaultChiDinhXetNghiemShouldNotBeFound("donGia.lessThan=" + DEFAULT_DON_GIA);

        // Get all the chiDinhXetNghiemList where donGia is less than UPDATED_DON_GIA
        defaultChiDinhXetNghiemShouldBeFound("donGia.lessThan=" + UPDATED_DON_GIA);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDonGiaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where donGia is greater than DEFAULT_DON_GIA
        defaultChiDinhXetNghiemShouldNotBeFound("donGia.greaterThan=" + DEFAULT_DON_GIA);

        // Get all the chiDinhXetNghiemList where donGia is greater than SMALLER_DON_GIA
        defaultChiDinhXetNghiemShouldBeFound("donGia.greaterThan=" + SMALLER_DON_GIA);
    }


    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDonGiaBhytIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where donGiaBhyt equals to DEFAULT_DON_GIA_BHYT
        defaultChiDinhXetNghiemShouldBeFound("donGiaBhyt.equals=" + DEFAULT_DON_GIA_BHYT);

        // Get all the chiDinhXetNghiemList where donGiaBhyt equals to UPDATED_DON_GIA_BHYT
        defaultChiDinhXetNghiemShouldNotBeFound("donGiaBhyt.equals=" + UPDATED_DON_GIA_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDonGiaBhytIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where donGiaBhyt not equals to DEFAULT_DON_GIA_BHYT
        defaultChiDinhXetNghiemShouldNotBeFound("donGiaBhyt.notEquals=" + DEFAULT_DON_GIA_BHYT);

        // Get all the chiDinhXetNghiemList where donGiaBhyt not equals to UPDATED_DON_GIA_BHYT
        defaultChiDinhXetNghiemShouldBeFound("donGiaBhyt.notEquals=" + UPDATED_DON_GIA_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDonGiaBhytIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where donGiaBhyt in DEFAULT_DON_GIA_BHYT or UPDATED_DON_GIA_BHYT
        defaultChiDinhXetNghiemShouldBeFound("donGiaBhyt.in=" + DEFAULT_DON_GIA_BHYT + "," + UPDATED_DON_GIA_BHYT);

        // Get all the chiDinhXetNghiemList where donGiaBhyt equals to UPDATED_DON_GIA_BHYT
        defaultChiDinhXetNghiemShouldNotBeFound("donGiaBhyt.in=" + UPDATED_DON_GIA_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDonGiaBhytIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where donGiaBhyt is not null
        defaultChiDinhXetNghiemShouldBeFound("donGiaBhyt.specified=true");

        // Get all the chiDinhXetNghiemList where donGiaBhyt is null
        defaultChiDinhXetNghiemShouldNotBeFound("donGiaBhyt.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDonGiaBhytIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where donGiaBhyt is greater than or equal to DEFAULT_DON_GIA_BHYT
        defaultChiDinhXetNghiemShouldBeFound("donGiaBhyt.greaterThanOrEqual=" + DEFAULT_DON_GIA_BHYT);

        // Get all the chiDinhXetNghiemList where donGiaBhyt is greater than or equal to UPDATED_DON_GIA_BHYT
        defaultChiDinhXetNghiemShouldNotBeFound("donGiaBhyt.greaterThanOrEqual=" + UPDATED_DON_GIA_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDonGiaBhytIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where donGiaBhyt is less than or equal to DEFAULT_DON_GIA_BHYT
        defaultChiDinhXetNghiemShouldBeFound("donGiaBhyt.lessThanOrEqual=" + DEFAULT_DON_GIA_BHYT);

        // Get all the chiDinhXetNghiemList where donGiaBhyt is less than or equal to SMALLER_DON_GIA_BHYT
        defaultChiDinhXetNghiemShouldNotBeFound("donGiaBhyt.lessThanOrEqual=" + SMALLER_DON_GIA_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDonGiaBhytIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where donGiaBhyt is less than DEFAULT_DON_GIA_BHYT
        defaultChiDinhXetNghiemShouldNotBeFound("donGiaBhyt.lessThan=" + DEFAULT_DON_GIA_BHYT);

        // Get all the chiDinhXetNghiemList where donGiaBhyt is less than UPDATED_DON_GIA_BHYT
        defaultChiDinhXetNghiemShouldBeFound("donGiaBhyt.lessThan=" + UPDATED_DON_GIA_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDonGiaBhytIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where donGiaBhyt is greater than DEFAULT_DON_GIA_BHYT
        defaultChiDinhXetNghiemShouldNotBeFound("donGiaBhyt.greaterThan=" + DEFAULT_DON_GIA_BHYT);

        // Get all the chiDinhXetNghiemList where donGiaBhyt is greater than SMALLER_DON_GIA_BHYT
        defaultChiDinhXetNghiemShouldBeFound("donGiaBhyt.greaterThan=" + SMALLER_DON_GIA_BHYT);
    }


    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDonGiaKhongBhytIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where donGiaKhongBhyt equals to DEFAULT_DON_GIA_KHONG_BHYT
        defaultChiDinhXetNghiemShouldBeFound("donGiaKhongBhyt.equals=" + DEFAULT_DON_GIA_KHONG_BHYT);

        // Get all the chiDinhXetNghiemList where donGiaKhongBhyt equals to UPDATED_DON_GIA_KHONG_BHYT
        defaultChiDinhXetNghiemShouldNotBeFound("donGiaKhongBhyt.equals=" + UPDATED_DON_GIA_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDonGiaKhongBhytIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where donGiaKhongBhyt not equals to DEFAULT_DON_GIA_KHONG_BHYT
        defaultChiDinhXetNghiemShouldNotBeFound("donGiaKhongBhyt.notEquals=" + DEFAULT_DON_GIA_KHONG_BHYT);

        // Get all the chiDinhXetNghiemList where donGiaKhongBhyt not equals to UPDATED_DON_GIA_KHONG_BHYT
        defaultChiDinhXetNghiemShouldBeFound("donGiaKhongBhyt.notEquals=" + UPDATED_DON_GIA_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDonGiaKhongBhytIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where donGiaKhongBhyt in DEFAULT_DON_GIA_KHONG_BHYT or UPDATED_DON_GIA_KHONG_BHYT
        defaultChiDinhXetNghiemShouldBeFound("donGiaKhongBhyt.in=" + DEFAULT_DON_GIA_KHONG_BHYT + "," + UPDATED_DON_GIA_KHONG_BHYT);

        // Get all the chiDinhXetNghiemList where donGiaKhongBhyt equals to UPDATED_DON_GIA_KHONG_BHYT
        defaultChiDinhXetNghiemShouldNotBeFound("donGiaKhongBhyt.in=" + UPDATED_DON_GIA_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDonGiaKhongBhytIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where donGiaKhongBhyt is not null
        defaultChiDinhXetNghiemShouldBeFound("donGiaKhongBhyt.specified=true");

        // Get all the chiDinhXetNghiemList where donGiaKhongBhyt is null
        defaultChiDinhXetNghiemShouldNotBeFound("donGiaKhongBhyt.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDonGiaKhongBhytIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where donGiaKhongBhyt is greater than or equal to DEFAULT_DON_GIA_KHONG_BHYT
        defaultChiDinhXetNghiemShouldBeFound("donGiaKhongBhyt.greaterThanOrEqual=" + DEFAULT_DON_GIA_KHONG_BHYT);

        // Get all the chiDinhXetNghiemList where donGiaKhongBhyt is greater than or equal to UPDATED_DON_GIA_KHONG_BHYT
        defaultChiDinhXetNghiemShouldNotBeFound("donGiaKhongBhyt.greaterThanOrEqual=" + UPDATED_DON_GIA_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDonGiaKhongBhytIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where donGiaKhongBhyt is less than or equal to DEFAULT_DON_GIA_KHONG_BHYT
        defaultChiDinhXetNghiemShouldBeFound("donGiaKhongBhyt.lessThanOrEqual=" + DEFAULT_DON_GIA_KHONG_BHYT);

        // Get all the chiDinhXetNghiemList where donGiaKhongBhyt is less than or equal to SMALLER_DON_GIA_KHONG_BHYT
        defaultChiDinhXetNghiemShouldNotBeFound("donGiaKhongBhyt.lessThanOrEqual=" + SMALLER_DON_GIA_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDonGiaKhongBhytIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where donGiaKhongBhyt is less than DEFAULT_DON_GIA_KHONG_BHYT
        defaultChiDinhXetNghiemShouldNotBeFound("donGiaKhongBhyt.lessThan=" + DEFAULT_DON_GIA_KHONG_BHYT);

        // Get all the chiDinhXetNghiemList where donGiaKhongBhyt is less than UPDATED_DON_GIA_KHONG_BHYT
        defaultChiDinhXetNghiemShouldBeFound("donGiaKhongBhyt.lessThan=" + UPDATED_DON_GIA_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDonGiaKhongBhytIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where donGiaKhongBhyt is greater than DEFAULT_DON_GIA_KHONG_BHYT
        defaultChiDinhXetNghiemShouldNotBeFound("donGiaKhongBhyt.greaterThan=" + DEFAULT_DON_GIA_KHONG_BHYT);

        // Get all the chiDinhXetNghiemList where donGiaKhongBhyt is greater than SMALLER_DON_GIA_KHONG_BHYT
        defaultChiDinhXetNghiemShouldBeFound("donGiaKhongBhyt.greaterThan=" + SMALLER_DON_GIA_KHONG_BHYT);
    }


    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByGhiChuChiDinhIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where ghiChuChiDinh equals to DEFAULT_GHI_CHU_CHI_DINH
        defaultChiDinhXetNghiemShouldBeFound("ghiChuChiDinh.equals=" + DEFAULT_GHI_CHU_CHI_DINH);

        // Get all the chiDinhXetNghiemList where ghiChuChiDinh equals to UPDATED_GHI_CHU_CHI_DINH
        defaultChiDinhXetNghiemShouldNotBeFound("ghiChuChiDinh.equals=" + UPDATED_GHI_CHU_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByGhiChuChiDinhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where ghiChuChiDinh not equals to DEFAULT_GHI_CHU_CHI_DINH
        defaultChiDinhXetNghiemShouldNotBeFound("ghiChuChiDinh.notEquals=" + DEFAULT_GHI_CHU_CHI_DINH);

        // Get all the chiDinhXetNghiemList where ghiChuChiDinh not equals to UPDATED_GHI_CHU_CHI_DINH
        defaultChiDinhXetNghiemShouldBeFound("ghiChuChiDinh.notEquals=" + UPDATED_GHI_CHU_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByGhiChuChiDinhIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where ghiChuChiDinh in DEFAULT_GHI_CHU_CHI_DINH or UPDATED_GHI_CHU_CHI_DINH
        defaultChiDinhXetNghiemShouldBeFound("ghiChuChiDinh.in=" + DEFAULT_GHI_CHU_CHI_DINH + "," + UPDATED_GHI_CHU_CHI_DINH);

        // Get all the chiDinhXetNghiemList where ghiChuChiDinh equals to UPDATED_GHI_CHU_CHI_DINH
        defaultChiDinhXetNghiemShouldNotBeFound("ghiChuChiDinh.in=" + UPDATED_GHI_CHU_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByGhiChuChiDinhIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where ghiChuChiDinh is not null
        defaultChiDinhXetNghiemShouldBeFound("ghiChuChiDinh.specified=true");

        // Get all the chiDinhXetNghiemList where ghiChuChiDinh is null
        defaultChiDinhXetNghiemShouldNotBeFound("ghiChuChiDinh.specified=false");
    }
                @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByGhiChuChiDinhContainsSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where ghiChuChiDinh contains DEFAULT_GHI_CHU_CHI_DINH
        defaultChiDinhXetNghiemShouldBeFound("ghiChuChiDinh.contains=" + DEFAULT_GHI_CHU_CHI_DINH);

        // Get all the chiDinhXetNghiemList where ghiChuChiDinh contains UPDATED_GHI_CHU_CHI_DINH
        defaultChiDinhXetNghiemShouldNotBeFound("ghiChuChiDinh.contains=" + UPDATED_GHI_CHU_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByGhiChuChiDinhNotContainsSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where ghiChuChiDinh does not contain DEFAULT_GHI_CHU_CHI_DINH
        defaultChiDinhXetNghiemShouldNotBeFound("ghiChuChiDinh.doesNotContain=" + DEFAULT_GHI_CHU_CHI_DINH);

        // Get all the chiDinhXetNghiemList where ghiChuChiDinh does not contain UPDATED_GHI_CHU_CHI_DINH
        defaultChiDinhXetNghiemShouldBeFound("ghiChuChiDinh.doesNotContain=" + UPDATED_GHI_CHU_CHI_DINH);
    }


    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByMoTaIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where moTa equals to DEFAULT_MO_TA
        defaultChiDinhXetNghiemShouldBeFound("moTa.equals=" + DEFAULT_MO_TA);

        // Get all the chiDinhXetNghiemList where moTa equals to UPDATED_MO_TA
        defaultChiDinhXetNghiemShouldNotBeFound("moTa.equals=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByMoTaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where moTa not equals to DEFAULT_MO_TA
        defaultChiDinhXetNghiemShouldNotBeFound("moTa.notEquals=" + DEFAULT_MO_TA);

        // Get all the chiDinhXetNghiemList where moTa not equals to UPDATED_MO_TA
        defaultChiDinhXetNghiemShouldBeFound("moTa.notEquals=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByMoTaIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where moTa in DEFAULT_MO_TA or UPDATED_MO_TA
        defaultChiDinhXetNghiemShouldBeFound("moTa.in=" + DEFAULT_MO_TA + "," + UPDATED_MO_TA);

        // Get all the chiDinhXetNghiemList where moTa equals to UPDATED_MO_TA
        defaultChiDinhXetNghiemShouldNotBeFound("moTa.in=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByMoTaIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where moTa is not null
        defaultChiDinhXetNghiemShouldBeFound("moTa.specified=true");

        // Get all the chiDinhXetNghiemList where moTa is null
        defaultChiDinhXetNghiemShouldNotBeFound("moTa.specified=false");
    }
                @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByMoTaContainsSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where moTa contains DEFAULT_MO_TA
        defaultChiDinhXetNghiemShouldBeFound("moTa.contains=" + DEFAULT_MO_TA);

        // Get all the chiDinhXetNghiemList where moTa contains UPDATED_MO_TA
        defaultChiDinhXetNghiemShouldNotBeFound("moTa.contains=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByMoTaNotContainsSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where moTa does not contain DEFAULT_MO_TA
        defaultChiDinhXetNghiemShouldNotBeFound("moTa.doesNotContain=" + DEFAULT_MO_TA);

        // Get all the chiDinhXetNghiemList where moTa does not contain UPDATED_MO_TA
        defaultChiDinhXetNghiemShouldBeFound("moTa.doesNotContain=" + UPDATED_MO_TA);
    }


    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByNguoiChiDinhIdIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where nguoiChiDinhId equals to DEFAULT_NGUOI_CHI_DINH_ID
        defaultChiDinhXetNghiemShouldBeFound("nguoiChiDinhId.equals=" + DEFAULT_NGUOI_CHI_DINH_ID);

        // Get all the chiDinhXetNghiemList where nguoiChiDinhId equals to UPDATED_NGUOI_CHI_DINH_ID
        defaultChiDinhXetNghiemShouldNotBeFound("nguoiChiDinhId.equals=" + UPDATED_NGUOI_CHI_DINH_ID);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByNguoiChiDinhIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where nguoiChiDinhId not equals to DEFAULT_NGUOI_CHI_DINH_ID
        defaultChiDinhXetNghiemShouldNotBeFound("nguoiChiDinhId.notEquals=" + DEFAULT_NGUOI_CHI_DINH_ID);

        // Get all the chiDinhXetNghiemList where nguoiChiDinhId not equals to UPDATED_NGUOI_CHI_DINH_ID
        defaultChiDinhXetNghiemShouldBeFound("nguoiChiDinhId.notEquals=" + UPDATED_NGUOI_CHI_DINH_ID);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByNguoiChiDinhIdIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where nguoiChiDinhId in DEFAULT_NGUOI_CHI_DINH_ID or UPDATED_NGUOI_CHI_DINH_ID
        defaultChiDinhXetNghiemShouldBeFound("nguoiChiDinhId.in=" + DEFAULT_NGUOI_CHI_DINH_ID + "," + UPDATED_NGUOI_CHI_DINH_ID);

        // Get all the chiDinhXetNghiemList where nguoiChiDinhId equals to UPDATED_NGUOI_CHI_DINH_ID
        defaultChiDinhXetNghiemShouldNotBeFound("nguoiChiDinhId.in=" + UPDATED_NGUOI_CHI_DINH_ID);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByNguoiChiDinhIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where nguoiChiDinhId is not null
        defaultChiDinhXetNghiemShouldBeFound("nguoiChiDinhId.specified=true");

        // Get all the chiDinhXetNghiemList where nguoiChiDinhId is null
        defaultChiDinhXetNghiemShouldNotBeFound("nguoiChiDinhId.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByNguoiChiDinhIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where nguoiChiDinhId is greater than or equal to DEFAULT_NGUOI_CHI_DINH_ID
        defaultChiDinhXetNghiemShouldBeFound("nguoiChiDinhId.greaterThanOrEqual=" + DEFAULT_NGUOI_CHI_DINH_ID);

        // Get all the chiDinhXetNghiemList where nguoiChiDinhId is greater than or equal to UPDATED_NGUOI_CHI_DINH_ID
        defaultChiDinhXetNghiemShouldNotBeFound("nguoiChiDinhId.greaterThanOrEqual=" + UPDATED_NGUOI_CHI_DINH_ID);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByNguoiChiDinhIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where nguoiChiDinhId is less than or equal to DEFAULT_NGUOI_CHI_DINH_ID
        defaultChiDinhXetNghiemShouldBeFound("nguoiChiDinhId.lessThanOrEqual=" + DEFAULT_NGUOI_CHI_DINH_ID);

        // Get all the chiDinhXetNghiemList where nguoiChiDinhId is less than or equal to SMALLER_NGUOI_CHI_DINH_ID
        defaultChiDinhXetNghiemShouldNotBeFound("nguoiChiDinhId.lessThanOrEqual=" + SMALLER_NGUOI_CHI_DINH_ID);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByNguoiChiDinhIdIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where nguoiChiDinhId is less than DEFAULT_NGUOI_CHI_DINH_ID
        defaultChiDinhXetNghiemShouldNotBeFound("nguoiChiDinhId.lessThan=" + DEFAULT_NGUOI_CHI_DINH_ID);

        // Get all the chiDinhXetNghiemList where nguoiChiDinhId is less than UPDATED_NGUOI_CHI_DINH_ID
        defaultChiDinhXetNghiemShouldBeFound("nguoiChiDinhId.lessThan=" + UPDATED_NGUOI_CHI_DINH_ID);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByNguoiChiDinhIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where nguoiChiDinhId is greater than DEFAULT_NGUOI_CHI_DINH_ID
        defaultChiDinhXetNghiemShouldNotBeFound("nguoiChiDinhId.greaterThan=" + DEFAULT_NGUOI_CHI_DINH_ID);

        // Get all the chiDinhXetNghiemList where nguoiChiDinhId is greater than SMALLER_NGUOI_CHI_DINH_ID
        defaultChiDinhXetNghiemShouldBeFound("nguoiChiDinhId.greaterThan=" + SMALLER_NGUOI_CHI_DINH_ID);
    }


    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsBySoLuongIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where soLuong equals to DEFAULT_SO_LUONG
        defaultChiDinhXetNghiemShouldBeFound("soLuong.equals=" + DEFAULT_SO_LUONG);

        // Get all the chiDinhXetNghiemList where soLuong equals to UPDATED_SO_LUONG
        defaultChiDinhXetNghiemShouldNotBeFound("soLuong.equals=" + UPDATED_SO_LUONG);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsBySoLuongIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where soLuong not equals to DEFAULT_SO_LUONG
        defaultChiDinhXetNghiemShouldNotBeFound("soLuong.notEquals=" + DEFAULT_SO_LUONG);

        // Get all the chiDinhXetNghiemList where soLuong not equals to UPDATED_SO_LUONG
        defaultChiDinhXetNghiemShouldBeFound("soLuong.notEquals=" + UPDATED_SO_LUONG);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsBySoLuongIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where soLuong in DEFAULT_SO_LUONG or UPDATED_SO_LUONG
        defaultChiDinhXetNghiemShouldBeFound("soLuong.in=" + DEFAULT_SO_LUONG + "," + UPDATED_SO_LUONG);

        // Get all the chiDinhXetNghiemList where soLuong equals to UPDATED_SO_LUONG
        defaultChiDinhXetNghiemShouldNotBeFound("soLuong.in=" + UPDATED_SO_LUONG);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsBySoLuongIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where soLuong is not null
        defaultChiDinhXetNghiemShouldBeFound("soLuong.specified=true");

        // Get all the chiDinhXetNghiemList where soLuong is null
        defaultChiDinhXetNghiemShouldNotBeFound("soLuong.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsBySoLuongIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where soLuong is greater than or equal to DEFAULT_SO_LUONG
        defaultChiDinhXetNghiemShouldBeFound("soLuong.greaterThanOrEqual=" + DEFAULT_SO_LUONG);

        // Get all the chiDinhXetNghiemList where soLuong is greater than or equal to UPDATED_SO_LUONG
        defaultChiDinhXetNghiemShouldNotBeFound("soLuong.greaterThanOrEqual=" + UPDATED_SO_LUONG);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsBySoLuongIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where soLuong is less than or equal to DEFAULT_SO_LUONG
        defaultChiDinhXetNghiemShouldBeFound("soLuong.lessThanOrEqual=" + DEFAULT_SO_LUONG);

        // Get all the chiDinhXetNghiemList where soLuong is less than or equal to SMALLER_SO_LUONG
        defaultChiDinhXetNghiemShouldNotBeFound("soLuong.lessThanOrEqual=" + SMALLER_SO_LUONG);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsBySoLuongIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where soLuong is less than DEFAULT_SO_LUONG
        defaultChiDinhXetNghiemShouldNotBeFound("soLuong.lessThan=" + DEFAULT_SO_LUONG);

        // Get all the chiDinhXetNghiemList where soLuong is less than UPDATED_SO_LUONG
        defaultChiDinhXetNghiemShouldBeFound("soLuong.lessThan=" + UPDATED_SO_LUONG);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsBySoLuongIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where soLuong is greater than DEFAULT_SO_LUONG
        defaultChiDinhXetNghiemShouldNotBeFound("soLuong.greaterThan=" + DEFAULT_SO_LUONG);

        // Get all the chiDinhXetNghiemList where soLuong is greater than SMALLER_SO_LUONG
        defaultChiDinhXetNghiemShouldBeFound("soLuong.greaterThan=" + SMALLER_SO_LUONG);
    }


    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByTenIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where ten equals to DEFAULT_TEN
        defaultChiDinhXetNghiemShouldBeFound("ten.equals=" + DEFAULT_TEN);

        // Get all the chiDinhXetNghiemList where ten equals to UPDATED_TEN
        defaultChiDinhXetNghiemShouldNotBeFound("ten.equals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByTenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where ten not equals to DEFAULT_TEN
        defaultChiDinhXetNghiemShouldNotBeFound("ten.notEquals=" + DEFAULT_TEN);

        // Get all the chiDinhXetNghiemList where ten not equals to UPDATED_TEN
        defaultChiDinhXetNghiemShouldBeFound("ten.notEquals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByTenIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where ten in DEFAULT_TEN or UPDATED_TEN
        defaultChiDinhXetNghiemShouldBeFound("ten.in=" + DEFAULT_TEN + "," + UPDATED_TEN);

        // Get all the chiDinhXetNghiemList where ten equals to UPDATED_TEN
        defaultChiDinhXetNghiemShouldNotBeFound("ten.in=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByTenIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where ten is not null
        defaultChiDinhXetNghiemShouldBeFound("ten.specified=true");

        // Get all the chiDinhXetNghiemList where ten is null
        defaultChiDinhXetNghiemShouldNotBeFound("ten.specified=false");
    }
                @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByTenContainsSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where ten contains DEFAULT_TEN
        defaultChiDinhXetNghiemShouldBeFound("ten.contains=" + DEFAULT_TEN);

        // Get all the chiDinhXetNghiemList where ten contains UPDATED_TEN
        defaultChiDinhXetNghiemShouldNotBeFound("ten.contains=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByTenNotContainsSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where ten does not contain DEFAULT_TEN
        defaultChiDinhXetNghiemShouldNotBeFound("ten.doesNotContain=" + DEFAULT_TEN);

        // Get all the chiDinhXetNghiemList where ten does not contain UPDATED_TEN
        defaultChiDinhXetNghiemShouldBeFound("ten.doesNotContain=" + UPDATED_TEN);
    }


    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByThanhTienIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where thanhTien equals to DEFAULT_THANH_TIEN
        defaultChiDinhXetNghiemShouldBeFound("thanhTien.equals=" + DEFAULT_THANH_TIEN);

        // Get all the chiDinhXetNghiemList where thanhTien equals to UPDATED_THANH_TIEN
        defaultChiDinhXetNghiemShouldNotBeFound("thanhTien.equals=" + UPDATED_THANH_TIEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByThanhTienIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where thanhTien not equals to DEFAULT_THANH_TIEN
        defaultChiDinhXetNghiemShouldNotBeFound("thanhTien.notEquals=" + DEFAULT_THANH_TIEN);

        // Get all the chiDinhXetNghiemList where thanhTien not equals to UPDATED_THANH_TIEN
        defaultChiDinhXetNghiemShouldBeFound("thanhTien.notEquals=" + UPDATED_THANH_TIEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByThanhTienIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where thanhTien in DEFAULT_THANH_TIEN or UPDATED_THANH_TIEN
        defaultChiDinhXetNghiemShouldBeFound("thanhTien.in=" + DEFAULT_THANH_TIEN + "," + UPDATED_THANH_TIEN);

        // Get all the chiDinhXetNghiemList where thanhTien equals to UPDATED_THANH_TIEN
        defaultChiDinhXetNghiemShouldNotBeFound("thanhTien.in=" + UPDATED_THANH_TIEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByThanhTienIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where thanhTien is not null
        defaultChiDinhXetNghiemShouldBeFound("thanhTien.specified=true");

        // Get all the chiDinhXetNghiemList where thanhTien is null
        defaultChiDinhXetNghiemShouldNotBeFound("thanhTien.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByThanhTienIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where thanhTien is greater than or equal to DEFAULT_THANH_TIEN
        defaultChiDinhXetNghiemShouldBeFound("thanhTien.greaterThanOrEqual=" + DEFAULT_THANH_TIEN);

        // Get all the chiDinhXetNghiemList where thanhTien is greater than or equal to UPDATED_THANH_TIEN
        defaultChiDinhXetNghiemShouldNotBeFound("thanhTien.greaterThanOrEqual=" + UPDATED_THANH_TIEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByThanhTienIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where thanhTien is less than or equal to DEFAULT_THANH_TIEN
        defaultChiDinhXetNghiemShouldBeFound("thanhTien.lessThanOrEqual=" + DEFAULT_THANH_TIEN);

        // Get all the chiDinhXetNghiemList where thanhTien is less than or equal to SMALLER_THANH_TIEN
        defaultChiDinhXetNghiemShouldNotBeFound("thanhTien.lessThanOrEqual=" + SMALLER_THANH_TIEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByThanhTienIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where thanhTien is less than DEFAULT_THANH_TIEN
        defaultChiDinhXetNghiemShouldNotBeFound("thanhTien.lessThan=" + DEFAULT_THANH_TIEN);

        // Get all the chiDinhXetNghiemList where thanhTien is less than UPDATED_THANH_TIEN
        defaultChiDinhXetNghiemShouldBeFound("thanhTien.lessThan=" + UPDATED_THANH_TIEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByThanhTienIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where thanhTien is greater than DEFAULT_THANH_TIEN
        defaultChiDinhXetNghiemShouldNotBeFound("thanhTien.greaterThan=" + DEFAULT_THANH_TIEN);

        // Get all the chiDinhXetNghiemList where thanhTien is greater than SMALLER_THANH_TIEN
        defaultChiDinhXetNghiemShouldBeFound("thanhTien.greaterThan=" + SMALLER_THANH_TIEN);
    }


    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByThanhTienBhytIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where thanhTienBhyt equals to DEFAULT_THANH_TIEN_BHYT
        defaultChiDinhXetNghiemShouldBeFound("thanhTienBhyt.equals=" + DEFAULT_THANH_TIEN_BHYT);

        // Get all the chiDinhXetNghiemList where thanhTienBhyt equals to UPDATED_THANH_TIEN_BHYT
        defaultChiDinhXetNghiemShouldNotBeFound("thanhTienBhyt.equals=" + UPDATED_THANH_TIEN_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByThanhTienBhytIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where thanhTienBhyt not equals to DEFAULT_THANH_TIEN_BHYT
        defaultChiDinhXetNghiemShouldNotBeFound("thanhTienBhyt.notEquals=" + DEFAULT_THANH_TIEN_BHYT);

        // Get all the chiDinhXetNghiemList where thanhTienBhyt not equals to UPDATED_THANH_TIEN_BHYT
        defaultChiDinhXetNghiemShouldBeFound("thanhTienBhyt.notEquals=" + UPDATED_THANH_TIEN_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByThanhTienBhytIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where thanhTienBhyt in DEFAULT_THANH_TIEN_BHYT or UPDATED_THANH_TIEN_BHYT
        defaultChiDinhXetNghiemShouldBeFound("thanhTienBhyt.in=" + DEFAULT_THANH_TIEN_BHYT + "," + UPDATED_THANH_TIEN_BHYT);

        // Get all the chiDinhXetNghiemList where thanhTienBhyt equals to UPDATED_THANH_TIEN_BHYT
        defaultChiDinhXetNghiemShouldNotBeFound("thanhTienBhyt.in=" + UPDATED_THANH_TIEN_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByThanhTienBhytIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where thanhTienBhyt is not null
        defaultChiDinhXetNghiemShouldBeFound("thanhTienBhyt.specified=true");

        // Get all the chiDinhXetNghiemList where thanhTienBhyt is null
        defaultChiDinhXetNghiemShouldNotBeFound("thanhTienBhyt.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByThanhTienBhytIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where thanhTienBhyt is greater than or equal to DEFAULT_THANH_TIEN_BHYT
        defaultChiDinhXetNghiemShouldBeFound("thanhTienBhyt.greaterThanOrEqual=" + DEFAULT_THANH_TIEN_BHYT);

        // Get all the chiDinhXetNghiemList where thanhTienBhyt is greater than or equal to UPDATED_THANH_TIEN_BHYT
        defaultChiDinhXetNghiemShouldNotBeFound("thanhTienBhyt.greaterThanOrEqual=" + UPDATED_THANH_TIEN_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByThanhTienBhytIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where thanhTienBhyt is less than or equal to DEFAULT_THANH_TIEN_BHYT
        defaultChiDinhXetNghiemShouldBeFound("thanhTienBhyt.lessThanOrEqual=" + DEFAULT_THANH_TIEN_BHYT);

        // Get all the chiDinhXetNghiemList where thanhTienBhyt is less than or equal to SMALLER_THANH_TIEN_BHYT
        defaultChiDinhXetNghiemShouldNotBeFound("thanhTienBhyt.lessThanOrEqual=" + SMALLER_THANH_TIEN_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByThanhTienBhytIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where thanhTienBhyt is less than DEFAULT_THANH_TIEN_BHYT
        defaultChiDinhXetNghiemShouldNotBeFound("thanhTienBhyt.lessThan=" + DEFAULT_THANH_TIEN_BHYT);

        // Get all the chiDinhXetNghiemList where thanhTienBhyt is less than UPDATED_THANH_TIEN_BHYT
        defaultChiDinhXetNghiemShouldBeFound("thanhTienBhyt.lessThan=" + UPDATED_THANH_TIEN_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByThanhTienBhytIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where thanhTienBhyt is greater than DEFAULT_THANH_TIEN_BHYT
        defaultChiDinhXetNghiemShouldNotBeFound("thanhTienBhyt.greaterThan=" + DEFAULT_THANH_TIEN_BHYT);

        // Get all the chiDinhXetNghiemList where thanhTienBhyt is greater than SMALLER_THANH_TIEN_BHYT
        defaultChiDinhXetNghiemShouldBeFound("thanhTienBhyt.greaterThan=" + SMALLER_THANH_TIEN_BHYT);
    }


    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByThanhTienKhongBHYTIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where thanhTienKhongBHYT equals to DEFAULT_THANH_TIEN_KHONG_BHYT
        defaultChiDinhXetNghiemShouldBeFound("thanhTienKhongBHYT.equals=" + DEFAULT_THANH_TIEN_KHONG_BHYT);

        // Get all the chiDinhXetNghiemList where thanhTienKhongBHYT equals to UPDATED_THANH_TIEN_KHONG_BHYT
        defaultChiDinhXetNghiemShouldNotBeFound("thanhTienKhongBHYT.equals=" + UPDATED_THANH_TIEN_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByThanhTienKhongBHYTIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where thanhTienKhongBHYT not equals to DEFAULT_THANH_TIEN_KHONG_BHYT
        defaultChiDinhXetNghiemShouldNotBeFound("thanhTienKhongBHYT.notEquals=" + DEFAULT_THANH_TIEN_KHONG_BHYT);

        // Get all the chiDinhXetNghiemList where thanhTienKhongBHYT not equals to UPDATED_THANH_TIEN_KHONG_BHYT
        defaultChiDinhXetNghiemShouldBeFound("thanhTienKhongBHYT.notEquals=" + UPDATED_THANH_TIEN_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByThanhTienKhongBHYTIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where thanhTienKhongBHYT in DEFAULT_THANH_TIEN_KHONG_BHYT or UPDATED_THANH_TIEN_KHONG_BHYT
        defaultChiDinhXetNghiemShouldBeFound("thanhTienKhongBHYT.in=" + DEFAULT_THANH_TIEN_KHONG_BHYT + "," + UPDATED_THANH_TIEN_KHONG_BHYT);

        // Get all the chiDinhXetNghiemList where thanhTienKhongBHYT equals to UPDATED_THANH_TIEN_KHONG_BHYT
        defaultChiDinhXetNghiemShouldNotBeFound("thanhTienKhongBHYT.in=" + UPDATED_THANH_TIEN_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByThanhTienKhongBHYTIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where thanhTienKhongBHYT is not null
        defaultChiDinhXetNghiemShouldBeFound("thanhTienKhongBHYT.specified=true");

        // Get all the chiDinhXetNghiemList where thanhTienKhongBHYT is null
        defaultChiDinhXetNghiemShouldNotBeFound("thanhTienKhongBHYT.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByThanhTienKhongBHYTIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where thanhTienKhongBHYT is greater than or equal to DEFAULT_THANH_TIEN_KHONG_BHYT
        defaultChiDinhXetNghiemShouldBeFound("thanhTienKhongBHYT.greaterThanOrEqual=" + DEFAULT_THANH_TIEN_KHONG_BHYT);

        // Get all the chiDinhXetNghiemList where thanhTienKhongBHYT is greater than or equal to UPDATED_THANH_TIEN_KHONG_BHYT
        defaultChiDinhXetNghiemShouldNotBeFound("thanhTienKhongBHYT.greaterThanOrEqual=" + UPDATED_THANH_TIEN_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByThanhTienKhongBHYTIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where thanhTienKhongBHYT is less than or equal to DEFAULT_THANH_TIEN_KHONG_BHYT
        defaultChiDinhXetNghiemShouldBeFound("thanhTienKhongBHYT.lessThanOrEqual=" + DEFAULT_THANH_TIEN_KHONG_BHYT);

        // Get all the chiDinhXetNghiemList where thanhTienKhongBHYT is less than or equal to SMALLER_THANH_TIEN_KHONG_BHYT
        defaultChiDinhXetNghiemShouldNotBeFound("thanhTienKhongBHYT.lessThanOrEqual=" + SMALLER_THANH_TIEN_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByThanhTienKhongBHYTIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where thanhTienKhongBHYT is less than DEFAULT_THANH_TIEN_KHONG_BHYT
        defaultChiDinhXetNghiemShouldNotBeFound("thanhTienKhongBHYT.lessThan=" + DEFAULT_THANH_TIEN_KHONG_BHYT);

        // Get all the chiDinhXetNghiemList where thanhTienKhongBHYT is less than UPDATED_THANH_TIEN_KHONG_BHYT
        defaultChiDinhXetNghiemShouldBeFound("thanhTienKhongBHYT.lessThan=" + UPDATED_THANH_TIEN_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByThanhTienKhongBHYTIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where thanhTienKhongBHYT is greater than DEFAULT_THANH_TIEN_KHONG_BHYT
        defaultChiDinhXetNghiemShouldNotBeFound("thanhTienKhongBHYT.greaterThan=" + DEFAULT_THANH_TIEN_KHONG_BHYT);

        // Get all the chiDinhXetNghiemList where thanhTienKhongBHYT is greater than SMALLER_THANH_TIEN_KHONG_BHYT
        defaultChiDinhXetNghiemShouldBeFound("thanhTienKhongBHYT.greaterThan=" + SMALLER_THANH_TIEN_KHONG_BHYT);
    }


    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByThoiGianChiDinhIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where thoiGianChiDinh equals to DEFAULT_THOI_GIAN_CHI_DINH
        defaultChiDinhXetNghiemShouldBeFound("thoiGianChiDinh.equals=" + DEFAULT_THOI_GIAN_CHI_DINH);

        // Get all the chiDinhXetNghiemList where thoiGianChiDinh equals to UPDATED_THOI_GIAN_CHI_DINH
        defaultChiDinhXetNghiemShouldNotBeFound("thoiGianChiDinh.equals=" + UPDATED_THOI_GIAN_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByThoiGianChiDinhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where thoiGianChiDinh not equals to DEFAULT_THOI_GIAN_CHI_DINH
        defaultChiDinhXetNghiemShouldNotBeFound("thoiGianChiDinh.notEquals=" + DEFAULT_THOI_GIAN_CHI_DINH);

        // Get all the chiDinhXetNghiemList where thoiGianChiDinh not equals to UPDATED_THOI_GIAN_CHI_DINH
        defaultChiDinhXetNghiemShouldBeFound("thoiGianChiDinh.notEquals=" + UPDATED_THOI_GIAN_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByThoiGianChiDinhIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where thoiGianChiDinh in DEFAULT_THOI_GIAN_CHI_DINH or UPDATED_THOI_GIAN_CHI_DINH
        defaultChiDinhXetNghiemShouldBeFound("thoiGianChiDinh.in=" + DEFAULT_THOI_GIAN_CHI_DINH + "," + UPDATED_THOI_GIAN_CHI_DINH);

        // Get all the chiDinhXetNghiemList where thoiGianChiDinh equals to UPDATED_THOI_GIAN_CHI_DINH
        defaultChiDinhXetNghiemShouldNotBeFound("thoiGianChiDinh.in=" + UPDATED_THOI_GIAN_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByThoiGianChiDinhIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where thoiGianChiDinh is not null
        defaultChiDinhXetNghiemShouldBeFound("thoiGianChiDinh.specified=true");

        // Get all the chiDinhXetNghiemList where thoiGianChiDinh is null
        defaultChiDinhXetNghiemShouldNotBeFound("thoiGianChiDinh.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByThoiGianChiDinhIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where thoiGianChiDinh is greater than or equal to DEFAULT_THOI_GIAN_CHI_DINH
        defaultChiDinhXetNghiemShouldBeFound("thoiGianChiDinh.greaterThanOrEqual=" + DEFAULT_THOI_GIAN_CHI_DINH);

        // Get all the chiDinhXetNghiemList where thoiGianChiDinh is greater than or equal to UPDATED_THOI_GIAN_CHI_DINH
        defaultChiDinhXetNghiemShouldNotBeFound("thoiGianChiDinh.greaterThanOrEqual=" + UPDATED_THOI_GIAN_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByThoiGianChiDinhIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where thoiGianChiDinh is less than or equal to DEFAULT_THOI_GIAN_CHI_DINH
        defaultChiDinhXetNghiemShouldBeFound("thoiGianChiDinh.lessThanOrEqual=" + DEFAULT_THOI_GIAN_CHI_DINH);

        // Get all the chiDinhXetNghiemList where thoiGianChiDinh is less than or equal to SMALLER_THOI_GIAN_CHI_DINH
        defaultChiDinhXetNghiemShouldNotBeFound("thoiGianChiDinh.lessThanOrEqual=" + SMALLER_THOI_GIAN_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByThoiGianChiDinhIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where thoiGianChiDinh is less than DEFAULT_THOI_GIAN_CHI_DINH
        defaultChiDinhXetNghiemShouldNotBeFound("thoiGianChiDinh.lessThan=" + DEFAULT_THOI_GIAN_CHI_DINH);

        // Get all the chiDinhXetNghiemList where thoiGianChiDinh is less than UPDATED_THOI_GIAN_CHI_DINH
        defaultChiDinhXetNghiemShouldBeFound("thoiGianChiDinh.lessThan=" + UPDATED_THOI_GIAN_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByThoiGianChiDinhIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where thoiGianChiDinh is greater than DEFAULT_THOI_GIAN_CHI_DINH
        defaultChiDinhXetNghiemShouldNotBeFound("thoiGianChiDinh.greaterThan=" + DEFAULT_THOI_GIAN_CHI_DINH);

        // Get all the chiDinhXetNghiemList where thoiGianChiDinh is greater than SMALLER_THOI_GIAN_CHI_DINH
        defaultChiDinhXetNghiemShouldBeFound("thoiGianChiDinh.greaterThan=" + SMALLER_THOI_GIAN_CHI_DINH);
    }


    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByThoiGianTaoIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where thoiGianTao equals to DEFAULT_THOI_GIAN_TAO
        defaultChiDinhXetNghiemShouldBeFound("thoiGianTao.equals=" + DEFAULT_THOI_GIAN_TAO);

        // Get all the chiDinhXetNghiemList where thoiGianTao equals to UPDATED_THOI_GIAN_TAO
        defaultChiDinhXetNghiemShouldNotBeFound("thoiGianTao.equals=" + UPDATED_THOI_GIAN_TAO);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByThoiGianTaoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where thoiGianTao not equals to DEFAULT_THOI_GIAN_TAO
        defaultChiDinhXetNghiemShouldNotBeFound("thoiGianTao.notEquals=" + DEFAULT_THOI_GIAN_TAO);

        // Get all the chiDinhXetNghiemList where thoiGianTao not equals to UPDATED_THOI_GIAN_TAO
        defaultChiDinhXetNghiemShouldBeFound("thoiGianTao.notEquals=" + UPDATED_THOI_GIAN_TAO);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByThoiGianTaoIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where thoiGianTao in DEFAULT_THOI_GIAN_TAO or UPDATED_THOI_GIAN_TAO
        defaultChiDinhXetNghiemShouldBeFound("thoiGianTao.in=" + DEFAULT_THOI_GIAN_TAO + "," + UPDATED_THOI_GIAN_TAO);

        // Get all the chiDinhXetNghiemList where thoiGianTao equals to UPDATED_THOI_GIAN_TAO
        defaultChiDinhXetNghiemShouldNotBeFound("thoiGianTao.in=" + UPDATED_THOI_GIAN_TAO);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByThoiGianTaoIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where thoiGianTao is not null
        defaultChiDinhXetNghiemShouldBeFound("thoiGianTao.specified=true");

        // Get all the chiDinhXetNghiemList where thoiGianTao is null
        defaultChiDinhXetNghiemShouldNotBeFound("thoiGianTao.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByThoiGianTaoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where thoiGianTao is greater than or equal to DEFAULT_THOI_GIAN_TAO
        defaultChiDinhXetNghiemShouldBeFound("thoiGianTao.greaterThanOrEqual=" + DEFAULT_THOI_GIAN_TAO);

        // Get all the chiDinhXetNghiemList where thoiGianTao is greater than or equal to UPDATED_THOI_GIAN_TAO
        defaultChiDinhXetNghiemShouldNotBeFound("thoiGianTao.greaterThanOrEqual=" + UPDATED_THOI_GIAN_TAO);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByThoiGianTaoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where thoiGianTao is less than or equal to DEFAULT_THOI_GIAN_TAO
        defaultChiDinhXetNghiemShouldBeFound("thoiGianTao.lessThanOrEqual=" + DEFAULT_THOI_GIAN_TAO);

        // Get all the chiDinhXetNghiemList where thoiGianTao is less than or equal to SMALLER_THOI_GIAN_TAO
        defaultChiDinhXetNghiemShouldNotBeFound("thoiGianTao.lessThanOrEqual=" + SMALLER_THOI_GIAN_TAO);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByThoiGianTaoIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where thoiGianTao is less than DEFAULT_THOI_GIAN_TAO
        defaultChiDinhXetNghiemShouldNotBeFound("thoiGianTao.lessThan=" + DEFAULT_THOI_GIAN_TAO);

        // Get all the chiDinhXetNghiemList where thoiGianTao is less than UPDATED_THOI_GIAN_TAO
        defaultChiDinhXetNghiemShouldBeFound("thoiGianTao.lessThan=" + UPDATED_THOI_GIAN_TAO);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByThoiGianTaoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where thoiGianTao is greater than DEFAULT_THOI_GIAN_TAO
        defaultChiDinhXetNghiemShouldNotBeFound("thoiGianTao.greaterThan=" + DEFAULT_THOI_GIAN_TAO);

        // Get all the chiDinhXetNghiemList where thoiGianTao is greater than SMALLER_THOI_GIAN_TAO
        defaultChiDinhXetNghiemShouldBeFound("thoiGianTao.greaterThan=" + SMALLER_THOI_GIAN_TAO);
    }


    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByTienNgoaiBHYTIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where tienNgoaiBHYT equals to DEFAULT_TIEN_NGOAI_BHYT
        defaultChiDinhXetNghiemShouldBeFound("tienNgoaiBHYT.equals=" + DEFAULT_TIEN_NGOAI_BHYT);

        // Get all the chiDinhXetNghiemList where tienNgoaiBHYT equals to UPDATED_TIEN_NGOAI_BHYT
        defaultChiDinhXetNghiemShouldNotBeFound("tienNgoaiBHYT.equals=" + UPDATED_TIEN_NGOAI_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByTienNgoaiBHYTIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where tienNgoaiBHYT not equals to DEFAULT_TIEN_NGOAI_BHYT
        defaultChiDinhXetNghiemShouldNotBeFound("tienNgoaiBHYT.notEquals=" + DEFAULT_TIEN_NGOAI_BHYT);

        // Get all the chiDinhXetNghiemList where tienNgoaiBHYT not equals to UPDATED_TIEN_NGOAI_BHYT
        defaultChiDinhXetNghiemShouldBeFound("tienNgoaiBHYT.notEquals=" + UPDATED_TIEN_NGOAI_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByTienNgoaiBHYTIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where tienNgoaiBHYT in DEFAULT_TIEN_NGOAI_BHYT or UPDATED_TIEN_NGOAI_BHYT
        defaultChiDinhXetNghiemShouldBeFound("tienNgoaiBHYT.in=" + DEFAULT_TIEN_NGOAI_BHYT + "," + UPDATED_TIEN_NGOAI_BHYT);

        // Get all the chiDinhXetNghiemList where tienNgoaiBHYT equals to UPDATED_TIEN_NGOAI_BHYT
        defaultChiDinhXetNghiemShouldNotBeFound("tienNgoaiBHYT.in=" + UPDATED_TIEN_NGOAI_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByTienNgoaiBHYTIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where tienNgoaiBHYT is not null
        defaultChiDinhXetNghiemShouldBeFound("tienNgoaiBHYT.specified=true");

        // Get all the chiDinhXetNghiemList where tienNgoaiBHYT is null
        defaultChiDinhXetNghiemShouldNotBeFound("tienNgoaiBHYT.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByTienNgoaiBHYTIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where tienNgoaiBHYT is greater than or equal to DEFAULT_TIEN_NGOAI_BHYT
        defaultChiDinhXetNghiemShouldBeFound("tienNgoaiBHYT.greaterThanOrEqual=" + DEFAULT_TIEN_NGOAI_BHYT);

        // Get all the chiDinhXetNghiemList where tienNgoaiBHYT is greater than or equal to UPDATED_TIEN_NGOAI_BHYT
        defaultChiDinhXetNghiemShouldNotBeFound("tienNgoaiBHYT.greaterThanOrEqual=" + UPDATED_TIEN_NGOAI_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByTienNgoaiBHYTIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where tienNgoaiBHYT is less than or equal to DEFAULT_TIEN_NGOAI_BHYT
        defaultChiDinhXetNghiemShouldBeFound("tienNgoaiBHYT.lessThanOrEqual=" + DEFAULT_TIEN_NGOAI_BHYT);

        // Get all the chiDinhXetNghiemList where tienNgoaiBHYT is less than or equal to SMALLER_TIEN_NGOAI_BHYT
        defaultChiDinhXetNghiemShouldNotBeFound("tienNgoaiBHYT.lessThanOrEqual=" + SMALLER_TIEN_NGOAI_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByTienNgoaiBHYTIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where tienNgoaiBHYT is less than DEFAULT_TIEN_NGOAI_BHYT
        defaultChiDinhXetNghiemShouldNotBeFound("tienNgoaiBHYT.lessThan=" + DEFAULT_TIEN_NGOAI_BHYT);

        // Get all the chiDinhXetNghiemList where tienNgoaiBHYT is less than UPDATED_TIEN_NGOAI_BHYT
        defaultChiDinhXetNghiemShouldBeFound("tienNgoaiBHYT.lessThan=" + UPDATED_TIEN_NGOAI_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByTienNgoaiBHYTIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where tienNgoaiBHYT is greater than DEFAULT_TIEN_NGOAI_BHYT
        defaultChiDinhXetNghiemShouldNotBeFound("tienNgoaiBHYT.greaterThan=" + DEFAULT_TIEN_NGOAI_BHYT);

        // Get all the chiDinhXetNghiemList where tienNgoaiBHYT is greater than SMALLER_TIEN_NGOAI_BHYT
        defaultChiDinhXetNghiemShouldBeFound("tienNgoaiBHYT.greaterThan=" + SMALLER_TIEN_NGOAI_BHYT);
    }


    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByThanhToanChenhLechIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where thanhToanChenhLech equals to DEFAULT_THANH_TOAN_CHENH_LECH
        defaultChiDinhXetNghiemShouldBeFound("thanhToanChenhLech.equals=" + DEFAULT_THANH_TOAN_CHENH_LECH);

        // Get all the chiDinhXetNghiemList where thanhToanChenhLech equals to UPDATED_THANH_TOAN_CHENH_LECH
        defaultChiDinhXetNghiemShouldNotBeFound("thanhToanChenhLech.equals=" + UPDATED_THANH_TOAN_CHENH_LECH);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByThanhToanChenhLechIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where thanhToanChenhLech not equals to DEFAULT_THANH_TOAN_CHENH_LECH
        defaultChiDinhXetNghiemShouldNotBeFound("thanhToanChenhLech.notEquals=" + DEFAULT_THANH_TOAN_CHENH_LECH);

        // Get all the chiDinhXetNghiemList where thanhToanChenhLech not equals to UPDATED_THANH_TOAN_CHENH_LECH
        defaultChiDinhXetNghiemShouldBeFound("thanhToanChenhLech.notEquals=" + UPDATED_THANH_TOAN_CHENH_LECH);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByThanhToanChenhLechIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where thanhToanChenhLech in DEFAULT_THANH_TOAN_CHENH_LECH or UPDATED_THANH_TOAN_CHENH_LECH
        defaultChiDinhXetNghiemShouldBeFound("thanhToanChenhLech.in=" + DEFAULT_THANH_TOAN_CHENH_LECH + "," + UPDATED_THANH_TOAN_CHENH_LECH);

        // Get all the chiDinhXetNghiemList where thanhToanChenhLech equals to UPDATED_THANH_TOAN_CHENH_LECH
        defaultChiDinhXetNghiemShouldNotBeFound("thanhToanChenhLech.in=" + UPDATED_THANH_TOAN_CHENH_LECH);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByThanhToanChenhLechIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where thanhToanChenhLech is not null
        defaultChiDinhXetNghiemShouldBeFound("thanhToanChenhLech.specified=true");

        // Get all the chiDinhXetNghiemList where thanhToanChenhLech is null
        defaultChiDinhXetNghiemShouldNotBeFound("thanhToanChenhLech.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByTyLeThanhToanIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where tyLeThanhToan equals to DEFAULT_TY_LE_THANH_TOAN
        defaultChiDinhXetNghiemShouldBeFound("tyLeThanhToan.equals=" + DEFAULT_TY_LE_THANH_TOAN);

        // Get all the chiDinhXetNghiemList where tyLeThanhToan equals to UPDATED_TY_LE_THANH_TOAN
        defaultChiDinhXetNghiemShouldNotBeFound("tyLeThanhToan.equals=" + UPDATED_TY_LE_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByTyLeThanhToanIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where tyLeThanhToan not equals to DEFAULT_TY_LE_THANH_TOAN
        defaultChiDinhXetNghiemShouldNotBeFound("tyLeThanhToan.notEquals=" + DEFAULT_TY_LE_THANH_TOAN);

        // Get all the chiDinhXetNghiemList where tyLeThanhToan not equals to UPDATED_TY_LE_THANH_TOAN
        defaultChiDinhXetNghiemShouldBeFound("tyLeThanhToan.notEquals=" + UPDATED_TY_LE_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByTyLeThanhToanIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where tyLeThanhToan in DEFAULT_TY_LE_THANH_TOAN or UPDATED_TY_LE_THANH_TOAN
        defaultChiDinhXetNghiemShouldBeFound("tyLeThanhToan.in=" + DEFAULT_TY_LE_THANH_TOAN + "," + UPDATED_TY_LE_THANH_TOAN);

        // Get all the chiDinhXetNghiemList where tyLeThanhToan equals to UPDATED_TY_LE_THANH_TOAN
        defaultChiDinhXetNghiemShouldNotBeFound("tyLeThanhToan.in=" + UPDATED_TY_LE_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByTyLeThanhToanIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where tyLeThanhToan is not null
        defaultChiDinhXetNghiemShouldBeFound("tyLeThanhToan.specified=true");

        // Get all the chiDinhXetNghiemList where tyLeThanhToan is null
        defaultChiDinhXetNghiemShouldNotBeFound("tyLeThanhToan.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByTyLeThanhToanIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where tyLeThanhToan is greater than or equal to DEFAULT_TY_LE_THANH_TOAN
        defaultChiDinhXetNghiemShouldBeFound("tyLeThanhToan.greaterThanOrEqual=" + DEFAULT_TY_LE_THANH_TOAN);

        // Get all the chiDinhXetNghiemList where tyLeThanhToan is greater than or equal to UPDATED_TY_LE_THANH_TOAN
        defaultChiDinhXetNghiemShouldNotBeFound("tyLeThanhToan.greaterThanOrEqual=" + UPDATED_TY_LE_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByTyLeThanhToanIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where tyLeThanhToan is less than or equal to DEFAULT_TY_LE_THANH_TOAN
        defaultChiDinhXetNghiemShouldBeFound("tyLeThanhToan.lessThanOrEqual=" + DEFAULT_TY_LE_THANH_TOAN);

        // Get all the chiDinhXetNghiemList where tyLeThanhToan is less than or equal to SMALLER_TY_LE_THANH_TOAN
        defaultChiDinhXetNghiemShouldNotBeFound("tyLeThanhToan.lessThanOrEqual=" + SMALLER_TY_LE_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByTyLeThanhToanIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where tyLeThanhToan is less than DEFAULT_TY_LE_THANH_TOAN
        defaultChiDinhXetNghiemShouldNotBeFound("tyLeThanhToan.lessThan=" + DEFAULT_TY_LE_THANH_TOAN);

        // Get all the chiDinhXetNghiemList where tyLeThanhToan is less than UPDATED_TY_LE_THANH_TOAN
        defaultChiDinhXetNghiemShouldBeFound("tyLeThanhToan.lessThan=" + UPDATED_TY_LE_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByTyLeThanhToanIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where tyLeThanhToan is greater than DEFAULT_TY_LE_THANH_TOAN
        defaultChiDinhXetNghiemShouldNotBeFound("tyLeThanhToan.greaterThan=" + DEFAULT_TY_LE_THANH_TOAN);

        // Get all the chiDinhXetNghiemList where tyLeThanhToan is greater than SMALLER_TY_LE_THANH_TOAN
        defaultChiDinhXetNghiemShouldBeFound("tyLeThanhToan.greaterThan=" + SMALLER_TY_LE_THANH_TOAN);
    }


    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByMaDungChungIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where maDungChung equals to DEFAULT_MA_DUNG_CHUNG
        defaultChiDinhXetNghiemShouldBeFound("maDungChung.equals=" + DEFAULT_MA_DUNG_CHUNG);

        // Get all the chiDinhXetNghiemList where maDungChung equals to UPDATED_MA_DUNG_CHUNG
        defaultChiDinhXetNghiemShouldNotBeFound("maDungChung.equals=" + UPDATED_MA_DUNG_CHUNG);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByMaDungChungIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where maDungChung not equals to DEFAULT_MA_DUNG_CHUNG
        defaultChiDinhXetNghiemShouldNotBeFound("maDungChung.notEquals=" + DEFAULT_MA_DUNG_CHUNG);

        // Get all the chiDinhXetNghiemList where maDungChung not equals to UPDATED_MA_DUNG_CHUNG
        defaultChiDinhXetNghiemShouldBeFound("maDungChung.notEquals=" + UPDATED_MA_DUNG_CHUNG);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByMaDungChungIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where maDungChung in DEFAULT_MA_DUNG_CHUNG or UPDATED_MA_DUNG_CHUNG
        defaultChiDinhXetNghiemShouldBeFound("maDungChung.in=" + DEFAULT_MA_DUNG_CHUNG + "," + UPDATED_MA_DUNG_CHUNG);

        // Get all the chiDinhXetNghiemList where maDungChung equals to UPDATED_MA_DUNG_CHUNG
        defaultChiDinhXetNghiemShouldNotBeFound("maDungChung.in=" + UPDATED_MA_DUNG_CHUNG);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByMaDungChungIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where maDungChung is not null
        defaultChiDinhXetNghiemShouldBeFound("maDungChung.specified=true");

        // Get all the chiDinhXetNghiemList where maDungChung is null
        defaultChiDinhXetNghiemShouldNotBeFound("maDungChung.specified=false");
    }
                @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByMaDungChungContainsSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where maDungChung contains DEFAULT_MA_DUNG_CHUNG
        defaultChiDinhXetNghiemShouldBeFound("maDungChung.contains=" + DEFAULT_MA_DUNG_CHUNG);

        // Get all the chiDinhXetNghiemList where maDungChung contains UPDATED_MA_DUNG_CHUNG
        defaultChiDinhXetNghiemShouldNotBeFound("maDungChung.contains=" + UPDATED_MA_DUNG_CHUNG);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByMaDungChungNotContainsSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where maDungChung does not contain DEFAULT_MA_DUNG_CHUNG
        defaultChiDinhXetNghiemShouldNotBeFound("maDungChung.doesNotContain=" + DEFAULT_MA_DUNG_CHUNG);

        // Get all the chiDinhXetNghiemList where maDungChung does not contain UPDATED_MA_DUNG_CHUNG
        defaultChiDinhXetNghiemShouldBeFound("maDungChung.doesNotContain=" + UPDATED_MA_DUNG_CHUNG);
    }


    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDichVuYeuCauIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where dichVuYeuCau equals to DEFAULT_DICH_VU_YEU_CAU
        defaultChiDinhXetNghiemShouldBeFound("dichVuYeuCau.equals=" + DEFAULT_DICH_VU_YEU_CAU);

        // Get all the chiDinhXetNghiemList where dichVuYeuCau equals to UPDATED_DICH_VU_YEU_CAU
        defaultChiDinhXetNghiemShouldNotBeFound("dichVuYeuCau.equals=" + UPDATED_DICH_VU_YEU_CAU);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDichVuYeuCauIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where dichVuYeuCau not equals to DEFAULT_DICH_VU_YEU_CAU
        defaultChiDinhXetNghiemShouldNotBeFound("dichVuYeuCau.notEquals=" + DEFAULT_DICH_VU_YEU_CAU);

        // Get all the chiDinhXetNghiemList where dichVuYeuCau not equals to UPDATED_DICH_VU_YEU_CAU
        defaultChiDinhXetNghiemShouldBeFound("dichVuYeuCau.notEquals=" + UPDATED_DICH_VU_YEU_CAU);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDichVuYeuCauIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where dichVuYeuCau in DEFAULT_DICH_VU_YEU_CAU or UPDATED_DICH_VU_YEU_CAU
        defaultChiDinhXetNghiemShouldBeFound("dichVuYeuCau.in=" + DEFAULT_DICH_VU_YEU_CAU + "," + UPDATED_DICH_VU_YEU_CAU);

        // Get all the chiDinhXetNghiemList where dichVuYeuCau equals to UPDATED_DICH_VU_YEU_CAU
        defaultChiDinhXetNghiemShouldNotBeFound("dichVuYeuCau.in=" + UPDATED_DICH_VU_YEU_CAU);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDichVuYeuCauIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where dichVuYeuCau is not null
        defaultChiDinhXetNghiemShouldBeFound("dichVuYeuCau.specified=true");

        // Get all the chiDinhXetNghiemList where dichVuYeuCau is null
        defaultChiDinhXetNghiemShouldNotBeFound("dichVuYeuCau.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByNamIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where nam equals to DEFAULT_NAM
        defaultChiDinhXetNghiemShouldBeFound("nam.equals=" + DEFAULT_NAM);

        // Get all the chiDinhXetNghiemList where nam equals to UPDATED_NAM
        defaultChiDinhXetNghiemShouldNotBeFound("nam.equals=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByNamIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where nam not equals to DEFAULT_NAM
        defaultChiDinhXetNghiemShouldNotBeFound("nam.notEquals=" + DEFAULT_NAM);

        // Get all the chiDinhXetNghiemList where nam not equals to UPDATED_NAM
        defaultChiDinhXetNghiemShouldBeFound("nam.notEquals=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByNamIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where nam in DEFAULT_NAM or UPDATED_NAM
        defaultChiDinhXetNghiemShouldBeFound("nam.in=" + DEFAULT_NAM + "," + UPDATED_NAM);

        // Get all the chiDinhXetNghiemList where nam equals to UPDATED_NAM
        defaultChiDinhXetNghiemShouldNotBeFound("nam.in=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByNamIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where nam is not null
        defaultChiDinhXetNghiemShouldBeFound("nam.specified=true");

        // Get all the chiDinhXetNghiemList where nam is null
        defaultChiDinhXetNghiemShouldNotBeFound("nam.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByNamIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where nam is greater than or equal to DEFAULT_NAM
        defaultChiDinhXetNghiemShouldBeFound("nam.greaterThanOrEqual=" + DEFAULT_NAM);

        // Get all the chiDinhXetNghiemList where nam is greater than or equal to UPDATED_NAM
        defaultChiDinhXetNghiemShouldNotBeFound("nam.greaterThanOrEqual=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByNamIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where nam is less than or equal to DEFAULT_NAM
        defaultChiDinhXetNghiemShouldBeFound("nam.lessThanOrEqual=" + DEFAULT_NAM);

        // Get all the chiDinhXetNghiemList where nam is less than or equal to SMALLER_NAM
        defaultChiDinhXetNghiemShouldNotBeFound("nam.lessThanOrEqual=" + SMALLER_NAM);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByNamIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where nam is less than DEFAULT_NAM
        defaultChiDinhXetNghiemShouldNotBeFound("nam.lessThan=" + DEFAULT_NAM);

        // Get all the chiDinhXetNghiemList where nam is less than UPDATED_NAM
        defaultChiDinhXetNghiemShouldBeFound("nam.lessThan=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByNamIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        // Get all the chiDinhXetNghiemList where nam is greater than DEFAULT_NAM
        defaultChiDinhXetNghiemShouldNotBeFound("nam.greaterThan=" + DEFAULT_NAM);

        // Get all the chiDinhXetNghiemList where nam is greater than SMALLER_NAM
        defaultChiDinhXetNghiemShouldBeFound("nam.greaterThan=" + SMALLER_NAM);
    }


    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByDonViIsEqualToSomething() throws Exception {
        // Get already existing entity
        PhieuChiDinhXetNghiem donVi = chiDinhXetNghiem.getDonVi();
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);
        Long donViId = donVi.getId();

        // Get all the chiDinhXetNghiemList where donVi equals to donViId
        defaultChiDinhXetNghiemShouldBeFound("donViId.equals=" + donViId);

        // Get all the chiDinhXetNghiemList where donVi equals to donViId + 1
        defaultChiDinhXetNghiemShouldNotBeFound("donViId.equals=" + (donViId + 1));
    }


    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByBenhNhanIsEqualToSomething() throws Exception {
        // Get already existing entity
        PhieuChiDinhXetNghiem benhNhan = chiDinhXetNghiem.getBenhNhan();
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);
        Long benhNhanId = benhNhan.getId();

        // Get all the chiDinhXetNghiemList where benhNhan equals to benhNhanId
        defaultChiDinhXetNghiemShouldBeFound("benhNhanId.equals=" + benhNhanId);

        // Get all the chiDinhXetNghiemList where benhNhan equals to benhNhanId + 1
        defaultChiDinhXetNghiemShouldNotBeFound("benhNhanId.equals=" + (benhNhanId + 1));
    }


    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByBakbIsEqualToSomething() throws Exception {
        // Get already existing entity
        PhieuChiDinhXetNghiem bakb = chiDinhXetNghiem.getBakb();
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);
        Long bakbId = bakb.getId();

        // Get all the chiDinhXetNghiemList where bakb equals to bakbId
        defaultChiDinhXetNghiemShouldBeFound("bakbId.equals=" + bakbId);

        // Get all the chiDinhXetNghiemList where bakb equals to bakbId + 1
        defaultChiDinhXetNghiemShouldNotBeFound("bakbId.equals=" + (bakbId + 1));
    }


    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByPhieuCDIsEqualToSomething() throws Exception {
        // Get already existing entity
        PhieuChiDinhXetNghiem phieuCD = chiDinhXetNghiem.getPhieuCD();
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);
        Long phieuCDId = phieuCD.getId();

        // Get all the chiDinhXetNghiemList where phieuCD equals to phieuCDId
        defaultChiDinhXetNghiemShouldBeFound("phieuCDId.equals=" + phieuCDId);

        // Get all the chiDinhXetNghiemList where phieuCD equals to phieuCDId + 1
        defaultChiDinhXetNghiemShouldNotBeFound("phieuCDId.equals=" + (phieuCDId + 1));
    }


    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByPhongIsEqualToSomething() throws Exception {
        // Get already existing entity
        Phong phong = chiDinhXetNghiem.getPhong();
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);
        Long phongId = phong.getId();

        // Get all the chiDinhXetNghiemList where phong equals to phongId
        defaultChiDinhXetNghiemShouldBeFound("phongId.equals=" + phongId);

        // Get all the chiDinhXetNghiemList where phong equals to phongId + 1
        defaultChiDinhXetNghiemShouldNotBeFound("phongId.equals=" + (phongId + 1));
    }


    @Test
    @Transactional
    public void getAllChiDinhXetNghiemsByXetNghiemIsEqualToSomething() throws Exception {
        // Get already existing entity
        XetNghiem xetNghiem = chiDinhXetNghiem.getXetNghiem();
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);
        Long xetNghiemId = xetNghiem.getId();

        // Get all the chiDinhXetNghiemList where xetNghiem equals to xetNghiemId
        defaultChiDinhXetNghiemShouldBeFound("xetNghiemId.equals=" + xetNghiemId);

        // Get all the chiDinhXetNghiemList where xetNghiem equals to xetNghiemId + 1
        defaultChiDinhXetNghiemShouldNotBeFound("xetNghiemId.equals=" + (xetNghiemId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultChiDinhXetNghiemShouldBeFound(String filter) throws Exception {
        restChiDinhXetNghiemMockMvc.perform(get("/api/chi-dinh-xet-nghiems?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(chiDinhXetNghiem.getId().intValue())))
            .andExpect(jsonPath("$.[*].coBaoHiem").value(hasItem(DEFAULT_CO_BAO_HIEM.booleanValue())))
            .andExpect(jsonPath("$.[*].coKetQua").value(hasItem(DEFAULT_CO_KET_QUA.booleanValue())))
            .andExpect(jsonPath("$.[*].daThanhToan").value(hasItem(DEFAULT_DA_THANH_TOAN)))
            .andExpect(jsonPath("$.[*].daThanhToanChenhLech").value(hasItem(DEFAULT_DA_THANH_TOAN_CHENH_LECH)))
            .andExpect(jsonPath("$.[*].daThucHien").value(hasItem(DEFAULT_DA_THUC_HIEN)))
            .andExpect(jsonPath("$.[*].donGia").value(hasItem(DEFAULT_DON_GIA.intValue())))
            .andExpect(jsonPath("$.[*].donGiaBhyt").value(hasItem(DEFAULT_DON_GIA_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].donGiaKhongBhyt").value(hasItem(DEFAULT_DON_GIA_KHONG_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].ghiChuChiDinh").value(hasItem(DEFAULT_GHI_CHU_CHI_DINH)))
            .andExpect(jsonPath("$.[*].moTa").value(hasItem(DEFAULT_MO_TA)))
            .andExpect(jsonPath("$.[*].nguoiChiDinhId").value(hasItem(DEFAULT_NGUOI_CHI_DINH_ID.intValue())))
            .andExpect(jsonPath("$.[*].soLuong").value(hasItem(DEFAULT_SO_LUONG)))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].thanhTien").value(hasItem(DEFAULT_THANH_TIEN.intValue())))
            .andExpect(jsonPath("$.[*].thanhTienBhyt").value(hasItem(DEFAULT_THANH_TIEN_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].thanhTienKhongBHYT").value(hasItem(DEFAULT_THANH_TIEN_KHONG_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].thoiGianChiDinh").value(hasItem(DEFAULT_THOI_GIAN_CHI_DINH.toString())))
            .andExpect(jsonPath("$.[*].thoiGianTao").value(hasItem(DEFAULT_THOI_GIAN_TAO.toString())))
            .andExpect(jsonPath("$.[*].tienNgoaiBHYT").value(hasItem(DEFAULT_TIEN_NGOAI_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].thanhToanChenhLech").value(hasItem(DEFAULT_THANH_TOAN_CHENH_LECH.booleanValue())))
            .andExpect(jsonPath("$.[*].tyLeThanhToan").value(hasItem(DEFAULT_TY_LE_THANH_TOAN)))
            .andExpect(jsonPath("$.[*].maDungChung").value(hasItem(DEFAULT_MA_DUNG_CHUNG)))
            .andExpect(jsonPath("$.[*].dichVuYeuCau").value(hasItem(DEFAULT_DICH_VU_YEU_CAU.booleanValue())))
            .andExpect(jsonPath("$.[*].nam").value(hasItem(DEFAULT_NAM)));

        // Check, that the count call also returns 1
        restChiDinhXetNghiemMockMvc.perform(get("/api/chi-dinh-xet-nghiems/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultChiDinhXetNghiemShouldNotBeFound(String filter) throws Exception {
        restChiDinhXetNghiemMockMvc.perform(get("/api/chi-dinh-xet-nghiems?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restChiDinhXetNghiemMockMvc.perform(get("/api/chi-dinh-xet-nghiems/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingChiDinhXetNghiem() throws Exception {
        // Get the chiDinhXetNghiem
        restChiDinhXetNghiemMockMvc.perform(get("/api/chi-dinh-xet-nghiems/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateChiDinhXetNghiem() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        int databaseSizeBeforeUpdate = chiDinhXetNghiemRepository.findAll().size();

        // Update the chiDinhXetNghiem
        ChiDinhXetNghiem updatedChiDinhXetNghiem = chiDinhXetNghiemRepository.findById(chiDinhXetNghiem.getId()).get();
        // Disconnect from session so that the updates on updatedChiDinhXetNghiem are not directly saved in db
        em.detach(updatedChiDinhXetNghiem);
        updatedChiDinhXetNghiem
            .coBaoHiem(UPDATED_CO_BAO_HIEM)
            .coKetQua(UPDATED_CO_KET_QUA)
            .daThanhToan(UPDATED_DA_THANH_TOAN)
            .daThanhToanChenhLech(UPDATED_DA_THANH_TOAN_CHENH_LECH)
            .daThucHien(UPDATED_DA_THUC_HIEN)
            .donGia(UPDATED_DON_GIA)
            .donGiaBhyt(UPDATED_DON_GIA_BHYT)
            .donGiaKhongBhyt(UPDATED_DON_GIA_KHONG_BHYT)
            .ghiChuChiDinh(UPDATED_GHI_CHU_CHI_DINH)
            .moTa(UPDATED_MO_TA)
            .nguoiChiDinhId(UPDATED_NGUOI_CHI_DINH_ID)
            .soLuong(UPDATED_SO_LUONG)
            .ten(UPDATED_TEN)
            .thanhTien(UPDATED_THANH_TIEN)
            .thanhTienBhyt(UPDATED_THANH_TIEN_BHYT)
            .thanhTienKhongBHYT(UPDATED_THANH_TIEN_KHONG_BHYT)
            .thoiGianChiDinh(UPDATED_THOI_GIAN_CHI_DINH)
            .thoiGianTao(UPDATED_THOI_GIAN_TAO)
            .tienNgoaiBHYT(UPDATED_TIEN_NGOAI_BHYT)
            .thanhToanChenhLech(UPDATED_THANH_TOAN_CHENH_LECH)
            .tyLeThanhToan(UPDATED_TY_LE_THANH_TOAN)
            .maDungChung(UPDATED_MA_DUNG_CHUNG)
            .dichVuYeuCau(UPDATED_DICH_VU_YEU_CAU)
            .nam(UPDATED_NAM);
        ChiDinhXetNghiemDTO chiDinhXetNghiemDTO = chiDinhXetNghiemMapper.toDto(updatedChiDinhXetNghiem);

        restChiDinhXetNghiemMockMvc.perform(put("/api/chi-dinh-xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhXetNghiemDTO)))
            .andExpect(status().isOk());

        // Validate the ChiDinhXetNghiem in the database
        List<ChiDinhXetNghiem> chiDinhXetNghiemList = chiDinhXetNghiemRepository.findAll();
        assertThat(chiDinhXetNghiemList).hasSize(databaseSizeBeforeUpdate);
        ChiDinhXetNghiem testChiDinhXetNghiem = chiDinhXetNghiemList.get(chiDinhXetNghiemList.size() - 1);
        assertThat(testChiDinhXetNghiem.isCoBaoHiem()).isEqualTo(UPDATED_CO_BAO_HIEM);
        assertThat(testChiDinhXetNghiem.isCoKetQua()).isEqualTo(UPDATED_CO_KET_QUA);
        assertThat(testChiDinhXetNghiem.getDaThanhToan()).isEqualTo(UPDATED_DA_THANH_TOAN);
        assertThat(testChiDinhXetNghiem.getDaThanhToanChenhLech()).isEqualTo(UPDATED_DA_THANH_TOAN_CHENH_LECH);
        assertThat(testChiDinhXetNghiem.getDaThucHien()).isEqualTo(UPDATED_DA_THUC_HIEN);
        assertThat(testChiDinhXetNghiem.getDonGia()).isEqualTo(UPDATED_DON_GIA);
        assertThat(testChiDinhXetNghiem.getDonGiaBhyt()).isEqualTo(UPDATED_DON_GIA_BHYT);
        assertThat(testChiDinhXetNghiem.getDonGiaKhongBhyt()).isEqualTo(UPDATED_DON_GIA_KHONG_BHYT);
        assertThat(testChiDinhXetNghiem.getGhiChuChiDinh()).isEqualTo(UPDATED_GHI_CHU_CHI_DINH);
        assertThat(testChiDinhXetNghiem.getMoTa()).isEqualTo(UPDATED_MO_TA);
        assertThat(testChiDinhXetNghiem.getNguoiChiDinhId()).isEqualTo(UPDATED_NGUOI_CHI_DINH_ID);
        assertThat(testChiDinhXetNghiem.getSoLuong()).isEqualTo(UPDATED_SO_LUONG);
        assertThat(testChiDinhXetNghiem.getTen()).isEqualTo(UPDATED_TEN);
        assertThat(testChiDinhXetNghiem.getThanhTien()).isEqualTo(UPDATED_THANH_TIEN);
        assertThat(testChiDinhXetNghiem.getThanhTienBhyt()).isEqualTo(UPDATED_THANH_TIEN_BHYT);
        assertThat(testChiDinhXetNghiem.getThanhTienKhongBHYT()).isEqualTo(UPDATED_THANH_TIEN_KHONG_BHYT);
        assertThat(testChiDinhXetNghiem.getThoiGianChiDinh()).isEqualTo(UPDATED_THOI_GIAN_CHI_DINH);
        assertThat(testChiDinhXetNghiem.getThoiGianTao()).isEqualTo(UPDATED_THOI_GIAN_TAO);
        assertThat(testChiDinhXetNghiem.getTienNgoaiBHYT()).isEqualTo(UPDATED_TIEN_NGOAI_BHYT);
        assertThat(testChiDinhXetNghiem.isThanhToanChenhLech()).isEqualTo(UPDATED_THANH_TOAN_CHENH_LECH);
        assertThat(testChiDinhXetNghiem.getTyLeThanhToan()).isEqualTo(UPDATED_TY_LE_THANH_TOAN);
        assertThat(testChiDinhXetNghiem.getMaDungChung()).isEqualTo(UPDATED_MA_DUNG_CHUNG);
        assertThat(testChiDinhXetNghiem.isDichVuYeuCau()).isEqualTo(UPDATED_DICH_VU_YEU_CAU);
        assertThat(testChiDinhXetNghiem.getNam()).isEqualTo(UPDATED_NAM);
    }

    @Test
    @Transactional
    public void updateNonExistingChiDinhXetNghiem() throws Exception {
        int databaseSizeBeforeUpdate = chiDinhXetNghiemRepository.findAll().size();

        // Create the ChiDinhXetNghiem
        ChiDinhXetNghiemDTO chiDinhXetNghiemDTO = chiDinhXetNghiemMapper.toDto(chiDinhXetNghiem);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restChiDinhXetNghiemMockMvc.perform(put("/api/chi-dinh-xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhXetNghiemDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ChiDinhXetNghiem in the database
        List<ChiDinhXetNghiem> chiDinhXetNghiemList = chiDinhXetNghiemRepository.findAll();
        assertThat(chiDinhXetNghiemList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteChiDinhXetNghiem() throws Exception {
        // Initialize the database
        chiDinhXetNghiemRepository.saveAndFlush(chiDinhXetNghiem);

        int databaseSizeBeforeDelete = chiDinhXetNghiemRepository.findAll().size();

        // Delete the chiDinhXetNghiem
        restChiDinhXetNghiemMockMvc.perform(delete("/api/chi-dinh-xet-nghiems/{id}", chiDinhXetNghiem.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ChiDinhXetNghiem> chiDinhXetNghiemList = chiDinhXetNghiemRepository.findAll();
        assertThat(chiDinhXetNghiemList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

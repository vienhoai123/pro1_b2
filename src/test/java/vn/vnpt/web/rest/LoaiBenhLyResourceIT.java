package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.LoaiBenhLy;
import vn.vnpt.repository.LoaiBenhLyRepository;
import vn.vnpt.service.LoaiBenhLyService;
import vn.vnpt.service.dto.LoaiBenhLyDTO;
import vn.vnpt.service.mapper.LoaiBenhLyMapper;
import vn.vnpt.service.dto.LoaiBenhLyCriteria;
import vn.vnpt.service.LoaiBenhLyQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link LoaiBenhLyResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class LoaiBenhLyResourceIT {

    private static final String DEFAULT_KY_HIEU = "AAAAAAAAAA";
    private static final String UPDATED_KY_HIEU = "BBBBBBBBBB";

    private static final String DEFAULT_MO_TA = "AAAAAAAAAA";
    private static final String UPDATED_MO_TA = "BBBBBBBBBB";

    private static final String DEFAULT_TEN = "AAAAAAAAAA";
    private static final String UPDATED_TEN = "BBBBBBBBBB";

    private static final String DEFAULT_TEN_TIENG_ANH = "AAAAAAAAAA";
    private static final String UPDATED_TEN_TIENG_ANH = "BBBBBBBBBB";

    @Autowired
    private LoaiBenhLyRepository loaiBenhLyRepository;

    @Autowired
    private LoaiBenhLyMapper loaiBenhLyMapper;

    @Autowired
    private LoaiBenhLyService loaiBenhLyService;

    @Autowired
    private LoaiBenhLyQueryService loaiBenhLyQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restLoaiBenhLyMockMvc;

    private LoaiBenhLy loaiBenhLy;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LoaiBenhLy createEntity(EntityManager em) {
        LoaiBenhLy loaiBenhLy = new LoaiBenhLy()
            .kyHieu(DEFAULT_KY_HIEU)
            .moTa(DEFAULT_MO_TA)
            .ten(DEFAULT_TEN)
            .tenTiengAnh(DEFAULT_TEN_TIENG_ANH);
        return loaiBenhLy;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LoaiBenhLy createUpdatedEntity(EntityManager em) {
        LoaiBenhLy loaiBenhLy = new LoaiBenhLy()
            .kyHieu(UPDATED_KY_HIEU)
            .moTa(UPDATED_MO_TA)
            .ten(UPDATED_TEN)
            .tenTiengAnh(UPDATED_TEN_TIENG_ANH);
        return loaiBenhLy;
    }

    @BeforeEach
    public void initTest() {
        loaiBenhLy = createEntity(em);
    }

    @Test
    @Transactional
    public void createLoaiBenhLy() throws Exception {
        int databaseSizeBeforeCreate = loaiBenhLyRepository.findAll().size();

        // Create the LoaiBenhLy
        LoaiBenhLyDTO loaiBenhLyDTO = loaiBenhLyMapper.toDto(loaiBenhLy);
        restLoaiBenhLyMockMvc.perform(post("/api/loai-benh-lies").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(loaiBenhLyDTO)))
            .andExpect(status().isCreated());

        // Validate the LoaiBenhLy in the database
        List<LoaiBenhLy> loaiBenhLyList = loaiBenhLyRepository.findAll();
        assertThat(loaiBenhLyList).hasSize(databaseSizeBeforeCreate + 1);
        LoaiBenhLy testLoaiBenhLy = loaiBenhLyList.get(loaiBenhLyList.size() - 1);
        assertThat(testLoaiBenhLy.getKyHieu()).isEqualTo(DEFAULT_KY_HIEU);
        assertThat(testLoaiBenhLy.getMoTa()).isEqualTo(DEFAULT_MO_TA);
        assertThat(testLoaiBenhLy.getTen()).isEqualTo(DEFAULT_TEN);
        assertThat(testLoaiBenhLy.getTenTiengAnh()).isEqualTo(DEFAULT_TEN_TIENG_ANH);
    }

    @Test
    @Transactional
    public void createLoaiBenhLyWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = loaiBenhLyRepository.findAll().size();

        // Create the LoaiBenhLy with an existing ID
        loaiBenhLy.setId(1L);
        LoaiBenhLyDTO loaiBenhLyDTO = loaiBenhLyMapper.toDto(loaiBenhLy);

        // An entity with an existing ID cannot be created, so this API call must fail
        restLoaiBenhLyMockMvc.perform(post("/api/loai-benh-lies").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(loaiBenhLyDTO)))
            .andExpect(status().isBadRequest());

        // Validate the LoaiBenhLy in the database
        List<LoaiBenhLy> loaiBenhLyList = loaiBenhLyRepository.findAll();
        assertThat(loaiBenhLyList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkMoTaIsRequired() throws Exception {
        int databaseSizeBeforeTest = loaiBenhLyRepository.findAll().size();
        // set the field null
        loaiBenhLy.setMoTa(null);

        // Create the LoaiBenhLy, which fails.
        LoaiBenhLyDTO loaiBenhLyDTO = loaiBenhLyMapper.toDto(loaiBenhLy);

        restLoaiBenhLyMockMvc.perform(post("/api/loai-benh-lies").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(loaiBenhLyDTO)))
            .andExpect(status().isBadRequest());

        List<LoaiBenhLy> loaiBenhLyList = loaiBenhLyRepository.findAll();
        assertThat(loaiBenhLyList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTenTiengAnhIsRequired() throws Exception {
        int databaseSizeBeforeTest = loaiBenhLyRepository.findAll().size();
        // set the field null
        loaiBenhLy.setTenTiengAnh(null);

        // Create the LoaiBenhLy, which fails.
        LoaiBenhLyDTO loaiBenhLyDTO = loaiBenhLyMapper.toDto(loaiBenhLy);

        restLoaiBenhLyMockMvc.perform(post("/api/loai-benh-lies").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(loaiBenhLyDTO)))
            .andExpect(status().isBadRequest());

        List<LoaiBenhLy> loaiBenhLyList = loaiBenhLyRepository.findAll();
        assertThat(loaiBenhLyList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllLoaiBenhLies() throws Exception {
        // Initialize the database
        loaiBenhLyRepository.saveAndFlush(loaiBenhLy);

        // Get all the loaiBenhLyList
        restLoaiBenhLyMockMvc.perform(get("/api/loai-benh-lies?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(loaiBenhLy.getId().intValue())))
            .andExpect(jsonPath("$.[*].kyHieu").value(hasItem(DEFAULT_KY_HIEU)))
            .andExpect(jsonPath("$.[*].moTa").value(hasItem(DEFAULT_MO_TA)))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].tenTiengAnh").value(hasItem(DEFAULT_TEN_TIENG_ANH)));
    }
    
    @Test
    @Transactional
    public void getLoaiBenhLy() throws Exception {
        // Initialize the database
        loaiBenhLyRepository.saveAndFlush(loaiBenhLy);

        // Get the loaiBenhLy
        restLoaiBenhLyMockMvc.perform(get("/api/loai-benh-lies/{id}", loaiBenhLy.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(loaiBenhLy.getId().intValue()))
            .andExpect(jsonPath("$.kyHieu").value(DEFAULT_KY_HIEU))
            .andExpect(jsonPath("$.moTa").value(DEFAULT_MO_TA))
            .andExpect(jsonPath("$.ten").value(DEFAULT_TEN))
            .andExpect(jsonPath("$.tenTiengAnh").value(DEFAULT_TEN_TIENG_ANH));
    }


    @Test
    @Transactional
    public void getLoaiBenhLiesByIdFiltering() throws Exception {
        // Initialize the database
        loaiBenhLyRepository.saveAndFlush(loaiBenhLy);

        Long id = loaiBenhLy.getId();

        defaultLoaiBenhLyShouldBeFound("id.equals=" + id);
        defaultLoaiBenhLyShouldNotBeFound("id.notEquals=" + id);

        defaultLoaiBenhLyShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultLoaiBenhLyShouldNotBeFound("id.greaterThan=" + id);

        defaultLoaiBenhLyShouldBeFound("id.lessThanOrEqual=" + id);
        defaultLoaiBenhLyShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllLoaiBenhLiesByKyHieuIsEqualToSomething() throws Exception {
        // Initialize the database
        loaiBenhLyRepository.saveAndFlush(loaiBenhLy);

        // Get all the loaiBenhLyList where kyHieu equals to DEFAULT_KY_HIEU
        defaultLoaiBenhLyShouldBeFound("kyHieu.equals=" + DEFAULT_KY_HIEU);

        // Get all the loaiBenhLyList where kyHieu equals to UPDATED_KY_HIEU
        defaultLoaiBenhLyShouldNotBeFound("kyHieu.equals=" + UPDATED_KY_HIEU);
    }

    @Test
    @Transactional
    public void getAllLoaiBenhLiesByKyHieuIsNotEqualToSomething() throws Exception {
        // Initialize the database
        loaiBenhLyRepository.saveAndFlush(loaiBenhLy);

        // Get all the loaiBenhLyList where kyHieu not equals to DEFAULT_KY_HIEU
        defaultLoaiBenhLyShouldNotBeFound("kyHieu.notEquals=" + DEFAULT_KY_HIEU);

        // Get all the loaiBenhLyList where kyHieu not equals to UPDATED_KY_HIEU
        defaultLoaiBenhLyShouldBeFound("kyHieu.notEquals=" + UPDATED_KY_HIEU);
    }

    @Test
    @Transactional
    public void getAllLoaiBenhLiesByKyHieuIsInShouldWork() throws Exception {
        // Initialize the database
        loaiBenhLyRepository.saveAndFlush(loaiBenhLy);

        // Get all the loaiBenhLyList where kyHieu in DEFAULT_KY_HIEU or UPDATED_KY_HIEU
        defaultLoaiBenhLyShouldBeFound("kyHieu.in=" + DEFAULT_KY_HIEU + "," + UPDATED_KY_HIEU);

        // Get all the loaiBenhLyList where kyHieu equals to UPDATED_KY_HIEU
        defaultLoaiBenhLyShouldNotBeFound("kyHieu.in=" + UPDATED_KY_HIEU);
    }

    @Test
    @Transactional
    public void getAllLoaiBenhLiesByKyHieuIsNullOrNotNull() throws Exception {
        // Initialize the database
        loaiBenhLyRepository.saveAndFlush(loaiBenhLy);

        // Get all the loaiBenhLyList where kyHieu is not null
        defaultLoaiBenhLyShouldBeFound("kyHieu.specified=true");

        // Get all the loaiBenhLyList where kyHieu is null
        defaultLoaiBenhLyShouldNotBeFound("kyHieu.specified=false");
    }
                @Test
    @Transactional
    public void getAllLoaiBenhLiesByKyHieuContainsSomething() throws Exception {
        // Initialize the database
        loaiBenhLyRepository.saveAndFlush(loaiBenhLy);

        // Get all the loaiBenhLyList where kyHieu contains DEFAULT_KY_HIEU
        defaultLoaiBenhLyShouldBeFound("kyHieu.contains=" + DEFAULT_KY_HIEU);

        // Get all the loaiBenhLyList where kyHieu contains UPDATED_KY_HIEU
        defaultLoaiBenhLyShouldNotBeFound("kyHieu.contains=" + UPDATED_KY_HIEU);
    }

    @Test
    @Transactional
    public void getAllLoaiBenhLiesByKyHieuNotContainsSomething() throws Exception {
        // Initialize the database
        loaiBenhLyRepository.saveAndFlush(loaiBenhLy);

        // Get all the loaiBenhLyList where kyHieu does not contain DEFAULT_KY_HIEU
        defaultLoaiBenhLyShouldNotBeFound("kyHieu.doesNotContain=" + DEFAULT_KY_HIEU);

        // Get all the loaiBenhLyList where kyHieu does not contain UPDATED_KY_HIEU
        defaultLoaiBenhLyShouldBeFound("kyHieu.doesNotContain=" + UPDATED_KY_HIEU);
    }


    @Test
    @Transactional
    public void getAllLoaiBenhLiesByMoTaIsEqualToSomething() throws Exception {
        // Initialize the database
        loaiBenhLyRepository.saveAndFlush(loaiBenhLy);

        // Get all the loaiBenhLyList where moTa equals to DEFAULT_MO_TA
        defaultLoaiBenhLyShouldBeFound("moTa.equals=" + DEFAULT_MO_TA);

        // Get all the loaiBenhLyList where moTa equals to UPDATED_MO_TA
        defaultLoaiBenhLyShouldNotBeFound("moTa.equals=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    public void getAllLoaiBenhLiesByMoTaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        loaiBenhLyRepository.saveAndFlush(loaiBenhLy);

        // Get all the loaiBenhLyList where moTa not equals to DEFAULT_MO_TA
        defaultLoaiBenhLyShouldNotBeFound("moTa.notEquals=" + DEFAULT_MO_TA);

        // Get all the loaiBenhLyList where moTa not equals to UPDATED_MO_TA
        defaultLoaiBenhLyShouldBeFound("moTa.notEquals=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    public void getAllLoaiBenhLiesByMoTaIsInShouldWork() throws Exception {
        // Initialize the database
        loaiBenhLyRepository.saveAndFlush(loaiBenhLy);

        // Get all the loaiBenhLyList where moTa in DEFAULT_MO_TA or UPDATED_MO_TA
        defaultLoaiBenhLyShouldBeFound("moTa.in=" + DEFAULT_MO_TA + "," + UPDATED_MO_TA);

        // Get all the loaiBenhLyList where moTa equals to UPDATED_MO_TA
        defaultLoaiBenhLyShouldNotBeFound("moTa.in=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    public void getAllLoaiBenhLiesByMoTaIsNullOrNotNull() throws Exception {
        // Initialize the database
        loaiBenhLyRepository.saveAndFlush(loaiBenhLy);

        // Get all the loaiBenhLyList where moTa is not null
        defaultLoaiBenhLyShouldBeFound("moTa.specified=true");

        // Get all the loaiBenhLyList where moTa is null
        defaultLoaiBenhLyShouldNotBeFound("moTa.specified=false");
    }
                @Test
    @Transactional
    public void getAllLoaiBenhLiesByMoTaContainsSomething() throws Exception {
        // Initialize the database
        loaiBenhLyRepository.saveAndFlush(loaiBenhLy);

        // Get all the loaiBenhLyList where moTa contains DEFAULT_MO_TA
        defaultLoaiBenhLyShouldBeFound("moTa.contains=" + DEFAULT_MO_TA);

        // Get all the loaiBenhLyList where moTa contains UPDATED_MO_TA
        defaultLoaiBenhLyShouldNotBeFound("moTa.contains=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    public void getAllLoaiBenhLiesByMoTaNotContainsSomething() throws Exception {
        // Initialize the database
        loaiBenhLyRepository.saveAndFlush(loaiBenhLy);

        // Get all the loaiBenhLyList where moTa does not contain DEFAULT_MO_TA
        defaultLoaiBenhLyShouldNotBeFound("moTa.doesNotContain=" + DEFAULT_MO_TA);

        // Get all the loaiBenhLyList where moTa does not contain UPDATED_MO_TA
        defaultLoaiBenhLyShouldBeFound("moTa.doesNotContain=" + UPDATED_MO_TA);
    }


    @Test
    @Transactional
    public void getAllLoaiBenhLiesByTenIsEqualToSomething() throws Exception {
        // Initialize the database
        loaiBenhLyRepository.saveAndFlush(loaiBenhLy);

        // Get all the loaiBenhLyList where ten equals to DEFAULT_TEN
        defaultLoaiBenhLyShouldBeFound("ten.equals=" + DEFAULT_TEN);

        // Get all the loaiBenhLyList where ten equals to UPDATED_TEN
        defaultLoaiBenhLyShouldNotBeFound("ten.equals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllLoaiBenhLiesByTenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        loaiBenhLyRepository.saveAndFlush(loaiBenhLy);

        // Get all the loaiBenhLyList where ten not equals to DEFAULT_TEN
        defaultLoaiBenhLyShouldNotBeFound("ten.notEquals=" + DEFAULT_TEN);

        // Get all the loaiBenhLyList where ten not equals to UPDATED_TEN
        defaultLoaiBenhLyShouldBeFound("ten.notEquals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllLoaiBenhLiesByTenIsInShouldWork() throws Exception {
        // Initialize the database
        loaiBenhLyRepository.saveAndFlush(loaiBenhLy);

        // Get all the loaiBenhLyList where ten in DEFAULT_TEN or UPDATED_TEN
        defaultLoaiBenhLyShouldBeFound("ten.in=" + DEFAULT_TEN + "," + UPDATED_TEN);

        // Get all the loaiBenhLyList where ten equals to UPDATED_TEN
        defaultLoaiBenhLyShouldNotBeFound("ten.in=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllLoaiBenhLiesByTenIsNullOrNotNull() throws Exception {
        // Initialize the database
        loaiBenhLyRepository.saveAndFlush(loaiBenhLy);

        // Get all the loaiBenhLyList where ten is not null
        defaultLoaiBenhLyShouldBeFound("ten.specified=true");

        // Get all the loaiBenhLyList where ten is null
        defaultLoaiBenhLyShouldNotBeFound("ten.specified=false");
    }
                @Test
    @Transactional
    public void getAllLoaiBenhLiesByTenContainsSomething() throws Exception {
        // Initialize the database
        loaiBenhLyRepository.saveAndFlush(loaiBenhLy);

        // Get all the loaiBenhLyList where ten contains DEFAULT_TEN
        defaultLoaiBenhLyShouldBeFound("ten.contains=" + DEFAULT_TEN);

        // Get all the loaiBenhLyList where ten contains UPDATED_TEN
        defaultLoaiBenhLyShouldNotBeFound("ten.contains=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllLoaiBenhLiesByTenNotContainsSomething() throws Exception {
        // Initialize the database
        loaiBenhLyRepository.saveAndFlush(loaiBenhLy);

        // Get all the loaiBenhLyList where ten does not contain DEFAULT_TEN
        defaultLoaiBenhLyShouldNotBeFound("ten.doesNotContain=" + DEFAULT_TEN);

        // Get all the loaiBenhLyList where ten does not contain UPDATED_TEN
        defaultLoaiBenhLyShouldBeFound("ten.doesNotContain=" + UPDATED_TEN);
    }


    @Test
    @Transactional
    public void getAllLoaiBenhLiesByTenTiengAnhIsEqualToSomething() throws Exception {
        // Initialize the database
        loaiBenhLyRepository.saveAndFlush(loaiBenhLy);

        // Get all the loaiBenhLyList where tenTiengAnh equals to DEFAULT_TEN_TIENG_ANH
        defaultLoaiBenhLyShouldBeFound("tenTiengAnh.equals=" + DEFAULT_TEN_TIENG_ANH);

        // Get all the loaiBenhLyList where tenTiengAnh equals to UPDATED_TEN_TIENG_ANH
        defaultLoaiBenhLyShouldNotBeFound("tenTiengAnh.equals=" + UPDATED_TEN_TIENG_ANH);
    }

    @Test
    @Transactional
    public void getAllLoaiBenhLiesByTenTiengAnhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        loaiBenhLyRepository.saveAndFlush(loaiBenhLy);

        // Get all the loaiBenhLyList where tenTiengAnh not equals to DEFAULT_TEN_TIENG_ANH
        defaultLoaiBenhLyShouldNotBeFound("tenTiengAnh.notEquals=" + DEFAULT_TEN_TIENG_ANH);

        // Get all the loaiBenhLyList where tenTiengAnh not equals to UPDATED_TEN_TIENG_ANH
        defaultLoaiBenhLyShouldBeFound("tenTiengAnh.notEquals=" + UPDATED_TEN_TIENG_ANH);
    }

    @Test
    @Transactional
    public void getAllLoaiBenhLiesByTenTiengAnhIsInShouldWork() throws Exception {
        // Initialize the database
        loaiBenhLyRepository.saveAndFlush(loaiBenhLy);

        // Get all the loaiBenhLyList where tenTiengAnh in DEFAULT_TEN_TIENG_ANH or UPDATED_TEN_TIENG_ANH
        defaultLoaiBenhLyShouldBeFound("tenTiengAnh.in=" + DEFAULT_TEN_TIENG_ANH + "," + UPDATED_TEN_TIENG_ANH);

        // Get all the loaiBenhLyList where tenTiengAnh equals to UPDATED_TEN_TIENG_ANH
        defaultLoaiBenhLyShouldNotBeFound("tenTiengAnh.in=" + UPDATED_TEN_TIENG_ANH);
    }

    @Test
    @Transactional
    public void getAllLoaiBenhLiesByTenTiengAnhIsNullOrNotNull() throws Exception {
        // Initialize the database
        loaiBenhLyRepository.saveAndFlush(loaiBenhLy);

        // Get all the loaiBenhLyList where tenTiengAnh is not null
        defaultLoaiBenhLyShouldBeFound("tenTiengAnh.specified=true");

        // Get all the loaiBenhLyList where tenTiengAnh is null
        defaultLoaiBenhLyShouldNotBeFound("tenTiengAnh.specified=false");
    }
                @Test
    @Transactional
    public void getAllLoaiBenhLiesByTenTiengAnhContainsSomething() throws Exception {
        // Initialize the database
        loaiBenhLyRepository.saveAndFlush(loaiBenhLy);

        // Get all the loaiBenhLyList where tenTiengAnh contains DEFAULT_TEN_TIENG_ANH
        defaultLoaiBenhLyShouldBeFound("tenTiengAnh.contains=" + DEFAULT_TEN_TIENG_ANH);

        // Get all the loaiBenhLyList where tenTiengAnh contains UPDATED_TEN_TIENG_ANH
        defaultLoaiBenhLyShouldNotBeFound("tenTiengAnh.contains=" + UPDATED_TEN_TIENG_ANH);
    }

    @Test
    @Transactional
    public void getAllLoaiBenhLiesByTenTiengAnhNotContainsSomething() throws Exception {
        // Initialize the database
        loaiBenhLyRepository.saveAndFlush(loaiBenhLy);

        // Get all the loaiBenhLyList where tenTiengAnh does not contain DEFAULT_TEN_TIENG_ANH
        defaultLoaiBenhLyShouldNotBeFound("tenTiengAnh.doesNotContain=" + DEFAULT_TEN_TIENG_ANH);

        // Get all the loaiBenhLyList where tenTiengAnh does not contain UPDATED_TEN_TIENG_ANH
        defaultLoaiBenhLyShouldBeFound("tenTiengAnh.doesNotContain=" + UPDATED_TEN_TIENG_ANH);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultLoaiBenhLyShouldBeFound(String filter) throws Exception {
        restLoaiBenhLyMockMvc.perform(get("/api/loai-benh-lies?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(loaiBenhLy.getId().intValue())))
            .andExpect(jsonPath("$.[*].kyHieu").value(hasItem(DEFAULT_KY_HIEU)))
            .andExpect(jsonPath("$.[*].moTa").value(hasItem(DEFAULT_MO_TA)))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].tenTiengAnh").value(hasItem(DEFAULT_TEN_TIENG_ANH)));

        // Check, that the count call also returns 1
        restLoaiBenhLyMockMvc.perform(get("/api/loai-benh-lies/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultLoaiBenhLyShouldNotBeFound(String filter) throws Exception {
        restLoaiBenhLyMockMvc.perform(get("/api/loai-benh-lies?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restLoaiBenhLyMockMvc.perform(get("/api/loai-benh-lies/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingLoaiBenhLy() throws Exception {
        // Get the loaiBenhLy
        restLoaiBenhLyMockMvc.perform(get("/api/loai-benh-lies/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLoaiBenhLy() throws Exception {
        // Initialize the database
        loaiBenhLyRepository.saveAndFlush(loaiBenhLy);

        int databaseSizeBeforeUpdate = loaiBenhLyRepository.findAll().size();

        // Update the loaiBenhLy
        LoaiBenhLy updatedLoaiBenhLy = loaiBenhLyRepository.findById(loaiBenhLy.getId()).get();
        // Disconnect from session so that the updates on updatedLoaiBenhLy are not directly saved in db
        em.detach(updatedLoaiBenhLy);
        updatedLoaiBenhLy
            .kyHieu(UPDATED_KY_HIEU)
            .moTa(UPDATED_MO_TA)
            .ten(UPDATED_TEN)
            .tenTiengAnh(UPDATED_TEN_TIENG_ANH);
        LoaiBenhLyDTO loaiBenhLyDTO = loaiBenhLyMapper.toDto(updatedLoaiBenhLy);

        restLoaiBenhLyMockMvc.perform(put("/api/loai-benh-lies").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(loaiBenhLyDTO)))
            .andExpect(status().isOk());

        // Validate the LoaiBenhLy in the database
        List<LoaiBenhLy> loaiBenhLyList = loaiBenhLyRepository.findAll();
        assertThat(loaiBenhLyList).hasSize(databaseSizeBeforeUpdate);
        LoaiBenhLy testLoaiBenhLy = loaiBenhLyList.get(loaiBenhLyList.size() - 1);
        assertThat(testLoaiBenhLy.getKyHieu()).isEqualTo(UPDATED_KY_HIEU);
        assertThat(testLoaiBenhLy.getMoTa()).isEqualTo(UPDATED_MO_TA);
        assertThat(testLoaiBenhLy.getTen()).isEqualTo(UPDATED_TEN);
        assertThat(testLoaiBenhLy.getTenTiengAnh()).isEqualTo(UPDATED_TEN_TIENG_ANH);
    }

    @Test
    @Transactional
    public void updateNonExistingLoaiBenhLy() throws Exception {
        int databaseSizeBeforeUpdate = loaiBenhLyRepository.findAll().size();

        // Create the LoaiBenhLy
        LoaiBenhLyDTO loaiBenhLyDTO = loaiBenhLyMapper.toDto(loaiBenhLy);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLoaiBenhLyMockMvc.perform(put("/api/loai-benh-lies").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(loaiBenhLyDTO)))
            .andExpect(status().isBadRequest());

        // Validate the LoaiBenhLy in the database
        List<LoaiBenhLy> loaiBenhLyList = loaiBenhLyRepository.findAll();
        assertThat(loaiBenhLyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteLoaiBenhLy() throws Exception {
        // Initialize the database
        loaiBenhLyRepository.saveAndFlush(loaiBenhLy);

        int databaseSizeBeforeDelete = loaiBenhLyRepository.findAll().size();

        // Delete the loaiBenhLy
        restLoaiBenhLyMockMvc.perform(delete("/api/loai-benh-lies/{id}", loaiBenhLy.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<LoaiBenhLy> loaiBenhLyList = loaiBenhLyRepository.findAll();
        assertThat(loaiBenhLyList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

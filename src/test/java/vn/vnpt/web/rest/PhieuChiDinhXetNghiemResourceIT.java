package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.PhieuChiDinhXetNghiem;
import vn.vnpt.domain.BenhAnKhamBenh;
import vn.vnpt.repository.PhieuChiDinhXetNghiemRepository;
import vn.vnpt.service.PhieuChiDinhXetNghiemService;
import vn.vnpt.service.dto.PhieuChiDinhXetNghiemDTO;
import vn.vnpt.service.mapper.PhieuChiDinhXetNghiemMapper;
import vn.vnpt.service.dto.PhieuChiDinhXetNghiemCriteria;
import vn.vnpt.service.PhieuChiDinhXetNghiemQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link PhieuChiDinhXetNghiemResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class PhieuChiDinhXetNghiemResourceIT {

    private static final String DEFAULT_CHAN_DOAN_TONG_QUAT = "AAAAAAAAAA";
    private static final String UPDATED_CHAN_DOAN_TONG_QUAT = "BBBBBBBBBB";

    private static final String DEFAULT_GHI_CHU = "AAAAAAAAAA";
    private static final String UPDATED_GHI_CHU = "BBBBBBBBBB";

    private static final String DEFAULT_KET_QUA_TONG_QUAT = "AAAAAAAAAA";
    private static final String UPDATED_KET_QUA_TONG_QUAT = "BBBBBBBBBB";

    private static final Integer DEFAULT_NAM = 1;
    private static final Integer UPDATED_NAM = 2;
    private static final Integer SMALLER_NAM = 1 - 1;

    @Autowired
    private PhieuChiDinhXetNghiemRepository phieuChiDinhXetNghiemRepository;

    @Autowired
    private PhieuChiDinhXetNghiemMapper phieuChiDinhXetNghiemMapper;

    @Autowired
    private PhieuChiDinhXetNghiemService phieuChiDinhXetNghiemService;

    @Autowired
    private PhieuChiDinhXetNghiemQueryService phieuChiDinhXetNghiemQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPhieuChiDinhXetNghiemMockMvc;

    private PhieuChiDinhXetNghiem phieuChiDinhXetNghiem;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PhieuChiDinhXetNghiem createEntity(EntityManager em) {
        PhieuChiDinhXetNghiem phieuChiDinhXetNghiem = new PhieuChiDinhXetNghiem()
            .chanDoanTongQuat(DEFAULT_CHAN_DOAN_TONG_QUAT)
            .ghiChu(DEFAULT_GHI_CHU)
            .ketQuaTongQuat(DEFAULT_KET_QUA_TONG_QUAT)
            .nam(DEFAULT_NAM);
        // Add required entity
        BenhAnKhamBenh benhAnKhamBenh;
        if (TestUtil.findAll(em, BenhAnKhamBenh.class).isEmpty()) {
            benhAnKhamBenh = BenhAnKhamBenhResourceIT.createEntity(em);
            em.persist(benhAnKhamBenh);
            em.flush();
        } else {
            benhAnKhamBenh = TestUtil.findAll(em, BenhAnKhamBenh.class).get(0);
        }
        phieuChiDinhXetNghiem.setBenhNhan(benhAnKhamBenh);
        // Add required entity
        phieuChiDinhXetNghiem.setDonVi(benhAnKhamBenh);
        // Add required entity
        phieuChiDinhXetNghiem.setBakb(benhAnKhamBenh);
        return phieuChiDinhXetNghiem;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PhieuChiDinhXetNghiem createUpdatedEntity(EntityManager em) {
        PhieuChiDinhXetNghiem phieuChiDinhXetNghiem = new PhieuChiDinhXetNghiem()
            .chanDoanTongQuat(UPDATED_CHAN_DOAN_TONG_QUAT)
            .ghiChu(UPDATED_GHI_CHU)
            .ketQuaTongQuat(UPDATED_KET_QUA_TONG_QUAT)
            .nam(UPDATED_NAM);
        // Add required entity
        BenhAnKhamBenh benhAnKhamBenh;
        if (TestUtil.findAll(em, BenhAnKhamBenh.class).isEmpty()) {
            benhAnKhamBenh = BenhAnKhamBenhResourceIT.createUpdatedEntity(em);
            em.persist(benhAnKhamBenh);
            em.flush();
        } else {
            benhAnKhamBenh = TestUtil.findAll(em, BenhAnKhamBenh.class).get(0);
        }
        phieuChiDinhXetNghiem.setBenhNhan(benhAnKhamBenh);
        // Add required entity
        phieuChiDinhXetNghiem.setDonVi(benhAnKhamBenh);
        // Add required entity
        phieuChiDinhXetNghiem.setBakb(benhAnKhamBenh);
        return phieuChiDinhXetNghiem;
    }

    @BeforeEach
    public void initTest() {
        phieuChiDinhXetNghiem = createEntity(em);
    }

    @Test
    @Transactional
    public void createPhieuChiDinhXetNghiem() throws Exception {
        int databaseSizeBeforeCreate = phieuChiDinhXetNghiemRepository.findAll().size();

        // Create the PhieuChiDinhXetNghiem
        PhieuChiDinhXetNghiemDTO phieuChiDinhXetNghiemDTO = phieuChiDinhXetNghiemMapper.toDto(phieuChiDinhXetNghiem);
        restPhieuChiDinhXetNghiemMockMvc.perform(post("/api/phieu-chi-dinh-xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(phieuChiDinhXetNghiemDTO)))
            .andExpect(status().isCreated());

        // Validate the PhieuChiDinhXetNghiem in the database
        List<PhieuChiDinhXetNghiem> phieuChiDinhXetNghiemList = phieuChiDinhXetNghiemRepository.findAll();
        assertThat(phieuChiDinhXetNghiemList).hasSize(databaseSizeBeforeCreate + 1);
        PhieuChiDinhXetNghiem testPhieuChiDinhXetNghiem = phieuChiDinhXetNghiemList.get(phieuChiDinhXetNghiemList.size() - 1);
        assertThat(testPhieuChiDinhXetNghiem.getChanDoanTongQuat()).isEqualTo(DEFAULT_CHAN_DOAN_TONG_QUAT);
        assertThat(testPhieuChiDinhXetNghiem.getGhiChu()).isEqualTo(DEFAULT_GHI_CHU);
        assertThat(testPhieuChiDinhXetNghiem.getKetQuaTongQuat()).isEqualTo(DEFAULT_KET_QUA_TONG_QUAT);
        assertThat(testPhieuChiDinhXetNghiem.getNam()).isEqualTo(DEFAULT_NAM);
    }

    @Test
    @Transactional
    public void createPhieuChiDinhXetNghiemWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = phieuChiDinhXetNghiemRepository.findAll().size();

        // Create the PhieuChiDinhXetNghiem with an existing ID
        phieuChiDinhXetNghiem.setId(1L);
        PhieuChiDinhXetNghiemDTO phieuChiDinhXetNghiemDTO = phieuChiDinhXetNghiemMapper.toDto(phieuChiDinhXetNghiem);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPhieuChiDinhXetNghiemMockMvc.perform(post("/api/phieu-chi-dinh-xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(phieuChiDinhXetNghiemDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PhieuChiDinhXetNghiem in the database
        List<PhieuChiDinhXetNghiem> phieuChiDinhXetNghiemList = phieuChiDinhXetNghiemRepository.findAll();
        assertThat(phieuChiDinhXetNghiemList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNamIsRequired() throws Exception {
        int databaseSizeBeforeTest = phieuChiDinhXetNghiemRepository.findAll().size();
        // set the field null
        phieuChiDinhXetNghiem.setNam(null);

        // Create the PhieuChiDinhXetNghiem, which fails.
        PhieuChiDinhXetNghiemDTO phieuChiDinhXetNghiemDTO = phieuChiDinhXetNghiemMapper.toDto(phieuChiDinhXetNghiem);

        restPhieuChiDinhXetNghiemMockMvc.perform(post("/api/phieu-chi-dinh-xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(phieuChiDinhXetNghiemDTO)))
            .andExpect(status().isBadRequest());

        List<PhieuChiDinhXetNghiem> phieuChiDinhXetNghiemList = phieuChiDinhXetNghiemRepository.findAll();
        assertThat(phieuChiDinhXetNghiemList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhXetNghiems() throws Exception {
        // Initialize the database
        phieuChiDinhXetNghiemRepository.saveAndFlush(phieuChiDinhXetNghiem);

        // Get all the phieuChiDinhXetNghiemList
        restPhieuChiDinhXetNghiemMockMvc.perform(get("/api/phieu-chi-dinh-xet-nghiems?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(phieuChiDinhXetNghiem.getId().intValue())))
            .andExpect(jsonPath("$.[*].chanDoanTongQuat").value(hasItem(DEFAULT_CHAN_DOAN_TONG_QUAT)))
            .andExpect(jsonPath("$.[*].ghiChu").value(hasItem(DEFAULT_GHI_CHU)))
            .andExpect(jsonPath("$.[*].ketQuaTongQuat").value(hasItem(DEFAULT_KET_QUA_TONG_QUAT)))
            .andExpect(jsonPath("$.[*].nam").value(hasItem(DEFAULT_NAM)));
    }
    
    @Test
    @Transactional
    public void getPhieuChiDinhXetNghiem() throws Exception {
        // Initialize the database
        phieuChiDinhXetNghiemRepository.saveAndFlush(phieuChiDinhXetNghiem);

        // Get the phieuChiDinhXetNghiem
        restPhieuChiDinhXetNghiemMockMvc.perform(get("/api/phieu-chi-dinh-xet-nghiems/{id}", phieuChiDinhXetNghiem.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(phieuChiDinhXetNghiem.getId().intValue()))
            .andExpect(jsonPath("$.chanDoanTongQuat").value(DEFAULT_CHAN_DOAN_TONG_QUAT))
            .andExpect(jsonPath("$.ghiChu").value(DEFAULT_GHI_CHU))
            .andExpect(jsonPath("$.ketQuaTongQuat").value(DEFAULT_KET_QUA_TONG_QUAT))
            .andExpect(jsonPath("$.nam").value(DEFAULT_NAM));
    }


    @Test
    @Transactional
    public void getPhieuChiDinhXetNghiemsByIdFiltering() throws Exception {
        // Initialize the database
        phieuChiDinhXetNghiemRepository.saveAndFlush(phieuChiDinhXetNghiem);

        Long id = phieuChiDinhXetNghiem.getId();

        defaultPhieuChiDinhXetNghiemShouldBeFound("id.equals=" + id);
        defaultPhieuChiDinhXetNghiemShouldNotBeFound("id.notEquals=" + id);

        defaultPhieuChiDinhXetNghiemShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultPhieuChiDinhXetNghiemShouldNotBeFound("id.greaterThan=" + id);

        defaultPhieuChiDinhXetNghiemShouldBeFound("id.lessThanOrEqual=" + id);
        defaultPhieuChiDinhXetNghiemShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllPhieuChiDinhXetNghiemsByChanDoanTongQuatIsEqualToSomething() throws Exception {
        // Initialize the database
        phieuChiDinhXetNghiemRepository.saveAndFlush(phieuChiDinhXetNghiem);

        // Get all the phieuChiDinhXetNghiemList where chanDoanTongQuat equals to DEFAULT_CHAN_DOAN_TONG_QUAT
        defaultPhieuChiDinhXetNghiemShouldBeFound("chanDoanTongQuat.equals=" + DEFAULT_CHAN_DOAN_TONG_QUAT);

        // Get all the phieuChiDinhXetNghiemList where chanDoanTongQuat equals to UPDATED_CHAN_DOAN_TONG_QUAT
        defaultPhieuChiDinhXetNghiemShouldNotBeFound("chanDoanTongQuat.equals=" + UPDATED_CHAN_DOAN_TONG_QUAT);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhXetNghiemsByChanDoanTongQuatIsNotEqualToSomething() throws Exception {
        // Initialize the database
        phieuChiDinhXetNghiemRepository.saveAndFlush(phieuChiDinhXetNghiem);

        // Get all the phieuChiDinhXetNghiemList where chanDoanTongQuat not equals to DEFAULT_CHAN_DOAN_TONG_QUAT
        defaultPhieuChiDinhXetNghiemShouldNotBeFound("chanDoanTongQuat.notEquals=" + DEFAULT_CHAN_DOAN_TONG_QUAT);

        // Get all the phieuChiDinhXetNghiemList where chanDoanTongQuat not equals to UPDATED_CHAN_DOAN_TONG_QUAT
        defaultPhieuChiDinhXetNghiemShouldBeFound("chanDoanTongQuat.notEquals=" + UPDATED_CHAN_DOAN_TONG_QUAT);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhXetNghiemsByChanDoanTongQuatIsInShouldWork() throws Exception {
        // Initialize the database
        phieuChiDinhXetNghiemRepository.saveAndFlush(phieuChiDinhXetNghiem);

        // Get all the phieuChiDinhXetNghiemList where chanDoanTongQuat in DEFAULT_CHAN_DOAN_TONG_QUAT or UPDATED_CHAN_DOAN_TONG_QUAT
        defaultPhieuChiDinhXetNghiemShouldBeFound("chanDoanTongQuat.in=" + DEFAULT_CHAN_DOAN_TONG_QUAT + "," + UPDATED_CHAN_DOAN_TONG_QUAT);

        // Get all the phieuChiDinhXetNghiemList where chanDoanTongQuat equals to UPDATED_CHAN_DOAN_TONG_QUAT
        defaultPhieuChiDinhXetNghiemShouldNotBeFound("chanDoanTongQuat.in=" + UPDATED_CHAN_DOAN_TONG_QUAT);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhXetNghiemsByChanDoanTongQuatIsNullOrNotNull() throws Exception {
        // Initialize the database
        phieuChiDinhXetNghiemRepository.saveAndFlush(phieuChiDinhXetNghiem);

        // Get all the phieuChiDinhXetNghiemList where chanDoanTongQuat is not null
        defaultPhieuChiDinhXetNghiemShouldBeFound("chanDoanTongQuat.specified=true");

        // Get all the phieuChiDinhXetNghiemList where chanDoanTongQuat is null
        defaultPhieuChiDinhXetNghiemShouldNotBeFound("chanDoanTongQuat.specified=false");
    }
                @Test
    @Transactional
    public void getAllPhieuChiDinhXetNghiemsByChanDoanTongQuatContainsSomething() throws Exception {
        // Initialize the database
        phieuChiDinhXetNghiemRepository.saveAndFlush(phieuChiDinhXetNghiem);

        // Get all the phieuChiDinhXetNghiemList where chanDoanTongQuat contains DEFAULT_CHAN_DOAN_TONG_QUAT
        defaultPhieuChiDinhXetNghiemShouldBeFound("chanDoanTongQuat.contains=" + DEFAULT_CHAN_DOAN_TONG_QUAT);

        // Get all the phieuChiDinhXetNghiemList where chanDoanTongQuat contains UPDATED_CHAN_DOAN_TONG_QUAT
        defaultPhieuChiDinhXetNghiemShouldNotBeFound("chanDoanTongQuat.contains=" + UPDATED_CHAN_DOAN_TONG_QUAT);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhXetNghiemsByChanDoanTongQuatNotContainsSomething() throws Exception {
        // Initialize the database
        phieuChiDinhXetNghiemRepository.saveAndFlush(phieuChiDinhXetNghiem);

        // Get all the phieuChiDinhXetNghiemList where chanDoanTongQuat does not contain DEFAULT_CHAN_DOAN_TONG_QUAT
        defaultPhieuChiDinhXetNghiemShouldNotBeFound("chanDoanTongQuat.doesNotContain=" + DEFAULT_CHAN_DOAN_TONG_QUAT);

        // Get all the phieuChiDinhXetNghiemList where chanDoanTongQuat does not contain UPDATED_CHAN_DOAN_TONG_QUAT
        defaultPhieuChiDinhXetNghiemShouldBeFound("chanDoanTongQuat.doesNotContain=" + UPDATED_CHAN_DOAN_TONG_QUAT);
    }


    @Test
    @Transactional
    public void getAllPhieuChiDinhXetNghiemsByGhiChuIsEqualToSomething() throws Exception {
        // Initialize the database
        phieuChiDinhXetNghiemRepository.saveAndFlush(phieuChiDinhXetNghiem);

        // Get all the phieuChiDinhXetNghiemList where ghiChu equals to DEFAULT_GHI_CHU
        defaultPhieuChiDinhXetNghiemShouldBeFound("ghiChu.equals=" + DEFAULT_GHI_CHU);

        // Get all the phieuChiDinhXetNghiemList where ghiChu equals to UPDATED_GHI_CHU
        defaultPhieuChiDinhXetNghiemShouldNotBeFound("ghiChu.equals=" + UPDATED_GHI_CHU);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhXetNghiemsByGhiChuIsNotEqualToSomething() throws Exception {
        // Initialize the database
        phieuChiDinhXetNghiemRepository.saveAndFlush(phieuChiDinhXetNghiem);

        // Get all the phieuChiDinhXetNghiemList where ghiChu not equals to DEFAULT_GHI_CHU
        defaultPhieuChiDinhXetNghiemShouldNotBeFound("ghiChu.notEquals=" + DEFAULT_GHI_CHU);

        // Get all the phieuChiDinhXetNghiemList where ghiChu not equals to UPDATED_GHI_CHU
        defaultPhieuChiDinhXetNghiemShouldBeFound("ghiChu.notEquals=" + UPDATED_GHI_CHU);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhXetNghiemsByGhiChuIsInShouldWork() throws Exception {
        // Initialize the database
        phieuChiDinhXetNghiemRepository.saveAndFlush(phieuChiDinhXetNghiem);

        // Get all the phieuChiDinhXetNghiemList where ghiChu in DEFAULT_GHI_CHU or UPDATED_GHI_CHU
        defaultPhieuChiDinhXetNghiemShouldBeFound("ghiChu.in=" + DEFAULT_GHI_CHU + "," + UPDATED_GHI_CHU);

        // Get all the phieuChiDinhXetNghiemList where ghiChu equals to UPDATED_GHI_CHU
        defaultPhieuChiDinhXetNghiemShouldNotBeFound("ghiChu.in=" + UPDATED_GHI_CHU);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhXetNghiemsByGhiChuIsNullOrNotNull() throws Exception {
        // Initialize the database
        phieuChiDinhXetNghiemRepository.saveAndFlush(phieuChiDinhXetNghiem);

        // Get all the phieuChiDinhXetNghiemList where ghiChu is not null
        defaultPhieuChiDinhXetNghiemShouldBeFound("ghiChu.specified=true");

        // Get all the phieuChiDinhXetNghiemList where ghiChu is null
        defaultPhieuChiDinhXetNghiemShouldNotBeFound("ghiChu.specified=false");
    }
                @Test
    @Transactional
    public void getAllPhieuChiDinhXetNghiemsByGhiChuContainsSomething() throws Exception {
        // Initialize the database
        phieuChiDinhXetNghiemRepository.saveAndFlush(phieuChiDinhXetNghiem);

        // Get all the phieuChiDinhXetNghiemList where ghiChu contains DEFAULT_GHI_CHU
        defaultPhieuChiDinhXetNghiemShouldBeFound("ghiChu.contains=" + DEFAULT_GHI_CHU);

        // Get all the phieuChiDinhXetNghiemList where ghiChu contains UPDATED_GHI_CHU
        defaultPhieuChiDinhXetNghiemShouldNotBeFound("ghiChu.contains=" + UPDATED_GHI_CHU);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhXetNghiemsByGhiChuNotContainsSomething() throws Exception {
        // Initialize the database
        phieuChiDinhXetNghiemRepository.saveAndFlush(phieuChiDinhXetNghiem);

        // Get all the phieuChiDinhXetNghiemList where ghiChu does not contain DEFAULT_GHI_CHU
        defaultPhieuChiDinhXetNghiemShouldNotBeFound("ghiChu.doesNotContain=" + DEFAULT_GHI_CHU);

        // Get all the phieuChiDinhXetNghiemList where ghiChu does not contain UPDATED_GHI_CHU
        defaultPhieuChiDinhXetNghiemShouldBeFound("ghiChu.doesNotContain=" + UPDATED_GHI_CHU);
    }


    @Test
    @Transactional
    public void getAllPhieuChiDinhXetNghiemsByKetQuaTongQuatIsEqualToSomething() throws Exception {
        // Initialize the database
        phieuChiDinhXetNghiemRepository.saveAndFlush(phieuChiDinhXetNghiem);

        // Get all the phieuChiDinhXetNghiemList where ketQuaTongQuat equals to DEFAULT_KET_QUA_TONG_QUAT
        defaultPhieuChiDinhXetNghiemShouldBeFound("ketQuaTongQuat.equals=" + DEFAULT_KET_QUA_TONG_QUAT);

        // Get all the phieuChiDinhXetNghiemList where ketQuaTongQuat equals to UPDATED_KET_QUA_TONG_QUAT
        defaultPhieuChiDinhXetNghiemShouldNotBeFound("ketQuaTongQuat.equals=" + UPDATED_KET_QUA_TONG_QUAT);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhXetNghiemsByKetQuaTongQuatIsNotEqualToSomething() throws Exception {
        // Initialize the database
        phieuChiDinhXetNghiemRepository.saveAndFlush(phieuChiDinhXetNghiem);

        // Get all the phieuChiDinhXetNghiemList where ketQuaTongQuat not equals to DEFAULT_KET_QUA_TONG_QUAT
        defaultPhieuChiDinhXetNghiemShouldNotBeFound("ketQuaTongQuat.notEquals=" + DEFAULT_KET_QUA_TONG_QUAT);

        // Get all the phieuChiDinhXetNghiemList where ketQuaTongQuat not equals to UPDATED_KET_QUA_TONG_QUAT
        defaultPhieuChiDinhXetNghiemShouldBeFound("ketQuaTongQuat.notEquals=" + UPDATED_KET_QUA_TONG_QUAT);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhXetNghiemsByKetQuaTongQuatIsInShouldWork() throws Exception {
        // Initialize the database
        phieuChiDinhXetNghiemRepository.saveAndFlush(phieuChiDinhXetNghiem);

        // Get all the phieuChiDinhXetNghiemList where ketQuaTongQuat in DEFAULT_KET_QUA_TONG_QUAT or UPDATED_KET_QUA_TONG_QUAT
        defaultPhieuChiDinhXetNghiemShouldBeFound("ketQuaTongQuat.in=" + DEFAULT_KET_QUA_TONG_QUAT + "," + UPDATED_KET_QUA_TONG_QUAT);

        // Get all the phieuChiDinhXetNghiemList where ketQuaTongQuat equals to UPDATED_KET_QUA_TONG_QUAT
        defaultPhieuChiDinhXetNghiemShouldNotBeFound("ketQuaTongQuat.in=" + UPDATED_KET_QUA_TONG_QUAT);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhXetNghiemsByKetQuaTongQuatIsNullOrNotNull() throws Exception {
        // Initialize the database
        phieuChiDinhXetNghiemRepository.saveAndFlush(phieuChiDinhXetNghiem);

        // Get all the phieuChiDinhXetNghiemList where ketQuaTongQuat is not null
        defaultPhieuChiDinhXetNghiemShouldBeFound("ketQuaTongQuat.specified=true");

        // Get all the phieuChiDinhXetNghiemList where ketQuaTongQuat is null
        defaultPhieuChiDinhXetNghiemShouldNotBeFound("ketQuaTongQuat.specified=false");
    }
                @Test
    @Transactional
    public void getAllPhieuChiDinhXetNghiemsByKetQuaTongQuatContainsSomething() throws Exception {
        // Initialize the database
        phieuChiDinhXetNghiemRepository.saveAndFlush(phieuChiDinhXetNghiem);

        // Get all the phieuChiDinhXetNghiemList where ketQuaTongQuat contains DEFAULT_KET_QUA_TONG_QUAT
        defaultPhieuChiDinhXetNghiemShouldBeFound("ketQuaTongQuat.contains=" + DEFAULT_KET_QUA_TONG_QUAT);

        // Get all the phieuChiDinhXetNghiemList where ketQuaTongQuat contains UPDATED_KET_QUA_TONG_QUAT
        defaultPhieuChiDinhXetNghiemShouldNotBeFound("ketQuaTongQuat.contains=" + UPDATED_KET_QUA_TONG_QUAT);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhXetNghiemsByKetQuaTongQuatNotContainsSomething() throws Exception {
        // Initialize the database
        phieuChiDinhXetNghiemRepository.saveAndFlush(phieuChiDinhXetNghiem);

        // Get all the phieuChiDinhXetNghiemList where ketQuaTongQuat does not contain DEFAULT_KET_QUA_TONG_QUAT
        defaultPhieuChiDinhXetNghiemShouldNotBeFound("ketQuaTongQuat.doesNotContain=" + DEFAULT_KET_QUA_TONG_QUAT);

        // Get all the phieuChiDinhXetNghiemList where ketQuaTongQuat does not contain UPDATED_KET_QUA_TONG_QUAT
        defaultPhieuChiDinhXetNghiemShouldBeFound("ketQuaTongQuat.doesNotContain=" + UPDATED_KET_QUA_TONG_QUAT);
    }


    @Test
    @Transactional
    public void getAllPhieuChiDinhXetNghiemsByNamIsEqualToSomething() throws Exception {
        // Initialize the database
        phieuChiDinhXetNghiemRepository.saveAndFlush(phieuChiDinhXetNghiem);

        // Get all the phieuChiDinhXetNghiemList where nam equals to DEFAULT_NAM
        defaultPhieuChiDinhXetNghiemShouldBeFound("nam.equals=" + DEFAULT_NAM);

        // Get all the phieuChiDinhXetNghiemList where nam equals to UPDATED_NAM
        defaultPhieuChiDinhXetNghiemShouldNotBeFound("nam.equals=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhXetNghiemsByNamIsNotEqualToSomething() throws Exception {
        // Initialize the database
        phieuChiDinhXetNghiemRepository.saveAndFlush(phieuChiDinhXetNghiem);

        // Get all the phieuChiDinhXetNghiemList where nam not equals to DEFAULT_NAM
        defaultPhieuChiDinhXetNghiemShouldNotBeFound("nam.notEquals=" + DEFAULT_NAM);

        // Get all the phieuChiDinhXetNghiemList where nam not equals to UPDATED_NAM
        defaultPhieuChiDinhXetNghiemShouldBeFound("nam.notEquals=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhXetNghiemsByNamIsInShouldWork() throws Exception {
        // Initialize the database
        phieuChiDinhXetNghiemRepository.saveAndFlush(phieuChiDinhXetNghiem);

        // Get all the phieuChiDinhXetNghiemList where nam in DEFAULT_NAM or UPDATED_NAM
        defaultPhieuChiDinhXetNghiemShouldBeFound("nam.in=" + DEFAULT_NAM + "," + UPDATED_NAM);

        // Get all the phieuChiDinhXetNghiemList where nam equals to UPDATED_NAM
        defaultPhieuChiDinhXetNghiemShouldNotBeFound("nam.in=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhXetNghiemsByNamIsNullOrNotNull() throws Exception {
        // Initialize the database
        phieuChiDinhXetNghiemRepository.saveAndFlush(phieuChiDinhXetNghiem);

        // Get all the phieuChiDinhXetNghiemList where nam is not null
        defaultPhieuChiDinhXetNghiemShouldBeFound("nam.specified=true");

        // Get all the phieuChiDinhXetNghiemList where nam is null
        defaultPhieuChiDinhXetNghiemShouldNotBeFound("nam.specified=false");
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhXetNghiemsByNamIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        phieuChiDinhXetNghiemRepository.saveAndFlush(phieuChiDinhXetNghiem);

        // Get all the phieuChiDinhXetNghiemList where nam is greater than or equal to DEFAULT_NAM
        defaultPhieuChiDinhXetNghiemShouldBeFound("nam.greaterThanOrEqual=" + DEFAULT_NAM);

        // Get all the phieuChiDinhXetNghiemList where nam is greater than or equal to UPDATED_NAM
        defaultPhieuChiDinhXetNghiemShouldNotBeFound("nam.greaterThanOrEqual=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhXetNghiemsByNamIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        phieuChiDinhXetNghiemRepository.saveAndFlush(phieuChiDinhXetNghiem);

        // Get all the phieuChiDinhXetNghiemList where nam is less than or equal to DEFAULT_NAM
        defaultPhieuChiDinhXetNghiemShouldBeFound("nam.lessThanOrEqual=" + DEFAULT_NAM);

        // Get all the phieuChiDinhXetNghiemList where nam is less than or equal to SMALLER_NAM
        defaultPhieuChiDinhXetNghiemShouldNotBeFound("nam.lessThanOrEqual=" + SMALLER_NAM);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhXetNghiemsByNamIsLessThanSomething() throws Exception {
        // Initialize the database
        phieuChiDinhXetNghiemRepository.saveAndFlush(phieuChiDinhXetNghiem);

        // Get all the phieuChiDinhXetNghiemList where nam is less than DEFAULT_NAM
        defaultPhieuChiDinhXetNghiemShouldNotBeFound("nam.lessThan=" + DEFAULT_NAM);

        // Get all the phieuChiDinhXetNghiemList where nam is less than UPDATED_NAM
        defaultPhieuChiDinhXetNghiemShouldBeFound("nam.lessThan=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhXetNghiemsByNamIsGreaterThanSomething() throws Exception {
        // Initialize the database
        phieuChiDinhXetNghiemRepository.saveAndFlush(phieuChiDinhXetNghiem);

        // Get all the phieuChiDinhXetNghiemList where nam is greater than DEFAULT_NAM
        defaultPhieuChiDinhXetNghiemShouldNotBeFound("nam.greaterThan=" + DEFAULT_NAM);

        // Get all the phieuChiDinhXetNghiemList where nam is greater than SMALLER_NAM
        defaultPhieuChiDinhXetNghiemShouldBeFound("nam.greaterThan=" + SMALLER_NAM);
    }


    @Test
    @Transactional
    public void getAllPhieuChiDinhXetNghiemsByBenhNhanIsEqualToSomething() throws Exception {
        // Get already existing entity
        BenhAnKhamBenh benhNhan = phieuChiDinhXetNghiem.getBenhNhan();
        phieuChiDinhXetNghiemRepository.saveAndFlush(phieuChiDinhXetNghiem);
        Long benhNhanId = benhNhan.getId();

        // Get all the phieuChiDinhXetNghiemList where benhNhan equals to benhNhanId
        defaultPhieuChiDinhXetNghiemShouldBeFound("benhNhanId.equals=" + benhNhanId);

        // Get all the phieuChiDinhXetNghiemList where benhNhan equals to benhNhanId + 1
        defaultPhieuChiDinhXetNghiemShouldNotBeFound("benhNhanId.equals=" + (benhNhanId + 1));
    }


    @Test
    @Transactional
    public void getAllPhieuChiDinhXetNghiemsByDonViIsEqualToSomething() throws Exception {
        // Get already existing entity
        BenhAnKhamBenh donVi = phieuChiDinhXetNghiem.getDonVi();
        phieuChiDinhXetNghiemRepository.saveAndFlush(phieuChiDinhXetNghiem);
        Long donViId = donVi.getId();

        // Get all the phieuChiDinhXetNghiemList where donVi equals to donViId
        defaultPhieuChiDinhXetNghiemShouldBeFound("donViId.equals=" + donViId);

        // Get all the phieuChiDinhXetNghiemList where donVi equals to donViId + 1
        defaultPhieuChiDinhXetNghiemShouldNotBeFound("donViId.equals=" + (donViId + 1));
    }


    @Test
    @Transactional
    public void getAllPhieuChiDinhXetNghiemsByBakbIsEqualToSomething() throws Exception {
        // Get already existing entity
        BenhAnKhamBenh bakb = phieuChiDinhXetNghiem.getBakb();
        phieuChiDinhXetNghiemRepository.saveAndFlush(phieuChiDinhXetNghiem);
        Long bakbId = bakb.getId();

        // Get all the phieuChiDinhXetNghiemList where bakb equals to bakbId
        defaultPhieuChiDinhXetNghiemShouldBeFound("bakbId.equals=" + bakbId);

        // Get all the phieuChiDinhXetNghiemList where bakb equals to bakbId + 1
        defaultPhieuChiDinhXetNghiemShouldNotBeFound("bakbId.equals=" + (bakbId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultPhieuChiDinhXetNghiemShouldBeFound(String filter) throws Exception {
        restPhieuChiDinhXetNghiemMockMvc.perform(get("/api/phieu-chi-dinh-xet-nghiems?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(phieuChiDinhXetNghiem.getId().intValue())))
            .andExpect(jsonPath("$.[*].chanDoanTongQuat").value(hasItem(DEFAULT_CHAN_DOAN_TONG_QUAT)))
            .andExpect(jsonPath("$.[*].ghiChu").value(hasItem(DEFAULT_GHI_CHU)))
            .andExpect(jsonPath("$.[*].ketQuaTongQuat").value(hasItem(DEFAULT_KET_QUA_TONG_QUAT)))
            .andExpect(jsonPath("$.[*].nam").value(hasItem(DEFAULT_NAM)));

        // Check, that the count call also returns 1
        restPhieuChiDinhXetNghiemMockMvc.perform(get("/api/phieu-chi-dinh-xet-nghiems/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultPhieuChiDinhXetNghiemShouldNotBeFound(String filter) throws Exception {
        restPhieuChiDinhXetNghiemMockMvc.perform(get("/api/phieu-chi-dinh-xet-nghiems?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restPhieuChiDinhXetNghiemMockMvc.perform(get("/api/phieu-chi-dinh-xet-nghiems/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingPhieuChiDinhXetNghiem() throws Exception {
        // Get the phieuChiDinhXetNghiem
        restPhieuChiDinhXetNghiemMockMvc.perform(get("/api/phieu-chi-dinh-xet-nghiems/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePhieuChiDinhXetNghiem() throws Exception {
        // Initialize the database
        phieuChiDinhXetNghiemRepository.saveAndFlush(phieuChiDinhXetNghiem);

        int databaseSizeBeforeUpdate = phieuChiDinhXetNghiemRepository.findAll().size();

        // Update the phieuChiDinhXetNghiem
        PhieuChiDinhXetNghiem updatedPhieuChiDinhXetNghiem = phieuChiDinhXetNghiemRepository.findById(phieuChiDinhXetNghiem.getId()).get();
        // Disconnect from session so that the updates on updatedPhieuChiDinhXetNghiem are not directly saved in db
        em.detach(updatedPhieuChiDinhXetNghiem);
        updatedPhieuChiDinhXetNghiem
            .chanDoanTongQuat(UPDATED_CHAN_DOAN_TONG_QUAT)
            .ghiChu(UPDATED_GHI_CHU)
            .ketQuaTongQuat(UPDATED_KET_QUA_TONG_QUAT)
            .nam(UPDATED_NAM);
        PhieuChiDinhXetNghiemDTO phieuChiDinhXetNghiemDTO = phieuChiDinhXetNghiemMapper.toDto(updatedPhieuChiDinhXetNghiem);

        restPhieuChiDinhXetNghiemMockMvc.perform(put("/api/phieu-chi-dinh-xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(phieuChiDinhXetNghiemDTO)))
            .andExpect(status().isOk());

        // Validate the PhieuChiDinhXetNghiem in the database
        List<PhieuChiDinhXetNghiem> phieuChiDinhXetNghiemList = phieuChiDinhXetNghiemRepository.findAll();
        assertThat(phieuChiDinhXetNghiemList).hasSize(databaseSizeBeforeUpdate);
        PhieuChiDinhXetNghiem testPhieuChiDinhXetNghiem = phieuChiDinhXetNghiemList.get(phieuChiDinhXetNghiemList.size() - 1);
        assertThat(testPhieuChiDinhXetNghiem.getChanDoanTongQuat()).isEqualTo(UPDATED_CHAN_DOAN_TONG_QUAT);
        assertThat(testPhieuChiDinhXetNghiem.getGhiChu()).isEqualTo(UPDATED_GHI_CHU);
        assertThat(testPhieuChiDinhXetNghiem.getKetQuaTongQuat()).isEqualTo(UPDATED_KET_QUA_TONG_QUAT);
        assertThat(testPhieuChiDinhXetNghiem.getNam()).isEqualTo(UPDATED_NAM);
    }

    @Test
    @Transactional
    public void updateNonExistingPhieuChiDinhXetNghiem() throws Exception {
        int databaseSizeBeforeUpdate = phieuChiDinhXetNghiemRepository.findAll().size();

        // Create the PhieuChiDinhXetNghiem
        PhieuChiDinhXetNghiemDTO phieuChiDinhXetNghiemDTO = phieuChiDinhXetNghiemMapper.toDto(phieuChiDinhXetNghiem);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPhieuChiDinhXetNghiemMockMvc.perform(put("/api/phieu-chi-dinh-xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(phieuChiDinhXetNghiemDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PhieuChiDinhXetNghiem in the database
        List<PhieuChiDinhXetNghiem> phieuChiDinhXetNghiemList = phieuChiDinhXetNghiemRepository.findAll();
        assertThat(phieuChiDinhXetNghiemList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePhieuChiDinhXetNghiem() throws Exception {
        // Initialize the database
        phieuChiDinhXetNghiemRepository.saveAndFlush(phieuChiDinhXetNghiem);

        int databaseSizeBeforeDelete = phieuChiDinhXetNghiemRepository.findAll().size();

        // Delete the phieuChiDinhXetNghiem
        restPhieuChiDinhXetNghiemMockMvc.perform(delete("/api/phieu-chi-dinh-xet-nghiems/{id}", phieuChiDinhXetNghiem.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<PhieuChiDinhXetNghiem> phieuChiDinhXetNghiemList = phieuChiDinhXetNghiemRepository.findAll();
        assertThat(phieuChiDinhXetNghiemList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.ThuThuatPhauThuatApDung;
import vn.vnpt.domain.DotGiaDichVuBhxh;
import vn.vnpt.domain.ThuThuatPhauThuat;
import vn.vnpt.repository.ThuThuatPhauThuatApDungRepository;
import vn.vnpt.service.ThuThuatPhauThuatApDungService;
import vn.vnpt.service.dto.ThuThuatPhauThuatApDungDTO;
import vn.vnpt.service.mapper.ThuThuatPhauThuatApDungMapper;
import vn.vnpt.service.dto.ThuThuatPhauThuatApDungCriteria;
import vn.vnpt.service.ThuThuatPhauThuatApDungQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ThuThuatPhauThuatApDungResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class ThuThuatPhauThuatApDungResourceIT {

    private static final Integer DEFAULT_KY_THUAT_CAO = 1;
    private static final Integer UPDATED_KY_THUAT_CAO = 2;
    private static final Integer SMALLER_KY_THUAT_CAO = 1 - 1;

    private static final String DEFAULT_MA_BAO_CAO_BHXH = "AAAAAAAAAA";
    private static final String UPDATED_MA_BAO_CAO_BHXH = "BBBBBBBBBB";

    private static final String DEFAULT_MA_BAO_CAO_BHYT = "AAAAAAAAAA";
    private static final String UPDATED_MA_BAO_CAO_BHYT = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_NGAY_AP_DUNG = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_AP_DUNG = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_NGAY_AP_DUNG = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_SO_CONG_VAN_BHXH = "AAAAAAAAAA";
    private static final String UPDATED_SO_CONG_VAN_BHXH = "BBBBBBBBBB";

    private static final String DEFAULT_SO_QUYET_DINH = "AAAAAAAAAA";
    private static final String UPDATED_SO_QUYET_DINH = "BBBBBBBBBB";

    private static final String DEFAULT_TEN_BAO_CAO_BHXH = "AAAAAAAAAA";
    private static final String UPDATED_TEN_BAO_CAO_BHXH = "BBBBBBBBBB";

    private static final String DEFAULT_TEN_DICH_VU_KHONG_BHYT = "AAAAAAAAAA";
    private static final String UPDATED_TEN_DICH_VU_KHONG_BHYT = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_TIEN_BENH_NHAN_CHI = new BigDecimal(1);
    private static final BigDecimal UPDATED_TIEN_BENH_NHAN_CHI = new BigDecimal(2);
    private static final BigDecimal SMALLER_TIEN_BENH_NHAN_CHI = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_TIEN_BHXH_CHI = new BigDecimal(1);
    private static final BigDecimal UPDATED_TIEN_BHXH_CHI = new BigDecimal(2);
    private static final BigDecimal SMALLER_TIEN_BHXH_CHI = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_TIEN_NGOAI_BHYT = new BigDecimal(1);
    private static final BigDecimal UPDATED_TIEN_NGOAI_BHYT = new BigDecimal(2);
    private static final BigDecimal SMALLER_TIEN_NGOAI_BHYT = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_TONG_TIEN_THANH_TOAN = new BigDecimal(1);
    private static final BigDecimal UPDATED_TONG_TIEN_THANH_TOAN = new BigDecimal(2);
    private static final BigDecimal SMALLER_TONG_TIEN_THANH_TOAN = new BigDecimal(1 - 1);

    private static final Integer DEFAULT_TY_LE_BHXH_THANH_TOAN = 1;
    private static final Integer UPDATED_TY_LE_BHXH_THANH_TOAN = 2;
    private static final Integer SMALLER_TY_LE_BHXH_THANH_TOAN = 1 - 1;

    private static final BigDecimal DEFAULT_GIA_BHYT = new BigDecimal(1);
    private static final BigDecimal UPDATED_GIA_BHYT = new BigDecimal(2);
    private static final BigDecimal SMALLER_GIA_BHYT = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_GIA_KHONG_BHYT = new BigDecimal(1);
    private static final BigDecimal UPDATED_GIA_KHONG_BHYT = new BigDecimal(2);
    private static final BigDecimal SMALLER_GIA_KHONG_BHYT = new BigDecimal(1 - 1);

    private static final Boolean DEFAULT_DOI_TUONG_DAC_BIET = false;
    private static final Boolean UPDATED_DOI_TUONG_DAC_BIET = true;

    private static final Integer DEFAULT_NGUON_CHI = 1;
    private static final Integer UPDATED_NGUON_CHI = 2;
    private static final Integer SMALLER_NGUON_CHI = 1 - 1;

    private static final Boolean DEFAULT_ENABLE = false;
    private static final Boolean UPDATED_ENABLE = true;

    @Autowired
    private ThuThuatPhauThuatApDungRepository thuThuatPhauThuatApDungRepository;

    @Autowired
    private ThuThuatPhauThuatApDungMapper thuThuatPhauThuatApDungMapper;

    @Autowired
    private ThuThuatPhauThuatApDungService thuThuatPhauThuatApDungService;

    @Autowired
    private ThuThuatPhauThuatApDungQueryService thuThuatPhauThuatApDungQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restThuThuatPhauThuatApDungMockMvc;

    private ThuThuatPhauThuatApDung thuThuatPhauThuatApDung;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ThuThuatPhauThuatApDung createEntity(EntityManager em) {
        ThuThuatPhauThuatApDung thuThuatPhauThuatApDung = new ThuThuatPhauThuatApDung()
            .kyThuatCao(DEFAULT_KY_THUAT_CAO)
            .maBaoCaoBhxh(DEFAULT_MA_BAO_CAO_BHXH)
            .maBaoCaoBhyt(DEFAULT_MA_BAO_CAO_BHYT)
            .ngayApDung(DEFAULT_NGAY_AP_DUNG)
            .soCongVanBhxh(DEFAULT_SO_CONG_VAN_BHXH)
            .soQuyetDinh(DEFAULT_SO_QUYET_DINH)
            .tenBaoCaoBhxh(DEFAULT_TEN_BAO_CAO_BHXH)
            .tenDichVuKhongBhyt(DEFAULT_TEN_DICH_VU_KHONG_BHYT)
            .tienBenhNhanChi(DEFAULT_TIEN_BENH_NHAN_CHI)
            .tienBhxhChi(DEFAULT_TIEN_BHXH_CHI)
            .tienNgoaiBhyt(DEFAULT_TIEN_NGOAI_BHYT)
            .tongTienThanhToan(DEFAULT_TONG_TIEN_THANH_TOAN)
            .tyLeBhxhThanhToan(DEFAULT_TY_LE_BHXH_THANH_TOAN)
            .giaBhyt(DEFAULT_GIA_BHYT)
            .giaKhongBhyt(DEFAULT_GIA_KHONG_BHYT)
            .doiTuongDacBiet(DEFAULT_DOI_TUONG_DAC_BIET)
            .nguonChi(DEFAULT_NGUON_CHI)
            .enable(DEFAULT_ENABLE);
        // Add required entity
        DotGiaDichVuBhxh dotGiaDichVuBhxh;
        if (TestUtil.findAll(em, DotGiaDichVuBhxh.class).isEmpty()) {
            dotGiaDichVuBhxh = DotGiaDichVuBhxhResourceIT.createEntity(em);
            em.persist(dotGiaDichVuBhxh);
            em.flush();
        } else {
            dotGiaDichVuBhxh = TestUtil.findAll(em, DotGiaDichVuBhxh.class).get(0);
        }
        thuThuatPhauThuatApDung.setDotGia(dotGiaDichVuBhxh);
        // Add required entity
        ThuThuatPhauThuat thuThuatPhauThuat;
        if (TestUtil.findAll(em, ThuThuatPhauThuat.class).isEmpty()) {
            thuThuatPhauThuat = ThuThuatPhauThuatResourceIT.createEntity(em);
            em.persist(thuThuatPhauThuat);
            em.flush();
        } else {
            thuThuatPhauThuat = TestUtil.findAll(em, ThuThuatPhauThuat.class).get(0);
        }
        thuThuatPhauThuatApDung.setTtpt(thuThuatPhauThuat);
        return thuThuatPhauThuatApDung;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ThuThuatPhauThuatApDung createUpdatedEntity(EntityManager em) {
        ThuThuatPhauThuatApDung thuThuatPhauThuatApDung = new ThuThuatPhauThuatApDung()
            .kyThuatCao(UPDATED_KY_THUAT_CAO)
            .maBaoCaoBhxh(UPDATED_MA_BAO_CAO_BHXH)
            .maBaoCaoBhyt(UPDATED_MA_BAO_CAO_BHYT)
            .ngayApDung(UPDATED_NGAY_AP_DUNG)
            .soCongVanBhxh(UPDATED_SO_CONG_VAN_BHXH)
            .soQuyetDinh(UPDATED_SO_QUYET_DINH)
            .tenBaoCaoBhxh(UPDATED_TEN_BAO_CAO_BHXH)
            .tenDichVuKhongBhyt(UPDATED_TEN_DICH_VU_KHONG_BHYT)
            .tienBenhNhanChi(UPDATED_TIEN_BENH_NHAN_CHI)
            .tienBhxhChi(UPDATED_TIEN_BHXH_CHI)
            .tienNgoaiBhyt(UPDATED_TIEN_NGOAI_BHYT)
            .tongTienThanhToan(UPDATED_TONG_TIEN_THANH_TOAN)
            .tyLeBhxhThanhToan(UPDATED_TY_LE_BHXH_THANH_TOAN)
            .giaBhyt(UPDATED_GIA_BHYT)
            .giaKhongBhyt(UPDATED_GIA_KHONG_BHYT)
            .doiTuongDacBiet(UPDATED_DOI_TUONG_DAC_BIET)
            .nguonChi(UPDATED_NGUON_CHI)
            .enable(UPDATED_ENABLE);
        // Add required entity
        DotGiaDichVuBhxh dotGiaDichVuBhxh;
        if (TestUtil.findAll(em, DotGiaDichVuBhxh.class).isEmpty()) {
            dotGiaDichVuBhxh = DotGiaDichVuBhxhResourceIT.createUpdatedEntity(em);
            em.persist(dotGiaDichVuBhxh);
            em.flush();
        } else {
            dotGiaDichVuBhxh = TestUtil.findAll(em, DotGiaDichVuBhxh.class).get(0);
        }
        thuThuatPhauThuatApDung.setDotGia(dotGiaDichVuBhxh);
        // Add required entity
        ThuThuatPhauThuat thuThuatPhauThuat;
        if (TestUtil.findAll(em, ThuThuatPhauThuat.class).isEmpty()) {
            thuThuatPhauThuat = ThuThuatPhauThuatResourceIT.createUpdatedEntity(em);
            em.persist(thuThuatPhauThuat);
            em.flush();
        } else {
            thuThuatPhauThuat = TestUtil.findAll(em, ThuThuatPhauThuat.class).get(0);
        }
        thuThuatPhauThuatApDung.setTtpt(thuThuatPhauThuat);
        return thuThuatPhauThuatApDung;
    }

    @BeforeEach
    public void initTest() {
        thuThuatPhauThuatApDung = createEntity(em);
    }

    @Test
    @Transactional
    public void createThuThuatPhauThuatApDung() throws Exception {
        int databaseSizeBeforeCreate = thuThuatPhauThuatApDungRepository.findAll().size();

        // Create the ThuThuatPhauThuatApDung
        ThuThuatPhauThuatApDungDTO thuThuatPhauThuatApDungDTO = thuThuatPhauThuatApDungMapper.toDto(thuThuatPhauThuatApDung);
        restThuThuatPhauThuatApDungMockMvc.perform(post("/api/thu-thuat-phau-thuat-ap-dungs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(thuThuatPhauThuatApDungDTO)))
            .andExpect(status().isCreated());

        // Validate the ThuThuatPhauThuatApDung in the database
        List<ThuThuatPhauThuatApDung> thuThuatPhauThuatApDungList = thuThuatPhauThuatApDungRepository.findAll();
        assertThat(thuThuatPhauThuatApDungList).hasSize(databaseSizeBeforeCreate + 1);
        ThuThuatPhauThuatApDung testThuThuatPhauThuatApDung = thuThuatPhauThuatApDungList.get(thuThuatPhauThuatApDungList.size() - 1);
        assertThat(testThuThuatPhauThuatApDung.getKyThuatCao()).isEqualTo(DEFAULT_KY_THUAT_CAO);
        assertThat(testThuThuatPhauThuatApDung.getMaBaoCaoBhxh()).isEqualTo(DEFAULT_MA_BAO_CAO_BHXH);
        assertThat(testThuThuatPhauThuatApDung.getMaBaoCaoBhyt()).isEqualTo(DEFAULT_MA_BAO_CAO_BHYT);
        assertThat(testThuThuatPhauThuatApDung.getNgayApDung()).isEqualTo(DEFAULT_NGAY_AP_DUNG);
        assertThat(testThuThuatPhauThuatApDung.getSoCongVanBhxh()).isEqualTo(DEFAULT_SO_CONG_VAN_BHXH);
        assertThat(testThuThuatPhauThuatApDung.getSoQuyetDinh()).isEqualTo(DEFAULT_SO_QUYET_DINH);
        assertThat(testThuThuatPhauThuatApDung.getTenBaoCaoBhxh()).isEqualTo(DEFAULT_TEN_BAO_CAO_BHXH);
        assertThat(testThuThuatPhauThuatApDung.getTenDichVuKhongBhyt()).isEqualTo(DEFAULT_TEN_DICH_VU_KHONG_BHYT);
        assertThat(testThuThuatPhauThuatApDung.getTienBenhNhanChi()).isEqualTo(DEFAULT_TIEN_BENH_NHAN_CHI);
        assertThat(testThuThuatPhauThuatApDung.getTienBhxhChi()).isEqualTo(DEFAULT_TIEN_BHXH_CHI);
        assertThat(testThuThuatPhauThuatApDung.getTienNgoaiBhyt()).isEqualTo(DEFAULT_TIEN_NGOAI_BHYT);
        assertThat(testThuThuatPhauThuatApDung.getTongTienThanhToan()).isEqualTo(DEFAULT_TONG_TIEN_THANH_TOAN);
        assertThat(testThuThuatPhauThuatApDung.getTyLeBhxhThanhToan()).isEqualTo(DEFAULT_TY_LE_BHXH_THANH_TOAN);
        assertThat(testThuThuatPhauThuatApDung.getGiaBhyt()).isEqualTo(DEFAULT_GIA_BHYT);
        assertThat(testThuThuatPhauThuatApDung.getGiaKhongBhyt()).isEqualTo(DEFAULT_GIA_KHONG_BHYT);
        assertThat(testThuThuatPhauThuatApDung.isDoiTuongDacBiet()).isEqualTo(DEFAULT_DOI_TUONG_DAC_BIET);
        assertThat(testThuThuatPhauThuatApDung.getNguonChi()).isEqualTo(DEFAULT_NGUON_CHI);
        assertThat(testThuThuatPhauThuatApDung.isEnable()).isEqualTo(DEFAULT_ENABLE);
    }

    @Test
    @Transactional
    public void createThuThuatPhauThuatApDungWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = thuThuatPhauThuatApDungRepository.findAll().size();

        // Create the ThuThuatPhauThuatApDung with an existing ID
        thuThuatPhauThuatApDung.setId(1L);
        ThuThuatPhauThuatApDungDTO thuThuatPhauThuatApDungDTO = thuThuatPhauThuatApDungMapper.toDto(thuThuatPhauThuatApDung);

        // An entity with an existing ID cannot be created, so this API call must fail
        restThuThuatPhauThuatApDungMockMvc.perform(post("/api/thu-thuat-phau-thuat-ap-dungs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(thuThuatPhauThuatApDungDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ThuThuatPhauThuatApDung in the database
        List<ThuThuatPhauThuatApDung> thuThuatPhauThuatApDungList = thuThuatPhauThuatApDungRepository.findAll();
        assertThat(thuThuatPhauThuatApDungList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkKyThuatCaoIsRequired() throws Exception {
        int databaseSizeBeforeTest = thuThuatPhauThuatApDungRepository.findAll().size();
        // set the field null
        thuThuatPhauThuatApDung.setKyThuatCao(null);

        // Create the ThuThuatPhauThuatApDung, which fails.
        ThuThuatPhauThuatApDungDTO thuThuatPhauThuatApDungDTO = thuThuatPhauThuatApDungMapper.toDto(thuThuatPhauThuatApDung);

        restThuThuatPhauThuatApDungMockMvc.perform(post("/api/thu-thuat-phau-thuat-ap-dungs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(thuThuatPhauThuatApDungDTO)))
            .andExpect(status().isBadRequest());

        List<ThuThuatPhauThuatApDung> thuThuatPhauThuatApDungList = thuThuatPhauThuatApDungRepository.findAll();
        assertThat(thuThuatPhauThuatApDungList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNgayApDungIsRequired() throws Exception {
        int databaseSizeBeforeTest = thuThuatPhauThuatApDungRepository.findAll().size();
        // set the field null
        thuThuatPhauThuatApDung.setNgayApDung(null);

        // Create the ThuThuatPhauThuatApDung, which fails.
        ThuThuatPhauThuatApDungDTO thuThuatPhauThuatApDungDTO = thuThuatPhauThuatApDungMapper.toDto(thuThuatPhauThuatApDung);

        restThuThuatPhauThuatApDungMockMvc.perform(post("/api/thu-thuat-phau-thuat-ap-dungs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(thuThuatPhauThuatApDungDTO)))
            .andExpect(status().isBadRequest());

        List<ThuThuatPhauThuatApDung> thuThuatPhauThuatApDungList = thuThuatPhauThuatApDungRepository.findAll();
        assertThat(thuThuatPhauThuatApDungList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkGiaBhytIsRequired() throws Exception {
        int databaseSizeBeforeTest = thuThuatPhauThuatApDungRepository.findAll().size();
        // set the field null
        thuThuatPhauThuatApDung.setGiaBhyt(null);

        // Create the ThuThuatPhauThuatApDung, which fails.
        ThuThuatPhauThuatApDungDTO thuThuatPhauThuatApDungDTO = thuThuatPhauThuatApDungMapper.toDto(thuThuatPhauThuatApDung);

        restThuThuatPhauThuatApDungMockMvc.perform(post("/api/thu-thuat-phau-thuat-ap-dungs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(thuThuatPhauThuatApDungDTO)))
            .andExpect(status().isBadRequest());

        List<ThuThuatPhauThuatApDung> thuThuatPhauThuatApDungList = thuThuatPhauThuatApDungRepository.findAll();
        assertThat(thuThuatPhauThuatApDungList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkGiaKhongBhytIsRequired() throws Exception {
        int databaseSizeBeforeTest = thuThuatPhauThuatApDungRepository.findAll().size();
        // set the field null
        thuThuatPhauThuatApDung.setGiaKhongBhyt(null);

        // Create the ThuThuatPhauThuatApDung, which fails.
        ThuThuatPhauThuatApDungDTO thuThuatPhauThuatApDungDTO = thuThuatPhauThuatApDungMapper.toDto(thuThuatPhauThuatApDung);

        restThuThuatPhauThuatApDungMockMvc.perform(post("/api/thu-thuat-phau-thuat-ap-dungs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(thuThuatPhauThuatApDungDTO)))
            .andExpect(status().isBadRequest());

        List<ThuThuatPhauThuatApDung> thuThuatPhauThuatApDungList = thuThuatPhauThuatApDungRepository.findAll();
        assertThat(thuThuatPhauThuatApDungList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungs() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList
        restThuThuatPhauThuatApDungMockMvc.perform(get("/api/thu-thuat-phau-thuat-ap-dungs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(thuThuatPhauThuatApDung.getId().intValue())))
            .andExpect(jsonPath("$.[*].kyThuatCao").value(hasItem(DEFAULT_KY_THUAT_CAO)))
            .andExpect(jsonPath("$.[*].maBaoCaoBhxh").value(hasItem(DEFAULT_MA_BAO_CAO_BHXH)))
            .andExpect(jsonPath("$.[*].maBaoCaoBhyt").value(hasItem(DEFAULT_MA_BAO_CAO_BHYT)))
            .andExpect(jsonPath("$.[*].ngayApDung").value(hasItem(DEFAULT_NGAY_AP_DUNG.toString())))
            .andExpect(jsonPath("$.[*].soCongVanBhxh").value(hasItem(DEFAULT_SO_CONG_VAN_BHXH)))
            .andExpect(jsonPath("$.[*].soQuyetDinh").value(hasItem(DEFAULT_SO_QUYET_DINH)))
            .andExpect(jsonPath("$.[*].tenBaoCaoBhxh").value(hasItem(DEFAULT_TEN_BAO_CAO_BHXH)))
            .andExpect(jsonPath("$.[*].tenDichVuKhongBhyt").value(hasItem(DEFAULT_TEN_DICH_VU_KHONG_BHYT)))
            .andExpect(jsonPath("$.[*].tienBenhNhanChi").value(hasItem(DEFAULT_TIEN_BENH_NHAN_CHI.intValue())))
            .andExpect(jsonPath("$.[*].tienBhxhChi").value(hasItem(DEFAULT_TIEN_BHXH_CHI.intValue())))
            .andExpect(jsonPath("$.[*].tienNgoaiBhyt").value(hasItem(DEFAULT_TIEN_NGOAI_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].tongTienThanhToan").value(hasItem(DEFAULT_TONG_TIEN_THANH_TOAN.intValue())))
            .andExpect(jsonPath("$.[*].tyLeBhxhThanhToan").value(hasItem(DEFAULT_TY_LE_BHXH_THANH_TOAN)))
            .andExpect(jsonPath("$.[*].giaBhyt").value(hasItem(DEFAULT_GIA_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].giaKhongBhyt").value(hasItem(DEFAULT_GIA_KHONG_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].doiTuongDacBiet").value(hasItem(DEFAULT_DOI_TUONG_DAC_BIET.booleanValue())))
            .andExpect(jsonPath("$.[*].nguonChi").value(hasItem(DEFAULT_NGUON_CHI)))
            .andExpect(jsonPath("$.[*].enable").value(hasItem(DEFAULT_ENABLE.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getThuThuatPhauThuatApDung() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get the thuThuatPhauThuatApDung
        restThuThuatPhauThuatApDungMockMvc.perform(get("/api/thu-thuat-phau-thuat-ap-dungs/{id}", thuThuatPhauThuatApDung.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(thuThuatPhauThuatApDung.getId().intValue()))
            .andExpect(jsonPath("$.kyThuatCao").value(DEFAULT_KY_THUAT_CAO))
            .andExpect(jsonPath("$.maBaoCaoBhxh").value(DEFAULT_MA_BAO_CAO_BHXH))
            .andExpect(jsonPath("$.maBaoCaoBhyt").value(DEFAULT_MA_BAO_CAO_BHYT))
            .andExpect(jsonPath("$.ngayApDung").value(DEFAULT_NGAY_AP_DUNG.toString()))
            .andExpect(jsonPath("$.soCongVanBhxh").value(DEFAULT_SO_CONG_VAN_BHXH))
            .andExpect(jsonPath("$.soQuyetDinh").value(DEFAULT_SO_QUYET_DINH))
            .andExpect(jsonPath("$.tenBaoCaoBhxh").value(DEFAULT_TEN_BAO_CAO_BHXH))
            .andExpect(jsonPath("$.tenDichVuKhongBhyt").value(DEFAULT_TEN_DICH_VU_KHONG_BHYT))
            .andExpect(jsonPath("$.tienBenhNhanChi").value(DEFAULT_TIEN_BENH_NHAN_CHI.intValue()))
            .andExpect(jsonPath("$.tienBhxhChi").value(DEFAULT_TIEN_BHXH_CHI.intValue()))
            .andExpect(jsonPath("$.tienNgoaiBhyt").value(DEFAULT_TIEN_NGOAI_BHYT.intValue()))
            .andExpect(jsonPath("$.tongTienThanhToan").value(DEFAULT_TONG_TIEN_THANH_TOAN.intValue()))
            .andExpect(jsonPath("$.tyLeBhxhThanhToan").value(DEFAULT_TY_LE_BHXH_THANH_TOAN))
            .andExpect(jsonPath("$.giaBhyt").value(DEFAULT_GIA_BHYT.intValue()))
            .andExpect(jsonPath("$.giaKhongBhyt").value(DEFAULT_GIA_KHONG_BHYT.intValue()))
            .andExpect(jsonPath("$.doiTuongDacBiet").value(DEFAULT_DOI_TUONG_DAC_BIET.booleanValue()))
            .andExpect(jsonPath("$.nguonChi").value(DEFAULT_NGUON_CHI))
            .andExpect(jsonPath("$.enable").value(DEFAULT_ENABLE.booleanValue()));
    }


    @Test
    @Transactional
    public void getThuThuatPhauThuatApDungsByIdFiltering() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        Long id = thuThuatPhauThuatApDung.getId();

        defaultThuThuatPhauThuatApDungShouldBeFound("id.equals=" + id);
        defaultThuThuatPhauThuatApDungShouldNotBeFound("id.notEquals=" + id);

        defaultThuThuatPhauThuatApDungShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultThuThuatPhauThuatApDungShouldNotBeFound("id.greaterThan=" + id);

        defaultThuThuatPhauThuatApDungShouldBeFound("id.lessThanOrEqual=" + id);
        defaultThuThuatPhauThuatApDungShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByKyThuatCaoIsEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where kyThuatCao equals to DEFAULT_KY_THUAT_CAO
        defaultThuThuatPhauThuatApDungShouldBeFound("kyThuatCao.equals=" + DEFAULT_KY_THUAT_CAO);

        // Get all the thuThuatPhauThuatApDungList where kyThuatCao equals to UPDATED_KY_THUAT_CAO
        defaultThuThuatPhauThuatApDungShouldNotBeFound("kyThuatCao.equals=" + UPDATED_KY_THUAT_CAO);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByKyThuatCaoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where kyThuatCao not equals to DEFAULT_KY_THUAT_CAO
        defaultThuThuatPhauThuatApDungShouldNotBeFound("kyThuatCao.notEquals=" + DEFAULT_KY_THUAT_CAO);

        // Get all the thuThuatPhauThuatApDungList where kyThuatCao not equals to UPDATED_KY_THUAT_CAO
        defaultThuThuatPhauThuatApDungShouldBeFound("kyThuatCao.notEquals=" + UPDATED_KY_THUAT_CAO);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByKyThuatCaoIsInShouldWork() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where kyThuatCao in DEFAULT_KY_THUAT_CAO or UPDATED_KY_THUAT_CAO
        defaultThuThuatPhauThuatApDungShouldBeFound("kyThuatCao.in=" + DEFAULT_KY_THUAT_CAO + "," + UPDATED_KY_THUAT_CAO);

        // Get all the thuThuatPhauThuatApDungList where kyThuatCao equals to UPDATED_KY_THUAT_CAO
        defaultThuThuatPhauThuatApDungShouldNotBeFound("kyThuatCao.in=" + UPDATED_KY_THUAT_CAO);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByKyThuatCaoIsNullOrNotNull() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where kyThuatCao is not null
        defaultThuThuatPhauThuatApDungShouldBeFound("kyThuatCao.specified=true");

        // Get all the thuThuatPhauThuatApDungList where kyThuatCao is null
        defaultThuThuatPhauThuatApDungShouldNotBeFound("kyThuatCao.specified=false");
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByKyThuatCaoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where kyThuatCao is greater than or equal to DEFAULT_KY_THUAT_CAO
        defaultThuThuatPhauThuatApDungShouldBeFound("kyThuatCao.greaterThanOrEqual=" + DEFAULT_KY_THUAT_CAO);

        // Get all the thuThuatPhauThuatApDungList where kyThuatCao is greater than or equal to UPDATED_KY_THUAT_CAO
        defaultThuThuatPhauThuatApDungShouldNotBeFound("kyThuatCao.greaterThanOrEqual=" + UPDATED_KY_THUAT_CAO);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByKyThuatCaoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where kyThuatCao is less than or equal to DEFAULT_KY_THUAT_CAO
        defaultThuThuatPhauThuatApDungShouldBeFound("kyThuatCao.lessThanOrEqual=" + DEFAULT_KY_THUAT_CAO);

        // Get all the thuThuatPhauThuatApDungList where kyThuatCao is less than or equal to SMALLER_KY_THUAT_CAO
        defaultThuThuatPhauThuatApDungShouldNotBeFound("kyThuatCao.lessThanOrEqual=" + SMALLER_KY_THUAT_CAO);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByKyThuatCaoIsLessThanSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where kyThuatCao is less than DEFAULT_KY_THUAT_CAO
        defaultThuThuatPhauThuatApDungShouldNotBeFound("kyThuatCao.lessThan=" + DEFAULT_KY_THUAT_CAO);

        // Get all the thuThuatPhauThuatApDungList where kyThuatCao is less than UPDATED_KY_THUAT_CAO
        defaultThuThuatPhauThuatApDungShouldBeFound("kyThuatCao.lessThan=" + UPDATED_KY_THUAT_CAO);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByKyThuatCaoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where kyThuatCao is greater than DEFAULT_KY_THUAT_CAO
        defaultThuThuatPhauThuatApDungShouldNotBeFound("kyThuatCao.greaterThan=" + DEFAULT_KY_THUAT_CAO);

        // Get all the thuThuatPhauThuatApDungList where kyThuatCao is greater than SMALLER_KY_THUAT_CAO
        defaultThuThuatPhauThuatApDungShouldBeFound("kyThuatCao.greaterThan=" + SMALLER_KY_THUAT_CAO);
    }


    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByMaBaoCaoBhxhIsEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where maBaoCaoBhxh equals to DEFAULT_MA_BAO_CAO_BHXH
        defaultThuThuatPhauThuatApDungShouldBeFound("maBaoCaoBhxh.equals=" + DEFAULT_MA_BAO_CAO_BHXH);

        // Get all the thuThuatPhauThuatApDungList where maBaoCaoBhxh equals to UPDATED_MA_BAO_CAO_BHXH
        defaultThuThuatPhauThuatApDungShouldNotBeFound("maBaoCaoBhxh.equals=" + UPDATED_MA_BAO_CAO_BHXH);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByMaBaoCaoBhxhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where maBaoCaoBhxh not equals to DEFAULT_MA_BAO_CAO_BHXH
        defaultThuThuatPhauThuatApDungShouldNotBeFound("maBaoCaoBhxh.notEquals=" + DEFAULT_MA_BAO_CAO_BHXH);

        // Get all the thuThuatPhauThuatApDungList where maBaoCaoBhxh not equals to UPDATED_MA_BAO_CAO_BHXH
        defaultThuThuatPhauThuatApDungShouldBeFound("maBaoCaoBhxh.notEquals=" + UPDATED_MA_BAO_CAO_BHXH);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByMaBaoCaoBhxhIsInShouldWork() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where maBaoCaoBhxh in DEFAULT_MA_BAO_CAO_BHXH or UPDATED_MA_BAO_CAO_BHXH
        defaultThuThuatPhauThuatApDungShouldBeFound("maBaoCaoBhxh.in=" + DEFAULT_MA_BAO_CAO_BHXH + "," + UPDATED_MA_BAO_CAO_BHXH);

        // Get all the thuThuatPhauThuatApDungList where maBaoCaoBhxh equals to UPDATED_MA_BAO_CAO_BHXH
        defaultThuThuatPhauThuatApDungShouldNotBeFound("maBaoCaoBhxh.in=" + UPDATED_MA_BAO_CAO_BHXH);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByMaBaoCaoBhxhIsNullOrNotNull() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where maBaoCaoBhxh is not null
        defaultThuThuatPhauThuatApDungShouldBeFound("maBaoCaoBhxh.specified=true");

        // Get all the thuThuatPhauThuatApDungList where maBaoCaoBhxh is null
        defaultThuThuatPhauThuatApDungShouldNotBeFound("maBaoCaoBhxh.specified=false");
    }
                @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByMaBaoCaoBhxhContainsSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where maBaoCaoBhxh contains DEFAULT_MA_BAO_CAO_BHXH
        defaultThuThuatPhauThuatApDungShouldBeFound("maBaoCaoBhxh.contains=" + DEFAULT_MA_BAO_CAO_BHXH);

        // Get all the thuThuatPhauThuatApDungList where maBaoCaoBhxh contains UPDATED_MA_BAO_CAO_BHXH
        defaultThuThuatPhauThuatApDungShouldNotBeFound("maBaoCaoBhxh.contains=" + UPDATED_MA_BAO_CAO_BHXH);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByMaBaoCaoBhxhNotContainsSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where maBaoCaoBhxh does not contain DEFAULT_MA_BAO_CAO_BHXH
        defaultThuThuatPhauThuatApDungShouldNotBeFound("maBaoCaoBhxh.doesNotContain=" + DEFAULT_MA_BAO_CAO_BHXH);

        // Get all the thuThuatPhauThuatApDungList where maBaoCaoBhxh does not contain UPDATED_MA_BAO_CAO_BHXH
        defaultThuThuatPhauThuatApDungShouldBeFound("maBaoCaoBhxh.doesNotContain=" + UPDATED_MA_BAO_CAO_BHXH);
    }


    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByMaBaoCaoBhytIsEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where maBaoCaoBhyt equals to DEFAULT_MA_BAO_CAO_BHYT
        defaultThuThuatPhauThuatApDungShouldBeFound("maBaoCaoBhyt.equals=" + DEFAULT_MA_BAO_CAO_BHYT);

        // Get all the thuThuatPhauThuatApDungList where maBaoCaoBhyt equals to UPDATED_MA_BAO_CAO_BHYT
        defaultThuThuatPhauThuatApDungShouldNotBeFound("maBaoCaoBhyt.equals=" + UPDATED_MA_BAO_CAO_BHYT);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByMaBaoCaoBhytIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where maBaoCaoBhyt not equals to DEFAULT_MA_BAO_CAO_BHYT
        defaultThuThuatPhauThuatApDungShouldNotBeFound("maBaoCaoBhyt.notEquals=" + DEFAULT_MA_BAO_CAO_BHYT);

        // Get all the thuThuatPhauThuatApDungList where maBaoCaoBhyt not equals to UPDATED_MA_BAO_CAO_BHYT
        defaultThuThuatPhauThuatApDungShouldBeFound("maBaoCaoBhyt.notEquals=" + UPDATED_MA_BAO_CAO_BHYT);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByMaBaoCaoBhytIsInShouldWork() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where maBaoCaoBhyt in DEFAULT_MA_BAO_CAO_BHYT or UPDATED_MA_BAO_CAO_BHYT
        defaultThuThuatPhauThuatApDungShouldBeFound("maBaoCaoBhyt.in=" + DEFAULT_MA_BAO_CAO_BHYT + "," + UPDATED_MA_BAO_CAO_BHYT);

        // Get all the thuThuatPhauThuatApDungList where maBaoCaoBhyt equals to UPDATED_MA_BAO_CAO_BHYT
        defaultThuThuatPhauThuatApDungShouldNotBeFound("maBaoCaoBhyt.in=" + UPDATED_MA_BAO_CAO_BHYT);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByMaBaoCaoBhytIsNullOrNotNull() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where maBaoCaoBhyt is not null
        defaultThuThuatPhauThuatApDungShouldBeFound("maBaoCaoBhyt.specified=true");

        // Get all the thuThuatPhauThuatApDungList where maBaoCaoBhyt is null
        defaultThuThuatPhauThuatApDungShouldNotBeFound("maBaoCaoBhyt.specified=false");
    }
                @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByMaBaoCaoBhytContainsSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where maBaoCaoBhyt contains DEFAULT_MA_BAO_CAO_BHYT
        defaultThuThuatPhauThuatApDungShouldBeFound("maBaoCaoBhyt.contains=" + DEFAULT_MA_BAO_CAO_BHYT);

        // Get all the thuThuatPhauThuatApDungList where maBaoCaoBhyt contains UPDATED_MA_BAO_CAO_BHYT
        defaultThuThuatPhauThuatApDungShouldNotBeFound("maBaoCaoBhyt.contains=" + UPDATED_MA_BAO_CAO_BHYT);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByMaBaoCaoBhytNotContainsSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where maBaoCaoBhyt does not contain DEFAULT_MA_BAO_CAO_BHYT
        defaultThuThuatPhauThuatApDungShouldNotBeFound("maBaoCaoBhyt.doesNotContain=" + DEFAULT_MA_BAO_CAO_BHYT);

        // Get all the thuThuatPhauThuatApDungList where maBaoCaoBhyt does not contain UPDATED_MA_BAO_CAO_BHYT
        defaultThuThuatPhauThuatApDungShouldBeFound("maBaoCaoBhyt.doesNotContain=" + UPDATED_MA_BAO_CAO_BHYT);
    }


    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByNgayApDungIsEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where ngayApDung equals to DEFAULT_NGAY_AP_DUNG
        defaultThuThuatPhauThuatApDungShouldBeFound("ngayApDung.equals=" + DEFAULT_NGAY_AP_DUNG);

        // Get all the thuThuatPhauThuatApDungList where ngayApDung equals to UPDATED_NGAY_AP_DUNG
        defaultThuThuatPhauThuatApDungShouldNotBeFound("ngayApDung.equals=" + UPDATED_NGAY_AP_DUNG);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByNgayApDungIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where ngayApDung not equals to DEFAULT_NGAY_AP_DUNG
        defaultThuThuatPhauThuatApDungShouldNotBeFound("ngayApDung.notEquals=" + DEFAULT_NGAY_AP_DUNG);

        // Get all the thuThuatPhauThuatApDungList where ngayApDung not equals to UPDATED_NGAY_AP_DUNG
        defaultThuThuatPhauThuatApDungShouldBeFound("ngayApDung.notEquals=" + UPDATED_NGAY_AP_DUNG);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByNgayApDungIsInShouldWork() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where ngayApDung in DEFAULT_NGAY_AP_DUNG or UPDATED_NGAY_AP_DUNG
        defaultThuThuatPhauThuatApDungShouldBeFound("ngayApDung.in=" + DEFAULT_NGAY_AP_DUNG + "," + UPDATED_NGAY_AP_DUNG);

        // Get all the thuThuatPhauThuatApDungList where ngayApDung equals to UPDATED_NGAY_AP_DUNG
        defaultThuThuatPhauThuatApDungShouldNotBeFound("ngayApDung.in=" + UPDATED_NGAY_AP_DUNG);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByNgayApDungIsNullOrNotNull() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where ngayApDung is not null
        defaultThuThuatPhauThuatApDungShouldBeFound("ngayApDung.specified=true");

        // Get all the thuThuatPhauThuatApDungList where ngayApDung is null
        defaultThuThuatPhauThuatApDungShouldNotBeFound("ngayApDung.specified=false");
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByNgayApDungIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where ngayApDung is greater than or equal to DEFAULT_NGAY_AP_DUNG
        defaultThuThuatPhauThuatApDungShouldBeFound("ngayApDung.greaterThanOrEqual=" + DEFAULT_NGAY_AP_DUNG);

        // Get all the thuThuatPhauThuatApDungList where ngayApDung is greater than or equal to UPDATED_NGAY_AP_DUNG
        defaultThuThuatPhauThuatApDungShouldNotBeFound("ngayApDung.greaterThanOrEqual=" + UPDATED_NGAY_AP_DUNG);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByNgayApDungIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where ngayApDung is less than or equal to DEFAULT_NGAY_AP_DUNG
        defaultThuThuatPhauThuatApDungShouldBeFound("ngayApDung.lessThanOrEqual=" + DEFAULT_NGAY_AP_DUNG);

        // Get all the thuThuatPhauThuatApDungList where ngayApDung is less than or equal to SMALLER_NGAY_AP_DUNG
        defaultThuThuatPhauThuatApDungShouldNotBeFound("ngayApDung.lessThanOrEqual=" + SMALLER_NGAY_AP_DUNG);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByNgayApDungIsLessThanSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where ngayApDung is less than DEFAULT_NGAY_AP_DUNG
        defaultThuThuatPhauThuatApDungShouldNotBeFound("ngayApDung.lessThan=" + DEFAULT_NGAY_AP_DUNG);

        // Get all the thuThuatPhauThuatApDungList where ngayApDung is less than UPDATED_NGAY_AP_DUNG
        defaultThuThuatPhauThuatApDungShouldBeFound("ngayApDung.lessThan=" + UPDATED_NGAY_AP_DUNG);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByNgayApDungIsGreaterThanSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where ngayApDung is greater than DEFAULT_NGAY_AP_DUNG
        defaultThuThuatPhauThuatApDungShouldNotBeFound("ngayApDung.greaterThan=" + DEFAULT_NGAY_AP_DUNG);

        // Get all the thuThuatPhauThuatApDungList where ngayApDung is greater than SMALLER_NGAY_AP_DUNG
        defaultThuThuatPhauThuatApDungShouldBeFound("ngayApDung.greaterThan=" + SMALLER_NGAY_AP_DUNG);
    }


    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsBySoCongVanBhxhIsEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where soCongVanBhxh equals to DEFAULT_SO_CONG_VAN_BHXH
        defaultThuThuatPhauThuatApDungShouldBeFound("soCongVanBhxh.equals=" + DEFAULT_SO_CONG_VAN_BHXH);

        // Get all the thuThuatPhauThuatApDungList where soCongVanBhxh equals to UPDATED_SO_CONG_VAN_BHXH
        defaultThuThuatPhauThuatApDungShouldNotBeFound("soCongVanBhxh.equals=" + UPDATED_SO_CONG_VAN_BHXH);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsBySoCongVanBhxhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where soCongVanBhxh not equals to DEFAULT_SO_CONG_VAN_BHXH
        defaultThuThuatPhauThuatApDungShouldNotBeFound("soCongVanBhxh.notEquals=" + DEFAULT_SO_CONG_VAN_BHXH);

        // Get all the thuThuatPhauThuatApDungList where soCongVanBhxh not equals to UPDATED_SO_CONG_VAN_BHXH
        defaultThuThuatPhauThuatApDungShouldBeFound("soCongVanBhxh.notEquals=" + UPDATED_SO_CONG_VAN_BHXH);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsBySoCongVanBhxhIsInShouldWork() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where soCongVanBhxh in DEFAULT_SO_CONG_VAN_BHXH or UPDATED_SO_CONG_VAN_BHXH
        defaultThuThuatPhauThuatApDungShouldBeFound("soCongVanBhxh.in=" + DEFAULT_SO_CONG_VAN_BHXH + "," + UPDATED_SO_CONG_VAN_BHXH);

        // Get all the thuThuatPhauThuatApDungList where soCongVanBhxh equals to UPDATED_SO_CONG_VAN_BHXH
        defaultThuThuatPhauThuatApDungShouldNotBeFound("soCongVanBhxh.in=" + UPDATED_SO_CONG_VAN_BHXH);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsBySoCongVanBhxhIsNullOrNotNull() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where soCongVanBhxh is not null
        defaultThuThuatPhauThuatApDungShouldBeFound("soCongVanBhxh.specified=true");

        // Get all the thuThuatPhauThuatApDungList where soCongVanBhxh is null
        defaultThuThuatPhauThuatApDungShouldNotBeFound("soCongVanBhxh.specified=false");
    }
                @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsBySoCongVanBhxhContainsSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where soCongVanBhxh contains DEFAULT_SO_CONG_VAN_BHXH
        defaultThuThuatPhauThuatApDungShouldBeFound("soCongVanBhxh.contains=" + DEFAULT_SO_CONG_VAN_BHXH);

        // Get all the thuThuatPhauThuatApDungList where soCongVanBhxh contains UPDATED_SO_CONG_VAN_BHXH
        defaultThuThuatPhauThuatApDungShouldNotBeFound("soCongVanBhxh.contains=" + UPDATED_SO_CONG_VAN_BHXH);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsBySoCongVanBhxhNotContainsSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where soCongVanBhxh does not contain DEFAULT_SO_CONG_VAN_BHXH
        defaultThuThuatPhauThuatApDungShouldNotBeFound("soCongVanBhxh.doesNotContain=" + DEFAULT_SO_CONG_VAN_BHXH);

        // Get all the thuThuatPhauThuatApDungList where soCongVanBhxh does not contain UPDATED_SO_CONG_VAN_BHXH
        defaultThuThuatPhauThuatApDungShouldBeFound("soCongVanBhxh.doesNotContain=" + UPDATED_SO_CONG_VAN_BHXH);
    }


    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsBySoQuyetDinhIsEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where soQuyetDinh equals to DEFAULT_SO_QUYET_DINH
        defaultThuThuatPhauThuatApDungShouldBeFound("soQuyetDinh.equals=" + DEFAULT_SO_QUYET_DINH);

        // Get all the thuThuatPhauThuatApDungList where soQuyetDinh equals to UPDATED_SO_QUYET_DINH
        defaultThuThuatPhauThuatApDungShouldNotBeFound("soQuyetDinh.equals=" + UPDATED_SO_QUYET_DINH);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsBySoQuyetDinhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where soQuyetDinh not equals to DEFAULT_SO_QUYET_DINH
        defaultThuThuatPhauThuatApDungShouldNotBeFound("soQuyetDinh.notEquals=" + DEFAULT_SO_QUYET_DINH);

        // Get all the thuThuatPhauThuatApDungList where soQuyetDinh not equals to UPDATED_SO_QUYET_DINH
        defaultThuThuatPhauThuatApDungShouldBeFound("soQuyetDinh.notEquals=" + UPDATED_SO_QUYET_DINH);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsBySoQuyetDinhIsInShouldWork() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where soQuyetDinh in DEFAULT_SO_QUYET_DINH or UPDATED_SO_QUYET_DINH
        defaultThuThuatPhauThuatApDungShouldBeFound("soQuyetDinh.in=" + DEFAULT_SO_QUYET_DINH + "," + UPDATED_SO_QUYET_DINH);

        // Get all the thuThuatPhauThuatApDungList where soQuyetDinh equals to UPDATED_SO_QUYET_DINH
        defaultThuThuatPhauThuatApDungShouldNotBeFound("soQuyetDinh.in=" + UPDATED_SO_QUYET_DINH);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsBySoQuyetDinhIsNullOrNotNull() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where soQuyetDinh is not null
        defaultThuThuatPhauThuatApDungShouldBeFound("soQuyetDinh.specified=true");

        // Get all the thuThuatPhauThuatApDungList where soQuyetDinh is null
        defaultThuThuatPhauThuatApDungShouldNotBeFound("soQuyetDinh.specified=false");
    }
                @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsBySoQuyetDinhContainsSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where soQuyetDinh contains DEFAULT_SO_QUYET_DINH
        defaultThuThuatPhauThuatApDungShouldBeFound("soQuyetDinh.contains=" + DEFAULT_SO_QUYET_DINH);

        // Get all the thuThuatPhauThuatApDungList where soQuyetDinh contains UPDATED_SO_QUYET_DINH
        defaultThuThuatPhauThuatApDungShouldNotBeFound("soQuyetDinh.contains=" + UPDATED_SO_QUYET_DINH);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsBySoQuyetDinhNotContainsSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where soQuyetDinh does not contain DEFAULT_SO_QUYET_DINH
        defaultThuThuatPhauThuatApDungShouldNotBeFound("soQuyetDinh.doesNotContain=" + DEFAULT_SO_QUYET_DINH);

        // Get all the thuThuatPhauThuatApDungList where soQuyetDinh does not contain UPDATED_SO_QUYET_DINH
        defaultThuThuatPhauThuatApDungShouldBeFound("soQuyetDinh.doesNotContain=" + UPDATED_SO_QUYET_DINH);
    }


    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTenBaoCaoBhxhIsEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tenBaoCaoBhxh equals to DEFAULT_TEN_BAO_CAO_BHXH
        defaultThuThuatPhauThuatApDungShouldBeFound("tenBaoCaoBhxh.equals=" + DEFAULT_TEN_BAO_CAO_BHXH);

        // Get all the thuThuatPhauThuatApDungList where tenBaoCaoBhxh equals to UPDATED_TEN_BAO_CAO_BHXH
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tenBaoCaoBhxh.equals=" + UPDATED_TEN_BAO_CAO_BHXH);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTenBaoCaoBhxhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tenBaoCaoBhxh not equals to DEFAULT_TEN_BAO_CAO_BHXH
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tenBaoCaoBhxh.notEquals=" + DEFAULT_TEN_BAO_CAO_BHXH);

        // Get all the thuThuatPhauThuatApDungList where tenBaoCaoBhxh not equals to UPDATED_TEN_BAO_CAO_BHXH
        defaultThuThuatPhauThuatApDungShouldBeFound("tenBaoCaoBhxh.notEquals=" + UPDATED_TEN_BAO_CAO_BHXH);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTenBaoCaoBhxhIsInShouldWork() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tenBaoCaoBhxh in DEFAULT_TEN_BAO_CAO_BHXH or UPDATED_TEN_BAO_CAO_BHXH
        defaultThuThuatPhauThuatApDungShouldBeFound("tenBaoCaoBhxh.in=" + DEFAULT_TEN_BAO_CAO_BHXH + "," + UPDATED_TEN_BAO_CAO_BHXH);

        // Get all the thuThuatPhauThuatApDungList where tenBaoCaoBhxh equals to UPDATED_TEN_BAO_CAO_BHXH
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tenBaoCaoBhxh.in=" + UPDATED_TEN_BAO_CAO_BHXH);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTenBaoCaoBhxhIsNullOrNotNull() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tenBaoCaoBhxh is not null
        defaultThuThuatPhauThuatApDungShouldBeFound("tenBaoCaoBhxh.specified=true");

        // Get all the thuThuatPhauThuatApDungList where tenBaoCaoBhxh is null
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tenBaoCaoBhxh.specified=false");
    }
                @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTenBaoCaoBhxhContainsSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tenBaoCaoBhxh contains DEFAULT_TEN_BAO_CAO_BHXH
        defaultThuThuatPhauThuatApDungShouldBeFound("tenBaoCaoBhxh.contains=" + DEFAULT_TEN_BAO_CAO_BHXH);

        // Get all the thuThuatPhauThuatApDungList where tenBaoCaoBhxh contains UPDATED_TEN_BAO_CAO_BHXH
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tenBaoCaoBhxh.contains=" + UPDATED_TEN_BAO_CAO_BHXH);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTenBaoCaoBhxhNotContainsSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tenBaoCaoBhxh does not contain DEFAULT_TEN_BAO_CAO_BHXH
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tenBaoCaoBhxh.doesNotContain=" + DEFAULT_TEN_BAO_CAO_BHXH);

        // Get all the thuThuatPhauThuatApDungList where tenBaoCaoBhxh does not contain UPDATED_TEN_BAO_CAO_BHXH
        defaultThuThuatPhauThuatApDungShouldBeFound("tenBaoCaoBhxh.doesNotContain=" + UPDATED_TEN_BAO_CAO_BHXH);
    }


    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTenDichVuKhongBhytIsEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tenDichVuKhongBhyt equals to DEFAULT_TEN_DICH_VU_KHONG_BHYT
        defaultThuThuatPhauThuatApDungShouldBeFound("tenDichVuKhongBhyt.equals=" + DEFAULT_TEN_DICH_VU_KHONG_BHYT);

        // Get all the thuThuatPhauThuatApDungList where tenDichVuKhongBhyt equals to UPDATED_TEN_DICH_VU_KHONG_BHYT
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tenDichVuKhongBhyt.equals=" + UPDATED_TEN_DICH_VU_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTenDichVuKhongBhytIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tenDichVuKhongBhyt not equals to DEFAULT_TEN_DICH_VU_KHONG_BHYT
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tenDichVuKhongBhyt.notEquals=" + DEFAULT_TEN_DICH_VU_KHONG_BHYT);

        // Get all the thuThuatPhauThuatApDungList where tenDichVuKhongBhyt not equals to UPDATED_TEN_DICH_VU_KHONG_BHYT
        defaultThuThuatPhauThuatApDungShouldBeFound("tenDichVuKhongBhyt.notEquals=" + UPDATED_TEN_DICH_VU_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTenDichVuKhongBhytIsInShouldWork() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tenDichVuKhongBhyt in DEFAULT_TEN_DICH_VU_KHONG_BHYT or UPDATED_TEN_DICH_VU_KHONG_BHYT
        defaultThuThuatPhauThuatApDungShouldBeFound("tenDichVuKhongBhyt.in=" + DEFAULT_TEN_DICH_VU_KHONG_BHYT + "," + UPDATED_TEN_DICH_VU_KHONG_BHYT);

        // Get all the thuThuatPhauThuatApDungList where tenDichVuKhongBhyt equals to UPDATED_TEN_DICH_VU_KHONG_BHYT
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tenDichVuKhongBhyt.in=" + UPDATED_TEN_DICH_VU_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTenDichVuKhongBhytIsNullOrNotNull() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tenDichVuKhongBhyt is not null
        defaultThuThuatPhauThuatApDungShouldBeFound("tenDichVuKhongBhyt.specified=true");

        // Get all the thuThuatPhauThuatApDungList where tenDichVuKhongBhyt is null
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tenDichVuKhongBhyt.specified=false");
    }
                @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTenDichVuKhongBhytContainsSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tenDichVuKhongBhyt contains DEFAULT_TEN_DICH_VU_KHONG_BHYT
        defaultThuThuatPhauThuatApDungShouldBeFound("tenDichVuKhongBhyt.contains=" + DEFAULT_TEN_DICH_VU_KHONG_BHYT);

        // Get all the thuThuatPhauThuatApDungList where tenDichVuKhongBhyt contains UPDATED_TEN_DICH_VU_KHONG_BHYT
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tenDichVuKhongBhyt.contains=" + UPDATED_TEN_DICH_VU_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTenDichVuKhongBhytNotContainsSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tenDichVuKhongBhyt does not contain DEFAULT_TEN_DICH_VU_KHONG_BHYT
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tenDichVuKhongBhyt.doesNotContain=" + DEFAULT_TEN_DICH_VU_KHONG_BHYT);

        // Get all the thuThuatPhauThuatApDungList where tenDichVuKhongBhyt does not contain UPDATED_TEN_DICH_VU_KHONG_BHYT
        defaultThuThuatPhauThuatApDungShouldBeFound("tenDichVuKhongBhyt.doesNotContain=" + UPDATED_TEN_DICH_VU_KHONG_BHYT);
    }


    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTienBenhNhanChiIsEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tienBenhNhanChi equals to DEFAULT_TIEN_BENH_NHAN_CHI
        defaultThuThuatPhauThuatApDungShouldBeFound("tienBenhNhanChi.equals=" + DEFAULT_TIEN_BENH_NHAN_CHI);

        // Get all the thuThuatPhauThuatApDungList where tienBenhNhanChi equals to UPDATED_TIEN_BENH_NHAN_CHI
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tienBenhNhanChi.equals=" + UPDATED_TIEN_BENH_NHAN_CHI);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTienBenhNhanChiIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tienBenhNhanChi not equals to DEFAULT_TIEN_BENH_NHAN_CHI
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tienBenhNhanChi.notEquals=" + DEFAULT_TIEN_BENH_NHAN_CHI);

        // Get all the thuThuatPhauThuatApDungList where tienBenhNhanChi not equals to UPDATED_TIEN_BENH_NHAN_CHI
        defaultThuThuatPhauThuatApDungShouldBeFound("tienBenhNhanChi.notEquals=" + UPDATED_TIEN_BENH_NHAN_CHI);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTienBenhNhanChiIsInShouldWork() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tienBenhNhanChi in DEFAULT_TIEN_BENH_NHAN_CHI or UPDATED_TIEN_BENH_NHAN_CHI
        defaultThuThuatPhauThuatApDungShouldBeFound("tienBenhNhanChi.in=" + DEFAULT_TIEN_BENH_NHAN_CHI + "," + UPDATED_TIEN_BENH_NHAN_CHI);

        // Get all the thuThuatPhauThuatApDungList where tienBenhNhanChi equals to UPDATED_TIEN_BENH_NHAN_CHI
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tienBenhNhanChi.in=" + UPDATED_TIEN_BENH_NHAN_CHI);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTienBenhNhanChiIsNullOrNotNull() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tienBenhNhanChi is not null
        defaultThuThuatPhauThuatApDungShouldBeFound("tienBenhNhanChi.specified=true");

        // Get all the thuThuatPhauThuatApDungList where tienBenhNhanChi is null
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tienBenhNhanChi.specified=false");
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTienBenhNhanChiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tienBenhNhanChi is greater than or equal to DEFAULT_TIEN_BENH_NHAN_CHI
        defaultThuThuatPhauThuatApDungShouldBeFound("tienBenhNhanChi.greaterThanOrEqual=" + DEFAULT_TIEN_BENH_NHAN_CHI);

        // Get all the thuThuatPhauThuatApDungList where tienBenhNhanChi is greater than or equal to UPDATED_TIEN_BENH_NHAN_CHI
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tienBenhNhanChi.greaterThanOrEqual=" + UPDATED_TIEN_BENH_NHAN_CHI);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTienBenhNhanChiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tienBenhNhanChi is less than or equal to DEFAULT_TIEN_BENH_NHAN_CHI
        defaultThuThuatPhauThuatApDungShouldBeFound("tienBenhNhanChi.lessThanOrEqual=" + DEFAULT_TIEN_BENH_NHAN_CHI);

        // Get all the thuThuatPhauThuatApDungList where tienBenhNhanChi is less than or equal to SMALLER_TIEN_BENH_NHAN_CHI
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tienBenhNhanChi.lessThanOrEqual=" + SMALLER_TIEN_BENH_NHAN_CHI);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTienBenhNhanChiIsLessThanSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tienBenhNhanChi is less than DEFAULT_TIEN_BENH_NHAN_CHI
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tienBenhNhanChi.lessThan=" + DEFAULT_TIEN_BENH_NHAN_CHI);

        // Get all the thuThuatPhauThuatApDungList where tienBenhNhanChi is less than UPDATED_TIEN_BENH_NHAN_CHI
        defaultThuThuatPhauThuatApDungShouldBeFound("tienBenhNhanChi.lessThan=" + UPDATED_TIEN_BENH_NHAN_CHI);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTienBenhNhanChiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tienBenhNhanChi is greater than DEFAULT_TIEN_BENH_NHAN_CHI
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tienBenhNhanChi.greaterThan=" + DEFAULT_TIEN_BENH_NHAN_CHI);

        // Get all the thuThuatPhauThuatApDungList where tienBenhNhanChi is greater than SMALLER_TIEN_BENH_NHAN_CHI
        defaultThuThuatPhauThuatApDungShouldBeFound("tienBenhNhanChi.greaterThan=" + SMALLER_TIEN_BENH_NHAN_CHI);
    }


    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTienBhxhChiIsEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tienBhxhChi equals to DEFAULT_TIEN_BHXH_CHI
        defaultThuThuatPhauThuatApDungShouldBeFound("tienBhxhChi.equals=" + DEFAULT_TIEN_BHXH_CHI);

        // Get all the thuThuatPhauThuatApDungList where tienBhxhChi equals to UPDATED_TIEN_BHXH_CHI
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tienBhxhChi.equals=" + UPDATED_TIEN_BHXH_CHI);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTienBhxhChiIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tienBhxhChi not equals to DEFAULT_TIEN_BHXH_CHI
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tienBhxhChi.notEquals=" + DEFAULT_TIEN_BHXH_CHI);

        // Get all the thuThuatPhauThuatApDungList where tienBhxhChi not equals to UPDATED_TIEN_BHXH_CHI
        defaultThuThuatPhauThuatApDungShouldBeFound("tienBhxhChi.notEquals=" + UPDATED_TIEN_BHXH_CHI);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTienBhxhChiIsInShouldWork() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tienBhxhChi in DEFAULT_TIEN_BHXH_CHI or UPDATED_TIEN_BHXH_CHI
        defaultThuThuatPhauThuatApDungShouldBeFound("tienBhxhChi.in=" + DEFAULT_TIEN_BHXH_CHI + "," + UPDATED_TIEN_BHXH_CHI);

        // Get all the thuThuatPhauThuatApDungList where tienBhxhChi equals to UPDATED_TIEN_BHXH_CHI
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tienBhxhChi.in=" + UPDATED_TIEN_BHXH_CHI);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTienBhxhChiIsNullOrNotNull() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tienBhxhChi is not null
        defaultThuThuatPhauThuatApDungShouldBeFound("tienBhxhChi.specified=true");

        // Get all the thuThuatPhauThuatApDungList where tienBhxhChi is null
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tienBhxhChi.specified=false");
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTienBhxhChiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tienBhxhChi is greater than or equal to DEFAULT_TIEN_BHXH_CHI
        defaultThuThuatPhauThuatApDungShouldBeFound("tienBhxhChi.greaterThanOrEqual=" + DEFAULT_TIEN_BHXH_CHI);

        // Get all the thuThuatPhauThuatApDungList where tienBhxhChi is greater than or equal to UPDATED_TIEN_BHXH_CHI
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tienBhxhChi.greaterThanOrEqual=" + UPDATED_TIEN_BHXH_CHI);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTienBhxhChiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tienBhxhChi is less than or equal to DEFAULT_TIEN_BHXH_CHI
        defaultThuThuatPhauThuatApDungShouldBeFound("tienBhxhChi.lessThanOrEqual=" + DEFAULT_TIEN_BHXH_CHI);

        // Get all the thuThuatPhauThuatApDungList where tienBhxhChi is less than or equal to SMALLER_TIEN_BHXH_CHI
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tienBhxhChi.lessThanOrEqual=" + SMALLER_TIEN_BHXH_CHI);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTienBhxhChiIsLessThanSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tienBhxhChi is less than DEFAULT_TIEN_BHXH_CHI
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tienBhxhChi.lessThan=" + DEFAULT_TIEN_BHXH_CHI);

        // Get all the thuThuatPhauThuatApDungList where tienBhxhChi is less than UPDATED_TIEN_BHXH_CHI
        defaultThuThuatPhauThuatApDungShouldBeFound("tienBhxhChi.lessThan=" + UPDATED_TIEN_BHXH_CHI);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTienBhxhChiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tienBhxhChi is greater than DEFAULT_TIEN_BHXH_CHI
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tienBhxhChi.greaterThan=" + DEFAULT_TIEN_BHXH_CHI);

        // Get all the thuThuatPhauThuatApDungList where tienBhxhChi is greater than SMALLER_TIEN_BHXH_CHI
        defaultThuThuatPhauThuatApDungShouldBeFound("tienBhxhChi.greaterThan=" + SMALLER_TIEN_BHXH_CHI);
    }


    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTienNgoaiBhytIsEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tienNgoaiBhyt equals to DEFAULT_TIEN_NGOAI_BHYT
        defaultThuThuatPhauThuatApDungShouldBeFound("tienNgoaiBhyt.equals=" + DEFAULT_TIEN_NGOAI_BHYT);

        // Get all the thuThuatPhauThuatApDungList where tienNgoaiBhyt equals to UPDATED_TIEN_NGOAI_BHYT
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tienNgoaiBhyt.equals=" + UPDATED_TIEN_NGOAI_BHYT);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTienNgoaiBhytIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tienNgoaiBhyt not equals to DEFAULT_TIEN_NGOAI_BHYT
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tienNgoaiBhyt.notEquals=" + DEFAULT_TIEN_NGOAI_BHYT);

        // Get all the thuThuatPhauThuatApDungList where tienNgoaiBhyt not equals to UPDATED_TIEN_NGOAI_BHYT
        defaultThuThuatPhauThuatApDungShouldBeFound("tienNgoaiBhyt.notEquals=" + UPDATED_TIEN_NGOAI_BHYT);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTienNgoaiBhytIsInShouldWork() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tienNgoaiBhyt in DEFAULT_TIEN_NGOAI_BHYT or UPDATED_TIEN_NGOAI_BHYT
        defaultThuThuatPhauThuatApDungShouldBeFound("tienNgoaiBhyt.in=" + DEFAULT_TIEN_NGOAI_BHYT + "," + UPDATED_TIEN_NGOAI_BHYT);

        // Get all the thuThuatPhauThuatApDungList where tienNgoaiBhyt equals to UPDATED_TIEN_NGOAI_BHYT
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tienNgoaiBhyt.in=" + UPDATED_TIEN_NGOAI_BHYT);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTienNgoaiBhytIsNullOrNotNull() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tienNgoaiBhyt is not null
        defaultThuThuatPhauThuatApDungShouldBeFound("tienNgoaiBhyt.specified=true");

        // Get all the thuThuatPhauThuatApDungList where tienNgoaiBhyt is null
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tienNgoaiBhyt.specified=false");
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTienNgoaiBhytIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tienNgoaiBhyt is greater than or equal to DEFAULT_TIEN_NGOAI_BHYT
        defaultThuThuatPhauThuatApDungShouldBeFound("tienNgoaiBhyt.greaterThanOrEqual=" + DEFAULT_TIEN_NGOAI_BHYT);

        // Get all the thuThuatPhauThuatApDungList where tienNgoaiBhyt is greater than or equal to UPDATED_TIEN_NGOAI_BHYT
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tienNgoaiBhyt.greaterThanOrEqual=" + UPDATED_TIEN_NGOAI_BHYT);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTienNgoaiBhytIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tienNgoaiBhyt is less than or equal to DEFAULT_TIEN_NGOAI_BHYT
        defaultThuThuatPhauThuatApDungShouldBeFound("tienNgoaiBhyt.lessThanOrEqual=" + DEFAULT_TIEN_NGOAI_BHYT);

        // Get all the thuThuatPhauThuatApDungList where tienNgoaiBhyt is less than or equal to SMALLER_TIEN_NGOAI_BHYT
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tienNgoaiBhyt.lessThanOrEqual=" + SMALLER_TIEN_NGOAI_BHYT);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTienNgoaiBhytIsLessThanSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tienNgoaiBhyt is less than DEFAULT_TIEN_NGOAI_BHYT
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tienNgoaiBhyt.lessThan=" + DEFAULT_TIEN_NGOAI_BHYT);

        // Get all the thuThuatPhauThuatApDungList where tienNgoaiBhyt is less than UPDATED_TIEN_NGOAI_BHYT
        defaultThuThuatPhauThuatApDungShouldBeFound("tienNgoaiBhyt.lessThan=" + UPDATED_TIEN_NGOAI_BHYT);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTienNgoaiBhytIsGreaterThanSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tienNgoaiBhyt is greater than DEFAULT_TIEN_NGOAI_BHYT
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tienNgoaiBhyt.greaterThan=" + DEFAULT_TIEN_NGOAI_BHYT);

        // Get all the thuThuatPhauThuatApDungList where tienNgoaiBhyt is greater than SMALLER_TIEN_NGOAI_BHYT
        defaultThuThuatPhauThuatApDungShouldBeFound("tienNgoaiBhyt.greaterThan=" + SMALLER_TIEN_NGOAI_BHYT);
    }


    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTongTienThanhToanIsEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tongTienThanhToan equals to DEFAULT_TONG_TIEN_THANH_TOAN
        defaultThuThuatPhauThuatApDungShouldBeFound("tongTienThanhToan.equals=" + DEFAULT_TONG_TIEN_THANH_TOAN);

        // Get all the thuThuatPhauThuatApDungList where tongTienThanhToan equals to UPDATED_TONG_TIEN_THANH_TOAN
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tongTienThanhToan.equals=" + UPDATED_TONG_TIEN_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTongTienThanhToanIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tongTienThanhToan not equals to DEFAULT_TONG_TIEN_THANH_TOAN
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tongTienThanhToan.notEquals=" + DEFAULT_TONG_TIEN_THANH_TOAN);

        // Get all the thuThuatPhauThuatApDungList where tongTienThanhToan not equals to UPDATED_TONG_TIEN_THANH_TOAN
        defaultThuThuatPhauThuatApDungShouldBeFound("tongTienThanhToan.notEquals=" + UPDATED_TONG_TIEN_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTongTienThanhToanIsInShouldWork() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tongTienThanhToan in DEFAULT_TONG_TIEN_THANH_TOAN or UPDATED_TONG_TIEN_THANH_TOAN
        defaultThuThuatPhauThuatApDungShouldBeFound("tongTienThanhToan.in=" + DEFAULT_TONG_TIEN_THANH_TOAN + "," + UPDATED_TONG_TIEN_THANH_TOAN);

        // Get all the thuThuatPhauThuatApDungList where tongTienThanhToan equals to UPDATED_TONG_TIEN_THANH_TOAN
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tongTienThanhToan.in=" + UPDATED_TONG_TIEN_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTongTienThanhToanIsNullOrNotNull() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tongTienThanhToan is not null
        defaultThuThuatPhauThuatApDungShouldBeFound("tongTienThanhToan.specified=true");

        // Get all the thuThuatPhauThuatApDungList where tongTienThanhToan is null
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tongTienThanhToan.specified=false");
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTongTienThanhToanIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tongTienThanhToan is greater than or equal to DEFAULT_TONG_TIEN_THANH_TOAN
        defaultThuThuatPhauThuatApDungShouldBeFound("tongTienThanhToan.greaterThanOrEqual=" + DEFAULT_TONG_TIEN_THANH_TOAN);

        // Get all the thuThuatPhauThuatApDungList where tongTienThanhToan is greater than or equal to UPDATED_TONG_TIEN_THANH_TOAN
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tongTienThanhToan.greaterThanOrEqual=" + UPDATED_TONG_TIEN_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTongTienThanhToanIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tongTienThanhToan is less than or equal to DEFAULT_TONG_TIEN_THANH_TOAN
        defaultThuThuatPhauThuatApDungShouldBeFound("tongTienThanhToan.lessThanOrEqual=" + DEFAULT_TONG_TIEN_THANH_TOAN);

        // Get all the thuThuatPhauThuatApDungList where tongTienThanhToan is less than or equal to SMALLER_TONG_TIEN_THANH_TOAN
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tongTienThanhToan.lessThanOrEqual=" + SMALLER_TONG_TIEN_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTongTienThanhToanIsLessThanSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tongTienThanhToan is less than DEFAULT_TONG_TIEN_THANH_TOAN
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tongTienThanhToan.lessThan=" + DEFAULT_TONG_TIEN_THANH_TOAN);

        // Get all the thuThuatPhauThuatApDungList where tongTienThanhToan is less than UPDATED_TONG_TIEN_THANH_TOAN
        defaultThuThuatPhauThuatApDungShouldBeFound("tongTienThanhToan.lessThan=" + UPDATED_TONG_TIEN_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTongTienThanhToanIsGreaterThanSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tongTienThanhToan is greater than DEFAULT_TONG_TIEN_THANH_TOAN
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tongTienThanhToan.greaterThan=" + DEFAULT_TONG_TIEN_THANH_TOAN);

        // Get all the thuThuatPhauThuatApDungList where tongTienThanhToan is greater than SMALLER_TONG_TIEN_THANH_TOAN
        defaultThuThuatPhauThuatApDungShouldBeFound("tongTienThanhToan.greaterThan=" + SMALLER_TONG_TIEN_THANH_TOAN);
    }


    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTyLeBhxhThanhToanIsEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tyLeBhxhThanhToan equals to DEFAULT_TY_LE_BHXH_THANH_TOAN
        defaultThuThuatPhauThuatApDungShouldBeFound("tyLeBhxhThanhToan.equals=" + DEFAULT_TY_LE_BHXH_THANH_TOAN);

        // Get all the thuThuatPhauThuatApDungList where tyLeBhxhThanhToan equals to UPDATED_TY_LE_BHXH_THANH_TOAN
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tyLeBhxhThanhToan.equals=" + UPDATED_TY_LE_BHXH_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTyLeBhxhThanhToanIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tyLeBhxhThanhToan not equals to DEFAULT_TY_LE_BHXH_THANH_TOAN
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tyLeBhxhThanhToan.notEquals=" + DEFAULT_TY_LE_BHXH_THANH_TOAN);

        // Get all the thuThuatPhauThuatApDungList where tyLeBhxhThanhToan not equals to UPDATED_TY_LE_BHXH_THANH_TOAN
        defaultThuThuatPhauThuatApDungShouldBeFound("tyLeBhxhThanhToan.notEquals=" + UPDATED_TY_LE_BHXH_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTyLeBhxhThanhToanIsInShouldWork() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tyLeBhxhThanhToan in DEFAULT_TY_LE_BHXH_THANH_TOAN or UPDATED_TY_LE_BHXH_THANH_TOAN
        defaultThuThuatPhauThuatApDungShouldBeFound("tyLeBhxhThanhToan.in=" + DEFAULT_TY_LE_BHXH_THANH_TOAN + "," + UPDATED_TY_LE_BHXH_THANH_TOAN);

        // Get all the thuThuatPhauThuatApDungList where tyLeBhxhThanhToan equals to UPDATED_TY_LE_BHXH_THANH_TOAN
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tyLeBhxhThanhToan.in=" + UPDATED_TY_LE_BHXH_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTyLeBhxhThanhToanIsNullOrNotNull() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tyLeBhxhThanhToan is not null
        defaultThuThuatPhauThuatApDungShouldBeFound("tyLeBhxhThanhToan.specified=true");

        // Get all the thuThuatPhauThuatApDungList where tyLeBhxhThanhToan is null
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tyLeBhxhThanhToan.specified=false");
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTyLeBhxhThanhToanIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tyLeBhxhThanhToan is greater than or equal to DEFAULT_TY_LE_BHXH_THANH_TOAN
        defaultThuThuatPhauThuatApDungShouldBeFound("tyLeBhxhThanhToan.greaterThanOrEqual=" + DEFAULT_TY_LE_BHXH_THANH_TOAN);

        // Get all the thuThuatPhauThuatApDungList where tyLeBhxhThanhToan is greater than or equal to UPDATED_TY_LE_BHXH_THANH_TOAN
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tyLeBhxhThanhToan.greaterThanOrEqual=" + UPDATED_TY_LE_BHXH_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTyLeBhxhThanhToanIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tyLeBhxhThanhToan is less than or equal to DEFAULT_TY_LE_BHXH_THANH_TOAN
        defaultThuThuatPhauThuatApDungShouldBeFound("tyLeBhxhThanhToan.lessThanOrEqual=" + DEFAULT_TY_LE_BHXH_THANH_TOAN);

        // Get all the thuThuatPhauThuatApDungList where tyLeBhxhThanhToan is less than or equal to SMALLER_TY_LE_BHXH_THANH_TOAN
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tyLeBhxhThanhToan.lessThanOrEqual=" + SMALLER_TY_LE_BHXH_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTyLeBhxhThanhToanIsLessThanSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tyLeBhxhThanhToan is less than DEFAULT_TY_LE_BHXH_THANH_TOAN
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tyLeBhxhThanhToan.lessThan=" + DEFAULT_TY_LE_BHXH_THANH_TOAN);

        // Get all the thuThuatPhauThuatApDungList where tyLeBhxhThanhToan is less than UPDATED_TY_LE_BHXH_THANH_TOAN
        defaultThuThuatPhauThuatApDungShouldBeFound("tyLeBhxhThanhToan.lessThan=" + UPDATED_TY_LE_BHXH_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTyLeBhxhThanhToanIsGreaterThanSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where tyLeBhxhThanhToan is greater than DEFAULT_TY_LE_BHXH_THANH_TOAN
        defaultThuThuatPhauThuatApDungShouldNotBeFound("tyLeBhxhThanhToan.greaterThan=" + DEFAULT_TY_LE_BHXH_THANH_TOAN);

        // Get all the thuThuatPhauThuatApDungList where tyLeBhxhThanhToan is greater than SMALLER_TY_LE_BHXH_THANH_TOAN
        defaultThuThuatPhauThuatApDungShouldBeFound("tyLeBhxhThanhToan.greaterThan=" + SMALLER_TY_LE_BHXH_THANH_TOAN);
    }


    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByGiaBhytIsEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where giaBhyt equals to DEFAULT_GIA_BHYT
        defaultThuThuatPhauThuatApDungShouldBeFound("giaBhyt.equals=" + DEFAULT_GIA_BHYT);

        // Get all the thuThuatPhauThuatApDungList where giaBhyt equals to UPDATED_GIA_BHYT
        defaultThuThuatPhauThuatApDungShouldNotBeFound("giaBhyt.equals=" + UPDATED_GIA_BHYT);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByGiaBhytIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where giaBhyt not equals to DEFAULT_GIA_BHYT
        defaultThuThuatPhauThuatApDungShouldNotBeFound("giaBhyt.notEquals=" + DEFAULT_GIA_BHYT);

        // Get all the thuThuatPhauThuatApDungList where giaBhyt not equals to UPDATED_GIA_BHYT
        defaultThuThuatPhauThuatApDungShouldBeFound("giaBhyt.notEquals=" + UPDATED_GIA_BHYT);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByGiaBhytIsInShouldWork() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where giaBhyt in DEFAULT_GIA_BHYT or UPDATED_GIA_BHYT
        defaultThuThuatPhauThuatApDungShouldBeFound("giaBhyt.in=" + DEFAULT_GIA_BHYT + "," + UPDATED_GIA_BHYT);

        // Get all the thuThuatPhauThuatApDungList where giaBhyt equals to UPDATED_GIA_BHYT
        defaultThuThuatPhauThuatApDungShouldNotBeFound("giaBhyt.in=" + UPDATED_GIA_BHYT);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByGiaBhytIsNullOrNotNull() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where giaBhyt is not null
        defaultThuThuatPhauThuatApDungShouldBeFound("giaBhyt.specified=true");

        // Get all the thuThuatPhauThuatApDungList where giaBhyt is null
        defaultThuThuatPhauThuatApDungShouldNotBeFound("giaBhyt.specified=false");
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByGiaBhytIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where giaBhyt is greater than or equal to DEFAULT_GIA_BHYT
        defaultThuThuatPhauThuatApDungShouldBeFound("giaBhyt.greaterThanOrEqual=" + DEFAULT_GIA_BHYT);

        // Get all the thuThuatPhauThuatApDungList where giaBhyt is greater than or equal to UPDATED_GIA_BHYT
        defaultThuThuatPhauThuatApDungShouldNotBeFound("giaBhyt.greaterThanOrEqual=" + UPDATED_GIA_BHYT);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByGiaBhytIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where giaBhyt is less than or equal to DEFAULT_GIA_BHYT
        defaultThuThuatPhauThuatApDungShouldBeFound("giaBhyt.lessThanOrEqual=" + DEFAULT_GIA_BHYT);

        // Get all the thuThuatPhauThuatApDungList where giaBhyt is less than or equal to SMALLER_GIA_BHYT
        defaultThuThuatPhauThuatApDungShouldNotBeFound("giaBhyt.lessThanOrEqual=" + SMALLER_GIA_BHYT);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByGiaBhytIsLessThanSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where giaBhyt is less than DEFAULT_GIA_BHYT
        defaultThuThuatPhauThuatApDungShouldNotBeFound("giaBhyt.lessThan=" + DEFAULT_GIA_BHYT);

        // Get all the thuThuatPhauThuatApDungList where giaBhyt is less than UPDATED_GIA_BHYT
        defaultThuThuatPhauThuatApDungShouldBeFound("giaBhyt.lessThan=" + UPDATED_GIA_BHYT);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByGiaBhytIsGreaterThanSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where giaBhyt is greater than DEFAULT_GIA_BHYT
        defaultThuThuatPhauThuatApDungShouldNotBeFound("giaBhyt.greaterThan=" + DEFAULT_GIA_BHYT);

        // Get all the thuThuatPhauThuatApDungList where giaBhyt is greater than SMALLER_GIA_BHYT
        defaultThuThuatPhauThuatApDungShouldBeFound("giaBhyt.greaterThan=" + SMALLER_GIA_BHYT);
    }


    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByGiaKhongBhytIsEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where giaKhongBhyt equals to DEFAULT_GIA_KHONG_BHYT
        defaultThuThuatPhauThuatApDungShouldBeFound("giaKhongBhyt.equals=" + DEFAULT_GIA_KHONG_BHYT);

        // Get all the thuThuatPhauThuatApDungList where giaKhongBhyt equals to UPDATED_GIA_KHONG_BHYT
        defaultThuThuatPhauThuatApDungShouldNotBeFound("giaKhongBhyt.equals=" + UPDATED_GIA_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByGiaKhongBhytIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where giaKhongBhyt not equals to DEFAULT_GIA_KHONG_BHYT
        defaultThuThuatPhauThuatApDungShouldNotBeFound("giaKhongBhyt.notEquals=" + DEFAULT_GIA_KHONG_BHYT);

        // Get all the thuThuatPhauThuatApDungList where giaKhongBhyt not equals to UPDATED_GIA_KHONG_BHYT
        defaultThuThuatPhauThuatApDungShouldBeFound("giaKhongBhyt.notEquals=" + UPDATED_GIA_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByGiaKhongBhytIsInShouldWork() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where giaKhongBhyt in DEFAULT_GIA_KHONG_BHYT or UPDATED_GIA_KHONG_BHYT
        defaultThuThuatPhauThuatApDungShouldBeFound("giaKhongBhyt.in=" + DEFAULT_GIA_KHONG_BHYT + "," + UPDATED_GIA_KHONG_BHYT);

        // Get all the thuThuatPhauThuatApDungList where giaKhongBhyt equals to UPDATED_GIA_KHONG_BHYT
        defaultThuThuatPhauThuatApDungShouldNotBeFound("giaKhongBhyt.in=" + UPDATED_GIA_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByGiaKhongBhytIsNullOrNotNull() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where giaKhongBhyt is not null
        defaultThuThuatPhauThuatApDungShouldBeFound("giaKhongBhyt.specified=true");

        // Get all the thuThuatPhauThuatApDungList where giaKhongBhyt is null
        defaultThuThuatPhauThuatApDungShouldNotBeFound("giaKhongBhyt.specified=false");
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByGiaKhongBhytIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where giaKhongBhyt is greater than or equal to DEFAULT_GIA_KHONG_BHYT
        defaultThuThuatPhauThuatApDungShouldBeFound("giaKhongBhyt.greaterThanOrEqual=" + DEFAULT_GIA_KHONG_BHYT);

        // Get all the thuThuatPhauThuatApDungList where giaKhongBhyt is greater than or equal to UPDATED_GIA_KHONG_BHYT
        defaultThuThuatPhauThuatApDungShouldNotBeFound("giaKhongBhyt.greaterThanOrEqual=" + UPDATED_GIA_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByGiaKhongBhytIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where giaKhongBhyt is less than or equal to DEFAULT_GIA_KHONG_BHYT
        defaultThuThuatPhauThuatApDungShouldBeFound("giaKhongBhyt.lessThanOrEqual=" + DEFAULT_GIA_KHONG_BHYT);

        // Get all the thuThuatPhauThuatApDungList where giaKhongBhyt is less than or equal to SMALLER_GIA_KHONG_BHYT
        defaultThuThuatPhauThuatApDungShouldNotBeFound("giaKhongBhyt.lessThanOrEqual=" + SMALLER_GIA_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByGiaKhongBhytIsLessThanSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where giaKhongBhyt is less than DEFAULT_GIA_KHONG_BHYT
        defaultThuThuatPhauThuatApDungShouldNotBeFound("giaKhongBhyt.lessThan=" + DEFAULT_GIA_KHONG_BHYT);

        // Get all the thuThuatPhauThuatApDungList where giaKhongBhyt is less than UPDATED_GIA_KHONG_BHYT
        defaultThuThuatPhauThuatApDungShouldBeFound("giaKhongBhyt.lessThan=" + UPDATED_GIA_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByGiaKhongBhytIsGreaterThanSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where giaKhongBhyt is greater than DEFAULT_GIA_KHONG_BHYT
        defaultThuThuatPhauThuatApDungShouldNotBeFound("giaKhongBhyt.greaterThan=" + DEFAULT_GIA_KHONG_BHYT);

        // Get all the thuThuatPhauThuatApDungList where giaKhongBhyt is greater than SMALLER_GIA_KHONG_BHYT
        defaultThuThuatPhauThuatApDungShouldBeFound("giaKhongBhyt.greaterThan=" + SMALLER_GIA_KHONG_BHYT);
    }


    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByDoiTuongDacBietIsEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where doiTuongDacBiet equals to DEFAULT_DOI_TUONG_DAC_BIET
        defaultThuThuatPhauThuatApDungShouldBeFound("doiTuongDacBiet.equals=" + DEFAULT_DOI_TUONG_DAC_BIET);

        // Get all the thuThuatPhauThuatApDungList where doiTuongDacBiet equals to UPDATED_DOI_TUONG_DAC_BIET
        defaultThuThuatPhauThuatApDungShouldNotBeFound("doiTuongDacBiet.equals=" + UPDATED_DOI_TUONG_DAC_BIET);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByDoiTuongDacBietIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where doiTuongDacBiet not equals to DEFAULT_DOI_TUONG_DAC_BIET
        defaultThuThuatPhauThuatApDungShouldNotBeFound("doiTuongDacBiet.notEquals=" + DEFAULT_DOI_TUONG_DAC_BIET);

        // Get all the thuThuatPhauThuatApDungList where doiTuongDacBiet not equals to UPDATED_DOI_TUONG_DAC_BIET
        defaultThuThuatPhauThuatApDungShouldBeFound("doiTuongDacBiet.notEquals=" + UPDATED_DOI_TUONG_DAC_BIET);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByDoiTuongDacBietIsInShouldWork() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where doiTuongDacBiet in DEFAULT_DOI_TUONG_DAC_BIET or UPDATED_DOI_TUONG_DAC_BIET
        defaultThuThuatPhauThuatApDungShouldBeFound("doiTuongDacBiet.in=" + DEFAULT_DOI_TUONG_DAC_BIET + "," + UPDATED_DOI_TUONG_DAC_BIET);

        // Get all the thuThuatPhauThuatApDungList where doiTuongDacBiet equals to UPDATED_DOI_TUONG_DAC_BIET
        defaultThuThuatPhauThuatApDungShouldNotBeFound("doiTuongDacBiet.in=" + UPDATED_DOI_TUONG_DAC_BIET);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByDoiTuongDacBietIsNullOrNotNull() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where doiTuongDacBiet is not null
        defaultThuThuatPhauThuatApDungShouldBeFound("doiTuongDacBiet.specified=true");

        // Get all the thuThuatPhauThuatApDungList where doiTuongDacBiet is null
        defaultThuThuatPhauThuatApDungShouldNotBeFound("doiTuongDacBiet.specified=false");
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByNguonChiIsEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where nguonChi equals to DEFAULT_NGUON_CHI
        defaultThuThuatPhauThuatApDungShouldBeFound("nguonChi.equals=" + DEFAULT_NGUON_CHI);

        // Get all the thuThuatPhauThuatApDungList where nguonChi equals to UPDATED_NGUON_CHI
        defaultThuThuatPhauThuatApDungShouldNotBeFound("nguonChi.equals=" + UPDATED_NGUON_CHI);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByNguonChiIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where nguonChi not equals to DEFAULT_NGUON_CHI
        defaultThuThuatPhauThuatApDungShouldNotBeFound("nguonChi.notEquals=" + DEFAULT_NGUON_CHI);

        // Get all the thuThuatPhauThuatApDungList where nguonChi not equals to UPDATED_NGUON_CHI
        defaultThuThuatPhauThuatApDungShouldBeFound("nguonChi.notEquals=" + UPDATED_NGUON_CHI);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByNguonChiIsInShouldWork() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where nguonChi in DEFAULT_NGUON_CHI or UPDATED_NGUON_CHI
        defaultThuThuatPhauThuatApDungShouldBeFound("nguonChi.in=" + DEFAULT_NGUON_CHI + "," + UPDATED_NGUON_CHI);

        // Get all the thuThuatPhauThuatApDungList where nguonChi equals to UPDATED_NGUON_CHI
        defaultThuThuatPhauThuatApDungShouldNotBeFound("nguonChi.in=" + UPDATED_NGUON_CHI);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByNguonChiIsNullOrNotNull() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where nguonChi is not null
        defaultThuThuatPhauThuatApDungShouldBeFound("nguonChi.specified=true");

        // Get all the thuThuatPhauThuatApDungList where nguonChi is null
        defaultThuThuatPhauThuatApDungShouldNotBeFound("nguonChi.specified=false");
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByNguonChiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where nguonChi is greater than or equal to DEFAULT_NGUON_CHI
        defaultThuThuatPhauThuatApDungShouldBeFound("nguonChi.greaterThanOrEqual=" + DEFAULT_NGUON_CHI);

        // Get all the thuThuatPhauThuatApDungList where nguonChi is greater than or equal to UPDATED_NGUON_CHI
        defaultThuThuatPhauThuatApDungShouldNotBeFound("nguonChi.greaterThanOrEqual=" + UPDATED_NGUON_CHI);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByNguonChiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where nguonChi is less than or equal to DEFAULT_NGUON_CHI
        defaultThuThuatPhauThuatApDungShouldBeFound("nguonChi.lessThanOrEqual=" + DEFAULT_NGUON_CHI);

        // Get all the thuThuatPhauThuatApDungList where nguonChi is less than or equal to SMALLER_NGUON_CHI
        defaultThuThuatPhauThuatApDungShouldNotBeFound("nguonChi.lessThanOrEqual=" + SMALLER_NGUON_CHI);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByNguonChiIsLessThanSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where nguonChi is less than DEFAULT_NGUON_CHI
        defaultThuThuatPhauThuatApDungShouldNotBeFound("nguonChi.lessThan=" + DEFAULT_NGUON_CHI);

        // Get all the thuThuatPhauThuatApDungList where nguonChi is less than UPDATED_NGUON_CHI
        defaultThuThuatPhauThuatApDungShouldBeFound("nguonChi.lessThan=" + UPDATED_NGUON_CHI);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByNguonChiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where nguonChi is greater than DEFAULT_NGUON_CHI
        defaultThuThuatPhauThuatApDungShouldNotBeFound("nguonChi.greaterThan=" + DEFAULT_NGUON_CHI);

        // Get all the thuThuatPhauThuatApDungList where nguonChi is greater than SMALLER_NGUON_CHI
        defaultThuThuatPhauThuatApDungShouldBeFound("nguonChi.greaterThan=" + SMALLER_NGUON_CHI);
    }


    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByEnableIsEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where enable equals to DEFAULT_ENABLE
        defaultThuThuatPhauThuatApDungShouldBeFound("enable.equals=" + DEFAULT_ENABLE);

        // Get all the thuThuatPhauThuatApDungList where enable equals to UPDATED_ENABLE
        defaultThuThuatPhauThuatApDungShouldNotBeFound("enable.equals=" + UPDATED_ENABLE);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByEnableIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where enable not equals to DEFAULT_ENABLE
        defaultThuThuatPhauThuatApDungShouldNotBeFound("enable.notEquals=" + DEFAULT_ENABLE);

        // Get all the thuThuatPhauThuatApDungList where enable not equals to UPDATED_ENABLE
        defaultThuThuatPhauThuatApDungShouldBeFound("enable.notEquals=" + UPDATED_ENABLE);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByEnableIsInShouldWork() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where enable in DEFAULT_ENABLE or UPDATED_ENABLE
        defaultThuThuatPhauThuatApDungShouldBeFound("enable.in=" + DEFAULT_ENABLE + "," + UPDATED_ENABLE);

        // Get all the thuThuatPhauThuatApDungList where enable equals to UPDATED_ENABLE
        defaultThuThuatPhauThuatApDungShouldNotBeFound("enable.in=" + UPDATED_ENABLE);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByEnableIsNullOrNotNull() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        // Get all the thuThuatPhauThuatApDungList where enable is not null
        defaultThuThuatPhauThuatApDungShouldBeFound("enable.specified=true");

        // Get all the thuThuatPhauThuatApDungList where enable is null
        defaultThuThuatPhauThuatApDungShouldNotBeFound("enable.specified=false");
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByDotGiaIsEqualToSomething() throws Exception {
        // Get already existing entity
        DotGiaDichVuBhxh dotGia = thuThuatPhauThuatApDung.getDotGia();
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);
        Long dotGiaId = dotGia.getId();

        // Get all the thuThuatPhauThuatApDungList where dotGia equals to dotGiaId
        defaultThuThuatPhauThuatApDungShouldBeFound("dotGiaId.equals=" + dotGiaId);

        // Get all the thuThuatPhauThuatApDungList where dotGia equals to dotGiaId + 1
        defaultThuThuatPhauThuatApDungShouldNotBeFound("dotGiaId.equals=" + (dotGiaId + 1));
    }


    @Test
    @Transactional
    public void getAllThuThuatPhauThuatApDungsByTtptIsEqualToSomething() throws Exception {
        // Get already existing entity
        ThuThuatPhauThuat ttpt = thuThuatPhauThuatApDung.getTtpt();
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);
        Long ttptId = ttpt.getId();

        // Get all the thuThuatPhauThuatApDungList where ttpt equals to ttptId
        defaultThuThuatPhauThuatApDungShouldBeFound("ttptId.equals=" + ttptId);

        // Get all the thuThuatPhauThuatApDungList where ttpt equals to ttptId + 1
        defaultThuThuatPhauThuatApDungShouldNotBeFound("ttptId.equals=" + (ttptId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultThuThuatPhauThuatApDungShouldBeFound(String filter) throws Exception {
        restThuThuatPhauThuatApDungMockMvc.perform(get("/api/thu-thuat-phau-thuat-ap-dungs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(thuThuatPhauThuatApDung.getId().intValue())))
            .andExpect(jsonPath("$.[*].kyThuatCao").value(hasItem(DEFAULT_KY_THUAT_CAO)))
            .andExpect(jsonPath("$.[*].maBaoCaoBhxh").value(hasItem(DEFAULT_MA_BAO_CAO_BHXH)))
            .andExpect(jsonPath("$.[*].maBaoCaoBhyt").value(hasItem(DEFAULT_MA_BAO_CAO_BHYT)))
            .andExpect(jsonPath("$.[*].ngayApDung").value(hasItem(DEFAULT_NGAY_AP_DUNG.toString())))
            .andExpect(jsonPath("$.[*].soCongVanBhxh").value(hasItem(DEFAULT_SO_CONG_VAN_BHXH)))
            .andExpect(jsonPath("$.[*].soQuyetDinh").value(hasItem(DEFAULT_SO_QUYET_DINH)))
            .andExpect(jsonPath("$.[*].tenBaoCaoBhxh").value(hasItem(DEFAULT_TEN_BAO_CAO_BHXH)))
            .andExpect(jsonPath("$.[*].tenDichVuKhongBhyt").value(hasItem(DEFAULT_TEN_DICH_VU_KHONG_BHYT)))
            .andExpect(jsonPath("$.[*].tienBenhNhanChi").value(hasItem(DEFAULT_TIEN_BENH_NHAN_CHI.intValue())))
            .andExpect(jsonPath("$.[*].tienBhxhChi").value(hasItem(DEFAULT_TIEN_BHXH_CHI.intValue())))
            .andExpect(jsonPath("$.[*].tienNgoaiBhyt").value(hasItem(DEFAULT_TIEN_NGOAI_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].tongTienThanhToan").value(hasItem(DEFAULT_TONG_TIEN_THANH_TOAN.intValue())))
            .andExpect(jsonPath("$.[*].tyLeBhxhThanhToan").value(hasItem(DEFAULT_TY_LE_BHXH_THANH_TOAN)))
            .andExpect(jsonPath("$.[*].giaBhyt").value(hasItem(DEFAULT_GIA_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].giaKhongBhyt").value(hasItem(DEFAULT_GIA_KHONG_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].doiTuongDacBiet").value(hasItem(DEFAULT_DOI_TUONG_DAC_BIET.booleanValue())))
            .andExpect(jsonPath("$.[*].nguonChi").value(hasItem(DEFAULT_NGUON_CHI)))
            .andExpect(jsonPath("$.[*].enable").value(hasItem(DEFAULT_ENABLE.booleanValue())));

        // Check, that the count call also returns 1
        restThuThuatPhauThuatApDungMockMvc.perform(get("/api/thu-thuat-phau-thuat-ap-dungs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultThuThuatPhauThuatApDungShouldNotBeFound(String filter) throws Exception {
        restThuThuatPhauThuatApDungMockMvc.perform(get("/api/thu-thuat-phau-thuat-ap-dungs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restThuThuatPhauThuatApDungMockMvc.perform(get("/api/thu-thuat-phau-thuat-ap-dungs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingThuThuatPhauThuatApDung() throws Exception {
        // Get the thuThuatPhauThuatApDung
        restThuThuatPhauThuatApDungMockMvc.perform(get("/api/thu-thuat-phau-thuat-ap-dungs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateThuThuatPhauThuatApDung() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        int databaseSizeBeforeUpdate = thuThuatPhauThuatApDungRepository.findAll().size();

        // Update the thuThuatPhauThuatApDung
        ThuThuatPhauThuatApDung updatedThuThuatPhauThuatApDung = thuThuatPhauThuatApDungRepository.findById(thuThuatPhauThuatApDung.getId()).get();
        // Disconnect from session so that the updates on updatedThuThuatPhauThuatApDung are not directly saved in db
        em.detach(updatedThuThuatPhauThuatApDung);
        updatedThuThuatPhauThuatApDung
            .kyThuatCao(UPDATED_KY_THUAT_CAO)
            .maBaoCaoBhxh(UPDATED_MA_BAO_CAO_BHXH)
            .maBaoCaoBhyt(UPDATED_MA_BAO_CAO_BHYT)
            .ngayApDung(UPDATED_NGAY_AP_DUNG)
            .soCongVanBhxh(UPDATED_SO_CONG_VAN_BHXH)
            .soQuyetDinh(UPDATED_SO_QUYET_DINH)
            .tenBaoCaoBhxh(UPDATED_TEN_BAO_CAO_BHXH)
            .tenDichVuKhongBhyt(UPDATED_TEN_DICH_VU_KHONG_BHYT)
            .tienBenhNhanChi(UPDATED_TIEN_BENH_NHAN_CHI)
            .tienBhxhChi(UPDATED_TIEN_BHXH_CHI)
            .tienNgoaiBhyt(UPDATED_TIEN_NGOAI_BHYT)
            .tongTienThanhToan(UPDATED_TONG_TIEN_THANH_TOAN)
            .tyLeBhxhThanhToan(UPDATED_TY_LE_BHXH_THANH_TOAN)
            .giaBhyt(UPDATED_GIA_BHYT)
            .giaKhongBhyt(UPDATED_GIA_KHONG_BHYT)
            .doiTuongDacBiet(UPDATED_DOI_TUONG_DAC_BIET)
            .nguonChi(UPDATED_NGUON_CHI)
            .enable(UPDATED_ENABLE);
        ThuThuatPhauThuatApDungDTO thuThuatPhauThuatApDungDTO = thuThuatPhauThuatApDungMapper.toDto(updatedThuThuatPhauThuatApDung);

        restThuThuatPhauThuatApDungMockMvc.perform(put("/api/thu-thuat-phau-thuat-ap-dungs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(thuThuatPhauThuatApDungDTO)))
            .andExpect(status().isOk());

        // Validate the ThuThuatPhauThuatApDung in the database
        List<ThuThuatPhauThuatApDung> thuThuatPhauThuatApDungList = thuThuatPhauThuatApDungRepository.findAll();
        assertThat(thuThuatPhauThuatApDungList).hasSize(databaseSizeBeforeUpdate);
        ThuThuatPhauThuatApDung testThuThuatPhauThuatApDung = thuThuatPhauThuatApDungList.get(thuThuatPhauThuatApDungList.size() - 1);
        assertThat(testThuThuatPhauThuatApDung.getKyThuatCao()).isEqualTo(UPDATED_KY_THUAT_CAO);
        assertThat(testThuThuatPhauThuatApDung.getMaBaoCaoBhxh()).isEqualTo(UPDATED_MA_BAO_CAO_BHXH);
        assertThat(testThuThuatPhauThuatApDung.getMaBaoCaoBhyt()).isEqualTo(UPDATED_MA_BAO_CAO_BHYT);
        assertThat(testThuThuatPhauThuatApDung.getNgayApDung()).isEqualTo(UPDATED_NGAY_AP_DUNG);
        assertThat(testThuThuatPhauThuatApDung.getSoCongVanBhxh()).isEqualTo(UPDATED_SO_CONG_VAN_BHXH);
        assertThat(testThuThuatPhauThuatApDung.getSoQuyetDinh()).isEqualTo(UPDATED_SO_QUYET_DINH);
        assertThat(testThuThuatPhauThuatApDung.getTenBaoCaoBhxh()).isEqualTo(UPDATED_TEN_BAO_CAO_BHXH);
        assertThat(testThuThuatPhauThuatApDung.getTenDichVuKhongBhyt()).isEqualTo(UPDATED_TEN_DICH_VU_KHONG_BHYT);
        assertThat(testThuThuatPhauThuatApDung.getTienBenhNhanChi()).isEqualTo(UPDATED_TIEN_BENH_NHAN_CHI);
        assertThat(testThuThuatPhauThuatApDung.getTienBhxhChi()).isEqualTo(UPDATED_TIEN_BHXH_CHI);
        assertThat(testThuThuatPhauThuatApDung.getTienNgoaiBhyt()).isEqualTo(UPDATED_TIEN_NGOAI_BHYT);
        assertThat(testThuThuatPhauThuatApDung.getTongTienThanhToan()).isEqualTo(UPDATED_TONG_TIEN_THANH_TOAN);
        assertThat(testThuThuatPhauThuatApDung.getTyLeBhxhThanhToan()).isEqualTo(UPDATED_TY_LE_BHXH_THANH_TOAN);
        assertThat(testThuThuatPhauThuatApDung.getGiaBhyt()).isEqualTo(UPDATED_GIA_BHYT);
        assertThat(testThuThuatPhauThuatApDung.getGiaKhongBhyt()).isEqualTo(UPDATED_GIA_KHONG_BHYT);
        assertThat(testThuThuatPhauThuatApDung.isDoiTuongDacBiet()).isEqualTo(UPDATED_DOI_TUONG_DAC_BIET);
        assertThat(testThuThuatPhauThuatApDung.getNguonChi()).isEqualTo(UPDATED_NGUON_CHI);
        assertThat(testThuThuatPhauThuatApDung.isEnable()).isEqualTo(UPDATED_ENABLE);
    }

    @Test
    @Transactional
    public void updateNonExistingThuThuatPhauThuatApDung() throws Exception {
        int databaseSizeBeforeUpdate = thuThuatPhauThuatApDungRepository.findAll().size();

        // Create the ThuThuatPhauThuatApDung
        ThuThuatPhauThuatApDungDTO thuThuatPhauThuatApDungDTO = thuThuatPhauThuatApDungMapper.toDto(thuThuatPhauThuatApDung);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restThuThuatPhauThuatApDungMockMvc.perform(put("/api/thu-thuat-phau-thuat-ap-dungs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(thuThuatPhauThuatApDungDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ThuThuatPhauThuatApDung in the database
        List<ThuThuatPhauThuatApDung> thuThuatPhauThuatApDungList = thuThuatPhauThuatApDungRepository.findAll();
        assertThat(thuThuatPhauThuatApDungList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteThuThuatPhauThuatApDung() throws Exception {
        // Initialize the database
        thuThuatPhauThuatApDungRepository.saveAndFlush(thuThuatPhauThuatApDung);

        int databaseSizeBeforeDelete = thuThuatPhauThuatApDungRepository.findAll().size();

        // Delete the thuThuatPhauThuatApDung
        restThuThuatPhauThuatApDungMockMvc.perform(delete("/api/thu-thuat-phau-thuat-ap-dungs/{id}", thuThuatPhauThuatApDung.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ThuThuatPhauThuatApDung> thuThuatPhauThuatApDungList = thuThuatPhauThuatApDungRepository.findAll();
        assertThat(thuThuatPhauThuatApDungList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

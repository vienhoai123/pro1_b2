package vn.vnpt.web.rest;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.DonVi;
import vn.vnpt.domain.DotGiaDichVuBhxh;
import vn.vnpt.repository.DotGiaDichVuBhxhRepository;
import vn.vnpt.service.DotGiaDichVuBhxhQueryService;
import vn.vnpt.service.DotGiaDichVuBhxhService;
import vn.vnpt.service.dto.DotGiaDichVuBhxhDTO;
import vn.vnpt.service.mapper.DotGiaDichVuBhxhMapper;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link DotGiaDichVuBhxhResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class DotGiaDichVuBhxhResourceIT {

    private static final BigDecimal DEFAULT_DOI_TUONG_AP_DUNG = new BigDecimal(1);
    private static final BigDecimal UPDATED_DOI_TUONG_AP_DUNG = new BigDecimal(2);
    private static final BigDecimal SMALLER_DOI_TUONG_AP_DUNG = new BigDecimal(1 - 1);

    private static final String DEFAULT_GHI_CHU = "AAAAAAAAAA";
    private static final String UPDATED_GHI_CHU = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_NGAY_AP_DUNG = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_AP_DUNG = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_NGAY_AP_DUNG = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_TEN = "AAAAAAAAAA";
    private static final String UPDATED_TEN = "BBBBBBBBBB";

    @Autowired
    private DotGiaDichVuBhxhRepository dotGiaDichVuBhxhRepository;

    @Autowired
    private DotGiaDichVuBhxhMapper dotGiaDichVuBhxhMapper;

    @Autowired
    private DotGiaDichVuBhxhService dotGiaDichVuBhxhService;

    @Autowired
    private DotGiaDichVuBhxhQueryService dotGiaDichVuBhxhQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDotGiaDichVuBhxhMockMvc;

    private DotGiaDichVuBhxh dotGiaDichVuBhxh;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DotGiaDichVuBhxh createEntity(EntityManager em) {
        DotGiaDichVuBhxh dotGiaDichVuBhxh = new DotGiaDichVuBhxh()
            .doiTuongApDung(DEFAULT_DOI_TUONG_AP_DUNG)
            .ghiChu(DEFAULT_GHI_CHU)
            .ngayApDung(DEFAULT_NGAY_AP_DUNG)
            .ten(DEFAULT_TEN);
        // Add required entity
        DonVi donVi;
        if (TestUtil.findAll(em, DonVi.class).isEmpty()) {
            donVi = DonViResourceIT.createEntity(em);
            em.persist(donVi);
            em.flush();
        } else {
            donVi = TestUtil.findAll(em, DonVi.class).get(0);
        }
        dotGiaDichVuBhxh.setDonVi(donVi);
        return dotGiaDichVuBhxh;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DotGiaDichVuBhxh createUpdatedEntity(EntityManager em) {
        DotGiaDichVuBhxh dotGiaDichVuBhxh = new DotGiaDichVuBhxh()
            .doiTuongApDung(UPDATED_DOI_TUONG_AP_DUNG)
            .ghiChu(UPDATED_GHI_CHU)
            .ngayApDung(UPDATED_NGAY_AP_DUNG)
            .ten(UPDATED_TEN);
        // Add required entity
        DonVi donVi;
        if (TestUtil.findAll(em, DonVi.class).isEmpty()) {
            donVi = DonViResourceIT.createUpdatedEntity(em);
            em.persist(donVi);
            em.flush();
        } else {
            donVi = TestUtil.findAll(em, DonVi.class).get(0);
        }
        dotGiaDichVuBhxh.setDonVi(donVi);
        return dotGiaDichVuBhxh;
    }

    @BeforeEach
    public void initTest() {
        dotGiaDichVuBhxh = createEntity(em);
    }

    @Test
    @Transactional
    public void createDotGiaDichVuBhxh() throws Exception {
        int databaseSizeBeforeCreate = dotGiaDichVuBhxhRepository.findAll().size();

        // Create the DotGiaDichVuBhxh
        DotGiaDichVuBhxhDTO dotGiaDichVuBhxhDTO = dotGiaDichVuBhxhMapper.toDto(dotGiaDichVuBhxh);
        restDotGiaDichVuBhxhMockMvc.perform(post("/api/dot-gia-dich-vu-bhxhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dotGiaDichVuBhxhDTO)))
            .andExpect(status().isCreated());

        // Validate the DotGiaDichVuBhxh in the database
        List<DotGiaDichVuBhxh> dotGiaDichVuBhxhList = dotGiaDichVuBhxhRepository.findAll();
        assertThat(dotGiaDichVuBhxhList).hasSize(databaseSizeBeforeCreate + 1);
        DotGiaDichVuBhxh testDotGiaDichVuBhxh = dotGiaDichVuBhxhList.get(dotGiaDichVuBhxhList.size() - 1);
        assertThat(testDotGiaDichVuBhxh.getDoiTuongApDung()).isEqualTo(DEFAULT_DOI_TUONG_AP_DUNG);
        assertThat(testDotGiaDichVuBhxh.getGhiChu()).isEqualTo(DEFAULT_GHI_CHU);
        assertThat(testDotGiaDichVuBhxh.getNgayApDung()).isEqualTo(DEFAULT_NGAY_AP_DUNG);
        assertThat(testDotGiaDichVuBhxh.getTen()).isEqualTo(DEFAULT_TEN);
    }

    @Test
    @Transactional
    public void createDotGiaDichVuBhxhWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = dotGiaDichVuBhxhRepository.findAll().size();

        // Create the DotGiaDichVuBhxh with an existing ID
        dotGiaDichVuBhxh.setId(1L);
        DotGiaDichVuBhxhDTO dotGiaDichVuBhxhDTO = dotGiaDichVuBhxhMapper.toDto(dotGiaDichVuBhxh);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDotGiaDichVuBhxhMockMvc.perform(post("/api/dot-gia-dich-vu-bhxhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dotGiaDichVuBhxhDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DotGiaDichVuBhxh in the database
        List<DotGiaDichVuBhxh> dotGiaDichVuBhxhList = dotGiaDichVuBhxhRepository.findAll();
        assertThat(dotGiaDichVuBhxhList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkDoiTuongApDungIsRequired() throws Exception {
        int databaseSizeBeforeTest = dotGiaDichVuBhxhRepository.findAll().size();
        // set the field null
        dotGiaDichVuBhxh.setDoiTuongApDung(null);

        // Create the DotGiaDichVuBhxh, which fails.
        DotGiaDichVuBhxhDTO dotGiaDichVuBhxhDTO = dotGiaDichVuBhxhMapper.toDto(dotGiaDichVuBhxh);

        restDotGiaDichVuBhxhMockMvc.perform(post("/api/dot-gia-dich-vu-bhxhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dotGiaDichVuBhxhDTO)))
            .andExpect(status().isBadRequest());

        List<DotGiaDichVuBhxh> dotGiaDichVuBhxhList = dotGiaDichVuBhxhRepository.findAll();
        assertThat(dotGiaDichVuBhxhList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNgayApDungIsRequired() throws Exception {
        int databaseSizeBeforeTest = dotGiaDichVuBhxhRepository.findAll().size();
        // set the field null
        dotGiaDichVuBhxh.setNgayApDung(null);

        // Create the DotGiaDichVuBhxh, which fails.
        DotGiaDichVuBhxhDTO dotGiaDichVuBhxhDTO = dotGiaDichVuBhxhMapper.toDto(dotGiaDichVuBhxh);

        restDotGiaDichVuBhxhMockMvc.perform(post("/api/dot-gia-dich-vu-bhxhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dotGiaDichVuBhxhDTO)))
            .andExpect(status().isBadRequest());

        List<DotGiaDichVuBhxh> dotGiaDichVuBhxhList = dotGiaDichVuBhxhRepository.findAll();
        assertThat(dotGiaDichVuBhxhList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTenIsRequired() throws Exception {
        int databaseSizeBeforeTest = dotGiaDichVuBhxhRepository.findAll().size();
        // set the field null
        dotGiaDichVuBhxh.setTen(null);

        // Create the DotGiaDichVuBhxh, which fails.
        DotGiaDichVuBhxhDTO dotGiaDichVuBhxhDTO = dotGiaDichVuBhxhMapper.toDto(dotGiaDichVuBhxh);

        restDotGiaDichVuBhxhMockMvc.perform(post("/api/dot-gia-dich-vu-bhxhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dotGiaDichVuBhxhDTO)))
            .andExpect(status().isBadRequest());

        List<DotGiaDichVuBhxh> dotGiaDichVuBhxhList = dotGiaDichVuBhxhRepository.findAll();
        assertThat(dotGiaDichVuBhxhList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDotGiaDichVuBhxhs() throws Exception {
        // Initialize the database
        dotGiaDichVuBhxhRepository.saveAndFlush(dotGiaDichVuBhxh);

        // Get all the dotGiaDichVuBhxhList
        restDotGiaDichVuBhxhMockMvc.perform(get("/api/dot-gia-dich-vu-bhxhs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dotGiaDichVuBhxh.getId().intValue())))
            .andExpect(jsonPath("$.[*].doiTuongApDung").value(hasItem(DEFAULT_DOI_TUONG_AP_DUNG.intValue())))
            .andExpect(jsonPath("$.[*].ghiChu").value(hasItem(DEFAULT_GHI_CHU)))
            .andExpect(jsonPath("$.[*].ngayApDung").value(hasItem(DEFAULT_NGAY_AP_DUNG.toString())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)));
    }

    @Test
    @Transactional
    public void getDotGiaDichVuBhxh() throws Exception {
        // Initialize the database
        dotGiaDichVuBhxhRepository.saveAndFlush(dotGiaDichVuBhxh);

        // Get the dotGiaDichVuBhxh
        restDotGiaDichVuBhxhMockMvc.perform(get("/api/dot-gia-dich-vu-bhxhs/{id}", dotGiaDichVuBhxh.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(dotGiaDichVuBhxh.getId().intValue()))
            .andExpect(jsonPath("$.doiTuongApDung").value(DEFAULT_DOI_TUONG_AP_DUNG.intValue()))
            .andExpect(jsonPath("$.ghiChu").value(DEFAULT_GHI_CHU))
            .andExpect(jsonPath("$.ngayApDung").value(DEFAULT_NGAY_AP_DUNG.toString()))
            .andExpect(jsonPath("$.ten").value(DEFAULT_TEN));
    }


    @Test
    @Transactional
    public void getDotGiaDichVuBhxhsByIdFiltering() throws Exception {
        // Initialize the database
        dotGiaDichVuBhxhRepository.saveAndFlush(dotGiaDichVuBhxh);

        Long id = dotGiaDichVuBhxh.getId();

        defaultDotGiaDichVuBhxhShouldBeFound("id.equals=" + id);
        defaultDotGiaDichVuBhxhShouldNotBeFound("id.notEquals=" + id);

        defaultDotGiaDichVuBhxhShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultDotGiaDichVuBhxhShouldNotBeFound("id.greaterThan=" + id);

        defaultDotGiaDichVuBhxhShouldBeFound("id.lessThanOrEqual=" + id);
        defaultDotGiaDichVuBhxhShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllDotGiaDichVuBhxhsByDoiTuongApDungIsEqualToSomething() throws Exception {
        // Initialize the database
        dotGiaDichVuBhxhRepository.saveAndFlush(dotGiaDichVuBhxh);

        // Get all the dotGiaDichVuBhxhList where doiTuongApDung equals to DEFAULT_DOI_TUONG_AP_DUNG
        defaultDotGiaDichVuBhxhShouldBeFound("doiTuongApDung.equals=" + DEFAULT_DOI_TUONG_AP_DUNG);

        // Get all the dotGiaDichVuBhxhList where doiTuongApDung equals to UPDATED_DOI_TUONG_AP_DUNG
        defaultDotGiaDichVuBhxhShouldNotBeFound("doiTuongApDung.equals=" + UPDATED_DOI_TUONG_AP_DUNG);
    }

    @Test
    @Transactional
    public void getAllDotGiaDichVuBhxhsByDoiTuongApDungIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dotGiaDichVuBhxhRepository.saveAndFlush(dotGiaDichVuBhxh);

        // Get all the dotGiaDichVuBhxhList where doiTuongApDung not equals to DEFAULT_DOI_TUONG_AP_DUNG
        defaultDotGiaDichVuBhxhShouldNotBeFound("doiTuongApDung.notEquals=" + DEFAULT_DOI_TUONG_AP_DUNG);

        // Get all the dotGiaDichVuBhxhList where doiTuongApDung not equals to UPDATED_DOI_TUONG_AP_DUNG
        defaultDotGiaDichVuBhxhShouldBeFound("doiTuongApDung.notEquals=" + UPDATED_DOI_TUONG_AP_DUNG);
    }

    @Test
    @Transactional
    public void getAllDotGiaDichVuBhxhsByDoiTuongApDungIsInShouldWork() throws Exception {
        // Initialize the database
        dotGiaDichVuBhxhRepository.saveAndFlush(dotGiaDichVuBhxh);

        // Get all the dotGiaDichVuBhxhList where doiTuongApDung in DEFAULT_DOI_TUONG_AP_DUNG or UPDATED_DOI_TUONG_AP_DUNG
        defaultDotGiaDichVuBhxhShouldBeFound("doiTuongApDung.in=" + DEFAULT_DOI_TUONG_AP_DUNG + "," + UPDATED_DOI_TUONG_AP_DUNG);

        // Get all the dotGiaDichVuBhxhList where doiTuongApDung equals to UPDATED_DOI_TUONG_AP_DUNG
        defaultDotGiaDichVuBhxhShouldNotBeFound("doiTuongApDung.in=" + UPDATED_DOI_TUONG_AP_DUNG);
    }

    @Test
    @Transactional
    public void getAllDotGiaDichVuBhxhsByDoiTuongApDungIsNullOrNotNull() throws Exception {
        // Initialize the database
        dotGiaDichVuBhxhRepository.saveAndFlush(dotGiaDichVuBhxh);

        // Get all the dotGiaDichVuBhxhList where doiTuongApDung is not null
        defaultDotGiaDichVuBhxhShouldBeFound("doiTuongApDung.specified=true");

        // Get all the dotGiaDichVuBhxhList where doiTuongApDung is null
        defaultDotGiaDichVuBhxhShouldNotBeFound("doiTuongApDung.specified=false");
    }

    @Test
    @Transactional
    public void getAllDotGiaDichVuBhxhsByDoiTuongApDungIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dotGiaDichVuBhxhRepository.saveAndFlush(dotGiaDichVuBhxh);

        // Get all the dotGiaDichVuBhxhList where doiTuongApDung is greater than or equal to DEFAULT_DOI_TUONG_AP_DUNG
        defaultDotGiaDichVuBhxhShouldBeFound("doiTuongApDung.greaterThanOrEqual=" + DEFAULT_DOI_TUONG_AP_DUNG);

        // Get all the dotGiaDichVuBhxhList where doiTuongApDung is greater than or equal to UPDATED_DOI_TUONG_AP_DUNG
        defaultDotGiaDichVuBhxhShouldNotBeFound("doiTuongApDung.greaterThanOrEqual=" + UPDATED_DOI_TUONG_AP_DUNG);
    }

    @Test
    @Transactional
    public void getAllDotGiaDichVuBhxhsByDoiTuongApDungIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dotGiaDichVuBhxhRepository.saveAndFlush(dotGiaDichVuBhxh);

        // Get all the dotGiaDichVuBhxhList where doiTuongApDung is less than or equal to DEFAULT_DOI_TUONG_AP_DUNG
        defaultDotGiaDichVuBhxhShouldBeFound("doiTuongApDung.lessThanOrEqual=" + DEFAULT_DOI_TUONG_AP_DUNG);

        // Get all the dotGiaDichVuBhxhList where doiTuongApDung is less than or equal to SMALLER_DOI_TUONG_AP_DUNG
        defaultDotGiaDichVuBhxhShouldNotBeFound("doiTuongApDung.lessThanOrEqual=" + SMALLER_DOI_TUONG_AP_DUNG);
    }

    @Test
    @Transactional
    public void getAllDotGiaDichVuBhxhsByDoiTuongApDungIsLessThanSomething() throws Exception {
        // Initialize the database
        dotGiaDichVuBhxhRepository.saveAndFlush(dotGiaDichVuBhxh);

        // Get all the dotGiaDichVuBhxhList where doiTuongApDung is less than DEFAULT_DOI_TUONG_AP_DUNG
        defaultDotGiaDichVuBhxhShouldNotBeFound("doiTuongApDung.lessThan=" + DEFAULT_DOI_TUONG_AP_DUNG);

        // Get all the dotGiaDichVuBhxhList where doiTuongApDung is less than UPDATED_DOI_TUONG_AP_DUNG
        defaultDotGiaDichVuBhxhShouldBeFound("doiTuongApDung.lessThan=" + UPDATED_DOI_TUONG_AP_DUNG);
    }

    @Test
    @Transactional
    public void getAllDotGiaDichVuBhxhsByDoiTuongApDungIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dotGiaDichVuBhxhRepository.saveAndFlush(dotGiaDichVuBhxh);

        // Get all the dotGiaDichVuBhxhList where doiTuongApDung is greater than DEFAULT_DOI_TUONG_AP_DUNG
        defaultDotGiaDichVuBhxhShouldNotBeFound("doiTuongApDung.greaterThan=" + DEFAULT_DOI_TUONG_AP_DUNG);

        // Get all the dotGiaDichVuBhxhList where doiTuongApDung is greater than SMALLER_DOI_TUONG_AP_DUNG
        defaultDotGiaDichVuBhxhShouldBeFound("doiTuongApDung.greaterThan=" + SMALLER_DOI_TUONG_AP_DUNG);
    }


    @Test
    @Transactional
    public void getAllDotGiaDichVuBhxhsByGhiChuIsEqualToSomething() throws Exception {
        // Initialize the database
        dotGiaDichVuBhxhRepository.saveAndFlush(dotGiaDichVuBhxh);

        // Get all the dotGiaDichVuBhxhList where ghiChu equals to DEFAULT_GHI_CHU
        defaultDotGiaDichVuBhxhShouldBeFound("ghiChu.equals=" + DEFAULT_GHI_CHU);

        // Get all the dotGiaDichVuBhxhList where ghiChu equals to UPDATED_GHI_CHU
        defaultDotGiaDichVuBhxhShouldNotBeFound("ghiChu.equals=" + UPDATED_GHI_CHU);
    }

    @Test
    @Transactional
    public void getAllDotGiaDichVuBhxhsByGhiChuIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dotGiaDichVuBhxhRepository.saveAndFlush(dotGiaDichVuBhxh);

        // Get all the dotGiaDichVuBhxhList where ghiChu not equals to DEFAULT_GHI_CHU
        defaultDotGiaDichVuBhxhShouldNotBeFound("ghiChu.notEquals=" + DEFAULT_GHI_CHU);

        // Get all the dotGiaDichVuBhxhList where ghiChu not equals to UPDATED_GHI_CHU
        defaultDotGiaDichVuBhxhShouldBeFound("ghiChu.notEquals=" + UPDATED_GHI_CHU);
    }

    @Test
    @Transactional
    public void getAllDotGiaDichVuBhxhsByGhiChuIsInShouldWork() throws Exception {
        // Initialize the database
        dotGiaDichVuBhxhRepository.saveAndFlush(dotGiaDichVuBhxh);

        // Get all the dotGiaDichVuBhxhList where ghiChu in DEFAULT_GHI_CHU or UPDATED_GHI_CHU
        defaultDotGiaDichVuBhxhShouldBeFound("ghiChu.in=" + DEFAULT_GHI_CHU + "," + UPDATED_GHI_CHU);

        // Get all the dotGiaDichVuBhxhList where ghiChu equals to UPDATED_GHI_CHU
        defaultDotGiaDichVuBhxhShouldNotBeFound("ghiChu.in=" + UPDATED_GHI_CHU);
    }

    @Test
    @Transactional
    public void getAllDotGiaDichVuBhxhsByGhiChuIsNullOrNotNull() throws Exception {
        // Initialize the database
        dotGiaDichVuBhxhRepository.saveAndFlush(dotGiaDichVuBhxh);

        // Get all the dotGiaDichVuBhxhList where ghiChu is not null
        defaultDotGiaDichVuBhxhShouldBeFound("ghiChu.specified=true");

        // Get all the dotGiaDichVuBhxhList where ghiChu is null
        defaultDotGiaDichVuBhxhShouldNotBeFound("ghiChu.specified=false");
    }
                @Test
    @Transactional
    public void getAllDotGiaDichVuBhxhsByGhiChuContainsSomething() throws Exception {
        // Initialize the database
        dotGiaDichVuBhxhRepository.saveAndFlush(dotGiaDichVuBhxh);

        // Get all the dotGiaDichVuBhxhList where ghiChu contains DEFAULT_GHI_CHU
        defaultDotGiaDichVuBhxhShouldBeFound("ghiChu.contains=" + DEFAULT_GHI_CHU);

        // Get all the dotGiaDichVuBhxhList where ghiChu contains UPDATED_GHI_CHU
        defaultDotGiaDichVuBhxhShouldNotBeFound("ghiChu.contains=" + UPDATED_GHI_CHU);
    }

    @Test
    @Transactional
    public void getAllDotGiaDichVuBhxhsByGhiChuNotContainsSomething() throws Exception {
        // Initialize the database
        dotGiaDichVuBhxhRepository.saveAndFlush(dotGiaDichVuBhxh);

        // Get all the dotGiaDichVuBhxhList where ghiChu does not contain DEFAULT_GHI_CHU
        defaultDotGiaDichVuBhxhShouldNotBeFound("ghiChu.doesNotContain=" + DEFAULT_GHI_CHU);

        // Get all the dotGiaDichVuBhxhList where ghiChu does not contain UPDATED_GHI_CHU
        defaultDotGiaDichVuBhxhShouldBeFound("ghiChu.doesNotContain=" + UPDATED_GHI_CHU);
    }


    @Test
    @Transactional
    public void getAllDotGiaDichVuBhxhsByNgayApDungIsEqualToSomething() throws Exception {
        // Initialize the database
        dotGiaDichVuBhxhRepository.saveAndFlush(dotGiaDichVuBhxh);

        // Get all the dotGiaDichVuBhxhList where ngayApDung equals to DEFAULT_NGAY_AP_DUNG
        defaultDotGiaDichVuBhxhShouldBeFound("ngayApDung.equals=" + DEFAULT_NGAY_AP_DUNG);

        // Get all the dotGiaDichVuBhxhList where ngayApDung equals to UPDATED_NGAY_AP_DUNG
        defaultDotGiaDichVuBhxhShouldNotBeFound("ngayApDung.equals=" + UPDATED_NGAY_AP_DUNG);
    }

    @Test
    @Transactional
    public void getAllDotGiaDichVuBhxhsByNgayApDungIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dotGiaDichVuBhxhRepository.saveAndFlush(dotGiaDichVuBhxh);

        // Get all the dotGiaDichVuBhxhList where ngayApDung not equals to DEFAULT_NGAY_AP_DUNG
        defaultDotGiaDichVuBhxhShouldNotBeFound("ngayApDung.notEquals=" + DEFAULT_NGAY_AP_DUNG);

        // Get all the dotGiaDichVuBhxhList where ngayApDung not equals to UPDATED_NGAY_AP_DUNG
        defaultDotGiaDichVuBhxhShouldBeFound("ngayApDung.notEquals=" + UPDATED_NGAY_AP_DUNG);
    }

    @Test
    @Transactional
    public void getAllDotGiaDichVuBhxhsByNgayApDungIsInShouldWork() throws Exception {
        // Initialize the database
        dotGiaDichVuBhxhRepository.saveAndFlush(dotGiaDichVuBhxh);

        // Get all the dotGiaDichVuBhxhList where ngayApDung in DEFAULT_NGAY_AP_DUNG or UPDATED_NGAY_AP_DUNG
        defaultDotGiaDichVuBhxhShouldBeFound("ngayApDung.in=" + DEFAULT_NGAY_AP_DUNG + "," + UPDATED_NGAY_AP_DUNG);

        // Get all the dotGiaDichVuBhxhList where ngayApDung equals to UPDATED_NGAY_AP_DUNG
        defaultDotGiaDichVuBhxhShouldNotBeFound("ngayApDung.in=" + UPDATED_NGAY_AP_DUNG);
    }

    @Test
    @Transactional
    public void getAllDotGiaDichVuBhxhsByNgayApDungIsNullOrNotNull() throws Exception {
        // Initialize the database
        dotGiaDichVuBhxhRepository.saveAndFlush(dotGiaDichVuBhxh);

        // Get all the dotGiaDichVuBhxhList where ngayApDung is not null
        defaultDotGiaDichVuBhxhShouldBeFound("ngayApDung.specified=true");

        // Get all the dotGiaDichVuBhxhList where ngayApDung is null
        defaultDotGiaDichVuBhxhShouldNotBeFound("ngayApDung.specified=false");
    }

    @Test
    @Transactional
    public void getAllDotGiaDichVuBhxhsByNgayApDungIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dotGiaDichVuBhxhRepository.saveAndFlush(dotGiaDichVuBhxh);

        // Get all the dotGiaDichVuBhxhList where ngayApDung is greater than or equal to DEFAULT_NGAY_AP_DUNG
        defaultDotGiaDichVuBhxhShouldBeFound("ngayApDung.greaterThanOrEqual=" + DEFAULT_NGAY_AP_DUNG);

        // Get all the dotGiaDichVuBhxhList where ngayApDung is greater than or equal to UPDATED_NGAY_AP_DUNG
        defaultDotGiaDichVuBhxhShouldNotBeFound("ngayApDung.greaterThanOrEqual=" + UPDATED_NGAY_AP_DUNG);
    }

    @Test
    @Transactional
    public void getAllDotGiaDichVuBhxhsByNgayApDungIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dotGiaDichVuBhxhRepository.saveAndFlush(dotGiaDichVuBhxh);

        // Get all the dotGiaDichVuBhxhList where ngayApDung is less than or equal to DEFAULT_NGAY_AP_DUNG
        defaultDotGiaDichVuBhxhShouldBeFound("ngayApDung.lessThanOrEqual=" + DEFAULT_NGAY_AP_DUNG);

        // Get all the dotGiaDichVuBhxhList where ngayApDung is less than or equal to SMALLER_NGAY_AP_DUNG
        defaultDotGiaDichVuBhxhShouldNotBeFound("ngayApDung.lessThanOrEqual=" + SMALLER_NGAY_AP_DUNG);
    }

    @Test
    @Transactional
    public void getAllDotGiaDichVuBhxhsByNgayApDungIsLessThanSomething() throws Exception {
        // Initialize the database
        dotGiaDichVuBhxhRepository.saveAndFlush(dotGiaDichVuBhxh);

        // Get all the dotGiaDichVuBhxhList where ngayApDung is less than DEFAULT_NGAY_AP_DUNG
        defaultDotGiaDichVuBhxhShouldNotBeFound("ngayApDung.lessThan=" + DEFAULT_NGAY_AP_DUNG);

        // Get all the dotGiaDichVuBhxhList where ngayApDung is less than UPDATED_NGAY_AP_DUNG
        defaultDotGiaDichVuBhxhShouldBeFound("ngayApDung.lessThan=" + UPDATED_NGAY_AP_DUNG);
    }

    @Test
    @Transactional
    public void getAllDotGiaDichVuBhxhsByNgayApDungIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dotGiaDichVuBhxhRepository.saveAndFlush(dotGiaDichVuBhxh);

        // Get all the dotGiaDichVuBhxhList where ngayApDung is greater than DEFAULT_NGAY_AP_DUNG
        defaultDotGiaDichVuBhxhShouldNotBeFound("ngayApDung.greaterThan=" + DEFAULT_NGAY_AP_DUNG);

        // Get all the dotGiaDichVuBhxhList where ngayApDung is greater than SMALLER_NGAY_AP_DUNG
        defaultDotGiaDichVuBhxhShouldBeFound("ngayApDung.greaterThan=" + SMALLER_NGAY_AP_DUNG);
    }


    @Test
    @Transactional
    public void getAllDotGiaDichVuBhxhsByTenIsEqualToSomething() throws Exception {
        // Initialize the database
        dotGiaDichVuBhxhRepository.saveAndFlush(dotGiaDichVuBhxh);

        // Get all the dotGiaDichVuBhxhList where ten equals to DEFAULT_TEN
        defaultDotGiaDichVuBhxhShouldBeFound("ten.equals=" + DEFAULT_TEN);

        // Get all the dotGiaDichVuBhxhList where ten equals to UPDATED_TEN
        defaultDotGiaDichVuBhxhShouldNotBeFound("ten.equals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllDotGiaDichVuBhxhsByTenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dotGiaDichVuBhxhRepository.saveAndFlush(dotGiaDichVuBhxh);

        // Get all the dotGiaDichVuBhxhList where ten not equals to DEFAULT_TEN
        defaultDotGiaDichVuBhxhShouldNotBeFound("ten.notEquals=" + DEFAULT_TEN);

        // Get all the dotGiaDichVuBhxhList where ten not equals to UPDATED_TEN
        defaultDotGiaDichVuBhxhShouldBeFound("ten.notEquals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllDotGiaDichVuBhxhsByTenIsInShouldWork() throws Exception {
        // Initialize the database
        dotGiaDichVuBhxhRepository.saveAndFlush(dotGiaDichVuBhxh);

        // Get all the dotGiaDichVuBhxhList where ten in DEFAULT_TEN or UPDATED_TEN
        defaultDotGiaDichVuBhxhShouldBeFound("ten.in=" + DEFAULT_TEN + "," + UPDATED_TEN);

        // Get all the dotGiaDichVuBhxhList where ten equals to UPDATED_TEN
        defaultDotGiaDichVuBhxhShouldNotBeFound("ten.in=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllDotGiaDichVuBhxhsByTenIsNullOrNotNull() throws Exception {
        // Initialize the database
        dotGiaDichVuBhxhRepository.saveAndFlush(dotGiaDichVuBhxh);

        // Get all the dotGiaDichVuBhxhList where ten is not null
        defaultDotGiaDichVuBhxhShouldBeFound("ten.specified=true");

        // Get all the dotGiaDichVuBhxhList where ten is null
        defaultDotGiaDichVuBhxhShouldNotBeFound("ten.specified=false");
    }
                @Test
    @Transactional
    public void getAllDotGiaDichVuBhxhsByTenContainsSomething() throws Exception {
        // Initialize the database
        dotGiaDichVuBhxhRepository.saveAndFlush(dotGiaDichVuBhxh);

        // Get all the dotGiaDichVuBhxhList where ten contains DEFAULT_TEN
        defaultDotGiaDichVuBhxhShouldBeFound("ten.contains=" + DEFAULT_TEN);

        // Get all the dotGiaDichVuBhxhList where ten contains UPDATED_TEN
        defaultDotGiaDichVuBhxhShouldNotBeFound("ten.contains=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllDotGiaDichVuBhxhsByTenNotContainsSomething() throws Exception {
        // Initialize the database
        dotGiaDichVuBhxhRepository.saveAndFlush(dotGiaDichVuBhxh);

        // Get all the dotGiaDichVuBhxhList where ten does not contain DEFAULT_TEN
        defaultDotGiaDichVuBhxhShouldNotBeFound("ten.doesNotContain=" + DEFAULT_TEN);

        // Get all the dotGiaDichVuBhxhList where ten does not contain UPDATED_TEN
        defaultDotGiaDichVuBhxhShouldBeFound("ten.doesNotContain=" + UPDATED_TEN);
    }


    @Test
    @Transactional
    public void getAllDotGiaDichVuBhxhsByDonViIsEqualToSomething() throws Exception {
        // Get already existing entity
        DonVi donVi = dotGiaDichVuBhxh.getDonVi();
        dotGiaDichVuBhxhRepository.saveAndFlush(dotGiaDichVuBhxh);
        Long donViId = donVi.getId();

        // Get all the dotGiaDichVuBhxhList where donVi equals to donViId
        defaultDotGiaDichVuBhxhShouldBeFound("donViId.equals=" + donViId);

        // Get all the dotGiaDichVuBhxhList where donVi equals to donViId + 1
        defaultDotGiaDichVuBhxhShouldNotBeFound("donViId.equals=" + (donViId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultDotGiaDichVuBhxhShouldBeFound(String filter) throws Exception {
        restDotGiaDichVuBhxhMockMvc.perform(get("/api/dot-gia-dich-vu-bhxhs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dotGiaDichVuBhxh.getId().intValue())))
            .andExpect(jsonPath("$.[*].doiTuongApDung").value(hasItem(DEFAULT_DOI_TUONG_AP_DUNG.intValue())))
            .andExpect(jsonPath("$.[*].ghiChu").value(hasItem(DEFAULT_GHI_CHU)))
            .andExpect(jsonPath("$.[*].ngayApDung").value(hasItem(DEFAULT_NGAY_AP_DUNG.toString())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)));

        // Check, that the count call also returns 1
        restDotGiaDichVuBhxhMockMvc.perform(get("/api/dot-gia-dich-vu-bhxhs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultDotGiaDichVuBhxhShouldNotBeFound(String filter) throws Exception {
        restDotGiaDichVuBhxhMockMvc.perform(get("/api/dot-gia-dich-vu-bhxhs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restDotGiaDichVuBhxhMockMvc.perform(get("/api/dot-gia-dich-vu-bhxhs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingDotGiaDichVuBhxh() throws Exception {
        // Get the dotGiaDichVuBhxh
        restDotGiaDichVuBhxhMockMvc.perform(get("/api/dot-gia-dich-vu-bhxhs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDotGiaDichVuBhxh() throws Exception {
        // Initialize the database
        dotGiaDichVuBhxhRepository.saveAndFlush(dotGiaDichVuBhxh);

        int databaseSizeBeforeUpdate = dotGiaDichVuBhxhRepository.findAll().size();

        // Update the dotGiaDichVuBhxh
        DotGiaDichVuBhxh updatedDotGiaDichVuBhxh = dotGiaDichVuBhxhRepository.findById(dotGiaDichVuBhxh.getId()).get();
        // Disconnect from session so that the updates on updatedDotGiaDichVuBhxh are not directly saved in db
        em.detach(updatedDotGiaDichVuBhxh);
        updatedDotGiaDichVuBhxh
            .doiTuongApDung(UPDATED_DOI_TUONG_AP_DUNG)
            .ghiChu(UPDATED_GHI_CHU)
            .ngayApDung(UPDATED_NGAY_AP_DUNG)
            .ten(UPDATED_TEN);
        DotGiaDichVuBhxhDTO dotGiaDichVuBhxhDTO = dotGiaDichVuBhxhMapper.toDto(updatedDotGiaDichVuBhxh);

        restDotGiaDichVuBhxhMockMvc.perform(put("/api/dot-gia-dich-vu-bhxhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dotGiaDichVuBhxhDTO)))
            .andExpect(status().isOk());

        // Validate the DotGiaDichVuBhxh in the database
        List<DotGiaDichVuBhxh> dotGiaDichVuBhxhList = dotGiaDichVuBhxhRepository.findAll();
        assertThat(dotGiaDichVuBhxhList).hasSize(databaseSizeBeforeUpdate);
        DotGiaDichVuBhxh testDotGiaDichVuBhxh = dotGiaDichVuBhxhList.get(dotGiaDichVuBhxhList.size() - 1);
        assertThat(testDotGiaDichVuBhxh.getDoiTuongApDung()).isEqualTo(UPDATED_DOI_TUONG_AP_DUNG);
        assertThat(testDotGiaDichVuBhxh.getGhiChu()).isEqualTo(UPDATED_GHI_CHU);
        assertThat(testDotGiaDichVuBhxh.getNgayApDung()).isEqualTo(UPDATED_NGAY_AP_DUNG);
        assertThat(testDotGiaDichVuBhxh.getTen()).isEqualTo(UPDATED_TEN);
    }

    @Test
    @Transactional
    public void updateNonExistingDotGiaDichVuBhxh() throws Exception {
        int databaseSizeBeforeUpdate = dotGiaDichVuBhxhRepository.findAll().size();

        // Create the DotGiaDichVuBhxh
        DotGiaDichVuBhxhDTO dotGiaDichVuBhxhDTO = dotGiaDichVuBhxhMapper.toDto(dotGiaDichVuBhxh);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDotGiaDichVuBhxhMockMvc.perform(put("/api/dot-gia-dich-vu-bhxhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dotGiaDichVuBhxhDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DotGiaDichVuBhxh in the database
        List<DotGiaDichVuBhxh> dotGiaDichVuBhxhList = dotGiaDichVuBhxhRepository.findAll();
        assertThat(dotGiaDichVuBhxhList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDotGiaDichVuBhxh() throws Exception {
        // Initialize the database
        dotGiaDichVuBhxhRepository.saveAndFlush(dotGiaDichVuBhxh);

        int databaseSizeBeforeDelete = dotGiaDichVuBhxhRepository.findAll().size();

        // Delete the dotGiaDichVuBhxh
        restDotGiaDichVuBhxhMockMvc.perform(delete("/api/dot-gia-dich-vu-bhxhs/{id}", dotGiaDichVuBhxh.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DotGiaDichVuBhxh> dotGiaDichVuBhxhList = dotGiaDichVuBhxhRepository.findAll();
        assertThat(dotGiaDichVuBhxhList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

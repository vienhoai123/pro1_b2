package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.NhomChanDoanHinhAnh;
import vn.vnpt.domain.DonVi;
import vn.vnpt.repository.NhomChanDoanHinhAnhRepository;
import vn.vnpt.service.NhomChanDoanHinhAnhService;
import vn.vnpt.service.dto.NhomChanDoanHinhAnhDTO;
import vn.vnpt.service.mapper.NhomChanDoanHinhAnhMapper;
import vn.vnpt.service.dto.NhomChanDoanHinhAnhCriteria;
import vn.vnpt.service.NhomChanDoanHinhAnhQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link NhomChanDoanHinhAnhResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class NhomChanDoanHinhAnhResourceIT {

    private static final Boolean DEFAULT_ENABLE = false;
    private static final Boolean UPDATED_ENABLE = true;

    private static final Integer DEFAULT_LEVEL = 1;
    private static final Integer UPDATED_LEVEL = 2;
    private static final Integer SMALLER_LEVEL = 1 - 1;

    private static final BigDecimal DEFAULT_PARENT_ID = new BigDecimal(1);
    private static final BigDecimal UPDATED_PARENT_ID = new BigDecimal(2);
    private static final BigDecimal SMALLER_PARENT_ID = new BigDecimal(1 - 1);

    private static final String DEFAULT_TEN = "AAAAAAAAAA";
    private static final String UPDATED_TEN = "BBBBBBBBBB";

    @Autowired
    private NhomChanDoanHinhAnhRepository nhomChanDoanHinhAnhRepository;

    @Autowired
    private NhomChanDoanHinhAnhMapper nhomChanDoanHinhAnhMapper;

    @Autowired
    private NhomChanDoanHinhAnhService nhomChanDoanHinhAnhService;

    @Autowired
    private NhomChanDoanHinhAnhQueryService nhomChanDoanHinhAnhQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restNhomChanDoanHinhAnhMockMvc;

    private NhomChanDoanHinhAnh nhomChanDoanHinhAnh;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NhomChanDoanHinhAnh createEntity(EntityManager em) {
        NhomChanDoanHinhAnh nhomChanDoanHinhAnh = new NhomChanDoanHinhAnh()
            .enable(DEFAULT_ENABLE)
            .level(DEFAULT_LEVEL)
            .parentId(DEFAULT_PARENT_ID)
            .ten(DEFAULT_TEN);
        // Add required entity
        DonVi donVi;
        if (TestUtil.findAll(em, DonVi.class).isEmpty()) {
            donVi = DonViResourceIT.createEntity(em);
            em.persist(donVi);
            em.flush();
        } else {
            donVi = TestUtil.findAll(em, DonVi.class).get(0);
        }
        nhomChanDoanHinhAnh.setDonVi(donVi);
        return nhomChanDoanHinhAnh;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NhomChanDoanHinhAnh createUpdatedEntity(EntityManager em) {
        NhomChanDoanHinhAnh nhomChanDoanHinhAnh = new NhomChanDoanHinhAnh()
            .enable(UPDATED_ENABLE)
            .level(UPDATED_LEVEL)
            .parentId(UPDATED_PARENT_ID)
            .ten(UPDATED_TEN);
        // Add required entity
        DonVi donVi;
        if (TestUtil.findAll(em, DonVi.class).isEmpty()) {
            donVi = DonViResourceIT.createUpdatedEntity(em);
            em.persist(donVi);
            em.flush();
        } else {
            donVi = TestUtil.findAll(em, DonVi.class).get(0);
        }
        nhomChanDoanHinhAnh.setDonVi(donVi);
        return nhomChanDoanHinhAnh;
    }

    @BeforeEach
    public void initTest() {
        nhomChanDoanHinhAnh = createEntity(em);
    }

    @Test
    @Transactional
    public void createNhomChanDoanHinhAnh() throws Exception {
        int databaseSizeBeforeCreate = nhomChanDoanHinhAnhRepository.findAll().size();

        // Create the NhomChanDoanHinhAnh
        NhomChanDoanHinhAnhDTO nhomChanDoanHinhAnhDTO = nhomChanDoanHinhAnhMapper.toDto(nhomChanDoanHinhAnh);
        restNhomChanDoanHinhAnhMockMvc.perform(post("/api/nhom-chan-doan-hinh-anhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(nhomChanDoanHinhAnhDTO)))
            .andExpect(status().isCreated());

        // Validate the NhomChanDoanHinhAnh in the database
        List<NhomChanDoanHinhAnh> nhomChanDoanHinhAnhList = nhomChanDoanHinhAnhRepository.findAll();
        assertThat(nhomChanDoanHinhAnhList).hasSize(databaseSizeBeforeCreate + 1);
        NhomChanDoanHinhAnh testNhomChanDoanHinhAnh = nhomChanDoanHinhAnhList.get(nhomChanDoanHinhAnhList.size() - 1);
        assertThat(testNhomChanDoanHinhAnh.isEnable()).isEqualTo(DEFAULT_ENABLE);
        assertThat(testNhomChanDoanHinhAnh.getLevel()).isEqualTo(DEFAULT_LEVEL);
        assertThat(testNhomChanDoanHinhAnh.getParentId()).isEqualTo(DEFAULT_PARENT_ID);
        assertThat(testNhomChanDoanHinhAnh.getTen()).isEqualTo(DEFAULT_TEN);
    }

    @Test
    @Transactional
    public void createNhomChanDoanHinhAnhWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = nhomChanDoanHinhAnhRepository.findAll().size();

        // Create the NhomChanDoanHinhAnh with an existing ID
        nhomChanDoanHinhAnh.setId(1L);
        NhomChanDoanHinhAnhDTO nhomChanDoanHinhAnhDTO = nhomChanDoanHinhAnhMapper.toDto(nhomChanDoanHinhAnh);

        // An entity with an existing ID cannot be created, so this API call must fail
        restNhomChanDoanHinhAnhMockMvc.perform(post("/api/nhom-chan-doan-hinh-anhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(nhomChanDoanHinhAnhDTO)))
            .andExpect(status().isBadRequest());

        // Validate the NhomChanDoanHinhAnh in the database
        List<NhomChanDoanHinhAnh> nhomChanDoanHinhAnhList = nhomChanDoanHinhAnhRepository.findAll();
        assertThat(nhomChanDoanHinhAnhList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkEnableIsRequired() throws Exception {
        int databaseSizeBeforeTest = nhomChanDoanHinhAnhRepository.findAll().size();
        // set the field null
        nhomChanDoanHinhAnh.setEnable(null);

        // Create the NhomChanDoanHinhAnh, which fails.
        NhomChanDoanHinhAnhDTO nhomChanDoanHinhAnhDTO = nhomChanDoanHinhAnhMapper.toDto(nhomChanDoanHinhAnh);

        restNhomChanDoanHinhAnhMockMvc.perform(post("/api/nhom-chan-doan-hinh-anhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(nhomChanDoanHinhAnhDTO)))
            .andExpect(status().isBadRequest());

        List<NhomChanDoanHinhAnh> nhomChanDoanHinhAnhList = nhomChanDoanHinhAnhRepository.findAll();
        assertThat(nhomChanDoanHinhAnhList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLevelIsRequired() throws Exception {
        int databaseSizeBeforeTest = nhomChanDoanHinhAnhRepository.findAll().size();
        // set the field null
        nhomChanDoanHinhAnh.setLevel(null);

        // Create the NhomChanDoanHinhAnh, which fails.
        NhomChanDoanHinhAnhDTO nhomChanDoanHinhAnhDTO = nhomChanDoanHinhAnhMapper.toDto(nhomChanDoanHinhAnh);

        restNhomChanDoanHinhAnhMockMvc.perform(post("/api/nhom-chan-doan-hinh-anhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(nhomChanDoanHinhAnhDTO)))
            .andExpect(status().isBadRequest());

        List<NhomChanDoanHinhAnh> nhomChanDoanHinhAnhList = nhomChanDoanHinhAnhRepository.findAll();
        assertThat(nhomChanDoanHinhAnhList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkParentIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = nhomChanDoanHinhAnhRepository.findAll().size();
        // set the field null
        nhomChanDoanHinhAnh.setParentId(null);

        // Create the NhomChanDoanHinhAnh, which fails.
        NhomChanDoanHinhAnhDTO nhomChanDoanHinhAnhDTO = nhomChanDoanHinhAnhMapper.toDto(nhomChanDoanHinhAnh);

        restNhomChanDoanHinhAnhMockMvc.perform(post("/api/nhom-chan-doan-hinh-anhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(nhomChanDoanHinhAnhDTO)))
            .andExpect(status().isBadRequest());

        List<NhomChanDoanHinhAnh> nhomChanDoanHinhAnhList = nhomChanDoanHinhAnhRepository.findAll();
        assertThat(nhomChanDoanHinhAnhList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllNhomChanDoanHinhAnhs() throws Exception {
        // Initialize the database
        nhomChanDoanHinhAnhRepository.saveAndFlush(nhomChanDoanHinhAnh);

        // Get all the nhomChanDoanHinhAnhList
        restNhomChanDoanHinhAnhMockMvc.perform(get("/api/nhom-chan-doan-hinh-anhs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(nhomChanDoanHinhAnh.getId().intValue())))
            .andExpect(jsonPath("$.[*].enable").value(hasItem(DEFAULT_ENABLE.booleanValue())))
            .andExpect(jsonPath("$.[*].level").value(hasItem(DEFAULT_LEVEL)))
            .andExpect(jsonPath("$.[*].parentId").value(hasItem(DEFAULT_PARENT_ID.intValue())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)));
    }
    
    @Test
    @Transactional
    public void getNhomChanDoanHinhAnh() throws Exception {
        // Initialize the database
        nhomChanDoanHinhAnhRepository.saveAndFlush(nhomChanDoanHinhAnh);

        // Get the nhomChanDoanHinhAnh
        restNhomChanDoanHinhAnhMockMvc.perform(get("/api/nhom-chan-doan-hinh-anhs/{id}", nhomChanDoanHinhAnh.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(nhomChanDoanHinhAnh.getId().intValue()))
            .andExpect(jsonPath("$.enable").value(DEFAULT_ENABLE.booleanValue()))
            .andExpect(jsonPath("$.level").value(DEFAULT_LEVEL))
            .andExpect(jsonPath("$.parentId").value(DEFAULT_PARENT_ID.intValue()))
            .andExpect(jsonPath("$.ten").value(DEFAULT_TEN));
    }


    @Test
    @Transactional
    public void getNhomChanDoanHinhAnhsByIdFiltering() throws Exception {
        // Initialize the database
        nhomChanDoanHinhAnhRepository.saveAndFlush(nhomChanDoanHinhAnh);

        Long id = nhomChanDoanHinhAnh.getId();

        defaultNhomChanDoanHinhAnhShouldBeFound("id.equals=" + id);
        defaultNhomChanDoanHinhAnhShouldNotBeFound("id.notEquals=" + id);

        defaultNhomChanDoanHinhAnhShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultNhomChanDoanHinhAnhShouldNotBeFound("id.greaterThan=" + id);

        defaultNhomChanDoanHinhAnhShouldBeFound("id.lessThanOrEqual=" + id);
        defaultNhomChanDoanHinhAnhShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllNhomChanDoanHinhAnhsByEnableIsEqualToSomething() throws Exception {
        // Initialize the database
        nhomChanDoanHinhAnhRepository.saveAndFlush(nhomChanDoanHinhAnh);

        // Get all the nhomChanDoanHinhAnhList where enable equals to DEFAULT_ENABLE
        defaultNhomChanDoanHinhAnhShouldBeFound("enable.equals=" + DEFAULT_ENABLE);

        // Get all the nhomChanDoanHinhAnhList where enable equals to UPDATED_ENABLE
        defaultNhomChanDoanHinhAnhShouldNotBeFound("enable.equals=" + UPDATED_ENABLE);
    }

    @Test
    @Transactional
    public void getAllNhomChanDoanHinhAnhsByEnableIsNotEqualToSomething() throws Exception {
        // Initialize the database
        nhomChanDoanHinhAnhRepository.saveAndFlush(nhomChanDoanHinhAnh);

        // Get all the nhomChanDoanHinhAnhList where enable not equals to DEFAULT_ENABLE
        defaultNhomChanDoanHinhAnhShouldNotBeFound("enable.notEquals=" + DEFAULT_ENABLE);

        // Get all the nhomChanDoanHinhAnhList where enable not equals to UPDATED_ENABLE
        defaultNhomChanDoanHinhAnhShouldBeFound("enable.notEquals=" + UPDATED_ENABLE);
    }

    @Test
    @Transactional
    public void getAllNhomChanDoanHinhAnhsByEnableIsInShouldWork() throws Exception {
        // Initialize the database
        nhomChanDoanHinhAnhRepository.saveAndFlush(nhomChanDoanHinhAnh);

        // Get all the nhomChanDoanHinhAnhList where enable in DEFAULT_ENABLE or UPDATED_ENABLE
        defaultNhomChanDoanHinhAnhShouldBeFound("enable.in=" + DEFAULT_ENABLE + "," + UPDATED_ENABLE);

        // Get all the nhomChanDoanHinhAnhList where enable equals to UPDATED_ENABLE
        defaultNhomChanDoanHinhAnhShouldNotBeFound("enable.in=" + UPDATED_ENABLE);
    }

    @Test
    @Transactional
    public void getAllNhomChanDoanHinhAnhsByEnableIsNullOrNotNull() throws Exception {
        // Initialize the database
        nhomChanDoanHinhAnhRepository.saveAndFlush(nhomChanDoanHinhAnh);

        // Get all the nhomChanDoanHinhAnhList where enable is not null
        defaultNhomChanDoanHinhAnhShouldBeFound("enable.specified=true");

        // Get all the nhomChanDoanHinhAnhList where enable is null
        defaultNhomChanDoanHinhAnhShouldNotBeFound("enable.specified=false");
    }

    @Test
    @Transactional
    public void getAllNhomChanDoanHinhAnhsByLevelIsEqualToSomething() throws Exception {
        // Initialize the database
        nhomChanDoanHinhAnhRepository.saveAndFlush(nhomChanDoanHinhAnh);

        // Get all the nhomChanDoanHinhAnhList where level equals to DEFAULT_LEVEL
        defaultNhomChanDoanHinhAnhShouldBeFound("level.equals=" + DEFAULT_LEVEL);

        // Get all the nhomChanDoanHinhAnhList where level equals to UPDATED_LEVEL
        defaultNhomChanDoanHinhAnhShouldNotBeFound("level.equals=" + UPDATED_LEVEL);
    }

    @Test
    @Transactional
    public void getAllNhomChanDoanHinhAnhsByLevelIsNotEqualToSomething() throws Exception {
        // Initialize the database
        nhomChanDoanHinhAnhRepository.saveAndFlush(nhomChanDoanHinhAnh);

        // Get all the nhomChanDoanHinhAnhList where level not equals to DEFAULT_LEVEL
        defaultNhomChanDoanHinhAnhShouldNotBeFound("level.notEquals=" + DEFAULT_LEVEL);

        // Get all the nhomChanDoanHinhAnhList where level not equals to UPDATED_LEVEL
        defaultNhomChanDoanHinhAnhShouldBeFound("level.notEquals=" + UPDATED_LEVEL);
    }

    @Test
    @Transactional
    public void getAllNhomChanDoanHinhAnhsByLevelIsInShouldWork() throws Exception {
        // Initialize the database
        nhomChanDoanHinhAnhRepository.saveAndFlush(nhomChanDoanHinhAnh);

        // Get all the nhomChanDoanHinhAnhList where level in DEFAULT_LEVEL or UPDATED_LEVEL
        defaultNhomChanDoanHinhAnhShouldBeFound("level.in=" + DEFAULT_LEVEL + "," + UPDATED_LEVEL);

        // Get all the nhomChanDoanHinhAnhList where level equals to UPDATED_LEVEL
        defaultNhomChanDoanHinhAnhShouldNotBeFound("level.in=" + UPDATED_LEVEL);
    }

    @Test
    @Transactional
    public void getAllNhomChanDoanHinhAnhsByLevelIsNullOrNotNull() throws Exception {
        // Initialize the database
        nhomChanDoanHinhAnhRepository.saveAndFlush(nhomChanDoanHinhAnh);

        // Get all the nhomChanDoanHinhAnhList where level is not null
        defaultNhomChanDoanHinhAnhShouldBeFound("level.specified=true");

        // Get all the nhomChanDoanHinhAnhList where level is null
        defaultNhomChanDoanHinhAnhShouldNotBeFound("level.specified=false");
    }

    @Test
    @Transactional
    public void getAllNhomChanDoanHinhAnhsByLevelIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        nhomChanDoanHinhAnhRepository.saveAndFlush(nhomChanDoanHinhAnh);

        // Get all the nhomChanDoanHinhAnhList where level is greater than or equal to DEFAULT_LEVEL
        defaultNhomChanDoanHinhAnhShouldBeFound("level.greaterThanOrEqual=" + DEFAULT_LEVEL);

        // Get all the nhomChanDoanHinhAnhList where level is greater than or equal to UPDATED_LEVEL
        defaultNhomChanDoanHinhAnhShouldNotBeFound("level.greaterThanOrEqual=" + UPDATED_LEVEL);
    }

    @Test
    @Transactional
    public void getAllNhomChanDoanHinhAnhsByLevelIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        nhomChanDoanHinhAnhRepository.saveAndFlush(nhomChanDoanHinhAnh);

        // Get all the nhomChanDoanHinhAnhList where level is less than or equal to DEFAULT_LEVEL
        defaultNhomChanDoanHinhAnhShouldBeFound("level.lessThanOrEqual=" + DEFAULT_LEVEL);

        // Get all the nhomChanDoanHinhAnhList where level is less than or equal to SMALLER_LEVEL
        defaultNhomChanDoanHinhAnhShouldNotBeFound("level.lessThanOrEqual=" + SMALLER_LEVEL);
    }

    @Test
    @Transactional
    public void getAllNhomChanDoanHinhAnhsByLevelIsLessThanSomething() throws Exception {
        // Initialize the database
        nhomChanDoanHinhAnhRepository.saveAndFlush(nhomChanDoanHinhAnh);

        // Get all the nhomChanDoanHinhAnhList where level is less than DEFAULT_LEVEL
        defaultNhomChanDoanHinhAnhShouldNotBeFound("level.lessThan=" + DEFAULT_LEVEL);

        // Get all the nhomChanDoanHinhAnhList where level is less than UPDATED_LEVEL
        defaultNhomChanDoanHinhAnhShouldBeFound("level.lessThan=" + UPDATED_LEVEL);
    }

    @Test
    @Transactional
    public void getAllNhomChanDoanHinhAnhsByLevelIsGreaterThanSomething() throws Exception {
        // Initialize the database
        nhomChanDoanHinhAnhRepository.saveAndFlush(nhomChanDoanHinhAnh);

        // Get all the nhomChanDoanHinhAnhList where level is greater than DEFAULT_LEVEL
        defaultNhomChanDoanHinhAnhShouldNotBeFound("level.greaterThan=" + DEFAULT_LEVEL);

        // Get all the nhomChanDoanHinhAnhList where level is greater than SMALLER_LEVEL
        defaultNhomChanDoanHinhAnhShouldBeFound("level.greaterThan=" + SMALLER_LEVEL);
    }


    @Test
    @Transactional
    public void getAllNhomChanDoanHinhAnhsByParentIdIsEqualToSomething() throws Exception {
        // Initialize the database
        nhomChanDoanHinhAnhRepository.saveAndFlush(nhomChanDoanHinhAnh);

        // Get all the nhomChanDoanHinhAnhList where parentId equals to DEFAULT_PARENT_ID
        defaultNhomChanDoanHinhAnhShouldBeFound("parentId.equals=" + DEFAULT_PARENT_ID);

        // Get all the nhomChanDoanHinhAnhList where parentId equals to UPDATED_PARENT_ID
        defaultNhomChanDoanHinhAnhShouldNotBeFound("parentId.equals=" + UPDATED_PARENT_ID);
    }

    @Test
    @Transactional
    public void getAllNhomChanDoanHinhAnhsByParentIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        nhomChanDoanHinhAnhRepository.saveAndFlush(nhomChanDoanHinhAnh);

        // Get all the nhomChanDoanHinhAnhList where parentId not equals to DEFAULT_PARENT_ID
        defaultNhomChanDoanHinhAnhShouldNotBeFound("parentId.notEquals=" + DEFAULT_PARENT_ID);

        // Get all the nhomChanDoanHinhAnhList where parentId not equals to UPDATED_PARENT_ID
        defaultNhomChanDoanHinhAnhShouldBeFound("parentId.notEquals=" + UPDATED_PARENT_ID);
    }

    @Test
    @Transactional
    public void getAllNhomChanDoanHinhAnhsByParentIdIsInShouldWork() throws Exception {
        // Initialize the database
        nhomChanDoanHinhAnhRepository.saveAndFlush(nhomChanDoanHinhAnh);

        // Get all the nhomChanDoanHinhAnhList where parentId in DEFAULT_PARENT_ID or UPDATED_PARENT_ID
        defaultNhomChanDoanHinhAnhShouldBeFound("parentId.in=" + DEFAULT_PARENT_ID + "," + UPDATED_PARENT_ID);

        // Get all the nhomChanDoanHinhAnhList where parentId equals to UPDATED_PARENT_ID
        defaultNhomChanDoanHinhAnhShouldNotBeFound("parentId.in=" + UPDATED_PARENT_ID);
    }

    @Test
    @Transactional
    public void getAllNhomChanDoanHinhAnhsByParentIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        nhomChanDoanHinhAnhRepository.saveAndFlush(nhomChanDoanHinhAnh);

        // Get all the nhomChanDoanHinhAnhList where parentId is not null
        defaultNhomChanDoanHinhAnhShouldBeFound("parentId.specified=true");

        // Get all the nhomChanDoanHinhAnhList where parentId is null
        defaultNhomChanDoanHinhAnhShouldNotBeFound("parentId.specified=false");
    }

    @Test
    @Transactional
    public void getAllNhomChanDoanHinhAnhsByParentIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        nhomChanDoanHinhAnhRepository.saveAndFlush(nhomChanDoanHinhAnh);

        // Get all the nhomChanDoanHinhAnhList where parentId is greater than or equal to DEFAULT_PARENT_ID
        defaultNhomChanDoanHinhAnhShouldBeFound("parentId.greaterThanOrEqual=" + DEFAULT_PARENT_ID);

        // Get all the nhomChanDoanHinhAnhList where parentId is greater than or equal to UPDATED_PARENT_ID
        defaultNhomChanDoanHinhAnhShouldNotBeFound("parentId.greaterThanOrEqual=" + UPDATED_PARENT_ID);
    }

    @Test
    @Transactional
    public void getAllNhomChanDoanHinhAnhsByParentIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        nhomChanDoanHinhAnhRepository.saveAndFlush(nhomChanDoanHinhAnh);

        // Get all the nhomChanDoanHinhAnhList where parentId is less than or equal to DEFAULT_PARENT_ID
        defaultNhomChanDoanHinhAnhShouldBeFound("parentId.lessThanOrEqual=" + DEFAULT_PARENT_ID);

        // Get all the nhomChanDoanHinhAnhList where parentId is less than or equal to SMALLER_PARENT_ID
        defaultNhomChanDoanHinhAnhShouldNotBeFound("parentId.lessThanOrEqual=" + SMALLER_PARENT_ID);
    }

    @Test
    @Transactional
    public void getAllNhomChanDoanHinhAnhsByParentIdIsLessThanSomething() throws Exception {
        // Initialize the database
        nhomChanDoanHinhAnhRepository.saveAndFlush(nhomChanDoanHinhAnh);

        // Get all the nhomChanDoanHinhAnhList where parentId is less than DEFAULT_PARENT_ID
        defaultNhomChanDoanHinhAnhShouldNotBeFound("parentId.lessThan=" + DEFAULT_PARENT_ID);

        // Get all the nhomChanDoanHinhAnhList where parentId is less than UPDATED_PARENT_ID
        defaultNhomChanDoanHinhAnhShouldBeFound("parentId.lessThan=" + UPDATED_PARENT_ID);
    }

    @Test
    @Transactional
    public void getAllNhomChanDoanHinhAnhsByParentIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        nhomChanDoanHinhAnhRepository.saveAndFlush(nhomChanDoanHinhAnh);

        // Get all the nhomChanDoanHinhAnhList where parentId is greater than DEFAULT_PARENT_ID
        defaultNhomChanDoanHinhAnhShouldNotBeFound("parentId.greaterThan=" + DEFAULT_PARENT_ID);

        // Get all the nhomChanDoanHinhAnhList where parentId is greater than SMALLER_PARENT_ID
        defaultNhomChanDoanHinhAnhShouldBeFound("parentId.greaterThan=" + SMALLER_PARENT_ID);
    }


    @Test
    @Transactional
    public void getAllNhomChanDoanHinhAnhsByTenIsEqualToSomething() throws Exception {
        // Initialize the database
        nhomChanDoanHinhAnhRepository.saveAndFlush(nhomChanDoanHinhAnh);

        // Get all the nhomChanDoanHinhAnhList where ten equals to DEFAULT_TEN
        defaultNhomChanDoanHinhAnhShouldBeFound("ten.equals=" + DEFAULT_TEN);

        // Get all the nhomChanDoanHinhAnhList where ten equals to UPDATED_TEN
        defaultNhomChanDoanHinhAnhShouldNotBeFound("ten.equals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllNhomChanDoanHinhAnhsByTenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        nhomChanDoanHinhAnhRepository.saveAndFlush(nhomChanDoanHinhAnh);

        // Get all the nhomChanDoanHinhAnhList where ten not equals to DEFAULT_TEN
        defaultNhomChanDoanHinhAnhShouldNotBeFound("ten.notEquals=" + DEFAULT_TEN);

        // Get all the nhomChanDoanHinhAnhList where ten not equals to UPDATED_TEN
        defaultNhomChanDoanHinhAnhShouldBeFound("ten.notEquals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllNhomChanDoanHinhAnhsByTenIsInShouldWork() throws Exception {
        // Initialize the database
        nhomChanDoanHinhAnhRepository.saveAndFlush(nhomChanDoanHinhAnh);

        // Get all the nhomChanDoanHinhAnhList where ten in DEFAULT_TEN or UPDATED_TEN
        defaultNhomChanDoanHinhAnhShouldBeFound("ten.in=" + DEFAULT_TEN + "," + UPDATED_TEN);

        // Get all the nhomChanDoanHinhAnhList where ten equals to UPDATED_TEN
        defaultNhomChanDoanHinhAnhShouldNotBeFound("ten.in=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllNhomChanDoanHinhAnhsByTenIsNullOrNotNull() throws Exception {
        // Initialize the database
        nhomChanDoanHinhAnhRepository.saveAndFlush(nhomChanDoanHinhAnh);

        // Get all the nhomChanDoanHinhAnhList where ten is not null
        defaultNhomChanDoanHinhAnhShouldBeFound("ten.specified=true");

        // Get all the nhomChanDoanHinhAnhList where ten is null
        defaultNhomChanDoanHinhAnhShouldNotBeFound("ten.specified=false");
    }
                @Test
    @Transactional
    public void getAllNhomChanDoanHinhAnhsByTenContainsSomething() throws Exception {
        // Initialize the database
        nhomChanDoanHinhAnhRepository.saveAndFlush(nhomChanDoanHinhAnh);

        // Get all the nhomChanDoanHinhAnhList where ten contains DEFAULT_TEN
        defaultNhomChanDoanHinhAnhShouldBeFound("ten.contains=" + DEFAULT_TEN);

        // Get all the nhomChanDoanHinhAnhList where ten contains UPDATED_TEN
        defaultNhomChanDoanHinhAnhShouldNotBeFound("ten.contains=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllNhomChanDoanHinhAnhsByTenNotContainsSomething() throws Exception {
        // Initialize the database
        nhomChanDoanHinhAnhRepository.saveAndFlush(nhomChanDoanHinhAnh);

        // Get all the nhomChanDoanHinhAnhList where ten does not contain DEFAULT_TEN
        defaultNhomChanDoanHinhAnhShouldNotBeFound("ten.doesNotContain=" + DEFAULT_TEN);

        // Get all the nhomChanDoanHinhAnhList where ten does not contain UPDATED_TEN
        defaultNhomChanDoanHinhAnhShouldBeFound("ten.doesNotContain=" + UPDATED_TEN);
    }


    @Test
    @Transactional
    public void getAllNhomChanDoanHinhAnhsByDonViIsEqualToSomething() throws Exception {
        // Get already existing entity
        DonVi donVi = nhomChanDoanHinhAnh.getDonVi();
        nhomChanDoanHinhAnhRepository.saveAndFlush(nhomChanDoanHinhAnh);
        Long donViId = donVi.getId();

        // Get all the nhomChanDoanHinhAnhList where donVi equals to donViId
        defaultNhomChanDoanHinhAnhShouldBeFound("donViId.equals=" + donViId);

        // Get all the nhomChanDoanHinhAnhList where donVi equals to donViId + 1
        defaultNhomChanDoanHinhAnhShouldNotBeFound("donViId.equals=" + (donViId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultNhomChanDoanHinhAnhShouldBeFound(String filter) throws Exception {
        restNhomChanDoanHinhAnhMockMvc.perform(get("/api/nhom-chan-doan-hinh-anhs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(nhomChanDoanHinhAnh.getId().intValue())))
            .andExpect(jsonPath("$.[*].enable").value(hasItem(DEFAULT_ENABLE.booleanValue())))
            .andExpect(jsonPath("$.[*].level").value(hasItem(DEFAULT_LEVEL)))
            .andExpect(jsonPath("$.[*].parentId").value(hasItem(DEFAULT_PARENT_ID.intValue())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)));

        // Check, that the count call also returns 1
        restNhomChanDoanHinhAnhMockMvc.perform(get("/api/nhom-chan-doan-hinh-anhs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultNhomChanDoanHinhAnhShouldNotBeFound(String filter) throws Exception {
        restNhomChanDoanHinhAnhMockMvc.perform(get("/api/nhom-chan-doan-hinh-anhs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restNhomChanDoanHinhAnhMockMvc.perform(get("/api/nhom-chan-doan-hinh-anhs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingNhomChanDoanHinhAnh() throws Exception {
        // Get the nhomChanDoanHinhAnh
        restNhomChanDoanHinhAnhMockMvc.perform(get("/api/nhom-chan-doan-hinh-anhs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNhomChanDoanHinhAnh() throws Exception {
        // Initialize the database
        nhomChanDoanHinhAnhRepository.saveAndFlush(nhomChanDoanHinhAnh);

        int databaseSizeBeforeUpdate = nhomChanDoanHinhAnhRepository.findAll().size();

        // Update the nhomChanDoanHinhAnh
        NhomChanDoanHinhAnh updatedNhomChanDoanHinhAnh = nhomChanDoanHinhAnhRepository.findById(nhomChanDoanHinhAnh.getId()).get();
        // Disconnect from session so that the updates on updatedNhomChanDoanHinhAnh are not directly saved in db
        em.detach(updatedNhomChanDoanHinhAnh);
        updatedNhomChanDoanHinhAnh
            .enable(UPDATED_ENABLE)
            .level(UPDATED_LEVEL)
            .parentId(UPDATED_PARENT_ID)
            .ten(UPDATED_TEN);
        NhomChanDoanHinhAnhDTO nhomChanDoanHinhAnhDTO = nhomChanDoanHinhAnhMapper.toDto(updatedNhomChanDoanHinhAnh);

        restNhomChanDoanHinhAnhMockMvc.perform(put("/api/nhom-chan-doan-hinh-anhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(nhomChanDoanHinhAnhDTO)))
            .andExpect(status().isOk());

        // Validate the NhomChanDoanHinhAnh in the database
        List<NhomChanDoanHinhAnh> nhomChanDoanHinhAnhList = nhomChanDoanHinhAnhRepository.findAll();
        assertThat(nhomChanDoanHinhAnhList).hasSize(databaseSizeBeforeUpdate);
        NhomChanDoanHinhAnh testNhomChanDoanHinhAnh = nhomChanDoanHinhAnhList.get(nhomChanDoanHinhAnhList.size() - 1);
        assertThat(testNhomChanDoanHinhAnh.isEnable()).isEqualTo(UPDATED_ENABLE);
        assertThat(testNhomChanDoanHinhAnh.getLevel()).isEqualTo(UPDATED_LEVEL);
        assertThat(testNhomChanDoanHinhAnh.getParentId()).isEqualTo(UPDATED_PARENT_ID);
        assertThat(testNhomChanDoanHinhAnh.getTen()).isEqualTo(UPDATED_TEN);
    }

    @Test
    @Transactional
    public void updateNonExistingNhomChanDoanHinhAnh() throws Exception {
        int databaseSizeBeforeUpdate = nhomChanDoanHinhAnhRepository.findAll().size();

        // Create the NhomChanDoanHinhAnh
        NhomChanDoanHinhAnhDTO nhomChanDoanHinhAnhDTO = nhomChanDoanHinhAnhMapper.toDto(nhomChanDoanHinhAnh);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restNhomChanDoanHinhAnhMockMvc.perform(put("/api/nhom-chan-doan-hinh-anhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(nhomChanDoanHinhAnhDTO)))
            .andExpect(status().isBadRequest());

        // Validate the NhomChanDoanHinhAnh in the database
        List<NhomChanDoanHinhAnh> nhomChanDoanHinhAnhList = nhomChanDoanHinhAnhRepository.findAll();
        assertThat(nhomChanDoanHinhAnhList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteNhomChanDoanHinhAnh() throws Exception {
        // Initialize the database
        nhomChanDoanHinhAnhRepository.saveAndFlush(nhomChanDoanHinhAnh);

        int databaseSizeBeforeDelete = nhomChanDoanHinhAnhRepository.findAll().size();

        // Delete the nhomChanDoanHinhAnh
        restNhomChanDoanHinhAnhMockMvc.perform(delete("/api/nhom-chan-doan-hinh-anhs/{id}", nhomChanDoanHinhAnh.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<NhomChanDoanHinhAnh> nhomChanDoanHinhAnhList = nhomChanDoanHinhAnhRepository.findAll();
        assertThat(nhomChanDoanHinhAnhList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

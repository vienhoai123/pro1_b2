package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.TPhanNhomCDHA;
import vn.vnpt.domain.NhomChanDoanHinhAnh;
import vn.vnpt.domain.ChanDoanHinhAnh;
import vn.vnpt.repository.TPhanNhomCDHARepository;
import vn.vnpt.service.TPhanNhomCDHAService;
import vn.vnpt.service.dto.TPhanNhomCDHADTO;
import vn.vnpt.service.mapper.TPhanNhomCDHAMapper;
import vn.vnpt.service.dto.TPhanNhomCDHACriteria;
import vn.vnpt.service.TPhanNhomCDHAQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TPhanNhomCDHAResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class TPhanNhomCDHAResourceIT {

    @Autowired
    private TPhanNhomCDHARepository tPhanNhomCDHARepository;

    @Autowired
    private TPhanNhomCDHAMapper tPhanNhomCDHAMapper;

    @Autowired
    private TPhanNhomCDHAService tPhanNhomCDHAService;

    @Autowired
    private TPhanNhomCDHAQueryService tPhanNhomCDHAQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restTPhanNhomCDHAMockMvc;

    private TPhanNhomCDHA tPhanNhomCDHA;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TPhanNhomCDHA createEntity(EntityManager em) {
        TPhanNhomCDHA tPhanNhomCDHA = new TPhanNhomCDHA();
        // Add required entity
        NhomChanDoanHinhAnh nhomChanDoanHinhAnh;
        if (TestUtil.findAll(em, NhomChanDoanHinhAnh.class).isEmpty()) {
            nhomChanDoanHinhAnh = NhomChanDoanHinhAnhResourceIT.createEntity(em);
            em.persist(nhomChanDoanHinhAnh);
            em.flush();
        } else {
            nhomChanDoanHinhAnh = TestUtil.findAll(em, NhomChanDoanHinhAnh.class).get(0);
        }
        tPhanNhomCDHA.setNhom_cdha(nhomChanDoanHinhAnh);
        // Add required entity
        ChanDoanHinhAnh chanDoanHinhAnh;
        if (TestUtil.findAll(em, ChanDoanHinhAnh.class).isEmpty()) {
            chanDoanHinhAnh = ChanDoanHinhAnhResourceIT.createEntity(em);
            em.persist(chanDoanHinhAnh);
            em.flush();
        } else {
            chanDoanHinhAnh = TestUtil.findAll(em, ChanDoanHinhAnh.class).get(0);
        }
        tPhanNhomCDHA.setCdha(chanDoanHinhAnh);
        return tPhanNhomCDHA;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TPhanNhomCDHA createUpdatedEntity(EntityManager em) {
        TPhanNhomCDHA tPhanNhomCDHA = new TPhanNhomCDHA();
        // Add required entity
        NhomChanDoanHinhAnh nhomChanDoanHinhAnh;
        if (TestUtil.findAll(em, NhomChanDoanHinhAnh.class).isEmpty()) {
            nhomChanDoanHinhAnh = NhomChanDoanHinhAnhResourceIT.createUpdatedEntity(em);
            em.persist(nhomChanDoanHinhAnh);
            em.flush();
        } else {
            nhomChanDoanHinhAnh = TestUtil.findAll(em, NhomChanDoanHinhAnh.class).get(0);
        }
        tPhanNhomCDHA.setNhom_cdha(nhomChanDoanHinhAnh);
        // Add required entity
        ChanDoanHinhAnh chanDoanHinhAnh;
        if (TestUtil.findAll(em, ChanDoanHinhAnh.class).isEmpty()) {
            chanDoanHinhAnh = ChanDoanHinhAnhResourceIT.createUpdatedEntity(em);
            em.persist(chanDoanHinhAnh);
            em.flush();
        } else {
            chanDoanHinhAnh = TestUtil.findAll(em, ChanDoanHinhAnh.class).get(0);
        }
        tPhanNhomCDHA.setCdha(chanDoanHinhAnh);
        return tPhanNhomCDHA;
    }

    @BeforeEach
    public void initTest() {
        tPhanNhomCDHA = createEntity(em);
    }

    @Test
    @Transactional
    public void createTPhanNhomCDHA() throws Exception {
        int databaseSizeBeforeCreate = tPhanNhomCDHARepository.findAll().size();

        // Create the TPhanNhomCDHA
        TPhanNhomCDHADTO tPhanNhomCDHADTO = tPhanNhomCDHAMapper.toDto(tPhanNhomCDHA);
        restTPhanNhomCDHAMockMvc.perform(post("/api/t-phan-nhom-cdhas").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tPhanNhomCDHADTO)))
            .andExpect(status().isCreated());

        // Validate the TPhanNhomCDHA in the database
        List<TPhanNhomCDHA> tPhanNhomCDHAList = tPhanNhomCDHARepository.findAll();
        assertThat(tPhanNhomCDHAList).hasSize(databaseSizeBeforeCreate + 1);
        TPhanNhomCDHA testTPhanNhomCDHA = tPhanNhomCDHAList.get(tPhanNhomCDHAList.size() - 1);
    }

    @Test
    @Transactional
    public void createTPhanNhomCDHAWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tPhanNhomCDHARepository.findAll().size();

        // Create the TPhanNhomCDHA with an existing ID
        tPhanNhomCDHA.setId(1L);
        TPhanNhomCDHADTO tPhanNhomCDHADTO = tPhanNhomCDHAMapper.toDto(tPhanNhomCDHA);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTPhanNhomCDHAMockMvc.perform(post("/api/t-phan-nhom-cdhas").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tPhanNhomCDHADTO)))
            .andExpect(status().isBadRequest());

        // Validate the TPhanNhomCDHA in the database
        List<TPhanNhomCDHA> tPhanNhomCDHAList = tPhanNhomCDHARepository.findAll();
        assertThat(tPhanNhomCDHAList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllTPhanNhomCDHAS() throws Exception {
        // Initialize the database
        tPhanNhomCDHARepository.saveAndFlush(tPhanNhomCDHA);

        // Get all the tPhanNhomCDHAList
        restTPhanNhomCDHAMockMvc.perform(get("/api/t-phan-nhom-cdhas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tPhanNhomCDHA.getId().intValue())));
    }
    
    @Test
    @Transactional
    public void getTPhanNhomCDHA() throws Exception {
        // Initialize the database
        tPhanNhomCDHARepository.saveAndFlush(tPhanNhomCDHA);

        // Get the tPhanNhomCDHA
        restTPhanNhomCDHAMockMvc.perform(get("/api/t-phan-nhom-cdhas/{id}", tPhanNhomCDHA.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(tPhanNhomCDHA.getId().intValue()));
    }


    @Test
    @Transactional
    public void getTPhanNhomCDHASByIdFiltering() throws Exception {
        // Initialize the database
        tPhanNhomCDHARepository.saveAndFlush(tPhanNhomCDHA);

        Long id = tPhanNhomCDHA.getId();

        defaultTPhanNhomCDHAShouldBeFound("id.equals=" + id);
        defaultTPhanNhomCDHAShouldNotBeFound("id.notEquals=" + id);

        defaultTPhanNhomCDHAShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultTPhanNhomCDHAShouldNotBeFound("id.greaterThan=" + id);

        defaultTPhanNhomCDHAShouldBeFound("id.lessThanOrEqual=" + id);
        defaultTPhanNhomCDHAShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllTPhanNhomCDHASByNhom_cdhaIsEqualToSomething() throws Exception {
        // Get already existing entity
        NhomChanDoanHinhAnh nhom_cdha = tPhanNhomCDHA.getNhom_cdha();
        tPhanNhomCDHARepository.saveAndFlush(tPhanNhomCDHA);
        Long nhom_cdhaId = nhom_cdha.getId();

        // Get all the tPhanNhomCDHAList where nhom_cdha equals to nhom_cdhaId
        defaultTPhanNhomCDHAShouldBeFound("nhom_cdhaId.equals=" + nhom_cdhaId);

        // Get all the tPhanNhomCDHAList where nhom_cdha equals to nhom_cdhaId + 1
        defaultTPhanNhomCDHAShouldNotBeFound("nhom_cdhaId.equals=" + (nhom_cdhaId + 1));
    }


    @Test
    @Transactional
    public void getAllTPhanNhomCDHASByCdhaIsEqualToSomething() throws Exception {
        // Get already existing entity
        ChanDoanHinhAnh cdha = tPhanNhomCDHA.getCdha();
        tPhanNhomCDHARepository.saveAndFlush(tPhanNhomCDHA);
        Long cdhaId = cdha.getId();

        // Get all the tPhanNhomCDHAList where cdha equals to cdhaId
        defaultTPhanNhomCDHAShouldBeFound("cdhaId.equals=" + cdhaId);

        // Get all the tPhanNhomCDHAList where cdha equals to cdhaId + 1
        defaultTPhanNhomCDHAShouldNotBeFound("cdhaId.equals=" + (cdhaId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultTPhanNhomCDHAShouldBeFound(String filter) throws Exception {
        restTPhanNhomCDHAMockMvc.perform(get("/api/t-phan-nhom-cdhas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tPhanNhomCDHA.getId().intValue())));

        // Check, that the count call also returns 1
        restTPhanNhomCDHAMockMvc.perform(get("/api/t-phan-nhom-cdhas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultTPhanNhomCDHAShouldNotBeFound(String filter) throws Exception {
        restTPhanNhomCDHAMockMvc.perform(get("/api/t-phan-nhom-cdhas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restTPhanNhomCDHAMockMvc.perform(get("/api/t-phan-nhom-cdhas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingTPhanNhomCDHA() throws Exception {
        // Get the tPhanNhomCDHA
        restTPhanNhomCDHAMockMvc.perform(get("/api/t-phan-nhom-cdhas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTPhanNhomCDHA() throws Exception {
        // Initialize the database
        tPhanNhomCDHARepository.saveAndFlush(tPhanNhomCDHA);

        int databaseSizeBeforeUpdate = tPhanNhomCDHARepository.findAll().size();

        // Update the tPhanNhomCDHA
        TPhanNhomCDHA updatedTPhanNhomCDHA = tPhanNhomCDHARepository.findById(tPhanNhomCDHA.getId()).get();
        // Disconnect from session so that the updates on updatedTPhanNhomCDHA are not directly saved in db
        em.detach(updatedTPhanNhomCDHA);
        TPhanNhomCDHADTO tPhanNhomCDHADTO = tPhanNhomCDHAMapper.toDto(updatedTPhanNhomCDHA);

        restTPhanNhomCDHAMockMvc.perform(put("/api/t-phan-nhom-cdhas").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tPhanNhomCDHADTO)))
            .andExpect(status().isOk());

        // Validate the TPhanNhomCDHA in the database
        List<TPhanNhomCDHA> tPhanNhomCDHAList = tPhanNhomCDHARepository.findAll();
        assertThat(tPhanNhomCDHAList).hasSize(databaseSizeBeforeUpdate);
        TPhanNhomCDHA testTPhanNhomCDHA = tPhanNhomCDHAList.get(tPhanNhomCDHAList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingTPhanNhomCDHA() throws Exception {
        int databaseSizeBeforeUpdate = tPhanNhomCDHARepository.findAll().size();

        // Create the TPhanNhomCDHA
        TPhanNhomCDHADTO tPhanNhomCDHADTO = tPhanNhomCDHAMapper.toDto(tPhanNhomCDHA);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTPhanNhomCDHAMockMvc.perform(put("/api/t-phan-nhom-cdhas").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tPhanNhomCDHADTO)))
            .andExpect(status().isBadRequest());

        // Validate the TPhanNhomCDHA in the database
        List<TPhanNhomCDHA> tPhanNhomCDHAList = tPhanNhomCDHARepository.findAll();
        assertThat(tPhanNhomCDHAList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTPhanNhomCDHA() throws Exception {
        // Initialize the database
        tPhanNhomCDHARepository.saveAndFlush(tPhanNhomCDHA);

        int databaseSizeBeforeDelete = tPhanNhomCDHARepository.findAll().size();

        // Delete the tPhanNhomCDHA
        restTPhanNhomCDHAMockMvc.perform(delete("/api/t-phan-nhom-cdhas/{id}", tPhanNhomCDHA.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TPhanNhomCDHA> tPhanNhomCDHAList = tPhanNhomCDHARepository.findAll();
        assertThat(tPhanNhomCDHAList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

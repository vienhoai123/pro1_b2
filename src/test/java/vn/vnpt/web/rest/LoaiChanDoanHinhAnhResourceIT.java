package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.LoaiChanDoanHinhAnh;
import vn.vnpt.domain.DonVi;
import vn.vnpt.repository.LoaiChanDoanHinhAnhRepository;
import vn.vnpt.service.LoaiChanDoanHinhAnhService;
import vn.vnpt.service.dto.LoaiChanDoanHinhAnhDTO;
import vn.vnpt.service.mapper.LoaiChanDoanHinhAnhMapper;
import vn.vnpt.service.dto.LoaiChanDoanHinhAnhCriteria;
import vn.vnpt.service.LoaiChanDoanHinhAnhQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link LoaiChanDoanHinhAnhResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class LoaiChanDoanHinhAnhResourceIT {

    private static final String DEFAULT_TEN = "AAAAAAAAAA";
    private static final String UPDATED_TEN = "BBBBBBBBBB";

    private static final String DEFAULT_MO_TA = "AAAAAAAAAA";
    private static final String UPDATED_MO_TA = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ENABLE = false;
    private static final Boolean UPDATED_ENABLE = true;

    private static final Integer DEFAULT_UU_TIEN = 1;
    private static final Integer UPDATED_UU_TIEN = 2;
    private static final Integer SMALLER_UU_TIEN = 1 - 1;

    private static final String DEFAULT_MA_PHAN_LOAI = "AAAAAAAAAA";
    private static final String UPDATED_MA_PHAN_LOAI = "BBBBBBBBBB";

    @Autowired
    private LoaiChanDoanHinhAnhRepository loaiChanDoanHinhAnhRepository;

    @Autowired
    private LoaiChanDoanHinhAnhMapper loaiChanDoanHinhAnhMapper;

    @Autowired
    private LoaiChanDoanHinhAnhService loaiChanDoanHinhAnhService;

    @Autowired
    private LoaiChanDoanHinhAnhQueryService loaiChanDoanHinhAnhQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restLoaiChanDoanHinhAnhMockMvc;

    private LoaiChanDoanHinhAnh loaiChanDoanHinhAnh;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LoaiChanDoanHinhAnh createEntity(EntityManager em) {
        LoaiChanDoanHinhAnh loaiChanDoanHinhAnh = new LoaiChanDoanHinhAnh()
            .ten(DEFAULT_TEN)
            .moTa(DEFAULT_MO_TA)
            .enable(DEFAULT_ENABLE)
            .uuTien(DEFAULT_UU_TIEN)
            .maPhanLoai(DEFAULT_MA_PHAN_LOAI);
        // Add required entity
        DonVi donVi;
        if (TestUtil.findAll(em, DonVi.class).isEmpty()) {
            donVi = DonViResourceIT.createEntity(em);
            em.persist(donVi);
            em.flush();
        } else {
            donVi = TestUtil.findAll(em, DonVi.class).get(0);
        }
        loaiChanDoanHinhAnh.setDonVi(donVi);
        return loaiChanDoanHinhAnh;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LoaiChanDoanHinhAnh createUpdatedEntity(EntityManager em) {
        LoaiChanDoanHinhAnh loaiChanDoanHinhAnh = new LoaiChanDoanHinhAnh()
            .ten(UPDATED_TEN)
            .moTa(UPDATED_MO_TA)
            .enable(UPDATED_ENABLE)
            .uuTien(UPDATED_UU_TIEN)
            .maPhanLoai(UPDATED_MA_PHAN_LOAI);
        // Add required entity
        DonVi donVi;
        if (TestUtil.findAll(em, DonVi.class).isEmpty()) {
            donVi = DonViResourceIT.createUpdatedEntity(em);
            em.persist(donVi);
            em.flush();
        } else {
            donVi = TestUtil.findAll(em, DonVi.class).get(0);
        }
        loaiChanDoanHinhAnh.setDonVi(donVi);
        return loaiChanDoanHinhAnh;
    }

    @BeforeEach
    public void initTest() {
        loaiChanDoanHinhAnh = createEntity(em);
    }

    @Test
    @Transactional
    public void createLoaiChanDoanHinhAnh() throws Exception {
        int databaseSizeBeforeCreate = loaiChanDoanHinhAnhRepository.findAll().size();

        // Create the LoaiChanDoanHinhAnh
        LoaiChanDoanHinhAnhDTO loaiChanDoanHinhAnhDTO = loaiChanDoanHinhAnhMapper.toDto(loaiChanDoanHinhAnh);
        restLoaiChanDoanHinhAnhMockMvc.perform(post("/api/loai-chan-doan-hinh-anhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(loaiChanDoanHinhAnhDTO)))
            .andExpect(status().isCreated());

        // Validate the LoaiChanDoanHinhAnh in the database
        List<LoaiChanDoanHinhAnh> loaiChanDoanHinhAnhList = loaiChanDoanHinhAnhRepository.findAll();
        assertThat(loaiChanDoanHinhAnhList).hasSize(databaseSizeBeforeCreate + 1);
        LoaiChanDoanHinhAnh testLoaiChanDoanHinhAnh = loaiChanDoanHinhAnhList.get(loaiChanDoanHinhAnhList.size() - 1);
        assertThat(testLoaiChanDoanHinhAnh.getTen()).isEqualTo(DEFAULT_TEN);
        assertThat(testLoaiChanDoanHinhAnh.getMoTa()).isEqualTo(DEFAULT_MO_TA);
        assertThat(testLoaiChanDoanHinhAnh.isEnable()).isEqualTo(DEFAULT_ENABLE);
        assertThat(testLoaiChanDoanHinhAnh.getUuTien()).isEqualTo(DEFAULT_UU_TIEN);
        assertThat(testLoaiChanDoanHinhAnh.getMaPhanLoai()).isEqualTo(DEFAULT_MA_PHAN_LOAI);
    }

    @Test
    @Transactional
    public void createLoaiChanDoanHinhAnhWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = loaiChanDoanHinhAnhRepository.findAll().size();

        // Create the LoaiChanDoanHinhAnh with an existing ID
        loaiChanDoanHinhAnh.setId(1L);
        LoaiChanDoanHinhAnhDTO loaiChanDoanHinhAnhDTO = loaiChanDoanHinhAnhMapper.toDto(loaiChanDoanHinhAnh);

        // An entity with an existing ID cannot be created, so this API call must fail
        restLoaiChanDoanHinhAnhMockMvc.perform(post("/api/loai-chan-doan-hinh-anhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(loaiChanDoanHinhAnhDTO)))
            .andExpect(status().isBadRequest());

        // Validate the LoaiChanDoanHinhAnh in the database
        List<LoaiChanDoanHinhAnh> loaiChanDoanHinhAnhList = loaiChanDoanHinhAnhRepository.findAll();
        assertThat(loaiChanDoanHinhAnhList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkUuTienIsRequired() throws Exception {
        int databaseSizeBeforeTest = loaiChanDoanHinhAnhRepository.findAll().size();
        // set the field null
        loaiChanDoanHinhAnh.setUuTien(null);

        // Create the LoaiChanDoanHinhAnh, which fails.
        LoaiChanDoanHinhAnhDTO loaiChanDoanHinhAnhDTO = loaiChanDoanHinhAnhMapper.toDto(loaiChanDoanHinhAnh);

        restLoaiChanDoanHinhAnhMockMvc.perform(post("/api/loai-chan-doan-hinh-anhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(loaiChanDoanHinhAnhDTO)))
            .andExpect(status().isBadRequest());

        List<LoaiChanDoanHinhAnh> loaiChanDoanHinhAnhList = loaiChanDoanHinhAnhRepository.findAll();
        assertThat(loaiChanDoanHinhAnhList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllLoaiChanDoanHinhAnhs() throws Exception {
        // Initialize the database
        loaiChanDoanHinhAnhRepository.saveAndFlush(loaiChanDoanHinhAnh);

        // Get all the loaiChanDoanHinhAnhList
        restLoaiChanDoanHinhAnhMockMvc.perform(get("/api/loai-chan-doan-hinh-anhs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(loaiChanDoanHinhAnh.getId().intValue())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].moTa").value(hasItem(DEFAULT_MO_TA)))
            .andExpect(jsonPath("$.[*].enable").value(hasItem(DEFAULT_ENABLE.booleanValue())))
            .andExpect(jsonPath("$.[*].uuTien").value(hasItem(DEFAULT_UU_TIEN)))
            .andExpect(jsonPath("$.[*].maPhanLoai").value(hasItem(DEFAULT_MA_PHAN_LOAI)));
    }
    
    @Test
    @Transactional
    public void getLoaiChanDoanHinhAnh() throws Exception {
        // Initialize the database
        loaiChanDoanHinhAnhRepository.saveAndFlush(loaiChanDoanHinhAnh);

        // Get the loaiChanDoanHinhAnh
        restLoaiChanDoanHinhAnhMockMvc.perform(get("/api/loai-chan-doan-hinh-anhs/{id}", loaiChanDoanHinhAnh.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(loaiChanDoanHinhAnh.getId().intValue()))
            .andExpect(jsonPath("$.ten").value(DEFAULT_TEN))
            .andExpect(jsonPath("$.moTa").value(DEFAULT_MO_TA))
            .andExpect(jsonPath("$.enable").value(DEFAULT_ENABLE.booleanValue()))
            .andExpect(jsonPath("$.uuTien").value(DEFAULT_UU_TIEN))
            .andExpect(jsonPath("$.maPhanLoai").value(DEFAULT_MA_PHAN_LOAI));
    }


    @Test
    @Transactional
    public void getLoaiChanDoanHinhAnhsByIdFiltering() throws Exception {
        // Initialize the database
        loaiChanDoanHinhAnhRepository.saveAndFlush(loaiChanDoanHinhAnh);

        Long id = loaiChanDoanHinhAnh.getId();

        defaultLoaiChanDoanHinhAnhShouldBeFound("id.equals=" + id);
        defaultLoaiChanDoanHinhAnhShouldNotBeFound("id.notEquals=" + id);

        defaultLoaiChanDoanHinhAnhShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultLoaiChanDoanHinhAnhShouldNotBeFound("id.greaterThan=" + id);

        defaultLoaiChanDoanHinhAnhShouldBeFound("id.lessThanOrEqual=" + id);
        defaultLoaiChanDoanHinhAnhShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllLoaiChanDoanHinhAnhsByTenIsEqualToSomething() throws Exception {
        // Initialize the database
        loaiChanDoanHinhAnhRepository.saveAndFlush(loaiChanDoanHinhAnh);

        // Get all the loaiChanDoanHinhAnhList where ten equals to DEFAULT_TEN
        defaultLoaiChanDoanHinhAnhShouldBeFound("ten.equals=" + DEFAULT_TEN);

        // Get all the loaiChanDoanHinhAnhList where ten equals to UPDATED_TEN
        defaultLoaiChanDoanHinhAnhShouldNotBeFound("ten.equals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllLoaiChanDoanHinhAnhsByTenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        loaiChanDoanHinhAnhRepository.saveAndFlush(loaiChanDoanHinhAnh);

        // Get all the loaiChanDoanHinhAnhList where ten not equals to DEFAULT_TEN
        defaultLoaiChanDoanHinhAnhShouldNotBeFound("ten.notEquals=" + DEFAULT_TEN);

        // Get all the loaiChanDoanHinhAnhList where ten not equals to UPDATED_TEN
        defaultLoaiChanDoanHinhAnhShouldBeFound("ten.notEquals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllLoaiChanDoanHinhAnhsByTenIsInShouldWork() throws Exception {
        // Initialize the database
        loaiChanDoanHinhAnhRepository.saveAndFlush(loaiChanDoanHinhAnh);

        // Get all the loaiChanDoanHinhAnhList where ten in DEFAULT_TEN or UPDATED_TEN
        defaultLoaiChanDoanHinhAnhShouldBeFound("ten.in=" + DEFAULT_TEN + "," + UPDATED_TEN);

        // Get all the loaiChanDoanHinhAnhList where ten equals to UPDATED_TEN
        defaultLoaiChanDoanHinhAnhShouldNotBeFound("ten.in=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllLoaiChanDoanHinhAnhsByTenIsNullOrNotNull() throws Exception {
        // Initialize the database
        loaiChanDoanHinhAnhRepository.saveAndFlush(loaiChanDoanHinhAnh);

        // Get all the loaiChanDoanHinhAnhList where ten is not null
        defaultLoaiChanDoanHinhAnhShouldBeFound("ten.specified=true");

        // Get all the loaiChanDoanHinhAnhList where ten is null
        defaultLoaiChanDoanHinhAnhShouldNotBeFound("ten.specified=false");
    }
                @Test
    @Transactional
    public void getAllLoaiChanDoanHinhAnhsByTenContainsSomething() throws Exception {
        // Initialize the database
        loaiChanDoanHinhAnhRepository.saveAndFlush(loaiChanDoanHinhAnh);

        // Get all the loaiChanDoanHinhAnhList where ten contains DEFAULT_TEN
        defaultLoaiChanDoanHinhAnhShouldBeFound("ten.contains=" + DEFAULT_TEN);

        // Get all the loaiChanDoanHinhAnhList where ten contains UPDATED_TEN
        defaultLoaiChanDoanHinhAnhShouldNotBeFound("ten.contains=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllLoaiChanDoanHinhAnhsByTenNotContainsSomething() throws Exception {
        // Initialize the database
        loaiChanDoanHinhAnhRepository.saveAndFlush(loaiChanDoanHinhAnh);

        // Get all the loaiChanDoanHinhAnhList where ten does not contain DEFAULT_TEN
        defaultLoaiChanDoanHinhAnhShouldNotBeFound("ten.doesNotContain=" + DEFAULT_TEN);

        // Get all the loaiChanDoanHinhAnhList where ten does not contain UPDATED_TEN
        defaultLoaiChanDoanHinhAnhShouldBeFound("ten.doesNotContain=" + UPDATED_TEN);
    }


    @Test
    @Transactional
    public void getAllLoaiChanDoanHinhAnhsByMoTaIsEqualToSomething() throws Exception {
        // Initialize the database
        loaiChanDoanHinhAnhRepository.saveAndFlush(loaiChanDoanHinhAnh);

        // Get all the loaiChanDoanHinhAnhList where moTa equals to DEFAULT_MO_TA
        defaultLoaiChanDoanHinhAnhShouldBeFound("moTa.equals=" + DEFAULT_MO_TA);

        // Get all the loaiChanDoanHinhAnhList where moTa equals to UPDATED_MO_TA
        defaultLoaiChanDoanHinhAnhShouldNotBeFound("moTa.equals=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    public void getAllLoaiChanDoanHinhAnhsByMoTaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        loaiChanDoanHinhAnhRepository.saveAndFlush(loaiChanDoanHinhAnh);

        // Get all the loaiChanDoanHinhAnhList where moTa not equals to DEFAULT_MO_TA
        defaultLoaiChanDoanHinhAnhShouldNotBeFound("moTa.notEquals=" + DEFAULT_MO_TA);

        // Get all the loaiChanDoanHinhAnhList where moTa not equals to UPDATED_MO_TA
        defaultLoaiChanDoanHinhAnhShouldBeFound("moTa.notEquals=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    public void getAllLoaiChanDoanHinhAnhsByMoTaIsInShouldWork() throws Exception {
        // Initialize the database
        loaiChanDoanHinhAnhRepository.saveAndFlush(loaiChanDoanHinhAnh);

        // Get all the loaiChanDoanHinhAnhList where moTa in DEFAULT_MO_TA or UPDATED_MO_TA
        defaultLoaiChanDoanHinhAnhShouldBeFound("moTa.in=" + DEFAULT_MO_TA + "," + UPDATED_MO_TA);

        // Get all the loaiChanDoanHinhAnhList where moTa equals to UPDATED_MO_TA
        defaultLoaiChanDoanHinhAnhShouldNotBeFound("moTa.in=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    public void getAllLoaiChanDoanHinhAnhsByMoTaIsNullOrNotNull() throws Exception {
        // Initialize the database
        loaiChanDoanHinhAnhRepository.saveAndFlush(loaiChanDoanHinhAnh);

        // Get all the loaiChanDoanHinhAnhList where moTa is not null
        defaultLoaiChanDoanHinhAnhShouldBeFound("moTa.specified=true");

        // Get all the loaiChanDoanHinhAnhList where moTa is null
        defaultLoaiChanDoanHinhAnhShouldNotBeFound("moTa.specified=false");
    }
                @Test
    @Transactional
    public void getAllLoaiChanDoanHinhAnhsByMoTaContainsSomething() throws Exception {
        // Initialize the database
        loaiChanDoanHinhAnhRepository.saveAndFlush(loaiChanDoanHinhAnh);

        // Get all the loaiChanDoanHinhAnhList where moTa contains DEFAULT_MO_TA
        defaultLoaiChanDoanHinhAnhShouldBeFound("moTa.contains=" + DEFAULT_MO_TA);

        // Get all the loaiChanDoanHinhAnhList where moTa contains UPDATED_MO_TA
        defaultLoaiChanDoanHinhAnhShouldNotBeFound("moTa.contains=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    public void getAllLoaiChanDoanHinhAnhsByMoTaNotContainsSomething() throws Exception {
        // Initialize the database
        loaiChanDoanHinhAnhRepository.saveAndFlush(loaiChanDoanHinhAnh);

        // Get all the loaiChanDoanHinhAnhList where moTa does not contain DEFAULT_MO_TA
        defaultLoaiChanDoanHinhAnhShouldNotBeFound("moTa.doesNotContain=" + DEFAULT_MO_TA);

        // Get all the loaiChanDoanHinhAnhList where moTa does not contain UPDATED_MO_TA
        defaultLoaiChanDoanHinhAnhShouldBeFound("moTa.doesNotContain=" + UPDATED_MO_TA);
    }


    @Test
    @Transactional
    public void getAllLoaiChanDoanHinhAnhsByEnableIsEqualToSomething() throws Exception {
        // Initialize the database
        loaiChanDoanHinhAnhRepository.saveAndFlush(loaiChanDoanHinhAnh);

        // Get all the loaiChanDoanHinhAnhList where enable equals to DEFAULT_ENABLE
        defaultLoaiChanDoanHinhAnhShouldBeFound("enable.equals=" + DEFAULT_ENABLE);

        // Get all the loaiChanDoanHinhAnhList where enable equals to UPDATED_ENABLE
        defaultLoaiChanDoanHinhAnhShouldNotBeFound("enable.equals=" + UPDATED_ENABLE);
    }

    @Test
    @Transactional
    public void getAllLoaiChanDoanHinhAnhsByEnableIsNotEqualToSomething() throws Exception {
        // Initialize the database
        loaiChanDoanHinhAnhRepository.saveAndFlush(loaiChanDoanHinhAnh);

        // Get all the loaiChanDoanHinhAnhList where enable not equals to DEFAULT_ENABLE
        defaultLoaiChanDoanHinhAnhShouldNotBeFound("enable.notEquals=" + DEFAULT_ENABLE);

        // Get all the loaiChanDoanHinhAnhList where enable not equals to UPDATED_ENABLE
        defaultLoaiChanDoanHinhAnhShouldBeFound("enable.notEquals=" + UPDATED_ENABLE);
    }

    @Test
    @Transactional
    public void getAllLoaiChanDoanHinhAnhsByEnableIsInShouldWork() throws Exception {
        // Initialize the database
        loaiChanDoanHinhAnhRepository.saveAndFlush(loaiChanDoanHinhAnh);

        // Get all the loaiChanDoanHinhAnhList where enable in DEFAULT_ENABLE or UPDATED_ENABLE
        defaultLoaiChanDoanHinhAnhShouldBeFound("enable.in=" + DEFAULT_ENABLE + "," + UPDATED_ENABLE);

        // Get all the loaiChanDoanHinhAnhList where enable equals to UPDATED_ENABLE
        defaultLoaiChanDoanHinhAnhShouldNotBeFound("enable.in=" + UPDATED_ENABLE);
    }

    @Test
    @Transactional
    public void getAllLoaiChanDoanHinhAnhsByEnableIsNullOrNotNull() throws Exception {
        // Initialize the database
        loaiChanDoanHinhAnhRepository.saveAndFlush(loaiChanDoanHinhAnh);

        // Get all the loaiChanDoanHinhAnhList where enable is not null
        defaultLoaiChanDoanHinhAnhShouldBeFound("enable.specified=true");

        // Get all the loaiChanDoanHinhAnhList where enable is null
        defaultLoaiChanDoanHinhAnhShouldNotBeFound("enable.specified=false");
    }

    @Test
    @Transactional
    public void getAllLoaiChanDoanHinhAnhsByUuTienIsEqualToSomething() throws Exception {
        // Initialize the database
        loaiChanDoanHinhAnhRepository.saveAndFlush(loaiChanDoanHinhAnh);

        // Get all the loaiChanDoanHinhAnhList where uuTien equals to DEFAULT_UU_TIEN
        defaultLoaiChanDoanHinhAnhShouldBeFound("uuTien.equals=" + DEFAULT_UU_TIEN);

        // Get all the loaiChanDoanHinhAnhList where uuTien equals to UPDATED_UU_TIEN
        defaultLoaiChanDoanHinhAnhShouldNotBeFound("uuTien.equals=" + UPDATED_UU_TIEN);
    }

    @Test
    @Transactional
    public void getAllLoaiChanDoanHinhAnhsByUuTienIsNotEqualToSomething() throws Exception {
        // Initialize the database
        loaiChanDoanHinhAnhRepository.saveAndFlush(loaiChanDoanHinhAnh);

        // Get all the loaiChanDoanHinhAnhList where uuTien not equals to DEFAULT_UU_TIEN
        defaultLoaiChanDoanHinhAnhShouldNotBeFound("uuTien.notEquals=" + DEFAULT_UU_TIEN);

        // Get all the loaiChanDoanHinhAnhList where uuTien not equals to UPDATED_UU_TIEN
        defaultLoaiChanDoanHinhAnhShouldBeFound("uuTien.notEquals=" + UPDATED_UU_TIEN);
    }

    @Test
    @Transactional
    public void getAllLoaiChanDoanHinhAnhsByUuTienIsInShouldWork() throws Exception {
        // Initialize the database
        loaiChanDoanHinhAnhRepository.saveAndFlush(loaiChanDoanHinhAnh);

        // Get all the loaiChanDoanHinhAnhList where uuTien in DEFAULT_UU_TIEN or UPDATED_UU_TIEN
        defaultLoaiChanDoanHinhAnhShouldBeFound("uuTien.in=" + DEFAULT_UU_TIEN + "," + UPDATED_UU_TIEN);

        // Get all the loaiChanDoanHinhAnhList where uuTien equals to UPDATED_UU_TIEN
        defaultLoaiChanDoanHinhAnhShouldNotBeFound("uuTien.in=" + UPDATED_UU_TIEN);
    }

    @Test
    @Transactional
    public void getAllLoaiChanDoanHinhAnhsByUuTienIsNullOrNotNull() throws Exception {
        // Initialize the database
        loaiChanDoanHinhAnhRepository.saveAndFlush(loaiChanDoanHinhAnh);

        // Get all the loaiChanDoanHinhAnhList where uuTien is not null
        defaultLoaiChanDoanHinhAnhShouldBeFound("uuTien.specified=true");

        // Get all the loaiChanDoanHinhAnhList where uuTien is null
        defaultLoaiChanDoanHinhAnhShouldNotBeFound("uuTien.specified=false");
    }

    @Test
    @Transactional
    public void getAllLoaiChanDoanHinhAnhsByUuTienIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        loaiChanDoanHinhAnhRepository.saveAndFlush(loaiChanDoanHinhAnh);

        // Get all the loaiChanDoanHinhAnhList where uuTien is greater than or equal to DEFAULT_UU_TIEN
        defaultLoaiChanDoanHinhAnhShouldBeFound("uuTien.greaterThanOrEqual=" + DEFAULT_UU_TIEN);

        // Get all the loaiChanDoanHinhAnhList where uuTien is greater than or equal to UPDATED_UU_TIEN
        defaultLoaiChanDoanHinhAnhShouldNotBeFound("uuTien.greaterThanOrEqual=" + UPDATED_UU_TIEN);
    }

    @Test
    @Transactional
    public void getAllLoaiChanDoanHinhAnhsByUuTienIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        loaiChanDoanHinhAnhRepository.saveAndFlush(loaiChanDoanHinhAnh);

        // Get all the loaiChanDoanHinhAnhList where uuTien is less than or equal to DEFAULT_UU_TIEN
        defaultLoaiChanDoanHinhAnhShouldBeFound("uuTien.lessThanOrEqual=" + DEFAULT_UU_TIEN);

        // Get all the loaiChanDoanHinhAnhList where uuTien is less than or equal to SMALLER_UU_TIEN
        defaultLoaiChanDoanHinhAnhShouldNotBeFound("uuTien.lessThanOrEqual=" + SMALLER_UU_TIEN);
    }

    @Test
    @Transactional
    public void getAllLoaiChanDoanHinhAnhsByUuTienIsLessThanSomething() throws Exception {
        // Initialize the database
        loaiChanDoanHinhAnhRepository.saveAndFlush(loaiChanDoanHinhAnh);

        // Get all the loaiChanDoanHinhAnhList where uuTien is less than DEFAULT_UU_TIEN
        defaultLoaiChanDoanHinhAnhShouldNotBeFound("uuTien.lessThan=" + DEFAULT_UU_TIEN);

        // Get all the loaiChanDoanHinhAnhList where uuTien is less than UPDATED_UU_TIEN
        defaultLoaiChanDoanHinhAnhShouldBeFound("uuTien.lessThan=" + UPDATED_UU_TIEN);
    }

    @Test
    @Transactional
    public void getAllLoaiChanDoanHinhAnhsByUuTienIsGreaterThanSomething() throws Exception {
        // Initialize the database
        loaiChanDoanHinhAnhRepository.saveAndFlush(loaiChanDoanHinhAnh);

        // Get all the loaiChanDoanHinhAnhList where uuTien is greater than DEFAULT_UU_TIEN
        defaultLoaiChanDoanHinhAnhShouldNotBeFound("uuTien.greaterThan=" + DEFAULT_UU_TIEN);

        // Get all the loaiChanDoanHinhAnhList where uuTien is greater than SMALLER_UU_TIEN
        defaultLoaiChanDoanHinhAnhShouldBeFound("uuTien.greaterThan=" + SMALLER_UU_TIEN);
    }


    @Test
    @Transactional
    public void getAllLoaiChanDoanHinhAnhsByMaPhanLoaiIsEqualToSomething() throws Exception {
        // Initialize the database
        loaiChanDoanHinhAnhRepository.saveAndFlush(loaiChanDoanHinhAnh);

        // Get all the loaiChanDoanHinhAnhList where maPhanLoai equals to DEFAULT_MA_PHAN_LOAI
        defaultLoaiChanDoanHinhAnhShouldBeFound("maPhanLoai.equals=" + DEFAULT_MA_PHAN_LOAI);

        // Get all the loaiChanDoanHinhAnhList where maPhanLoai equals to UPDATED_MA_PHAN_LOAI
        defaultLoaiChanDoanHinhAnhShouldNotBeFound("maPhanLoai.equals=" + UPDATED_MA_PHAN_LOAI);
    }

    @Test
    @Transactional
    public void getAllLoaiChanDoanHinhAnhsByMaPhanLoaiIsNotEqualToSomething() throws Exception {
        // Initialize the database
        loaiChanDoanHinhAnhRepository.saveAndFlush(loaiChanDoanHinhAnh);

        // Get all the loaiChanDoanHinhAnhList where maPhanLoai not equals to DEFAULT_MA_PHAN_LOAI
        defaultLoaiChanDoanHinhAnhShouldNotBeFound("maPhanLoai.notEquals=" + DEFAULT_MA_PHAN_LOAI);

        // Get all the loaiChanDoanHinhAnhList where maPhanLoai not equals to UPDATED_MA_PHAN_LOAI
        defaultLoaiChanDoanHinhAnhShouldBeFound("maPhanLoai.notEquals=" + UPDATED_MA_PHAN_LOAI);
    }

    @Test
    @Transactional
    public void getAllLoaiChanDoanHinhAnhsByMaPhanLoaiIsInShouldWork() throws Exception {
        // Initialize the database
        loaiChanDoanHinhAnhRepository.saveAndFlush(loaiChanDoanHinhAnh);

        // Get all the loaiChanDoanHinhAnhList where maPhanLoai in DEFAULT_MA_PHAN_LOAI or UPDATED_MA_PHAN_LOAI
        defaultLoaiChanDoanHinhAnhShouldBeFound("maPhanLoai.in=" + DEFAULT_MA_PHAN_LOAI + "," + UPDATED_MA_PHAN_LOAI);

        // Get all the loaiChanDoanHinhAnhList where maPhanLoai equals to UPDATED_MA_PHAN_LOAI
        defaultLoaiChanDoanHinhAnhShouldNotBeFound("maPhanLoai.in=" + UPDATED_MA_PHAN_LOAI);
    }

    @Test
    @Transactional
    public void getAllLoaiChanDoanHinhAnhsByMaPhanLoaiIsNullOrNotNull() throws Exception {
        // Initialize the database
        loaiChanDoanHinhAnhRepository.saveAndFlush(loaiChanDoanHinhAnh);

        // Get all the loaiChanDoanHinhAnhList where maPhanLoai is not null
        defaultLoaiChanDoanHinhAnhShouldBeFound("maPhanLoai.specified=true");

        // Get all the loaiChanDoanHinhAnhList where maPhanLoai is null
        defaultLoaiChanDoanHinhAnhShouldNotBeFound("maPhanLoai.specified=false");
    }
                @Test
    @Transactional
    public void getAllLoaiChanDoanHinhAnhsByMaPhanLoaiContainsSomething() throws Exception {
        // Initialize the database
        loaiChanDoanHinhAnhRepository.saveAndFlush(loaiChanDoanHinhAnh);

        // Get all the loaiChanDoanHinhAnhList where maPhanLoai contains DEFAULT_MA_PHAN_LOAI
        defaultLoaiChanDoanHinhAnhShouldBeFound("maPhanLoai.contains=" + DEFAULT_MA_PHAN_LOAI);

        // Get all the loaiChanDoanHinhAnhList where maPhanLoai contains UPDATED_MA_PHAN_LOAI
        defaultLoaiChanDoanHinhAnhShouldNotBeFound("maPhanLoai.contains=" + UPDATED_MA_PHAN_LOAI);
    }

    @Test
    @Transactional
    public void getAllLoaiChanDoanHinhAnhsByMaPhanLoaiNotContainsSomething() throws Exception {
        // Initialize the database
        loaiChanDoanHinhAnhRepository.saveAndFlush(loaiChanDoanHinhAnh);

        // Get all the loaiChanDoanHinhAnhList where maPhanLoai does not contain DEFAULT_MA_PHAN_LOAI
        defaultLoaiChanDoanHinhAnhShouldNotBeFound("maPhanLoai.doesNotContain=" + DEFAULT_MA_PHAN_LOAI);

        // Get all the loaiChanDoanHinhAnhList where maPhanLoai does not contain UPDATED_MA_PHAN_LOAI
        defaultLoaiChanDoanHinhAnhShouldBeFound("maPhanLoai.doesNotContain=" + UPDATED_MA_PHAN_LOAI);
    }


    @Test
    @Transactional
    public void getAllLoaiChanDoanHinhAnhsByDonViIsEqualToSomething() throws Exception {
        // Get already existing entity
        DonVi donVi = loaiChanDoanHinhAnh.getDonVi();
        loaiChanDoanHinhAnhRepository.saveAndFlush(loaiChanDoanHinhAnh);
        Long donViId = donVi.getId();

        // Get all the loaiChanDoanHinhAnhList where donVi equals to donViId
        defaultLoaiChanDoanHinhAnhShouldBeFound("donViId.equals=" + donViId);

        // Get all the loaiChanDoanHinhAnhList where donVi equals to donViId + 1
        defaultLoaiChanDoanHinhAnhShouldNotBeFound("donViId.equals=" + (donViId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultLoaiChanDoanHinhAnhShouldBeFound(String filter) throws Exception {
        restLoaiChanDoanHinhAnhMockMvc.perform(get("/api/loai-chan-doan-hinh-anhs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(loaiChanDoanHinhAnh.getId().intValue())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].moTa").value(hasItem(DEFAULT_MO_TA)))
            .andExpect(jsonPath("$.[*].enable").value(hasItem(DEFAULT_ENABLE.booleanValue())))
            .andExpect(jsonPath("$.[*].uuTien").value(hasItem(DEFAULT_UU_TIEN)))
            .andExpect(jsonPath("$.[*].maPhanLoai").value(hasItem(DEFAULT_MA_PHAN_LOAI)));

        // Check, that the count call also returns 1
        restLoaiChanDoanHinhAnhMockMvc.perform(get("/api/loai-chan-doan-hinh-anhs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultLoaiChanDoanHinhAnhShouldNotBeFound(String filter) throws Exception {
        restLoaiChanDoanHinhAnhMockMvc.perform(get("/api/loai-chan-doan-hinh-anhs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restLoaiChanDoanHinhAnhMockMvc.perform(get("/api/loai-chan-doan-hinh-anhs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingLoaiChanDoanHinhAnh() throws Exception {
        // Get the loaiChanDoanHinhAnh
        restLoaiChanDoanHinhAnhMockMvc.perform(get("/api/loai-chan-doan-hinh-anhs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLoaiChanDoanHinhAnh() throws Exception {
        // Initialize the database
        loaiChanDoanHinhAnhRepository.saveAndFlush(loaiChanDoanHinhAnh);

        int databaseSizeBeforeUpdate = loaiChanDoanHinhAnhRepository.findAll().size();

        // Update the loaiChanDoanHinhAnh
        LoaiChanDoanHinhAnh updatedLoaiChanDoanHinhAnh = loaiChanDoanHinhAnhRepository.findById(loaiChanDoanHinhAnh.getId()).get();
        // Disconnect from session so that the updates on updatedLoaiChanDoanHinhAnh are not directly saved in db
        em.detach(updatedLoaiChanDoanHinhAnh);
        updatedLoaiChanDoanHinhAnh
            .ten(UPDATED_TEN)
            .moTa(UPDATED_MO_TA)
            .enable(UPDATED_ENABLE)
            .uuTien(UPDATED_UU_TIEN)
            .maPhanLoai(UPDATED_MA_PHAN_LOAI);
        LoaiChanDoanHinhAnhDTO loaiChanDoanHinhAnhDTO = loaiChanDoanHinhAnhMapper.toDto(updatedLoaiChanDoanHinhAnh);

        restLoaiChanDoanHinhAnhMockMvc.perform(put("/api/loai-chan-doan-hinh-anhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(loaiChanDoanHinhAnhDTO)))
            .andExpect(status().isOk());

        // Validate the LoaiChanDoanHinhAnh in the database
        List<LoaiChanDoanHinhAnh> loaiChanDoanHinhAnhList = loaiChanDoanHinhAnhRepository.findAll();
        assertThat(loaiChanDoanHinhAnhList).hasSize(databaseSizeBeforeUpdate);
        LoaiChanDoanHinhAnh testLoaiChanDoanHinhAnh = loaiChanDoanHinhAnhList.get(loaiChanDoanHinhAnhList.size() - 1);
        assertThat(testLoaiChanDoanHinhAnh.getTen()).isEqualTo(UPDATED_TEN);
        assertThat(testLoaiChanDoanHinhAnh.getMoTa()).isEqualTo(UPDATED_MO_TA);
        assertThat(testLoaiChanDoanHinhAnh.isEnable()).isEqualTo(UPDATED_ENABLE);
        assertThat(testLoaiChanDoanHinhAnh.getUuTien()).isEqualTo(UPDATED_UU_TIEN);
        assertThat(testLoaiChanDoanHinhAnh.getMaPhanLoai()).isEqualTo(UPDATED_MA_PHAN_LOAI);
    }

    @Test
    @Transactional
    public void updateNonExistingLoaiChanDoanHinhAnh() throws Exception {
        int databaseSizeBeforeUpdate = loaiChanDoanHinhAnhRepository.findAll().size();

        // Create the LoaiChanDoanHinhAnh
        LoaiChanDoanHinhAnhDTO loaiChanDoanHinhAnhDTO = loaiChanDoanHinhAnhMapper.toDto(loaiChanDoanHinhAnh);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLoaiChanDoanHinhAnhMockMvc.perform(put("/api/loai-chan-doan-hinh-anhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(loaiChanDoanHinhAnhDTO)))
            .andExpect(status().isBadRequest());

        // Validate the LoaiChanDoanHinhAnh in the database
        List<LoaiChanDoanHinhAnh> loaiChanDoanHinhAnhList = loaiChanDoanHinhAnhRepository.findAll();
        assertThat(loaiChanDoanHinhAnhList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteLoaiChanDoanHinhAnh() throws Exception {
        // Initialize the database
        loaiChanDoanHinhAnhRepository.saveAndFlush(loaiChanDoanHinhAnh);

        int databaseSizeBeforeDelete = loaiChanDoanHinhAnhRepository.findAll().size();

        // Delete the loaiChanDoanHinhAnh
        restLoaiChanDoanHinhAnhMockMvc.perform(delete("/api/loai-chan-doan-hinh-anhs/{id}", loaiChanDoanHinhAnh.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<LoaiChanDoanHinhAnh> loaiChanDoanHinhAnhList = loaiChanDoanHinhAnhRepository.findAll();
        assertThat(loaiChanDoanHinhAnhList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.DonVi;
import vn.vnpt.repository.DonViRepository;
import vn.vnpt.service.DonViService;
import vn.vnpt.service.dto.DonViDTO;
import vn.vnpt.service.mapper.DonViMapper;
import vn.vnpt.service.dto.DonViCriteria;
import vn.vnpt.service.DonViQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link DonViResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class DonViResourceIT {

    public static final Integer DEFAULT_CAP = 1;
    public static final Integer UPDATED_CAP = 2;
    public static final Integer SMALLER_CAP = 1 - 1;

    public static final String DEFAULT_CHI_NHANH_NGAN_HANG = "AAAAAAAAAA";
    public static final String UPDATED_CHI_NHANH_NGAN_HANG = "BBBBBBBBBB";

    public static final String DEFAULT_DIA_CHI = "AAAAAAAAAA";
    public static final String UPDATED_DIA_CHI = "BBBBBBBBBB";

    public static final BigDecimal DEFAULT_DON_VI_QUAN_LY_ID = new BigDecimal(1);
    public static final BigDecimal UPDATED_DON_VI_QUAN_LY_ID = new BigDecimal(2);
    public static final BigDecimal SMALLER_DON_VI_QUAN_LY_ID = new BigDecimal(1 - 1);

    public static final String DEFAULT_DVTT = "AAAAAAAAAA";
    public static final String UPDATED_DVTT = "BBBBBBBBBB";

    public static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    public static final String UPDATED_EMAIL = "BBBBBBBBBB";

    public static final Boolean DEFAULT_ENABLED = false;
    public static final Boolean UPDATED_ENABLED = true;

    public static final String DEFAULT_KY_HIEU = "AAAAAAAAAA";
    public static final String UPDATED_KY_HIEU = "BBBBBBBBBB";

    public static final Integer DEFAULT_LOAI = 1;
    public static final Integer UPDATED_LOAI = 2;
    public static final Integer SMALLER_LOAI = 1 - 1;

    public static final String DEFAULT_MA_TINH = "AAAAAAAAAA";
    public static final String UPDATED_MA_TINH = "BBBBBBBBBB";

    public static final String DEFAULT_PHONE = "AAAAAAAAAA";
    public static final String UPDATED_PHONE = "BBBBBBBBBB";

    public static final String DEFAULT_SO_TAI_KHOAN = "AAAAAAAAAA";
    public static final String UPDATED_SO_TAI_KHOAN = "BBBBBBBBBB";

    public static final String DEFAULT_TEN = "AAAAAAAAAA";
    public static final String UPDATED_TEN = "BBBBBBBBBB";

    public static final String DEFAULT_TEN_NGAN_HANG = "AAAAAAAAAA";
    public static final String UPDATED_TEN_NGAN_HANG = "BBBBBBBBBB";

    @Autowired
    private DonViRepository donViRepository;

    @Autowired
    private DonViMapper donViMapper;

    @Autowired
    private DonViService donViService;

    @Autowired
    private DonViQueryService donViQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDonViMockMvc;

    private DonVi donVi;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DonVi createEntity(EntityManager em) {
        DonVi donVi = new DonVi()
            .cap(DEFAULT_CAP)
            .chiNhanhNganHang(DEFAULT_CHI_NHANH_NGAN_HANG)
            .diaChi(DEFAULT_DIA_CHI)
            .donViQuanLyId(DEFAULT_DON_VI_QUAN_LY_ID)
            .dvtt(DEFAULT_DVTT)
            .email(DEFAULT_EMAIL)
            .enabled(DEFAULT_ENABLED)
            .kyHieu(DEFAULT_KY_HIEU)
            .loai(DEFAULT_LOAI)
            .maTinh(DEFAULT_MA_TINH)
            .phone(DEFAULT_PHONE)
            .soTaiKhoan(DEFAULT_SO_TAI_KHOAN)
            .ten(DEFAULT_TEN)
            .tenNganHang(DEFAULT_TEN_NGAN_HANG);
        return donVi;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DonVi createUpdatedEntity(EntityManager em) {
        DonVi donVi = new DonVi()
            .cap(UPDATED_CAP)
            .chiNhanhNganHang(UPDATED_CHI_NHANH_NGAN_HANG)
            .diaChi(UPDATED_DIA_CHI)
            .donViQuanLyId(UPDATED_DON_VI_QUAN_LY_ID)
            .dvtt(UPDATED_DVTT)
            .email(UPDATED_EMAIL)
            .enabled(UPDATED_ENABLED)
            .kyHieu(UPDATED_KY_HIEU)
            .loai(UPDATED_LOAI)
            .maTinh(UPDATED_MA_TINH)
            .phone(UPDATED_PHONE)
            .soTaiKhoan(UPDATED_SO_TAI_KHOAN)
            .ten(UPDATED_TEN)
            .tenNganHang(UPDATED_TEN_NGAN_HANG);
        return donVi;
    }

    @BeforeEach
    public void initTest() {
        donVi = createEntity(em);
    }

    @Test
    @Transactional
    public void createDonVi() throws Exception {
        int databaseSizeBeforeCreate = donViRepository.findAll().size();

        // Create the DonVi
        DonViDTO donViDTO = donViMapper.toDto(donVi);
        restDonViMockMvc.perform(post("/api/don-vis").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(donViDTO)))
            .andExpect(status().isCreated());

        // Validate the DonVi in the database
        List<DonVi> donViList = donViRepository.findAll();
        assertThat(donViList).hasSize(databaseSizeBeforeCreate + 1);
        DonVi testDonVi = donViList.get(donViList.size() - 1);
        assertThat(testDonVi.getCap()).isEqualTo(DEFAULT_CAP);
        assertThat(testDonVi.getChiNhanhNganHang()).isEqualTo(DEFAULT_CHI_NHANH_NGAN_HANG);
        assertThat(testDonVi.getDiaChi()).isEqualTo(DEFAULT_DIA_CHI);
        assertThat(testDonVi.getDonViQuanLyId()).isEqualTo(DEFAULT_DON_VI_QUAN_LY_ID);
        assertThat(testDonVi.getDvtt()).isEqualTo(DEFAULT_DVTT);
        assertThat(testDonVi.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testDonVi.isEnabled()).isEqualTo(DEFAULT_ENABLED);
        assertThat(testDonVi.getKyHieu()).isEqualTo(DEFAULT_KY_HIEU);
        assertThat(testDonVi.getLoai()).isEqualTo(DEFAULT_LOAI);
        assertThat(testDonVi.getMaTinh()).isEqualTo(DEFAULT_MA_TINH);
        assertThat(testDonVi.getPhone()).isEqualTo(DEFAULT_PHONE);
        assertThat(testDonVi.getSoTaiKhoan()).isEqualTo(DEFAULT_SO_TAI_KHOAN);
        assertThat(testDonVi.getTen()).isEqualTo(DEFAULT_TEN);
        assertThat(testDonVi.getTenNganHang()).isEqualTo(DEFAULT_TEN_NGAN_HANG);
    }

    @Test
    @Transactional
    public void createDonViWithExistingId() throws Exception {
        donViRepository.save(donVi);
        int databaseSizeBeforeCreate = donViRepository.findAll().size();

        // Create the DonVi with an existing ID
        donVi.setId(donVi.getId());
        DonViDTO donViDTO = donViMapper.toDto(donVi);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDonViMockMvc.perform(post("/api/don-vis").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(donViDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DonVi in the database
        List<DonVi> donViList = donViRepository.findAll();
        assertThat(donViList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkCapIsRequired() throws Exception {
        int databaseSizeBeforeTest = donViRepository.findAll().size();
        // set the field null
        donVi.setCap(null);

        // Create the DonVi, which fails.
        DonViDTO donViDTO = donViMapper.toDto(donVi);

        restDonViMockMvc.perform(post("/api/don-vis").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(donViDTO)))
            .andExpect(status().isBadRequest());

        List<DonVi> donViList = donViRepository.findAll();
        assertThat(donViList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEnabledIsRequired() throws Exception {
        int databaseSizeBeforeTest = donViRepository.findAll().size();
        // set the field null
        donVi.setEnabled(null);

        // Create the DonVi, which fails.
        DonViDTO donViDTO = donViMapper.toDto(donVi);

        restDonViMockMvc.perform(post("/api/don-vis").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(donViDTO)))
            .andExpect(status().isBadRequest());

        List<DonVi> donViList = donViRepository.findAll();
        assertThat(donViList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLoaiIsRequired() throws Exception {
        int databaseSizeBeforeTest = donViRepository.findAll().size();
        // set the field null
        donVi.setLoai(null);

        // Create the DonVi, which fails.
        DonViDTO donViDTO = donViMapper.toDto(donVi);

        restDonViMockMvc.perform(post("/api/don-vis").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(donViDTO)))
            .andExpect(status().isBadRequest());

        List<DonVi> donViList = donViRepository.findAll();
        assertThat(donViList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkMaTinhIsRequired() throws Exception {
        int databaseSizeBeforeTest = donViRepository.findAll().size();
        // set the field null
        donVi.setMaTinh(null);

        // Create the DonVi, which fails.
        DonViDTO donViDTO = donViMapper.toDto(donVi);

        restDonViMockMvc.perform(post("/api/don-vis").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(donViDTO)))
            .andExpect(status().isBadRequest());

        List<DonVi> donViList = donViRepository.findAll();
        assertThat(donViList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTenIsRequired() throws Exception {
        int databaseSizeBeforeTest = donViRepository.findAll().size();
        // set the field null
        donVi.setTen(null);

        // Create the DonVi, which fails.
        DonViDTO donViDTO = donViMapper.toDto(donVi);

        restDonViMockMvc.perform(post("/api/don-vis").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(donViDTO)))
            .andExpect(status().isBadRequest());

        List<DonVi> donViList = donViRepository.findAll();
        assertThat(donViList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDonVis() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList
        restDonViMockMvc.perform(get("/api/don-vis"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(donVi.getId().intValue())))
            .andExpect(jsonPath("$.[*].cap").value(hasItem(DEFAULT_CAP)))
            .andExpect(jsonPath("$.[*].chiNhanhNganHang").value(hasItem(DEFAULT_CHI_NHANH_NGAN_HANG)))
            .andExpect(jsonPath("$.[*].diaChi").value(hasItem(DEFAULT_DIA_CHI)))
            .andExpect(jsonPath("$.[*].donViQuanLyId").value(hasItem(DEFAULT_DON_VI_QUAN_LY_ID.intValue())))
            .andExpect(jsonPath("$.[*].dvtt").value(hasItem(DEFAULT_DVTT)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].enabled").value(hasItem(DEFAULT_ENABLED.booleanValue())))
            .andExpect(jsonPath("$.[*].kyHieu").value(hasItem(DEFAULT_KY_HIEU)))
            .andExpect(jsonPath("$.[*].loai").value(hasItem(DEFAULT_LOAI)))
            .andExpect(jsonPath("$.[*].maTinh").value(hasItem(DEFAULT_MA_TINH)))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE)))
            .andExpect(jsonPath("$.[*].soTaiKhoan").value(hasItem(DEFAULT_SO_TAI_KHOAN)))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].tenNganHang").value(hasItem(DEFAULT_TEN_NGAN_HANG)));
    }

    @Test
    @Transactional
    public void getDonVi() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get the donVi
        restDonViMockMvc.perform(get("/api/don-vis/{id}", donVi.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(donVi.getId().intValue()))
            .andExpect(jsonPath("$.cap").value(DEFAULT_CAP))
            .andExpect(jsonPath("$.chiNhanhNganHang").value(DEFAULT_CHI_NHANH_NGAN_HANG))
            .andExpect(jsonPath("$.diaChi").value(DEFAULT_DIA_CHI))
            .andExpect(jsonPath("$.donViQuanLyId").value(DEFAULT_DON_VI_QUAN_LY_ID.intValue()))
            .andExpect(jsonPath("$.dvtt").value(DEFAULT_DVTT))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.enabled").value(DEFAULT_ENABLED.booleanValue()))
            .andExpect(jsonPath("$.kyHieu").value(DEFAULT_KY_HIEU))
            .andExpect(jsonPath("$.loai").value(DEFAULT_LOAI))
            .andExpect(jsonPath("$.maTinh").value(DEFAULT_MA_TINH))
            .andExpect(jsonPath("$.phone").value(DEFAULT_PHONE))
            .andExpect(jsonPath("$.soTaiKhoan").value(DEFAULT_SO_TAI_KHOAN))
            .andExpect(jsonPath("$.ten").value(DEFAULT_TEN))
            .andExpect(jsonPath("$.tenNganHang").value(DEFAULT_TEN_NGAN_HANG));
    }

    @Test
    @Transactional
    public void getDonVisByIdFiltering() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        Long id = donVi.getId();

        defaultDonViShouldBeFound("id.equals=" + id);
        defaultDonViShouldNotBeFound("id.notEquals=" + id);

        defaultDonViShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultDonViShouldNotBeFound("id.greaterThan=" + id);

        defaultDonViShouldBeFound("id.lessThanOrEqual=" + id);
        defaultDonViShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    public void getAllDonVisByCapIsEqualToSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where cap equals to DEFAULT_CAP
        defaultDonViShouldBeFound("cap.equals=" + DEFAULT_CAP);

        // Get all the donViList where cap equals to UPDATED_CAP
        defaultDonViShouldNotBeFound("cap.equals=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    public void getAllDonVisByCapIsNotEqualToSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where cap not equals to DEFAULT_CAP
        defaultDonViShouldNotBeFound("cap.notEquals=" + DEFAULT_CAP);

        // Get all the donViList where cap not equals to UPDATED_CAP
        defaultDonViShouldBeFound("cap.notEquals=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    public void getAllDonVisByCapIsInShouldWork() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where cap in DEFAULT_CAP or UPDATED_CAP
        defaultDonViShouldBeFound("cap.in=" + DEFAULT_CAP + "," + UPDATED_CAP);

        // Get all the donViList where cap equals to UPDATED_CAP
        defaultDonViShouldNotBeFound("cap.in=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    public void getAllDonVisByCapIsNullOrNotNull() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where cap is not null
        defaultDonViShouldBeFound("cap.specified=true");

        // Get all the donViList where cap is null
        defaultDonViShouldNotBeFound("cap.specified=false");
    }

    @Test
    @Transactional
    public void getAllDonVisByCapIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where cap is greater than or equal to DEFAULT_CAP
        defaultDonViShouldBeFound("cap.greaterThanOrEqual=" + DEFAULT_CAP);

        // Get all the donViList where cap is greater than or equal to UPDATED_CAP
        defaultDonViShouldNotBeFound("cap.greaterThanOrEqual=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    public void getAllDonVisByCapIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where cap is less than or equal to DEFAULT_CAP
        defaultDonViShouldBeFound("cap.lessThanOrEqual=" + DEFAULT_CAP);

        // Get all the donViList where cap is less than or equal to SMALLER_CAP
        defaultDonViShouldNotBeFound("cap.lessThanOrEqual=" + SMALLER_CAP);
    }

    @Test
    @Transactional
    public void getAllDonVisByCapIsLessThanSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where cap is less than DEFAULT_CAP
        defaultDonViShouldNotBeFound("cap.lessThan=" + DEFAULT_CAP);

        // Get all the donViList where cap is less than UPDATED_CAP
        defaultDonViShouldBeFound("cap.lessThan=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    public void getAllDonVisByCapIsGreaterThanSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where cap is greater than DEFAULT_CAP
        defaultDonViShouldNotBeFound("cap.greaterThan=" + DEFAULT_CAP);

        // Get all the donViList where cap is greater than SMALLER_CAP
        defaultDonViShouldBeFound("cap.greaterThan=" + SMALLER_CAP);
    }

    @Test
    @Transactional
    public void getAllDonVisByChiNhanhNganHangIsEqualToSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where chiNhanhNganHang equals to DEFAULT_CHI_NHANH_NGAN_HANG
        defaultDonViShouldBeFound("chiNhanhNganHang.equals=" + DEFAULT_CHI_NHANH_NGAN_HANG);

        // Get all the donViList where chiNhanhNganHang equals to UPDATED_CHI_NHANH_NGAN_HANG
        defaultDonViShouldNotBeFound("chiNhanhNganHang.equals=" + UPDATED_CHI_NHANH_NGAN_HANG);
    }

    @Test
    @Transactional
    public void getAllDonVisByChiNhanhNganHangIsNotEqualToSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where chiNhanhNganHang not equals to DEFAULT_CHI_NHANH_NGAN_HANG
        defaultDonViShouldNotBeFound("chiNhanhNganHang.notEquals=" + DEFAULT_CHI_NHANH_NGAN_HANG);

        // Get all the donViList where chiNhanhNganHang not equals to UPDATED_CHI_NHANH_NGAN_HANG
        defaultDonViShouldBeFound("chiNhanhNganHang.notEquals=" + UPDATED_CHI_NHANH_NGAN_HANG);
    }

    @Test
    @Transactional
    public void getAllDonVisByChiNhanhNganHangIsInShouldWork() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where chiNhanhNganHang in DEFAULT_CHI_NHANH_NGAN_HANG or UPDATED_CHI_NHANH_NGAN_HANG
        defaultDonViShouldBeFound("chiNhanhNganHang.in=" + DEFAULT_CHI_NHANH_NGAN_HANG + "," + UPDATED_CHI_NHANH_NGAN_HANG);

        // Get all the donViList where chiNhanhNganHang equals to UPDATED_CHI_NHANH_NGAN_HANG
        defaultDonViShouldNotBeFound("chiNhanhNganHang.in=" + UPDATED_CHI_NHANH_NGAN_HANG);
    }

    @Test
    @Transactional
    public void getAllDonVisByChiNhanhNganHangIsNullOrNotNull() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where chiNhanhNganHang is not null
        defaultDonViShouldBeFound("chiNhanhNganHang.specified=true");

        // Get all the donViList where chiNhanhNganHang is null
        defaultDonViShouldNotBeFound("chiNhanhNganHang.specified=false");
    }

    @Test
    @Transactional
    public void getAllDonVisByChiNhanhNganHangContainsSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where chiNhanhNganHang contains DEFAULT_CHI_NHANH_NGAN_HANG
        defaultDonViShouldBeFound("chiNhanhNganHang.contains=" + DEFAULT_CHI_NHANH_NGAN_HANG);

        // Get all the donViList where chiNhanhNganHang contains UPDATED_CHI_NHANH_NGAN_HANG
        defaultDonViShouldNotBeFound("chiNhanhNganHang.contains=" + UPDATED_CHI_NHANH_NGAN_HANG);
    }

    @Test
    @Transactional
    public void getAllDonVisByChiNhanhNganHangNotContainsSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where chiNhanhNganHang does not contain DEFAULT_CHI_NHANH_NGAN_HANG
        defaultDonViShouldNotBeFound("chiNhanhNganHang.doesNotContain=" + DEFAULT_CHI_NHANH_NGAN_HANG);

        // Get all the donViList where chiNhanhNganHang does not contain UPDATED_CHI_NHANH_NGAN_HANG
        defaultDonViShouldBeFound("chiNhanhNganHang.doesNotContain=" + UPDATED_CHI_NHANH_NGAN_HANG);
    }

    @Test
    @Transactional
    public void getAllDonVisByDiaChiIsEqualToSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where diaChi equals to DEFAULT_DIA_CHI
        defaultDonViShouldBeFound("diaChi.equals=" + DEFAULT_DIA_CHI);

        // Get all the donViList where diaChi equals to UPDATED_DIA_CHI
        defaultDonViShouldNotBeFound("diaChi.equals=" + UPDATED_DIA_CHI);
    }

    @Test
    @Transactional
    public void getAllDonVisByDiaChiIsNotEqualToSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where diaChi not equals to DEFAULT_DIA_CHI
        defaultDonViShouldNotBeFound("diaChi.notEquals=" + DEFAULT_DIA_CHI);

        // Get all the donViList where diaChi not equals to UPDATED_DIA_CHI
        defaultDonViShouldBeFound("diaChi.notEquals=" + UPDATED_DIA_CHI);
    }

    @Test
    @Transactional
    public void getAllDonVisByDiaChiIsInShouldWork() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where diaChi in DEFAULT_DIA_CHI or UPDATED_DIA_CHI
        defaultDonViShouldBeFound("diaChi.in=" + DEFAULT_DIA_CHI + "," + UPDATED_DIA_CHI);

        // Get all the donViList where diaChi equals to UPDATED_DIA_CHI
        defaultDonViShouldNotBeFound("diaChi.in=" + UPDATED_DIA_CHI);
    }

    @Test
    @Transactional
    public void getAllDonVisByDiaChiIsNullOrNotNull() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where diaChi is not null
        defaultDonViShouldBeFound("diaChi.specified=true");

        // Get all the donViList where diaChi is null
        defaultDonViShouldNotBeFound("diaChi.specified=false");
    }

    @Test
    @Transactional
    public void getAllDonVisByDiaChiContainsSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where diaChi contains DEFAULT_DIA_CHI
        defaultDonViShouldBeFound("diaChi.contains=" + DEFAULT_DIA_CHI);

        // Get all the donViList where diaChi contains UPDATED_DIA_CHI
        defaultDonViShouldNotBeFound("diaChi.contains=" + UPDATED_DIA_CHI);
    }

    @Test
    @Transactional
    public void getAllDonVisByDiaChiNotContainsSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where diaChi does not contain DEFAULT_DIA_CHI
        defaultDonViShouldNotBeFound("diaChi.doesNotContain=" + DEFAULT_DIA_CHI);

        // Get all the donViList where diaChi does not contain UPDATED_DIA_CHI
        defaultDonViShouldBeFound("diaChi.doesNotContain=" + UPDATED_DIA_CHI);
    }

    @Test
    @Transactional
    public void getAllDonVisByDonViQuanLyIdIsEqualToSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where donViQuanLyId equals to DEFAULT_DON_VI_QUAN_LY_ID
        defaultDonViShouldBeFound("donViQuanLyId.equals=" + DEFAULT_DON_VI_QUAN_LY_ID);

        // Get all the donViList where donViQuanLyId equals to UPDATED_DON_VI_QUAN_LY_ID
        defaultDonViShouldNotBeFound("donViQuanLyId.equals=" + UPDATED_DON_VI_QUAN_LY_ID);
    }

    @Test
    @Transactional
    public void getAllDonVisByDonViQuanLyIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where donViQuanLyId not equals to DEFAULT_DON_VI_QUAN_LY_ID
        defaultDonViShouldNotBeFound("donViQuanLyId.notEquals=" + DEFAULT_DON_VI_QUAN_LY_ID);

        // Get all the donViList where donViQuanLyId not equals to UPDATED_DON_VI_QUAN_LY_ID
        defaultDonViShouldBeFound("donViQuanLyId.notEquals=" + UPDATED_DON_VI_QUAN_LY_ID);
    }

    @Test
    @Transactional
    public void getAllDonVisByDonViQuanLyIdIsInShouldWork() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where donViQuanLyId in DEFAULT_DON_VI_QUAN_LY_ID or UPDATED_DON_VI_QUAN_LY_ID
        defaultDonViShouldBeFound("donViQuanLyId.in=" + DEFAULT_DON_VI_QUAN_LY_ID + "," + UPDATED_DON_VI_QUAN_LY_ID);

        // Get all the donViList where donViQuanLyId equals to UPDATED_DON_VI_QUAN_LY_ID
        defaultDonViShouldNotBeFound("donViQuanLyId.in=" + UPDATED_DON_VI_QUAN_LY_ID);
    }

    @Test
    @Transactional
    public void getAllDonVisByDonViQuanLyIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where donViQuanLyId is not null
        defaultDonViShouldBeFound("donViQuanLyId.specified=true");

        // Get all the donViList where donViQuanLyId is null
        defaultDonViShouldNotBeFound("donViQuanLyId.specified=false");
    }

    @Test
    @Transactional
    public void getAllDonVisByDonViQuanLyIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where donViQuanLyId is greater than or equal to DEFAULT_DON_VI_QUAN_LY_ID
        defaultDonViShouldBeFound("donViQuanLyId.greaterThanOrEqual=" + DEFAULT_DON_VI_QUAN_LY_ID);

        // Get all the donViList where donViQuanLyId is greater than or equal to UPDATED_DON_VI_QUAN_LY_ID
        defaultDonViShouldNotBeFound("donViQuanLyId.greaterThanOrEqual=" + UPDATED_DON_VI_QUAN_LY_ID);
    }

    @Test
    @Transactional
    public void getAllDonVisByDonViQuanLyIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where donViQuanLyId is less than or equal to DEFAULT_DON_VI_QUAN_LY_ID
        defaultDonViShouldBeFound("donViQuanLyId.lessThanOrEqual=" + DEFAULT_DON_VI_QUAN_LY_ID);

        // Get all the donViList where donViQuanLyId is less than or equal to SMALLER_DON_VI_QUAN_LY_ID
        defaultDonViShouldNotBeFound("donViQuanLyId.lessThanOrEqual=" + SMALLER_DON_VI_QUAN_LY_ID);
    }

    @Test
    @Transactional
    public void getAllDonVisByDonViQuanLyIdIsLessThanSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where donViQuanLyId is less than DEFAULT_DON_VI_QUAN_LY_ID
        defaultDonViShouldNotBeFound("donViQuanLyId.lessThan=" + DEFAULT_DON_VI_QUAN_LY_ID);

        // Get all the donViList where donViQuanLyId is less than UPDATED_DON_VI_QUAN_LY_ID
        defaultDonViShouldBeFound("donViQuanLyId.lessThan=" + UPDATED_DON_VI_QUAN_LY_ID);
    }

    @Test
    @Transactional
    public void getAllDonVisByDonViQuanLyIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where donViQuanLyId is greater than DEFAULT_DON_VI_QUAN_LY_ID
        defaultDonViShouldNotBeFound("donViQuanLyId.greaterThan=" + DEFAULT_DON_VI_QUAN_LY_ID);

        // Get all the donViList where donViQuanLyId is greater than SMALLER_DON_VI_QUAN_LY_ID
        defaultDonViShouldBeFound("donViQuanLyId.greaterThan=" + SMALLER_DON_VI_QUAN_LY_ID);
    }

    @Test
    @Transactional
    public void getAllDonVisByDvttIsEqualToSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where dvtt equals to DEFAULT_DVTT
        defaultDonViShouldBeFound("dvtt.equals=" + DEFAULT_DVTT);

        // Get all the donViList where dvtt equals to UPDATED_DVTT
        defaultDonViShouldNotBeFound("dvtt.equals=" + UPDATED_DVTT);
    }

    @Test
    @Transactional
    public void getAllDonVisByDvttIsNotEqualToSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where dvtt not equals to DEFAULT_DVTT
        defaultDonViShouldNotBeFound("dvtt.notEquals=" + DEFAULT_DVTT);

        // Get all the donViList where dvtt not equals to UPDATED_DVTT
        defaultDonViShouldBeFound("dvtt.notEquals=" + UPDATED_DVTT);
    }

    @Test
    @Transactional
    public void getAllDonVisByDvttIsInShouldWork() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where dvtt in DEFAULT_DVTT or UPDATED_DVTT
        defaultDonViShouldBeFound("dvtt.in=" + DEFAULT_DVTT + "," + UPDATED_DVTT);

        // Get all the donViList where dvtt equals to UPDATED_DVTT
        defaultDonViShouldNotBeFound("dvtt.in=" + UPDATED_DVTT);
    }

    @Test
    @Transactional
    public void getAllDonVisByDvttIsNullOrNotNull() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where dvtt is not null
        defaultDonViShouldBeFound("dvtt.specified=true");

        // Get all the donViList where dvtt is null
        defaultDonViShouldNotBeFound("dvtt.specified=false");
    }

    @Test
    @Transactional
    public void getAllDonVisByDvttContainsSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where dvtt contains DEFAULT_DVTT
        defaultDonViShouldBeFound("dvtt.contains=" + DEFAULT_DVTT);

        // Get all the donViList where dvtt contains UPDATED_DVTT
        defaultDonViShouldNotBeFound("dvtt.contains=" + UPDATED_DVTT);
    }

    @Test
    @Transactional
    public void getAllDonVisByDvttNotContainsSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where dvtt does not contain DEFAULT_DVTT
        defaultDonViShouldNotBeFound("dvtt.doesNotContain=" + DEFAULT_DVTT);

        // Get all the donViList where dvtt does not contain UPDATED_DVTT
        defaultDonViShouldBeFound("dvtt.doesNotContain=" + UPDATED_DVTT);
    }

    @Test
    @Transactional
    public void getAllDonVisByEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where email equals to DEFAULT_EMAIL
        defaultDonViShouldBeFound("email.equals=" + DEFAULT_EMAIL);

        // Get all the donViList where email equals to UPDATED_EMAIL
        defaultDonViShouldNotBeFound("email.equals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllDonVisByEmailIsNotEqualToSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where email not equals to DEFAULT_EMAIL
        defaultDonViShouldNotBeFound("email.notEquals=" + DEFAULT_EMAIL);

        // Get all the donViList where email not equals to UPDATED_EMAIL
        defaultDonViShouldBeFound("email.notEquals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllDonVisByEmailIsInShouldWork() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where email in DEFAULT_EMAIL or UPDATED_EMAIL
        defaultDonViShouldBeFound("email.in=" + DEFAULT_EMAIL + "," + UPDATED_EMAIL);

        // Get all the donViList where email equals to UPDATED_EMAIL
        defaultDonViShouldNotBeFound("email.in=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllDonVisByEmailIsNullOrNotNull() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where email is not null
        defaultDonViShouldBeFound("email.specified=true");

        // Get all the donViList where email is null
        defaultDonViShouldNotBeFound("email.specified=false");
    }

    @Test
    @Transactional
    public void getAllDonVisByEmailContainsSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where email contains DEFAULT_EMAIL
        defaultDonViShouldBeFound("email.contains=" + DEFAULT_EMAIL);

        // Get all the donViList where email contains UPDATED_EMAIL
        defaultDonViShouldNotBeFound("email.contains=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllDonVisByEmailNotContainsSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where email does not contain DEFAULT_EMAIL
        defaultDonViShouldNotBeFound("email.doesNotContain=" + DEFAULT_EMAIL);

        // Get all the donViList where email does not contain UPDATED_EMAIL
        defaultDonViShouldBeFound("email.doesNotContain=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllDonVisByEnabledIsEqualToSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where enabled equals to DEFAULT_ENABLED
        defaultDonViShouldBeFound("enabled.equals=" + DEFAULT_ENABLED);

        // Get all the donViList where enabled equals to UPDATED_ENABLED
        defaultDonViShouldNotBeFound("enabled.equals=" + UPDATED_ENABLED);
    }

    @Test
    @Transactional
    public void getAllDonVisByEnabledIsNotEqualToSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where enabled not equals to DEFAULT_ENABLED
        defaultDonViShouldNotBeFound("enabled.notEquals=" + DEFAULT_ENABLED);

        // Get all the donViList where enabled not equals to UPDATED_ENABLED
        defaultDonViShouldBeFound("enabled.notEquals=" + UPDATED_ENABLED);
    }

    @Test
    @Transactional
    public void getAllDonVisByEnabledIsInShouldWork() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where enabled in DEFAULT_ENABLED or UPDATED_ENABLED
        defaultDonViShouldBeFound("enabled.in=" + DEFAULT_ENABLED + "," + UPDATED_ENABLED);

        // Get all the donViList where enabled equals to UPDATED_ENABLED
        defaultDonViShouldNotBeFound("enabled.in=" + UPDATED_ENABLED);
    }

    @Test
    @Transactional
    public void getAllDonVisByEnabledIsNullOrNotNull() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where enabled is not null
        defaultDonViShouldBeFound("enabled.specified=true");

        // Get all the donViList where enabled is null
        defaultDonViShouldNotBeFound("enabled.specified=false");
    }

    @Test
    @Transactional
    public void getAllDonVisByKyHieuIsEqualToSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where kyHieu equals to DEFAULT_KY_HIEU
        defaultDonViShouldBeFound("kyHieu.equals=" + DEFAULT_KY_HIEU);

        // Get all the donViList where kyHieu equals to UPDATED_KY_HIEU
        defaultDonViShouldNotBeFound("kyHieu.equals=" + UPDATED_KY_HIEU);
    }

    @Test
    @Transactional
    public void getAllDonVisByKyHieuIsNotEqualToSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where kyHieu not equals to DEFAULT_KY_HIEU
        defaultDonViShouldNotBeFound("kyHieu.notEquals=" + DEFAULT_KY_HIEU);

        // Get all the donViList where kyHieu not equals to UPDATED_KY_HIEU
        defaultDonViShouldBeFound("kyHieu.notEquals=" + UPDATED_KY_HIEU);
    }

    @Test
    @Transactional
    public void getAllDonVisByKyHieuIsInShouldWork() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where kyHieu in DEFAULT_KY_HIEU or UPDATED_KY_HIEU
        defaultDonViShouldBeFound("kyHieu.in=" + DEFAULT_KY_HIEU + "," + UPDATED_KY_HIEU);

        // Get all the donViList where kyHieu equals to UPDATED_KY_HIEU
        defaultDonViShouldNotBeFound("kyHieu.in=" + UPDATED_KY_HIEU);
    }

    @Test
    @Transactional
    public void getAllDonVisByKyHieuIsNullOrNotNull() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where kyHieu is not null
        defaultDonViShouldBeFound("kyHieu.specified=true");

        // Get all the donViList where kyHieu is null
        defaultDonViShouldNotBeFound("kyHieu.specified=false");
    }

    @Test
    @Transactional
    public void getAllDonVisByKyHieuContainsSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where kyHieu contains DEFAULT_KY_HIEU
        defaultDonViShouldBeFound("kyHieu.contains=" + DEFAULT_KY_HIEU);

        // Get all the donViList where kyHieu contains UPDATED_KY_HIEU
        defaultDonViShouldNotBeFound("kyHieu.contains=" + UPDATED_KY_HIEU);
    }

    @Test
    @Transactional
    public void getAllDonVisByKyHieuNotContainsSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where kyHieu does not contain DEFAULT_KY_HIEU
        defaultDonViShouldNotBeFound("kyHieu.doesNotContain=" + DEFAULT_KY_HIEU);

        // Get all the donViList where kyHieu does not contain UPDATED_KY_HIEU
        defaultDonViShouldBeFound("kyHieu.doesNotContain=" + UPDATED_KY_HIEU);
    }

    @Test
    @Transactional
    public void getAllDonVisByLoaiIsEqualToSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where loai equals to DEFAULT_LOAI
        defaultDonViShouldBeFound("loai.equals=" + DEFAULT_LOAI);

        // Get all the donViList where loai equals to UPDATED_LOAI
        defaultDonViShouldNotBeFound("loai.equals=" + UPDATED_LOAI);
    }

    @Test
    @Transactional
    public void getAllDonVisByLoaiIsNotEqualToSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where loai not equals to DEFAULT_LOAI
        defaultDonViShouldNotBeFound("loai.notEquals=" + DEFAULT_LOAI);

        // Get all the donViList where loai not equals to UPDATED_LOAI
        defaultDonViShouldBeFound("loai.notEquals=" + UPDATED_LOAI);
    }

    @Test
    @Transactional
    public void getAllDonVisByLoaiIsInShouldWork() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where loai in DEFAULT_LOAI or UPDATED_LOAI
        defaultDonViShouldBeFound("loai.in=" + DEFAULT_LOAI + "," + UPDATED_LOAI);

        // Get all the donViList where loai equals to UPDATED_LOAI
        defaultDonViShouldNotBeFound("loai.in=" + UPDATED_LOAI);
    }

    @Test
    @Transactional
    public void getAllDonVisByLoaiIsNullOrNotNull() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where loai is not null
        defaultDonViShouldBeFound("loai.specified=true");

        // Get all the donViList where loai is null
        defaultDonViShouldNotBeFound("loai.specified=false");
    }

    @Test
    @Transactional
    public void getAllDonVisByLoaiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where loai is greater than or equal to DEFAULT_LOAI
        defaultDonViShouldBeFound("loai.greaterThanOrEqual=" + DEFAULT_LOAI);

        // Get all the donViList where loai is greater than or equal to UPDATED_LOAI
        defaultDonViShouldNotBeFound("loai.greaterThanOrEqual=" + UPDATED_LOAI);
    }

    @Test
    @Transactional
    public void getAllDonVisByLoaiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where loai is less than or equal to DEFAULT_LOAI
        defaultDonViShouldBeFound("loai.lessThanOrEqual=" + DEFAULT_LOAI);

        // Get all the donViList where loai is less than or equal to SMALLER_LOAI
        defaultDonViShouldNotBeFound("loai.lessThanOrEqual=" + SMALLER_LOAI);
    }

    @Test
    @Transactional
    public void getAllDonVisByLoaiIsLessThanSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where loai is less than DEFAULT_LOAI
        defaultDonViShouldNotBeFound("loai.lessThan=" + DEFAULT_LOAI);

        // Get all the donViList where loai is less than UPDATED_LOAI
        defaultDonViShouldBeFound("loai.lessThan=" + UPDATED_LOAI);
    }

    @Test
    @Transactional
    public void getAllDonVisByLoaiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where loai is greater than DEFAULT_LOAI
        defaultDonViShouldNotBeFound("loai.greaterThan=" + DEFAULT_LOAI);

        // Get all the donViList where loai is greater than SMALLER_LOAI
        defaultDonViShouldBeFound("loai.greaterThan=" + SMALLER_LOAI);
    }

    @Test
    @Transactional
    public void getAllDonVisByMaTinhIsEqualToSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where maTinh equals to DEFAULT_MA_TINH
        defaultDonViShouldBeFound("maTinh.equals=" + DEFAULT_MA_TINH);

        // Get all the donViList where maTinh equals to UPDATED_MA_TINH
        defaultDonViShouldNotBeFound("maTinh.equals=" + UPDATED_MA_TINH);
    }

    @Test
    @Transactional
    public void getAllDonVisByMaTinhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where maTinh not equals to DEFAULT_MA_TINH
        defaultDonViShouldNotBeFound("maTinh.notEquals=" + DEFAULT_MA_TINH);

        // Get all the donViList where maTinh not equals to UPDATED_MA_TINH
        defaultDonViShouldBeFound("maTinh.notEquals=" + UPDATED_MA_TINH);
    }

    @Test
    @Transactional
    public void getAllDonVisByMaTinhIsInShouldWork() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where maTinh in DEFAULT_MA_TINH or UPDATED_MA_TINH
        defaultDonViShouldBeFound("maTinh.in=" + DEFAULT_MA_TINH + "," + UPDATED_MA_TINH);

        // Get all the donViList where maTinh equals to UPDATED_MA_TINH
        defaultDonViShouldNotBeFound("maTinh.in=" + UPDATED_MA_TINH);
    }

    @Test
    @Transactional
    public void getAllDonVisByMaTinhIsNullOrNotNull() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where maTinh is not null
        defaultDonViShouldBeFound("maTinh.specified=true");

        // Get all the donViList where maTinh is null
        defaultDonViShouldNotBeFound("maTinh.specified=false");
    }

    @Test
    @Transactional
    public void getAllDonVisByMaTinhContainsSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where maTinh contains DEFAULT_MA_TINH
        defaultDonViShouldBeFound("maTinh.contains=" + DEFAULT_MA_TINH);

        // Get all the donViList where maTinh contains UPDATED_MA_TINH
        defaultDonViShouldNotBeFound("maTinh.contains=" + UPDATED_MA_TINH);
    }

    @Test
    @Transactional
    public void getAllDonVisByMaTinhNotContainsSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where maTinh does not contain DEFAULT_MA_TINH
        defaultDonViShouldNotBeFound("maTinh.doesNotContain=" + DEFAULT_MA_TINH);

        // Get all the donViList where maTinh does not contain UPDATED_MA_TINH
        defaultDonViShouldBeFound("maTinh.doesNotContain=" + UPDATED_MA_TINH);
    }

    @Test
    @Transactional
    public void getAllDonVisByPhoneIsEqualToSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where phone equals to DEFAULT_PHONE
        defaultDonViShouldBeFound("phone.equals=" + DEFAULT_PHONE);

        // Get all the donViList where phone equals to UPDATED_PHONE
        defaultDonViShouldNotBeFound("phone.equals=" + UPDATED_PHONE);
    }

    @Test
    @Transactional
    public void getAllDonVisByPhoneIsNotEqualToSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where phone not equals to DEFAULT_PHONE
        defaultDonViShouldNotBeFound("phone.notEquals=" + DEFAULT_PHONE);

        // Get all the donViList where phone not equals to UPDATED_PHONE
        defaultDonViShouldBeFound("phone.notEquals=" + UPDATED_PHONE);
    }

    @Test
    @Transactional
    public void getAllDonVisByPhoneIsInShouldWork() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where phone in DEFAULT_PHONE or UPDATED_PHONE
        defaultDonViShouldBeFound("phone.in=" + DEFAULT_PHONE + "," + UPDATED_PHONE);

        // Get all the donViList where phone equals to UPDATED_PHONE
        defaultDonViShouldNotBeFound("phone.in=" + UPDATED_PHONE);
    }

    @Test
    @Transactional
    public void getAllDonVisByPhoneIsNullOrNotNull() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where phone is not null
        defaultDonViShouldBeFound("phone.specified=true");

        // Get all the donViList where phone is null
        defaultDonViShouldNotBeFound("phone.specified=false");
    }

    @Test
    @Transactional
    public void getAllDonVisByPhoneContainsSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where phone contains DEFAULT_PHONE
        defaultDonViShouldBeFound("phone.contains=" + DEFAULT_PHONE);

        // Get all the donViList where phone contains UPDATED_PHONE
        defaultDonViShouldNotBeFound("phone.contains=" + UPDATED_PHONE);
    }

    @Test
    @Transactional
    public void getAllDonVisByPhoneNotContainsSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where phone does not contain DEFAULT_PHONE
        defaultDonViShouldNotBeFound("phone.doesNotContain=" + DEFAULT_PHONE);

        // Get all the donViList where phone does not contain UPDATED_PHONE
        defaultDonViShouldBeFound("phone.doesNotContain=" + UPDATED_PHONE);
    }

    @Test
    @Transactional
    public void getAllDonVisBySoTaiKhoanIsEqualToSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where soTaiKhoan equals to DEFAULT_SO_TAI_KHOAN
        defaultDonViShouldBeFound("soTaiKhoan.equals=" + DEFAULT_SO_TAI_KHOAN);

        // Get all the donViList where soTaiKhoan equals to UPDATED_SO_TAI_KHOAN
        defaultDonViShouldNotBeFound("soTaiKhoan.equals=" + UPDATED_SO_TAI_KHOAN);
    }

    @Test
    @Transactional
    public void getAllDonVisBySoTaiKhoanIsNotEqualToSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where soTaiKhoan not equals to DEFAULT_SO_TAI_KHOAN
        defaultDonViShouldNotBeFound("soTaiKhoan.notEquals=" + DEFAULT_SO_TAI_KHOAN);

        // Get all the donViList where soTaiKhoan not equals to UPDATED_SO_TAI_KHOAN
        defaultDonViShouldBeFound("soTaiKhoan.notEquals=" + UPDATED_SO_TAI_KHOAN);
    }

    @Test
    @Transactional
    public void getAllDonVisBySoTaiKhoanIsInShouldWork() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where soTaiKhoan in DEFAULT_SO_TAI_KHOAN or UPDATED_SO_TAI_KHOAN
        defaultDonViShouldBeFound("soTaiKhoan.in=" + DEFAULT_SO_TAI_KHOAN + "," + UPDATED_SO_TAI_KHOAN);

        // Get all the donViList where soTaiKhoan equals to UPDATED_SO_TAI_KHOAN
        defaultDonViShouldNotBeFound("soTaiKhoan.in=" + UPDATED_SO_TAI_KHOAN);
    }

    @Test
    @Transactional
    public void getAllDonVisBySoTaiKhoanIsNullOrNotNull() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where soTaiKhoan is not null
        defaultDonViShouldBeFound("soTaiKhoan.specified=true");

        // Get all the donViList where soTaiKhoan is null
        defaultDonViShouldNotBeFound("soTaiKhoan.specified=false");
    }

    @Test
    @Transactional
    public void getAllDonVisBySoTaiKhoanContainsSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where soTaiKhoan contains DEFAULT_SO_TAI_KHOAN
        defaultDonViShouldBeFound("soTaiKhoan.contains=" + DEFAULT_SO_TAI_KHOAN);

        // Get all the donViList where soTaiKhoan contains UPDATED_SO_TAI_KHOAN
        defaultDonViShouldNotBeFound("soTaiKhoan.contains=" + UPDATED_SO_TAI_KHOAN);
    }

    @Test
    @Transactional
    public void getAllDonVisBySoTaiKhoanNotContainsSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where soTaiKhoan does not contain DEFAULT_SO_TAI_KHOAN
        defaultDonViShouldNotBeFound("soTaiKhoan.doesNotContain=" + DEFAULT_SO_TAI_KHOAN);

        // Get all the donViList where soTaiKhoan does not contain UPDATED_SO_TAI_KHOAN
        defaultDonViShouldBeFound("soTaiKhoan.doesNotContain=" + UPDATED_SO_TAI_KHOAN);
    }

    @Test
    @Transactional
    public void getAllDonVisByTenIsEqualToSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where ten equals to DEFAULT_TEN
        defaultDonViShouldBeFound("ten.equals=" + DEFAULT_TEN);

        // Get all the donViList where ten equals to UPDATED_TEN
        defaultDonViShouldNotBeFound("ten.equals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllDonVisByTenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where ten not equals to DEFAULT_TEN
        defaultDonViShouldNotBeFound("ten.notEquals=" + DEFAULT_TEN);

        // Get all the donViList where ten not equals to UPDATED_TEN
        defaultDonViShouldBeFound("ten.notEquals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllDonVisByTenIsInShouldWork() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where ten in DEFAULT_TEN or UPDATED_TEN
        defaultDonViShouldBeFound("ten.in=" + DEFAULT_TEN + "," + UPDATED_TEN);

        // Get all the donViList where ten equals to UPDATED_TEN
        defaultDonViShouldNotBeFound("ten.in=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllDonVisByTenIsNullOrNotNull() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where ten is not null
        defaultDonViShouldBeFound("ten.specified=true");

        // Get all the donViList where ten is null
        defaultDonViShouldNotBeFound("ten.specified=false");
    }

    @Test
    @Transactional
    public void getAllDonVisByTenContainsSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where ten contains DEFAULT_TEN
        defaultDonViShouldBeFound("ten.contains=" + DEFAULT_TEN);

        // Get all the donViList where ten contains UPDATED_TEN
        defaultDonViShouldNotBeFound("ten.contains=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllDonVisByTenNotContainsSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where ten does not contain DEFAULT_TEN
        defaultDonViShouldNotBeFound("ten.doesNotContain=" + DEFAULT_TEN);

        // Get all the donViList where ten does not contain UPDATED_TEN
        defaultDonViShouldBeFound("ten.doesNotContain=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllDonVisByTenNganHangIsEqualToSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where tenNganHang equals to DEFAULT_TEN_NGAN_HANG
        defaultDonViShouldBeFound("tenNganHang.equals=" + DEFAULT_TEN_NGAN_HANG);

        // Get all the donViList where tenNganHang equals to UPDATED_TEN_NGAN_HANG
        defaultDonViShouldNotBeFound("tenNganHang.equals=" + UPDATED_TEN_NGAN_HANG);
    }

    @Test
    @Transactional
    public void getAllDonVisByTenNganHangIsNotEqualToSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where tenNganHang not equals to DEFAULT_TEN_NGAN_HANG
        defaultDonViShouldNotBeFound("tenNganHang.notEquals=" + DEFAULT_TEN_NGAN_HANG);

        // Get all the donViList where tenNganHang not equals to UPDATED_TEN_NGAN_HANG
        defaultDonViShouldBeFound("tenNganHang.notEquals=" + UPDATED_TEN_NGAN_HANG);
    }

    @Test
    @Transactional
    public void getAllDonVisByTenNganHangIsInShouldWork() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where tenNganHang in DEFAULT_TEN_NGAN_HANG or UPDATED_TEN_NGAN_HANG
        defaultDonViShouldBeFound("tenNganHang.in=" + DEFAULT_TEN_NGAN_HANG + "," + UPDATED_TEN_NGAN_HANG);

        // Get all the donViList where tenNganHang equals to UPDATED_TEN_NGAN_HANG
        defaultDonViShouldNotBeFound("tenNganHang.in=" + UPDATED_TEN_NGAN_HANG);
    }

    @Test
    @Transactional
    public void getAllDonVisByTenNganHangIsNullOrNotNull() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where tenNganHang is not null
        defaultDonViShouldBeFound("tenNganHang.specified=true");

        // Get all the donViList where tenNganHang is null
        defaultDonViShouldNotBeFound("tenNganHang.specified=false");
    }

    @Test
    @Transactional
    public void getAllDonVisByTenNganHangContainsSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where tenNganHang contains DEFAULT_TEN_NGAN_HANG
        defaultDonViShouldBeFound("tenNganHang.contains=" + DEFAULT_TEN_NGAN_HANG);

        // Get all the donViList where tenNganHang contains UPDATED_TEN_NGAN_HANG
        defaultDonViShouldNotBeFound("tenNganHang.contains=" + UPDATED_TEN_NGAN_HANG);
    }

    @Test
    @Transactional
    public void getAllDonVisByTenNganHangNotContainsSomething() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        // Get all the donViList where tenNganHang does not contain DEFAULT_TEN_NGAN_HANG
        defaultDonViShouldNotBeFound("tenNganHang.doesNotContain=" + DEFAULT_TEN_NGAN_HANG);

        // Get all the donViList where tenNganHang does not contain UPDATED_TEN_NGAN_HANG
        defaultDonViShouldBeFound("tenNganHang.doesNotContain=" + UPDATED_TEN_NGAN_HANG);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultDonViShouldBeFound(String filter) throws Exception {
        restDonViMockMvc.perform(get("/api/don-vis?" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(donVi.getId().intValue())))
            .andExpect(jsonPath("$.[*].cap").value(hasItem(DEFAULT_CAP)))
            .andExpect(jsonPath("$.[*].chiNhanhNganHang").value(hasItem(DEFAULT_CHI_NHANH_NGAN_HANG)))
            .andExpect(jsonPath("$.[*].diaChi").value(hasItem(DEFAULT_DIA_CHI)))
            .andExpect(jsonPath("$.[*].donViQuanLyId").value(hasItem(DEFAULT_DON_VI_QUAN_LY_ID.intValue())))
            .andExpect(jsonPath("$.[*].dvtt").value(hasItem(DEFAULT_DVTT)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].enabled").value(hasItem(DEFAULT_ENABLED.booleanValue())))
            .andExpect(jsonPath("$.[*].kyHieu").value(hasItem(DEFAULT_KY_HIEU)))
            .andExpect(jsonPath("$.[*].loai").value(hasItem(DEFAULT_LOAI)))
            .andExpect(jsonPath("$.[*].maTinh").value(hasItem(DEFAULT_MA_TINH)))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE)))
            .andExpect(jsonPath("$.[*].soTaiKhoan").value(hasItem(DEFAULT_SO_TAI_KHOAN)))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].tenNganHang").value(hasItem(DEFAULT_TEN_NGAN_HANG)));

        // Check, that the count call also returns 1
        restDonViMockMvc.perform(get("/api/don-vis/count?" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultDonViShouldNotBeFound(String filter) throws Exception {
        restDonViMockMvc.perform(get("/api/don-vis?" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restDonViMockMvc.perform(get("/api/don-vis/count?" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingDonVi() throws Exception {
        // Get the donVi
        restDonViMockMvc.perform(get("/api/don-vis/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDonVi() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        int databaseSizeBeforeUpdate = donViRepository.findAll().size();

        // Update the donVi
        DonVi updatedDonVi = donViRepository.findById(donVi.getId()).get();
        // Disconnect from session so that the updates on updatedDonVi are not directly saved in db
        em.detach(updatedDonVi);
        updatedDonVi
            .cap(UPDATED_CAP)
            .chiNhanhNganHang(UPDATED_CHI_NHANH_NGAN_HANG)
            .diaChi(UPDATED_DIA_CHI)
            .donViQuanLyId(UPDATED_DON_VI_QUAN_LY_ID)
            .dvtt(UPDATED_DVTT)
            .email(UPDATED_EMAIL)
            .enabled(UPDATED_ENABLED)
            .kyHieu(UPDATED_KY_HIEU)
            .loai(UPDATED_LOAI)
            .maTinh(UPDATED_MA_TINH)
            .phone(UPDATED_PHONE)
            .soTaiKhoan(UPDATED_SO_TAI_KHOAN)
            .ten(UPDATED_TEN)
            .tenNganHang(UPDATED_TEN_NGAN_HANG);
        DonViDTO donViDTO = donViMapper.toDto(updatedDonVi);

        restDonViMockMvc.perform(put("/api/don-vis").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(donViDTO)))
            .andExpect(status().isOk());

        // Validate the DonVi in the database
        List<DonVi> donViList = donViRepository.findAll();
        assertThat(donViList).hasSize(databaseSizeBeforeUpdate);
        DonVi testDonVi = donViList.get(donViList.size() - 1);
        assertThat(testDonVi.getCap()).isEqualTo(UPDATED_CAP);
        assertThat(testDonVi.getChiNhanhNganHang()).isEqualTo(UPDATED_CHI_NHANH_NGAN_HANG);
        assertThat(testDonVi.getDiaChi()).isEqualTo(UPDATED_DIA_CHI);
        assertThat(testDonVi.getDonViQuanLyId()).isEqualTo(UPDATED_DON_VI_QUAN_LY_ID);
        assertThat(testDonVi.getDvtt()).isEqualTo(UPDATED_DVTT);
        assertThat(testDonVi.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testDonVi.isEnabled()).isEqualTo(UPDATED_ENABLED);
        assertThat(testDonVi.getKyHieu()).isEqualTo(UPDATED_KY_HIEU);
        assertThat(testDonVi.getLoai()).isEqualTo(UPDATED_LOAI);
        assertThat(testDonVi.getMaTinh()).isEqualTo(UPDATED_MA_TINH);
        assertThat(testDonVi.getPhone()).isEqualTo(UPDATED_PHONE);
        assertThat(testDonVi.getSoTaiKhoan()).isEqualTo(UPDATED_SO_TAI_KHOAN);
        assertThat(testDonVi.getTen()).isEqualTo(UPDATED_TEN);
        assertThat(testDonVi.getTenNganHang()).isEqualTo(UPDATED_TEN_NGAN_HANG);
    }

    @Test
    @Transactional
    public void updateNonExistingDonVi() throws Exception {
        int databaseSizeBeforeUpdate = donViRepository.findAll().size();

        // Create the DonVi
        DonViDTO donViDTO = donViMapper.toDto(donVi);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDonViMockMvc.perform(put("/api/don-vis").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(donViDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DonVi in the database
        List<DonVi> donViList = donViRepository.findAll();
        assertThat(donViList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDonVi() throws Exception {
        // Initialize the database
        donViRepository.saveAndFlush(donVi);

        int databaseSizeBeforeDelete = donViRepository.findAll().size();

        // Delete the donVi
        restDonViMockMvc.perform(delete("/api/don-vis/{id}", donVi.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DonVi> donViList = donViRepository.findAll();
        assertThat(donViList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

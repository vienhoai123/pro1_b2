package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.UserType;
import vn.vnpt.repository.UserTypeRepository;
import vn.vnpt.service.UserTypeService;
import vn.vnpt.service.dto.UserTypeDTO;
import vn.vnpt.service.mapper.UserTypeMapper;
import vn.vnpt.service.dto.UserTypeCriteria;
import vn.vnpt.service.UserTypeQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link UserTypeResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class UserTypeResourceIT {

    private static final String DEFAULT_MO_TA = "AAAAAAAAAA";
    private static final String UPDATED_MO_TA = "BBBBBBBBBB";

    @Autowired
    private UserTypeRepository userTypeRepository;

    @Autowired
    private UserTypeMapper userTypeMapper;

    @Autowired
    private UserTypeService userTypeService;

    @Autowired
    private UserTypeQueryService userTypeQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restUserTypeMockMvc;

    private UserType userType;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserType createEntity(EntityManager em) {
        UserType userType = new UserType()
            .moTa(DEFAULT_MO_TA);
        return userType;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserType createUpdatedEntity(EntityManager em) {
        UserType userType = new UserType()
            .moTa(UPDATED_MO_TA);
        return userType;
    }

    @BeforeEach
    public void initTest() {
        userType = createEntity(em);
    }

    @Test
    @Transactional
    public void createUserType() throws Exception {
        int databaseSizeBeforeCreate = userTypeRepository.findAll().size();

        // Create the UserType
        UserTypeDTO userTypeDTO = userTypeMapper.toDto(userType);
        restUserTypeMockMvc.perform(post("/api/user-types").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(userTypeDTO)))
            .andExpect(status().isCreated());

        // Validate the UserType in the database
        List<UserType> userTypeList = userTypeRepository.findAll();
        assertThat(userTypeList).hasSize(databaseSizeBeforeCreate + 1);
        UserType testUserType = userTypeList.get(userTypeList.size() - 1);
        assertThat(testUserType.getMoTa()).isEqualTo(DEFAULT_MO_TA);
    }

    @Test
    @Transactional
    public void createUserTypeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = userTypeRepository.findAll().size();

        // Create the UserType with an existing ID
        userType.setId(1L);
        UserTypeDTO userTypeDTO = userTypeMapper.toDto(userType);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUserTypeMockMvc.perform(post("/api/user-types").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(userTypeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UserType in the database
        List<UserType> userTypeList = userTypeRepository.findAll();
        assertThat(userTypeList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllUserTypes() throws Exception {
        // Initialize the database
        userTypeRepository.saveAndFlush(userType);

        // Get all the userTypeList
        restUserTypeMockMvc.perform(get("/api/user-types?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userType.getId().intValue())))
            .andExpect(jsonPath("$.[*].moTa").value(hasItem(DEFAULT_MO_TA)));
    }
    
    @Test
    @Transactional
    public void getUserType() throws Exception {
        // Initialize the database
        userTypeRepository.saveAndFlush(userType);

        // Get the userType
        restUserTypeMockMvc.perform(get("/api/user-types/{id}", userType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(userType.getId().intValue()))
            .andExpect(jsonPath("$.moTa").value(DEFAULT_MO_TA));
    }


    @Test
    @Transactional
    public void getUserTypesByIdFiltering() throws Exception {
        // Initialize the database
        userTypeRepository.saveAndFlush(userType);

        Long id = userType.getId();

        defaultUserTypeShouldBeFound("id.equals=" + id);
        defaultUserTypeShouldNotBeFound("id.notEquals=" + id);

        defaultUserTypeShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultUserTypeShouldNotBeFound("id.greaterThan=" + id);

        defaultUserTypeShouldBeFound("id.lessThanOrEqual=" + id);
        defaultUserTypeShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllUserTypesByMoTaIsEqualToSomething() throws Exception {
        // Initialize the database
        userTypeRepository.saveAndFlush(userType);

        // Get all the userTypeList where moTa equals to DEFAULT_MO_TA
        defaultUserTypeShouldBeFound("moTa.equals=" + DEFAULT_MO_TA);

        // Get all the userTypeList where moTa equals to UPDATED_MO_TA
        defaultUserTypeShouldNotBeFound("moTa.equals=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    public void getAllUserTypesByMoTaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        userTypeRepository.saveAndFlush(userType);

        // Get all the userTypeList where moTa not equals to DEFAULT_MO_TA
        defaultUserTypeShouldNotBeFound("moTa.notEquals=" + DEFAULT_MO_TA);

        // Get all the userTypeList where moTa not equals to UPDATED_MO_TA
        defaultUserTypeShouldBeFound("moTa.notEquals=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    public void getAllUserTypesByMoTaIsInShouldWork() throws Exception {
        // Initialize the database
        userTypeRepository.saveAndFlush(userType);

        // Get all the userTypeList where moTa in DEFAULT_MO_TA or UPDATED_MO_TA
        defaultUserTypeShouldBeFound("moTa.in=" + DEFAULT_MO_TA + "," + UPDATED_MO_TA);

        // Get all the userTypeList where moTa equals to UPDATED_MO_TA
        defaultUserTypeShouldNotBeFound("moTa.in=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    public void getAllUserTypesByMoTaIsNullOrNotNull() throws Exception {
        // Initialize the database
        userTypeRepository.saveAndFlush(userType);

        // Get all the userTypeList where moTa is not null
        defaultUserTypeShouldBeFound("moTa.specified=true");

        // Get all the userTypeList where moTa is null
        defaultUserTypeShouldNotBeFound("moTa.specified=false");
    }
                @Test
    @Transactional
    public void getAllUserTypesByMoTaContainsSomething() throws Exception {
        // Initialize the database
        userTypeRepository.saveAndFlush(userType);

        // Get all the userTypeList where moTa contains DEFAULT_MO_TA
        defaultUserTypeShouldBeFound("moTa.contains=" + DEFAULT_MO_TA);

        // Get all the userTypeList where moTa contains UPDATED_MO_TA
        defaultUserTypeShouldNotBeFound("moTa.contains=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    public void getAllUserTypesByMoTaNotContainsSomething() throws Exception {
        // Initialize the database
        userTypeRepository.saveAndFlush(userType);

        // Get all the userTypeList where moTa does not contain DEFAULT_MO_TA
        defaultUserTypeShouldNotBeFound("moTa.doesNotContain=" + DEFAULT_MO_TA);

        // Get all the userTypeList where moTa does not contain UPDATED_MO_TA
        defaultUserTypeShouldBeFound("moTa.doesNotContain=" + UPDATED_MO_TA);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultUserTypeShouldBeFound(String filter) throws Exception {
        restUserTypeMockMvc.perform(get("/api/user-types?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userType.getId().intValue())))
            .andExpect(jsonPath("$.[*].moTa").value(hasItem(DEFAULT_MO_TA)));

        // Check, that the count call also returns 1
        restUserTypeMockMvc.perform(get("/api/user-types/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultUserTypeShouldNotBeFound(String filter) throws Exception {
        restUserTypeMockMvc.perform(get("/api/user-types?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restUserTypeMockMvc.perform(get("/api/user-types/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingUserType() throws Exception {
        // Get the userType
        restUserTypeMockMvc.perform(get("/api/user-types/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUserType() throws Exception {
        // Initialize the database
        userTypeRepository.saveAndFlush(userType);

        int databaseSizeBeforeUpdate = userTypeRepository.findAll().size();

        // Update the userType
        UserType updatedUserType = userTypeRepository.findById(userType.getId()).get();
        // Disconnect from session so that the updates on updatedUserType are not directly saved in db
        em.detach(updatedUserType);
        updatedUserType
            .moTa(UPDATED_MO_TA);
        UserTypeDTO userTypeDTO = userTypeMapper.toDto(updatedUserType);

        restUserTypeMockMvc.perform(put("/api/user-types").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(userTypeDTO)))
            .andExpect(status().isOk());

        // Validate the UserType in the database
        List<UserType> userTypeList = userTypeRepository.findAll();
        assertThat(userTypeList).hasSize(databaseSizeBeforeUpdate);
        UserType testUserType = userTypeList.get(userTypeList.size() - 1);
        assertThat(testUserType.getMoTa()).isEqualTo(UPDATED_MO_TA);
    }

    @Test
    @Transactional
    public void updateNonExistingUserType() throws Exception {
        int databaseSizeBeforeUpdate = userTypeRepository.findAll().size();

        // Create the UserType
        UserTypeDTO userTypeDTO = userTypeMapper.toDto(userType);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUserTypeMockMvc.perform(put("/api/user-types").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(userTypeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UserType in the database
        List<UserType> userTypeList = userTypeRepository.findAll();
        assertThat(userTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteUserType() throws Exception {
        // Initialize the database
        userTypeRepository.saveAndFlush(userType);

        int databaseSizeBeforeDelete = userTypeRepository.findAll().size();

        // Delete the userType
        restUserTypeMockMvc.perform(delete("/api/user-types/{id}", userType.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<UserType> userTypeList = userTypeRepository.findAll();
        assertThat(userTypeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

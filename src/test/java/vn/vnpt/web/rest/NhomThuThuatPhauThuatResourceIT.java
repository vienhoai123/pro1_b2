package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.NhomThuThuatPhauThuat;
import vn.vnpt.domain.DonVi;
import vn.vnpt.repository.NhomThuThuatPhauThuatRepository;
import vn.vnpt.service.NhomThuThuatPhauThuatService;
import vn.vnpt.service.dto.NhomThuThuatPhauThuatDTO;
import vn.vnpt.service.mapper.NhomThuThuatPhauThuatMapper;
import vn.vnpt.service.dto.NhomThuThuatPhauThuatCriteria;
import vn.vnpt.service.NhomThuThuatPhauThuatQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link NhomThuThuatPhauThuatResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class NhomThuThuatPhauThuatResourceIT {

    private static final Boolean DEFAULT_ENABLE = false;
    private static final Boolean UPDATED_ENABLE = true;

    private static final Integer DEFAULT_LEVEL = 1;
    private static final Integer UPDATED_LEVEL = 2;
    private static final Integer SMALLER_LEVEL = 1 - 1;

    private static final BigDecimal DEFAULT_PARENT_ID = new BigDecimal(1);
    private static final BigDecimal UPDATED_PARENT_ID = new BigDecimal(2);
    private static final BigDecimal SMALLER_PARENT_ID = new BigDecimal(1 - 1);

    private static final String DEFAULT_TEN = "AAAAAAAAAA";
    private static final String UPDATED_TEN = "BBBBBBBBBB";

    @Autowired
    private NhomThuThuatPhauThuatRepository nhomThuThuatPhauThuatRepository;

    @Autowired
    private NhomThuThuatPhauThuatMapper nhomThuThuatPhauThuatMapper;

    @Autowired
    private NhomThuThuatPhauThuatService nhomThuThuatPhauThuatService;

    @Autowired
    private NhomThuThuatPhauThuatQueryService nhomThuThuatPhauThuatQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restNhomThuThuatPhauThuatMockMvc;

    private NhomThuThuatPhauThuat nhomThuThuatPhauThuat;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NhomThuThuatPhauThuat createEntity(EntityManager em) {
        NhomThuThuatPhauThuat nhomThuThuatPhauThuat = new NhomThuThuatPhauThuat()
            .enable(DEFAULT_ENABLE)
            .level(DEFAULT_LEVEL)
            .parentId(DEFAULT_PARENT_ID)
            .ten(DEFAULT_TEN);
        // Add required entity
        DonVi donVi;
        if (TestUtil.findAll(em, DonVi.class).isEmpty()) {
            donVi = DonViResourceIT.createEntity(em);
            em.persist(donVi);
            em.flush();
        } else {
            donVi = TestUtil.findAll(em, DonVi.class).get(0);
        }
        nhomThuThuatPhauThuat.setDonVi(donVi);
        return nhomThuThuatPhauThuat;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NhomThuThuatPhauThuat createUpdatedEntity(EntityManager em) {
        NhomThuThuatPhauThuat nhomThuThuatPhauThuat = new NhomThuThuatPhauThuat()
            .enable(UPDATED_ENABLE)
            .level(UPDATED_LEVEL)
            .parentId(UPDATED_PARENT_ID)
            .ten(UPDATED_TEN);
        // Add required entity
        DonVi donVi;
        if (TestUtil.findAll(em, DonVi.class).isEmpty()) {
            donVi = DonViResourceIT.createUpdatedEntity(em);
            em.persist(donVi);
            em.flush();
        } else {
            donVi = TestUtil.findAll(em, DonVi.class).get(0);
        }
        nhomThuThuatPhauThuat.setDonVi(donVi);
        return nhomThuThuatPhauThuat;
    }

    @BeforeEach
    public void initTest() {
        nhomThuThuatPhauThuat = createEntity(em);
    }

    @Test
    @Transactional
    public void createNhomThuThuatPhauThuat() throws Exception {
        int databaseSizeBeforeCreate = nhomThuThuatPhauThuatRepository.findAll().size();

        // Create the NhomThuThuatPhauThuat
        NhomThuThuatPhauThuatDTO nhomThuThuatPhauThuatDTO = nhomThuThuatPhauThuatMapper.toDto(nhomThuThuatPhauThuat);
        restNhomThuThuatPhauThuatMockMvc.perform(post("/api/nhom-thu-thuat-phau-thuats").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(nhomThuThuatPhauThuatDTO)))
            .andExpect(status().isCreated());

        // Validate the NhomThuThuatPhauThuat in the database
        List<NhomThuThuatPhauThuat> nhomThuThuatPhauThuatList = nhomThuThuatPhauThuatRepository.findAll();
        assertThat(nhomThuThuatPhauThuatList).hasSize(databaseSizeBeforeCreate + 1);
        NhomThuThuatPhauThuat testNhomThuThuatPhauThuat = nhomThuThuatPhauThuatList.get(nhomThuThuatPhauThuatList.size() - 1);
        assertThat(testNhomThuThuatPhauThuat.isEnable()).isEqualTo(DEFAULT_ENABLE);
        assertThat(testNhomThuThuatPhauThuat.getLevel()).isEqualTo(DEFAULT_LEVEL);
        assertThat(testNhomThuThuatPhauThuat.getParentId()).isEqualTo(DEFAULT_PARENT_ID);
        assertThat(testNhomThuThuatPhauThuat.getTen()).isEqualTo(DEFAULT_TEN);
    }

    @Test
    @Transactional
    public void createNhomThuThuatPhauThuatWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = nhomThuThuatPhauThuatRepository.findAll().size();

        // Create the NhomThuThuatPhauThuat with an existing ID
        nhomThuThuatPhauThuat.setId(1L);
        NhomThuThuatPhauThuatDTO nhomThuThuatPhauThuatDTO = nhomThuThuatPhauThuatMapper.toDto(nhomThuThuatPhauThuat);

        // An entity with an existing ID cannot be created, so this API call must fail
        restNhomThuThuatPhauThuatMockMvc.perform(post("/api/nhom-thu-thuat-phau-thuats").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(nhomThuThuatPhauThuatDTO)))
            .andExpect(status().isBadRequest());

        // Validate the NhomThuThuatPhauThuat in the database
        List<NhomThuThuatPhauThuat> nhomThuThuatPhauThuatList = nhomThuThuatPhauThuatRepository.findAll();
        assertThat(nhomThuThuatPhauThuatList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkEnableIsRequired() throws Exception {
        int databaseSizeBeforeTest = nhomThuThuatPhauThuatRepository.findAll().size();
        // set the field null
        nhomThuThuatPhauThuat.setEnable(null);

        // Create the NhomThuThuatPhauThuat, which fails.
        NhomThuThuatPhauThuatDTO nhomThuThuatPhauThuatDTO = nhomThuThuatPhauThuatMapper.toDto(nhomThuThuatPhauThuat);

        restNhomThuThuatPhauThuatMockMvc.perform(post("/api/nhom-thu-thuat-phau-thuats").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(nhomThuThuatPhauThuatDTO)))
            .andExpect(status().isBadRequest());

        List<NhomThuThuatPhauThuat> nhomThuThuatPhauThuatList = nhomThuThuatPhauThuatRepository.findAll();
        assertThat(nhomThuThuatPhauThuatList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLevelIsRequired() throws Exception {
        int databaseSizeBeforeTest = nhomThuThuatPhauThuatRepository.findAll().size();
        // set the field null
        nhomThuThuatPhauThuat.setLevel(null);

        // Create the NhomThuThuatPhauThuat, which fails.
        NhomThuThuatPhauThuatDTO nhomThuThuatPhauThuatDTO = nhomThuThuatPhauThuatMapper.toDto(nhomThuThuatPhauThuat);

        restNhomThuThuatPhauThuatMockMvc.perform(post("/api/nhom-thu-thuat-phau-thuats").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(nhomThuThuatPhauThuatDTO)))
            .andExpect(status().isBadRequest());

        List<NhomThuThuatPhauThuat> nhomThuThuatPhauThuatList = nhomThuThuatPhauThuatRepository.findAll();
        assertThat(nhomThuThuatPhauThuatList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkParentIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = nhomThuThuatPhauThuatRepository.findAll().size();
        // set the field null
        nhomThuThuatPhauThuat.setParentId(null);

        // Create the NhomThuThuatPhauThuat, which fails.
        NhomThuThuatPhauThuatDTO nhomThuThuatPhauThuatDTO = nhomThuThuatPhauThuatMapper.toDto(nhomThuThuatPhauThuat);

        restNhomThuThuatPhauThuatMockMvc.perform(post("/api/nhom-thu-thuat-phau-thuats").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(nhomThuThuatPhauThuatDTO)))
            .andExpect(status().isBadRequest());

        List<NhomThuThuatPhauThuat> nhomThuThuatPhauThuatList = nhomThuThuatPhauThuatRepository.findAll();
        assertThat(nhomThuThuatPhauThuatList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllNhomThuThuatPhauThuats() throws Exception {
        // Initialize the database
        nhomThuThuatPhauThuatRepository.saveAndFlush(nhomThuThuatPhauThuat);

        // Get all the nhomThuThuatPhauThuatList
        restNhomThuThuatPhauThuatMockMvc.perform(get("/api/nhom-thu-thuat-phau-thuats?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(nhomThuThuatPhauThuat.getId().intValue())))
            .andExpect(jsonPath("$.[*].enable").value(hasItem(DEFAULT_ENABLE.booleanValue())))
            .andExpect(jsonPath("$.[*].level").value(hasItem(DEFAULT_LEVEL)))
            .andExpect(jsonPath("$.[*].parentId").value(hasItem(DEFAULT_PARENT_ID.intValue())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)));
    }
    
    @Test
    @Transactional
    public void getNhomThuThuatPhauThuat() throws Exception {
        // Initialize the database
        nhomThuThuatPhauThuatRepository.saveAndFlush(nhomThuThuatPhauThuat);

        // Get the nhomThuThuatPhauThuat
        restNhomThuThuatPhauThuatMockMvc.perform(get("/api/nhom-thu-thuat-phau-thuats/{id}", nhomThuThuatPhauThuat.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(nhomThuThuatPhauThuat.getId().intValue()))
            .andExpect(jsonPath("$.enable").value(DEFAULT_ENABLE.booleanValue()))
            .andExpect(jsonPath("$.level").value(DEFAULT_LEVEL))
            .andExpect(jsonPath("$.parentId").value(DEFAULT_PARENT_ID.intValue()))
            .andExpect(jsonPath("$.ten").value(DEFAULT_TEN));
    }


    @Test
    @Transactional
    public void getNhomThuThuatPhauThuatsByIdFiltering() throws Exception {
        // Initialize the database
        nhomThuThuatPhauThuatRepository.saveAndFlush(nhomThuThuatPhauThuat);

        Long id = nhomThuThuatPhauThuat.getId();

        defaultNhomThuThuatPhauThuatShouldBeFound("id.equals=" + id);
        defaultNhomThuThuatPhauThuatShouldNotBeFound("id.notEquals=" + id);

        defaultNhomThuThuatPhauThuatShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultNhomThuThuatPhauThuatShouldNotBeFound("id.greaterThan=" + id);

        defaultNhomThuThuatPhauThuatShouldBeFound("id.lessThanOrEqual=" + id);
        defaultNhomThuThuatPhauThuatShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllNhomThuThuatPhauThuatsByEnableIsEqualToSomething() throws Exception {
        // Initialize the database
        nhomThuThuatPhauThuatRepository.saveAndFlush(nhomThuThuatPhauThuat);

        // Get all the nhomThuThuatPhauThuatList where enable equals to DEFAULT_ENABLE
        defaultNhomThuThuatPhauThuatShouldBeFound("enable.equals=" + DEFAULT_ENABLE);

        // Get all the nhomThuThuatPhauThuatList where enable equals to UPDATED_ENABLE
        defaultNhomThuThuatPhauThuatShouldNotBeFound("enable.equals=" + UPDATED_ENABLE);
    }

    @Test
    @Transactional
    public void getAllNhomThuThuatPhauThuatsByEnableIsNotEqualToSomething() throws Exception {
        // Initialize the database
        nhomThuThuatPhauThuatRepository.saveAndFlush(nhomThuThuatPhauThuat);

        // Get all the nhomThuThuatPhauThuatList where enable not equals to DEFAULT_ENABLE
        defaultNhomThuThuatPhauThuatShouldNotBeFound("enable.notEquals=" + DEFAULT_ENABLE);

        // Get all the nhomThuThuatPhauThuatList where enable not equals to UPDATED_ENABLE
        defaultNhomThuThuatPhauThuatShouldBeFound("enable.notEquals=" + UPDATED_ENABLE);
    }

    @Test
    @Transactional
    public void getAllNhomThuThuatPhauThuatsByEnableIsInShouldWork() throws Exception {
        // Initialize the database
        nhomThuThuatPhauThuatRepository.saveAndFlush(nhomThuThuatPhauThuat);

        // Get all the nhomThuThuatPhauThuatList where enable in DEFAULT_ENABLE or UPDATED_ENABLE
        defaultNhomThuThuatPhauThuatShouldBeFound("enable.in=" + DEFAULT_ENABLE + "," + UPDATED_ENABLE);

        // Get all the nhomThuThuatPhauThuatList where enable equals to UPDATED_ENABLE
        defaultNhomThuThuatPhauThuatShouldNotBeFound("enable.in=" + UPDATED_ENABLE);
    }

    @Test
    @Transactional
    public void getAllNhomThuThuatPhauThuatsByEnableIsNullOrNotNull() throws Exception {
        // Initialize the database
        nhomThuThuatPhauThuatRepository.saveAndFlush(nhomThuThuatPhauThuat);

        // Get all the nhomThuThuatPhauThuatList where enable is not null
        defaultNhomThuThuatPhauThuatShouldBeFound("enable.specified=true");

        // Get all the nhomThuThuatPhauThuatList where enable is null
        defaultNhomThuThuatPhauThuatShouldNotBeFound("enable.specified=false");
    }

    @Test
    @Transactional
    public void getAllNhomThuThuatPhauThuatsByLevelIsEqualToSomething() throws Exception {
        // Initialize the database
        nhomThuThuatPhauThuatRepository.saveAndFlush(nhomThuThuatPhauThuat);

        // Get all the nhomThuThuatPhauThuatList where level equals to DEFAULT_LEVEL
        defaultNhomThuThuatPhauThuatShouldBeFound("level.equals=" + DEFAULT_LEVEL);

        // Get all the nhomThuThuatPhauThuatList where level equals to UPDATED_LEVEL
        defaultNhomThuThuatPhauThuatShouldNotBeFound("level.equals=" + UPDATED_LEVEL);
    }

    @Test
    @Transactional
    public void getAllNhomThuThuatPhauThuatsByLevelIsNotEqualToSomething() throws Exception {
        // Initialize the database
        nhomThuThuatPhauThuatRepository.saveAndFlush(nhomThuThuatPhauThuat);

        // Get all the nhomThuThuatPhauThuatList where level not equals to DEFAULT_LEVEL
        defaultNhomThuThuatPhauThuatShouldNotBeFound("level.notEquals=" + DEFAULT_LEVEL);

        // Get all the nhomThuThuatPhauThuatList where level not equals to UPDATED_LEVEL
        defaultNhomThuThuatPhauThuatShouldBeFound("level.notEquals=" + UPDATED_LEVEL);
    }

    @Test
    @Transactional
    public void getAllNhomThuThuatPhauThuatsByLevelIsInShouldWork() throws Exception {
        // Initialize the database
        nhomThuThuatPhauThuatRepository.saveAndFlush(nhomThuThuatPhauThuat);

        // Get all the nhomThuThuatPhauThuatList where level in DEFAULT_LEVEL or UPDATED_LEVEL
        defaultNhomThuThuatPhauThuatShouldBeFound("level.in=" + DEFAULT_LEVEL + "," + UPDATED_LEVEL);

        // Get all the nhomThuThuatPhauThuatList where level equals to UPDATED_LEVEL
        defaultNhomThuThuatPhauThuatShouldNotBeFound("level.in=" + UPDATED_LEVEL);
    }

    @Test
    @Transactional
    public void getAllNhomThuThuatPhauThuatsByLevelIsNullOrNotNull() throws Exception {
        // Initialize the database
        nhomThuThuatPhauThuatRepository.saveAndFlush(nhomThuThuatPhauThuat);

        // Get all the nhomThuThuatPhauThuatList where level is not null
        defaultNhomThuThuatPhauThuatShouldBeFound("level.specified=true");

        // Get all the nhomThuThuatPhauThuatList where level is null
        defaultNhomThuThuatPhauThuatShouldNotBeFound("level.specified=false");
    }

    @Test
    @Transactional
    public void getAllNhomThuThuatPhauThuatsByLevelIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        nhomThuThuatPhauThuatRepository.saveAndFlush(nhomThuThuatPhauThuat);

        // Get all the nhomThuThuatPhauThuatList where level is greater than or equal to DEFAULT_LEVEL
        defaultNhomThuThuatPhauThuatShouldBeFound("level.greaterThanOrEqual=" + DEFAULT_LEVEL);

        // Get all the nhomThuThuatPhauThuatList where level is greater than or equal to UPDATED_LEVEL
        defaultNhomThuThuatPhauThuatShouldNotBeFound("level.greaterThanOrEqual=" + UPDATED_LEVEL);
    }

    @Test
    @Transactional
    public void getAllNhomThuThuatPhauThuatsByLevelIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        nhomThuThuatPhauThuatRepository.saveAndFlush(nhomThuThuatPhauThuat);

        // Get all the nhomThuThuatPhauThuatList where level is less than or equal to DEFAULT_LEVEL
        defaultNhomThuThuatPhauThuatShouldBeFound("level.lessThanOrEqual=" + DEFAULT_LEVEL);

        // Get all the nhomThuThuatPhauThuatList where level is less than or equal to SMALLER_LEVEL
        defaultNhomThuThuatPhauThuatShouldNotBeFound("level.lessThanOrEqual=" + SMALLER_LEVEL);
    }

    @Test
    @Transactional
    public void getAllNhomThuThuatPhauThuatsByLevelIsLessThanSomething() throws Exception {
        // Initialize the database
        nhomThuThuatPhauThuatRepository.saveAndFlush(nhomThuThuatPhauThuat);

        // Get all the nhomThuThuatPhauThuatList where level is less than DEFAULT_LEVEL
        defaultNhomThuThuatPhauThuatShouldNotBeFound("level.lessThan=" + DEFAULT_LEVEL);

        // Get all the nhomThuThuatPhauThuatList where level is less than UPDATED_LEVEL
        defaultNhomThuThuatPhauThuatShouldBeFound("level.lessThan=" + UPDATED_LEVEL);
    }

    @Test
    @Transactional
    public void getAllNhomThuThuatPhauThuatsByLevelIsGreaterThanSomething() throws Exception {
        // Initialize the database
        nhomThuThuatPhauThuatRepository.saveAndFlush(nhomThuThuatPhauThuat);

        // Get all the nhomThuThuatPhauThuatList where level is greater than DEFAULT_LEVEL
        defaultNhomThuThuatPhauThuatShouldNotBeFound("level.greaterThan=" + DEFAULT_LEVEL);

        // Get all the nhomThuThuatPhauThuatList where level is greater than SMALLER_LEVEL
        defaultNhomThuThuatPhauThuatShouldBeFound("level.greaterThan=" + SMALLER_LEVEL);
    }


    @Test
    @Transactional
    public void getAllNhomThuThuatPhauThuatsByParentIdIsEqualToSomething() throws Exception {
        // Initialize the database
        nhomThuThuatPhauThuatRepository.saveAndFlush(nhomThuThuatPhauThuat);

        // Get all the nhomThuThuatPhauThuatList where parentId equals to DEFAULT_PARENT_ID
        defaultNhomThuThuatPhauThuatShouldBeFound("parentId.equals=" + DEFAULT_PARENT_ID);

        // Get all the nhomThuThuatPhauThuatList where parentId equals to UPDATED_PARENT_ID
        defaultNhomThuThuatPhauThuatShouldNotBeFound("parentId.equals=" + UPDATED_PARENT_ID);
    }

    @Test
    @Transactional
    public void getAllNhomThuThuatPhauThuatsByParentIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        nhomThuThuatPhauThuatRepository.saveAndFlush(nhomThuThuatPhauThuat);

        // Get all the nhomThuThuatPhauThuatList where parentId not equals to DEFAULT_PARENT_ID
        defaultNhomThuThuatPhauThuatShouldNotBeFound("parentId.notEquals=" + DEFAULT_PARENT_ID);

        // Get all the nhomThuThuatPhauThuatList where parentId not equals to UPDATED_PARENT_ID
        defaultNhomThuThuatPhauThuatShouldBeFound("parentId.notEquals=" + UPDATED_PARENT_ID);
    }

    @Test
    @Transactional
    public void getAllNhomThuThuatPhauThuatsByParentIdIsInShouldWork() throws Exception {
        // Initialize the database
        nhomThuThuatPhauThuatRepository.saveAndFlush(nhomThuThuatPhauThuat);

        // Get all the nhomThuThuatPhauThuatList where parentId in DEFAULT_PARENT_ID or UPDATED_PARENT_ID
        defaultNhomThuThuatPhauThuatShouldBeFound("parentId.in=" + DEFAULT_PARENT_ID + "," + UPDATED_PARENT_ID);

        // Get all the nhomThuThuatPhauThuatList where parentId equals to UPDATED_PARENT_ID
        defaultNhomThuThuatPhauThuatShouldNotBeFound("parentId.in=" + UPDATED_PARENT_ID);
    }

    @Test
    @Transactional
    public void getAllNhomThuThuatPhauThuatsByParentIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        nhomThuThuatPhauThuatRepository.saveAndFlush(nhomThuThuatPhauThuat);

        // Get all the nhomThuThuatPhauThuatList where parentId is not null
        defaultNhomThuThuatPhauThuatShouldBeFound("parentId.specified=true");

        // Get all the nhomThuThuatPhauThuatList where parentId is null
        defaultNhomThuThuatPhauThuatShouldNotBeFound("parentId.specified=false");
    }

    @Test
    @Transactional
    public void getAllNhomThuThuatPhauThuatsByParentIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        nhomThuThuatPhauThuatRepository.saveAndFlush(nhomThuThuatPhauThuat);

        // Get all the nhomThuThuatPhauThuatList where parentId is greater than or equal to DEFAULT_PARENT_ID
        defaultNhomThuThuatPhauThuatShouldBeFound("parentId.greaterThanOrEqual=" + DEFAULT_PARENT_ID);

        // Get all the nhomThuThuatPhauThuatList where parentId is greater than or equal to UPDATED_PARENT_ID
        defaultNhomThuThuatPhauThuatShouldNotBeFound("parentId.greaterThanOrEqual=" + UPDATED_PARENT_ID);
    }

    @Test
    @Transactional
    public void getAllNhomThuThuatPhauThuatsByParentIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        nhomThuThuatPhauThuatRepository.saveAndFlush(nhomThuThuatPhauThuat);

        // Get all the nhomThuThuatPhauThuatList where parentId is less than or equal to DEFAULT_PARENT_ID
        defaultNhomThuThuatPhauThuatShouldBeFound("parentId.lessThanOrEqual=" + DEFAULT_PARENT_ID);

        // Get all the nhomThuThuatPhauThuatList where parentId is less than or equal to SMALLER_PARENT_ID
        defaultNhomThuThuatPhauThuatShouldNotBeFound("parentId.lessThanOrEqual=" + SMALLER_PARENT_ID);
    }

    @Test
    @Transactional
    public void getAllNhomThuThuatPhauThuatsByParentIdIsLessThanSomething() throws Exception {
        // Initialize the database
        nhomThuThuatPhauThuatRepository.saveAndFlush(nhomThuThuatPhauThuat);

        // Get all the nhomThuThuatPhauThuatList where parentId is less than DEFAULT_PARENT_ID
        defaultNhomThuThuatPhauThuatShouldNotBeFound("parentId.lessThan=" + DEFAULT_PARENT_ID);

        // Get all the nhomThuThuatPhauThuatList where parentId is less than UPDATED_PARENT_ID
        defaultNhomThuThuatPhauThuatShouldBeFound("parentId.lessThan=" + UPDATED_PARENT_ID);
    }

    @Test
    @Transactional
    public void getAllNhomThuThuatPhauThuatsByParentIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        nhomThuThuatPhauThuatRepository.saveAndFlush(nhomThuThuatPhauThuat);

        // Get all the nhomThuThuatPhauThuatList where parentId is greater than DEFAULT_PARENT_ID
        defaultNhomThuThuatPhauThuatShouldNotBeFound("parentId.greaterThan=" + DEFAULT_PARENT_ID);

        // Get all the nhomThuThuatPhauThuatList where parentId is greater than SMALLER_PARENT_ID
        defaultNhomThuThuatPhauThuatShouldBeFound("parentId.greaterThan=" + SMALLER_PARENT_ID);
    }


    @Test
    @Transactional
    public void getAllNhomThuThuatPhauThuatsByTenIsEqualToSomething() throws Exception {
        // Initialize the database
        nhomThuThuatPhauThuatRepository.saveAndFlush(nhomThuThuatPhauThuat);

        // Get all the nhomThuThuatPhauThuatList where ten equals to DEFAULT_TEN
        defaultNhomThuThuatPhauThuatShouldBeFound("ten.equals=" + DEFAULT_TEN);

        // Get all the nhomThuThuatPhauThuatList where ten equals to UPDATED_TEN
        defaultNhomThuThuatPhauThuatShouldNotBeFound("ten.equals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllNhomThuThuatPhauThuatsByTenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        nhomThuThuatPhauThuatRepository.saveAndFlush(nhomThuThuatPhauThuat);

        // Get all the nhomThuThuatPhauThuatList where ten not equals to DEFAULT_TEN
        defaultNhomThuThuatPhauThuatShouldNotBeFound("ten.notEquals=" + DEFAULT_TEN);

        // Get all the nhomThuThuatPhauThuatList where ten not equals to UPDATED_TEN
        defaultNhomThuThuatPhauThuatShouldBeFound("ten.notEquals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllNhomThuThuatPhauThuatsByTenIsInShouldWork() throws Exception {
        // Initialize the database
        nhomThuThuatPhauThuatRepository.saveAndFlush(nhomThuThuatPhauThuat);

        // Get all the nhomThuThuatPhauThuatList where ten in DEFAULT_TEN or UPDATED_TEN
        defaultNhomThuThuatPhauThuatShouldBeFound("ten.in=" + DEFAULT_TEN + "," + UPDATED_TEN);

        // Get all the nhomThuThuatPhauThuatList where ten equals to UPDATED_TEN
        defaultNhomThuThuatPhauThuatShouldNotBeFound("ten.in=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllNhomThuThuatPhauThuatsByTenIsNullOrNotNull() throws Exception {
        // Initialize the database
        nhomThuThuatPhauThuatRepository.saveAndFlush(nhomThuThuatPhauThuat);

        // Get all the nhomThuThuatPhauThuatList where ten is not null
        defaultNhomThuThuatPhauThuatShouldBeFound("ten.specified=true");

        // Get all the nhomThuThuatPhauThuatList where ten is null
        defaultNhomThuThuatPhauThuatShouldNotBeFound("ten.specified=false");
    }
                @Test
    @Transactional
    public void getAllNhomThuThuatPhauThuatsByTenContainsSomething() throws Exception {
        // Initialize the database
        nhomThuThuatPhauThuatRepository.saveAndFlush(nhomThuThuatPhauThuat);

        // Get all the nhomThuThuatPhauThuatList where ten contains DEFAULT_TEN
        defaultNhomThuThuatPhauThuatShouldBeFound("ten.contains=" + DEFAULT_TEN);

        // Get all the nhomThuThuatPhauThuatList where ten contains UPDATED_TEN
        defaultNhomThuThuatPhauThuatShouldNotBeFound("ten.contains=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllNhomThuThuatPhauThuatsByTenNotContainsSomething() throws Exception {
        // Initialize the database
        nhomThuThuatPhauThuatRepository.saveAndFlush(nhomThuThuatPhauThuat);

        // Get all the nhomThuThuatPhauThuatList where ten does not contain DEFAULT_TEN
        defaultNhomThuThuatPhauThuatShouldNotBeFound("ten.doesNotContain=" + DEFAULT_TEN);

        // Get all the nhomThuThuatPhauThuatList where ten does not contain UPDATED_TEN
        defaultNhomThuThuatPhauThuatShouldBeFound("ten.doesNotContain=" + UPDATED_TEN);
    }


    @Test
    @Transactional
    public void getAllNhomThuThuatPhauThuatsByDonViIsEqualToSomething() throws Exception {
        // Get already existing entity
        DonVi donVi = nhomThuThuatPhauThuat.getDonVi();
        nhomThuThuatPhauThuatRepository.saveAndFlush(nhomThuThuatPhauThuat);
        Long donViId = donVi.getId();

        // Get all the nhomThuThuatPhauThuatList where donVi equals to donViId
        defaultNhomThuThuatPhauThuatShouldBeFound("donViId.equals=" + donViId);

        // Get all the nhomThuThuatPhauThuatList where donVi equals to donViId + 1
        defaultNhomThuThuatPhauThuatShouldNotBeFound("donViId.equals=" + (donViId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultNhomThuThuatPhauThuatShouldBeFound(String filter) throws Exception {
        restNhomThuThuatPhauThuatMockMvc.perform(get("/api/nhom-thu-thuat-phau-thuats?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(nhomThuThuatPhauThuat.getId().intValue())))
            .andExpect(jsonPath("$.[*].enable").value(hasItem(DEFAULT_ENABLE.booleanValue())))
            .andExpect(jsonPath("$.[*].level").value(hasItem(DEFAULT_LEVEL)))
            .andExpect(jsonPath("$.[*].parentId").value(hasItem(DEFAULT_PARENT_ID.intValue())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)));

        // Check, that the count call also returns 1
        restNhomThuThuatPhauThuatMockMvc.perform(get("/api/nhom-thu-thuat-phau-thuats/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultNhomThuThuatPhauThuatShouldNotBeFound(String filter) throws Exception {
        restNhomThuThuatPhauThuatMockMvc.perform(get("/api/nhom-thu-thuat-phau-thuats?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restNhomThuThuatPhauThuatMockMvc.perform(get("/api/nhom-thu-thuat-phau-thuats/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingNhomThuThuatPhauThuat() throws Exception {
        // Get the nhomThuThuatPhauThuat
        restNhomThuThuatPhauThuatMockMvc.perform(get("/api/nhom-thu-thuat-phau-thuats/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNhomThuThuatPhauThuat() throws Exception {
        // Initialize the database
        nhomThuThuatPhauThuatRepository.saveAndFlush(nhomThuThuatPhauThuat);

        int databaseSizeBeforeUpdate = nhomThuThuatPhauThuatRepository.findAll().size();

        // Update the nhomThuThuatPhauThuat
        NhomThuThuatPhauThuat updatedNhomThuThuatPhauThuat = nhomThuThuatPhauThuatRepository.findById(nhomThuThuatPhauThuat.getId()).get();
        // Disconnect from session so that the updates on updatedNhomThuThuatPhauThuat are not directly saved in db
        em.detach(updatedNhomThuThuatPhauThuat);
        updatedNhomThuThuatPhauThuat
            .enable(UPDATED_ENABLE)
            .level(UPDATED_LEVEL)
            .parentId(UPDATED_PARENT_ID)
            .ten(UPDATED_TEN);
        NhomThuThuatPhauThuatDTO nhomThuThuatPhauThuatDTO = nhomThuThuatPhauThuatMapper.toDto(updatedNhomThuThuatPhauThuat);

        restNhomThuThuatPhauThuatMockMvc.perform(put("/api/nhom-thu-thuat-phau-thuats").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(nhomThuThuatPhauThuatDTO)))
            .andExpect(status().isOk());

        // Validate the NhomThuThuatPhauThuat in the database
        List<NhomThuThuatPhauThuat> nhomThuThuatPhauThuatList = nhomThuThuatPhauThuatRepository.findAll();
        assertThat(nhomThuThuatPhauThuatList).hasSize(databaseSizeBeforeUpdate);
        NhomThuThuatPhauThuat testNhomThuThuatPhauThuat = nhomThuThuatPhauThuatList.get(nhomThuThuatPhauThuatList.size() - 1);
        assertThat(testNhomThuThuatPhauThuat.isEnable()).isEqualTo(UPDATED_ENABLE);
        assertThat(testNhomThuThuatPhauThuat.getLevel()).isEqualTo(UPDATED_LEVEL);
        assertThat(testNhomThuThuatPhauThuat.getParentId()).isEqualTo(UPDATED_PARENT_ID);
        assertThat(testNhomThuThuatPhauThuat.getTen()).isEqualTo(UPDATED_TEN);
    }

    @Test
    @Transactional
    public void updateNonExistingNhomThuThuatPhauThuat() throws Exception {
        int databaseSizeBeforeUpdate = nhomThuThuatPhauThuatRepository.findAll().size();

        // Create the NhomThuThuatPhauThuat
        NhomThuThuatPhauThuatDTO nhomThuThuatPhauThuatDTO = nhomThuThuatPhauThuatMapper.toDto(nhomThuThuatPhauThuat);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restNhomThuThuatPhauThuatMockMvc.perform(put("/api/nhom-thu-thuat-phau-thuats").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(nhomThuThuatPhauThuatDTO)))
            .andExpect(status().isBadRequest());

        // Validate the NhomThuThuatPhauThuat in the database
        List<NhomThuThuatPhauThuat> nhomThuThuatPhauThuatList = nhomThuThuatPhauThuatRepository.findAll();
        assertThat(nhomThuThuatPhauThuatList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteNhomThuThuatPhauThuat() throws Exception {
        // Initialize the database
        nhomThuThuatPhauThuatRepository.saveAndFlush(nhomThuThuatPhauThuat);

        int databaseSizeBeforeDelete = nhomThuThuatPhauThuatRepository.findAll().size();

        // Delete the nhomThuThuatPhauThuat
        restNhomThuThuatPhauThuatMockMvc.perform(delete("/api/nhom-thu-thuat-phau-thuats/{id}", nhomThuThuatPhauThuat.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<NhomThuThuatPhauThuat> nhomThuThuatPhauThuatList = nhomThuThuatPhauThuatRepository.findAll();
        assertThat(nhomThuThuatPhauThuatList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.ChiDinhThuThuatPhauThuat;
import vn.vnpt.domain.PhieuChiDinhTTPT;
import vn.vnpt.domain.Phong;
import vn.vnpt.domain.ThuThuatPhauThuat;
import vn.vnpt.repository.ChiDinhThuThuatPhauThuatRepository;
import vn.vnpt.service.ChiDinhThuThuatPhauThuatService;
import vn.vnpt.service.dto.ChiDinhThuThuatPhauThuatDTO;
import vn.vnpt.service.mapper.ChiDinhThuThuatPhauThuatMapper;
import vn.vnpt.service.dto.ChiDinhThuThuatPhauThuatCriteria;
import vn.vnpt.service.ChiDinhThuThuatPhauThuatQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ChiDinhThuThuatPhauThuatResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class ChiDinhThuThuatPhauThuatResourceIT {

    private static final Boolean DEFAULT_CO_BAO_HIEM = false;
    private static final Boolean UPDATED_CO_BAO_HIEM = true;

    private static final Boolean DEFAULT_CO_KET_QUA = false;
    private static final Boolean UPDATED_CO_KET_QUA = true;

    private static final Integer DEFAULT_DA_THANH_TOAN = 1;
    private static final Integer UPDATED_DA_THANH_TOAN = 2;
    private static final Integer SMALLER_DA_THANH_TOAN = 1 - 1;

    private static final Integer DEFAULT_DA_THANH_TOAN_CHENH_LECH = 1;
    private static final Integer UPDATED_DA_THANH_TOAN_CHENH_LECH = 2;
    private static final Integer SMALLER_DA_THANH_TOAN_CHENH_LECH = 1 - 1;

    private static final Integer DEFAULT_DA_THUC_HIEN = 1;
    private static final Integer UPDATED_DA_THUC_HIEN = 2;
    private static final Integer SMALLER_DA_THUC_HIEN = 1 - 1;

    private static final BigDecimal DEFAULT_DON_GIA = new BigDecimal(1);
    private static final BigDecimal UPDATED_DON_GIA = new BigDecimal(2);
    private static final BigDecimal SMALLER_DON_GIA = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_DON_GIA_BHYT = new BigDecimal(1);
    private static final BigDecimal UPDATED_DON_GIA_BHYT = new BigDecimal(2);
    private static final BigDecimal SMALLER_DON_GIA_BHYT = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_DON_GIA_KHONG_BHYT = new BigDecimal(1);
    private static final BigDecimal UPDATED_DON_GIA_KHONG_BHYT = new BigDecimal(2);
    private static final BigDecimal SMALLER_DON_GIA_KHONG_BHYT = new BigDecimal(1 - 1);

    private static final String DEFAULT_GHI_CHU_CHI_DINH = "AAAAAAAAAA";
    private static final String UPDATED_GHI_CHU_CHI_DINH = "BBBBBBBBBB";

    private static final String DEFAULT_MO_TA = "AAAAAAAAAA";
    private static final String UPDATED_MO_TA = "BBBBBBBBBB";

    private static final Long DEFAULT_NGUOI_CHI_DINH_ID = 1L;
    private static final Long UPDATED_NGUOI_CHI_DINH_ID = 2L;
    private static final Long SMALLER_NGUOI_CHI_DINH_ID = 1L - 1L;

    private static final Integer DEFAULT_SO_LUONG = 1;
    private static final Integer UPDATED_SO_LUONG = 2;
    private static final Integer SMALLER_SO_LUONG = 1 - 1;

    private static final String DEFAULT_TEN = "AAAAAAAAAA";
    private static final String UPDATED_TEN = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_THANH_TIEN = new BigDecimal(1);
    private static final BigDecimal UPDATED_THANH_TIEN = new BigDecimal(2);
    private static final BigDecimal SMALLER_THANH_TIEN = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_THANH_TIEN_BHYT = new BigDecimal(1);
    private static final BigDecimal UPDATED_THANH_TIEN_BHYT = new BigDecimal(2);
    private static final BigDecimal SMALLER_THANH_TIEN_BHYT = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_THANH_TIEN_KHONG_BHYT = new BigDecimal(1);
    private static final BigDecimal UPDATED_THANH_TIEN_KHONG_BHYT = new BigDecimal(2);
    private static final BigDecimal SMALLER_THANH_TIEN_KHONG_BHYT = new BigDecimal(1 - 1);

    private static final LocalDate DEFAULT_THOI_GIAN_CHI_DINH = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_THOI_GIAN_CHI_DINH = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_THOI_GIAN_CHI_DINH = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_THOI_GIAN_TAO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_THOI_GIAN_TAO = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_THOI_GIAN_TAO = LocalDate.ofEpochDay(-1L);

    private static final BigDecimal DEFAULT_TIEN_NGOAI_BHYT = new BigDecimal(1);
    private static final BigDecimal UPDATED_TIEN_NGOAI_BHYT = new BigDecimal(2);
    private static final BigDecimal SMALLER_TIEN_NGOAI_BHYT = new BigDecimal(1 - 1);

    private static final Boolean DEFAULT_THANH_TOAN_CHENH_LECH = false;
    private static final Boolean UPDATED_THANH_TOAN_CHENH_LECH = true;

    private static final Integer DEFAULT_TY_LE_THANH_TOAN = 1;
    private static final Integer UPDATED_TY_LE_THANH_TOAN = 2;
    private static final Integer SMALLER_TY_LE_THANH_TOAN = 1 - 1;

    private static final String DEFAULT_MA_DUNG_CHUNG = "AAAAAAAAAA";
    private static final String UPDATED_MA_DUNG_CHUNG = "BBBBBBBBBB";

    private static final Boolean DEFAULT_DICH_VU_YEU_CAU = false;
    private static final Boolean UPDATED_DICH_VU_YEU_CAU = true;

    private static final Integer DEFAULT_NAM = 1;
    private static final Integer UPDATED_NAM = 2;
    private static final Integer SMALLER_NAM = 1 - 1;

    @Autowired
    private ChiDinhThuThuatPhauThuatRepository chiDinhThuThuatPhauThuatRepository;

    @Autowired
    private ChiDinhThuThuatPhauThuatMapper chiDinhThuThuatPhauThuatMapper;

    @Autowired
    private ChiDinhThuThuatPhauThuatService chiDinhThuThuatPhauThuatService;

    @Autowired
    private ChiDinhThuThuatPhauThuatQueryService chiDinhThuThuatPhauThuatQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restChiDinhThuThuatPhauThuatMockMvc;

    private ChiDinhThuThuatPhauThuat chiDinhThuThuatPhauThuat;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ChiDinhThuThuatPhauThuat createEntity(EntityManager em) {
        ChiDinhThuThuatPhauThuat chiDinhThuThuatPhauThuat = new ChiDinhThuThuatPhauThuat()
            .coBaoHiem(DEFAULT_CO_BAO_HIEM)
            .coKetQua(DEFAULT_CO_KET_QUA)
            .daThanhToan(DEFAULT_DA_THANH_TOAN)
            .daThanhToanChenhLech(DEFAULT_DA_THANH_TOAN_CHENH_LECH)
            .daThucHien(DEFAULT_DA_THUC_HIEN)
            .donGia(DEFAULT_DON_GIA)
            .donGiaBhyt(DEFAULT_DON_GIA_BHYT)
            .donGiaKhongBhyt(DEFAULT_DON_GIA_KHONG_BHYT)
            .ghiChuChiDinh(DEFAULT_GHI_CHU_CHI_DINH)
            .moTa(DEFAULT_MO_TA)
            .nguoiChiDinhId(DEFAULT_NGUOI_CHI_DINH_ID)
            .soLuong(DEFAULT_SO_LUONG)
            .ten(DEFAULT_TEN)
            .thanhTien(DEFAULT_THANH_TIEN)
            .thanhTienBhyt(DEFAULT_THANH_TIEN_BHYT)
            .thanhTienKhongBHYT(DEFAULT_THANH_TIEN_KHONG_BHYT)
            .thoiGianChiDinh(DEFAULT_THOI_GIAN_CHI_DINH)
            .thoiGianTao(DEFAULT_THOI_GIAN_TAO)
            .tienNgoaiBHYT(DEFAULT_TIEN_NGOAI_BHYT)
            .thanhToanChenhLech(DEFAULT_THANH_TOAN_CHENH_LECH)
            .tyLeThanhToan(DEFAULT_TY_LE_THANH_TOAN)
            .maDungChung(DEFAULT_MA_DUNG_CHUNG)
            .dichVuYeuCau(DEFAULT_DICH_VU_YEU_CAU)
            .nam(DEFAULT_NAM);
        // Add required entity
        PhieuChiDinhTTPT phieuChiDinhTTPT;
        if (TestUtil.findAll(em, PhieuChiDinhTTPT.class).isEmpty()) {
            phieuChiDinhTTPT = PhieuChiDinhTTPTResourceIT.createEntity(em);
            em.persist(phieuChiDinhTTPT);
            em.flush();
        } else {
            phieuChiDinhTTPT = TestUtil.findAll(em, PhieuChiDinhTTPT.class).get(0);
        }
        chiDinhThuThuatPhauThuat.setDonVi(phieuChiDinhTTPT);
        // Add required entity
        chiDinhThuThuatPhauThuat.setBenhNhan(phieuChiDinhTTPT);
        // Add required entity
        chiDinhThuThuatPhauThuat.setBakb(phieuChiDinhTTPT);
        // Add required entity
        chiDinhThuThuatPhauThuat.setPhieuCD(phieuChiDinhTTPT);
        // Add required entity
        Phong phong;
        if (TestUtil.findAll(em, Phong.class).isEmpty()) {
            phong = PhongResourceIT.createEntity(em);
            em.persist(phong);
            em.flush();
        } else {
            phong = TestUtil.findAll(em, Phong.class).get(0);
        }
        chiDinhThuThuatPhauThuat.setPhong(phong);
        // Add required entity
        ThuThuatPhauThuat thuThuatPhauThuat;
        if (TestUtil.findAll(em, ThuThuatPhauThuat.class).isEmpty()) {
            thuThuatPhauThuat = ThuThuatPhauThuatResourceIT.createEntity(em);
            em.persist(thuThuatPhauThuat);
            em.flush();
        } else {
            thuThuatPhauThuat = TestUtil.findAll(em, ThuThuatPhauThuat.class).get(0);
        }
        chiDinhThuThuatPhauThuat.setTtpt(thuThuatPhauThuat);
        return chiDinhThuThuatPhauThuat;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ChiDinhThuThuatPhauThuat createUpdatedEntity(EntityManager em) {
        ChiDinhThuThuatPhauThuat chiDinhThuThuatPhauThuat = new ChiDinhThuThuatPhauThuat()
            .coBaoHiem(UPDATED_CO_BAO_HIEM)
            .coKetQua(UPDATED_CO_KET_QUA)
            .daThanhToan(UPDATED_DA_THANH_TOAN)
            .daThanhToanChenhLech(UPDATED_DA_THANH_TOAN_CHENH_LECH)
            .daThucHien(UPDATED_DA_THUC_HIEN)
            .donGia(UPDATED_DON_GIA)
            .donGiaBhyt(UPDATED_DON_GIA_BHYT)
            .donGiaKhongBhyt(UPDATED_DON_GIA_KHONG_BHYT)
            .ghiChuChiDinh(UPDATED_GHI_CHU_CHI_DINH)
            .moTa(UPDATED_MO_TA)
            .nguoiChiDinhId(UPDATED_NGUOI_CHI_DINH_ID)
            .soLuong(UPDATED_SO_LUONG)
            .ten(UPDATED_TEN)
            .thanhTien(UPDATED_THANH_TIEN)
            .thanhTienBhyt(UPDATED_THANH_TIEN_BHYT)
            .thanhTienKhongBHYT(UPDATED_THANH_TIEN_KHONG_BHYT)
            .thoiGianChiDinh(UPDATED_THOI_GIAN_CHI_DINH)
            .thoiGianTao(UPDATED_THOI_GIAN_TAO)
            .tienNgoaiBHYT(UPDATED_TIEN_NGOAI_BHYT)
            .thanhToanChenhLech(UPDATED_THANH_TOAN_CHENH_LECH)
            .tyLeThanhToan(UPDATED_TY_LE_THANH_TOAN)
            .maDungChung(UPDATED_MA_DUNG_CHUNG)
            .dichVuYeuCau(UPDATED_DICH_VU_YEU_CAU)
            .nam(UPDATED_NAM);
        // Add required entity
        PhieuChiDinhTTPT phieuChiDinhTTPT;
        if (TestUtil.findAll(em, PhieuChiDinhTTPT.class).isEmpty()) {
            phieuChiDinhTTPT = PhieuChiDinhTTPTResourceIT.createUpdatedEntity(em);
            em.persist(phieuChiDinhTTPT);
            em.flush();
        } else {
            phieuChiDinhTTPT = TestUtil.findAll(em, PhieuChiDinhTTPT.class).get(0);
        }
        chiDinhThuThuatPhauThuat.setDonVi(phieuChiDinhTTPT);
        // Add required entity
        chiDinhThuThuatPhauThuat.setBenhNhan(phieuChiDinhTTPT);
        // Add required entity
        chiDinhThuThuatPhauThuat.setBakb(phieuChiDinhTTPT);
        // Add required entity
        chiDinhThuThuatPhauThuat.setPhieuCD(phieuChiDinhTTPT);
        // Add required entity
        Phong phong;
        if (TestUtil.findAll(em, Phong.class).isEmpty()) {
            phong = PhongResourceIT.createUpdatedEntity(em);
            em.persist(phong);
            em.flush();
        } else {
            phong = TestUtil.findAll(em, Phong.class).get(0);
        }
        chiDinhThuThuatPhauThuat.setPhong(phong);
        // Add required entity
        ThuThuatPhauThuat thuThuatPhauThuat;
        if (TestUtil.findAll(em, ThuThuatPhauThuat.class).isEmpty()) {
            thuThuatPhauThuat = ThuThuatPhauThuatResourceIT.createUpdatedEntity(em);
            em.persist(thuThuatPhauThuat);
            em.flush();
        } else {
            thuThuatPhauThuat = TestUtil.findAll(em, ThuThuatPhauThuat.class).get(0);
        }
        chiDinhThuThuatPhauThuat.setTtpt(thuThuatPhauThuat);
        return chiDinhThuThuatPhauThuat;
    }

    @BeforeEach
    public void initTest() {
        chiDinhThuThuatPhauThuat = createEntity(em);
    }

    @Test
    @Transactional
    public void createChiDinhThuThuatPhauThuat() throws Exception {
        int databaseSizeBeforeCreate = chiDinhThuThuatPhauThuatRepository.findAll().size();

        // Create the ChiDinhThuThuatPhauThuat
        ChiDinhThuThuatPhauThuatDTO chiDinhThuThuatPhauThuatDTO = chiDinhThuThuatPhauThuatMapper.toDto(chiDinhThuThuatPhauThuat);
        restChiDinhThuThuatPhauThuatMockMvc.perform(post("/api/chi-dinh-thu-thuat-phau-thuats").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhThuThuatPhauThuatDTO)))
            .andExpect(status().isCreated());

        // Validate the ChiDinhThuThuatPhauThuat in the database
        List<ChiDinhThuThuatPhauThuat> chiDinhThuThuatPhauThuatList = chiDinhThuThuatPhauThuatRepository.findAll();
        assertThat(chiDinhThuThuatPhauThuatList).hasSize(databaseSizeBeforeCreate + 1);
        ChiDinhThuThuatPhauThuat testChiDinhThuThuatPhauThuat = chiDinhThuThuatPhauThuatList.get(chiDinhThuThuatPhauThuatList.size() - 1);
        assertThat(testChiDinhThuThuatPhauThuat.isCoBaoHiem()).isEqualTo(DEFAULT_CO_BAO_HIEM);
        assertThat(testChiDinhThuThuatPhauThuat.isCoKetQua()).isEqualTo(DEFAULT_CO_KET_QUA);
        assertThat(testChiDinhThuThuatPhauThuat.getDaThanhToan()).isEqualTo(DEFAULT_DA_THANH_TOAN);
        assertThat(testChiDinhThuThuatPhauThuat.getDaThanhToanChenhLech()).isEqualTo(DEFAULT_DA_THANH_TOAN_CHENH_LECH);
        assertThat(testChiDinhThuThuatPhauThuat.getDaThucHien()).isEqualTo(DEFAULT_DA_THUC_HIEN);
        assertThat(testChiDinhThuThuatPhauThuat.getDonGia()).isEqualTo(DEFAULT_DON_GIA);
        assertThat(testChiDinhThuThuatPhauThuat.getDonGiaBhyt()).isEqualTo(DEFAULT_DON_GIA_BHYT);
        assertThat(testChiDinhThuThuatPhauThuat.getDonGiaKhongBhyt()).isEqualTo(DEFAULT_DON_GIA_KHONG_BHYT);
        assertThat(testChiDinhThuThuatPhauThuat.getGhiChuChiDinh()).isEqualTo(DEFAULT_GHI_CHU_CHI_DINH);
        assertThat(testChiDinhThuThuatPhauThuat.getMoTa()).isEqualTo(DEFAULT_MO_TA);
        assertThat(testChiDinhThuThuatPhauThuat.getNguoiChiDinhId()).isEqualTo(DEFAULT_NGUOI_CHI_DINH_ID);
        assertThat(testChiDinhThuThuatPhauThuat.getSoLuong()).isEqualTo(DEFAULT_SO_LUONG);
        assertThat(testChiDinhThuThuatPhauThuat.getTen()).isEqualTo(DEFAULT_TEN);
        assertThat(testChiDinhThuThuatPhauThuat.getThanhTien()).isEqualTo(DEFAULT_THANH_TIEN);
        assertThat(testChiDinhThuThuatPhauThuat.getThanhTienBhyt()).isEqualTo(DEFAULT_THANH_TIEN_BHYT);
        assertThat(testChiDinhThuThuatPhauThuat.getThanhTienKhongBHYT()).isEqualTo(DEFAULT_THANH_TIEN_KHONG_BHYT);
        assertThat(testChiDinhThuThuatPhauThuat.getThoiGianChiDinh()).isEqualTo(DEFAULT_THOI_GIAN_CHI_DINH);
        assertThat(testChiDinhThuThuatPhauThuat.getThoiGianTao()).isEqualTo(DEFAULT_THOI_GIAN_TAO);
        assertThat(testChiDinhThuThuatPhauThuat.getTienNgoaiBHYT()).isEqualTo(DEFAULT_TIEN_NGOAI_BHYT);
        assertThat(testChiDinhThuThuatPhauThuat.isThanhToanChenhLech()).isEqualTo(DEFAULT_THANH_TOAN_CHENH_LECH);
        assertThat(testChiDinhThuThuatPhauThuat.getTyLeThanhToan()).isEqualTo(DEFAULT_TY_LE_THANH_TOAN);
        assertThat(testChiDinhThuThuatPhauThuat.getMaDungChung()).isEqualTo(DEFAULT_MA_DUNG_CHUNG);
        assertThat(testChiDinhThuThuatPhauThuat.isDichVuYeuCau()).isEqualTo(DEFAULT_DICH_VU_YEU_CAU);
        assertThat(testChiDinhThuThuatPhauThuat.getNam()).isEqualTo(DEFAULT_NAM);
    }

    @Test
    @Transactional
    public void createChiDinhThuThuatPhauThuatWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = chiDinhThuThuatPhauThuatRepository.findAll().size();

        // Create the ChiDinhThuThuatPhauThuat with an existing ID
        chiDinhThuThuatPhauThuat.setId(1L);
        ChiDinhThuThuatPhauThuatDTO chiDinhThuThuatPhauThuatDTO = chiDinhThuThuatPhauThuatMapper.toDto(chiDinhThuThuatPhauThuat);

        // An entity with an existing ID cannot be created, so this API call must fail
        restChiDinhThuThuatPhauThuatMockMvc.perform(post("/api/chi-dinh-thu-thuat-phau-thuats").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhThuThuatPhauThuatDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ChiDinhThuThuatPhauThuat in the database
        List<ChiDinhThuThuatPhauThuat> chiDinhThuThuatPhauThuatList = chiDinhThuThuatPhauThuatRepository.findAll();
        assertThat(chiDinhThuThuatPhauThuatList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkCoBaoHiemIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiDinhThuThuatPhauThuatRepository.findAll().size();
        // set the field null
        chiDinhThuThuatPhauThuat.setCoBaoHiem(null);

        // Create the ChiDinhThuThuatPhauThuat, which fails.
        ChiDinhThuThuatPhauThuatDTO chiDinhThuThuatPhauThuatDTO = chiDinhThuThuatPhauThuatMapper.toDto(chiDinhThuThuatPhauThuat);

        restChiDinhThuThuatPhauThuatMockMvc.perform(post("/api/chi-dinh-thu-thuat-phau-thuats").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhThuThuatPhauThuatDTO)))
            .andExpect(status().isBadRequest());

        List<ChiDinhThuThuatPhauThuat> chiDinhThuThuatPhauThuatList = chiDinhThuThuatPhauThuatRepository.findAll();
        assertThat(chiDinhThuThuatPhauThuatList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCoKetQuaIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiDinhThuThuatPhauThuatRepository.findAll().size();
        // set the field null
        chiDinhThuThuatPhauThuat.setCoKetQua(null);

        // Create the ChiDinhThuThuatPhauThuat, which fails.
        ChiDinhThuThuatPhauThuatDTO chiDinhThuThuatPhauThuatDTO = chiDinhThuThuatPhauThuatMapper.toDto(chiDinhThuThuatPhauThuat);

        restChiDinhThuThuatPhauThuatMockMvc.perform(post("/api/chi-dinh-thu-thuat-phau-thuats").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhThuThuatPhauThuatDTO)))
            .andExpect(status().isBadRequest());

        List<ChiDinhThuThuatPhauThuat> chiDinhThuThuatPhauThuatList = chiDinhThuThuatPhauThuatRepository.findAll();
        assertThat(chiDinhThuThuatPhauThuatList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDaThanhToanIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiDinhThuThuatPhauThuatRepository.findAll().size();
        // set the field null
        chiDinhThuThuatPhauThuat.setDaThanhToan(null);

        // Create the ChiDinhThuThuatPhauThuat, which fails.
        ChiDinhThuThuatPhauThuatDTO chiDinhThuThuatPhauThuatDTO = chiDinhThuThuatPhauThuatMapper.toDto(chiDinhThuThuatPhauThuat);

        restChiDinhThuThuatPhauThuatMockMvc.perform(post("/api/chi-dinh-thu-thuat-phau-thuats").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhThuThuatPhauThuatDTO)))
            .andExpect(status().isBadRequest());

        List<ChiDinhThuThuatPhauThuat> chiDinhThuThuatPhauThuatList = chiDinhThuThuatPhauThuatRepository.findAll();
        assertThat(chiDinhThuThuatPhauThuatList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDaThanhToanChenhLechIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiDinhThuThuatPhauThuatRepository.findAll().size();
        // set the field null
        chiDinhThuThuatPhauThuat.setDaThanhToanChenhLech(null);

        // Create the ChiDinhThuThuatPhauThuat, which fails.
        ChiDinhThuThuatPhauThuatDTO chiDinhThuThuatPhauThuatDTO = chiDinhThuThuatPhauThuatMapper.toDto(chiDinhThuThuatPhauThuat);

        restChiDinhThuThuatPhauThuatMockMvc.perform(post("/api/chi-dinh-thu-thuat-phau-thuats").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhThuThuatPhauThuatDTO)))
            .andExpect(status().isBadRequest());

        List<ChiDinhThuThuatPhauThuat> chiDinhThuThuatPhauThuatList = chiDinhThuThuatPhauThuatRepository.findAll();
        assertThat(chiDinhThuThuatPhauThuatList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDaThucHienIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiDinhThuThuatPhauThuatRepository.findAll().size();
        // set the field null
        chiDinhThuThuatPhauThuat.setDaThucHien(null);

        // Create the ChiDinhThuThuatPhauThuat, which fails.
        ChiDinhThuThuatPhauThuatDTO chiDinhThuThuatPhauThuatDTO = chiDinhThuThuatPhauThuatMapper.toDto(chiDinhThuThuatPhauThuat);

        restChiDinhThuThuatPhauThuatMockMvc.perform(post("/api/chi-dinh-thu-thuat-phau-thuats").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhThuThuatPhauThuatDTO)))
            .andExpect(status().isBadRequest());

        List<ChiDinhThuThuatPhauThuat> chiDinhThuThuatPhauThuatList = chiDinhThuThuatPhauThuatRepository.findAll();
        assertThat(chiDinhThuThuatPhauThuatList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDonGiaIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiDinhThuThuatPhauThuatRepository.findAll().size();
        // set the field null
        chiDinhThuThuatPhauThuat.setDonGia(null);

        // Create the ChiDinhThuThuatPhauThuat, which fails.
        ChiDinhThuThuatPhauThuatDTO chiDinhThuThuatPhauThuatDTO = chiDinhThuThuatPhauThuatMapper.toDto(chiDinhThuThuatPhauThuat);

        restChiDinhThuThuatPhauThuatMockMvc.perform(post("/api/chi-dinh-thu-thuat-phau-thuats").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhThuThuatPhauThuatDTO)))
            .andExpect(status().isBadRequest());

        List<ChiDinhThuThuatPhauThuat> chiDinhThuThuatPhauThuatList = chiDinhThuThuatPhauThuatRepository.findAll();
        assertThat(chiDinhThuThuatPhauThuatList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDonGiaBhytIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiDinhThuThuatPhauThuatRepository.findAll().size();
        // set the field null
        chiDinhThuThuatPhauThuat.setDonGiaBhyt(null);

        // Create the ChiDinhThuThuatPhauThuat, which fails.
        ChiDinhThuThuatPhauThuatDTO chiDinhThuThuatPhauThuatDTO = chiDinhThuThuatPhauThuatMapper.toDto(chiDinhThuThuatPhauThuat);

        restChiDinhThuThuatPhauThuatMockMvc.perform(post("/api/chi-dinh-thu-thuat-phau-thuats").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhThuThuatPhauThuatDTO)))
            .andExpect(status().isBadRequest());

        List<ChiDinhThuThuatPhauThuat> chiDinhThuThuatPhauThuatList = chiDinhThuThuatPhauThuatRepository.findAll();
        assertThat(chiDinhThuThuatPhauThuatList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkThanhTienIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiDinhThuThuatPhauThuatRepository.findAll().size();
        // set the field null
        chiDinhThuThuatPhauThuat.setThanhTien(null);

        // Create the ChiDinhThuThuatPhauThuat, which fails.
        ChiDinhThuThuatPhauThuatDTO chiDinhThuThuatPhauThuatDTO = chiDinhThuThuatPhauThuatMapper.toDto(chiDinhThuThuatPhauThuat);

        restChiDinhThuThuatPhauThuatMockMvc.perform(post("/api/chi-dinh-thu-thuat-phau-thuats").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhThuThuatPhauThuatDTO)))
            .andExpect(status().isBadRequest());

        List<ChiDinhThuThuatPhauThuat> chiDinhThuThuatPhauThuatList = chiDinhThuThuatPhauThuatRepository.findAll();
        assertThat(chiDinhThuThuatPhauThuatList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkThoiGianTaoIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiDinhThuThuatPhauThuatRepository.findAll().size();
        // set the field null
        chiDinhThuThuatPhauThuat.setThoiGianTao(null);

        // Create the ChiDinhThuThuatPhauThuat, which fails.
        ChiDinhThuThuatPhauThuatDTO chiDinhThuThuatPhauThuatDTO = chiDinhThuThuatPhauThuatMapper.toDto(chiDinhThuThuatPhauThuat);

        restChiDinhThuThuatPhauThuatMockMvc.perform(post("/api/chi-dinh-thu-thuat-phau-thuats").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhThuThuatPhauThuatDTO)))
            .andExpect(status().isBadRequest());

        List<ChiDinhThuThuatPhauThuat> chiDinhThuThuatPhauThuatList = chiDinhThuThuatPhauThuatRepository.findAll();
        assertThat(chiDinhThuThuatPhauThuatList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTienNgoaiBHYTIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiDinhThuThuatPhauThuatRepository.findAll().size();
        // set the field null
        chiDinhThuThuatPhauThuat.setTienNgoaiBHYT(null);

        // Create the ChiDinhThuThuatPhauThuat, which fails.
        ChiDinhThuThuatPhauThuatDTO chiDinhThuThuatPhauThuatDTO = chiDinhThuThuatPhauThuatMapper.toDto(chiDinhThuThuatPhauThuat);

        restChiDinhThuThuatPhauThuatMockMvc.perform(post("/api/chi-dinh-thu-thuat-phau-thuats").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhThuThuatPhauThuatDTO)))
            .andExpect(status().isBadRequest());

        List<ChiDinhThuThuatPhauThuat> chiDinhThuThuatPhauThuatList = chiDinhThuThuatPhauThuatRepository.findAll();
        assertThat(chiDinhThuThuatPhauThuatList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkThanhToanChenhLechIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiDinhThuThuatPhauThuatRepository.findAll().size();
        // set the field null
        chiDinhThuThuatPhauThuat.setThanhToanChenhLech(null);

        // Create the ChiDinhThuThuatPhauThuat, which fails.
        ChiDinhThuThuatPhauThuatDTO chiDinhThuThuatPhauThuatDTO = chiDinhThuThuatPhauThuatMapper.toDto(chiDinhThuThuatPhauThuat);

        restChiDinhThuThuatPhauThuatMockMvc.perform(post("/api/chi-dinh-thu-thuat-phau-thuats").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhThuThuatPhauThuatDTO)))
            .andExpect(status().isBadRequest());

        List<ChiDinhThuThuatPhauThuat> chiDinhThuThuatPhauThuatList = chiDinhThuThuatPhauThuatRepository.findAll();
        assertThat(chiDinhThuThuatPhauThuatList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNamIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiDinhThuThuatPhauThuatRepository.findAll().size();
        // set the field null
        chiDinhThuThuatPhauThuat.setNam(null);

        // Create the ChiDinhThuThuatPhauThuat, which fails.
        ChiDinhThuThuatPhauThuatDTO chiDinhThuThuatPhauThuatDTO = chiDinhThuThuatPhauThuatMapper.toDto(chiDinhThuThuatPhauThuat);

        restChiDinhThuThuatPhauThuatMockMvc.perform(post("/api/chi-dinh-thu-thuat-phau-thuats").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhThuThuatPhauThuatDTO)))
            .andExpect(status().isBadRequest());

        List<ChiDinhThuThuatPhauThuat> chiDinhThuThuatPhauThuatList = chiDinhThuThuatPhauThuatRepository.findAll();
        assertThat(chiDinhThuThuatPhauThuatList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuats() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList
        restChiDinhThuThuatPhauThuatMockMvc.perform(get("/api/chi-dinh-thu-thuat-phau-thuats?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(chiDinhThuThuatPhauThuat.getId().intValue())))
            .andExpect(jsonPath("$.[*].coBaoHiem").value(hasItem(DEFAULT_CO_BAO_HIEM.booleanValue())))
            .andExpect(jsonPath("$.[*].coKetQua").value(hasItem(DEFAULT_CO_KET_QUA.booleanValue())))
            .andExpect(jsonPath("$.[*].daThanhToan").value(hasItem(DEFAULT_DA_THANH_TOAN)))
            .andExpect(jsonPath("$.[*].daThanhToanChenhLech").value(hasItem(DEFAULT_DA_THANH_TOAN_CHENH_LECH)))
            .andExpect(jsonPath("$.[*].daThucHien").value(hasItem(DEFAULT_DA_THUC_HIEN)))
            .andExpect(jsonPath("$.[*].donGia").value(hasItem(DEFAULT_DON_GIA.intValue())))
            .andExpect(jsonPath("$.[*].donGiaBhyt").value(hasItem(DEFAULT_DON_GIA_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].donGiaKhongBhyt").value(hasItem(DEFAULT_DON_GIA_KHONG_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].ghiChuChiDinh").value(hasItem(DEFAULT_GHI_CHU_CHI_DINH)))
            .andExpect(jsonPath("$.[*].moTa").value(hasItem(DEFAULT_MO_TA)))
            .andExpect(jsonPath("$.[*].nguoiChiDinhId").value(hasItem(DEFAULT_NGUOI_CHI_DINH_ID.intValue())))
            .andExpect(jsonPath("$.[*].soLuong").value(hasItem(DEFAULT_SO_LUONG)))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].thanhTien").value(hasItem(DEFAULT_THANH_TIEN.intValue())))
            .andExpect(jsonPath("$.[*].thanhTienBhyt").value(hasItem(DEFAULT_THANH_TIEN_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].thanhTienKhongBHYT").value(hasItem(DEFAULT_THANH_TIEN_KHONG_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].thoiGianChiDinh").value(hasItem(DEFAULT_THOI_GIAN_CHI_DINH.toString())))
            .andExpect(jsonPath("$.[*].thoiGianTao").value(hasItem(DEFAULT_THOI_GIAN_TAO.toString())))
            .andExpect(jsonPath("$.[*].tienNgoaiBHYT").value(hasItem(DEFAULT_TIEN_NGOAI_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].thanhToanChenhLech").value(hasItem(DEFAULT_THANH_TOAN_CHENH_LECH.booleanValue())))
            .andExpect(jsonPath("$.[*].tyLeThanhToan").value(hasItem(DEFAULT_TY_LE_THANH_TOAN)))
            .andExpect(jsonPath("$.[*].maDungChung").value(hasItem(DEFAULT_MA_DUNG_CHUNG)))
            .andExpect(jsonPath("$.[*].dichVuYeuCau").value(hasItem(DEFAULT_DICH_VU_YEU_CAU.booleanValue())))
            .andExpect(jsonPath("$.[*].nam").value(hasItem(DEFAULT_NAM)));
    }
    
    @Test
    @Transactional
    public void getChiDinhThuThuatPhauThuat() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get the chiDinhThuThuatPhauThuat
        restChiDinhThuThuatPhauThuatMockMvc.perform(get("/api/chi-dinh-thu-thuat-phau-thuats/{id}", chiDinhThuThuatPhauThuat.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(chiDinhThuThuatPhauThuat.getId().intValue()))
            .andExpect(jsonPath("$.coBaoHiem").value(DEFAULT_CO_BAO_HIEM.booleanValue()))
            .andExpect(jsonPath("$.coKetQua").value(DEFAULT_CO_KET_QUA.booleanValue()))
            .andExpect(jsonPath("$.daThanhToan").value(DEFAULT_DA_THANH_TOAN))
            .andExpect(jsonPath("$.daThanhToanChenhLech").value(DEFAULT_DA_THANH_TOAN_CHENH_LECH))
            .andExpect(jsonPath("$.daThucHien").value(DEFAULT_DA_THUC_HIEN))
            .andExpect(jsonPath("$.donGia").value(DEFAULT_DON_GIA.intValue()))
            .andExpect(jsonPath("$.donGiaBhyt").value(DEFAULT_DON_GIA_BHYT.intValue()))
            .andExpect(jsonPath("$.donGiaKhongBhyt").value(DEFAULT_DON_GIA_KHONG_BHYT.intValue()))
            .andExpect(jsonPath("$.ghiChuChiDinh").value(DEFAULT_GHI_CHU_CHI_DINH))
            .andExpect(jsonPath("$.moTa").value(DEFAULT_MO_TA))
            .andExpect(jsonPath("$.nguoiChiDinhId").value(DEFAULT_NGUOI_CHI_DINH_ID.intValue()))
            .andExpect(jsonPath("$.soLuong").value(DEFAULT_SO_LUONG))
            .andExpect(jsonPath("$.ten").value(DEFAULT_TEN))
            .andExpect(jsonPath("$.thanhTien").value(DEFAULT_THANH_TIEN.intValue()))
            .andExpect(jsonPath("$.thanhTienBhyt").value(DEFAULT_THANH_TIEN_BHYT.intValue()))
            .andExpect(jsonPath("$.thanhTienKhongBHYT").value(DEFAULT_THANH_TIEN_KHONG_BHYT.intValue()))
            .andExpect(jsonPath("$.thoiGianChiDinh").value(DEFAULT_THOI_GIAN_CHI_DINH.toString()))
            .andExpect(jsonPath("$.thoiGianTao").value(DEFAULT_THOI_GIAN_TAO.toString()))
            .andExpect(jsonPath("$.tienNgoaiBHYT").value(DEFAULT_TIEN_NGOAI_BHYT.intValue()))
            .andExpect(jsonPath("$.thanhToanChenhLech").value(DEFAULT_THANH_TOAN_CHENH_LECH.booleanValue()))
            .andExpect(jsonPath("$.tyLeThanhToan").value(DEFAULT_TY_LE_THANH_TOAN))
            .andExpect(jsonPath("$.maDungChung").value(DEFAULT_MA_DUNG_CHUNG))
            .andExpect(jsonPath("$.dichVuYeuCau").value(DEFAULT_DICH_VU_YEU_CAU.booleanValue()))
            .andExpect(jsonPath("$.nam").value(DEFAULT_NAM));
    }


    @Test
    @Transactional
    public void getChiDinhThuThuatPhauThuatsByIdFiltering() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        Long id = chiDinhThuThuatPhauThuat.getId();

        defaultChiDinhThuThuatPhauThuatShouldBeFound("id.equals=" + id);
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("id.notEquals=" + id);

        defaultChiDinhThuThuatPhauThuatShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("id.greaterThan=" + id);

        defaultChiDinhThuThuatPhauThuatShouldBeFound("id.lessThanOrEqual=" + id);
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByCoBaoHiemIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where coBaoHiem equals to DEFAULT_CO_BAO_HIEM
        defaultChiDinhThuThuatPhauThuatShouldBeFound("coBaoHiem.equals=" + DEFAULT_CO_BAO_HIEM);

        // Get all the chiDinhThuThuatPhauThuatList where coBaoHiem equals to UPDATED_CO_BAO_HIEM
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("coBaoHiem.equals=" + UPDATED_CO_BAO_HIEM);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByCoBaoHiemIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where coBaoHiem not equals to DEFAULT_CO_BAO_HIEM
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("coBaoHiem.notEquals=" + DEFAULT_CO_BAO_HIEM);

        // Get all the chiDinhThuThuatPhauThuatList where coBaoHiem not equals to UPDATED_CO_BAO_HIEM
        defaultChiDinhThuThuatPhauThuatShouldBeFound("coBaoHiem.notEquals=" + UPDATED_CO_BAO_HIEM);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByCoBaoHiemIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where coBaoHiem in DEFAULT_CO_BAO_HIEM or UPDATED_CO_BAO_HIEM
        defaultChiDinhThuThuatPhauThuatShouldBeFound("coBaoHiem.in=" + DEFAULT_CO_BAO_HIEM + "," + UPDATED_CO_BAO_HIEM);

        // Get all the chiDinhThuThuatPhauThuatList where coBaoHiem equals to UPDATED_CO_BAO_HIEM
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("coBaoHiem.in=" + UPDATED_CO_BAO_HIEM);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByCoBaoHiemIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where coBaoHiem is not null
        defaultChiDinhThuThuatPhauThuatShouldBeFound("coBaoHiem.specified=true");

        // Get all the chiDinhThuThuatPhauThuatList where coBaoHiem is null
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("coBaoHiem.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByCoKetQuaIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where coKetQua equals to DEFAULT_CO_KET_QUA
        defaultChiDinhThuThuatPhauThuatShouldBeFound("coKetQua.equals=" + DEFAULT_CO_KET_QUA);

        // Get all the chiDinhThuThuatPhauThuatList where coKetQua equals to UPDATED_CO_KET_QUA
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("coKetQua.equals=" + UPDATED_CO_KET_QUA);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByCoKetQuaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where coKetQua not equals to DEFAULT_CO_KET_QUA
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("coKetQua.notEquals=" + DEFAULT_CO_KET_QUA);

        // Get all the chiDinhThuThuatPhauThuatList where coKetQua not equals to UPDATED_CO_KET_QUA
        defaultChiDinhThuThuatPhauThuatShouldBeFound("coKetQua.notEquals=" + UPDATED_CO_KET_QUA);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByCoKetQuaIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where coKetQua in DEFAULT_CO_KET_QUA or UPDATED_CO_KET_QUA
        defaultChiDinhThuThuatPhauThuatShouldBeFound("coKetQua.in=" + DEFAULT_CO_KET_QUA + "," + UPDATED_CO_KET_QUA);

        // Get all the chiDinhThuThuatPhauThuatList where coKetQua equals to UPDATED_CO_KET_QUA
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("coKetQua.in=" + UPDATED_CO_KET_QUA);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByCoKetQuaIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where coKetQua is not null
        defaultChiDinhThuThuatPhauThuatShouldBeFound("coKetQua.specified=true");

        // Get all the chiDinhThuThuatPhauThuatList where coKetQua is null
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("coKetQua.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDaThanhToanIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where daThanhToan equals to DEFAULT_DA_THANH_TOAN
        defaultChiDinhThuThuatPhauThuatShouldBeFound("daThanhToan.equals=" + DEFAULT_DA_THANH_TOAN);

        // Get all the chiDinhThuThuatPhauThuatList where daThanhToan equals to UPDATED_DA_THANH_TOAN
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("daThanhToan.equals=" + UPDATED_DA_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDaThanhToanIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where daThanhToan not equals to DEFAULT_DA_THANH_TOAN
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("daThanhToan.notEquals=" + DEFAULT_DA_THANH_TOAN);

        // Get all the chiDinhThuThuatPhauThuatList where daThanhToan not equals to UPDATED_DA_THANH_TOAN
        defaultChiDinhThuThuatPhauThuatShouldBeFound("daThanhToan.notEquals=" + UPDATED_DA_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDaThanhToanIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where daThanhToan in DEFAULT_DA_THANH_TOAN or UPDATED_DA_THANH_TOAN
        defaultChiDinhThuThuatPhauThuatShouldBeFound("daThanhToan.in=" + DEFAULT_DA_THANH_TOAN + "," + UPDATED_DA_THANH_TOAN);

        // Get all the chiDinhThuThuatPhauThuatList where daThanhToan equals to UPDATED_DA_THANH_TOAN
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("daThanhToan.in=" + UPDATED_DA_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDaThanhToanIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where daThanhToan is not null
        defaultChiDinhThuThuatPhauThuatShouldBeFound("daThanhToan.specified=true");

        // Get all the chiDinhThuThuatPhauThuatList where daThanhToan is null
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("daThanhToan.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDaThanhToanIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where daThanhToan is greater than or equal to DEFAULT_DA_THANH_TOAN
        defaultChiDinhThuThuatPhauThuatShouldBeFound("daThanhToan.greaterThanOrEqual=" + DEFAULT_DA_THANH_TOAN);

        // Get all the chiDinhThuThuatPhauThuatList where daThanhToan is greater than or equal to UPDATED_DA_THANH_TOAN
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("daThanhToan.greaterThanOrEqual=" + UPDATED_DA_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDaThanhToanIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where daThanhToan is less than or equal to DEFAULT_DA_THANH_TOAN
        defaultChiDinhThuThuatPhauThuatShouldBeFound("daThanhToan.lessThanOrEqual=" + DEFAULT_DA_THANH_TOAN);

        // Get all the chiDinhThuThuatPhauThuatList where daThanhToan is less than or equal to SMALLER_DA_THANH_TOAN
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("daThanhToan.lessThanOrEqual=" + SMALLER_DA_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDaThanhToanIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where daThanhToan is less than DEFAULT_DA_THANH_TOAN
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("daThanhToan.lessThan=" + DEFAULT_DA_THANH_TOAN);

        // Get all the chiDinhThuThuatPhauThuatList where daThanhToan is less than UPDATED_DA_THANH_TOAN
        defaultChiDinhThuThuatPhauThuatShouldBeFound("daThanhToan.lessThan=" + UPDATED_DA_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDaThanhToanIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where daThanhToan is greater than DEFAULT_DA_THANH_TOAN
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("daThanhToan.greaterThan=" + DEFAULT_DA_THANH_TOAN);

        // Get all the chiDinhThuThuatPhauThuatList where daThanhToan is greater than SMALLER_DA_THANH_TOAN
        defaultChiDinhThuThuatPhauThuatShouldBeFound("daThanhToan.greaterThan=" + SMALLER_DA_THANH_TOAN);
    }


    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDaThanhToanChenhLechIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where daThanhToanChenhLech equals to DEFAULT_DA_THANH_TOAN_CHENH_LECH
        defaultChiDinhThuThuatPhauThuatShouldBeFound("daThanhToanChenhLech.equals=" + DEFAULT_DA_THANH_TOAN_CHENH_LECH);

        // Get all the chiDinhThuThuatPhauThuatList where daThanhToanChenhLech equals to UPDATED_DA_THANH_TOAN_CHENH_LECH
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("daThanhToanChenhLech.equals=" + UPDATED_DA_THANH_TOAN_CHENH_LECH);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDaThanhToanChenhLechIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where daThanhToanChenhLech not equals to DEFAULT_DA_THANH_TOAN_CHENH_LECH
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("daThanhToanChenhLech.notEquals=" + DEFAULT_DA_THANH_TOAN_CHENH_LECH);

        // Get all the chiDinhThuThuatPhauThuatList where daThanhToanChenhLech not equals to UPDATED_DA_THANH_TOAN_CHENH_LECH
        defaultChiDinhThuThuatPhauThuatShouldBeFound("daThanhToanChenhLech.notEquals=" + UPDATED_DA_THANH_TOAN_CHENH_LECH);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDaThanhToanChenhLechIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where daThanhToanChenhLech in DEFAULT_DA_THANH_TOAN_CHENH_LECH or UPDATED_DA_THANH_TOAN_CHENH_LECH
        defaultChiDinhThuThuatPhauThuatShouldBeFound("daThanhToanChenhLech.in=" + DEFAULT_DA_THANH_TOAN_CHENH_LECH + "," + UPDATED_DA_THANH_TOAN_CHENH_LECH);

        // Get all the chiDinhThuThuatPhauThuatList where daThanhToanChenhLech equals to UPDATED_DA_THANH_TOAN_CHENH_LECH
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("daThanhToanChenhLech.in=" + UPDATED_DA_THANH_TOAN_CHENH_LECH);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDaThanhToanChenhLechIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where daThanhToanChenhLech is not null
        defaultChiDinhThuThuatPhauThuatShouldBeFound("daThanhToanChenhLech.specified=true");

        // Get all the chiDinhThuThuatPhauThuatList where daThanhToanChenhLech is null
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("daThanhToanChenhLech.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDaThanhToanChenhLechIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where daThanhToanChenhLech is greater than or equal to DEFAULT_DA_THANH_TOAN_CHENH_LECH
        defaultChiDinhThuThuatPhauThuatShouldBeFound("daThanhToanChenhLech.greaterThanOrEqual=" + DEFAULT_DA_THANH_TOAN_CHENH_LECH);

        // Get all the chiDinhThuThuatPhauThuatList where daThanhToanChenhLech is greater than or equal to UPDATED_DA_THANH_TOAN_CHENH_LECH
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("daThanhToanChenhLech.greaterThanOrEqual=" + UPDATED_DA_THANH_TOAN_CHENH_LECH);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDaThanhToanChenhLechIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where daThanhToanChenhLech is less than or equal to DEFAULT_DA_THANH_TOAN_CHENH_LECH
        defaultChiDinhThuThuatPhauThuatShouldBeFound("daThanhToanChenhLech.lessThanOrEqual=" + DEFAULT_DA_THANH_TOAN_CHENH_LECH);

        // Get all the chiDinhThuThuatPhauThuatList where daThanhToanChenhLech is less than or equal to SMALLER_DA_THANH_TOAN_CHENH_LECH
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("daThanhToanChenhLech.lessThanOrEqual=" + SMALLER_DA_THANH_TOAN_CHENH_LECH);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDaThanhToanChenhLechIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where daThanhToanChenhLech is less than DEFAULT_DA_THANH_TOAN_CHENH_LECH
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("daThanhToanChenhLech.lessThan=" + DEFAULT_DA_THANH_TOAN_CHENH_LECH);

        // Get all the chiDinhThuThuatPhauThuatList where daThanhToanChenhLech is less than UPDATED_DA_THANH_TOAN_CHENH_LECH
        defaultChiDinhThuThuatPhauThuatShouldBeFound("daThanhToanChenhLech.lessThan=" + UPDATED_DA_THANH_TOAN_CHENH_LECH);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDaThanhToanChenhLechIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where daThanhToanChenhLech is greater than DEFAULT_DA_THANH_TOAN_CHENH_LECH
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("daThanhToanChenhLech.greaterThan=" + DEFAULT_DA_THANH_TOAN_CHENH_LECH);

        // Get all the chiDinhThuThuatPhauThuatList where daThanhToanChenhLech is greater than SMALLER_DA_THANH_TOAN_CHENH_LECH
        defaultChiDinhThuThuatPhauThuatShouldBeFound("daThanhToanChenhLech.greaterThan=" + SMALLER_DA_THANH_TOAN_CHENH_LECH);
    }


    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDaThucHienIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where daThucHien equals to DEFAULT_DA_THUC_HIEN
        defaultChiDinhThuThuatPhauThuatShouldBeFound("daThucHien.equals=" + DEFAULT_DA_THUC_HIEN);

        // Get all the chiDinhThuThuatPhauThuatList where daThucHien equals to UPDATED_DA_THUC_HIEN
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("daThucHien.equals=" + UPDATED_DA_THUC_HIEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDaThucHienIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where daThucHien not equals to DEFAULT_DA_THUC_HIEN
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("daThucHien.notEquals=" + DEFAULT_DA_THUC_HIEN);

        // Get all the chiDinhThuThuatPhauThuatList where daThucHien not equals to UPDATED_DA_THUC_HIEN
        defaultChiDinhThuThuatPhauThuatShouldBeFound("daThucHien.notEquals=" + UPDATED_DA_THUC_HIEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDaThucHienIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where daThucHien in DEFAULT_DA_THUC_HIEN or UPDATED_DA_THUC_HIEN
        defaultChiDinhThuThuatPhauThuatShouldBeFound("daThucHien.in=" + DEFAULT_DA_THUC_HIEN + "," + UPDATED_DA_THUC_HIEN);

        // Get all the chiDinhThuThuatPhauThuatList where daThucHien equals to UPDATED_DA_THUC_HIEN
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("daThucHien.in=" + UPDATED_DA_THUC_HIEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDaThucHienIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where daThucHien is not null
        defaultChiDinhThuThuatPhauThuatShouldBeFound("daThucHien.specified=true");

        // Get all the chiDinhThuThuatPhauThuatList where daThucHien is null
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("daThucHien.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDaThucHienIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where daThucHien is greater than or equal to DEFAULT_DA_THUC_HIEN
        defaultChiDinhThuThuatPhauThuatShouldBeFound("daThucHien.greaterThanOrEqual=" + DEFAULT_DA_THUC_HIEN);

        // Get all the chiDinhThuThuatPhauThuatList where daThucHien is greater than or equal to UPDATED_DA_THUC_HIEN
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("daThucHien.greaterThanOrEqual=" + UPDATED_DA_THUC_HIEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDaThucHienIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where daThucHien is less than or equal to DEFAULT_DA_THUC_HIEN
        defaultChiDinhThuThuatPhauThuatShouldBeFound("daThucHien.lessThanOrEqual=" + DEFAULT_DA_THUC_HIEN);

        // Get all the chiDinhThuThuatPhauThuatList where daThucHien is less than or equal to SMALLER_DA_THUC_HIEN
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("daThucHien.lessThanOrEqual=" + SMALLER_DA_THUC_HIEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDaThucHienIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where daThucHien is less than DEFAULT_DA_THUC_HIEN
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("daThucHien.lessThan=" + DEFAULT_DA_THUC_HIEN);

        // Get all the chiDinhThuThuatPhauThuatList where daThucHien is less than UPDATED_DA_THUC_HIEN
        defaultChiDinhThuThuatPhauThuatShouldBeFound("daThucHien.lessThan=" + UPDATED_DA_THUC_HIEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDaThucHienIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where daThucHien is greater than DEFAULT_DA_THUC_HIEN
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("daThucHien.greaterThan=" + DEFAULT_DA_THUC_HIEN);

        // Get all the chiDinhThuThuatPhauThuatList where daThucHien is greater than SMALLER_DA_THUC_HIEN
        defaultChiDinhThuThuatPhauThuatShouldBeFound("daThucHien.greaterThan=" + SMALLER_DA_THUC_HIEN);
    }


    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDonGiaIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where donGia equals to DEFAULT_DON_GIA
        defaultChiDinhThuThuatPhauThuatShouldBeFound("donGia.equals=" + DEFAULT_DON_GIA);

        // Get all the chiDinhThuThuatPhauThuatList where donGia equals to UPDATED_DON_GIA
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("donGia.equals=" + UPDATED_DON_GIA);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDonGiaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where donGia not equals to DEFAULT_DON_GIA
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("donGia.notEquals=" + DEFAULT_DON_GIA);

        // Get all the chiDinhThuThuatPhauThuatList where donGia not equals to UPDATED_DON_GIA
        defaultChiDinhThuThuatPhauThuatShouldBeFound("donGia.notEquals=" + UPDATED_DON_GIA);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDonGiaIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where donGia in DEFAULT_DON_GIA or UPDATED_DON_GIA
        defaultChiDinhThuThuatPhauThuatShouldBeFound("donGia.in=" + DEFAULT_DON_GIA + "," + UPDATED_DON_GIA);

        // Get all the chiDinhThuThuatPhauThuatList where donGia equals to UPDATED_DON_GIA
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("donGia.in=" + UPDATED_DON_GIA);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDonGiaIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where donGia is not null
        defaultChiDinhThuThuatPhauThuatShouldBeFound("donGia.specified=true");

        // Get all the chiDinhThuThuatPhauThuatList where donGia is null
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("donGia.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDonGiaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where donGia is greater than or equal to DEFAULT_DON_GIA
        defaultChiDinhThuThuatPhauThuatShouldBeFound("donGia.greaterThanOrEqual=" + DEFAULT_DON_GIA);

        // Get all the chiDinhThuThuatPhauThuatList where donGia is greater than or equal to UPDATED_DON_GIA
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("donGia.greaterThanOrEqual=" + UPDATED_DON_GIA);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDonGiaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where donGia is less than or equal to DEFAULT_DON_GIA
        defaultChiDinhThuThuatPhauThuatShouldBeFound("donGia.lessThanOrEqual=" + DEFAULT_DON_GIA);

        // Get all the chiDinhThuThuatPhauThuatList where donGia is less than or equal to SMALLER_DON_GIA
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("donGia.lessThanOrEqual=" + SMALLER_DON_GIA);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDonGiaIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where donGia is less than DEFAULT_DON_GIA
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("donGia.lessThan=" + DEFAULT_DON_GIA);

        // Get all the chiDinhThuThuatPhauThuatList where donGia is less than UPDATED_DON_GIA
        defaultChiDinhThuThuatPhauThuatShouldBeFound("donGia.lessThan=" + UPDATED_DON_GIA);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDonGiaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where donGia is greater than DEFAULT_DON_GIA
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("donGia.greaterThan=" + DEFAULT_DON_GIA);

        // Get all the chiDinhThuThuatPhauThuatList where donGia is greater than SMALLER_DON_GIA
        defaultChiDinhThuThuatPhauThuatShouldBeFound("donGia.greaterThan=" + SMALLER_DON_GIA);
    }


    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDonGiaBhytIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where donGiaBhyt equals to DEFAULT_DON_GIA_BHYT
        defaultChiDinhThuThuatPhauThuatShouldBeFound("donGiaBhyt.equals=" + DEFAULT_DON_GIA_BHYT);

        // Get all the chiDinhThuThuatPhauThuatList where donGiaBhyt equals to UPDATED_DON_GIA_BHYT
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("donGiaBhyt.equals=" + UPDATED_DON_GIA_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDonGiaBhytIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where donGiaBhyt not equals to DEFAULT_DON_GIA_BHYT
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("donGiaBhyt.notEquals=" + DEFAULT_DON_GIA_BHYT);

        // Get all the chiDinhThuThuatPhauThuatList where donGiaBhyt not equals to UPDATED_DON_GIA_BHYT
        defaultChiDinhThuThuatPhauThuatShouldBeFound("donGiaBhyt.notEquals=" + UPDATED_DON_GIA_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDonGiaBhytIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where donGiaBhyt in DEFAULT_DON_GIA_BHYT or UPDATED_DON_GIA_BHYT
        defaultChiDinhThuThuatPhauThuatShouldBeFound("donGiaBhyt.in=" + DEFAULT_DON_GIA_BHYT + "," + UPDATED_DON_GIA_BHYT);

        // Get all the chiDinhThuThuatPhauThuatList where donGiaBhyt equals to UPDATED_DON_GIA_BHYT
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("donGiaBhyt.in=" + UPDATED_DON_GIA_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDonGiaBhytIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where donGiaBhyt is not null
        defaultChiDinhThuThuatPhauThuatShouldBeFound("donGiaBhyt.specified=true");

        // Get all the chiDinhThuThuatPhauThuatList where donGiaBhyt is null
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("donGiaBhyt.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDonGiaBhytIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where donGiaBhyt is greater than or equal to DEFAULT_DON_GIA_BHYT
        defaultChiDinhThuThuatPhauThuatShouldBeFound("donGiaBhyt.greaterThanOrEqual=" + DEFAULT_DON_GIA_BHYT);

        // Get all the chiDinhThuThuatPhauThuatList where donGiaBhyt is greater than or equal to UPDATED_DON_GIA_BHYT
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("donGiaBhyt.greaterThanOrEqual=" + UPDATED_DON_GIA_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDonGiaBhytIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where donGiaBhyt is less than or equal to DEFAULT_DON_GIA_BHYT
        defaultChiDinhThuThuatPhauThuatShouldBeFound("donGiaBhyt.lessThanOrEqual=" + DEFAULT_DON_GIA_BHYT);

        // Get all the chiDinhThuThuatPhauThuatList where donGiaBhyt is less than or equal to SMALLER_DON_GIA_BHYT
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("donGiaBhyt.lessThanOrEqual=" + SMALLER_DON_GIA_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDonGiaBhytIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where donGiaBhyt is less than DEFAULT_DON_GIA_BHYT
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("donGiaBhyt.lessThan=" + DEFAULT_DON_GIA_BHYT);

        // Get all the chiDinhThuThuatPhauThuatList where donGiaBhyt is less than UPDATED_DON_GIA_BHYT
        defaultChiDinhThuThuatPhauThuatShouldBeFound("donGiaBhyt.lessThan=" + UPDATED_DON_GIA_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDonGiaBhytIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where donGiaBhyt is greater than DEFAULT_DON_GIA_BHYT
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("donGiaBhyt.greaterThan=" + DEFAULT_DON_GIA_BHYT);

        // Get all the chiDinhThuThuatPhauThuatList where donGiaBhyt is greater than SMALLER_DON_GIA_BHYT
        defaultChiDinhThuThuatPhauThuatShouldBeFound("donGiaBhyt.greaterThan=" + SMALLER_DON_GIA_BHYT);
    }


    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDonGiaKhongBhytIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where donGiaKhongBhyt equals to DEFAULT_DON_GIA_KHONG_BHYT
        defaultChiDinhThuThuatPhauThuatShouldBeFound("donGiaKhongBhyt.equals=" + DEFAULT_DON_GIA_KHONG_BHYT);

        // Get all the chiDinhThuThuatPhauThuatList where donGiaKhongBhyt equals to UPDATED_DON_GIA_KHONG_BHYT
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("donGiaKhongBhyt.equals=" + UPDATED_DON_GIA_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDonGiaKhongBhytIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where donGiaKhongBhyt not equals to DEFAULT_DON_GIA_KHONG_BHYT
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("donGiaKhongBhyt.notEquals=" + DEFAULT_DON_GIA_KHONG_BHYT);

        // Get all the chiDinhThuThuatPhauThuatList where donGiaKhongBhyt not equals to UPDATED_DON_GIA_KHONG_BHYT
        defaultChiDinhThuThuatPhauThuatShouldBeFound("donGiaKhongBhyt.notEquals=" + UPDATED_DON_GIA_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDonGiaKhongBhytIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where donGiaKhongBhyt in DEFAULT_DON_GIA_KHONG_BHYT or UPDATED_DON_GIA_KHONG_BHYT
        defaultChiDinhThuThuatPhauThuatShouldBeFound("donGiaKhongBhyt.in=" + DEFAULT_DON_GIA_KHONG_BHYT + "," + UPDATED_DON_GIA_KHONG_BHYT);

        // Get all the chiDinhThuThuatPhauThuatList where donGiaKhongBhyt equals to UPDATED_DON_GIA_KHONG_BHYT
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("donGiaKhongBhyt.in=" + UPDATED_DON_GIA_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDonGiaKhongBhytIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where donGiaKhongBhyt is not null
        defaultChiDinhThuThuatPhauThuatShouldBeFound("donGiaKhongBhyt.specified=true");

        // Get all the chiDinhThuThuatPhauThuatList where donGiaKhongBhyt is null
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("donGiaKhongBhyt.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDonGiaKhongBhytIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where donGiaKhongBhyt is greater than or equal to DEFAULT_DON_GIA_KHONG_BHYT
        defaultChiDinhThuThuatPhauThuatShouldBeFound("donGiaKhongBhyt.greaterThanOrEqual=" + DEFAULT_DON_GIA_KHONG_BHYT);

        // Get all the chiDinhThuThuatPhauThuatList where donGiaKhongBhyt is greater than or equal to UPDATED_DON_GIA_KHONG_BHYT
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("donGiaKhongBhyt.greaterThanOrEqual=" + UPDATED_DON_GIA_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDonGiaKhongBhytIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where donGiaKhongBhyt is less than or equal to DEFAULT_DON_GIA_KHONG_BHYT
        defaultChiDinhThuThuatPhauThuatShouldBeFound("donGiaKhongBhyt.lessThanOrEqual=" + DEFAULT_DON_GIA_KHONG_BHYT);

        // Get all the chiDinhThuThuatPhauThuatList where donGiaKhongBhyt is less than or equal to SMALLER_DON_GIA_KHONG_BHYT
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("donGiaKhongBhyt.lessThanOrEqual=" + SMALLER_DON_GIA_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDonGiaKhongBhytIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where donGiaKhongBhyt is less than DEFAULT_DON_GIA_KHONG_BHYT
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("donGiaKhongBhyt.lessThan=" + DEFAULT_DON_GIA_KHONG_BHYT);

        // Get all the chiDinhThuThuatPhauThuatList where donGiaKhongBhyt is less than UPDATED_DON_GIA_KHONG_BHYT
        defaultChiDinhThuThuatPhauThuatShouldBeFound("donGiaKhongBhyt.lessThan=" + UPDATED_DON_GIA_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDonGiaKhongBhytIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where donGiaKhongBhyt is greater than DEFAULT_DON_GIA_KHONG_BHYT
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("donGiaKhongBhyt.greaterThan=" + DEFAULT_DON_GIA_KHONG_BHYT);

        // Get all the chiDinhThuThuatPhauThuatList where donGiaKhongBhyt is greater than SMALLER_DON_GIA_KHONG_BHYT
        defaultChiDinhThuThuatPhauThuatShouldBeFound("donGiaKhongBhyt.greaterThan=" + SMALLER_DON_GIA_KHONG_BHYT);
    }


    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByGhiChuChiDinhIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where ghiChuChiDinh equals to DEFAULT_GHI_CHU_CHI_DINH
        defaultChiDinhThuThuatPhauThuatShouldBeFound("ghiChuChiDinh.equals=" + DEFAULT_GHI_CHU_CHI_DINH);

        // Get all the chiDinhThuThuatPhauThuatList where ghiChuChiDinh equals to UPDATED_GHI_CHU_CHI_DINH
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("ghiChuChiDinh.equals=" + UPDATED_GHI_CHU_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByGhiChuChiDinhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where ghiChuChiDinh not equals to DEFAULT_GHI_CHU_CHI_DINH
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("ghiChuChiDinh.notEquals=" + DEFAULT_GHI_CHU_CHI_DINH);

        // Get all the chiDinhThuThuatPhauThuatList where ghiChuChiDinh not equals to UPDATED_GHI_CHU_CHI_DINH
        defaultChiDinhThuThuatPhauThuatShouldBeFound("ghiChuChiDinh.notEquals=" + UPDATED_GHI_CHU_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByGhiChuChiDinhIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where ghiChuChiDinh in DEFAULT_GHI_CHU_CHI_DINH or UPDATED_GHI_CHU_CHI_DINH
        defaultChiDinhThuThuatPhauThuatShouldBeFound("ghiChuChiDinh.in=" + DEFAULT_GHI_CHU_CHI_DINH + "," + UPDATED_GHI_CHU_CHI_DINH);

        // Get all the chiDinhThuThuatPhauThuatList where ghiChuChiDinh equals to UPDATED_GHI_CHU_CHI_DINH
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("ghiChuChiDinh.in=" + UPDATED_GHI_CHU_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByGhiChuChiDinhIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where ghiChuChiDinh is not null
        defaultChiDinhThuThuatPhauThuatShouldBeFound("ghiChuChiDinh.specified=true");

        // Get all the chiDinhThuThuatPhauThuatList where ghiChuChiDinh is null
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("ghiChuChiDinh.specified=false");
    }
                @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByGhiChuChiDinhContainsSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where ghiChuChiDinh contains DEFAULT_GHI_CHU_CHI_DINH
        defaultChiDinhThuThuatPhauThuatShouldBeFound("ghiChuChiDinh.contains=" + DEFAULT_GHI_CHU_CHI_DINH);

        // Get all the chiDinhThuThuatPhauThuatList where ghiChuChiDinh contains UPDATED_GHI_CHU_CHI_DINH
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("ghiChuChiDinh.contains=" + UPDATED_GHI_CHU_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByGhiChuChiDinhNotContainsSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where ghiChuChiDinh does not contain DEFAULT_GHI_CHU_CHI_DINH
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("ghiChuChiDinh.doesNotContain=" + DEFAULT_GHI_CHU_CHI_DINH);

        // Get all the chiDinhThuThuatPhauThuatList where ghiChuChiDinh does not contain UPDATED_GHI_CHU_CHI_DINH
        defaultChiDinhThuThuatPhauThuatShouldBeFound("ghiChuChiDinh.doesNotContain=" + UPDATED_GHI_CHU_CHI_DINH);
    }


    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByMoTaIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where moTa equals to DEFAULT_MO_TA
        defaultChiDinhThuThuatPhauThuatShouldBeFound("moTa.equals=" + DEFAULT_MO_TA);

        // Get all the chiDinhThuThuatPhauThuatList where moTa equals to UPDATED_MO_TA
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("moTa.equals=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByMoTaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where moTa not equals to DEFAULT_MO_TA
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("moTa.notEquals=" + DEFAULT_MO_TA);

        // Get all the chiDinhThuThuatPhauThuatList where moTa not equals to UPDATED_MO_TA
        defaultChiDinhThuThuatPhauThuatShouldBeFound("moTa.notEquals=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByMoTaIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where moTa in DEFAULT_MO_TA or UPDATED_MO_TA
        defaultChiDinhThuThuatPhauThuatShouldBeFound("moTa.in=" + DEFAULT_MO_TA + "," + UPDATED_MO_TA);

        // Get all the chiDinhThuThuatPhauThuatList where moTa equals to UPDATED_MO_TA
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("moTa.in=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByMoTaIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where moTa is not null
        defaultChiDinhThuThuatPhauThuatShouldBeFound("moTa.specified=true");

        // Get all the chiDinhThuThuatPhauThuatList where moTa is null
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("moTa.specified=false");
    }
                @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByMoTaContainsSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where moTa contains DEFAULT_MO_TA
        defaultChiDinhThuThuatPhauThuatShouldBeFound("moTa.contains=" + DEFAULT_MO_TA);

        // Get all the chiDinhThuThuatPhauThuatList where moTa contains UPDATED_MO_TA
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("moTa.contains=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByMoTaNotContainsSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where moTa does not contain DEFAULT_MO_TA
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("moTa.doesNotContain=" + DEFAULT_MO_TA);

        // Get all the chiDinhThuThuatPhauThuatList where moTa does not contain UPDATED_MO_TA
        defaultChiDinhThuThuatPhauThuatShouldBeFound("moTa.doesNotContain=" + UPDATED_MO_TA);
    }


    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByNguoiChiDinhIdIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where nguoiChiDinhId equals to DEFAULT_NGUOI_CHI_DINH_ID
        defaultChiDinhThuThuatPhauThuatShouldBeFound("nguoiChiDinhId.equals=" + DEFAULT_NGUOI_CHI_DINH_ID);

        // Get all the chiDinhThuThuatPhauThuatList where nguoiChiDinhId equals to UPDATED_NGUOI_CHI_DINH_ID
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("nguoiChiDinhId.equals=" + UPDATED_NGUOI_CHI_DINH_ID);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByNguoiChiDinhIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where nguoiChiDinhId not equals to DEFAULT_NGUOI_CHI_DINH_ID
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("nguoiChiDinhId.notEquals=" + DEFAULT_NGUOI_CHI_DINH_ID);

        // Get all the chiDinhThuThuatPhauThuatList where nguoiChiDinhId not equals to UPDATED_NGUOI_CHI_DINH_ID
        defaultChiDinhThuThuatPhauThuatShouldBeFound("nguoiChiDinhId.notEquals=" + UPDATED_NGUOI_CHI_DINH_ID);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByNguoiChiDinhIdIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where nguoiChiDinhId in DEFAULT_NGUOI_CHI_DINH_ID or UPDATED_NGUOI_CHI_DINH_ID
        defaultChiDinhThuThuatPhauThuatShouldBeFound("nguoiChiDinhId.in=" + DEFAULT_NGUOI_CHI_DINH_ID + "," + UPDATED_NGUOI_CHI_DINH_ID);

        // Get all the chiDinhThuThuatPhauThuatList where nguoiChiDinhId equals to UPDATED_NGUOI_CHI_DINH_ID
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("nguoiChiDinhId.in=" + UPDATED_NGUOI_CHI_DINH_ID);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByNguoiChiDinhIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where nguoiChiDinhId is not null
        defaultChiDinhThuThuatPhauThuatShouldBeFound("nguoiChiDinhId.specified=true");

        // Get all the chiDinhThuThuatPhauThuatList where nguoiChiDinhId is null
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("nguoiChiDinhId.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByNguoiChiDinhIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where nguoiChiDinhId is greater than or equal to DEFAULT_NGUOI_CHI_DINH_ID
        defaultChiDinhThuThuatPhauThuatShouldBeFound("nguoiChiDinhId.greaterThanOrEqual=" + DEFAULT_NGUOI_CHI_DINH_ID);

        // Get all the chiDinhThuThuatPhauThuatList where nguoiChiDinhId is greater than or equal to UPDATED_NGUOI_CHI_DINH_ID
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("nguoiChiDinhId.greaterThanOrEqual=" + UPDATED_NGUOI_CHI_DINH_ID);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByNguoiChiDinhIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where nguoiChiDinhId is less than or equal to DEFAULT_NGUOI_CHI_DINH_ID
        defaultChiDinhThuThuatPhauThuatShouldBeFound("nguoiChiDinhId.lessThanOrEqual=" + DEFAULT_NGUOI_CHI_DINH_ID);

        // Get all the chiDinhThuThuatPhauThuatList where nguoiChiDinhId is less than or equal to SMALLER_NGUOI_CHI_DINH_ID
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("nguoiChiDinhId.lessThanOrEqual=" + SMALLER_NGUOI_CHI_DINH_ID);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByNguoiChiDinhIdIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where nguoiChiDinhId is less than DEFAULT_NGUOI_CHI_DINH_ID
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("nguoiChiDinhId.lessThan=" + DEFAULT_NGUOI_CHI_DINH_ID);

        // Get all the chiDinhThuThuatPhauThuatList where nguoiChiDinhId is less than UPDATED_NGUOI_CHI_DINH_ID
        defaultChiDinhThuThuatPhauThuatShouldBeFound("nguoiChiDinhId.lessThan=" + UPDATED_NGUOI_CHI_DINH_ID);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByNguoiChiDinhIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where nguoiChiDinhId is greater than DEFAULT_NGUOI_CHI_DINH_ID
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("nguoiChiDinhId.greaterThan=" + DEFAULT_NGUOI_CHI_DINH_ID);

        // Get all the chiDinhThuThuatPhauThuatList where nguoiChiDinhId is greater than SMALLER_NGUOI_CHI_DINH_ID
        defaultChiDinhThuThuatPhauThuatShouldBeFound("nguoiChiDinhId.greaterThan=" + SMALLER_NGUOI_CHI_DINH_ID);
    }


    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsBySoLuongIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where soLuong equals to DEFAULT_SO_LUONG
        defaultChiDinhThuThuatPhauThuatShouldBeFound("soLuong.equals=" + DEFAULT_SO_LUONG);

        // Get all the chiDinhThuThuatPhauThuatList where soLuong equals to UPDATED_SO_LUONG
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("soLuong.equals=" + UPDATED_SO_LUONG);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsBySoLuongIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where soLuong not equals to DEFAULT_SO_LUONG
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("soLuong.notEquals=" + DEFAULT_SO_LUONG);

        // Get all the chiDinhThuThuatPhauThuatList where soLuong not equals to UPDATED_SO_LUONG
        defaultChiDinhThuThuatPhauThuatShouldBeFound("soLuong.notEquals=" + UPDATED_SO_LUONG);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsBySoLuongIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where soLuong in DEFAULT_SO_LUONG or UPDATED_SO_LUONG
        defaultChiDinhThuThuatPhauThuatShouldBeFound("soLuong.in=" + DEFAULT_SO_LUONG + "," + UPDATED_SO_LUONG);

        // Get all the chiDinhThuThuatPhauThuatList where soLuong equals to UPDATED_SO_LUONG
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("soLuong.in=" + UPDATED_SO_LUONG);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsBySoLuongIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where soLuong is not null
        defaultChiDinhThuThuatPhauThuatShouldBeFound("soLuong.specified=true");

        // Get all the chiDinhThuThuatPhauThuatList where soLuong is null
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("soLuong.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsBySoLuongIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where soLuong is greater than or equal to DEFAULT_SO_LUONG
        defaultChiDinhThuThuatPhauThuatShouldBeFound("soLuong.greaterThanOrEqual=" + DEFAULT_SO_LUONG);

        // Get all the chiDinhThuThuatPhauThuatList where soLuong is greater than or equal to UPDATED_SO_LUONG
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("soLuong.greaterThanOrEqual=" + UPDATED_SO_LUONG);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsBySoLuongIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where soLuong is less than or equal to DEFAULT_SO_LUONG
        defaultChiDinhThuThuatPhauThuatShouldBeFound("soLuong.lessThanOrEqual=" + DEFAULT_SO_LUONG);

        // Get all the chiDinhThuThuatPhauThuatList where soLuong is less than or equal to SMALLER_SO_LUONG
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("soLuong.lessThanOrEqual=" + SMALLER_SO_LUONG);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsBySoLuongIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where soLuong is less than DEFAULT_SO_LUONG
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("soLuong.lessThan=" + DEFAULT_SO_LUONG);

        // Get all the chiDinhThuThuatPhauThuatList where soLuong is less than UPDATED_SO_LUONG
        defaultChiDinhThuThuatPhauThuatShouldBeFound("soLuong.lessThan=" + UPDATED_SO_LUONG);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsBySoLuongIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where soLuong is greater than DEFAULT_SO_LUONG
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("soLuong.greaterThan=" + DEFAULT_SO_LUONG);

        // Get all the chiDinhThuThuatPhauThuatList where soLuong is greater than SMALLER_SO_LUONG
        defaultChiDinhThuThuatPhauThuatShouldBeFound("soLuong.greaterThan=" + SMALLER_SO_LUONG);
    }


    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByTenIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where ten equals to DEFAULT_TEN
        defaultChiDinhThuThuatPhauThuatShouldBeFound("ten.equals=" + DEFAULT_TEN);

        // Get all the chiDinhThuThuatPhauThuatList where ten equals to UPDATED_TEN
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("ten.equals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByTenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where ten not equals to DEFAULT_TEN
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("ten.notEquals=" + DEFAULT_TEN);

        // Get all the chiDinhThuThuatPhauThuatList where ten not equals to UPDATED_TEN
        defaultChiDinhThuThuatPhauThuatShouldBeFound("ten.notEquals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByTenIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where ten in DEFAULT_TEN or UPDATED_TEN
        defaultChiDinhThuThuatPhauThuatShouldBeFound("ten.in=" + DEFAULT_TEN + "," + UPDATED_TEN);

        // Get all the chiDinhThuThuatPhauThuatList where ten equals to UPDATED_TEN
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("ten.in=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByTenIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where ten is not null
        defaultChiDinhThuThuatPhauThuatShouldBeFound("ten.specified=true");

        // Get all the chiDinhThuThuatPhauThuatList where ten is null
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("ten.specified=false");
    }
                @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByTenContainsSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where ten contains DEFAULT_TEN
        defaultChiDinhThuThuatPhauThuatShouldBeFound("ten.contains=" + DEFAULT_TEN);

        // Get all the chiDinhThuThuatPhauThuatList where ten contains UPDATED_TEN
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("ten.contains=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByTenNotContainsSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where ten does not contain DEFAULT_TEN
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("ten.doesNotContain=" + DEFAULT_TEN);

        // Get all the chiDinhThuThuatPhauThuatList where ten does not contain UPDATED_TEN
        defaultChiDinhThuThuatPhauThuatShouldBeFound("ten.doesNotContain=" + UPDATED_TEN);
    }


    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByThanhTienIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where thanhTien equals to DEFAULT_THANH_TIEN
        defaultChiDinhThuThuatPhauThuatShouldBeFound("thanhTien.equals=" + DEFAULT_THANH_TIEN);

        // Get all the chiDinhThuThuatPhauThuatList where thanhTien equals to UPDATED_THANH_TIEN
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("thanhTien.equals=" + UPDATED_THANH_TIEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByThanhTienIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where thanhTien not equals to DEFAULT_THANH_TIEN
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("thanhTien.notEquals=" + DEFAULT_THANH_TIEN);

        // Get all the chiDinhThuThuatPhauThuatList where thanhTien not equals to UPDATED_THANH_TIEN
        defaultChiDinhThuThuatPhauThuatShouldBeFound("thanhTien.notEquals=" + UPDATED_THANH_TIEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByThanhTienIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where thanhTien in DEFAULT_THANH_TIEN or UPDATED_THANH_TIEN
        defaultChiDinhThuThuatPhauThuatShouldBeFound("thanhTien.in=" + DEFAULT_THANH_TIEN + "," + UPDATED_THANH_TIEN);

        // Get all the chiDinhThuThuatPhauThuatList where thanhTien equals to UPDATED_THANH_TIEN
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("thanhTien.in=" + UPDATED_THANH_TIEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByThanhTienIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where thanhTien is not null
        defaultChiDinhThuThuatPhauThuatShouldBeFound("thanhTien.specified=true");

        // Get all the chiDinhThuThuatPhauThuatList where thanhTien is null
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("thanhTien.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByThanhTienIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where thanhTien is greater than or equal to DEFAULT_THANH_TIEN
        defaultChiDinhThuThuatPhauThuatShouldBeFound("thanhTien.greaterThanOrEqual=" + DEFAULT_THANH_TIEN);

        // Get all the chiDinhThuThuatPhauThuatList where thanhTien is greater than or equal to UPDATED_THANH_TIEN
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("thanhTien.greaterThanOrEqual=" + UPDATED_THANH_TIEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByThanhTienIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where thanhTien is less than or equal to DEFAULT_THANH_TIEN
        defaultChiDinhThuThuatPhauThuatShouldBeFound("thanhTien.lessThanOrEqual=" + DEFAULT_THANH_TIEN);

        // Get all the chiDinhThuThuatPhauThuatList where thanhTien is less than or equal to SMALLER_THANH_TIEN
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("thanhTien.lessThanOrEqual=" + SMALLER_THANH_TIEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByThanhTienIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where thanhTien is less than DEFAULT_THANH_TIEN
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("thanhTien.lessThan=" + DEFAULT_THANH_TIEN);

        // Get all the chiDinhThuThuatPhauThuatList where thanhTien is less than UPDATED_THANH_TIEN
        defaultChiDinhThuThuatPhauThuatShouldBeFound("thanhTien.lessThan=" + UPDATED_THANH_TIEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByThanhTienIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where thanhTien is greater than DEFAULT_THANH_TIEN
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("thanhTien.greaterThan=" + DEFAULT_THANH_TIEN);

        // Get all the chiDinhThuThuatPhauThuatList where thanhTien is greater than SMALLER_THANH_TIEN
        defaultChiDinhThuThuatPhauThuatShouldBeFound("thanhTien.greaterThan=" + SMALLER_THANH_TIEN);
    }


    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByThanhTienBhytIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where thanhTienBhyt equals to DEFAULT_THANH_TIEN_BHYT
        defaultChiDinhThuThuatPhauThuatShouldBeFound("thanhTienBhyt.equals=" + DEFAULT_THANH_TIEN_BHYT);

        // Get all the chiDinhThuThuatPhauThuatList where thanhTienBhyt equals to UPDATED_THANH_TIEN_BHYT
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("thanhTienBhyt.equals=" + UPDATED_THANH_TIEN_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByThanhTienBhytIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where thanhTienBhyt not equals to DEFAULT_THANH_TIEN_BHYT
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("thanhTienBhyt.notEquals=" + DEFAULT_THANH_TIEN_BHYT);

        // Get all the chiDinhThuThuatPhauThuatList where thanhTienBhyt not equals to UPDATED_THANH_TIEN_BHYT
        defaultChiDinhThuThuatPhauThuatShouldBeFound("thanhTienBhyt.notEquals=" + UPDATED_THANH_TIEN_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByThanhTienBhytIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where thanhTienBhyt in DEFAULT_THANH_TIEN_BHYT or UPDATED_THANH_TIEN_BHYT
        defaultChiDinhThuThuatPhauThuatShouldBeFound("thanhTienBhyt.in=" + DEFAULT_THANH_TIEN_BHYT + "," + UPDATED_THANH_TIEN_BHYT);

        // Get all the chiDinhThuThuatPhauThuatList where thanhTienBhyt equals to UPDATED_THANH_TIEN_BHYT
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("thanhTienBhyt.in=" + UPDATED_THANH_TIEN_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByThanhTienBhytIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where thanhTienBhyt is not null
        defaultChiDinhThuThuatPhauThuatShouldBeFound("thanhTienBhyt.specified=true");

        // Get all the chiDinhThuThuatPhauThuatList where thanhTienBhyt is null
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("thanhTienBhyt.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByThanhTienBhytIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where thanhTienBhyt is greater than or equal to DEFAULT_THANH_TIEN_BHYT
        defaultChiDinhThuThuatPhauThuatShouldBeFound("thanhTienBhyt.greaterThanOrEqual=" + DEFAULT_THANH_TIEN_BHYT);

        // Get all the chiDinhThuThuatPhauThuatList where thanhTienBhyt is greater than or equal to UPDATED_THANH_TIEN_BHYT
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("thanhTienBhyt.greaterThanOrEqual=" + UPDATED_THANH_TIEN_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByThanhTienBhytIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where thanhTienBhyt is less than or equal to DEFAULT_THANH_TIEN_BHYT
        defaultChiDinhThuThuatPhauThuatShouldBeFound("thanhTienBhyt.lessThanOrEqual=" + DEFAULT_THANH_TIEN_BHYT);

        // Get all the chiDinhThuThuatPhauThuatList where thanhTienBhyt is less than or equal to SMALLER_THANH_TIEN_BHYT
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("thanhTienBhyt.lessThanOrEqual=" + SMALLER_THANH_TIEN_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByThanhTienBhytIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where thanhTienBhyt is less than DEFAULT_THANH_TIEN_BHYT
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("thanhTienBhyt.lessThan=" + DEFAULT_THANH_TIEN_BHYT);

        // Get all the chiDinhThuThuatPhauThuatList where thanhTienBhyt is less than UPDATED_THANH_TIEN_BHYT
        defaultChiDinhThuThuatPhauThuatShouldBeFound("thanhTienBhyt.lessThan=" + UPDATED_THANH_TIEN_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByThanhTienBhytIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where thanhTienBhyt is greater than DEFAULT_THANH_TIEN_BHYT
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("thanhTienBhyt.greaterThan=" + DEFAULT_THANH_TIEN_BHYT);

        // Get all the chiDinhThuThuatPhauThuatList where thanhTienBhyt is greater than SMALLER_THANH_TIEN_BHYT
        defaultChiDinhThuThuatPhauThuatShouldBeFound("thanhTienBhyt.greaterThan=" + SMALLER_THANH_TIEN_BHYT);
    }


    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByThanhTienKhongBHYTIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where thanhTienKhongBHYT equals to DEFAULT_THANH_TIEN_KHONG_BHYT
        defaultChiDinhThuThuatPhauThuatShouldBeFound("thanhTienKhongBHYT.equals=" + DEFAULT_THANH_TIEN_KHONG_BHYT);

        // Get all the chiDinhThuThuatPhauThuatList where thanhTienKhongBHYT equals to UPDATED_THANH_TIEN_KHONG_BHYT
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("thanhTienKhongBHYT.equals=" + UPDATED_THANH_TIEN_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByThanhTienKhongBHYTIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where thanhTienKhongBHYT not equals to DEFAULT_THANH_TIEN_KHONG_BHYT
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("thanhTienKhongBHYT.notEquals=" + DEFAULT_THANH_TIEN_KHONG_BHYT);

        // Get all the chiDinhThuThuatPhauThuatList where thanhTienKhongBHYT not equals to UPDATED_THANH_TIEN_KHONG_BHYT
        defaultChiDinhThuThuatPhauThuatShouldBeFound("thanhTienKhongBHYT.notEquals=" + UPDATED_THANH_TIEN_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByThanhTienKhongBHYTIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where thanhTienKhongBHYT in DEFAULT_THANH_TIEN_KHONG_BHYT or UPDATED_THANH_TIEN_KHONG_BHYT
        defaultChiDinhThuThuatPhauThuatShouldBeFound("thanhTienKhongBHYT.in=" + DEFAULT_THANH_TIEN_KHONG_BHYT + "," + UPDATED_THANH_TIEN_KHONG_BHYT);

        // Get all the chiDinhThuThuatPhauThuatList where thanhTienKhongBHYT equals to UPDATED_THANH_TIEN_KHONG_BHYT
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("thanhTienKhongBHYT.in=" + UPDATED_THANH_TIEN_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByThanhTienKhongBHYTIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where thanhTienKhongBHYT is not null
        defaultChiDinhThuThuatPhauThuatShouldBeFound("thanhTienKhongBHYT.specified=true");

        // Get all the chiDinhThuThuatPhauThuatList where thanhTienKhongBHYT is null
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("thanhTienKhongBHYT.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByThanhTienKhongBHYTIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where thanhTienKhongBHYT is greater than or equal to DEFAULT_THANH_TIEN_KHONG_BHYT
        defaultChiDinhThuThuatPhauThuatShouldBeFound("thanhTienKhongBHYT.greaterThanOrEqual=" + DEFAULT_THANH_TIEN_KHONG_BHYT);

        // Get all the chiDinhThuThuatPhauThuatList where thanhTienKhongBHYT is greater than or equal to UPDATED_THANH_TIEN_KHONG_BHYT
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("thanhTienKhongBHYT.greaterThanOrEqual=" + UPDATED_THANH_TIEN_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByThanhTienKhongBHYTIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where thanhTienKhongBHYT is less than or equal to DEFAULT_THANH_TIEN_KHONG_BHYT
        defaultChiDinhThuThuatPhauThuatShouldBeFound("thanhTienKhongBHYT.lessThanOrEqual=" + DEFAULT_THANH_TIEN_KHONG_BHYT);

        // Get all the chiDinhThuThuatPhauThuatList where thanhTienKhongBHYT is less than or equal to SMALLER_THANH_TIEN_KHONG_BHYT
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("thanhTienKhongBHYT.lessThanOrEqual=" + SMALLER_THANH_TIEN_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByThanhTienKhongBHYTIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where thanhTienKhongBHYT is less than DEFAULT_THANH_TIEN_KHONG_BHYT
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("thanhTienKhongBHYT.lessThan=" + DEFAULT_THANH_TIEN_KHONG_BHYT);

        // Get all the chiDinhThuThuatPhauThuatList where thanhTienKhongBHYT is less than UPDATED_THANH_TIEN_KHONG_BHYT
        defaultChiDinhThuThuatPhauThuatShouldBeFound("thanhTienKhongBHYT.lessThan=" + UPDATED_THANH_TIEN_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByThanhTienKhongBHYTIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where thanhTienKhongBHYT is greater than DEFAULT_THANH_TIEN_KHONG_BHYT
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("thanhTienKhongBHYT.greaterThan=" + DEFAULT_THANH_TIEN_KHONG_BHYT);

        // Get all the chiDinhThuThuatPhauThuatList where thanhTienKhongBHYT is greater than SMALLER_THANH_TIEN_KHONG_BHYT
        defaultChiDinhThuThuatPhauThuatShouldBeFound("thanhTienKhongBHYT.greaterThan=" + SMALLER_THANH_TIEN_KHONG_BHYT);
    }


    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByThoiGianChiDinhIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where thoiGianChiDinh equals to DEFAULT_THOI_GIAN_CHI_DINH
        defaultChiDinhThuThuatPhauThuatShouldBeFound("thoiGianChiDinh.equals=" + DEFAULT_THOI_GIAN_CHI_DINH);

        // Get all the chiDinhThuThuatPhauThuatList where thoiGianChiDinh equals to UPDATED_THOI_GIAN_CHI_DINH
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("thoiGianChiDinh.equals=" + UPDATED_THOI_GIAN_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByThoiGianChiDinhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where thoiGianChiDinh not equals to DEFAULT_THOI_GIAN_CHI_DINH
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("thoiGianChiDinh.notEquals=" + DEFAULT_THOI_GIAN_CHI_DINH);

        // Get all the chiDinhThuThuatPhauThuatList where thoiGianChiDinh not equals to UPDATED_THOI_GIAN_CHI_DINH
        defaultChiDinhThuThuatPhauThuatShouldBeFound("thoiGianChiDinh.notEquals=" + UPDATED_THOI_GIAN_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByThoiGianChiDinhIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where thoiGianChiDinh in DEFAULT_THOI_GIAN_CHI_DINH or UPDATED_THOI_GIAN_CHI_DINH
        defaultChiDinhThuThuatPhauThuatShouldBeFound("thoiGianChiDinh.in=" + DEFAULT_THOI_GIAN_CHI_DINH + "," + UPDATED_THOI_GIAN_CHI_DINH);

        // Get all the chiDinhThuThuatPhauThuatList where thoiGianChiDinh equals to UPDATED_THOI_GIAN_CHI_DINH
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("thoiGianChiDinh.in=" + UPDATED_THOI_GIAN_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByThoiGianChiDinhIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where thoiGianChiDinh is not null
        defaultChiDinhThuThuatPhauThuatShouldBeFound("thoiGianChiDinh.specified=true");

        // Get all the chiDinhThuThuatPhauThuatList where thoiGianChiDinh is null
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("thoiGianChiDinh.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByThoiGianChiDinhIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where thoiGianChiDinh is greater than or equal to DEFAULT_THOI_GIAN_CHI_DINH
        defaultChiDinhThuThuatPhauThuatShouldBeFound("thoiGianChiDinh.greaterThanOrEqual=" + DEFAULT_THOI_GIAN_CHI_DINH);

        // Get all the chiDinhThuThuatPhauThuatList where thoiGianChiDinh is greater than or equal to UPDATED_THOI_GIAN_CHI_DINH
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("thoiGianChiDinh.greaterThanOrEqual=" + UPDATED_THOI_GIAN_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByThoiGianChiDinhIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where thoiGianChiDinh is less than or equal to DEFAULT_THOI_GIAN_CHI_DINH
        defaultChiDinhThuThuatPhauThuatShouldBeFound("thoiGianChiDinh.lessThanOrEqual=" + DEFAULT_THOI_GIAN_CHI_DINH);

        // Get all the chiDinhThuThuatPhauThuatList where thoiGianChiDinh is less than or equal to SMALLER_THOI_GIAN_CHI_DINH
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("thoiGianChiDinh.lessThanOrEqual=" + SMALLER_THOI_GIAN_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByThoiGianChiDinhIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where thoiGianChiDinh is less than DEFAULT_THOI_GIAN_CHI_DINH
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("thoiGianChiDinh.lessThan=" + DEFAULT_THOI_GIAN_CHI_DINH);

        // Get all the chiDinhThuThuatPhauThuatList where thoiGianChiDinh is less than UPDATED_THOI_GIAN_CHI_DINH
        defaultChiDinhThuThuatPhauThuatShouldBeFound("thoiGianChiDinh.lessThan=" + UPDATED_THOI_GIAN_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByThoiGianChiDinhIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where thoiGianChiDinh is greater than DEFAULT_THOI_GIAN_CHI_DINH
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("thoiGianChiDinh.greaterThan=" + DEFAULT_THOI_GIAN_CHI_DINH);

        // Get all the chiDinhThuThuatPhauThuatList where thoiGianChiDinh is greater than SMALLER_THOI_GIAN_CHI_DINH
        defaultChiDinhThuThuatPhauThuatShouldBeFound("thoiGianChiDinh.greaterThan=" + SMALLER_THOI_GIAN_CHI_DINH);
    }


    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByThoiGianTaoIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where thoiGianTao equals to DEFAULT_THOI_GIAN_TAO
        defaultChiDinhThuThuatPhauThuatShouldBeFound("thoiGianTao.equals=" + DEFAULT_THOI_GIAN_TAO);

        // Get all the chiDinhThuThuatPhauThuatList where thoiGianTao equals to UPDATED_THOI_GIAN_TAO
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("thoiGianTao.equals=" + UPDATED_THOI_GIAN_TAO);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByThoiGianTaoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where thoiGianTao not equals to DEFAULT_THOI_GIAN_TAO
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("thoiGianTao.notEquals=" + DEFAULT_THOI_GIAN_TAO);

        // Get all the chiDinhThuThuatPhauThuatList where thoiGianTao not equals to UPDATED_THOI_GIAN_TAO
        defaultChiDinhThuThuatPhauThuatShouldBeFound("thoiGianTao.notEquals=" + UPDATED_THOI_GIAN_TAO);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByThoiGianTaoIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where thoiGianTao in DEFAULT_THOI_GIAN_TAO or UPDATED_THOI_GIAN_TAO
        defaultChiDinhThuThuatPhauThuatShouldBeFound("thoiGianTao.in=" + DEFAULT_THOI_GIAN_TAO + "," + UPDATED_THOI_GIAN_TAO);

        // Get all the chiDinhThuThuatPhauThuatList where thoiGianTao equals to UPDATED_THOI_GIAN_TAO
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("thoiGianTao.in=" + UPDATED_THOI_GIAN_TAO);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByThoiGianTaoIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where thoiGianTao is not null
        defaultChiDinhThuThuatPhauThuatShouldBeFound("thoiGianTao.specified=true");

        // Get all the chiDinhThuThuatPhauThuatList where thoiGianTao is null
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("thoiGianTao.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByThoiGianTaoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where thoiGianTao is greater than or equal to DEFAULT_THOI_GIAN_TAO
        defaultChiDinhThuThuatPhauThuatShouldBeFound("thoiGianTao.greaterThanOrEqual=" + DEFAULT_THOI_GIAN_TAO);

        // Get all the chiDinhThuThuatPhauThuatList where thoiGianTao is greater than or equal to UPDATED_THOI_GIAN_TAO
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("thoiGianTao.greaterThanOrEqual=" + UPDATED_THOI_GIAN_TAO);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByThoiGianTaoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where thoiGianTao is less than or equal to DEFAULT_THOI_GIAN_TAO
        defaultChiDinhThuThuatPhauThuatShouldBeFound("thoiGianTao.lessThanOrEqual=" + DEFAULT_THOI_GIAN_TAO);

        // Get all the chiDinhThuThuatPhauThuatList where thoiGianTao is less than or equal to SMALLER_THOI_GIAN_TAO
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("thoiGianTao.lessThanOrEqual=" + SMALLER_THOI_GIAN_TAO);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByThoiGianTaoIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where thoiGianTao is less than DEFAULT_THOI_GIAN_TAO
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("thoiGianTao.lessThan=" + DEFAULT_THOI_GIAN_TAO);

        // Get all the chiDinhThuThuatPhauThuatList where thoiGianTao is less than UPDATED_THOI_GIAN_TAO
        defaultChiDinhThuThuatPhauThuatShouldBeFound("thoiGianTao.lessThan=" + UPDATED_THOI_GIAN_TAO);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByThoiGianTaoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where thoiGianTao is greater than DEFAULT_THOI_GIAN_TAO
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("thoiGianTao.greaterThan=" + DEFAULT_THOI_GIAN_TAO);

        // Get all the chiDinhThuThuatPhauThuatList where thoiGianTao is greater than SMALLER_THOI_GIAN_TAO
        defaultChiDinhThuThuatPhauThuatShouldBeFound("thoiGianTao.greaterThan=" + SMALLER_THOI_GIAN_TAO);
    }


    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByTienNgoaiBHYTIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where tienNgoaiBHYT equals to DEFAULT_TIEN_NGOAI_BHYT
        defaultChiDinhThuThuatPhauThuatShouldBeFound("tienNgoaiBHYT.equals=" + DEFAULT_TIEN_NGOAI_BHYT);

        // Get all the chiDinhThuThuatPhauThuatList where tienNgoaiBHYT equals to UPDATED_TIEN_NGOAI_BHYT
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("tienNgoaiBHYT.equals=" + UPDATED_TIEN_NGOAI_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByTienNgoaiBHYTIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where tienNgoaiBHYT not equals to DEFAULT_TIEN_NGOAI_BHYT
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("tienNgoaiBHYT.notEquals=" + DEFAULT_TIEN_NGOAI_BHYT);

        // Get all the chiDinhThuThuatPhauThuatList where tienNgoaiBHYT not equals to UPDATED_TIEN_NGOAI_BHYT
        defaultChiDinhThuThuatPhauThuatShouldBeFound("tienNgoaiBHYT.notEquals=" + UPDATED_TIEN_NGOAI_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByTienNgoaiBHYTIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where tienNgoaiBHYT in DEFAULT_TIEN_NGOAI_BHYT or UPDATED_TIEN_NGOAI_BHYT
        defaultChiDinhThuThuatPhauThuatShouldBeFound("tienNgoaiBHYT.in=" + DEFAULT_TIEN_NGOAI_BHYT + "," + UPDATED_TIEN_NGOAI_BHYT);

        // Get all the chiDinhThuThuatPhauThuatList where tienNgoaiBHYT equals to UPDATED_TIEN_NGOAI_BHYT
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("tienNgoaiBHYT.in=" + UPDATED_TIEN_NGOAI_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByTienNgoaiBHYTIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where tienNgoaiBHYT is not null
        defaultChiDinhThuThuatPhauThuatShouldBeFound("tienNgoaiBHYT.specified=true");

        // Get all the chiDinhThuThuatPhauThuatList where tienNgoaiBHYT is null
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("tienNgoaiBHYT.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByTienNgoaiBHYTIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where tienNgoaiBHYT is greater than or equal to DEFAULT_TIEN_NGOAI_BHYT
        defaultChiDinhThuThuatPhauThuatShouldBeFound("tienNgoaiBHYT.greaterThanOrEqual=" + DEFAULT_TIEN_NGOAI_BHYT);

        // Get all the chiDinhThuThuatPhauThuatList where tienNgoaiBHYT is greater than or equal to UPDATED_TIEN_NGOAI_BHYT
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("tienNgoaiBHYT.greaterThanOrEqual=" + UPDATED_TIEN_NGOAI_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByTienNgoaiBHYTIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where tienNgoaiBHYT is less than or equal to DEFAULT_TIEN_NGOAI_BHYT
        defaultChiDinhThuThuatPhauThuatShouldBeFound("tienNgoaiBHYT.lessThanOrEqual=" + DEFAULT_TIEN_NGOAI_BHYT);

        // Get all the chiDinhThuThuatPhauThuatList where tienNgoaiBHYT is less than or equal to SMALLER_TIEN_NGOAI_BHYT
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("tienNgoaiBHYT.lessThanOrEqual=" + SMALLER_TIEN_NGOAI_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByTienNgoaiBHYTIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where tienNgoaiBHYT is less than DEFAULT_TIEN_NGOAI_BHYT
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("tienNgoaiBHYT.lessThan=" + DEFAULT_TIEN_NGOAI_BHYT);

        // Get all the chiDinhThuThuatPhauThuatList where tienNgoaiBHYT is less than UPDATED_TIEN_NGOAI_BHYT
        defaultChiDinhThuThuatPhauThuatShouldBeFound("tienNgoaiBHYT.lessThan=" + UPDATED_TIEN_NGOAI_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByTienNgoaiBHYTIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where tienNgoaiBHYT is greater than DEFAULT_TIEN_NGOAI_BHYT
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("tienNgoaiBHYT.greaterThan=" + DEFAULT_TIEN_NGOAI_BHYT);

        // Get all the chiDinhThuThuatPhauThuatList where tienNgoaiBHYT is greater than SMALLER_TIEN_NGOAI_BHYT
        defaultChiDinhThuThuatPhauThuatShouldBeFound("tienNgoaiBHYT.greaterThan=" + SMALLER_TIEN_NGOAI_BHYT);
    }


    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByThanhToanChenhLechIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where thanhToanChenhLech equals to DEFAULT_THANH_TOAN_CHENH_LECH
        defaultChiDinhThuThuatPhauThuatShouldBeFound("thanhToanChenhLech.equals=" + DEFAULT_THANH_TOAN_CHENH_LECH);

        // Get all the chiDinhThuThuatPhauThuatList where thanhToanChenhLech equals to UPDATED_THANH_TOAN_CHENH_LECH
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("thanhToanChenhLech.equals=" + UPDATED_THANH_TOAN_CHENH_LECH);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByThanhToanChenhLechIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where thanhToanChenhLech not equals to DEFAULT_THANH_TOAN_CHENH_LECH
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("thanhToanChenhLech.notEquals=" + DEFAULT_THANH_TOAN_CHENH_LECH);

        // Get all the chiDinhThuThuatPhauThuatList where thanhToanChenhLech not equals to UPDATED_THANH_TOAN_CHENH_LECH
        defaultChiDinhThuThuatPhauThuatShouldBeFound("thanhToanChenhLech.notEquals=" + UPDATED_THANH_TOAN_CHENH_LECH);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByThanhToanChenhLechIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where thanhToanChenhLech in DEFAULT_THANH_TOAN_CHENH_LECH or UPDATED_THANH_TOAN_CHENH_LECH
        defaultChiDinhThuThuatPhauThuatShouldBeFound("thanhToanChenhLech.in=" + DEFAULT_THANH_TOAN_CHENH_LECH + "," + UPDATED_THANH_TOAN_CHENH_LECH);

        // Get all the chiDinhThuThuatPhauThuatList where thanhToanChenhLech equals to UPDATED_THANH_TOAN_CHENH_LECH
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("thanhToanChenhLech.in=" + UPDATED_THANH_TOAN_CHENH_LECH);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByThanhToanChenhLechIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where thanhToanChenhLech is not null
        defaultChiDinhThuThuatPhauThuatShouldBeFound("thanhToanChenhLech.specified=true");

        // Get all the chiDinhThuThuatPhauThuatList where thanhToanChenhLech is null
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("thanhToanChenhLech.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByTyLeThanhToanIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where tyLeThanhToan equals to DEFAULT_TY_LE_THANH_TOAN
        defaultChiDinhThuThuatPhauThuatShouldBeFound("tyLeThanhToan.equals=" + DEFAULT_TY_LE_THANH_TOAN);

        // Get all the chiDinhThuThuatPhauThuatList where tyLeThanhToan equals to UPDATED_TY_LE_THANH_TOAN
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("tyLeThanhToan.equals=" + UPDATED_TY_LE_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByTyLeThanhToanIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where tyLeThanhToan not equals to DEFAULT_TY_LE_THANH_TOAN
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("tyLeThanhToan.notEquals=" + DEFAULT_TY_LE_THANH_TOAN);

        // Get all the chiDinhThuThuatPhauThuatList where tyLeThanhToan not equals to UPDATED_TY_LE_THANH_TOAN
        defaultChiDinhThuThuatPhauThuatShouldBeFound("tyLeThanhToan.notEquals=" + UPDATED_TY_LE_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByTyLeThanhToanIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where tyLeThanhToan in DEFAULT_TY_LE_THANH_TOAN or UPDATED_TY_LE_THANH_TOAN
        defaultChiDinhThuThuatPhauThuatShouldBeFound("tyLeThanhToan.in=" + DEFAULT_TY_LE_THANH_TOAN + "," + UPDATED_TY_LE_THANH_TOAN);

        // Get all the chiDinhThuThuatPhauThuatList where tyLeThanhToan equals to UPDATED_TY_LE_THANH_TOAN
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("tyLeThanhToan.in=" + UPDATED_TY_LE_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByTyLeThanhToanIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where tyLeThanhToan is not null
        defaultChiDinhThuThuatPhauThuatShouldBeFound("tyLeThanhToan.specified=true");

        // Get all the chiDinhThuThuatPhauThuatList where tyLeThanhToan is null
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("tyLeThanhToan.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByTyLeThanhToanIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where tyLeThanhToan is greater than or equal to DEFAULT_TY_LE_THANH_TOAN
        defaultChiDinhThuThuatPhauThuatShouldBeFound("tyLeThanhToan.greaterThanOrEqual=" + DEFAULT_TY_LE_THANH_TOAN);

        // Get all the chiDinhThuThuatPhauThuatList where tyLeThanhToan is greater than or equal to UPDATED_TY_LE_THANH_TOAN
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("tyLeThanhToan.greaterThanOrEqual=" + UPDATED_TY_LE_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByTyLeThanhToanIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where tyLeThanhToan is less than or equal to DEFAULT_TY_LE_THANH_TOAN
        defaultChiDinhThuThuatPhauThuatShouldBeFound("tyLeThanhToan.lessThanOrEqual=" + DEFAULT_TY_LE_THANH_TOAN);

        // Get all the chiDinhThuThuatPhauThuatList where tyLeThanhToan is less than or equal to SMALLER_TY_LE_THANH_TOAN
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("tyLeThanhToan.lessThanOrEqual=" + SMALLER_TY_LE_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByTyLeThanhToanIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where tyLeThanhToan is less than DEFAULT_TY_LE_THANH_TOAN
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("tyLeThanhToan.lessThan=" + DEFAULT_TY_LE_THANH_TOAN);

        // Get all the chiDinhThuThuatPhauThuatList where tyLeThanhToan is less than UPDATED_TY_LE_THANH_TOAN
        defaultChiDinhThuThuatPhauThuatShouldBeFound("tyLeThanhToan.lessThan=" + UPDATED_TY_LE_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByTyLeThanhToanIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where tyLeThanhToan is greater than DEFAULT_TY_LE_THANH_TOAN
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("tyLeThanhToan.greaterThan=" + DEFAULT_TY_LE_THANH_TOAN);

        // Get all the chiDinhThuThuatPhauThuatList where tyLeThanhToan is greater than SMALLER_TY_LE_THANH_TOAN
        defaultChiDinhThuThuatPhauThuatShouldBeFound("tyLeThanhToan.greaterThan=" + SMALLER_TY_LE_THANH_TOAN);
    }


    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByMaDungChungIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where maDungChung equals to DEFAULT_MA_DUNG_CHUNG
        defaultChiDinhThuThuatPhauThuatShouldBeFound("maDungChung.equals=" + DEFAULT_MA_DUNG_CHUNG);

        // Get all the chiDinhThuThuatPhauThuatList where maDungChung equals to UPDATED_MA_DUNG_CHUNG
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("maDungChung.equals=" + UPDATED_MA_DUNG_CHUNG);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByMaDungChungIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where maDungChung not equals to DEFAULT_MA_DUNG_CHUNG
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("maDungChung.notEquals=" + DEFAULT_MA_DUNG_CHUNG);

        // Get all the chiDinhThuThuatPhauThuatList where maDungChung not equals to UPDATED_MA_DUNG_CHUNG
        defaultChiDinhThuThuatPhauThuatShouldBeFound("maDungChung.notEquals=" + UPDATED_MA_DUNG_CHUNG);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByMaDungChungIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where maDungChung in DEFAULT_MA_DUNG_CHUNG or UPDATED_MA_DUNG_CHUNG
        defaultChiDinhThuThuatPhauThuatShouldBeFound("maDungChung.in=" + DEFAULT_MA_DUNG_CHUNG + "," + UPDATED_MA_DUNG_CHUNG);

        // Get all the chiDinhThuThuatPhauThuatList where maDungChung equals to UPDATED_MA_DUNG_CHUNG
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("maDungChung.in=" + UPDATED_MA_DUNG_CHUNG);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByMaDungChungIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where maDungChung is not null
        defaultChiDinhThuThuatPhauThuatShouldBeFound("maDungChung.specified=true");

        // Get all the chiDinhThuThuatPhauThuatList where maDungChung is null
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("maDungChung.specified=false");
    }
                @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByMaDungChungContainsSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where maDungChung contains DEFAULT_MA_DUNG_CHUNG
        defaultChiDinhThuThuatPhauThuatShouldBeFound("maDungChung.contains=" + DEFAULT_MA_DUNG_CHUNG);

        // Get all the chiDinhThuThuatPhauThuatList where maDungChung contains UPDATED_MA_DUNG_CHUNG
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("maDungChung.contains=" + UPDATED_MA_DUNG_CHUNG);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByMaDungChungNotContainsSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where maDungChung does not contain DEFAULT_MA_DUNG_CHUNG
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("maDungChung.doesNotContain=" + DEFAULT_MA_DUNG_CHUNG);

        // Get all the chiDinhThuThuatPhauThuatList where maDungChung does not contain UPDATED_MA_DUNG_CHUNG
        defaultChiDinhThuThuatPhauThuatShouldBeFound("maDungChung.doesNotContain=" + UPDATED_MA_DUNG_CHUNG);
    }


    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDichVuYeuCauIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where dichVuYeuCau equals to DEFAULT_DICH_VU_YEU_CAU
        defaultChiDinhThuThuatPhauThuatShouldBeFound("dichVuYeuCau.equals=" + DEFAULT_DICH_VU_YEU_CAU);

        // Get all the chiDinhThuThuatPhauThuatList where dichVuYeuCau equals to UPDATED_DICH_VU_YEU_CAU
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("dichVuYeuCau.equals=" + UPDATED_DICH_VU_YEU_CAU);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDichVuYeuCauIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where dichVuYeuCau not equals to DEFAULT_DICH_VU_YEU_CAU
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("dichVuYeuCau.notEquals=" + DEFAULT_DICH_VU_YEU_CAU);

        // Get all the chiDinhThuThuatPhauThuatList where dichVuYeuCau not equals to UPDATED_DICH_VU_YEU_CAU
        defaultChiDinhThuThuatPhauThuatShouldBeFound("dichVuYeuCau.notEquals=" + UPDATED_DICH_VU_YEU_CAU);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDichVuYeuCauIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where dichVuYeuCau in DEFAULT_DICH_VU_YEU_CAU or UPDATED_DICH_VU_YEU_CAU
        defaultChiDinhThuThuatPhauThuatShouldBeFound("dichVuYeuCau.in=" + DEFAULT_DICH_VU_YEU_CAU + "," + UPDATED_DICH_VU_YEU_CAU);

        // Get all the chiDinhThuThuatPhauThuatList where dichVuYeuCau equals to UPDATED_DICH_VU_YEU_CAU
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("dichVuYeuCau.in=" + UPDATED_DICH_VU_YEU_CAU);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDichVuYeuCauIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where dichVuYeuCau is not null
        defaultChiDinhThuThuatPhauThuatShouldBeFound("dichVuYeuCau.specified=true");

        // Get all the chiDinhThuThuatPhauThuatList where dichVuYeuCau is null
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("dichVuYeuCau.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByNamIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where nam equals to DEFAULT_NAM
        defaultChiDinhThuThuatPhauThuatShouldBeFound("nam.equals=" + DEFAULT_NAM);

        // Get all the chiDinhThuThuatPhauThuatList where nam equals to UPDATED_NAM
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("nam.equals=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByNamIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where nam not equals to DEFAULT_NAM
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("nam.notEquals=" + DEFAULT_NAM);

        // Get all the chiDinhThuThuatPhauThuatList where nam not equals to UPDATED_NAM
        defaultChiDinhThuThuatPhauThuatShouldBeFound("nam.notEquals=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByNamIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where nam in DEFAULT_NAM or UPDATED_NAM
        defaultChiDinhThuThuatPhauThuatShouldBeFound("nam.in=" + DEFAULT_NAM + "," + UPDATED_NAM);

        // Get all the chiDinhThuThuatPhauThuatList where nam equals to UPDATED_NAM
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("nam.in=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByNamIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where nam is not null
        defaultChiDinhThuThuatPhauThuatShouldBeFound("nam.specified=true");

        // Get all the chiDinhThuThuatPhauThuatList where nam is null
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("nam.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByNamIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where nam is greater than or equal to DEFAULT_NAM
        defaultChiDinhThuThuatPhauThuatShouldBeFound("nam.greaterThanOrEqual=" + DEFAULT_NAM);

        // Get all the chiDinhThuThuatPhauThuatList where nam is greater than or equal to UPDATED_NAM
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("nam.greaterThanOrEqual=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByNamIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where nam is less than or equal to DEFAULT_NAM
        defaultChiDinhThuThuatPhauThuatShouldBeFound("nam.lessThanOrEqual=" + DEFAULT_NAM);

        // Get all the chiDinhThuThuatPhauThuatList where nam is less than or equal to SMALLER_NAM
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("nam.lessThanOrEqual=" + SMALLER_NAM);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByNamIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where nam is less than DEFAULT_NAM
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("nam.lessThan=" + DEFAULT_NAM);

        // Get all the chiDinhThuThuatPhauThuatList where nam is less than UPDATED_NAM
        defaultChiDinhThuThuatPhauThuatShouldBeFound("nam.lessThan=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByNamIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        // Get all the chiDinhThuThuatPhauThuatList where nam is greater than DEFAULT_NAM
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("nam.greaterThan=" + DEFAULT_NAM);

        // Get all the chiDinhThuThuatPhauThuatList where nam is greater than SMALLER_NAM
        defaultChiDinhThuThuatPhauThuatShouldBeFound("nam.greaterThan=" + SMALLER_NAM);
    }


    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByDonViIsEqualToSomething() throws Exception {
        // Get already existing entity
        PhieuChiDinhTTPT donVi = chiDinhThuThuatPhauThuat.getDonVi();
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);
        Long donViId = donVi.getId();

        // Get all the chiDinhThuThuatPhauThuatList where donVi equals to donViId
        defaultChiDinhThuThuatPhauThuatShouldBeFound("donViId.equals=" + donViId);

        // Get all the chiDinhThuThuatPhauThuatList where donVi equals to donViId + 1
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("donViId.equals=" + (donViId + 1));
    }


    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByBenhNhanIsEqualToSomething() throws Exception {
        // Get already existing entity
        PhieuChiDinhTTPT benhNhan = chiDinhThuThuatPhauThuat.getBenhNhan();
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);
        Long benhNhanId = benhNhan.getId();

        // Get all the chiDinhThuThuatPhauThuatList where benhNhan equals to benhNhanId
        defaultChiDinhThuThuatPhauThuatShouldBeFound("benhNhanId.equals=" + benhNhanId);

        // Get all the chiDinhThuThuatPhauThuatList where benhNhan equals to benhNhanId + 1
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("benhNhanId.equals=" + (benhNhanId + 1));
    }


    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByBakbIsEqualToSomething() throws Exception {
        // Get already existing entity
        PhieuChiDinhTTPT bakb = chiDinhThuThuatPhauThuat.getBakb();
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);
        Long bakbId = bakb.getId();

        // Get all the chiDinhThuThuatPhauThuatList where bakb equals to bakbId
        defaultChiDinhThuThuatPhauThuatShouldBeFound("bakbId.equals=" + bakbId);

        // Get all the chiDinhThuThuatPhauThuatList where bakb equals to bakbId + 1
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("bakbId.equals=" + (bakbId + 1));
    }


    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByPhieuCDIsEqualToSomething() throws Exception {
        // Get already existing entity
        PhieuChiDinhTTPT phieuCD = chiDinhThuThuatPhauThuat.getPhieuCD();
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);
        Long phieuCDId = phieuCD.getId();

        // Get all the chiDinhThuThuatPhauThuatList where phieuCD equals to phieuCDId
        defaultChiDinhThuThuatPhauThuatShouldBeFound("phieuCDId.equals=" + phieuCDId);

        // Get all the chiDinhThuThuatPhauThuatList where phieuCD equals to phieuCDId + 1
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("phieuCDId.equals=" + (phieuCDId + 1));
    }


    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByPhongIsEqualToSomething() throws Exception {
        // Get already existing entity
        Phong phong = chiDinhThuThuatPhauThuat.getPhong();
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);
        Long phongId = phong.getId();

        // Get all the chiDinhThuThuatPhauThuatList where phong equals to phongId
        defaultChiDinhThuThuatPhauThuatShouldBeFound("phongId.equals=" + phongId);

        // Get all the chiDinhThuThuatPhauThuatList where phong equals to phongId + 1
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("phongId.equals=" + (phongId + 1));
    }


    @Test
    @Transactional
    public void getAllChiDinhThuThuatPhauThuatsByTtptIsEqualToSomething() throws Exception {
        // Get already existing entity
        ThuThuatPhauThuat ttpt = chiDinhThuThuatPhauThuat.getTtpt();
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);
        Long ttptId = ttpt.getId();

        // Get all the chiDinhThuThuatPhauThuatList where ttpt equals to ttptId
        defaultChiDinhThuThuatPhauThuatShouldBeFound("ttptId.equals=" + ttptId);

        // Get all the chiDinhThuThuatPhauThuatList where ttpt equals to ttptId + 1
        defaultChiDinhThuThuatPhauThuatShouldNotBeFound("ttptId.equals=" + (ttptId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultChiDinhThuThuatPhauThuatShouldBeFound(String filter) throws Exception {
        restChiDinhThuThuatPhauThuatMockMvc.perform(get("/api/chi-dinh-thu-thuat-phau-thuats?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(chiDinhThuThuatPhauThuat.getId().intValue())))
            .andExpect(jsonPath("$.[*].coBaoHiem").value(hasItem(DEFAULT_CO_BAO_HIEM.booleanValue())))
            .andExpect(jsonPath("$.[*].coKetQua").value(hasItem(DEFAULT_CO_KET_QUA.booleanValue())))
            .andExpect(jsonPath("$.[*].daThanhToan").value(hasItem(DEFAULT_DA_THANH_TOAN)))
            .andExpect(jsonPath("$.[*].daThanhToanChenhLech").value(hasItem(DEFAULT_DA_THANH_TOAN_CHENH_LECH)))
            .andExpect(jsonPath("$.[*].daThucHien").value(hasItem(DEFAULT_DA_THUC_HIEN)))
            .andExpect(jsonPath("$.[*].donGia").value(hasItem(DEFAULT_DON_GIA.intValue())))
            .andExpect(jsonPath("$.[*].donGiaBhyt").value(hasItem(DEFAULT_DON_GIA_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].donGiaKhongBhyt").value(hasItem(DEFAULT_DON_GIA_KHONG_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].ghiChuChiDinh").value(hasItem(DEFAULT_GHI_CHU_CHI_DINH)))
            .andExpect(jsonPath("$.[*].moTa").value(hasItem(DEFAULT_MO_TA)))
            .andExpect(jsonPath("$.[*].nguoiChiDinhId").value(hasItem(DEFAULT_NGUOI_CHI_DINH_ID.intValue())))
            .andExpect(jsonPath("$.[*].soLuong").value(hasItem(DEFAULT_SO_LUONG)))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].thanhTien").value(hasItem(DEFAULT_THANH_TIEN.intValue())))
            .andExpect(jsonPath("$.[*].thanhTienBhyt").value(hasItem(DEFAULT_THANH_TIEN_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].thanhTienKhongBHYT").value(hasItem(DEFAULT_THANH_TIEN_KHONG_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].thoiGianChiDinh").value(hasItem(DEFAULT_THOI_GIAN_CHI_DINH.toString())))
            .andExpect(jsonPath("$.[*].thoiGianTao").value(hasItem(DEFAULT_THOI_GIAN_TAO.toString())))
            .andExpect(jsonPath("$.[*].tienNgoaiBHYT").value(hasItem(DEFAULT_TIEN_NGOAI_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].thanhToanChenhLech").value(hasItem(DEFAULT_THANH_TOAN_CHENH_LECH.booleanValue())))
            .andExpect(jsonPath("$.[*].tyLeThanhToan").value(hasItem(DEFAULT_TY_LE_THANH_TOAN)))
            .andExpect(jsonPath("$.[*].maDungChung").value(hasItem(DEFAULT_MA_DUNG_CHUNG)))
            .andExpect(jsonPath("$.[*].dichVuYeuCau").value(hasItem(DEFAULT_DICH_VU_YEU_CAU.booleanValue())))
            .andExpect(jsonPath("$.[*].nam").value(hasItem(DEFAULT_NAM)));

        // Check, that the count call also returns 1
        restChiDinhThuThuatPhauThuatMockMvc.perform(get("/api/chi-dinh-thu-thuat-phau-thuats/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultChiDinhThuThuatPhauThuatShouldNotBeFound(String filter) throws Exception {
        restChiDinhThuThuatPhauThuatMockMvc.perform(get("/api/chi-dinh-thu-thuat-phau-thuats?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restChiDinhThuThuatPhauThuatMockMvc.perform(get("/api/chi-dinh-thu-thuat-phau-thuats/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingChiDinhThuThuatPhauThuat() throws Exception {
        // Get the chiDinhThuThuatPhauThuat
        restChiDinhThuThuatPhauThuatMockMvc.perform(get("/api/chi-dinh-thu-thuat-phau-thuats/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateChiDinhThuThuatPhauThuat() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        int databaseSizeBeforeUpdate = chiDinhThuThuatPhauThuatRepository.findAll().size();

        // Update the chiDinhThuThuatPhauThuat
        ChiDinhThuThuatPhauThuat updatedChiDinhThuThuatPhauThuat = chiDinhThuThuatPhauThuatRepository.findById(chiDinhThuThuatPhauThuat.getId()).get();
        // Disconnect from session so that the updates on updatedChiDinhThuThuatPhauThuat are not directly saved in db
        em.detach(updatedChiDinhThuThuatPhauThuat);
        updatedChiDinhThuThuatPhauThuat
            .coBaoHiem(UPDATED_CO_BAO_HIEM)
            .coKetQua(UPDATED_CO_KET_QUA)
            .daThanhToan(UPDATED_DA_THANH_TOAN)
            .daThanhToanChenhLech(UPDATED_DA_THANH_TOAN_CHENH_LECH)
            .daThucHien(UPDATED_DA_THUC_HIEN)
            .donGia(UPDATED_DON_GIA)
            .donGiaBhyt(UPDATED_DON_GIA_BHYT)
            .donGiaKhongBhyt(UPDATED_DON_GIA_KHONG_BHYT)
            .ghiChuChiDinh(UPDATED_GHI_CHU_CHI_DINH)
            .moTa(UPDATED_MO_TA)
            .nguoiChiDinhId(UPDATED_NGUOI_CHI_DINH_ID)
            .soLuong(UPDATED_SO_LUONG)
            .ten(UPDATED_TEN)
            .thanhTien(UPDATED_THANH_TIEN)
            .thanhTienBhyt(UPDATED_THANH_TIEN_BHYT)
            .thanhTienKhongBHYT(UPDATED_THANH_TIEN_KHONG_BHYT)
            .thoiGianChiDinh(UPDATED_THOI_GIAN_CHI_DINH)
            .thoiGianTao(UPDATED_THOI_GIAN_TAO)
            .tienNgoaiBHYT(UPDATED_TIEN_NGOAI_BHYT)
            .thanhToanChenhLech(UPDATED_THANH_TOAN_CHENH_LECH)
            .tyLeThanhToan(UPDATED_TY_LE_THANH_TOAN)
            .maDungChung(UPDATED_MA_DUNG_CHUNG)
            .dichVuYeuCau(UPDATED_DICH_VU_YEU_CAU)
            .nam(UPDATED_NAM);
        ChiDinhThuThuatPhauThuatDTO chiDinhThuThuatPhauThuatDTO = chiDinhThuThuatPhauThuatMapper.toDto(updatedChiDinhThuThuatPhauThuat);

        restChiDinhThuThuatPhauThuatMockMvc.perform(put("/api/chi-dinh-thu-thuat-phau-thuats").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhThuThuatPhauThuatDTO)))
            .andExpect(status().isOk());

        // Validate the ChiDinhThuThuatPhauThuat in the database
        List<ChiDinhThuThuatPhauThuat> chiDinhThuThuatPhauThuatList = chiDinhThuThuatPhauThuatRepository.findAll();
        assertThat(chiDinhThuThuatPhauThuatList).hasSize(databaseSizeBeforeUpdate);
        ChiDinhThuThuatPhauThuat testChiDinhThuThuatPhauThuat = chiDinhThuThuatPhauThuatList.get(chiDinhThuThuatPhauThuatList.size() - 1);
        assertThat(testChiDinhThuThuatPhauThuat.isCoBaoHiem()).isEqualTo(UPDATED_CO_BAO_HIEM);
        assertThat(testChiDinhThuThuatPhauThuat.isCoKetQua()).isEqualTo(UPDATED_CO_KET_QUA);
        assertThat(testChiDinhThuThuatPhauThuat.getDaThanhToan()).isEqualTo(UPDATED_DA_THANH_TOAN);
        assertThat(testChiDinhThuThuatPhauThuat.getDaThanhToanChenhLech()).isEqualTo(UPDATED_DA_THANH_TOAN_CHENH_LECH);
        assertThat(testChiDinhThuThuatPhauThuat.getDaThucHien()).isEqualTo(UPDATED_DA_THUC_HIEN);
        assertThat(testChiDinhThuThuatPhauThuat.getDonGia()).isEqualTo(UPDATED_DON_GIA);
        assertThat(testChiDinhThuThuatPhauThuat.getDonGiaBhyt()).isEqualTo(UPDATED_DON_GIA_BHYT);
        assertThat(testChiDinhThuThuatPhauThuat.getDonGiaKhongBhyt()).isEqualTo(UPDATED_DON_GIA_KHONG_BHYT);
        assertThat(testChiDinhThuThuatPhauThuat.getGhiChuChiDinh()).isEqualTo(UPDATED_GHI_CHU_CHI_DINH);
        assertThat(testChiDinhThuThuatPhauThuat.getMoTa()).isEqualTo(UPDATED_MO_TA);
        assertThat(testChiDinhThuThuatPhauThuat.getNguoiChiDinhId()).isEqualTo(UPDATED_NGUOI_CHI_DINH_ID);
        assertThat(testChiDinhThuThuatPhauThuat.getSoLuong()).isEqualTo(UPDATED_SO_LUONG);
        assertThat(testChiDinhThuThuatPhauThuat.getTen()).isEqualTo(UPDATED_TEN);
        assertThat(testChiDinhThuThuatPhauThuat.getThanhTien()).isEqualTo(UPDATED_THANH_TIEN);
        assertThat(testChiDinhThuThuatPhauThuat.getThanhTienBhyt()).isEqualTo(UPDATED_THANH_TIEN_BHYT);
        assertThat(testChiDinhThuThuatPhauThuat.getThanhTienKhongBHYT()).isEqualTo(UPDATED_THANH_TIEN_KHONG_BHYT);
        assertThat(testChiDinhThuThuatPhauThuat.getThoiGianChiDinh()).isEqualTo(UPDATED_THOI_GIAN_CHI_DINH);
        assertThat(testChiDinhThuThuatPhauThuat.getThoiGianTao()).isEqualTo(UPDATED_THOI_GIAN_TAO);
        assertThat(testChiDinhThuThuatPhauThuat.getTienNgoaiBHYT()).isEqualTo(UPDATED_TIEN_NGOAI_BHYT);
        assertThat(testChiDinhThuThuatPhauThuat.isThanhToanChenhLech()).isEqualTo(UPDATED_THANH_TOAN_CHENH_LECH);
        assertThat(testChiDinhThuThuatPhauThuat.getTyLeThanhToan()).isEqualTo(UPDATED_TY_LE_THANH_TOAN);
        assertThat(testChiDinhThuThuatPhauThuat.getMaDungChung()).isEqualTo(UPDATED_MA_DUNG_CHUNG);
        assertThat(testChiDinhThuThuatPhauThuat.isDichVuYeuCau()).isEqualTo(UPDATED_DICH_VU_YEU_CAU);
        assertThat(testChiDinhThuThuatPhauThuat.getNam()).isEqualTo(UPDATED_NAM);
    }

    @Test
    @Transactional
    public void updateNonExistingChiDinhThuThuatPhauThuat() throws Exception {
        int databaseSizeBeforeUpdate = chiDinhThuThuatPhauThuatRepository.findAll().size();

        // Create the ChiDinhThuThuatPhauThuat
        ChiDinhThuThuatPhauThuatDTO chiDinhThuThuatPhauThuatDTO = chiDinhThuThuatPhauThuatMapper.toDto(chiDinhThuThuatPhauThuat);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restChiDinhThuThuatPhauThuatMockMvc.perform(put("/api/chi-dinh-thu-thuat-phau-thuats").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhThuThuatPhauThuatDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ChiDinhThuThuatPhauThuat in the database
        List<ChiDinhThuThuatPhauThuat> chiDinhThuThuatPhauThuatList = chiDinhThuThuatPhauThuatRepository.findAll();
        assertThat(chiDinhThuThuatPhauThuatList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteChiDinhThuThuatPhauThuat() throws Exception {
        // Initialize the database
        chiDinhThuThuatPhauThuatRepository.saveAndFlush(chiDinhThuThuatPhauThuat);

        int databaseSizeBeforeDelete = chiDinhThuThuatPhauThuatRepository.findAll().size();

        // Delete the chiDinhThuThuatPhauThuat
        restChiDinhThuThuatPhauThuatMockMvc.perform(delete("/api/chi-dinh-thu-thuat-phau-thuats/{id}", chiDinhThuThuatPhauThuat.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ChiDinhThuThuatPhauThuat> chiDinhThuThuatPhauThuatList = chiDinhThuThuatPhauThuatRepository.findAll();
        assertThat(chiDinhThuThuatPhauThuatList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

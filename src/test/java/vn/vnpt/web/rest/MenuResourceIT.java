package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.Menu;
import vn.vnpt.domain.UserExtra;
import vn.vnpt.repository.MenuRepository;
import vn.vnpt.service.MenuService;
import vn.vnpt.service.dto.MenuDTO;
import vn.vnpt.service.mapper.MenuMapper;
import vn.vnpt.service.dto.MenuCriteria;
import vn.vnpt.service.MenuQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link MenuResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class MenuResourceIT {

    private static final Boolean DEFAULT_ENABLED = false;
    private static final Boolean UPDATED_ENABLED = true;

    private static final Integer DEFAULT_LEVEL = 1;
    private static final Integer UPDATED_LEVEL = 2;
    private static final Integer SMALLER_LEVEL = 1 - 1;

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_URI = "AAAAAAAAAA";
    private static final String UPDATED_URI = "BBBBBBBBBB";

    private static final Integer DEFAULT_PARENT = 1;
    private static final Integer UPDATED_PARENT = 2;
    private static final Integer SMALLER_PARENT = 1 - 1;

    private static final Long DEFAULT_DON_VI_ID = 1L;
    private static final Long UPDATED_DON_VI_ID = 2L;
    private static final Long SMALLER_DON_VI_ID = 1L - 1L;

    @Autowired
    private MenuRepository menuRepository;

    @Mock
    private MenuRepository menuRepositoryMock;

    @Autowired
    private MenuMapper menuMapper;

    @Mock
    private MenuService menuServiceMock;

    @Autowired
    private MenuService menuService;

    @Autowired
    private MenuQueryService menuQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restMenuMockMvc;

    private Menu menu;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Menu createEntity(EntityManager em) {
        Menu menu = new Menu()
            .enabled(DEFAULT_ENABLED)
            .level(DEFAULT_LEVEL)
            .name(DEFAULT_NAME)
            .uri(DEFAULT_URI)
            .parent(DEFAULT_PARENT)
            .donViId(DEFAULT_DON_VI_ID);
        // Add required entity
        UserExtra userExtra;
        if (TestUtil.findAll(em, UserExtra.class).isEmpty()) {
            userExtra = UserExtraResourceIT.createEntity(em);
            em.persist(userExtra);
            em.flush();
        } else {
            userExtra = TestUtil.findAll(em, UserExtra.class).get(0);
        }
        menu.getUsers().add(userExtra);
        return menu;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Menu createUpdatedEntity(EntityManager em) {
        Menu menu = new Menu()
            .enabled(UPDATED_ENABLED)
            .level(UPDATED_LEVEL)
            .name(UPDATED_NAME)
            .uri(UPDATED_URI)
            .parent(UPDATED_PARENT)
            .donViId(UPDATED_DON_VI_ID);
        // Add required entity
        UserExtra userExtra;
        if (TestUtil.findAll(em, UserExtra.class).isEmpty()) {
            userExtra = UserExtraResourceIT.createUpdatedEntity(em);
            em.persist(userExtra);
            em.flush();
        } else {
            userExtra = TestUtil.findAll(em, UserExtra.class).get(0);
        }
        menu.getUsers().add(userExtra);
        return menu;
    }

    @BeforeEach
    public void initTest() {
        menu = createEntity(em);
    }

    @Test
    @Transactional
    public void createMenu() throws Exception {
        int databaseSizeBeforeCreate = menuRepository.findAll().size();
        // Create the Menu
        MenuDTO menuDTO = menuMapper.toDto(menu);
        restMenuMockMvc.perform(post("/api/menus").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(menuDTO)))
            .andExpect(status().isCreated());

        // Validate the Menu in the database
        List<Menu> menuList = menuRepository.findAll();
        assertThat(menuList).hasSize(databaseSizeBeforeCreate + 1);
        Menu testMenu = menuList.get(menuList.size() - 1);
        assertThat(testMenu.isEnabled()).isEqualTo(DEFAULT_ENABLED);
        assertThat(testMenu.getLevel()).isEqualTo(DEFAULT_LEVEL);
        assertThat(testMenu.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testMenu.getUri()).isEqualTo(DEFAULT_URI);
        assertThat(testMenu.getParent()).isEqualTo(DEFAULT_PARENT);
        assertThat(testMenu.getDonViId()).isEqualTo(DEFAULT_DON_VI_ID);
    }

    @Test
    @Transactional
    public void createMenuWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = menuRepository.findAll().size();

        // Create the Menu with an existing ID
        menu.setId(1L);
        MenuDTO menuDTO = menuMapper.toDto(menu);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMenuMockMvc.perform(post("/api/menus").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(menuDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Menu in the database
        List<Menu> menuList = menuRepository.findAll();
        assertThat(menuList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkEnabledIsRequired() throws Exception {
        int databaseSizeBeforeTest = menuRepository.findAll().size();
        // set the field null
        menu.setEnabled(null);

        // Create the Menu, which fails.
        MenuDTO menuDTO = menuMapper.toDto(menu);


        restMenuMockMvc.perform(post("/api/menus").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(menuDTO)))
            .andExpect(status().isBadRequest());

        List<Menu> menuList = menuRepository.findAll();
        assertThat(menuList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLevelIsRequired() throws Exception {
        int databaseSizeBeforeTest = menuRepository.findAll().size();
        // set the field null
        menu.setLevel(null);

        // Create the Menu, which fails.
        MenuDTO menuDTO = menuMapper.toDto(menu);


        restMenuMockMvc.perform(post("/api/menus").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(menuDTO)))
            .andExpect(status().isBadRequest());

        List<Menu> menuList = menuRepository.findAll();
        assertThat(menuList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = menuRepository.findAll().size();
        // set the field null
        menu.setName(null);

        // Create the Menu, which fails.
        MenuDTO menuDTO = menuMapper.toDto(menu);


        restMenuMockMvc.perform(post("/api/menus").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(menuDTO)))
            .andExpect(status().isBadRequest());

        List<Menu> menuList = menuRepository.findAll();
        assertThat(menuList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDonViIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = menuRepository.findAll().size();
        // set the field null
        menu.setDonViId(null);

        // Create the Menu, which fails.
        MenuDTO menuDTO = menuMapper.toDto(menu);


        restMenuMockMvc.perform(post("/api/menus").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(menuDTO)))
            .andExpect(status().isBadRequest());

        List<Menu> menuList = menuRepository.findAll();
        assertThat(menuList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllMenus() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);

        // Get all the menuList
        restMenuMockMvc.perform(get("/api/menus?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(menu.getId().intValue())))
            .andExpect(jsonPath("$.[*].enabled").value(hasItem(DEFAULT_ENABLED.booleanValue())))
            .andExpect(jsonPath("$.[*].level").value(hasItem(DEFAULT_LEVEL)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].uri").value(hasItem(DEFAULT_URI)))
            .andExpect(jsonPath("$.[*].parent").value(hasItem(DEFAULT_PARENT)))
            .andExpect(jsonPath("$.[*].donViId").value(hasItem(DEFAULT_DON_VI_ID.intValue())));
    }
    
    @SuppressWarnings({"unchecked"})
    public void getAllMenusWithEagerRelationshipsIsEnabled() throws Exception {
        when(menuServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restMenuMockMvc.perform(get("/api/menus?eagerload=true"))
            .andExpect(status().isOk());

        verify(menuServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllMenusWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(menuServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restMenuMockMvc.perform(get("/api/menus?eagerload=true"))
            .andExpect(status().isOk());

        verify(menuServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getMenu() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);

        // Get the menu
        restMenuMockMvc.perform(get("/api/menus/{id}", menu.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(menu.getId().intValue()))
            .andExpect(jsonPath("$.enabled").value(DEFAULT_ENABLED.booleanValue()))
            .andExpect(jsonPath("$.level").value(DEFAULT_LEVEL))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.uri").value(DEFAULT_URI))
            .andExpect(jsonPath("$.parent").value(DEFAULT_PARENT))
            .andExpect(jsonPath("$.donViId").value(DEFAULT_DON_VI_ID.intValue()));
    }


    @Test
    @Transactional
    public void getMenusByIdFiltering() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);

        Long id = menu.getId();

        defaultMenuShouldBeFound("id.equals=" + id);
        defaultMenuShouldNotBeFound("id.notEquals=" + id);

        defaultMenuShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultMenuShouldNotBeFound("id.greaterThan=" + id);

        defaultMenuShouldBeFound("id.lessThanOrEqual=" + id);
        defaultMenuShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllMenusByEnabledIsEqualToSomething() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);

        // Get all the menuList where enabled equals to DEFAULT_ENABLED
        defaultMenuShouldBeFound("enabled.equals=" + DEFAULT_ENABLED);

        // Get all the menuList where enabled equals to UPDATED_ENABLED
        defaultMenuShouldNotBeFound("enabled.equals=" + UPDATED_ENABLED);
    }

    @Test
    @Transactional
    public void getAllMenusByEnabledIsNotEqualToSomething() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);

        // Get all the menuList where enabled not equals to DEFAULT_ENABLED
        defaultMenuShouldNotBeFound("enabled.notEquals=" + DEFAULT_ENABLED);

        // Get all the menuList where enabled not equals to UPDATED_ENABLED
        defaultMenuShouldBeFound("enabled.notEquals=" + UPDATED_ENABLED);
    }

    @Test
    @Transactional
    public void getAllMenusByEnabledIsInShouldWork() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);

        // Get all the menuList where enabled in DEFAULT_ENABLED or UPDATED_ENABLED
        defaultMenuShouldBeFound("enabled.in=" + DEFAULT_ENABLED + "," + UPDATED_ENABLED);

        // Get all the menuList where enabled equals to UPDATED_ENABLED
        defaultMenuShouldNotBeFound("enabled.in=" + UPDATED_ENABLED);
    }

    @Test
    @Transactional
    public void getAllMenusByEnabledIsNullOrNotNull() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);

        // Get all the menuList where enabled is not null
        defaultMenuShouldBeFound("enabled.specified=true");

        // Get all the menuList where enabled is null
        defaultMenuShouldNotBeFound("enabled.specified=false");
    }

    @Test
    @Transactional
    public void getAllMenusByLevelIsEqualToSomething() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);

        // Get all the menuList where level equals to DEFAULT_LEVEL
        defaultMenuShouldBeFound("level.equals=" + DEFAULT_LEVEL);

        // Get all the menuList where level equals to UPDATED_LEVEL
        defaultMenuShouldNotBeFound("level.equals=" + UPDATED_LEVEL);
    }

    @Test
    @Transactional
    public void getAllMenusByLevelIsNotEqualToSomething() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);

        // Get all the menuList where level not equals to DEFAULT_LEVEL
        defaultMenuShouldNotBeFound("level.notEquals=" + DEFAULT_LEVEL);

        // Get all the menuList where level not equals to UPDATED_LEVEL
        defaultMenuShouldBeFound("level.notEquals=" + UPDATED_LEVEL);
    }

    @Test
    @Transactional
    public void getAllMenusByLevelIsInShouldWork() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);

        // Get all the menuList where level in DEFAULT_LEVEL or UPDATED_LEVEL
        defaultMenuShouldBeFound("level.in=" + DEFAULT_LEVEL + "," + UPDATED_LEVEL);

        // Get all the menuList where level equals to UPDATED_LEVEL
        defaultMenuShouldNotBeFound("level.in=" + UPDATED_LEVEL);
    }

    @Test
    @Transactional
    public void getAllMenusByLevelIsNullOrNotNull() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);

        // Get all the menuList where level is not null
        defaultMenuShouldBeFound("level.specified=true");

        // Get all the menuList where level is null
        defaultMenuShouldNotBeFound("level.specified=false");
    }

    @Test
    @Transactional
    public void getAllMenusByLevelIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);

        // Get all the menuList where level is greater than or equal to DEFAULT_LEVEL
        defaultMenuShouldBeFound("level.greaterThanOrEqual=" + DEFAULT_LEVEL);

        // Get all the menuList where level is greater than or equal to UPDATED_LEVEL
        defaultMenuShouldNotBeFound("level.greaterThanOrEqual=" + UPDATED_LEVEL);
    }

    @Test
    @Transactional
    public void getAllMenusByLevelIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);

        // Get all the menuList where level is less than or equal to DEFAULT_LEVEL
        defaultMenuShouldBeFound("level.lessThanOrEqual=" + DEFAULT_LEVEL);

        // Get all the menuList where level is less than or equal to SMALLER_LEVEL
        defaultMenuShouldNotBeFound("level.lessThanOrEqual=" + SMALLER_LEVEL);
    }

    @Test
    @Transactional
    public void getAllMenusByLevelIsLessThanSomething() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);

        // Get all the menuList where level is less than DEFAULT_LEVEL
        defaultMenuShouldNotBeFound("level.lessThan=" + DEFAULT_LEVEL);

        // Get all the menuList where level is less than UPDATED_LEVEL
        defaultMenuShouldBeFound("level.lessThan=" + UPDATED_LEVEL);
    }

    @Test
    @Transactional
    public void getAllMenusByLevelIsGreaterThanSomething() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);

        // Get all the menuList where level is greater than DEFAULT_LEVEL
        defaultMenuShouldNotBeFound("level.greaterThan=" + DEFAULT_LEVEL);

        // Get all the menuList where level is greater than SMALLER_LEVEL
        defaultMenuShouldBeFound("level.greaterThan=" + SMALLER_LEVEL);
    }


    @Test
    @Transactional
    public void getAllMenusByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);

        // Get all the menuList where name equals to DEFAULT_NAME
        defaultMenuShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the menuList where name equals to UPDATED_NAME
        defaultMenuShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllMenusByNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);

        // Get all the menuList where name not equals to DEFAULT_NAME
        defaultMenuShouldNotBeFound("name.notEquals=" + DEFAULT_NAME);

        // Get all the menuList where name not equals to UPDATED_NAME
        defaultMenuShouldBeFound("name.notEquals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllMenusByNameIsInShouldWork() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);

        // Get all the menuList where name in DEFAULT_NAME or UPDATED_NAME
        defaultMenuShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the menuList where name equals to UPDATED_NAME
        defaultMenuShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllMenusByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);

        // Get all the menuList where name is not null
        defaultMenuShouldBeFound("name.specified=true");

        // Get all the menuList where name is null
        defaultMenuShouldNotBeFound("name.specified=false");
    }
                @Test
    @Transactional
    public void getAllMenusByNameContainsSomething() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);

        // Get all the menuList where name contains DEFAULT_NAME
        defaultMenuShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the menuList where name contains UPDATED_NAME
        defaultMenuShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllMenusByNameNotContainsSomething() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);

        // Get all the menuList where name does not contain DEFAULT_NAME
        defaultMenuShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the menuList where name does not contain UPDATED_NAME
        defaultMenuShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }


    @Test
    @Transactional
    public void getAllMenusByUriIsEqualToSomething() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);

        // Get all the menuList where uri equals to DEFAULT_URI
        defaultMenuShouldBeFound("uri.equals=" + DEFAULT_URI);

        // Get all the menuList where uri equals to UPDATED_URI
        defaultMenuShouldNotBeFound("uri.equals=" + UPDATED_URI);
    }

    @Test
    @Transactional
    public void getAllMenusByUriIsNotEqualToSomething() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);

        // Get all the menuList where uri not equals to DEFAULT_URI
        defaultMenuShouldNotBeFound("uri.notEquals=" + DEFAULT_URI);

        // Get all the menuList where uri not equals to UPDATED_URI
        defaultMenuShouldBeFound("uri.notEquals=" + UPDATED_URI);
    }

    @Test
    @Transactional
    public void getAllMenusByUriIsInShouldWork() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);

        // Get all the menuList where uri in DEFAULT_URI or UPDATED_URI
        defaultMenuShouldBeFound("uri.in=" + DEFAULT_URI + "," + UPDATED_URI);

        // Get all the menuList where uri equals to UPDATED_URI
        defaultMenuShouldNotBeFound("uri.in=" + UPDATED_URI);
    }

    @Test
    @Transactional
    public void getAllMenusByUriIsNullOrNotNull() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);

        // Get all the menuList where uri is not null
        defaultMenuShouldBeFound("uri.specified=true");

        // Get all the menuList where uri is null
        defaultMenuShouldNotBeFound("uri.specified=false");
    }
                @Test
    @Transactional
    public void getAllMenusByUriContainsSomething() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);

        // Get all the menuList where uri contains DEFAULT_URI
        defaultMenuShouldBeFound("uri.contains=" + DEFAULT_URI);

        // Get all the menuList where uri contains UPDATED_URI
        defaultMenuShouldNotBeFound("uri.contains=" + UPDATED_URI);
    }

    @Test
    @Transactional
    public void getAllMenusByUriNotContainsSomething() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);

        // Get all the menuList where uri does not contain DEFAULT_URI
        defaultMenuShouldNotBeFound("uri.doesNotContain=" + DEFAULT_URI);

        // Get all the menuList where uri does not contain UPDATED_URI
        defaultMenuShouldBeFound("uri.doesNotContain=" + UPDATED_URI);
    }


    @Test
    @Transactional
    public void getAllMenusByParentIsEqualToSomething() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);

        // Get all the menuList where parent equals to DEFAULT_PARENT
        defaultMenuShouldBeFound("parent.equals=" + DEFAULT_PARENT);

        // Get all the menuList where parent equals to UPDATED_PARENT
        defaultMenuShouldNotBeFound("parent.equals=" + UPDATED_PARENT);
    }

    @Test
    @Transactional
    public void getAllMenusByParentIsNotEqualToSomething() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);

        // Get all the menuList where parent not equals to DEFAULT_PARENT
        defaultMenuShouldNotBeFound("parent.notEquals=" + DEFAULT_PARENT);

        // Get all the menuList where parent not equals to UPDATED_PARENT
        defaultMenuShouldBeFound("parent.notEquals=" + UPDATED_PARENT);
    }

    @Test
    @Transactional
    public void getAllMenusByParentIsInShouldWork() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);

        // Get all the menuList where parent in DEFAULT_PARENT or UPDATED_PARENT
        defaultMenuShouldBeFound("parent.in=" + DEFAULT_PARENT + "," + UPDATED_PARENT);

        // Get all the menuList where parent equals to UPDATED_PARENT
        defaultMenuShouldNotBeFound("parent.in=" + UPDATED_PARENT);
    }

    @Test
    @Transactional
    public void getAllMenusByParentIsNullOrNotNull() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);

        // Get all the menuList where parent is not null
        defaultMenuShouldBeFound("parent.specified=true");

        // Get all the menuList where parent is null
        defaultMenuShouldNotBeFound("parent.specified=false");
    }

    @Test
    @Transactional
    public void getAllMenusByParentIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);

        // Get all the menuList where parent is greater than or equal to DEFAULT_PARENT
        defaultMenuShouldBeFound("parent.greaterThanOrEqual=" + DEFAULT_PARENT);

        // Get all the menuList where parent is greater than or equal to UPDATED_PARENT
        defaultMenuShouldNotBeFound("parent.greaterThanOrEqual=" + UPDATED_PARENT);
    }

    @Test
    @Transactional
    public void getAllMenusByParentIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);

        // Get all the menuList where parent is less than or equal to DEFAULT_PARENT
        defaultMenuShouldBeFound("parent.lessThanOrEqual=" + DEFAULT_PARENT);

        // Get all the menuList where parent is less than or equal to SMALLER_PARENT
        defaultMenuShouldNotBeFound("parent.lessThanOrEqual=" + SMALLER_PARENT);
    }

    @Test
    @Transactional
    public void getAllMenusByParentIsLessThanSomething() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);

        // Get all the menuList where parent is less than DEFAULT_PARENT
        defaultMenuShouldNotBeFound("parent.lessThan=" + DEFAULT_PARENT);

        // Get all the menuList where parent is less than UPDATED_PARENT
        defaultMenuShouldBeFound("parent.lessThan=" + UPDATED_PARENT);
    }

    @Test
    @Transactional
    public void getAllMenusByParentIsGreaterThanSomething() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);

        // Get all the menuList where parent is greater than DEFAULT_PARENT
        defaultMenuShouldNotBeFound("parent.greaterThan=" + DEFAULT_PARENT);

        // Get all the menuList where parent is greater than SMALLER_PARENT
        defaultMenuShouldBeFound("parent.greaterThan=" + SMALLER_PARENT);
    }


    @Test
    @Transactional
    public void getAllMenusByDonViIdIsEqualToSomething() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);

        // Get all the menuList where donViId equals to DEFAULT_DON_VI_ID
        defaultMenuShouldBeFound("donViId.equals=" + DEFAULT_DON_VI_ID);

        // Get all the menuList where donViId equals to UPDATED_DON_VI_ID
        defaultMenuShouldNotBeFound("donViId.equals=" + UPDATED_DON_VI_ID);
    }

    @Test
    @Transactional
    public void getAllMenusByDonViIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);

        // Get all the menuList where donViId not equals to DEFAULT_DON_VI_ID
        defaultMenuShouldNotBeFound("donViId.notEquals=" + DEFAULT_DON_VI_ID);

        // Get all the menuList where donViId not equals to UPDATED_DON_VI_ID
        defaultMenuShouldBeFound("donViId.notEquals=" + UPDATED_DON_VI_ID);
    }

    @Test
    @Transactional
    public void getAllMenusByDonViIdIsInShouldWork() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);

        // Get all the menuList where donViId in DEFAULT_DON_VI_ID or UPDATED_DON_VI_ID
        defaultMenuShouldBeFound("donViId.in=" + DEFAULT_DON_VI_ID + "," + UPDATED_DON_VI_ID);

        // Get all the menuList where donViId equals to UPDATED_DON_VI_ID
        defaultMenuShouldNotBeFound("donViId.in=" + UPDATED_DON_VI_ID);
    }

    @Test
    @Transactional
    public void getAllMenusByDonViIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);

        // Get all the menuList where donViId is not null
        defaultMenuShouldBeFound("donViId.specified=true");

        // Get all the menuList where donViId is null
        defaultMenuShouldNotBeFound("donViId.specified=false");
    }

    @Test
    @Transactional
    public void getAllMenusByDonViIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);

        // Get all the menuList where donViId is greater than or equal to DEFAULT_DON_VI_ID
        defaultMenuShouldBeFound("donViId.greaterThanOrEqual=" + DEFAULT_DON_VI_ID);

        // Get all the menuList where donViId is greater than or equal to UPDATED_DON_VI_ID
        defaultMenuShouldNotBeFound("donViId.greaterThanOrEqual=" + UPDATED_DON_VI_ID);
    }

    @Test
    @Transactional
    public void getAllMenusByDonViIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);

        // Get all the menuList where donViId is less than or equal to DEFAULT_DON_VI_ID
        defaultMenuShouldBeFound("donViId.lessThanOrEqual=" + DEFAULT_DON_VI_ID);

        // Get all the menuList where donViId is less than or equal to SMALLER_DON_VI_ID
        defaultMenuShouldNotBeFound("donViId.lessThanOrEqual=" + SMALLER_DON_VI_ID);
    }

    @Test
    @Transactional
    public void getAllMenusByDonViIdIsLessThanSomething() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);

        // Get all the menuList where donViId is less than DEFAULT_DON_VI_ID
        defaultMenuShouldNotBeFound("donViId.lessThan=" + DEFAULT_DON_VI_ID);

        // Get all the menuList where donViId is less than UPDATED_DON_VI_ID
        defaultMenuShouldBeFound("donViId.lessThan=" + UPDATED_DON_VI_ID);
    }

    @Test
    @Transactional
    public void getAllMenusByDonViIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);

        // Get all the menuList where donViId is greater than DEFAULT_DON_VI_ID
        defaultMenuShouldNotBeFound("donViId.greaterThan=" + DEFAULT_DON_VI_ID);

        // Get all the menuList where donViId is greater than SMALLER_DON_VI_ID
        defaultMenuShouldBeFound("donViId.greaterThan=" + SMALLER_DON_VI_ID);
    }


    @Test
    @Transactional
    public void getAllMenusByUserIsEqualToSomething() throws Exception {
        // Get already existing entity
        UserExtra user = menu.getUser();
        menuRepository.saveAndFlush(menu);
        Long userId = user.getId();

        // Get all the menuList where user equals to userId
        defaultMenuShouldBeFound("userId.equals=" + userId);

        // Get all the menuList where user equals to userId + 1
        defaultMenuShouldNotBeFound("userId.equals=" + (userId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultMenuShouldBeFound(String filter) throws Exception {
        restMenuMockMvc.perform(get("/api/menus?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(menu.getId().intValue())))
            .andExpect(jsonPath("$.[*].enabled").value(hasItem(DEFAULT_ENABLED.booleanValue())))
            .andExpect(jsonPath("$.[*].level").value(hasItem(DEFAULT_LEVEL)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].uri").value(hasItem(DEFAULT_URI)))
            .andExpect(jsonPath("$.[*].parent").value(hasItem(DEFAULT_PARENT)))
            .andExpect(jsonPath("$.[*].donViId").value(hasItem(DEFAULT_DON_VI_ID.intValue())));

        // Check, that the count call also returns 1
        restMenuMockMvc.perform(get("/api/menus/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultMenuShouldNotBeFound(String filter) throws Exception {
        restMenuMockMvc.perform(get("/api/menus?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restMenuMockMvc.perform(get("/api/menus/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingMenu() throws Exception {
        // Get the menu
        restMenuMockMvc.perform(get("/api/menus/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMenu() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);

        int databaseSizeBeforeUpdate = menuRepository.findAll().size();

        // Update the menu
        Menu updatedMenu = menuRepository.findById(menu.getId()).get();
        // Disconnect from session so that the updates on updatedMenu are not directly saved in db
        em.detach(updatedMenu);
        updatedMenu
            .enabled(UPDATED_ENABLED)
            .level(UPDATED_LEVEL)
            .name(UPDATED_NAME)
            .uri(UPDATED_URI)
            .parent(UPDATED_PARENT)
            .donViId(UPDATED_DON_VI_ID);
        MenuDTO menuDTO = menuMapper.toDto(updatedMenu);

        restMenuMockMvc.perform(put("/api/menus").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(menuDTO)))
            .andExpect(status().isOk());

        // Validate the Menu in the database
        List<Menu> menuList = menuRepository.findAll();
        assertThat(menuList).hasSize(databaseSizeBeforeUpdate);
        Menu testMenu = menuList.get(menuList.size() - 1);
        assertThat(testMenu.isEnabled()).isEqualTo(UPDATED_ENABLED);
        assertThat(testMenu.getLevel()).isEqualTo(UPDATED_LEVEL);
        assertThat(testMenu.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testMenu.getUri()).isEqualTo(UPDATED_URI);
        assertThat(testMenu.getParent()).isEqualTo(UPDATED_PARENT);
        assertThat(testMenu.getDonViId()).isEqualTo(UPDATED_DON_VI_ID);
    }

    @Test
    @Transactional
    public void updateNonExistingMenu() throws Exception {
        int databaseSizeBeforeUpdate = menuRepository.findAll().size();

        // Create the Menu
        MenuDTO menuDTO = menuMapper.toDto(menu);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMenuMockMvc.perform(put("/api/menus").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(menuDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Menu in the database
        List<Menu> menuList = menuRepository.findAll();
        assertThat(menuList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteMenu() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);

        int databaseSizeBeforeDelete = menuRepository.findAll().size();

        // Delete the menu
        restMenuMockMvc.perform(delete("/api/menus/{id}", menu.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Menu> menuList = menuRepository.findAll();
        assertThat(menuList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

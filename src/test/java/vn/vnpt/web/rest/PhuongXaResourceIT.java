package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.PhuongXa;
import vn.vnpt.domain.QuanHuyen;
import vn.vnpt.repository.PhuongXaRepository;
import vn.vnpt.service.PhuongXaService;
import vn.vnpt.service.dto.PhuongXaDTO;
import vn.vnpt.service.mapper.PhuongXaMapper;
import vn.vnpt.service.dto.PhuongXaCriteria;
import vn.vnpt.service.PhuongXaQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link PhuongXaResource} REST controller.
 */
@SpringBootTest(classes = {KhamchuabenhApp.class, TestSecurityConfiguration.class})

@AutoConfigureMockMvc
@WithMockUser
public class PhuongXaResourceIT {

    private static final String DEFAULT_CAP = "AAAAAAAAAA";
    private static final String UPDATED_CAP = "BBBBBBBBBB";

    private static final String DEFAULT_GUESS_PHRASE = "AAAAAAAAAA";
    private static final String UPDATED_GUESS_PHRASE = "BBBBBBBBBB";

    private static final String DEFAULT_TEN = "AAAAAAAAAA";
    private static final String UPDATED_TEN = "BBBBBBBBBB";

    private static final String DEFAULT_TEN_KHONG_DAU = "AAAAAAAAAA";
    private static final String UPDATED_TEN_KHONG_DAU = "BBBBBBBBBB";

    @Autowired
    private PhuongXaRepository phuongXaRepository;

    @Autowired
    private PhuongXaMapper phuongXaMapper;

    @Autowired
    private PhuongXaService phuongXaService;

    @Autowired
    private PhuongXaQueryService phuongXaQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPhuongXaMockMvc;

    private PhuongXa phuongXa;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PhuongXa createEntity(EntityManager em) {
        PhuongXa phuongXa = new PhuongXa()
            .cap(DEFAULT_CAP)
            .guessPhrase(DEFAULT_GUESS_PHRASE)
            .ten(DEFAULT_TEN)
            .tenKhongDau(DEFAULT_TEN_KHONG_DAU);
        // Add required entity
        QuanHuyen quanHuyen;
        if (TestUtil.findAll(em, QuanHuyen.class).isEmpty()) {
            quanHuyen = QuanHuyenResourceIT.createEntity(em);
            em.persist(quanHuyen);
            em.flush();
        } else {
            quanHuyen = TestUtil.findAll(em, QuanHuyen.class).get(0);
        }
        phuongXa.setQuanHuyen(quanHuyen);
        return phuongXa;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PhuongXa createUpdatedEntity(EntityManager em) {
        PhuongXa phuongXa = new PhuongXa()
            .cap(UPDATED_CAP)
            .guessPhrase(UPDATED_GUESS_PHRASE)
            .ten(UPDATED_TEN)
            .tenKhongDau(UPDATED_TEN_KHONG_DAU);
        // Add required entity
        QuanHuyen quanHuyen;
        if (TestUtil.findAll(em, QuanHuyen.class).isEmpty()) {
            quanHuyen = QuanHuyenResourceIT.createUpdatedEntity(em);
            em.persist(quanHuyen);
            em.flush();
        } else {
            quanHuyen = TestUtil.findAll(em, QuanHuyen.class).get(0);
        }
        phuongXa.setQuanHuyen(quanHuyen);
        return phuongXa;
    }

    @BeforeEach
    public void initTest() {
        phuongXa = createEntity(em);
    }

    @Test
    @Transactional
    public void createPhuongXa() throws Exception {
        int databaseSizeBeforeCreate = phuongXaRepository.findAll().size();

        // Create the PhuongXa
        PhuongXaDTO phuongXaDTO = phuongXaMapper.toDto(phuongXa);
        restPhuongXaMockMvc.perform(post("/api/phuong-xas").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(phuongXaDTO)))
            .andExpect(status().isCreated());

        // Validate the PhuongXa in the database
        List<PhuongXa> phuongXaList = phuongXaRepository.findAll();
        assertThat(phuongXaList).hasSize(databaseSizeBeforeCreate + 1);
        PhuongXa testPhuongXa = phuongXaList.get(phuongXaList.size() - 1);
        assertThat(testPhuongXa.getCap()).isEqualTo(DEFAULT_CAP);
        assertThat(testPhuongXa.getGuessPhrase()).isEqualTo(DEFAULT_GUESS_PHRASE);
        assertThat(testPhuongXa.getTen()).isEqualTo(DEFAULT_TEN);
        assertThat(testPhuongXa.getTenKhongDau()).isEqualTo(DEFAULT_TEN_KHONG_DAU);
    }

    @Test
    @Transactional
    public void createPhuongXaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = phuongXaRepository.findAll().size();

        // Create the PhuongXa with an existing ID
        phuongXa.setId(1L);
        PhuongXaDTO phuongXaDTO = phuongXaMapper.toDto(phuongXa);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPhuongXaMockMvc.perform(post("/api/phuong-xas").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(phuongXaDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PhuongXa in the database
        List<PhuongXa> phuongXaList = phuongXaRepository.findAll();
        assertThat(phuongXaList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkTenIsRequired() throws Exception {
        int databaseSizeBeforeTest = phuongXaRepository.findAll().size();
        // set the field null
        phuongXa.setTen(null);

        // Create the PhuongXa, which fails.
        PhuongXaDTO phuongXaDTO = phuongXaMapper.toDto(phuongXa);

        restPhuongXaMockMvc.perform(post("/api/phuong-xas").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(phuongXaDTO)))
            .andExpect(status().isBadRequest());

        List<PhuongXa> phuongXaList = phuongXaRepository.findAll();
        assertThat(phuongXaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTenKhongDauIsRequired() throws Exception {
        int databaseSizeBeforeTest = phuongXaRepository.findAll().size();
        // set the field null
        phuongXa.setTenKhongDau(null);

        // Create the PhuongXa, which fails.
        PhuongXaDTO phuongXaDTO = phuongXaMapper.toDto(phuongXa);

        restPhuongXaMockMvc.perform(post("/api/phuong-xas").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(phuongXaDTO)))
            .andExpect(status().isBadRequest());

        List<PhuongXa> phuongXaList = phuongXaRepository.findAll();
        assertThat(phuongXaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPhuongXas() throws Exception {
        // Initialize the database
        phuongXaRepository.saveAndFlush(phuongXa);

        // Get all the phuongXaList
        restPhuongXaMockMvc.perform(get("/api/phuong-xas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(phuongXa.getId().intValue())))
            .andExpect(jsonPath("$.[*].cap").value(hasItem(DEFAULT_CAP)))
            .andExpect(jsonPath("$.[*].guessPhrase").value(hasItem(DEFAULT_GUESS_PHRASE)))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].tenKhongDau").value(hasItem(DEFAULT_TEN_KHONG_DAU)));
    }

    @Test
    @Transactional
    public void getPhuongXa() throws Exception {
        // Initialize the database
        phuongXaRepository.saveAndFlush(phuongXa);

        // Get the phuongXa
        restPhuongXaMockMvc.perform(get("/api/phuong-xas/{id}", phuongXa.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(phuongXa.getId().intValue()))
            .andExpect(jsonPath("$.cap").value(DEFAULT_CAP))
            .andExpect(jsonPath("$.guessPhrase").value(DEFAULT_GUESS_PHRASE))
            .andExpect(jsonPath("$.ten").value(DEFAULT_TEN))
            .andExpect(jsonPath("$.tenKhongDau").value(DEFAULT_TEN_KHONG_DAU));
    }


    @Test
    @Transactional
    public void getPhuongXasByIdFiltering() throws Exception {
        // Initialize the database
        phuongXaRepository.saveAndFlush(phuongXa);

        Long id = phuongXa.getId();

        defaultPhuongXaShouldBeFound("id.equals=" + id);
        defaultPhuongXaShouldNotBeFound("id.notEquals=" + id);

        defaultPhuongXaShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultPhuongXaShouldNotBeFound("id.greaterThan=" + id);

        defaultPhuongXaShouldBeFound("id.lessThanOrEqual=" + id);
        defaultPhuongXaShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllPhuongXasByCapIsEqualToSomething() throws Exception {
        // Initialize the database
        phuongXaRepository.saveAndFlush(phuongXa);

        // Get all the phuongXaList where cap equals to DEFAULT_CAP
        defaultPhuongXaShouldBeFound("cap.equals=" + DEFAULT_CAP);

        // Get all the phuongXaList where cap equals to UPDATED_CAP
        defaultPhuongXaShouldNotBeFound("cap.equals=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    public void getAllPhuongXasByCapIsNotEqualToSomething() throws Exception {
        // Initialize the database
        phuongXaRepository.saveAndFlush(phuongXa);

        // Get all the phuongXaList where cap not equals to DEFAULT_CAP
        defaultPhuongXaShouldNotBeFound("cap.notEquals=" + DEFAULT_CAP);

        // Get all the phuongXaList where cap not equals to UPDATED_CAP
        defaultPhuongXaShouldBeFound("cap.notEquals=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    public void getAllPhuongXasByCapIsInShouldWork() throws Exception {
        // Initialize the database
        phuongXaRepository.saveAndFlush(phuongXa);

        // Get all the phuongXaList where cap in DEFAULT_CAP or UPDATED_CAP
        defaultPhuongXaShouldBeFound("cap.in=" + DEFAULT_CAP + "," + UPDATED_CAP);

        // Get all the phuongXaList where cap equals to UPDATED_CAP
        defaultPhuongXaShouldNotBeFound("cap.in=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    public void getAllPhuongXasByCapIsNullOrNotNull() throws Exception {
        // Initialize the database
        phuongXaRepository.saveAndFlush(phuongXa);

        // Get all the phuongXaList where cap is not null
        defaultPhuongXaShouldBeFound("cap.specified=true");

        // Get all the phuongXaList where cap is null
        defaultPhuongXaShouldNotBeFound("cap.specified=false");
    }

    @Test
    @Transactional
    public void getAllPhuongXasByCapContainsSomething() throws Exception {
        // Initialize the database
        phuongXaRepository.saveAndFlush(phuongXa);

        // Get all the phuongXaList where cap contains DEFAULT_CAP
        defaultPhuongXaShouldBeFound("cap.contains=" + DEFAULT_CAP);

        // Get all the phuongXaList where cap contains UPDATED_CAP
        defaultPhuongXaShouldNotBeFound("cap.contains=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    public void getAllPhuongXasByCapNotContainsSomething() throws Exception {
        // Initialize the database
        phuongXaRepository.saveAndFlush(phuongXa);

        // Get all the phuongXaList where cap does not contain DEFAULT_CAP
        defaultPhuongXaShouldNotBeFound("cap.doesNotContain=" + DEFAULT_CAP);

        // Get all the phuongXaList where cap does not contain UPDATED_CAP
        defaultPhuongXaShouldBeFound("cap.doesNotContain=" + UPDATED_CAP);
    }


    @Test
    @Transactional
    public void getAllPhuongXasByGuessPhraseIsEqualToSomething() throws Exception {
        // Initialize the database
        phuongXaRepository.saveAndFlush(phuongXa);

        // Get all the phuongXaList where guessPhrase equals to DEFAULT_GUESS_PHRASE
        defaultPhuongXaShouldBeFound("guessPhrase.equals=" + DEFAULT_GUESS_PHRASE);

        // Get all the phuongXaList where guessPhrase equals to UPDATED_GUESS_PHRASE
        defaultPhuongXaShouldNotBeFound("guessPhrase.equals=" + UPDATED_GUESS_PHRASE);
    }

    @Test
    @Transactional
    public void getAllPhuongXasByGuessPhraseIsNotEqualToSomething() throws Exception {
        // Initialize the database
        phuongXaRepository.saveAndFlush(phuongXa);

        // Get all the phuongXaList where guessPhrase not equals to DEFAULT_GUESS_PHRASE
        defaultPhuongXaShouldNotBeFound("guessPhrase.notEquals=" + DEFAULT_GUESS_PHRASE);

        // Get all the phuongXaList where guessPhrase not equals to UPDATED_GUESS_PHRASE
        defaultPhuongXaShouldBeFound("guessPhrase.notEquals=" + UPDATED_GUESS_PHRASE);
    }

    @Test
    @Transactional
    public void getAllPhuongXasByGuessPhraseIsInShouldWork() throws Exception {
        // Initialize the database
        phuongXaRepository.saveAndFlush(phuongXa);

        // Get all the phuongXaList where guessPhrase in DEFAULT_GUESS_PHRASE or UPDATED_GUESS_PHRASE
        defaultPhuongXaShouldBeFound("guessPhrase.in=" + DEFAULT_GUESS_PHRASE + "," + UPDATED_GUESS_PHRASE);

        // Get all the phuongXaList where guessPhrase equals to UPDATED_GUESS_PHRASE
        defaultPhuongXaShouldNotBeFound("guessPhrase.in=" + UPDATED_GUESS_PHRASE);
    }

    @Test
    @Transactional
    public void getAllPhuongXasByGuessPhraseIsNullOrNotNull() throws Exception {
        // Initialize the database
        phuongXaRepository.saveAndFlush(phuongXa);

        // Get all the phuongXaList where guessPhrase is not null
        defaultPhuongXaShouldBeFound("guessPhrase.specified=true");

        // Get all the phuongXaList where guessPhrase is null
        defaultPhuongXaShouldNotBeFound("guessPhrase.specified=false");
    }
                @Test
    @Transactional
    public void getAllPhuongXasByGuessPhraseContainsSomething() throws Exception {
        // Initialize the database
        phuongXaRepository.saveAndFlush(phuongXa);

        // Get all the phuongXaList where guessPhrase contains DEFAULT_GUESS_PHRASE
        defaultPhuongXaShouldBeFound("guessPhrase.contains=" + DEFAULT_GUESS_PHRASE);

        // Get all the phuongXaList where guessPhrase contains UPDATED_GUESS_PHRASE
        defaultPhuongXaShouldNotBeFound("guessPhrase.contains=" + UPDATED_GUESS_PHRASE);
    }

    @Test
    @Transactional
    public void getAllPhuongXasByGuessPhraseNotContainsSomething() throws Exception {
        // Initialize the database
        phuongXaRepository.saveAndFlush(phuongXa);

        // Get all the phuongXaList where guessPhrase does not contain DEFAULT_GUESS_PHRASE
        defaultPhuongXaShouldNotBeFound("guessPhrase.doesNotContain=" + DEFAULT_GUESS_PHRASE);

        // Get all the phuongXaList where guessPhrase does not contain UPDATED_GUESS_PHRASE
        defaultPhuongXaShouldBeFound("guessPhrase.doesNotContain=" + UPDATED_GUESS_PHRASE);
    }


    @Test
    @Transactional
    public void getAllPhuongXasByTenIsEqualToSomething() throws Exception {
        // Initialize the database
        phuongXaRepository.saveAndFlush(phuongXa);

        // Get all the phuongXaList where ten equals to DEFAULT_TEN
        defaultPhuongXaShouldBeFound("ten.equals=" + DEFAULT_TEN);

        // Get all the phuongXaList where ten equals to UPDATED_TEN
        defaultPhuongXaShouldNotBeFound("ten.equals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllPhuongXasByTenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        phuongXaRepository.saveAndFlush(phuongXa);

        // Get all the phuongXaList where ten not equals to DEFAULT_TEN
        defaultPhuongXaShouldNotBeFound("ten.notEquals=" + DEFAULT_TEN);

        // Get all the phuongXaList where ten not equals to UPDATED_TEN
        defaultPhuongXaShouldBeFound("ten.notEquals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllPhuongXasByTenIsInShouldWork() throws Exception {
        // Initialize the database
        phuongXaRepository.saveAndFlush(phuongXa);

        // Get all the phuongXaList where ten in DEFAULT_TEN or UPDATED_TEN
        defaultPhuongXaShouldBeFound("ten.in=" + DEFAULT_TEN + "," + UPDATED_TEN);

        // Get all the phuongXaList where ten equals to UPDATED_TEN
        defaultPhuongXaShouldNotBeFound("ten.in=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllPhuongXasByTenIsNullOrNotNull() throws Exception {
        // Initialize the database
        phuongXaRepository.saveAndFlush(phuongXa);

        // Get all the phuongXaList where ten is not null
        defaultPhuongXaShouldBeFound("ten.specified=true");

        // Get all the phuongXaList where ten is null
        defaultPhuongXaShouldNotBeFound("ten.specified=false");
    }
                @Test
    @Transactional
    public void getAllPhuongXasByTenContainsSomething() throws Exception {
        // Initialize the database
        phuongXaRepository.saveAndFlush(phuongXa);

        // Get all the phuongXaList where ten contains DEFAULT_TEN
        defaultPhuongXaShouldBeFound("ten.contains=" + DEFAULT_TEN);

        // Get all the phuongXaList where ten contains UPDATED_TEN
        defaultPhuongXaShouldNotBeFound("ten.contains=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllPhuongXasByTenNotContainsSomething() throws Exception {
        // Initialize the database
        phuongXaRepository.saveAndFlush(phuongXa);

        // Get all the phuongXaList where ten does not contain DEFAULT_TEN
        defaultPhuongXaShouldNotBeFound("ten.doesNotContain=" + DEFAULT_TEN);

        // Get all the phuongXaList where ten does not contain UPDATED_TEN
        defaultPhuongXaShouldBeFound("ten.doesNotContain=" + UPDATED_TEN);
    }


    @Test
    @Transactional
    public void getAllPhuongXasByTenKhongDauIsEqualToSomething() throws Exception {
        // Initialize the database
        phuongXaRepository.saveAndFlush(phuongXa);

        // Get all the phuongXaList where tenKhongDau equals to DEFAULT_TEN_KHONG_DAU
        defaultPhuongXaShouldBeFound("tenKhongDau.equals=" + DEFAULT_TEN_KHONG_DAU);

        // Get all the phuongXaList where tenKhongDau equals to UPDATED_TEN_KHONG_DAU
        defaultPhuongXaShouldNotBeFound("tenKhongDau.equals=" + UPDATED_TEN_KHONG_DAU);
    }

    @Test
    @Transactional
    public void getAllPhuongXasByTenKhongDauIsNotEqualToSomething() throws Exception {
        // Initialize the database
        phuongXaRepository.saveAndFlush(phuongXa);

        // Get all the phuongXaList where tenKhongDau not equals to DEFAULT_TEN_KHONG_DAU
        defaultPhuongXaShouldNotBeFound("tenKhongDau.notEquals=" + DEFAULT_TEN_KHONG_DAU);

        // Get all the phuongXaList where tenKhongDau not equals to UPDATED_TEN_KHONG_DAU
        defaultPhuongXaShouldBeFound("tenKhongDau.notEquals=" + UPDATED_TEN_KHONG_DAU);
    }

    @Test
    @Transactional
    public void getAllPhuongXasByTenKhongDauIsInShouldWork() throws Exception {
        // Initialize the database
        phuongXaRepository.saveAndFlush(phuongXa);

        // Get all the phuongXaList where tenKhongDau in DEFAULT_TEN_KHONG_DAU or UPDATED_TEN_KHONG_DAU
        defaultPhuongXaShouldBeFound("tenKhongDau.in=" + DEFAULT_TEN_KHONG_DAU + "," + UPDATED_TEN_KHONG_DAU);

        // Get all the phuongXaList where tenKhongDau equals to UPDATED_TEN_KHONG_DAU
        defaultPhuongXaShouldNotBeFound("tenKhongDau.in=" + UPDATED_TEN_KHONG_DAU);
    }

    @Test
    @Transactional
    public void getAllPhuongXasByTenKhongDauIsNullOrNotNull() throws Exception {
        // Initialize the database
        phuongXaRepository.saveAndFlush(phuongXa);

        // Get all the phuongXaList where tenKhongDau is not null
        defaultPhuongXaShouldBeFound("tenKhongDau.specified=true");

        // Get all the phuongXaList where tenKhongDau is null
        defaultPhuongXaShouldNotBeFound("tenKhongDau.specified=false");
    }
                @Test
    @Transactional
    public void getAllPhuongXasByTenKhongDauContainsSomething() throws Exception {
        // Initialize the database
        phuongXaRepository.saveAndFlush(phuongXa);

        // Get all the phuongXaList where tenKhongDau contains DEFAULT_TEN_KHONG_DAU
        defaultPhuongXaShouldBeFound("tenKhongDau.contains=" + DEFAULT_TEN_KHONG_DAU);

        // Get all the phuongXaList where tenKhongDau contains UPDATED_TEN_KHONG_DAU
        defaultPhuongXaShouldNotBeFound("tenKhongDau.contains=" + UPDATED_TEN_KHONG_DAU);
    }

    @Test
    @Transactional
    public void getAllPhuongXasByTenKhongDauNotContainsSomething() throws Exception {
        // Initialize the database
        phuongXaRepository.saveAndFlush(phuongXa);

        // Get all the phuongXaList where tenKhongDau does not contain DEFAULT_TEN_KHONG_DAU
        defaultPhuongXaShouldNotBeFound("tenKhongDau.doesNotContain=" + DEFAULT_TEN_KHONG_DAU);

        // Get all the phuongXaList where tenKhongDau does not contain UPDATED_TEN_KHONG_DAU
        defaultPhuongXaShouldBeFound("tenKhongDau.doesNotContain=" + UPDATED_TEN_KHONG_DAU);
    }


    @Test
    @Transactional
    public void getAllPhuongXasByQuanHuyenIsEqualToSomething() throws Exception {
        // Get already existing entity
        QuanHuyen quanHuyen = phuongXa.getQuanHuyen();
        phuongXaRepository.saveAndFlush(phuongXa);
        Long quanHuyenId = quanHuyen.getId();

        // Get all the phuongXaList where quanHuyen equals to quanHuyenId
        defaultPhuongXaShouldBeFound("quanHuyenId.equals=" + quanHuyenId);

        // Get all the phuongXaList where quanHuyen equals to quanHuyenId + 1
        defaultPhuongXaShouldNotBeFound("quanHuyenId.equals=" + (quanHuyenId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultPhuongXaShouldBeFound(String filter) throws Exception {
        restPhuongXaMockMvc.perform(get("/api/phuong-xas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(phuongXa.getId().intValue())))
            .andExpect(jsonPath("$.[*].cap").value(hasItem(DEFAULT_CAP)))
            .andExpect(jsonPath("$.[*].guessPhrase").value(hasItem(DEFAULT_GUESS_PHRASE)))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].tenKhongDau").value(hasItem(DEFAULT_TEN_KHONG_DAU)));

        // Check, that the count call also returns 1
        restPhuongXaMockMvc.perform(get("/api/phuong-xas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultPhuongXaShouldNotBeFound(String filter) throws Exception {
        restPhuongXaMockMvc.perform(get("/api/phuong-xas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restPhuongXaMockMvc.perform(get("/api/phuong-xas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingPhuongXa() throws Exception {
        // Get the phuongXa
        restPhuongXaMockMvc.perform(get("/api/phuong-xas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePhuongXa() throws Exception {
        // Initialize the database
        phuongXaRepository.saveAndFlush(phuongXa);

        int databaseSizeBeforeUpdate = phuongXaRepository.findAll().size();

        // Update the phuongXa
        PhuongXa updatedPhuongXa = phuongXaRepository.findById(phuongXa.getId()).get();
        // Disconnect from session so that the updates on updatedPhuongXa are not directly saved in db
        em.detach(updatedPhuongXa);
        updatedPhuongXa
            .cap(UPDATED_CAP)
            .guessPhrase(UPDATED_GUESS_PHRASE)
            .ten(UPDATED_TEN)
            .tenKhongDau(UPDATED_TEN_KHONG_DAU);
        PhuongXaDTO phuongXaDTO = phuongXaMapper.toDto(updatedPhuongXa);

        restPhuongXaMockMvc.perform(put("/api/phuong-xas").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(phuongXaDTO)))
            .andExpect(status().isOk());

        // Validate the PhuongXa in the database
        List<PhuongXa> phuongXaList = phuongXaRepository.findAll();
        assertThat(phuongXaList).hasSize(databaseSizeBeforeUpdate);
        PhuongXa testPhuongXa = phuongXaList.get(phuongXaList.size() - 1);
        assertThat(testPhuongXa.getCap()).isEqualTo(UPDATED_CAP);
        assertThat(testPhuongXa.getGuessPhrase()).isEqualTo(UPDATED_GUESS_PHRASE);
        assertThat(testPhuongXa.getTen()).isEqualTo(UPDATED_TEN);
        assertThat(testPhuongXa.getTenKhongDau()).isEqualTo(UPDATED_TEN_KHONG_DAU);
    }

    @Test
    @Transactional
    public void updateNonExistingPhuongXa() throws Exception {
        int databaseSizeBeforeUpdate = phuongXaRepository.findAll().size();

        // Create the PhuongXa
        PhuongXaDTO phuongXaDTO = phuongXaMapper.toDto(phuongXa);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPhuongXaMockMvc.perform(put("/api/phuong-xas").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(phuongXaDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PhuongXa in the database
        List<PhuongXa> phuongXaList = phuongXaRepository.findAll();
        assertThat(phuongXaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePhuongXa() throws Exception {
        // Initialize the database
        phuongXaRepository.saveAndFlush(phuongXa);

        int databaseSizeBeforeDelete = phuongXaRepository.findAll().size();

        // Delete the phuongXa
        restPhuongXaMockMvc.perform(delete("/api/phuong-xas/{id}", phuongXa.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<PhuongXa> phuongXaList = phuongXaRepository.findAll();
        assertThat(phuongXaList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

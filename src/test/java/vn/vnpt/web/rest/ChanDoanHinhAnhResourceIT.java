package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.ChanDoanHinhAnh;
import vn.vnpt.domain.DonVi;
import vn.vnpt.domain.DotThayDoiMaDichVu;
import vn.vnpt.domain.LoaiChanDoanHinhAnh;
import vn.vnpt.repository.ChanDoanHinhAnhRepository;
import vn.vnpt.service.ChanDoanHinhAnhService;
import vn.vnpt.service.dto.ChanDoanHinhAnhDTO;
import vn.vnpt.service.mapper.ChanDoanHinhAnhMapper;
import vn.vnpt.service.dto.ChanDoanHinhAnhCriteria;
import vn.vnpt.service.ChanDoanHinhAnhQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ChanDoanHinhAnhResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class ChanDoanHinhAnhResourceIT {

    private static final Boolean DEFAULT_DELETED = false;
    private static final Boolean UPDATED_DELETED = true;

    private static final Boolean DEFAULT_DICH_VU_YEU_CAU = false;
    private static final Boolean UPDATED_DICH_VU_YEU_CAU = true;

    private static final Boolean DEFAULT_DOPPLER = false;
    private static final Boolean UPDATED_DOPPLER = true;

    private static final BigDecimal DEFAULT_DON_GIA_BENH_VIEN = new BigDecimal(1);
    private static final BigDecimal UPDATED_DON_GIA_BENH_VIEN = new BigDecimal(2);
    private static final BigDecimal SMALLER_DON_GIA_BENH_VIEN = new BigDecimal(1 - 1);

    private static final Boolean DEFAULT_ENABLED = false;
    private static final Boolean UPDATED_ENABLED = true;

    private static final BigDecimal DEFAULT_GOI_HAN_CHI_DINH = new BigDecimal(1);
    private static final BigDecimal UPDATED_GOI_HAN_CHI_DINH = new BigDecimal(2);
    private static final BigDecimal SMALLER_GOI_HAN_CHI_DINH = new BigDecimal(1 - 1);

    private static final Boolean DEFAULT_PHAM_VI_CHI_DINH = false;
    private static final Boolean UPDATED_PHAM_VI_CHI_DINH = true;

    private static final Integer DEFAULT_PHAN_THEO_GIOI_TINH = 1;
    private static final Integer UPDATED_PHAN_THEO_GIOI_TINH = 2;
    private static final Integer SMALLER_PHAN_THEO_GIOI_TINH = 1 - 1;

    private static final Integer DEFAULT_SAP_XEP = 1;
    private static final Integer UPDATED_SAP_XEP = 2;
    private static final Integer SMALLER_SAP_XEP = 1 - 1;

    private static final Integer DEFAULT_SO_LAN_THUC_HIEN = 1;
    private static final Integer UPDATED_SO_LAN_THUC_HIEN = 2;
    private static final Integer SMALLER_SO_LAN_THUC_HIEN = 1 - 1;

    private static final BigDecimal DEFAULT_SO_LUONG_FILM = new BigDecimal(1);
    private static final BigDecimal UPDATED_SO_LUONG_FILM = new BigDecimal(2);
    private static final BigDecimal SMALLER_SO_LUONG_FILM = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_SO_LUONG_QUI_DOI = new BigDecimal(1);
    private static final BigDecimal UPDATED_SO_LUONG_QUI_DOI = new BigDecimal(2);
    private static final BigDecimal SMALLER_SO_LUONG_QUI_DOI = new BigDecimal(1 - 1);

    private static final String DEFAULT_TEN = "AAAAAAAAAA";
    private static final String UPDATED_TEN = "BBBBBBBBBB";

    private static final String DEFAULT_TEN_HIEN_THI = "AAAAAAAAAA";
    private static final String UPDATED_TEN_HIEN_THI = "BBBBBBBBBB";

    private static final String DEFAULT_MA_NOI_BO = "AAAAAAAAAA";
    private static final String UPDATED_MA_NOI_BO = "BBBBBBBBBB";

    private static final String DEFAULT_MA_DUNG_CHUNG = "AAAAAAAAAA";
    private static final String UPDATED_MA_DUNG_CHUNG = "BBBBBBBBBB";

    @Autowired
    private ChanDoanHinhAnhRepository chanDoanHinhAnhRepository;

    @Autowired
    private ChanDoanHinhAnhMapper chanDoanHinhAnhMapper;

    @Autowired
    private ChanDoanHinhAnhService chanDoanHinhAnhService;

    @Autowired
    private ChanDoanHinhAnhQueryService chanDoanHinhAnhQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restChanDoanHinhAnhMockMvc;

    private ChanDoanHinhAnh chanDoanHinhAnh;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ChanDoanHinhAnh createEntity(EntityManager em) {
        ChanDoanHinhAnh chanDoanHinhAnh = new ChanDoanHinhAnh()
            .deleted(DEFAULT_DELETED)
            .dichVuYeuCau(DEFAULT_DICH_VU_YEU_CAU)
            .doppler(DEFAULT_DOPPLER)
            .donGiaBenhVien(DEFAULT_DON_GIA_BENH_VIEN)
            .enabled(DEFAULT_ENABLED)
            .goiHanChiDinh(DEFAULT_GOI_HAN_CHI_DINH)
            .phamViChiDinh(DEFAULT_PHAM_VI_CHI_DINH)
            .phanTheoGioiTinh(DEFAULT_PHAN_THEO_GIOI_TINH)
            .sapXep(DEFAULT_SAP_XEP)
            .soLanThucHien(DEFAULT_SO_LAN_THUC_HIEN)
            .soLuongFilm(DEFAULT_SO_LUONG_FILM)
            .soLuongQuiDoi(DEFAULT_SO_LUONG_QUI_DOI)
            .ten(DEFAULT_TEN)
            .tenHienThi(DEFAULT_TEN_HIEN_THI)
            .maNoiBo(DEFAULT_MA_NOI_BO)
            .maDungChung(DEFAULT_MA_DUNG_CHUNG);
        // Add required entity
        DonVi donVi;
        if (TestUtil.findAll(em, DonVi.class).isEmpty()) {
            donVi = DonViResourceIT.createEntity(em);
            em.persist(donVi);
            em.flush();
        } else {
            donVi = TestUtil.findAll(em, DonVi.class).get(0);
        }
        chanDoanHinhAnh.setDonVi(donVi);
        // Add required entity
        DotThayDoiMaDichVu dotThayDoiMaDichVu;
        if (TestUtil.findAll(em, DotThayDoiMaDichVu.class).isEmpty()) {
            dotThayDoiMaDichVu = DotThayDoiMaDichVuResourceIT.createEntity(em);
            em.persist(dotThayDoiMaDichVu);
            em.flush();
        } else {
            dotThayDoiMaDichVu = TestUtil.findAll(em, DotThayDoiMaDichVu.class).get(0);
        }
        chanDoanHinhAnh.setDotMa(dotThayDoiMaDichVu);
        // Add required entity
        LoaiChanDoanHinhAnh loaiChanDoanHinhAnh;
        if (TestUtil.findAll(em, LoaiChanDoanHinhAnh.class).isEmpty()) {
            loaiChanDoanHinhAnh = LoaiChanDoanHinhAnhResourceIT.createEntity(em);
            em.persist(loaiChanDoanHinhAnh);
            em.flush();
        } else {
            loaiChanDoanHinhAnh = TestUtil.findAll(em, LoaiChanDoanHinhAnh.class).get(0);
        }
        chanDoanHinhAnh.setLoaicdha(loaiChanDoanHinhAnh);
        return chanDoanHinhAnh;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ChanDoanHinhAnh createUpdatedEntity(EntityManager em) {
        ChanDoanHinhAnh chanDoanHinhAnh = new ChanDoanHinhAnh()
            .deleted(UPDATED_DELETED)
            .dichVuYeuCau(UPDATED_DICH_VU_YEU_CAU)
            .doppler(UPDATED_DOPPLER)
            .donGiaBenhVien(UPDATED_DON_GIA_BENH_VIEN)
            .enabled(UPDATED_ENABLED)
            .goiHanChiDinh(UPDATED_GOI_HAN_CHI_DINH)
            .phamViChiDinh(UPDATED_PHAM_VI_CHI_DINH)
            .phanTheoGioiTinh(UPDATED_PHAN_THEO_GIOI_TINH)
            .sapXep(UPDATED_SAP_XEP)
            .soLanThucHien(UPDATED_SO_LAN_THUC_HIEN)
            .soLuongFilm(UPDATED_SO_LUONG_FILM)
            .soLuongQuiDoi(UPDATED_SO_LUONG_QUI_DOI)
            .ten(UPDATED_TEN)
            .tenHienThi(UPDATED_TEN_HIEN_THI)
            .maNoiBo(UPDATED_MA_NOI_BO)
            .maDungChung(UPDATED_MA_DUNG_CHUNG);
        // Add required entity
        DonVi donVi;
        if (TestUtil.findAll(em, DonVi.class).isEmpty()) {
            donVi = DonViResourceIT.createUpdatedEntity(em);
            em.persist(donVi);
            em.flush();
        } else {
            donVi = TestUtil.findAll(em, DonVi.class).get(0);
        }
        chanDoanHinhAnh.setDonVi(donVi);
        // Add required entity
        DotThayDoiMaDichVu dotThayDoiMaDichVu;
        if (TestUtil.findAll(em, DotThayDoiMaDichVu.class).isEmpty()) {
            dotThayDoiMaDichVu = DotThayDoiMaDichVuResourceIT.createUpdatedEntity(em);
            em.persist(dotThayDoiMaDichVu);
            em.flush();
        } else {
            dotThayDoiMaDichVu = TestUtil.findAll(em, DotThayDoiMaDichVu.class).get(0);
        }
        chanDoanHinhAnh.setDotMa(dotThayDoiMaDichVu);
        // Add required entity
        LoaiChanDoanHinhAnh loaiChanDoanHinhAnh;
        if (TestUtil.findAll(em, LoaiChanDoanHinhAnh.class).isEmpty()) {
            loaiChanDoanHinhAnh = LoaiChanDoanHinhAnhResourceIT.createUpdatedEntity(em);
            em.persist(loaiChanDoanHinhAnh);
            em.flush();
        } else {
            loaiChanDoanHinhAnh = TestUtil.findAll(em, LoaiChanDoanHinhAnh.class).get(0);
        }
        chanDoanHinhAnh.setLoaicdha(loaiChanDoanHinhAnh);
        return chanDoanHinhAnh;
    }

    @BeforeEach
    public void initTest() {
        chanDoanHinhAnh = createEntity(em);
    }

    @Test
    @Transactional
    public void createChanDoanHinhAnh() throws Exception {
        int databaseSizeBeforeCreate = chanDoanHinhAnhRepository.findAll().size();

        // Create the ChanDoanHinhAnh
        ChanDoanHinhAnhDTO chanDoanHinhAnhDTO = chanDoanHinhAnhMapper.toDto(chanDoanHinhAnh);
        restChanDoanHinhAnhMockMvc.perform(post("/api/chan-doan-hinh-anhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chanDoanHinhAnhDTO)))
            .andExpect(status().isCreated());

        // Validate the ChanDoanHinhAnh in the database
        List<ChanDoanHinhAnh> chanDoanHinhAnhList = chanDoanHinhAnhRepository.findAll();
        assertThat(chanDoanHinhAnhList).hasSize(databaseSizeBeforeCreate + 1);
        ChanDoanHinhAnh testChanDoanHinhAnh = chanDoanHinhAnhList.get(chanDoanHinhAnhList.size() - 1);
        assertThat(testChanDoanHinhAnh.isDeleted()).isEqualTo(DEFAULT_DELETED);
        assertThat(testChanDoanHinhAnh.isDichVuYeuCau()).isEqualTo(DEFAULT_DICH_VU_YEU_CAU);
        assertThat(testChanDoanHinhAnh.isDoppler()).isEqualTo(DEFAULT_DOPPLER);
        assertThat(testChanDoanHinhAnh.getDonGiaBenhVien()).isEqualTo(DEFAULT_DON_GIA_BENH_VIEN);
        assertThat(testChanDoanHinhAnh.isEnabled()).isEqualTo(DEFAULT_ENABLED);
        assertThat(testChanDoanHinhAnh.getGoiHanChiDinh()).isEqualTo(DEFAULT_GOI_HAN_CHI_DINH);
        assertThat(testChanDoanHinhAnh.isPhamViChiDinh()).isEqualTo(DEFAULT_PHAM_VI_CHI_DINH);
        assertThat(testChanDoanHinhAnh.getPhanTheoGioiTinh()).isEqualTo(DEFAULT_PHAN_THEO_GIOI_TINH);
        assertThat(testChanDoanHinhAnh.getSapXep()).isEqualTo(DEFAULT_SAP_XEP);
        assertThat(testChanDoanHinhAnh.getSoLanThucHien()).isEqualTo(DEFAULT_SO_LAN_THUC_HIEN);
        assertThat(testChanDoanHinhAnh.getSoLuongFilm()).isEqualTo(DEFAULT_SO_LUONG_FILM);
        assertThat(testChanDoanHinhAnh.getSoLuongQuiDoi()).isEqualTo(DEFAULT_SO_LUONG_QUI_DOI);
        assertThat(testChanDoanHinhAnh.getTen()).isEqualTo(DEFAULT_TEN);
        assertThat(testChanDoanHinhAnh.getTenHienThi()).isEqualTo(DEFAULT_TEN_HIEN_THI);
        assertThat(testChanDoanHinhAnh.getMaNoiBo()).isEqualTo(DEFAULT_MA_NOI_BO);
        assertThat(testChanDoanHinhAnh.getMaDungChung()).isEqualTo(DEFAULT_MA_DUNG_CHUNG);
    }

    @Test
    @Transactional
    public void createChanDoanHinhAnhWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = chanDoanHinhAnhRepository.findAll().size();

        // Create the ChanDoanHinhAnh with an existing ID
        chanDoanHinhAnh.setId(1L);
        ChanDoanHinhAnhDTO chanDoanHinhAnhDTO = chanDoanHinhAnhMapper.toDto(chanDoanHinhAnh);

        // An entity with an existing ID cannot be created, so this API call must fail
        restChanDoanHinhAnhMockMvc.perform(post("/api/chan-doan-hinh-anhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chanDoanHinhAnhDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ChanDoanHinhAnh in the database
        List<ChanDoanHinhAnh> chanDoanHinhAnhList = chanDoanHinhAnhRepository.findAll();
        assertThat(chanDoanHinhAnhList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkDeletedIsRequired() throws Exception {
        int databaseSizeBeforeTest = chanDoanHinhAnhRepository.findAll().size();
        // set the field null
        chanDoanHinhAnh.setDeleted(null);

        // Create the ChanDoanHinhAnh, which fails.
        ChanDoanHinhAnhDTO chanDoanHinhAnhDTO = chanDoanHinhAnhMapper.toDto(chanDoanHinhAnh);

        restChanDoanHinhAnhMockMvc.perform(post("/api/chan-doan-hinh-anhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chanDoanHinhAnhDTO)))
            .andExpect(status().isBadRequest());

        List<ChanDoanHinhAnh> chanDoanHinhAnhList = chanDoanHinhAnhRepository.findAll();
        assertThat(chanDoanHinhAnhList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDichVuYeuCauIsRequired() throws Exception {
        int databaseSizeBeforeTest = chanDoanHinhAnhRepository.findAll().size();
        // set the field null
        chanDoanHinhAnh.setDichVuYeuCau(null);

        // Create the ChanDoanHinhAnh, which fails.
        ChanDoanHinhAnhDTO chanDoanHinhAnhDTO = chanDoanHinhAnhMapper.toDto(chanDoanHinhAnh);

        restChanDoanHinhAnhMockMvc.perform(post("/api/chan-doan-hinh-anhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chanDoanHinhAnhDTO)))
            .andExpect(status().isBadRequest());

        List<ChanDoanHinhAnh> chanDoanHinhAnhList = chanDoanHinhAnhRepository.findAll();
        assertThat(chanDoanHinhAnhList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDonGiaBenhVienIsRequired() throws Exception {
        int databaseSizeBeforeTest = chanDoanHinhAnhRepository.findAll().size();
        // set the field null
        chanDoanHinhAnh.setDonGiaBenhVien(null);

        // Create the ChanDoanHinhAnh, which fails.
        ChanDoanHinhAnhDTO chanDoanHinhAnhDTO = chanDoanHinhAnhMapper.toDto(chanDoanHinhAnh);

        restChanDoanHinhAnhMockMvc.perform(post("/api/chan-doan-hinh-anhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chanDoanHinhAnhDTO)))
            .andExpect(status().isBadRequest());

        List<ChanDoanHinhAnh> chanDoanHinhAnhList = chanDoanHinhAnhRepository.findAll();
        assertThat(chanDoanHinhAnhList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEnabledIsRequired() throws Exception {
        int databaseSizeBeforeTest = chanDoanHinhAnhRepository.findAll().size();
        // set the field null
        chanDoanHinhAnh.setEnabled(null);

        // Create the ChanDoanHinhAnh, which fails.
        ChanDoanHinhAnhDTO chanDoanHinhAnhDTO = chanDoanHinhAnhMapper.toDto(chanDoanHinhAnh);

        restChanDoanHinhAnhMockMvc.perform(post("/api/chan-doan-hinh-anhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chanDoanHinhAnhDTO)))
            .andExpect(status().isBadRequest());

        List<ChanDoanHinhAnh> chanDoanHinhAnhList = chanDoanHinhAnhRepository.findAll();
        assertThat(chanDoanHinhAnhList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPhamViChiDinhIsRequired() throws Exception {
        int databaseSizeBeforeTest = chanDoanHinhAnhRepository.findAll().size();
        // set the field null
        chanDoanHinhAnh.setPhamViChiDinh(null);

        // Create the ChanDoanHinhAnh, which fails.
        ChanDoanHinhAnhDTO chanDoanHinhAnhDTO = chanDoanHinhAnhMapper.toDto(chanDoanHinhAnh);

        restChanDoanHinhAnhMockMvc.perform(post("/api/chan-doan-hinh-anhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chanDoanHinhAnhDTO)))
            .andExpect(status().isBadRequest());

        List<ChanDoanHinhAnh> chanDoanHinhAnhList = chanDoanHinhAnhRepository.findAll();
        assertThat(chanDoanHinhAnhList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPhanTheoGioiTinhIsRequired() throws Exception {
        int databaseSizeBeforeTest = chanDoanHinhAnhRepository.findAll().size();
        // set the field null
        chanDoanHinhAnh.setPhanTheoGioiTinh(null);

        // Create the ChanDoanHinhAnh, which fails.
        ChanDoanHinhAnhDTO chanDoanHinhAnhDTO = chanDoanHinhAnhMapper.toDto(chanDoanHinhAnh);

        restChanDoanHinhAnhMockMvc.perform(post("/api/chan-doan-hinh-anhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chanDoanHinhAnhDTO)))
            .andExpect(status().isBadRequest());

        List<ChanDoanHinhAnh> chanDoanHinhAnhList = chanDoanHinhAnhRepository.findAll();
        assertThat(chanDoanHinhAnhList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSapXepIsRequired() throws Exception {
        int databaseSizeBeforeTest = chanDoanHinhAnhRepository.findAll().size();
        // set the field null
        chanDoanHinhAnh.setSapXep(null);

        // Create the ChanDoanHinhAnh, which fails.
        ChanDoanHinhAnhDTO chanDoanHinhAnhDTO = chanDoanHinhAnhMapper.toDto(chanDoanHinhAnh);

        restChanDoanHinhAnhMockMvc.perform(post("/api/chan-doan-hinh-anhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chanDoanHinhAnhDTO)))
            .andExpect(status().isBadRequest());

        List<ChanDoanHinhAnh> chanDoanHinhAnhList = chanDoanHinhAnhRepository.findAll();
        assertThat(chanDoanHinhAnhList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSoLanThucHienIsRequired() throws Exception {
        int databaseSizeBeforeTest = chanDoanHinhAnhRepository.findAll().size();
        // set the field null
        chanDoanHinhAnh.setSoLanThucHien(null);

        // Create the ChanDoanHinhAnh, which fails.
        ChanDoanHinhAnhDTO chanDoanHinhAnhDTO = chanDoanHinhAnhMapper.toDto(chanDoanHinhAnh);

        restChanDoanHinhAnhMockMvc.perform(post("/api/chan-doan-hinh-anhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chanDoanHinhAnhDTO)))
            .andExpect(status().isBadRequest());

        List<ChanDoanHinhAnh> chanDoanHinhAnhList = chanDoanHinhAnhRepository.findAll();
        assertThat(chanDoanHinhAnhList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSoLuongFilmIsRequired() throws Exception {
        int databaseSizeBeforeTest = chanDoanHinhAnhRepository.findAll().size();
        // set the field null
        chanDoanHinhAnh.setSoLuongFilm(null);

        // Create the ChanDoanHinhAnh, which fails.
        ChanDoanHinhAnhDTO chanDoanHinhAnhDTO = chanDoanHinhAnhMapper.toDto(chanDoanHinhAnh);

        restChanDoanHinhAnhMockMvc.perform(post("/api/chan-doan-hinh-anhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chanDoanHinhAnhDTO)))
            .andExpect(status().isBadRequest());

        List<ChanDoanHinhAnh> chanDoanHinhAnhList = chanDoanHinhAnhRepository.findAll();
        assertThat(chanDoanHinhAnhList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSoLuongQuiDoiIsRequired() throws Exception {
        int databaseSizeBeforeTest = chanDoanHinhAnhRepository.findAll().size();
        // set the field null
        chanDoanHinhAnh.setSoLuongQuiDoi(null);

        // Create the ChanDoanHinhAnh, which fails.
        ChanDoanHinhAnhDTO chanDoanHinhAnhDTO = chanDoanHinhAnhMapper.toDto(chanDoanHinhAnh);

        restChanDoanHinhAnhMockMvc.perform(post("/api/chan-doan-hinh-anhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chanDoanHinhAnhDTO)))
            .andExpect(status().isBadRequest());

        List<ChanDoanHinhAnh> chanDoanHinhAnhList = chanDoanHinhAnhRepository.findAll();
        assertThat(chanDoanHinhAnhList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhs() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList
        restChanDoanHinhAnhMockMvc.perform(get("/api/chan-doan-hinh-anhs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(chanDoanHinhAnh.getId().intValue())))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].dichVuYeuCau").value(hasItem(DEFAULT_DICH_VU_YEU_CAU.booleanValue())))
            .andExpect(jsonPath("$.[*].doppler").value(hasItem(DEFAULT_DOPPLER.booleanValue())))
            .andExpect(jsonPath("$.[*].donGiaBenhVien").value(hasItem(DEFAULT_DON_GIA_BENH_VIEN.intValue())))
            .andExpect(jsonPath("$.[*].enabled").value(hasItem(DEFAULT_ENABLED.booleanValue())))
            .andExpect(jsonPath("$.[*].goiHanChiDinh").value(hasItem(DEFAULT_GOI_HAN_CHI_DINH.intValue())))
            .andExpect(jsonPath("$.[*].phamViChiDinh").value(hasItem(DEFAULT_PHAM_VI_CHI_DINH.booleanValue())))
            .andExpect(jsonPath("$.[*].phanTheoGioiTinh").value(hasItem(DEFAULT_PHAN_THEO_GIOI_TINH)))
            .andExpect(jsonPath("$.[*].sapXep").value(hasItem(DEFAULT_SAP_XEP)))
            .andExpect(jsonPath("$.[*].soLanThucHien").value(hasItem(DEFAULT_SO_LAN_THUC_HIEN)))
            .andExpect(jsonPath("$.[*].soLuongFilm").value(hasItem(DEFAULT_SO_LUONG_FILM.intValue())))
            .andExpect(jsonPath("$.[*].soLuongQuiDoi").value(hasItem(DEFAULT_SO_LUONG_QUI_DOI.intValue())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].tenHienThi").value(hasItem(DEFAULT_TEN_HIEN_THI)))
            .andExpect(jsonPath("$.[*].maNoiBo").value(hasItem(DEFAULT_MA_NOI_BO)))
            .andExpect(jsonPath("$.[*].maDungChung").value(hasItem(DEFAULT_MA_DUNG_CHUNG)));
    }
    
    @Test
    @Transactional
    public void getChanDoanHinhAnh() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get the chanDoanHinhAnh
        restChanDoanHinhAnhMockMvc.perform(get("/api/chan-doan-hinh-anhs/{id}", chanDoanHinhAnh.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(chanDoanHinhAnh.getId().intValue()))
            .andExpect(jsonPath("$.deleted").value(DEFAULT_DELETED.booleanValue()))
            .andExpect(jsonPath("$.dichVuYeuCau").value(DEFAULT_DICH_VU_YEU_CAU.booleanValue()))
            .andExpect(jsonPath("$.doppler").value(DEFAULT_DOPPLER.booleanValue()))
            .andExpect(jsonPath("$.donGiaBenhVien").value(DEFAULT_DON_GIA_BENH_VIEN.intValue()))
            .andExpect(jsonPath("$.enabled").value(DEFAULT_ENABLED.booleanValue()))
            .andExpect(jsonPath("$.goiHanChiDinh").value(DEFAULT_GOI_HAN_CHI_DINH.intValue()))
            .andExpect(jsonPath("$.phamViChiDinh").value(DEFAULT_PHAM_VI_CHI_DINH.booleanValue()))
            .andExpect(jsonPath("$.phanTheoGioiTinh").value(DEFAULT_PHAN_THEO_GIOI_TINH))
            .andExpect(jsonPath("$.sapXep").value(DEFAULT_SAP_XEP))
            .andExpect(jsonPath("$.soLanThucHien").value(DEFAULT_SO_LAN_THUC_HIEN))
            .andExpect(jsonPath("$.soLuongFilm").value(DEFAULT_SO_LUONG_FILM.intValue()))
            .andExpect(jsonPath("$.soLuongQuiDoi").value(DEFAULT_SO_LUONG_QUI_DOI.intValue()))
            .andExpect(jsonPath("$.ten").value(DEFAULT_TEN))
            .andExpect(jsonPath("$.tenHienThi").value(DEFAULT_TEN_HIEN_THI))
            .andExpect(jsonPath("$.maNoiBo").value(DEFAULT_MA_NOI_BO))
            .andExpect(jsonPath("$.maDungChung").value(DEFAULT_MA_DUNG_CHUNG));
    }


    @Test
    @Transactional
    public void getChanDoanHinhAnhsByIdFiltering() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        Long id = chanDoanHinhAnh.getId();

        defaultChanDoanHinhAnhShouldBeFound("id.equals=" + id);
        defaultChanDoanHinhAnhShouldNotBeFound("id.notEquals=" + id);

        defaultChanDoanHinhAnhShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultChanDoanHinhAnhShouldNotBeFound("id.greaterThan=" + id);

        defaultChanDoanHinhAnhShouldBeFound("id.lessThanOrEqual=" + id);
        defaultChanDoanHinhAnhShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByDeletedIsEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where deleted equals to DEFAULT_DELETED
        defaultChanDoanHinhAnhShouldBeFound("deleted.equals=" + DEFAULT_DELETED);

        // Get all the chanDoanHinhAnhList where deleted equals to UPDATED_DELETED
        defaultChanDoanHinhAnhShouldNotBeFound("deleted.equals=" + UPDATED_DELETED);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByDeletedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where deleted not equals to DEFAULT_DELETED
        defaultChanDoanHinhAnhShouldNotBeFound("deleted.notEquals=" + DEFAULT_DELETED);

        // Get all the chanDoanHinhAnhList where deleted not equals to UPDATED_DELETED
        defaultChanDoanHinhAnhShouldBeFound("deleted.notEquals=" + UPDATED_DELETED);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByDeletedIsInShouldWork() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where deleted in DEFAULT_DELETED or UPDATED_DELETED
        defaultChanDoanHinhAnhShouldBeFound("deleted.in=" + DEFAULT_DELETED + "," + UPDATED_DELETED);

        // Get all the chanDoanHinhAnhList where deleted equals to UPDATED_DELETED
        defaultChanDoanHinhAnhShouldNotBeFound("deleted.in=" + UPDATED_DELETED);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByDeletedIsNullOrNotNull() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where deleted is not null
        defaultChanDoanHinhAnhShouldBeFound("deleted.specified=true");

        // Get all the chanDoanHinhAnhList where deleted is null
        defaultChanDoanHinhAnhShouldNotBeFound("deleted.specified=false");
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByDichVuYeuCauIsEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where dichVuYeuCau equals to DEFAULT_DICH_VU_YEU_CAU
        defaultChanDoanHinhAnhShouldBeFound("dichVuYeuCau.equals=" + DEFAULT_DICH_VU_YEU_CAU);

        // Get all the chanDoanHinhAnhList where dichVuYeuCau equals to UPDATED_DICH_VU_YEU_CAU
        defaultChanDoanHinhAnhShouldNotBeFound("dichVuYeuCau.equals=" + UPDATED_DICH_VU_YEU_CAU);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByDichVuYeuCauIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where dichVuYeuCau not equals to DEFAULT_DICH_VU_YEU_CAU
        defaultChanDoanHinhAnhShouldNotBeFound("dichVuYeuCau.notEquals=" + DEFAULT_DICH_VU_YEU_CAU);

        // Get all the chanDoanHinhAnhList where dichVuYeuCau not equals to UPDATED_DICH_VU_YEU_CAU
        defaultChanDoanHinhAnhShouldBeFound("dichVuYeuCau.notEquals=" + UPDATED_DICH_VU_YEU_CAU);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByDichVuYeuCauIsInShouldWork() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where dichVuYeuCau in DEFAULT_DICH_VU_YEU_CAU or UPDATED_DICH_VU_YEU_CAU
        defaultChanDoanHinhAnhShouldBeFound("dichVuYeuCau.in=" + DEFAULT_DICH_VU_YEU_CAU + "," + UPDATED_DICH_VU_YEU_CAU);

        // Get all the chanDoanHinhAnhList where dichVuYeuCau equals to UPDATED_DICH_VU_YEU_CAU
        defaultChanDoanHinhAnhShouldNotBeFound("dichVuYeuCau.in=" + UPDATED_DICH_VU_YEU_CAU);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByDichVuYeuCauIsNullOrNotNull() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where dichVuYeuCau is not null
        defaultChanDoanHinhAnhShouldBeFound("dichVuYeuCau.specified=true");

        // Get all the chanDoanHinhAnhList where dichVuYeuCau is null
        defaultChanDoanHinhAnhShouldNotBeFound("dichVuYeuCau.specified=false");
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByDopplerIsEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where doppler equals to DEFAULT_DOPPLER
        defaultChanDoanHinhAnhShouldBeFound("doppler.equals=" + DEFAULT_DOPPLER);

        // Get all the chanDoanHinhAnhList where doppler equals to UPDATED_DOPPLER
        defaultChanDoanHinhAnhShouldNotBeFound("doppler.equals=" + UPDATED_DOPPLER);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByDopplerIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where doppler not equals to DEFAULT_DOPPLER
        defaultChanDoanHinhAnhShouldNotBeFound("doppler.notEquals=" + DEFAULT_DOPPLER);

        // Get all the chanDoanHinhAnhList where doppler not equals to UPDATED_DOPPLER
        defaultChanDoanHinhAnhShouldBeFound("doppler.notEquals=" + UPDATED_DOPPLER);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByDopplerIsInShouldWork() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where doppler in DEFAULT_DOPPLER or UPDATED_DOPPLER
        defaultChanDoanHinhAnhShouldBeFound("doppler.in=" + DEFAULT_DOPPLER + "," + UPDATED_DOPPLER);

        // Get all the chanDoanHinhAnhList where doppler equals to UPDATED_DOPPLER
        defaultChanDoanHinhAnhShouldNotBeFound("doppler.in=" + UPDATED_DOPPLER);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByDopplerIsNullOrNotNull() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where doppler is not null
        defaultChanDoanHinhAnhShouldBeFound("doppler.specified=true");

        // Get all the chanDoanHinhAnhList where doppler is null
        defaultChanDoanHinhAnhShouldNotBeFound("doppler.specified=false");
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByDonGiaBenhVienIsEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where donGiaBenhVien equals to DEFAULT_DON_GIA_BENH_VIEN
        defaultChanDoanHinhAnhShouldBeFound("donGiaBenhVien.equals=" + DEFAULT_DON_GIA_BENH_VIEN);

        // Get all the chanDoanHinhAnhList where donGiaBenhVien equals to UPDATED_DON_GIA_BENH_VIEN
        defaultChanDoanHinhAnhShouldNotBeFound("donGiaBenhVien.equals=" + UPDATED_DON_GIA_BENH_VIEN);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByDonGiaBenhVienIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where donGiaBenhVien not equals to DEFAULT_DON_GIA_BENH_VIEN
        defaultChanDoanHinhAnhShouldNotBeFound("donGiaBenhVien.notEquals=" + DEFAULT_DON_GIA_BENH_VIEN);

        // Get all the chanDoanHinhAnhList where donGiaBenhVien not equals to UPDATED_DON_GIA_BENH_VIEN
        defaultChanDoanHinhAnhShouldBeFound("donGiaBenhVien.notEquals=" + UPDATED_DON_GIA_BENH_VIEN);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByDonGiaBenhVienIsInShouldWork() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where donGiaBenhVien in DEFAULT_DON_GIA_BENH_VIEN or UPDATED_DON_GIA_BENH_VIEN
        defaultChanDoanHinhAnhShouldBeFound("donGiaBenhVien.in=" + DEFAULT_DON_GIA_BENH_VIEN + "," + UPDATED_DON_GIA_BENH_VIEN);

        // Get all the chanDoanHinhAnhList where donGiaBenhVien equals to UPDATED_DON_GIA_BENH_VIEN
        defaultChanDoanHinhAnhShouldNotBeFound("donGiaBenhVien.in=" + UPDATED_DON_GIA_BENH_VIEN);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByDonGiaBenhVienIsNullOrNotNull() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where donGiaBenhVien is not null
        defaultChanDoanHinhAnhShouldBeFound("donGiaBenhVien.specified=true");

        // Get all the chanDoanHinhAnhList where donGiaBenhVien is null
        defaultChanDoanHinhAnhShouldNotBeFound("donGiaBenhVien.specified=false");
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByDonGiaBenhVienIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where donGiaBenhVien is greater than or equal to DEFAULT_DON_GIA_BENH_VIEN
        defaultChanDoanHinhAnhShouldBeFound("donGiaBenhVien.greaterThanOrEqual=" + DEFAULT_DON_GIA_BENH_VIEN);

        // Get all the chanDoanHinhAnhList where donGiaBenhVien is greater than or equal to UPDATED_DON_GIA_BENH_VIEN
        defaultChanDoanHinhAnhShouldNotBeFound("donGiaBenhVien.greaterThanOrEqual=" + UPDATED_DON_GIA_BENH_VIEN);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByDonGiaBenhVienIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where donGiaBenhVien is less than or equal to DEFAULT_DON_GIA_BENH_VIEN
        defaultChanDoanHinhAnhShouldBeFound("donGiaBenhVien.lessThanOrEqual=" + DEFAULT_DON_GIA_BENH_VIEN);

        // Get all the chanDoanHinhAnhList where donGiaBenhVien is less than or equal to SMALLER_DON_GIA_BENH_VIEN
        defaultChanDoanHinhAnhShouldNotBeFound("donGiaBenhVien.lessThanOrEqual=" + SMALLER_DON_GIA_BENH_VIEN);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByDonGiaBenhVienIsLessThanSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where donGiaBenhVien is less than DEFAULT_DON_GIA_BENH_VIEN
        defaultChanDoanHinhAnhShouldNotBeFound("donGiaBenhVien.lessThan=" + DEFAULT_DON_GIA_BENH_VIEN);

        // Get all the chanDoanHinhAnhList where donGiaBenhVien is less than UPDATED_DON_GIA_BENH_VIEN
        defaultChanDoanHinhAnhShouldBeFound("donGiaBenhVien.lessThan=" + UPDATED_DON_GIA_BENH_VIEN);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByDonGiaBenhVienIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where donGiaBenhVien is greater than DEFAULT_DON_GIA_BENH_VIEN
        defaultChanDoanHinhAnhShouldNotBeFound("donGiaBenhVien.greaterThan=" + DEFAULT_DON_GIA_BENH_VIEN);

        // Get all the chanDoanHinhAnhList where donGiaBenhVien is greater than SMALLER_DON_GIA_BENH_VIEN
        defaultChanDoanHinhAnhShouldBeFound("donGiaBenhVien.greaterThan=" + SMALLER_DON_GIA_BENH_VIEN);
    }


    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByEnabledIsEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where enabled equals to DEFAULT_ENABLED
        defaultChanDoanHinhAnhShouldBeFound("enabled.equals=" + DEFAULT_ENABLED);

        // Get all the chanDoanHinhAnhList where enabled equals to UPDATED_ENABLED
        defaultChanDoanHinhAnhShouldNotBeFound("enabled.equals=" + UPDATED_ENABLED);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByEnabledIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where enabled not equals to DEFAULT_ENABLED
        defaultChanDoanHinhAnhShouldNotBeFound("enabled.notEquals=" + DEFAULT_ENABLED);

        // Get all the chanDoanHinhAnhList where enabled not equals to UPDATED_ENABLED
        defaultChanDoanHinhAnhShouldBeFound("enabled.notEquals=" + UPDATED_ENABLED);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByEnabledIsInShouldWork() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where enabled in DEFAULT_ENABLED or UPDATED_ENABLED
        defaultChanDoanHinhAnhShouldBeFound("enabled.in=" + DEFAULT_ENABLED + "," + UPDATED_ENABLED);

        // Get all the chanDoanHinhAnhList where enabled equals to UPDATED_ENABLED
        defaultChanDoanHinhAnhShouldNotBeFound("enabled.in=" + UPDATED_ENABLED);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByEnabledIsNullOrNotNull() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where enabled is not null
        defaultChanDoanHinhAnhShouldBeFound("enabled.specified=true");

        // Get all the chanDoanHinhAnhList where enabled is null
        defaultChanDoanHinhAnhShouldNotBeFound("enabled.specified=false");
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByGoiHanChiDinhIsEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where goiHanChiDinh equals to DEFAULT_GOI_HAN_CHI_DINH
        defaultChanDoanHinhAnhShouldBeFound("goiHanChiDinh.equals=" + DEFAULT_GOI_HAN_CHI_DINH);

        // Get all the chanDoanHinhAnhList where goiHanChiDinh equals to UPDATED_GOI_HAN_CHI_DINH
        defaultChanDoanHinhAnhShouldNotBeFound("goiHanChiDinh.equals=" + UPDATED_GOI_HAN_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByGoiHanChiDinhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where goiHanChiDinh not equals to DEFAULT_GOI_HAN_CHI_DINH
        defaultChanDoanHinhAnhShouldNotBeFound("goiHanChiDinh.notEquals=" + DEFAULT_GOI_HAN_CHI_DINH);

        // Get all the chanDoanHinhAnhList where goiHanChiDinh not equals to UPDATED_GOI_HAN_CHI_DINH
        defaultChanDoanHinhAnhShouldBeFound("goiHanChiDinh.notEquals=" + UPDATED_GOI_HAN_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByGoiHanChiDinhIsInShouldWork() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where goiHanChiDinh in DEFAULT_GOI_HAN_CHI_DINH or UPDATED_GOI_HAN_CHI_DINH
        defaultChanDoanHinhAnhShouldBeFound("goiHanChiDinh.in=" + DEFAULT_GOI_HAN_CHI_DINH + "," + UPDATED_GOI_HAN_CHI_DINH);

        // Get all the chanDoanHinhAnhList where goiHanChiDinh equals to UPDATED_GOI_HAN_CHI_DINH
        defaultChanDoanHinhAnhShouldNotBeFound("goiHanChiDinh.in=" + UPDATED_GOI_HAN_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByGoiHanChiDinhIsNullOrNotNull() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where goiHanChiDinh is not null
        defaultChanDoanHinhAnhShouldBeFound("goiHanChiDinh.specified=true");

        // Get all the chanDoanHinhAnhList where goiHanChiDinh is null
        defaultChanDoanHinhAnhShouldNotBeFound("goiHanChiDinh.specified=false");
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByGoiHanChiDinhIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where goiHanChiDinh is greater than or equal to DEFAULT_GOI_HAN_CHI_DINH
        defaultChanDoanHinhAnhShouldBeFound("goiHanChiDinh.greaterThanOrEqual=" + DEFAULT_GOI_HAN_CHI_DINH);

        // Get all the chanDoanHinhAnhList where goiHanChiDinh is greater than or equal to UPDATED_GOI_HAN_CHI_DINH
        defaultChanDoanHinhAnhShouldNotBeFound("goiHanChiDinh.greaterThanOrEqual=" + UPDATED_GOI_HAN_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByGoiHanChiDinhIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where goiHanChiDinh is less than or equal to DEFAULT_GOI_HAN_CHI_DINH
        defaultChanDoanHinhAnhShouldBeFound("goiHanChiDinh.lessThanOrEqual=" + DEFAULT_GOI_HAN_CHI_DINH);

        // Get all the chanDoanHinhAnhList where goiHanChiDinh is less than or equal to SMALLER_GOI_HAN_CHI_DINH
        defaultChanDoanHinhAnhShouldNotBeFound("goiHanChiDinh.lessThanOrEqual=" + SMALLER_GOI_HAN_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByGoiHanChiDinhIsLessThanSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where goiHanChiDinh is less than DEFAULT_GOI_HAN_CHI_DINH
        defaultChanDoanHinhAnhShouldNotBeFound("goiHanChiDinh.lessThan=" + DEFAULT_GOI_HAN_CHI_DINH);

        // Get all the chanDoanHinhAnhList where goiHanChiDinh is less than UPDATED_GOI_HAN_CHI_DINH
        defaultChanDoanHinhAnhShouldBeFound("goiHanChiDinh.lessThan=" + UPDATED_GOI_HAN_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByGoiHanChiDinhIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where goiHanChiDinh is greater than DEFAULT_GOI_HAN_CHI_DINH
        defaultChanDoanHinhAnhShouldNotBeFound("goiHanChiDinh.greaterThan=" + DEFAULT_GOI_HAN_CHI_DINH);

        // Get all the chanDoanHinhAnhList where goiHanChiDinh is greater than SMALLER_GOI_HAN_CHI_DINH
        defaultChanDoanHinhAnhShouldBeFound("goiHanChiDinh.greaterThan=" + SMALLER_GOI_HAN_CHI_DINH);
    }


    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByPhamViChiDinhIsEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where phamViChiDinh equals to DEFAULT_PHAM_VI_CHI_DINH
        defaultChanDoanHinhAnhShouldBeFound("phamViChiDinh.equals=" + DEFAULT_PHAM_VI_CHI_DINH);

        // Get all the chanDoanHinhAnhList where phamViChiDinh equals to UPDATED_PHAM_VI_CHI_DINH
        defaultChanDoanHinhAnhShouldNotBeFound("phamViChiDinh.equals=" + UPDATED_PHAM_VI_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByPhamViChiDinhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where phamViChiDinh not equals to DEFAULT_PHAM_VI_CHI_DINH
        defaultChanDoanHinhAnhShouldNotBeFound("phamViChiDinh.notEquals=" + DEFAULT_PHAM_VI_CHI_DINH);

        // Get all the chanDoanHinhAnhList where phamViChiDinh not equals to UPDATED_PHAM_VI_CHI_DINH
        defaultChanDoanHinhAnhShouldBeFound("phamViChiDinh.notEquals=" + UPDATED_PHAM_VI_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByPhamViChiDinhIsInShouldWork() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where phamViChiDinh in DEFAULT_PHAM_VI_CHI_DINH or UPDATED_PHAM_VI_CHI_DINH
        defaultChanDoanHinhAnhShouldBeFound("phamViChiDinh.in=" + DEFAULT_PHAM_VI_CHI_DINH + "," + UPDATED_PHAM_VI_CHI_DINH);

        // Get all the chanDoanHinhAnhList where phamViChiDinh equals to UPDATED_PHAM_VI_CHI_DINH
        defaultChanDoanHinhAnhShouldNotBeFound("phamViChiDinh.in=" + UPDATED_PHAM_VI_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByPhamViChiDinhIsNullOrNotNull() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where phamViChiDinh is not null
        defaultChanDoanHinhAnhShouldBeFound("phamViChiDinh.specified=true");

        // Get all the chanDoanHinhAnhList where phamViChiDinh is null
        defaultChanDoanHinhAnhShouldNotBeFound("phamViChiDinh.specified=false");
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByPhanTheoGioiTinhIsEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where phanTheoGioiTinh equals to DEFAULT_PHAN_THEO_GIOI_TINH
        defaultChanDoanHinhAnhShouldBeFound("phanTheoGioiTinh.equals=" + DEFAULT_PHAN_THEO_GIOI_TINH);

        // Get all the chanDoanHinhAnhList where phanTheoGioiTinh equals to UPDATED_PHAN_THEO_GIOI_TINH
        defaultChanDoanHinhAnhShouldNotBeFound("phanTheoGioiTinh.equals=" + UPDATED_PHAN_THEO_GIOI_TINH);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByPhanTheoGioiTinhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where phanTheoGioiTinh not equals to DEFAULT_PHAN_THEO_GIOI_TINH
        defaultChanDoanHinhAnhShouldNotBeFound("phanTheoGioiTinh.notEquals=" + DEFAULT_PHAN_THEO_GIOI_TINH);

        // Get all the chanDoanHinhAnhList where phanTheoGioiTinh not equals to UPDATED_PHAN_THEO_GIOI_TINH
        defaultChanDoanHinhAnhShouldBeFound("phanTheoGioiTinh.notEquals=" + UPDATED_PHAN_THEO_GIOI_TINH);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByPhanTheoGioiTinhIsInShouldWork() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where phanTheoGioiTinh in DEFAULT_PHAN_THEO_GIOI_TINH or UPDATED_PHAN_THEO_GIOI_TINH
        defaultChanDoanHinhAnhShouldBeFound("phanTheoGioiTinh.in=" + DEFAULT_PHAN_THEO_GIOI_TINH + "," + UPDATED_PHAN_THEO_GIOI_TINH);

        // Get all the chanDoanHinhAnhList where phanTheoGioiTinh equals to UPDATED_PHAN_THEO_GIOI_TINH
        defaultChanDoanHinhAnhShouldNotBeFound("phanTheoGioiTinh.in=" + UPDATED_PHAN_THEO_GIOI_TINH);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByPhanTheoGioiTinhIsNullOrNotNull() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where phanTheoGioiTinh is not null
        defaultChanDoanHinhAnhShouldBeFound("phanTheoGioiTinh.specified=true");

        // Get all the chanDoanHinhAnhList where phanTheoGioiTinh is null
        defaultChanDoanHinhAnhShouldNotBeFound("phanTheoGioiTinh.specified=false");
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByPhanTheoGioiTinhIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where phanTheoGioiTinh is greater than or equal to DEFAULT_PHAN_THEO_GIOI_TINH
        defaultChanDoanHinhAnhShouldBeFound("phanTheoGioiTinh.greaterThanOrEqual=" + DEFAULT_PHAN_THEO_GIOI_TINH);

        // Get all the chanDoanHinhAnhList where phanTheoGioiTinh is greater than or equal to UPDATED_PHAN_THEO_GIOI_TINH
        defaultChanDoanHinhAnhShouldNotBeFound("phanTheoGioiTinh.greaterThanOrEqual=" + UPDATED_PHAN_THEO_GIOI_TINH);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByPhanTheoGioiTinhIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where phanTheoGioiTinh is less than or equal to DEFAULT_PHAN_THEO_GIOI_TINH
        defaultChanDoanHinhAnhShouldBeFound("phanTheoGioiTinh.lessThanOrEqual=" + DEFAULT_PHAN_THEO_GIOI_TINH);

        // Get all the chanDoanHinhAnhList where phanTheoGioiTinh is less than or equal to SMALLER_PHAN_THEO_GIOI_TINH
        defaultChanDoanHinhAnhShouldNotBeFound("phanTheoGioiTinh.lessThanOrEqual=" + SMALLER_PHAN_THEO_GIOI_TINH);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByPhanTheoGioiTinhIsLessThanSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where phanTheoGioiTinh is less than DEFAULT_PHAN_THEO_GIOI_TINH
        defaultChanDoanHinhAnhShouldNotBeFound("phanTheoGioiTinh.lessThan=" + DEFAULT_PHAN_THEO_GIOI_TINH);

        // Get all the chanDoanHinhAnhList where phanTheoGioiTinh is less than UPDATED_PHAN_THEO_GIOI_TINH
        defaultChanDoanHinhAnhShouldBeFound("phanTheoGioiTinh.lessThan=" + UPDATED_PHAN_THEO_GIOI_TINH);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByPhanTheoGioiTinhIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where phanTheoGioiTinh is greater than DEFAULT_PHAN_THEO_GIOI_TINH
        defaultChanDoanHinhAnhShouldNotBeFound("phanTheoGioiTinh.greaterThan=" + DEFAULT_PHAN_THEO_GIOI_TINH);

        // Get all the chanDoanHinhAnhList where phanTheoGioiTinh is greater than SMALLER_PHAN_THEO_GIOI_TINH
        defaultChanDoanHinhAnhShouldBeFound("phanTheoGioiTinh.greaterThan=" + SMALLER_PHAN_THEO_GIOI_TINH);
    }


    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsBySapXepIsEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where sapXep equals to DEFAULT_SAP_XEP
        defaultChanDoanHinhAnhShouldBeFound("sapXep.equals=" + DEFAULT_SAP_XEP);

        // Get all the chanDoanHinhAnhList where sapXep equals to UPDATED_SAP_XEP
        defaultChanDoanHinhAnhShouldNotBeFound("sapXep.equals=" + UPDATED_SAP_XEP);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsBySapXepIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where sapXep not equals to DEFAULT_SAP_XEP
        defaultChanDoanHinhAnhShouldNotBeFound("sapXep.notEquals=" + DEFAULT_SAP_XEP);

        // Get all the chanDoanHinhAnhList where sapXep not equals to UPDATED_SAP_XEP
        defaultChanDoanHinhAnhShouldBeFound("sapXep.notEquals=" + UPDATED_SAP_XEP);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsBySapXepIsInShouldWork() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where sapXep in DEFAULT_SAP_XEP or UPDATED_SAP_XEP
        defaultChanDoanHinhAnhShouldBeFound("sapXep.in=" + DEFAULT_SAP_XEP + "," + UPDATED_SAP_XEP);

        // Get all the chanDoanHinhAnhList where sapXep equals to UPDATED_SAP_XEP
        defaultChanDoanHinhAnhShouldNotBeFound("sapXep.in=" + UPDATED_SAP_XEP);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsBySapXepIsNullOrNotNull() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where sapXep is not null
        defaultChanDoanHinhAnhShouldBeFound("sapXep.specified=true");

        // Get all the chanDoanHinhAnhList where sapXep is null
        defaultChanDoanHinhAnhShouldNotBeFound("sapXep.specified=false");
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsBySapXepIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where sapXep is greater than or equal to DEFAULT_SAP_XEP
        defaultChanDoanHinhAnhShouldBeFound("sapXep.greaterThanOrEqual=" + DEFAULT_SAP_XEP);

        // Get all the chanDoanHinhAnhList where sapXep is greater than or equal to UPDATED_SAP_XEP
        defaultChanDoanHinhAnhShouldNotBeFound("sapXep.greaterThanOrEqual=" + UPDATED_SAP_XEP);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsBySapXepIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where sapXep is less than or equal to DEFAULT_SAP_XEP
        defaultChanDoanHinhAnhShouldBeFound("sapXep.lessThanOrEqual=" + DEFAULT_SAP_XEP);

        // Get all the chanDoanHinhAnhList where sapXep is less than or equal to SMALLER_SAP_XEP
        defaultChanDoanHinhAnhShouldNotBeFound("sapXep.lessThanOrEqual=" + SMALLER_SAP_XEP);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsBySapXepIsLessThanSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where sapXep is less than DEFAULT_SAP_XEP
        defaultChanDoanHinhAnhShouldNotBeFound("sapXep.lessThan=" + DEFAULT_SAP_XEP);

        // Get all the chanDoanHinhAnhList where sapXep is less than UPDATED_SAP_XEP
        defaultChanDoanHinhAnhShouldBeFound("sapXep.lessThan=" + UPDATED_SAP_XEP);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsBySapXepIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where sapXep is greater than DEFAULT_SAP_XEP
        defaultChanDoanHinhAnhShouldNotBeFound("sapXep.greaterThan=" + DEFAULT_SAP_XEP);

        // Get all the chanDoanHinhAnhList where sapXep is greater than SMALLER_SAP_XEP
        defaultChanDoanHinhAnhShouldBeFound("sapXep.greaterThan=" + SMALLER_SAP_XEP);
    }


    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsBySoLanThucHienIsEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where soLanThucHien equals to DEFAULT_SO_LAN_THUC_HIEN
        defaultChanDoanHinhAnhShouldBeFound("soLanThucHien.equals=" + DEFAULT_SO_LAN_THUC_HIEN);

        // Get all the chanDoanHinhAnhList where soLanThucHien equals to UPDATED_SO_LAN_THUC_HIEN
        defaultChanDoanHinhAnhShouldNotBeFound("soLanThucHien.equals=" + UPDATED_SO_LAN_THUC_HIEN);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsBySoLanThucHienIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where soLanThucHien not equals to DEFAULT_SO_LAN_THUC_HIEN
        defaultChanDoanHinhAnhShouldNotBeFound("soLanThucHien.notEquals=" + DEFAULT_SO_LAN_THUC_HIEN);

        // Get all the chanDoanHinhAnhList where soLanThucHien not equals to UPDATED_SO_LAN_THUC_HIEN
        defaultChanDoanHinhAnhShouldBeFound("soLanThucHien.notEquals=" + UPDATED_SO_LAN_THUC_HIEN);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsBySoLanThucHienIsInShouldWork() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where soLanThucHien in DEFAULT_SO_LAN_THUC_HIEN or UPDATED_SO_LAN_THUC_HIEN
        defaultChanDoanHinhAnhShouldBeFound("soLanThucHien.in=" + DEFAULT_SO_LAN_THUC_HIEN + "," + UPDATED_SO_LAN_THUC_HIEN);

        // Get all the chanDoanHinhAnhList where soLanThucHien equals to UPDATED_SO_LAN_THUC_HIEN
        defaultChanDoanHinhAnhShouldNotBeFound("soLanThucHien.in=" + UPDATED_SO_LAN_THUC_HIEN);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsBySoLanThucHienIsNullOrNotNull() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where soLanThucHien is not null
        defaultChanDoanHinhAnhShouldBeFound("soLanThucHien.specified=true");

        // Get all the chanDoanHinhAnhList where soLanThucHien is null
        defaultChanDoanHinhAnhShouldNotBeFound("soLanThucHien.specified=false");
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsBySoLanThucHienIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where soLanThucHien is greater than or equal to DEFAULT_SO_LAN_THUC_HIEN
        defaultChanDoanHinhAnhShouldBeFound("soLanThucHien.greaterThanOrEqual=" + DEFAULT_SO_LAN_THUC_HIEN);

        // Get all the chanDoanHinhAnhList where soLanThucHien is greater than or equal to UPDATED_SO_LAN_THUC_HIEN
        defaultChanDoanHinhAnhShouldNotBeFound("soLanThucHien.greaterThanOrEqual=" + UPDATED_SO_LAN_THUC_HIEN);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsBySoLanThucHienIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where soLanThucHien is less than or equal to DEFAULT_SO_LAN_THUC_HIEN
        defaultChanDoanHinhAnhShouldBeFound("soLanThucHien.lessThanOrEqual=" + DEFAULT_SO_LAN_THUC_HIEN);

        // Get all the chanDoanHinhAnhList where soLanThucHien is less than or equal to SMALLER_SO_LAN_THUC_HIEN
        defaultChanDoanHinhAnhShouldNotBeFound("soLanThucHien.lessThanOrEqual=" + SMALLER_SO_LAN_THUC_HIEN);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsBySoLanThucHienIsLessThanSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where soLanThucHien is less than DEFAULT_SO_LAN_THUC_HIEN
        defaultChanDoanHinhAnhShouldNotBeFound("soLanThucHien.lessThan=" + DEFAULT_SO_LAN_THUC_HIEN);

        // Get all the chanDoanHinhAnhList where soLanThucHien is less than UPDATED_SO_LAN_THUC_HIEN
        defaultChanDoanHinhAnhShouldBeFound("soLanThucHien.lessThan=" + UPDATED_SO_LAN_THUC_HIEN);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsBySoLanThucHienIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where soLanThucHien is greater than DEFAULT_SO_LAN_THUC_HIEN
        defaultChanDoanHinhAnhShouldNotBeFound("soLanThucHien.greaterThan=" + DEFAULT_SO_LAN_THUC_HIEN);

        // Get all the chanDoanHinhAnhList where soLanThucHien is greater than SMALLER_SO_LAN_THUC_HIEN
        defaultChanDoanHinhAnhShouldBeFound("soLanThucHien.greaterThan=" + SMALLER_SO_LAN_THUC_HIEN);
    }


    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsBySoLuongFilmIsEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where soLuongFilm equals to DEFAULT_SO_LUONG_FILM
        defaultChanDoanHinhAnhShouldBeFound("soLuongFilm.equals=" + DEFAULT_SO_LUONG_FILM);

        // Get all the chanDoanHinhAnhList where soLuongFilm equals to UPDATED_SO_LUONG_FILM
        defaultChanDoanHinhAnhShouldNotBeFound("soLuongFilm.equals=" + UPDATED_SO_LUONG_FILM);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsBySoLuongFilmIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where soLuongFilm not equals to DEFAULT_SO_LUONG_FILM
        defaultChanDoanHinhAnhShouldNotBeFound("soLuongFilm.notEquals=" + DEFAULT_SO_LUONG_FILM);

        // Get all the chanDoanHinhAnhList where soLuongFilm not equals to UPDATED_SO_LUONG_FILM
        defaultChanDoanHinhAnhShouldBeFound("soLuongFilm.notEquals=" + UPDATED_SO_LUONG_FILM);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsBySoLuongFilmIsInShouldWork() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where soLuongFilm in DEFAULT_SO_LUONG_FILM or UPDATED_SO_LUONG_FILM
        defaultChanDoanHinhAnhShouldBeFound("soLuongFilm.in=" + DEFAULT_SO_LUONG_FILM + "," + UPDATED_SO_LUONG_FILM);

        // Get all the chanDoanHinhAnhList where soLuongFilm equals to UPDATED_SO_LUONG_FILM
        defaultChanDoanHinhAnhShouldNotBeFound("soLuongFilm.in=" + UPDATED_SO_LUONG_FILM);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsBySoLuongFilmIsNullOrNotNull() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where soLuongFilm is not null
        defaultChanDoanHinhAnhShouldBeFound("soLuongFilm.specified=true");

        // Get all the chanDoanHinhAnhList where soLuongFilm is null
        defaultChanDoanHinhAnhShouldNotBeFound("soLuongFilm.specified=false");
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsBySoLuongFilmIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where soLuongFilm is greater than or equal to DEFAULT_SO_LUONG_FILM
        defaultChanDoanHinhAnhShouldBeFound("soLuongFilm.greaterThanOrEqual=" + DEFAULT_SO_LUONG_FILM);

        // Get all the chanDoanHinhAnhList where soLuongFilm is greater than or equal to UPDATED_SO_LUONG_FILM
        defaultChanDoanHinhAnhShouldNotBeFound("soLuongFilm.greaterThanOrEqual=" + UPDATED_SO_LUONG_FILM);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsBySoLuongFilmIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where soLuongFilm is less than or equal to DEFAULT_SO_LUONG_FILM
        defaultChanDoanHinhAnhShouldBeFound("soLuongFilm.lessThanOrEqual=" + DEFAULT_SO_LUONG_FILM);

        // Get all the chanDoanHinhAnhList where soLuongFilm is less than or equal to SMALLER_SO_LUONG_FILM
        defaultChanDoanHinhAnhShouldNotBeFound("soLuongFilm.lessThanOrEqual=" + SMALLER_SO_LUONG_FILM);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsBySoLuongFilmIsLessThanSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where soLuongFilm is less than DEFAULT_SO_LUONG_FILM
        defaultChanDoanHinhAnhShouldNotBeFound("soLuongFilm.lessThan=" + DEFAULT_SO_LUONG_FILM);

        // Get all the chanDoanHinhAnhList where soLuongFilm is less than UPDATED_SO_LUONG_FILM
        defaultChanDoanHinhAnhShouldBeFound("soLuongFilm.lessThan=" + UPDATED_SO_LUONG_FILM);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsBySoLuongFilmIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where soLuongFilm is greater than DEFAULT_SO_LUONG_FILM
        defaultChanDoanHinhAnhShouldNotBeFound("soLuongFilm.greaterThan=" + DEFAULT_SO_LUONG_FILM);

        // Get all the chanDoanHinhAnhList where soLuongFilm is greater than SMALLER_SO_LUONG_FILM
        defaultChanDoanHinhAnhShouldBeFound("soLuongFilm.greaterThan=" + SMALLER_SO_LUONG_FILM);
    }


    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsBySoLuongQuiDoiIsEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where soLuongQuiDoi equals to DEFAULT_SO_LUONG_QUI_DOI
        defaultChanDoanHinhAnhShouldBeFound("soLuongQuiDoi.equals=" + DEFAULT_SO_LUONG_QUI_DOI);

        // Get all the chanDoanHinhAnhList where soLuongQuiDoi equals to UPDATED_SO_LUONG_QUI_DOI
        defaultChanDoanHinhAnhShouldNotBeFound("soLuongQuiDoi.equals=" + UPDATED_SO_LUONG_QUI_DOI);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsBySoLuongQuiDoiIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where soLuongQuiDoi not equals to DEFAULT_SO_LUONG_QUI_DOI
        defaultChanDoanHinhAnhShouldNotBeFound("soLuongQuiDoi.notEquals=" + DEFAULT_SO_LUONG_QUI_DOI);

        // Get all the chanDoanHinhAnhList where soLuongQuiDoi not equals to UPDATED_SO_LUONG_QUI_DOI
        defaultChanDoanHinhAnhShouldBeFound("soLuongQuiDoi.notEquals=" + UPDATED_SO_LUONG_QUI_DOI);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsBySoLuongQuiDoiIsInShouldWork() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where soLuongQuiDoi in DEFAULT_SO_LUONG_QUI_DOI or UPDATED_SO_LUONG_QUI_DOI
        defaultChanDoanHinhAnhShouldBeFound("soLuongQuiDoi.in=" + DEFAULT_SO_LUONG_QUI_DOI + "," + UPDATED_SO_LUONG_QUI_DOI);

        // Get all the chanDoanHinhAnhList where soLuongQuiDoi equals to UPDATED_SO_LUONG_QUI_DOI
        defaultChanDoanHinhAnhShouldNotBeFound("soLuongQuiDoi.in=" + UPDATED_SO_LUONG_QUI_DOI);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsBySoLuongQuiDoiIsNullOrNotNull() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where soLuongQuiDoi is not null
        defaultChanDoanHinhAnhShouldBeFound("soLuongQuiDoi.specified=true");

        // Get all the chanDoanHinhAnhList where soLuongQuiDoi is null
        defaultChanDoanHinhAnhShouldNotBeFound("soLuongQuiDoi.specified=false");
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsBySoLuongQuiDoiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where soLuongQuiDoi is greater than or equal to DEFAULT_SO_LUONG_QUI_DOI
        defaultChanDoanHinhAnhShouldBeFound("soLuongQuiDoi.greaterThanOrEqual=" + DEFAULT_SO_LUONG_QUI_DOI);

        // Get all the chanDoanHinhAnhList where soLuongQuiDoi is greater than or equal to UPDATED_SO_LUONG_QUI_DOI
        defaultChanDoanHinhAnhShouldNotBeFound("soLuongQuiDoi.greaterThanOrEqual=" + UPDATED_SO_LUONG_QUI_DOI);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsBySoLuongQuiDoiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where soLuongQuiDoi is less than or equal to DEFAULT_SO_LUONG_QUI_DOI
        defaultChanDoanHinhAnhShouldBeFound("soLuongQuiDoi.lessThanOrEqual=" + DEFAULT_SO_LUONG_QUI_DOI);

        // Get all the chanDoanHinhAnhList where soLuongQuiDoi is less than or equal to SMALLER_SO_LUONG_QUI_DOI
        defaultChanDoanHinhAnhShouldNotBeFound("soLuongQuiDoi.lessThanOrEqual=" + SMALLER_SO_LUONG_QUI_DOI);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsBySoLuongQuiDoiIsLessThanSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where soLuongQuiDoi is less than DEFAULT_SO_LUONG_QUI_DOI
        defaultChanDoanHinhAnhShouldNotBeFound("soLuongQuiDoi.lessThan=" + DEFAULT_SO_LUONG_QUI_DOI);

        // Get all the chanDoanHinhAnhList where soLuongQuiDoi is less than UPDATED_SO_LUONG_QUI_DOI
        defaultChanDoanHinhAnhShouldBeFound("soLuongQuiDoi.lessThan=" + UPDATED_SO_LUONG_QUI_DOI);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsBySoLuongQuiDoiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where soLuongQuiDoi is greater than DEFAULT_SO_LUONG_QUI_DOI
        defaultChanDoanHinhAnhShouldNotBeFound("soLuongQuiDoi.greaterThan=" + DEFAULT_SO_LUONG_QUI_DOI);

        // Get all the chanDoanHinhAnhList where soLuongQuiDoi is greater than SMALLER_SO_LUONG_QUI_DOI
        defaultChanDoanHinhAnhShouldBeFound("soLuongQuiDoi.greaterThan=" + SMALLER_SO_LUONG_QUI_DOI);
    }


    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByTenIsEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where ten equals to DEFAULT_TEN
        defaultChanDoanHinhAnhShouldBeFound("ten.equals=" + DEFAULT_TEN);

        // Get all the chanDoanHinhAnhList where ten equals to UPDATED_TEN
        defaultChanDoanHinhAnhShouldNotBeFound("ten.equals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByTenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where ten not equals to DEFAULT_TEN
        defaultChanDoanHinhAnhShouldNotBeFound("ten.notEquals=" + DEFAULT_TEN);

        // Get all the chanDoanHinhAnhList where ten not equals to UPDATED_TEN
        defaultChanDoanHinhAnhShouldBeFound("ten.notEquals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByTenIsInShouldWork() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where ten in DEFAULT_TEN or UPDATED_TEN
        defaultChanDoanHinhAnhShouldBeFound("ten.in=" + DEFAULT_TEN + "," + UPDATED_TEN);

        // Get all the chanDoanHinhAnhList where ten equals to UPDATED_TEN
        defaultChanDoanHinhAnhShouldNotBeFound("ten.in=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByTenIsNullOrNotNull() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where ten is not null
        defaultChanDoanHinhAnhShouldBeFound("ten.specified=true");

        // Get all the chanDoanHinhAnhList where ten is null
        defaultChanDoanHinhAnhShouldNotBeFound("ten.specified=false");
    }
                @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByTenContainsSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where ten contains DEFAULT_TEN
        defaultChanDoanHinhAnhShouldBeFound("ten.contains=" + DEFAULT_TEN);

        // Get all the chanDoanHinhAnhList where ten contains UPDATED_TEN
        defaultChanDoanHinhAnhShouldNotBeFound("ten.contains=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByTenNotContainsSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where ten does not contain DEFAULT_TEN
        defaultChanDoanHinhAnhShouldNotBeFound("ten.doesNotContain=" + DEFAULT_TEN);

        // Get all the chanDoanHinhAnhList where ten does not contain UPDATED_TEN
        defaultChanDoanHinhAnhShouldBeFound("ten.doesNotContain=" + UPDATED_TEN);
    }


    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByTenHienThiIsEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where tenHienThi equals to DEFAULT_TEN_HIEN_THI
        defaultChanDoanHinhAnhShouldBeFound("tenHienThi.equals=" + DEFAULT_TEN_HIEN_THI);

        // Get all the chanDoanHinhAnhList where tenHienThi equals to UPDATED_TEN_HIEN_THI
        defaultChanDoanHinhAnhShouldNotBeFound("tenHienThi.equals=" + UPDATED_TEN_HIEN_THI);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByTenHienThiIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where tenHienThi not equals to DEFAULT_TEN_HIEN_THI
        defaultChanDoanHinhAnhShouldNotBeFound("tenHienThi.notEquals=" + DEFAULT_TEN_HIEN_THI);

        // Get all the chanDoanHinhAnhList where tenHienThi not equals to UPDATED_TEN_HIEN_THI
        defaultChanDoanHinhAnhShouldBeFound("tenHienThi.notEquals=" + UPDATED_TEN_HIEN_THI);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByTenHienThiIsInShouldWork() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where tenHienThi in DEFAULT_TEN_HIEN_THI or UPDATED_TEN_HIEN_THI
        defaultChanDoanHinhAnhShouldBeFound("tenHienThi.in=" + DEFAULT_TEN_HIEN_THI + "," + UPDATED_TEN_HIEN_THI);

        // Get all the chanDoanHinhAnhList where tenHienThi equals to UPDATED_TEN_HIEN_THI
        defaultChanDoanHinhAnhShouldNotBeFound("tenHienThi.in=" + UPDATED_TEN_HIEN_THI);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByTenHienThiIsNullOrNotNull() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where tenHienThi is not null
        defaultChanDoanHinhAnhShouldBeFound("tenHienThi.specified=true");

        // Get all the chanDoanHinhAnhList where tenHienThi is null
        defaultChanDoanHinhAnhShouldNotBeFound("tenHienThi.specified=false");
    }
                @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByTenHienThiContainsSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where tenHienThi contains DEFAULT_TEN_HIEN_THI
        defaultChanDoanHinhAnhShouldBeFound("tenHienThi.contains=" + DEFAULT_TEN_HIEN_THI);

        // Get all the chanDoanHinhAnhList where tenHienThi contains UPDATED_TEN_HIEN_THI
        defaultChanDoanHinhAnhShouldNotBeFound("tenHienThi.contains=" + UPDATED_TEN_HIEN_THI);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByTenHienThiNotContainsSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where tenHienThi does not contain DEFAULT_TEN_HIEN_THI
        defaultChanDoanHinhAnhShouldNotBeFound("tenHienThi.doesNotContain=" + DEFAULT_TEN_HIEN_THI);

        // Get all the chanDoanHinhAnhList where tenHienThi does not contain UPDATED_TEN_HIEN_THI
        defaultChanDoanHinhAnhShouldBeFound("tenHienThi.doesNotContain=" + UPDATED_TEN_HIEN_THI);
    }


    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByMaNoiBoIsEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where maNoiBo equals to DEFAULT_MA_NOI_BO
        defaultChanDoanHinhAnhShouldBeFound("maNoiBo.equals=" + DEFAULT_MA_NOI_BO);

        // Get all the chanDoanHinhAnhList where maNoiBo equals to UPDATED_MA_NOI_BO
        defaultChanDoanHinhAnhShouldNotBeFound("maNoiBo.equals=" + UPDATED_MA_NOI_BO);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByMaNoiBoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where maNoiBo not equals to DEFAULT_MA_NOI_BO
        defaultChanDoanHinhAnhShouldNotBeFound("maNoiBo.notEquals=" + DEFAULT_MA_NOI_BO);

        // Get all the chanDoanHinhAnhList where maNoiBo not equals to UPDATED_MA_NOI_BO
        defaultChanDoanHinhAnhShouldBeFound("maNoiBo.notEquals=" + UPDATED_MA_NOI_BO);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByMaNoiBoIsInShouldWork() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where maNoiBo in DEFAULT_MA_NOI_BO or UPDATED_MA_NOI_BO
        defaultChanDoanHinhAnhShouldBeFound("maNoiBo.in=" + DEFAULT_MA_NOI_BO + "," + UPDATED_MA_NOI_BO);

        // Get all the chanDoanHinhAnhList where maNoiBo equals to UPDATED_MA_NOI_BO
        defaultChanDoanHinhAnhShouldNotBeFound("maNoiBo.in=" + UPDATED_MA_NOI_BO);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByMaNoiBoIsNullOrNotNull() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where maNoiBo is not null
        defaultChanDoanHinhAnhShouldBeFound("maNoiBo.specified=true");

        // Get all the chanDoanHinhAnhList where maNoiBo is null
        defaultChanDoanHinhAnhShouldNotBeFound("maNoiBo.specified=false");
    }
                @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByMaNoiBoContainsSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where maNoiBo contains DEFAULT_MA_NOI_BO
        defaultChanDoanHinhAnhShouldBeFound("maNoiBo.contains=" + DEFAULT_MA_NOI_BO);

        // Get all the chanDoanHinhAnhList where maNoiBo contains UPDATED_MA_NOI_BO
        defaultChanDoanHinhAnhShouldNotBeFound("maNoiBo.contains=" + UPDATED_MA_NOI_BO);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByMaNoiBoNotContainsSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where maNoiBo does not contain DEFAULT_MA_NOI_BO
        defaultChanDoanHinhAnhShouldNotBeFound("maNoiBo.doesNotContain=" + DEFAULT_MA_NOI_BO);

        // Get all the chanDoanHinhAnhList where maNoiBo does not contain UPDATED_MA_NOI_BO
        defaultChanDoanHinhAnhShouldBeFound("maNoiBo.doesNotContain=" + UPDATED_MA_NOI_BO);
    }


    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByMaDungChungIsEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where maDungChung equals to DEFAULT_MA_DUNG_CHUNG
        defaultChanDoanHinhAnhShouldBeFound("maDungChung.equals=" + DEFAULT_MA_DUNG_CHUNG);

        // Get all the chanDoanHinhAnhList where maDungChung equals to UPDATED_MA_DUNG_CHUNG
        defaultChanDoanHinhAnhShouldNotBeFound("maDungChung.equals=" + UPDATED_MA_DUNG_CHUNG);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByMaDungChungIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where maDungChung not equals to DEFAULT_MA_DUNG_CHUNG
        defaultChanDoanHinhAnhShouldNotBeFound("maDungChung.notEquals=" + DEFAULT_MA_DUNG_CHUNG);

        // Get all the chanDoanHinhAnhList where maDungChung not equals to UPDATED_MA_DUNG_CHUNG
        defaultChanDoanHinhAnhShouldBeFound("maDungChung.notEquals=" + UPDATED_MA_DUNG_CHUNG);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByMaDungChungIsInShouldWork() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where maDungChung in DEFAULT_MA_DUNG_CHUNG or UPDATED_MA_DUNG_CHUNG
        defaultChanDoanHinhAnhShouldBeFound("maDungChung.in=" + DEFAULT_MA_DUNG_CHUNG + "," + UPDATED_MA_DUNG_CHUNG);

        // Get all the chanDoanHinhAnhList where maDungChung equals to UPDATED_MA_DUNG_CHUNG
        defaultChanDoanHinhAnhShouldNotBeFound("maDungChung.in=" + UPDATED_MA_DUNG_CHUNG);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByMaDungChungIsNullOrNotNull() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where maDungChung is not null
        defaultChanDoanHinhAnhShouldBeFound("maDungChung.specified=true");

        // Get all the chanDoanHinhAnhList where maDungChung is null
        defaultChanDoanHinhAnhShouldNotBeFound("maDungChung.specified=false");
    }
                @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByMaDungChungContainsSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where maDungChung contains DEFAULT_MA_DUNG_CHUNG
        defaultChanDoanHinhAnhShouldBeFound("maDungChung.contains=" + DEFAULT_MA_DUNG_CHUNG);

        // Get all the chanDoanHinhAnhList where maDungChung contains UPDATED_MA_DUNG_CHUNG
        defaultChanDoanHinhAnhShouldNotBeFound("maDungChung.contains=" + UPDATED_MA_DUNG_CHUNG);
    }

    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByMaDungChungNotContainsSomething() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        // Get all the chanDoanHinhAnhList where maDungChung does not contain DEFAULT_MA_DUNG_CHUNG
        defaultChanDoanHinhAnhShouldNotBeFound("maDungChung.doesNotContain=" + DEFAULT_MA_DUNG_CHUNG);

        // Get all the chanDoanHinhAnhList where maDungChung does not contain UPDATED_MA_DUNG_CHUNG
        defaultChanDoanHinhAnhShouldBeFound("maDungChung.doesNotContain=" + UPDATED_MA_DUNG_CHUNG);
    }


    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByDonViIsEqualToSomething() throws Exception {
        // Get already existing entity
        DonVi donVi = chanDoanHinhAnh.getDonVi();
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);
        Long donViId = donVi.getId();

        // Get all the chanDoanHinhAnhList where donVi equals to donViId
        defaultChanDoanHinhAnhShouldBeFound("donViId.equals=" + donViId);

        // Get all the chanDoanHinhAnhList where donVi equals to donViId + 1
        defaultChanDoanHinhAnhShouldNotBeFound("donViId.equals=" + (donViId + 1));
    }


    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByDotMaIsEqualToSomething() throws Exception {
        // Get already existing entity
        DotThayDoiMaDichVu dotMa = chanDoanHinhAnh.getDotMa();
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);
        Long dotMaId = dotMa.getId();

        // Get all the chanDoanHinhAnhList where dotMa equals to dotMaId
        defaultChanDoanHinhAnhShouldBeFound("dotMaId.equals=" + dotMaId);

        // Get all the chanDoanHinhAnhList where dotMa equals to dotMaId + 1
        defaultChanDoanHinhAnhShouldNotBeFound("dotMaId.equals=" + (dotMaId + 1));
    }


    @Test
    @Transactional
    public void getAllChanDoanHinhAnhsByLoaicdhaIsEqualToSomething() throws Exception {
        // Get already existing entity
        LoaiChanDoanHinhAnh loaicdha = chanDoanHinhAnh.getLoaicdha();
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);
        Long loaicdhaId = loaicdha.getId();

        // Get all the chanDoanHinhAnhList where loaicdha equals to loaicdhaId
        defaultChanDoanHinhAnhShouldBeFound("loaicdhaId.equals=" + loaicdhaId);

        // Get all the chanDoanHinhAnhList where loaicdha equals to loaicdhaId + 1
        defaultChanDoanHinhAnhShouldNotBeFound("loaicdhaId.equals=" + (loaicdhaId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultChanDoanHinhAnhShouldBeFound(String filter) throws Exception {
        restChanDoanHinhAnhMockMvc.perform(get("/api/chan-doan-hinh-anhs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(chanDoanHinhAnh.getId().intValue())))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].dichVuYeuCau").value(hasItem(DEFAULT_DICH_VU_YEU_CAU.booleanValue())))
            .andExpect(jsonPath("$.[*].doppler").value(hasItem(DEFAULT_DOPPLER.booleanValue())))
            .andExpect(jsonPath("$.[*].donGiaBenhVien").value(hasItem(DEFAULT_DON_GIA_BENH_VIEN.intValue())))
            .andExpect(jsonPath("$.[*].enabled").value(hasItem(DEFAULT_ENABLED.booleanValue())))
            .andExpect(jsonPath("$.[*].goiHanChiDinh").value(hasItem(DEFAULT_GOI_HAN_CHI_DINH.intValue())))
            .andExpect(jsonPath("$.[*].phamViChiDinh").value(hasItem(DEFAULT_PHAM_VI_CHI_DINH.booleanValue())))
            .andExpect(jsonPath("$.[*].phanTheoGioiTinh").value(hasItem(DEFAULT_PHAN_THEO_GIOI_TINH)))
            .andExpect(jsonPath("$.[*].sapXep").value(hasItem(DEFAULT_SAP_XEP)))
            .andExpect(jsonPath("$.[*].soLanThucHien").value(hasItem(DEFAULT_SO_LAN_THUC_HIEN)))
            .andExpect(jsonPath("$.[*].soLuongFilm").value(hasItem(DEFAULT_SO_LUONG_FILM.intValue())))
            .andExpect(jsonPath("$.[*].soLuongQuiDoi").value(hasItem(DEFAULT_SO_LUONG_QUI_DOI.intValue())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].tenHienThi").value(hasItem(DEFAULT_TEN_HIEN_THI)))
            .andExpect(jsonPath("$.[*].maNoiBo").value(hasItem(DEFAULT_MA_NOI_BO)))
            .andExpect(jsonPath("$.[*].maDungChung").value(hasItem(DEFAULT_MA_DUNG_CHUNG)));

        // Check, that the count call also returns 1
        restChanDoanHinhAnhMockMvc.perform(get("/api/chan-doan-hinh-anhs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultChanDoanHinhAnhShouldNotBeFound(String filter) throws Exception {
        restChanDoanHinhAnhMockMvc.perform(get("/api/chan-doan-hinh-anhs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restChanDoanHinhAnhMockMvc.perform(get("/api/chan-doan-hinh-anhs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingChanDoanHinhAnh() throws Exception {
        // Get the chanDoanHinhAnh
        restChanDoanHinhAnhMockMvc.perform(get("/api/chan-doan-hinh-anhs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateChanDoanHinhAnh() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        int databaseSizeBeforeUpdate = chanDoanHinhAnhRepository.findAll().size();

        // Update the chanDoanHinhAnh
        ChanDoanHinhAnh updatedChanDoanHinhAnh = chanDoanHinhAnhRepository.findById(chanDoanHinhAnh.getId()).get();
        // Disconnect from session so that the updates on updatedChanDoanHinhAnh are not directly saved in db
        em.detach(updatedChanDoanHinhAnh);
        updatedChanDoanHinhAnh
            .deleted(UPDATED_DELETED)
            .dichVuYeuCau(UPDATED_DICH_VU_YEU_CAU)
            .doppler(UPDATED_DOPPLER)
            .donGiaBenhVien(UPDATED_DON_GIA_BENH_VIEN)
            .enabled(UPDATED_ENABLED)
            .goiHanChiDinh(UPDATED_GOI_HAN_CHI_DINH)
            .phamViChiDinh(UPDATED_PHAM_VI_CHI_DINH)
            .phanTheoGioiTinh(UPDATED_PHAN_THEO_GIOI_TINH)
            .sapXep(UPDATED_SAP_XEP)
            .soLanThucHien(UPDATED_SO_LAN_THUC_HIEN)
            .soLuongFilm(UPDATED_SO_LUONG_FILM)
            .soLuongQuiDoi(UPDATED_SO_LUONG_QUI_DOI)
            .ten(UPDATED_TEN)
            .tenHienThi(UPDATED_TEN_HIEN_THI)
            .maNoiBo(UPDATED_MA_NOI_BO)
            .maDungChung(UPDATED_MA_DUNG_CHUNG);
        ChanDoanHinhAnhDTO chanDoanHinhAnhDTO = chanDoanHinhAnhMapper.toDto(updatedChanDoanHinhAnh);

        restChanDoanHinhAnhMockMvc.perform(put("/api/chan-doan-hinh-anhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chanDoanHinhAnhDTO)))
            .andExpect(status().isOk());

        // Validate the ChanDoanHinhAnh in the database
        List<ChanDoanHinhAnh> chanDoanHinhAnhList = chanDoanHinhAnhRepository.findAll();
        assertThat(chanDoanHinhAnhList).hasSize(databaseSizeBeforeUpdate);
        ChanDoanHinhAnh testChanDoanHinhAnh = chanDoanHinhAnhList.get(chanDoanHinhAnhList.size() - 1);
        assertThat(testChanDoanHinhAnh.isDeleted()).isEqualTo(UPDATED_DELETED);
        assertThat(testChanDoanHinhAnh.isDichVuYeuCau()).isEqualTo(UPDATED_DICH_VU_YEU_CAU);
        assertThat(testChanDoanHinhAnh.isDoppler()).isEqualTo(UPDATED_DOPPLER);
        assertThat(testChanDoanHinhAnh.getDonGiaBenhVien()).isEqualTo(UPDATED_DON_GIA_BENH_VIEN);
        assertThat(testChanDoanHinhAnh.isEnabled()).isEqualTo(UPDATED_ENABLED);
        assertThat(testChanDoanHinhAnh.getGoiHanChiDinh()).isEqualTo(UPDATED_GOI_HAN_CHI_DINH);
        assertThat(testChanDoanHinhAnh.isPhamViChiDinh()).isEqualTo(UPDATED_PHAM_VI_CHI_DINH);
        assertThat(testChanDoanHinhAnh.getPhanTheoGioiTinh()).isEqualTo(UPDATED_PHAN_THEO_GIOI_TINH);
        assertThat(testChanDoanHinhAnh.getSapXep()).isEqualTo(UPDATED_SAP_XEP);
        assertThat(testChanDoanHinhAnh.getSoLanThucHien()).isEqualTo(UPDATED_SO_LAN_THUC_HIEN);
        assertThat(testChanDoanHinhAnh.getSoLuongFilm()).isEqualTo(UPDATED_SO_LUONG_FILM);
        assertThat(testChanDoanHinhAnh.getSoLuongQuiDoi()).isEqualTo(UPDATED_SO_LUONG_QUI_DOI);
        assertThat(testChanDoanHinhAnh.getTen()).isEqualTo(UPDATED_TEN);
        assertThat(testChanDoanHinhAnh.getTenHienThi()).isEqualTo(UPDATED_TEN_HIEN_THI);
        assertThat(testChanDoanHinhAnh.getMaNoiBo()).isEqualTo(UPDATED_MA_NOI_BO);
        assertThat(testChanDoanHinhAnh.getMaDungChung()).isEqualTo(UPDATED_MA_DUNG_CHUNG);
    }

    @Test
    @Transactional
    public void updateNonExistingChanDoanHinhAnh() throws Exception {
        int databaseSizeBeforeUpdate = chanDoanHinhAnhRepository.findAll().size();

        // Create the ChanDoanHinhAnh
        ChanDoanHinhAnhDTO chanDoanHinhAnhDTO = chanDoanHinhAnhMapper.toDto(chanDoanHinhAnh);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restChanDoanHinhAnhMockMvc.perform(put("/api/chan-doan-hinh-anhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chanDoanHinhAnhDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ChanDoanHinhAnh in the database
        List<ChanDoanHinhAnh> chanDoanHinhAnhList = chanDoanHinhAnhRepository.findAll();
        assertThat(chanDoanHinhAnhList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteChanDoanHinhAnh() throws Exception {
        // Initialize the database
        chanDoanHinhAnhRepository.saveAndFlush(chanDoanHinhAnh);

        int databaseSizeBeforeDelete = chanDoanHinhAnhRepository.findAll().size();

        // Delete the chanDoanHinhAnh
        restChanDoanHinhAnhMockMvc.perform(delete("/api/chan-doan-hinh-anhs/{id}", chanDoanHinhAnh.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ChanDoanHinhAnh> chanDoanHinhAnhList = chanDoanHinhAnhRepository.findAll();
        assertThat(chanDoanHinhAnhList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

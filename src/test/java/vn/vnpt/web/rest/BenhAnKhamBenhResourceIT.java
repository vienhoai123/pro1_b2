package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.BenhAnKhamBenh;
import vn.vnpt.domain.BenhAnKhamBenhId;
import vn.vnpt.domain.BenhNhan;
import vn.vnpt.domain.DonVi;
import vn.vnpt.domain.NhanVien;
import vn.vnpt.domain.Phong;
import vn.vnpt.repository.BenhAnKhamBenhRepository;
import vn.vnpt.service.BenhAnKhamBenhService;
import vn.vnpt.service.dto.BenhAnKhamBenhDTO;
import vn.vnpt.service.mapper.BenhAnKhamBenhMapper;
import vn.vnpt.service.dto.BenhAnKhamBenhCriteria;
import vn.vnpt.service.BenhAnKhamBenhQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link BenhAnKhamBenhResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class BenhAnKhamBenhResourceIT {

    public static final Boolean DEFAULT_BENH_NHAN_CU = false;
    public static final Boolean UPDATED_BENH_NHAN_CU = true;

    public static final Boolean DEFAULT_CANH_BAO = false;
    public static final Boolean UPDATED_CANH_BAO = true;

    public static final Boolean DEFAULT_CAP_CUU = false;
    public static final Boolean UPDATED_CAP_CUU = true;

    public static final Boolean DEFAULT_CHI_CO_NAM_SINH = false;
    public static final Boolean UPDATED_CHI_CO_NAM_SINH = true;

    public static final String DEFAULT_CMND_BENH_NHAN = "AAAAAAAAAA";
    public static final String UPDATED_CMND_BENH_NHAN = "BBBBBBBBBB";

    public static final String DEFAULT_CMND_NGUOI_NHA = "AAAAAAAAAA";
    public static final String UPDATED_CMND_NGUOI_NHA = "BBBBBBBBBB";

    public static final Boolean DEFAULT_CO_BAO_HIEM = false;
    public static final Boolean UPDATED_CO_BAO_HIEM = true;

    public static final String DEFAULT_DIA_CHI = "AAAAAAAAAA";
    public static final String UPDATED_DIA_CHI = "BBBBBBBBBB";

    public static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    public static final String UPDATED_EMAIL = "BBBBBBBBBB";

    public static final Integer DEFAULT_GIOI_TINH = 1;
    public static final Integer UPDATED_GIOI_TINH = 2;
    public static final Integer SMALLER_GIOI_TINH = 1 - 1;

    public static final String DEFAULT_KHANG_THE = "AAAAA";
    public static final String UPDATED_KHANG_THE = "BBBBB";

    public static final Integer DEFAULT_LAN_KHAM_TRONG_NGAY = 1;
    public static final Integer UPDATED_LAN_KHAM_TRONG_NGAY = 2;
    public static final Integer SMALLER_LAN_KHAM_TRONG_NGAY = 1 - 1;

    public static final Integer DEFAULT_LOAI_GIAY_TO_TRE_EM = 1;
    public static final Integer UPDATED_LOAI_GIAY_TO_TRE_EM = 2;
    public static final Integer SMALLER_LOAI_GIAY_TO_TRE_EM = 1 - 1;

    public static final LocalDate DEFAULT_NGAY_CAP_CMND = LocalDate.ofEpochDay(0L);
    public static final LocalDate UPDATED_NGAY_CAP_CMND = LocalDate.now(ZoneId.systemDefault());
    public static final LocalDate SMALLER_NGAY_CAP_CMND = LocalDate.ofEpochDay(-1L);

    public static final LocalDate DEFAULT_NGAY_MIEN_CUNG_CHI_TRA = LocalDate.ofEpochDay(0L);
    public static final LocalDate UPDATED_NGAY_MIEN_CUNG_CHI_TRA = LocalDate.now(ZoneId.systemDefault());
    public static final LocalDate SMALLER_NGAY_MIEN_CUNG_CHI_TRA = LocalDate.ofEpochDay(-1L);

    public static final LocalDate DEFAULT_NGAY_SINH = LocalDate.ofEpochDay(0L);
    public static final LocalDate UPDATED_NGAY_SINH = LocalDate.now(ZoneId.systemDefault());
    public static final LocalDate SMALLER_NGAY_SINH = LocalDate.ofEpochDay(-1L);

    public static final String DEFAULT_NHOM_MAU = "AAAAA";
    public static final String UPDATED_NHOM_MAU = "BBBBB";

    public static final String DEFAULT_NOI_CAP_CMND = "AAAAAAAAAA";
    public static final String UPDATED_NOI_CAP_CMND = "BBBBBBBBBB";

    public static final String DEFAULT_NOI_LAM_VIEC = "AAAAAAAAAA";
    public static final String UPDATED_NOI_LAM_VIEC = "BBBBBBBBBB";

    public static final String DEFAULT_PHONE = "AAAAAAAAAA";
    public static final String UPDATED_PHONE = "BBBBBBBBBB";

    public static final String DEFAULT_PHONE_NGUOI_NHA = "AAAAAAAAAA";
    public static final String UPDATED_PHONE_NGUOI_NHA = "BBBBBBBBBB";

    public static final String DEFAULT_QUOC_TICH = "AAAAAAAAAA";
    public static final String UPDATED_QUOC_TICH = "BBBBBBBBBB";

    public static final String DEFAULT_TEN_BENH_NHAN = "AAAAAAAAAA";
    public static final String UPDATED_TEN_BENH_NHAN = "BBBBBBBBBB";

    public static final String DEFAULT_TEN_NGUOI_NHA = "AAAAAAAAAA";
    public static final String UPDATED_TEN_NGUOI_NHA = "BBBBBBBBBB";

    public static final Integer DEFAULT_THANG_TUOI = 1;
    public static final Integer UPDATED_THANG_TUOI = 2;
    public static final Integer SMALLER_THANG_TUOI = 1 - 1;

    public static final LocalDate DEFAULT_THOI_GIAN_TIEP_NHAN = LocalDate.ofEpochDay(0L);
    public static final LocalDate UPDATED_THOI_GIAN_TIEP_NHAN = LocalDate.now(ZoneId.systemDefault());
    public static final LocalDate SMALLER_THOI_GIAN_TIEP_NHAN = LocalDate.ofEpochDay(-1L);

    public static final Integer DEFAULT_TUOI = 1;
    public static final Integer UPDATED_TUOI = 2;
    public static final Integer SMALLER_TUOI = 1 - 1;

    public static final Integer DEFAULT_UU_TIEN = 1;
    public static final Integer UPDATED_UU_TIEN = 2;
    public static final Integer SMALLER_UU_TIEN = 1 - 1;

    public static final String DEFAULT_UUID = "AAAAAAAAAA";
    public static final String UPDATED_UUID = "BBBBBBBBBB";

    public static final Integer DEFAULT_TRANG_THAI = 1;
    public static final Integer UPDATED_TRANG_THAI = 2;
    public static final Integer SMALLER_TRANG_THAI = 1 - 1;

    public static final Integer DEFAULT_MA_KHOA_HOAN_TAT_KHAM = 1;
    public static final Integer UPDATED_MA_KHOA_HOAN_TAT_KHAM = 2;
    public static final Integer SMALLER_MA_KHOA_HOAN_TAT_KHAM = 1 - 1;

    public static final Integer DEFAULT_MA_PHONG_HOAN_TAT_KHAM = 1;
    public static final Integer UPDATED_MA_PHONG_HOAN_TAT_KHAM = 2;
    public static final Integer SMALLER_MA_PHONG_HOAN_TAT_KHAM = 1 - 1;

    public static final String DEFAULT_TEN_KHOA_HOAN_TAT_KHAM = "AAAAAAAAAA";
    public static final String UPDATED_TEN_KHOA_HOAN_TAT_KHAM = "BBBBBBBBBB";

    public static final String DEFAULT_TEN_PHONG_HOAN_TAT_KHAM = "AAAAAAAAAA";
    public static final String UPDATED_TEN_PHONG_HOAN_TAT_KHAM = "BBBBBBBBBB";

    public static final LocalDate DEFAULT_THOI_GIAN_HOAN_TAT_KHAM = LocalDate.ofEpochDay(0L);
    public static final LocalDate UPDATED_THOI_GIAN_HOAN_TAT_KHAM = LocalDate.now(ZoneId.systemDefault());
    public static final LocalDate SMALLER_THOI_GIAN_HOAN_TAT_KHAM = LocalDate.ofEpochDay(-1L);

    public static final String DEFAULT_SO_BENH_AN = "AAAAAAAAAA";
    public static final String UPDATED_SO_BENH_AN = "BBBBBBBBBB";

    public static final String DEFAULT_SO_BENH_AN_KHOA = "AAAAAAAAAA";
    public static final String UPDATED_SO_BENH_AN_KHOA = "BBBBBBBBBB";

    public static final Integer DEFAULT_NAM = 1;
    public static final Integer UPDATED_NAM = 2;
    public static final Integer SMALLER_NAM = 1 - 1;

    @Autowired
    private BenhAnKhamBenhRepository benhAnKhamBenhRepository;

    @Autowired
    private BenhAnKhamBenhMapper benhAnKhamBenhMapper;

    @Autowired
    private BenhAnKhamBenhService benhAnKhamBenhService;

    @Autowired
    private BenhAnKhamBenhQueryService benhAnKhamBenhQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restBenhAnKhamBenhMockMvc;

    private BenhAnKhamBenh benhAnKhamBenh;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BenhAnKhamBenh createEntity(EntityManager em) {
        BenhAnKhamBenh benhAnKhamBenh = new BenhAnKhamBenh()
            .benhNhanCu(DEFAULT_BENH_NHAN_CU)
            .canhBao(DEFAULT_CANH_BAO)
            .capCuu(DEFAULT_CAP_CUU)
            .chiCoNamSinh(DEFAULT_CHI_CO_NAM_SINH)
            .cmndBenhNhan(DEFAULT_CMND_BENH_NHAN)
            .cmndNguoiNha(DEFAULT_CMND_NGUOI_NHA)
            .coBaoHiem(DEFAULT_CO_BAO_HIEM)
            .diaChi(DEFAULT_DIA_CHI)
            .email(DEFAULT_EMAIL)
            .gioiTinh(DEFAULT_GIOI_TINH)
            .khangThe(DEFAULT_KHANG_THE)
            .lanKhamTrongNgay(DEFAULT_LAN_KHAM_TRONG_NGAY)
            .loaiGiayToTreEm(DEFAULT_LOAI_GIAY_TO_TRE_EM)
            .ngayCapCmnd(DEFAULT_NGAY_CAP_CMND)
            .ngayMienCungChiTra(DEFAULT_NGAY_MIEN_CUNG_CHI_TRA)
            .ngaySinh(DEFAULT_NGAY_SINH)
            .nhomMau(DEFAULT_NHOM_MAU)
            .noiCapCmnd(DEFAULT_NOI_CAP_CMND)
            .noiLamViec(DEFAULT_NOI_LAM_VIEC)
            .phone(DEFAULT_PHONE)
            .phoneNguoiNha(DEFAULT_PHONE_NGUOI_NHA)
            .quocTich(DEFAULT_QUOC_TICH)
            .tenBenhNhan(DEFAULT_TEN_BENH_NHAN)
            .tenNguoiNha(DEFAULT_TEN_NGUOI_NHA)
            .thangTuoi(DEFAULT_THANG_TUOI)
            .thoiGianTiepNhan(DEFAULT_THOI_GIAN_TIEP_NHAN)
            .tuoi(DEFAULT_TUOI)
            .uuTien(DEFAULT_UU_TIEN)
            .uuid(DEFAULT_UUID)
            .trangThai(DEFAULT_TRANG_THAI)
            .maKhoaHoanTatKham(DEFAULT_MA_KHOA_HOAN_TAT_KHAM)
            .maPhongHoanTatKham(DEFAULT_MA_PHONG_HOAN_TAT_KHAM)
            .tenKhoaHoanTatKham(DEFAULT_TEN_KHOA_HOAN_TAT_KHAM)
            .tenPhongHoanTatKham(DEFAULT_TEN_PHONG_HOAN_TAT_KHAM)
            .thoiGianHoanTatKham(DEFAULT_THOI_GIAN_HOAN_TAT_KHAM)
            .soBenhAn(DEFAULT_SO_BENH_AN)
            .soBenhAnKhoa(DEFAULT_SO_BENH_AN_KHOA)
            .nam(DEFAULT_NAM);
        // Add required entity
        BenhNhan newBenhNhan = BenhNhanResourceIT.createEntity(em);
        BenhNhan benhNhan = TestUtil.findAll(em, BenhNhan.class).stream()
            .filter(x -> x.getChiCoNamSinh().equals(newBenhNhan.getChiCoNamSinh()))
            .findAny().orElse(null);
        if (benhNhan == null) {
            benhNhan = newBenhNhan;
            em.persist(benhNhan);
            em.flush();
        }
        benhAnKhamBenh.setBenhNhan(benhNhan);
        // Add required entity
        DonVi newDonVi = DonViResourceIT.createEntity(em);
        DonVi donVi = TestUtil.findAll(em, DonVi.class).stream()
            .filter(x -> x.getCap().equals(newDonVi.getCap()))
            .findAny().orElse(null);
        if (donVi == null) {
            donVi = newDonVi;
            em.persist(donVi);
            em.flush();
        }
        benhAnKhamBenh.setDonVi(donVi);
        // Add required entity
        NhanVien newNhanVien = NhanVienResourceIT.createEntity(em);
        NhanVien nhanVien = TestUtil.findAll(em, NhanVien.class).stream()
            .filter(x -> x.getTen().equals(newNhanVien.getTen()))
            .findAny().orElse(null);
        if (nhanVien == null) {
            nhanVien = newNhanVien;
            em.persist(nhanVien);
            em.flush();
        }
        benhAnKhamBenh.setNhanVienTiepNhan(nhanVien);
        // Add required entity
        Phong newPhong = PhongResourceIT.createEntity(em);
        Phong phong = TestUtil.findAll(em, Phong.class).stream()
            .filter(x -> x.getEnabled().equals(newPhong.getEnabled()))
            .findAny().orElse(null);
        if (phong == null) {
            phong = newPhong;
            em.persist(phong);
            em.flush();
        }
        benhAnKhamBenh.setPhongTiepNhan(phong);
        benhAnKhamBenh.setId(new BenhAnKhamBenhId(benhNhan.getId(), donVi.getId()));
        return benhAnKhamBenh;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BenhAnKhamBenh createUpdatedEntity(EntityManager em) {
        BenhAnKhamBenh benhAnKhamBenh = new BenhAnKhamBenh()
            .benhNhanCu(UPDATED_BENH_NHAN_CU)
            .canhBao(UPDATED_CANH_BAO)
            .capCuu(UPDATED_CAP_CUU)
            .chiCoNamSinh(UPDATED_CHI_CO_NAM_SINH)
            .cmndBenhNhan(UPDATED_CMND_BENH_NHAN)
            .cmndNguoiNha(UPDATED_CMND_NGUOI_NHA)
            .coBaoHiem(UPDATED_CO_BAO_HIEM)
            .diaChi(UPDATED_DIA_CHI)
            .email(UPDATED_EMAIL)
            .gioiTinh(UPDATED_GIOI_TINH)
            .khangThe(UPDATED_KHANG_THE)
            .lanKhamTrongNgay(UPDATED_LAN_KHAM_TRONG_NGAY)
            .loaiGiayToTreEm(UPDATED_LOAI_GIAY_TO_TRE_EM)
            .ngayCapCmnd(UPDATED_NGAY_CAP_CMND)
            .ngayMienCungChiTra(UPDATED_NGAY_MIEN_CUNG_CHI_TRA)
            .ngaySinh(UPDATED_NGAY_SINH)
            .nhomMau(UPDATED_NHOM_MAU)
            .noiCapCmnd(UPDATED_NOI_CAP_CMND)
            .noiLamViec(UPDATED_NOI_LAM_VIEC)
            .phone(UPDATED_PHONE)
            .phoneNguoiNha(UPDATED_PHONE_NGUOI_NHA)
            .quocTich(UPDATED_QUOC_TICH)
            .tenBenhNhan(UPDATED_TEN_BENH_NHAN)
            .tenNguoiNha(UPDATED_TEN_NGUOI_NHA)
            .thangTuoi(UPDATED_THANG_TUOI)
            .thoiGianTiepNhan(UPDATED_THOI_GIAN_TIEP_NHAN)
            .tuoi(UPDATED_TUOI)
            .uuTien(UPDATED_UU_TIEN)
            .uuid(UPDATED_UUID)
            .trangThai(UPDATED_TRANG_THAI)
            .maKhoaHoanTatKham(UPDATED_MA_KHOA_HOAN_TAT_KHAM)
            .maPhongHoanTatKham(UPDATED_MA_PHONG_HOAN_TAT_KHAM)
            .tenKhoaHoanTatKham(UPDATED_TEN_KHOA_HOAN_TAT_KHAM)
            .tenPhongHoanTatKham(UPDATED_TEN_PHONG_HOAN_TAT_KHAM)
            .thoiGianHoanTatKham(UPDATED_THOI_GIAN_HOAN_TAT_KHAM)
            .soBenhAn(UPDATED_SO_BENH_AN)
            .soBenhAnKhoa(UPDATED_SO_BENH_AN_KHOA)
            .nam(UPDATED_NAM);
        // Add required entity
        BenhNhan newBenhNhan = BenhNhanResourceIT.createUpdatedEntity(em);
        BenhNhan benhNhan = TestUtil.findAll(em, BenhNhan.class).stream()
            .filter(x -> x.getChiCoNamSinh().equals(newBenhNhan.getChiCoNamSinh()))
            .findAny().orElse(null);
        if (benhNhan == null) {
            benhNhan = newBenhNhan;
            em.persist(benhNhan);
            em.flush();
        }
        benhAnKhamBenh.setBenhNhan(benhNhan);
        // Add required entity
        DonVi newDonVi = DonViResourceIT.createUpdatedEntity(em);
        DonVi donVi = TestUtil.findAll(em, DonVi.class).stream()
            .filter(x -> x.getCap().equals(newDonVi.getCap()))
            .findAny().orElse(null);
        if (donVi == null) {
            donVi = newDonVi;
            em.persist(donVi);
            em.flush();
        }
        benhAnKhamBenh.setDonVi(donVi);
        // Add required entity
        NhanVien newNhanVien = NhanVienResourceIT.createUpdatedEntity(em);
        NhanVien nhanVien = TestUtil.findAll(em, NhanVien.class).stream()
            .filter(x -> x.getTen().equals(newNhanVien.getTen()))
            .findAny().orElse(null);
        if (nhanVien == null) {
            nhanVien = newNhanVien;
            em.persist(nhanVien);
            em.flush();
        }
        benhAnKhamBenh.setNhanVienTiepNhan(nhanVien);
        // Add required entity
        Phong newPhong = PhongResourceIT.createUpdatedEntity(em);
        Phong phong = TestUtil.findAll(em, Phong.class).stream()
            .filter(x -> x.getEnabled().equals(newPhong.getEnabled()))
            .findAny().orElse(null);
        if (phong == null) {
            phong = newPhong;
            em.persist(phong);
            em.flush();
        }
        benhAnKhamBenh.setPhongTiepNhan(phong);
        benhAnKhamBenh.setId(new BenhAnKhamBenhId(benhNhan.getId(), donVi.getId()));
        return benhAnKhamBenh;
    }

    @BeforeEach
    public void initTest() {
        benhAnKhamBenh = createEntity(em);
    }

    @Test
    @Transactional
    public void createBenhAnKhamBenh() throws Exception {
        int databaseSizeBeforeCreate = benhAnKhamBenhRepository.findAll().size();

        // Create the BenhAnKhamBenh
        BenhAnKhamBenhDTO benhAnKhamBenhDTO = benhAnKhamBenhMapper.toDto(benhAnKhamBenh);
        restBenhAnKhamBenhMockMvc.perform(post("/api/benh-an-kham-benhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(benhAnKhamBenhDTO)))
            .andExpect(status().isCreated());

        // Validate the BenhAnKhamBenh in the database
        List<BenhAnKhamBenh> benhAnKhamBenhList = benhAnKhamBenhRepository.findAll();
        assertThat(benhAnKhamBenhList).hasSize(databaseSizeBeforeCreate + 1);
        BenhAnKhamBenh testBenhAnKhamBenh = benhAnKhamBenhList.get(benhAnKhamBenhList.size() - 1);
        assertThat(testBenhAnKhamBenh.isBenhNhanCu()).isEqualTo(DEFAULT_BENH_NHAN_CU);
        assertThat(testBenhAnKhamBenh.isCanhBao()).isEqualTo(DEFAULT_CANH_BAO);
        assertThat(testBenhAnKhamBenh.isCapCuu()).isEqualTo(DEFAULT_CAP_CUU);
        assertThat(testBenhAnKhamBenh.isChiCoNamSinh()).isEqualTo(DEFAULT_CHI_CO_NAM_SINH);
        assertThat(testBenhAnKhamBenh.getCmndBenhNhan()).isEqualTo(DEFAULT_CMND_BENH_NHAN);
        assertThat(testBenhAnKhamBenh.getCmndNguoiNha()).isEqualTo(DEFAULT_CMND_NGUOI_NHA);
        assertThat(testBenhAnKhamBenh.isCoBaoHiem()).isEqualTo(DEFAULT_CO_BAO_HIEM);
        assertThat(testBenhAnKhamBenh.getDiaChi()).isEqualTo(DEFAULT_DIA_CHI);
        assertThat(testBenhAnKhamBenh.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testBenhAnKhamBenh.getGioiTinh()).isEqualTo(DEFAULT_GIOI_TINH);
        assertThat(testBenhAnKhamBenh.getKhangThe()).isEqualTo(DEFAULT_KHANG_THE);
        assertThat(testBenhAnKhamBenh.getLanKhamTrongNgay()).isEqualTo(DEFAULT_LAN_KHAM_TRONG_NGAY);
        assertThat(testBenhAnKhamBenh.getLoaiGiayToTreEm()).isEqualTo(DEFAULT_LOAI_GIAY_TO_TRE_EM);
        assertThat(testBenhAnKhamBenh.getNgayCapCmnd()).isEqualTo(DEFAULT_NGAY_CAP_CMND);
        assertThat(testBenhAnKhamBenh.getNgayMienCungChiTra()).isEqualTo(DEFAULT_NGAY_MIEN_CUNG_CHI_TRA);
        assertThat(testBenhAnKhamBenh.getNgaySinh()).isEqualTo(DEFAULT_NGAY_SINH);
        assertThat(testBenhAnKhamBenh.getNhomMau()).isEqualTo(DEFAULT_NHOM_MAU);
        assertThat(testBenhAnKhamBenh.getNoiCapCmnd()).isEqualTo(DEFAULT_NOI_CAP_CMND);
        assertThat(testBenhAnKhamBenh.getNoiLamViec()).isEqualTo(DEFAULT_NOI_LAM_VIEC);
        assertThat(testBenhAnKhamBenh.getPhone()).isEqualTo(DEFAULT_PHONE);
        assertThat(testBenhAnKhamBenh.getPhoneNguoiNha()).isEqualTo(DEFAULT_PHONE_NGUOI_NHA);
        assertThat(testBenhAnKhamBenh.getQuocTich()).isEqualTo(DEFAULT_QUOC_TICH);
        assertThat(testBenhAnKhamBenh.getTenBenhNhan()).isEqualTo(DEFAULT_TEN_BENH_NHAN);
        assertThat(testBenhAnKhamBenh.getTenNguoiNha()).isEqualTo(DEFAULT_TEN_NGUOI_NHA);
        assertThat(testBenhAnKhamBenh.getThangTuoi()).isEqualTo(DEFAULT_THANG_TUOI);
        assertThat(testBenhAnKhamBenh.getThoiGianTiepNhan()).isEqualTo(DEFAULT_THOI_GIAN_TIEP_NHAN);
        assertThat(testBenhAnKhamBenh.getTuoi()).isEqualTo(DEFAULT_TUOI);
        assertThat(testBenhAnKhamBenh.getUuTien()).isEqualTo(DEFAULT_UU_TIEN);
        assertThat(testBenhAnKhamBenh.getUuid()).isEqualTo(DEFAULT_UUID);
        assertThat(testBenhAnKhamBenh.getTrangThai()).isEqualTo(DEFAULT_TRANG_THAI);
        assertThat(testBenhAnKhamBenh.getMaKhoaHoanTatKham()).isEqualTo(DEFAULT_MA_KHOA_HOAN_TAT_KHAM);
        assertThat(testBenhAnKhamBenh.getMaPhongHoanTatKham()).isEqualTo(DEFAULT_MA_PHONG_HOAN_TAT_KHAM);
        assertThat(testBenhAnKhamBenh.getTenKhoaHoanTatKham()).isEqualTo(DEFAULT_TEN_KHOA_HOAN_TAT_KHAM);
        assertThat(testBenhAnKhamBenh.getTenPhongHoanTatKham()).isEqualTo(DEFAULT_TEN_PHONG_HOAN_TAT_KHAM);
        assertThat(testBenhAnKhamBenh.getThoiGianHoanTatKham()).isEqualTo(DEFAULT_THOI_GIAN_HOAN_TAT_KHAM);
        assertThat(testBenhAnKhamBenh.getSoBenhAn()).isEqualTo(DEFAULT_SO_BENH_AN);
        assertThat(testBenhAnKhamBenh.getSoBenhAnKhoa()).isEqualTo(DEFAULT_SO_BENH_AN_KHOA);
        assertThat(testBenhAnKhamBenh.getNam()).isEqualTo(DEFAULT_NAM);
    }

    @Test
    @Transactional
    public void createBenhAnKhamBenhWithExistingId() throws Exception {
        benhAnKhamBenhRepository.save(benhAnKhamBenh);
        int databaseSizeBeforeCreate = benhAnKhamBenhRepository.findAll().size();

        // Create the BenhAnKhamBenh with an existing ID
        benhAnKhamBenh.setId(benhAnKhamBenh.getId());
        BenhAnKhamBenhDTO benhAnKhamBenhDTO = benhAnKhamBenhMapper.toDto(benhAnKhamBenh);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBenhAnKhamBenhMockMvc.perform(post("/api/benh-an-kham-benhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(benhAnKhamBenhDTO)))
            .andExpect(status().isBadRequest());

        // Validate the BenhAnKhamBenh in the database
        List<BenhAnKhamBenh> benhAnKhamBenhList = benhAnKhamBenhRepository.findAll();
        assertThat(benhAnKhamBenhList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkBenhNhanCuIsRequired() throws Exception {
        int databaseSizeBeforeTest = benhAnKhamBenhRepository.findAll().size();
        // set the field null
        benhAnKhamBenh.setBenhNhanCu(null);

        // Create the BenhAnKhamBenh, which fails.
        BenhAnKhamBenhDTO benhAnKhamBenhDTO = benhAnKhamBenhMapper.toDto(benhAnKhamBenh);

        restBenhAnKhamBenhMockMvc.perform(post("/api/benh-an-kham-benhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(benhAnKhamBenhDTO)))
            .andExpect(status().isBadRequest());

        List<BenhAnKhamBenh> benhAnKhamBenhList = benhAnKhamBenhRepository.findAll();
        assertThat(benhAnKhamBenhList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCanhBaoIsRequired() throws Exception {
        int databaseSizeBeforeTest = benhAnKhamBenhRepository.findAll().size();
        // set the field null
        benhAnKhamBenh.setCanhBao(null);

        // Create the BenhAnKhamBenh, which fails.
        BenhAnKhamBenhDTO benhAnKhamBenhDTO = benhAnKhamBenhMapper.toDto(benhAnKhamBenh);

        restBenhAnKhamBenhMockMvc.perform(post("/api/benh-an-kham-benhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(benhAnKhamBenhDTO)))
            .andExpect(status().isBadRequest());

        List<BenhAnKhamBenh> benhAnKhamBenhList = benhAnKhamBenhRepository.findAll();
        assertThat(benhAnKhamBenhList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCapCuuIsRequired() throws Exception {
        int databaseSizeBeforeTest = benhAnKhamBenhRepository.findAll().size();
        // set the field null
        benhAnKhamBenh.setCapCuu(null);

        // Create the BenhAnKhamBenh, which fails.
        BenhAnKhamBenhDTO benhAnKhamBenhDTO = benhAnKhamBenhMapper.toDto(benhAnKhamBenh);

        restBenhAnKhamBenhMockMvc.perform(post("/api/benh-an-kham-benhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(benhAnKhamBenhDTO)))
            .andExpect(status().isBadRequest());

        List<BenhAnKhamBenh> benhAnKhamBenhList = benhAnKhamBenhRepository.findAll();
        assertThat(benhAnKhamBenhList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkChiCoNamSinhIsRequired() throws Exception {
        int databaseSizeBeforeTest = benhAnKhamBenhRepository.findAll().size();
        // set the field null
        benhAnKhamBenh.setChiCoNamSinh(null);

        // Create the BenhAnKhamBenh, which fails.
        BenhAnKhamBenhDTO benhAnKhamBenhDTO = benhAnKhamBenhMapper.toDto(benhAnKhamBenh);

        restBenhAnKhamBenhMockMvc.perform(post("/api/benh-an-kham-benhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(benhAnKhamBenhDTO)))
            .andExpect(status().isBadRequest());

        List<BenhAnKhamBenh> benhAnKhamBenhList = benhAnKhamBenhRepository.findAll();
        assertThat(benhAnKhamBenhList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCoBaoHiemIsRequired() throws Exception {
        int databaseSizeBeforeTest = benhAnKhamBenhRepository.findAll().size();
        // set the field null
        benhAnKhamBenh.setCoBaoHiem(null);

        // Create the BenhAnKhamBenh, which fails.
        BenhAnKhamBenhDTO benhAnKhamBenhDTO = benhAnKhamBenhMapper.toDto(benhAnKhamBenh);

        restBenhAnKhamBenhMockMvc.perform(post("/api/benh-an-kham-benhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(benhAnKhamBenhDTO)))
            .andExpect(status().isBadRequest());

        List<BenhAnKhamBenh> benhAnKhamBenhList = benhAnKhamBenhRepository.findAll();
        assertThat(benhAnKhamBenhList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDiaChiIsRequired() throws Exception {
        int databaseSizeBeforeTest = benhAnKhamBenhRepository.findAll().size();
        // set the field null
        benhAnKhamBenh.setDiaChi(null);

        // Create the BenhAnKhamBenh, which fails.
        BenhAnKhamBenhDTO benhAnKhamBenhDTO = benhAnKhamBenhMapper.toDto(benhAnKhamBenh);

        restBenhAnKhamBenhMockMvc.perform(post("/api/benh-an-kham-benhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(benhAnKhamBenhDTO)))
            .andExpect(status().isBadRequest());

        List<BenhAnKhamBenh> benhAnKhamBenhList = benhAnKhamBenhRepository.findAll();
        assertThat(benhAnKhamBenhList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkGioiTinhIsRequired() throws Exception {
        int databaseSizeBeforeTest = benhAnKhamBenhRepository.findAll().size();
        // set the field null
        benhAnKhamBenh.setGioiTinh(null);

        // Create the BenhAnKhamBenh, which fails.
        BenhAnKhamBenhDTO benhAnKhamBenhDTO = benhAnKhamBenhMapper.toDto(benhAnKhamBenh);

        restBenhAnKhamBenhMockMvc.perform(post("/api/benh-an-kham-benhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(benhAnKhamBenhDTO)))
            .andExpect(status().isBadRequest());

        List<BenhAnKhamBenh> benhAnKhamBenhList = benhAnKhamBenhRepository.findAll();
        assertThat(benhAnKhamBenhList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNgaySinhIsRequired() throws Exception {
        int databaseSizeBeforeTest = benhAnKhamBenhRepository.findAll().size();
        // set the field null
        benhAnKhamBenh.setNgaySinh(null);

        // Create the BenhAnKhamBenh, which fails.
        BenhAnKhamBenhDTO benhAnKhamBenhDTO = benhAnKhamBenhMapper.toDto(benhAnKhamBenh);

        restBenhAnKhamBenhMockMvc.perform(post("/api/benh-an-kham-benhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(benhAnKhamBenhDTO)))
            .andExpect(status().isBadRequest());

        List<BenhAnKhamBenh> benhAnKhamBenhList = benhAnKhamBenhRepository.findAll();
        assertThat(benhAnKhamBenhList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTenBenhNhanIsRequired() throws Exception {
        int databaseSizeBeforeTest = benhAnKhamBenhRepository.findAll().size();
        // set the field null
        benhAnKhamBenh.setTenBenhNhan(null);

        // Create the BenhAnKhamBenh, which fails.
        BenhAnKhamBenhDTO benhAnKhamBenhDTO = benhAnKhamBenhMapper.toDto(benhAnKhamBenh);

        restBenhAnKhamBenhMockMvc.perform(post("/api/benh-an-kham-benhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(benhAnKhamBenhDTO)))
            .andExpect(status().isBadRequest());

        List<BenhAnKhamBenh> benhAnKhamBenhList = benhAnKhamBenhRepository.findAll();
        assertThat(benhAnKhamBenhList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkThoiGianTiepNhanIsRequired() throws Exception {
        int databaseSizeBeforeTest = benhAnKhamBenhRepository.findAll().size();
        // set the field null
        benhAnKhamBenh.setThoiGianTiepNhan(null);

        // Create the BenhAnKhamBenh, which fails.
        BenhAnKhamBenhDTO benhAnKhamBenhDTO = benhAnKhamBenhMapper.toDto(benhAnKhamBenh);

        restBenhAnKhamBenhMockMvc.perform(post("/api/benh-an-kham-benhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(benhAnKhamBenhDTO)))
            .andExpect(status().isBadRequest());

        List<BenhAnKhamBenh> benhAnKhamBenhList = benhAnKhamBenhRepository.findAll();
        assertThat(benhAnKhamBenhList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNamIsRequired() throws Exception {
        int databaseSizeBeforeTest = benhAnKhamBenhRepository.findAll().size();
        // set the field null
        benhAnKhamBenh.setNam(null);

        // Create the BenhAnKhamBenh, which fails.
        BenhAnKhamBenhDTO benhAnKhamBenhDTO = benhAnKhamBenhMapper.toDto(benhAnKhamBenh);

        restBenhAnKhamBenhMockMvc.perform(post("/api/benh-an-kham-benhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(benhAnKhamBenhDTO)))
            .andExpect(status().isBadRequest());

        List<BenhAnKhamBenh> benhAnKhamBenhList = benhAnKhamBenhRepository.findAll();
        assertThat(benhAnKhamBenhList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhs() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList
        restBenhAnKhamBenhMockMvc.perform(get("/api/benh-an-kham-benhs"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].benhNhanCu").value(hasItem(DEFAULT_BENH_NHAN_CU.booleanValue())))
            .andExpect(jsonPath("$.[*].canhBao").value(hasItem(DEFAULT_CANH_BAO.booleanValue())))
            .andExpect(jsonPath("$.[*].capCuu").value(hasItem(DEFAULT_CAP_CUU.booleanValue())))
            .andExpect(jsonPath("$.[*].chiCoNamSinh").value(hasItem(DEFAULT_CHI_CO_NAM_SINH.booleanValue())))
            .andExpect(jsonPath("$.[*].cmndBenhNhan").value(hasItem(DEFAULT_CMND_BENH_NHAN)))
            .andExpect(jsonPath("$.[*].cmndNguoiNha").value(hasItem(DEFAULT_CMND_NGUOI_NHA)))
            .andExpect(jsonPath("$.[*].coBaoHiem").value(hasItem(DEFAULT_CO_BAO_HIEM.booleanValue())))
            .andExpect(jsonPath("$.[*].diaChi").value(hasItem(DEFAULT_DIA_CHI)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].gioiTinh").value(hasItem(DEFAULT_GIOI_TINH)))
            .andExpect(jsonPath("$.[*].khangThe").value(hasItem(DEFAULT_KHANG_THE)))
            .andExpect(jsonPath("$.[*].lanKhamTrongNgay").value(hasItem(DEFAULT_LAN_KHAM_TRONG_NGAY)))
            .andExpect(jsonPath("$.[*].loaiGiayToTreEm").value(hasItem(DEFAULT_LOAI_GIAY_TO_TRE_EM)))
            .andExpect(jsonPath("$.[*].ngayCapCmnd").value(hasItem(DEFAULT_NGAY_CAP_CMND.toString())))
            .andExpect(jsonPath("$.[*].ngayMienCungChiTra").value(hasItem(DEFAULT_NGAY_MIEN_CUNG_CHI_TRA.toString())))
            .andExpect(jsonPath("$.[*].ngaySinh").value(hasItem(DEFAULT_NGAY_SINH.toString())))
            .andExpect(jsonPath("$.[*].nhomMau").value(hasItem(DEFAULT_NHOM_MAU)))
            .andExpect(jsonPath("$.[*].noiCapCmnd").value(hasItem(DEFAULT_NOI_CAP_CMND)))
            .andExpect(jsonPath("$.[*].noiLamViec").value(hasItem(DEFAULT_NOI_LAM_VIEC)))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE)))
            .andExpect(jsonPath("$.[*].phoneNguoiNha").value(hasItem(DEFAULT_PHONE_NGUOI_NHA)))
            .andExpect(jsonPath("$.[*].quocTich").value(hasItem(DEFAULT_QUOC_TICH)))
            .andExpect(jsonPath("$.[*].tenBenhNhan").value(hasItem(DEFAULT_TEN_BENH_NHAN)))
            .andExpect(jsonPath("$.[*].tenNguoiNha").value(hasItem(DEFAULT_TEN_NGUOI_NHA)))
            .andExpect(jsonPath("$.[*].thangTuoi").value(hasItem(DEFAULT_THANG_TUOI)))
            .andExpect(jsonPath("$.[*].thoiGianTiepNhan").value(hasItem(DEFAULT_THOI_GIAN_TIEP_NHAN.toString())))
            .andExpect(jsonPath("$.[*].tuoi").value(hasItem(DEFAULT_TUOI)))
            .andExpect(jsonPath("$.[*].uuTien").value(hasItem(DEFAULT_UU_TIEN)))
            .andExpect(jsonPath("$.[*].uuid").value(hasItem(DEFAULT_UUID)))
            .andExpect(jsonPath("$.[*].trangThai").value(hasItem(DEFAULT_TRANG_THAI)))
            .andExpect(jsonPath("$.[*].maKhoaHoanTatKham").value(hasItem(DEFAULT_MA_KHOA_HOAN_TAT_KHAM)))
            .andExpect(jsonPath("$.[*].maPhongHoanTatKham").value(hasItem(DEFAULT_MA_PHONG_HOAN_TAT_KHAM)))
            .andExpect(jsonPath("$.[*].tenKhoaHoanTatKham").value(hasItem(DEFAULT_TEN_KHOA_HOAN_TAT_KHAM)))
            .andExpect(jsonPath("$.[*].tenPhongHoanTatKham").value(hasItem(DEFAULT_TEN_PHONG_HOAN_TAT_KHAM)))
            .andExpect(jsonPath("$.[*].thoiGianHoanTatKham").value(hasItem(DEFAULT_THOI_GIAN_HOAN_TAT_KHAM.toString())))
            .andExpect(jsonPath("$.[*].soBenhAn").value(hasItem(DEFAULT_SO_BENH_AN)))
            .andExpect(jsonPath("$.[*].soBenhAnKhoa").value(hasItem(DEFAULT_SO_BENH_AN_KHOA)))
            .andExpect(jsonPath("$.[*].nam").value(hasItem(DEFAULT_NAM)));
    }

    @Test
    @Transactional
    public void getBenhAnKhamBenh() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get the benhAnKhamBenh
        restBenhAnKhamBenhMockMvc.perform(get("/api/benh-an-kham-benhs/{id}", "benhNhanId=" + benhAnKhamBenh.getId().getBenhNhanId() + ";" + "donViId=" + benhAnKhamBenh.getId().getDonViId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.benhNhanCu").value(DEFAULT_BENH_NHAN_CU.booleanValue()))
            .andExpect(jsonPath("$.canhBao").value(DEFAULT_CANH_BAO.booleanValue()))
            .andExpect(jsonPath("$.capCuu").value(DEFAULT_CAP_CUU.booleanValue()))
            .andExpect(jsonPath("$.chiCoNamSinh").value(DEFAULT_CHI_CO_NAM_SINH.booleanValue()))
            .andExpect(jsonPath("$.cmndBenhNhan").value(DEFAULT_CMND_BENH_NHAN))
            .andExpect(jsonPath("$.cmndNguoiNha").value(DEFAULT_CMND_NGUOI_NHA))
            .andExpect(jsonPath("$.coBaoHiem").value(DEFAULT_CO_BAO_HIEM.booleanValue()))
            .andExpect(jsonPath("$.diaChi").value(DEFAULT_DIA_CHI))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.gioiTinh").value(DEFAULT_GIOI_TINH))
            .andExpect(jsonPath("$.khangThe").value(DEFAULT_KHANG_THE))
            .andExpect(jsonPath("$.lanKhamTrongNgay").value(DEFAULT_LAN_KHAM_TRONG_NGAY))
            .andExpect(jsonPath("$.loaiGiayToTreEm").value(DEFAULT_LOAI_GIAY_TO_TRE_EM))
            .andExpect(jsonPath("$.ngayCapCmnd").value(DEFAULT_NGAY_CAP_CMND.toString()))
            .andExpect(jsonPath("$.ngayMienCungChiTra").value(DEFAULT_NGAY_MIEN_CUNG_CHI_TRA.toString()))
            .andExpect(jsonPath("$.ngaySinh").value(DEFAULT_NGAY_SINH.toString()))
            .andExpect(jsonPath("$.nhomMau").value(DEFAULT_NHOM_MAU))
            .andExpect(jsonPath("$.noiCapCmnd").value(DEFAULT_NOI_CAP_CMND))
            .andExpect(jsonPath("$.noiLamViec").value(DEFAULT_NOI_LAM_VIEC))
            .andExpect(jsonPath("$.phone").value(DEFAULT_PHONE))
            .andExpect(jsonPath("$.phoneNguoiNha").value(DEFAULT_PHONE_NGUOI_NHA))
            .andExpect(jsonPath("$.quocTich").value(DEFAULT_QUOC_TICH))
            .andExpect(jsonPath("$.tenBenhNhan").value(DEFAULT_TEN_BENH_NHAN))
            .andExpect(jsonPath("$.tenNguoiNha").value(DEFAULT_TEN_NGUOI_NHA))
            .andExpect(jsonPath("$.thangTuoi").value(DEFAULT_THANG_TUOI))
            .andExpect(jsonPath("$.thoiGianTiepNhan").value(DEFAULT_THOI_GIAN_TIEP_NHAN.toString()))
            .andExpect(jsonPath("$.tuoi").value(DEFAULT_TUOI))
            .andExpect(jsonPath("$.uuTien").value(DEFAULT_UU_TIEN))
            .andExpect(jsonPath("$.uuid").value(DEFAULT_UUID))
            .andExpect(jsonPath("$.trangThai").value(DEFAULT_TRANG_THAI))
            .andExpect(jsonPath("$.maKhoaHoanTatKham").value(DEFAULT_MA_KHOA_HOAN_TAT_KHAM))
            .andExpect(jsonPath("$.maPhongHoanTatKham").value(DEFAULT_MA_PHONG_HOAN_TAT_KHAM))
            .andExpect(jsonPath("$.tenKhoaHoanTatKham").value(DEFAULT_TEN_KHOA_HOAN_TAT_KHAM))
            .andExpect(jsonPath("$.tenPhongHoanTatKham").value(DEFAULT_TEN_PHONG_HOAN_TAT_KHAM))
            .andExpect(jsonPath("$.thoiGianHoanTatKham").value(DEFAULT_THOI_GIAN_HOAN_TAT_KHAM.toString()))
            .andExpect(jsonPath("$.soBenhAn").value(DEFAULT_SO_BENH_AN))
            .andExpect(jsonPath("$.soBenhAnKhoa").value(DEFAULT_SO_BENH_AN_KHOA))
            .andExpect(jsonPath("$.nam").value(DEFAULT_NAM));
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByBenhNhanCuIsEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where benhNhanCu equals to DEFAULT_BENH_NHAN_CU
        defaultBenhAnKhamBenhShouldBeFound("benhNhanCu.equals=" + DEFAULT_BENH_NHAN_CU);

        // Get all the benhAnKhamBenhList where benhNhanCu equals to UPDATED_BENH_NHAN_CU
        defaultBenhAnKhamBenhShouldNotBeFound("benhNhanCu.equals=" + UPDATED_BENH_NHAN_CU);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByBenhNhanCuIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where benhNhanCu not equals to DEFAULT_BENH_NHAN_CU
        defaultBenhAnKhamBenhShouldNotBeFound("benhNhanCu.notEquals=" + DEFAULT_BENH_NHAN_CU);

        // Get all the benhAnKhamBenhList where benhNhanCu not equals to UPDATED_BENH_NHAN_CU
        defaultBenhAnKhamBenhShouldBeFound("benhNhanCu.notEquals=" + UPDATED_BENH_NHAN_CU);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByBenhNhanCuIsInShouldWork() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where benhNhanCu in DEFAULT_BENH_NHAN_CU or UPDATED_BENH_NHAN_CU
        defaultBenhAnKhamBenhShouldBeFound("benhNhanCu.in=" + DEFAULT_BENH_NHAN_CU + "," + UPDATED_BENH_NHAN_CU);

        // Get all the benhAnKhamBenhList where benhNhanCu equals to UPDATED_BENH_NHAN_CU
        defaultBenhAnKhamBenhShouldNotBeFound("benhNhanCu.in=" + UPDATED_BENH_NHAN_CU);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByBenhNhanCuIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where benhNhanCu is not null
        defaultBenhAnKhamBenhShouldBeFound("benhNhanCu.specified=true");

        // Get all the benhAnKhamBenhList where benhNhanCu is null
        defaultBenhAnKhamBenhShouldNotBeFound("benhNhanCu.specified=false");
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByCanhBaoIsEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where canhBao equals to DEFAULT_CANH_BAO
        defaultBenhAnKhamBenhShouldBeFound("canhBao.equals=" + DEFAULT_CANH_BAO);

        // Get all the benhAnKhamBenhList where canhBao equals to UPDATED_CANH_BAO
        defaultBenhAnKhamBenhShouldNotBeFound("canhBao.equals=" + UPDATED_CANH_BAO);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByCanhBaoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where canhBao not equals to DEFAULT_CANH_BAO
        defaultBenhAnKhamBenhShouldNotBeFound("canhBao.notEquals=" + DEFAULT_CANH_BAO);

        // Get all the benhAnKhamBenhList where canhBao not equals to UPDATED_CANH_BAO
        defaultBenhAnKhamBenhShouldBeFound("canhBao.notEquals=" + UPDATED_CANH_BAO);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByCanhBaoIsInShouldWork() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where canhBao in DEFAULT_CANH_BAO or UPDATED_CANH_BAO
        defaultBenhAnKhamBenhShouldBeFound("canhBao.in=" + DEFAULT_CANH_BAO + "," + UPDATED_CANH_BAO);

        // Get all the benhAnKhamBenhList where canhBao equals to UPDATED_CANH_BAO
        defaultBenhAnKhamBenhShouldNotBeFound("canhBao.in=" + UPDATED_CANH_BAO);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByCanhBaoIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where canhBao is not null
        defaultBenhAnKhamBenhShouldBeFound("canhBao.specified=true");

        // Get all the benhAnKhamBenhList where canhBao is null
        defaultBenhAnKhamBenhShouldNotBeFound("canhBao.specified=false");
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByCapCuuIsEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where capCuu equals to DEFAULT_CAP_CUU
        defaultBenhAnKhamBenhShouldBeFound("capCuu.equals=" + DEFAULT_CAP_CUU);

        // Get all the benhAnKhamBenhList where capCuu equals to UPDATED_CAP_CUU
        defaultBenhAnKhamBenhShouldNotBeFound("capCuu.equals=" + UPDATED_CAP_CUU);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByCapCuuIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where capCuu not equals to DEFAULT_CAP_CUU
        defaultBenhAnKhamBenhShouldNotBeFound("capCuu.notEquals=" + DEFAULT_CAP_CUU);

        // Get all the benhAnKhamBenhList where capCuu not equals to UPDATED_CAP_CUU
        defaultBenhAnKhamBenhShouldBeFound("capCuu.notEquals=" + UPDATED_CAP_CUU);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByCapCuuIsInShouldWork() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where capCuu in DEFAULT_CAP_CUU or UPDATED_CAP_CUU
        defaultBenhAnKhamBenhShouldBeFound("capCuu.in=" + DEFAULT_CAP_CUU + "," + UPDATED_CAP_CUU);

        // Get all the benhAnKhamBenhList where capCuu equals to UPDATED_CAP_CUU
        defaultBenhAnKhamBenhShouldNotBeFound("capCuu.in=" + UPDATED_CAP_CUU);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByCapCuuIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where capCuu is not null
        defaultBenhAnKhamBenhShouldBeFound("capCuu.specified=true");

        // Get all the benhAnKhamBenhList where capCuu is null
        defaultBenhAnKhamBenhShouldNotBeFound("capCuu.specified=false");
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByChiCoNamSinhIsEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where chiCoNamSinh equals to DEFAULT_CHI_CO_NAM_SINH
        defaultBenhAnKhamBenhShouldBeFound("chiCoNamSinh.equals=" + DEFAULT_CHI_CO_NAM_SINH);

        // Get all the benhAnKhamBenhList where chiCoNamSinh equals to UPDATED_CHI_CO_NAM_SINH
        defaultBenhAnKhamBenhShouldNotBeFound("chiCoNamSinh.equals=" + UPDATED_CHI_CO_NAM_SINH);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByChiCoNamSinhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where chiCoNamSinh not equals to DEFAULT_CHI_CO_NAM_SINH
        defaultBenhAnKhamBenhShouldNotBeFound("chiCoNamSinh.notEquals=" + DEFAULT_CHI_CO_NAM_SINH);

        // Get all the benhAnKhamBenhList where chiCoNamSinh not equals to UPDATED_CHI_CO_NAM_SINH
        defaultBenhAnKhamBenhShouldBeFound("chiCoNamSinh.notEquals=" + UPDATED_CHI_CO_NAM_SINH);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByChiCoNamSinhIsInShouldWork() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where chiCoNamSinh in DEFAULT_CHI_CO_NAM_SINH or UPDATED_CHI_CO_NAM_SINH
        defaultBenhAnKhamBenhShouldBeFound("chiCoNamSinh.in=" + DEFAULT_CHI_CO_NAM_SINH + "," + UPDATED_CHI_CO_NAM_SINH);

        // Get all the benhAnKhamBenhList where chiCoNamSinh equals to UPDATED_CHI_CO_NAM_SINH
        defaultBenhAnKhamBenhShouldNotBeFound("chiCoNamSinh.in=" + UPDATED_CHI_CO_NAM_SINH);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByChiCoNamSinhIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where chiCoNamSinh is not null
        defaultBenhAnKhamBenhShouldBeFound("chiCoNamSinh.specified=true");

        // Get all the benhAnKhamBenhList where chiCoNamSinh is null
        defaultBenhAnKhamBenhShouldNotBeFound("chiCoNamSinh.specified=false");
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByCmndBenhNhanIsEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where cmndBenhNhan equals to DEFAULT_CMND_BENH_NHAN
        defaultBenhAnKhamBenhShouldBeFound("cmndBenhNhan.equals=" + DEFAULT_CMND_BENH_NHAN);

        // Get all the benhAnKhamBenhList where cmndBenhNhan equals to UPDATED_CMND_BENH_NHAN
        defaultBenhAnKhamBenhShouldNotBeFound("cmndBenhNhan.equals=" + UPDATED_CMND_BENH_NHAN);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByCmndBenhNhanIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where cmndBenhNhan not equals to DEFAULT_CMND_BENH_NHAN
        defaultBenhAnKhamBenhShouldNotBeFound("cmndBenhNhan.notEquals=" + DEFAULT_CMND_BENH_NHAN);

        // Get all the benhAnKhamBenhList where cmndBenhNhan not equals to UPDATED_CMND_BENH_NHAN
        defaultBenhAnKhamBenhShouldBeFound("cmndBenhNhan.notEquals=" + UPDATED_CMND_BENH_NHAN);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByCmndBenhNhanIsInShouldWork() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where cmndBenhNhan in DEFAULT_CMND_BENH_NHAN or UPDATED_CMND_BENH_NHAN
        defaultBenhAnKhamBenhShouldBeFound("cmndBenhNhan.in=" + DEFAULT_CMND_BENH_NHAN + "," + UPDATED_CMND_BENH_NHAN);

        // Get all the benhAnKhamBenhList where cmndBenhNhan equals to UPDATED_CMND_BENH_NHAN
        defaultBenhAnKhamBenhShouldNotBeFound("cmndBenhNhan.in=" + UPDATED_CMND_BENH_NHAN);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByCmndBenhNhanIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where cmndBenhNhan is not null
        defaultBenhAnKhamBenhShouldBeFound("cmndBenhNhan.specified=true");

        // Get all the benhAnKhamBenhList where cmndBenhNhan is null
        defaultBenhAnKhamBenhShouldNotBeFound("cmndBenhNhan.specified=false");
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByCmndBenhNhanContainsSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where cmndBenhNhan contains DEFAULT_CMND_BENH_NHAN
        defaultBenhAnKhamBenhShouldBeFound("cmndBenhNhan.contains=" + DEFAULT_CMND_BENH_NHAN);

        // Get all the benhAnKhamBenhList where cmndBenhNhan contains UPDATED_CMND_BENH_NHAN
        defaultBenhAnKhamBenhShouldNotBeFound("cmndBenhNhan.contains=" + UPDATED_CMND_BENH_NHAN);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByCmndBenhNhanNotContainsSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where cmndBenhNhan does not contain DEFAULT_CMND_BENH_NHAN
        defaultBenhAnKhamBenhShouldNotBeFound("cmndBenhNhan.doesNotContain=" + DEFAULT_CMND_BENH_NHAN);

        // Get all the benhAnKhamBenhList where cmndBenhNhan does not contain UPDATED_CMND_BENH_NHAN
        defaultBenhAnKhamBenhShouldBeFound("cmndBenhNhan.doesNotContain=" + UPDATED_CMND_BENH_NHAN);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByCmndNguoiNhaIsEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where cmndNguoiNha equals to DEFAULT_CMND_NGUOI_NHA
        defaultBenhAnKhamBenhShouldBeFound("cmndNguoiNha.equals=" + DEFAULT_CMND_NGUOI_NHA);

        // Get all the benhAnKhamBenhList where cmndNguoiNha equals to UPDATED_CMND_NGUOI_NHA
        defaultBenhAnKhamBenhShouldNotBeFound("cmndNguoiNha.equals=" + UPDATED_CMND_NGUOI_NHA);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByCmndNguoiNhaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where cmndNguoiNha not equals to DEFAULT_CMND_NGUOI_NHA
        defaultBenhAnKhamBenhShouldNotBeFound("cmndNguoiNha.notEquals=" + DEFAULT_CMND_NGUOI_NHA);

        // Get all the benhAnKhamBenhList where cmndNguoiNha not equals to UPDATED_CMND_NGUOI_NHA
        defaultBenhAnKhamBenhShouldBeFound("cmndNguoiNha.notEquals=" + UPDATED_CMND_NGUOI_NHA);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByCmndNguoiNhaIsInShouldWork() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where cmndNguoiNha in DEFAULT_CMND_NGUOI_NHA or UPDATED_CMND_NGUOI_NHA
        defaultBenhAnKhamBenhShouldBeFound("cmndNguoiNha.in=" + DEFAULT_CMND_NGUOI_NHA + "," + UPDATED_CMND_NGUOI_NHA);

        // Get all the benhAnKhamBenhList where cmndNguoiNha equals to UPDATED_CMND_NGUOI_NHA
        defaultBenhAnKhamBenhShouldNotBeFound("cmndNguoiNha.in=" + UPDATED_CMND_NGUOI_NHA);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByCmndNguoiNhaIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where cmndNguoiNha is not null
        defaultBenhAnKhamBenhShouldBeFound("cmndNguoiNha.specified=true");

        // Get all the benhAnKhamBenhList where cmndNguoiNha is null
        defaultBenhAnKhamBenhShouldNotBeFound("cmndNguoiNha.specified=false");
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByCmndNguoiNhaContainsSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where cmndNguoiNha contains DEFAULT_CMND_NGUOI_NHA
        defaultBenhAnKhamBenhShouldBeFound("cmndNguoiNha.contains=" + DEFAULT_CMND_NGUOI_NHA);

        // Get all the benhAnKhamBenhList where cmndNguoiNha contains UPDATED_CMND_NGUOI_NHA
        defaultBenhAnKhamBenhShouldNotBeFound("cmndNguoiNha.contains=" + UPDATED_CMND_NGUOI_NHA);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByCmndNguoiNhaNotContainsSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where cmndNguoiNha does not contain DEFAULT_CMND_NGUOI_NHA
        defaultBenhAnKhamBenhShouldNotBeFound("cmndNguoiNha.doesNotContain=" + DEFAULT_CMND_NGUOI_NHA);

        // Get all the benhAnKhamBenhList where cmndNguoiNha does not contain UPDATED_CMND_NGUOI_NHA
        defaultBenhAnKhamBenhShouldBeFound("cmndNguoiNha.doesNotContain=" + UPDATED_CMND_NGUOI_NHA);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByCoBaoHiemIsEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where coBaoHiem equals to DEFAULT_CO_BAO_HIEM
        defaultBenhAnKhamBenhShouldBeFound("coBaoHiem.equals=" + DEFAULT_CO_BAO_HIEM);

        // Get all the benhAnKhamBenhList where coBaoHiem equals to UPDATED_CO_BAO_HIEM
        defaultBenhAnKhamBenhShouldNotBeFound("coBaoHiem.equals=" + UPDATED_CO_BAO_HIEM);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByCoBaoHiemIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where coBaoHiem not equals to DEFAULT_CO_BAO_HIEM
        defaultBenhAnKhamBenhShouldNotBeFound("coBaoHiem.notEquals=" + DEFAULT_CO_BAO_HIEM);

        // Get all the benhAnKhamBenhList where coBaoHiem not equals to UPDATED_CO_BAO_HIEM
        defaultBenhAnKhamBenhShouldBeFound("coBaoHiem.notEquals=" + UPDATED_CO_BAO_HIEM);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByCoBaoHiemIsInShouldWork() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where coBaoHiem in DEFAULT_CO_BAO_HIEM or UPDATED_CO_BAO_HIEM
        defaultBenhAnKhamBenhShouldBeFound("coBaoHiem.in=" + DEFAULT_CO_BAO_HIEM + "," + UPDATED_CO_BAO_HIEM);

        // Get all the benhAnKhamBenhList where coBaoHiem equals to UPDATED_CO_BAO_HIEM
        defaultBenhAnKhamBenhShouldNotBeFound("coBaoHiem.in=" + UPDATED_CO_BAO_HIEM);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByCoBaoHiemIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where coBaoHiem is not null
        defaultBenhAnKhamBenhShouldBeFound("coBaoHiem.specified=true");

        // Get all the benhAnKhamBenhList where coBaoHiem is null
        defaultBenhAnKhamBenhShouldNotBeFound("coBaoHiem.specified=false");
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByDiaChiIsEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where diaChi equals to DEFAULT_DIA_CHI
        defaultBenhAnKhamBenhShouldBeFound("diaChi.equals=" + DEFAULT_DIA_CHI);

        // Get all the benhAnKhamBenhList where diaChi equals to UPDATED_DIA_CHI
        defaultBenhAnKhamBenhShouldNotBeFound("diaChi.equals=" + UPDATED_DIA_CHI);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByDiaChiIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where diaChi not equals to DEFAULT_DIA_CHI
        defaultBenhAnKhamBenhShouldNotBeFound("diaChi.notEquals=" + DEFAULT_DIA_CHI);

        // Get all the benhAnKhamBenhList where diaChi not equals to UPDATED_DIA_CHI
        defaultBenhAnKhamBenhShouldBeFound("diaChi.notEquals=" + UPDATED_DIA_CHI);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByDiaChiIsInShouldWork() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where diaChi in DEFAULT_DIA_CHI or UPDATED_DIA_CHI
        defaultBenhAnKhamBenhShouldBeFound("diaChi.in=" + DEFAULT_DIA_CHI + "," + UPDATED_DIA_CHI);

        // Get all the benhAnKhamBenhList where diaChi equals to UPDATED_DIA_CHI
        defaultBenhAnKhamBenhShouldNotBeFound("diaChi.in=" + UPDATED_DIA_CHI);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByDiaChiIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where diaChi is not null
        defaultBenhAnKhamBenhShouldBeFound("diaChi.specified=true");

        // Get all the benhAnKhamBenhList where diaChi is null
        defaultBenhAnKhamBenhShouldNotBeFound("diaChi.specified=false");
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByDiaChiContainsSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where diaChi contains DEFAULT_DIA_CHI
        defaultBenhAnKhamBenhShouldBeFound("diaChi.contains=" + DEFAULT_DIA_CHI);

        // Get all the benhAnKhamBenhList where diaChi contains UPDATED_DIA_CHI
        defaultBenhAnKhamBenhShouldNotBeFound("diaChi.contains=" + UPDATED_DIA_CHI);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByDiaChiNotContainsSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where diaChi does not contain DEFAULT_DIA_CHI
        defaultBenhAnKhamBenhShouldNotBeFound("diaChi.doesNotContain=" + DEFAULT_DIA_CHI);

        // Get all the benhAnKhamBenhList where diaChi does not contain UPDATED_DIA_CHI
        defaultBenhAnKhamBenhShouldBeFound("diaChi.doesNotContain=" + UPDATED_DIA_CHI);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where email equals to DEFAULT_EMAIL
        defaultBenhAnKhamBenhShouldBeFound("email.equals=" + DEFAULT_EMAIL);

        // Get all the benhAnKhamBenhList where email equals to UPDATED_EMAIL
        defaultBenhAnKhamBenhShouldNotBeFound("email.equals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByEmailIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where email not equals to DEFAULT_EMAIL
        defaultBenhAnKhamBenhShouldNotBeFound("email.notEquals=" + DEFAULT_EMAIL);

        // Get all the benhAnKhamBenhList where email not equals to UPDATED_EMAIL
        defaultBenhAnKhamBenhShouldBeFound("email.notEquals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByEmailIsInShouldWork() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where email in DEFAULT_EMAIL or UPDATED_EMAIL
        defaultBenhAnKhamBenhShouldBeFound("email.in=" + DEFAULT_EMAIL + "," + UPDATED_EMAIL);

        // Get all the benhAnKhamBenhList where email equals to UPDATED_EMAIL
        defaultBenhAnKhamBenhShouldNotBeFound("email.in=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByEmailIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where email is not null
        defaultBenhAnKhamBenhShouldBeFound("email.specified=true");

        // Get all the benhAnKhamBenhList where email is null
        defaultBenhAnKhamBenhShouldNotBeFound("email.specified=false");
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByEmailContainsSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where email contains DEFAULT_EMAIL
        defaultBenhAnKhamBenhShouldBeFound("email.contains=" + DEFAULT_EMAIL);

        // Get all the benhAnKhamBenhList where email contains UPDATED_EMAIL
        defaultBenhAnKhamBenhShouldNotBeFound("email.contains=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByEmailNotContainsSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where email does not contain DEFAULT_EMAIL
        defaultBenhAnKhamBenhShouldNotBeFound("email.doesNotContain=" + DEFAULT_EMAIL);

        // Get all the benhAnKhamBenhList where email does not contain UPDATED_EMAIL
        defaultBenhAnKhamBenhShouldBeFound("email.doesNotContain=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByGioiTinhIsEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where gioiTinh equals to DEFAULT_GIOI_TINH
        defaultBenhAnKhamBenhShouldBeFound("gioiTinh.equals=" + DEFAULT_GIOI_TINH);

        // Get all the benhAnKhamBenhList where gioiTinh equals to UPDATED_GIOI_TINH
        defaultBenhAnKhamBenhShouldNotBeFound("gioiTinh.equals=" + UPDATED_GIOI_TINH);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByGioiTinhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where gioiTinh not equals to DEFAULT_GIOI_TINH
        defaultBenhAnKhamBenhShouldNotBeFound("gioiTinh.notEquals=" + DEFAULT_GIOI_TINH);

        // Get all the benhAnKhamBenhList where gioiTinh not equals to UPDATED_GIOI_TINH
        defaultBenhAnKhamBenhShouldBeFound("gioiTinh.notEquals=" + UPDATED_GIOI_TINH);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByGioiTinhIsInShouldWork() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where gioiTinh in DEFAULT_GIOI_TINH or UPDATED_GIOI_TINH
        defaultBenhAnKhamBenhShouldBeFound("gioiTinh.in=" + DEFAULT_GIOI_TINH + "," + UPDATED_GIOI_TINH);

        // Get all the benhAnKhamBenhList where gioiTinh equals to UPDATED_GIOI_TINH
        defaultBenhAnKhamBenhShouldNotBeFound("gioiTinh.in=" + UPDATED_GIOI_TINH);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByGioiTinhIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where gioiTinh is not null
        defaultBenhAnKhamBenhShouldBeFound("gioiTinh.specified=true");

        // Get all the benhAnKhamBenhList where gioiTinh is null
        defaultBenhAnKhamBenhShouldNotBeFound("gioiTinh.specified=false");
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByGioiTinhIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where gioiTinh is greater than or equal to DEFAULT_GIOI_TINH
        defaultBenhAnKhamBenhShouldBeFound("gioiTinh.greaterThanOrEqual=" + DEFAULT_GIOI_TINH);

        // Get all the benhAnKhamBenhList where gioiTinh is greater than or equal to UPDATED_GIOI_TINH
        defaultBenhAnKhamBenhShouldNotBeFound("gioiTinh.greaterThanOrEqual=" + UPDATED_GIOI_TINH);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByGioiTinhIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where gioiTinh is less than or equal to DEFAULT_GIOI_TINH
        defaultBenhAnKhamBenhShouldBeFound("gioiTinh.lessThanOrEqual=" + DEFAULT_GIOI_TINH);

        // Get all the benhAnKhamBenhList where gioiTinh is less than or equal to SMALLER_GIOI_TINH
        defaultBenhAnKhamBenhShouldNotBeFound("gioiTinh.lessThanOrEqual=" + SMALLER_GIOI_TINH);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByGioiTinhIsLessThanSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where gioiTinh is less than DEFAULT_GIOI_TINH
        defaultBenhAnKhamBenhShouldNotBeFound("gioiTinh.lessThan=" + DEFAULT_GIOI_TINH);

        // Get all the benhAnKhamBenhList where gioiTinh is less than UPDATED_GIOI_TINH
        defaultBenhAnKhamBenhShouldBeFound("gioiTinh.lessThan=" + UPDATED_GIOI_TINH);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByGioiTinhIsGreaterThanSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where gioiTinh is greater than DEFAULT_GIOI_TINH
        defaultBenhAnKhamBenhShouldNotBeFound("gioiTinh.greaterThan=" + DEFAULT_GIOI_TINH);

        // Get all the benhAnKhamBenhList where gioiTinh is greater than SMALLER_GIOI_TINH
        defaultBenhAnKhamBenhShouldBeFound("gioiTinh.greaterThan=" + SMALLER_GIOI_TINH);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByKhangTheIsEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where khangThe equals to DEFAULT_KHANG_THE
        defaultBenhAnKhamBenhShouldBeFound("khangThe.equals=" + DEFAULT_KHANG_THE);

        // Get all the benhAnKhamBenhList where khangThe equals to UPDATED_KHANG_THE
        defaultBenhAnKhamBenhShouldNotBeFound("khangThe.equals=" + UPDATED_KHANG_THE);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByKhangTheIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where khangThe not equals to DEFAULT_KHANG_THE
        defaultBenhAnKhamBenhShouldNotBeFound("khangThe.notEquals=" + DEFAULT_KHANG_THE);

        // Get all the benhAnKhamBenhList where khangThe not equals to UPDATED_KHANG_THE
        defaultBenhAnKhamBenhShouldBeFound("khangThe.notEquals=" + UPDATED_KHANG_THE);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByKhangTheIsInShouldWork() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where khangThe in DEFAULT_KHANG_THE or UPDATED_KHANG_THE
        defaultBenhAnKhamBenhShouldBeFound("khangThe.in=" + DEFAULT_KHANG_THE + "," + UPDATED_KHANG_THE);

        // Get all the benhAnKhamBenhList where khangThe equals to UPDATED_KHANG_THE
        defaultBenhAnKhamBenhShouldNotBeFound("khangThe.in=" + UPDATED_KHANG_THE);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByKhangTheIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where khangThe is not null
        defaultBenhAnKhamBenhShouldBeFound("khangThe.specified=true");

        // Get all the benhAnKhamBenhList where khangThe is null
        defaultBenhAnKhamBenhShouldNotBeFound("khangThe.specified=false");
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByKhangTheContainsSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where khangThe contains DEFAULT_KHANG_THE
        defaultBenhAnKhamBenhShouldBeFound("khangThe.contains=" + DEFAULT_KHANG_THE);

        // Get all the benhAnKhamBenhList where khangThe contains UPDATED_KHANG_THE
        defaultBenhAnKhamBenhShouldNotBeFound("khangThe.contains=" + UPDATED_KHANG_THE);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByKhangTheNotContainsSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where khangThe does not contain DEFAULT_KHANG_THE
        defaultBenhAnKhamBenhShouldNotBeFound("khangThe.doesNotContain=" + DEFAULT_KHANG_THE);

        // Get all the benhAnKhamBenhList where khangThe does not contain UPDATED_KHANG_THE
        defaultBenhAnKhamBenhShouldBeFound("khangThe.doesNotContain=" + UPDATED_KHANG_THE);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByLanKhamTrongNgayIsEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where lanKhamTrongNgay equals to DEFAULT_LAN_KHAM_TRONG_NGAY
        defaultBenhAnKhamBenhShouldBeFound("lanKhamTrongNgay.equals=" + DEFAULT_LAN_KHAM_TRONG_NGAY);

        // Get all the benhAnKhamBenhList where lanKhamTrongNgay equals to UPDATED_LAN_KHAM_TRONG_NGAY
        defaultBenhAnKhamBenhShouldNotBeFound("lanKhamTrongNgay.equals=" + UPDATED_LAN_KHAM_TRONG_NGAY);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByLanKhamTrongNgayIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where lanKhamTrongNgay not equals to DEFAULT_LAN_KHAM_TRONG_NGAY
        defaultBenhAnKhamBenhShouldNotBeFound("lanKhamTrongNgay.notEquals=" + DEFAULT_LAN_KHAM_TRONG_NGAY);

        // Get all the benhAnKhamBenhList where lanKhamTrongNgay not equals to UPDATED_LAN_KHAM_TRONG_NGAY
        defaultBenhAnKhamBenhShouldBeFound("lanKhamTrongNgay.notEquals=" + UPDATED_LAN_KHAM_TRONG_NGAY);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByLanKhamTrongNgayIsInShouldWork() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where lanKhamTrongNgay in DEFAULT_LAN_KHAM_TRONG_NGAY or UPDATED_LAN_KHAM_TRONG_NGAY
        defaultBenhAnKhamBenhShouldBeFound("lanKhamTrongNgay.in=" + DEFAULT_LAN_KHAM_TRONG_NGAY + "," + UPDATED_LAN_KHAM_TRONG_NGAY);

        // Get all the benhAnKhamBenhList where lanKhamTrongNgay equals to UPDATED_LAN_KHAM_TRONG_NGAY
        defaultBenhAnKhamBenhShouldNotBeFound("lanKhamTrongNgay.in=" + UPDATED_LAN_KHAM_TRONG_NGAY);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByLanKhamTrongNgayIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where lanKhamTrongNgay is not null
        defaultBenhAnKhamBenhShouldBeFound("lanKhamTrongNgay.specified=true");

        // Get all the benhAnKhamBenhList where lanKhamTrongNgay is null
        defaultBenhAnKhamBenhShouldNotBeFound("lanKhamTrongNgay.specified=false");
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByLanKhamTrongNgayIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where lanKhamTrongNgay is greater than or equal to DEFAULT_LAN_KHAM_TRONG_NGAY
        defaultBenhAnKhamBenhShouldBeFound("lanKhamTrongNgay.greaterThanOrEqual=" + DEFAULT_LAN_KHAM_TRONG_NGAY);

        // Get all the benhAnKhamBenhList where lanKhamTrongNgay is greater than or equal to UPDATED_LAN_KHAM_TRONG_NGAY
        defaultBenhAnKhamBenhShouldNotBeFound("lanKhamTrongNgay.greaterThanOrEqual=" + UPDATED_LAN_KHAM_TRONG_NGAY);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByLanKhamTrongNgayIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where lanKhamTrongNgay is less than or equal to DEFAULT_LAN_KHAM_TRONG_NGAY
        defaultBenhAnKhamBenhShouldBeFound("lanKhamTrongNgay.lessThanOrEqual=" + DEFAULT_LAN_KHAM_TRONG_NGAY);

        // Get all the benhAnKhamBenhList where lanKhamTrongNgay is less than or equal to SMALLER_LAN_KHAM_TRONG_NGAY
        defaultBenhAnKhamBenhShouldNotBeFound("lanKhamTrongNgay.lessThanOrEqual=" + SMALLER_LAN_KHAM_TRONG_NGAY);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByLanKhamTrongNgayIsLessThanSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where lanKhamTrongNgay is less than DEFAULT_LAN_KHAM_TRONG_NGAY
        defaultBenhAnKhamBenhShouldNotBeFound("lanKhamTrongNgay.lessThan=" + DEFAULT_LAN_KHAM_TRONG_NGAY);

        // Get all the benhAnKhamBenhList where lanKhamTrongNgay is less than UPDATED_LAN_KHAM_TRONG_NGAY
        defaultBenhAnKhamBenhShouldBeFound("lanKhamTrongNgay.lessThan=" + UPDATED_LAN_KHAM_TRONG_NGAY);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByLanKhamTrongNgayIsGreaterThanSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where lanKhamTrongNgay is greater than DEFAULT_LAN_KHAM_TRONG_NGAY
        defaultBenhAnKhamBenhShouldNotBeFound("lanKhamTrongNgay.greaterThan=" + DEFAULT_LAN_KHAM_TRONG_NGAY);

        // Get all the benhAnKhamBenhList where lanKhamTrongNgay is greater than SMALLER_LAN_KHAM_TRONG_NGAY
        defaultBenhAnKhamBenhShouldBeFound("lanKhamTrongNgay.greaterThan=" + SMALLER_LAN_KHAM_TRONG_NGAY);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByLoaiGiayToTreEmIsEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where loaiGiayToTreEm equals to DEFAULT_LOAI_GIAY_TO_TRE_EM
        defaultBenhAnKhamBenhShouldBeFound("loaiGiayToTreEm.equals=" + DEFAULT_LOAI_GIAY_TO_TRE_EM);

        // Get all the benhAnKhamBenhList where loaiGiayToTreEm equals to UPDATED_LOAI_GIAY_TO_TRE_EM
        defaultBenhAnKhamBenhShouldNotBeFound("loaiGiayToTreEm.equals=" + UPDATED_LOAI_GIAY_TO_TRE_EM);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByLoaiGiayToTreEmIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where loaiGiayToTreEm not equals to DEFAULT_LOAI_GIAY_TO_TRE_EM
        defaultBenhAnKhamBenhShouldNotBeFound("loaiGiayToTreEm.notEquals=" + DEFAULT_LOAI_GIAY_TO_TRE_EM);

        // Get all the benhAnKhamBenhList where loaiGiayToTreEm not equals to UPDATED_LOAI_GIAY_TO_TRE_EM
        defaultBenhAnKhamBenhShouldBeFound("loaiGiayToTreEm.notEquals=" + UPDATED_LOAI_GIAY_TO_TRE_EM);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByLoaiGiayToTreEmIsInShouldWork() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where loaiGiayToTreEm in DEFAULT_LOAI_GIAY_TO_TRE_EM or UPDATED_LOAI_GIAY_TO_TRE_EM
        defaultBenhAnKhamBenhShouldBeFound("loaiGiayToTreEm.in=" + DEFAULT_LOAI_GIAY_TO_TRE_EM + "," + UPDATED_LOAI_GIAY_TO_TRE_EM);

        // Get all the benhAnKhamBenhList where loaiGiayToTreEm equals to UPDATED_LOAI_GIAY_TO_TRE_EM
        defaultBenhAnKhamBenhShouldNotBeFound("loaiGiayToTreEm.in=" + UPDATED_LOAI_GIAY_TO_TRE_EM);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByLoaiGiayToTreEmIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where loaiGiayToTreEm is not null
        defaultBenhAnKhamBenhShouldBeFound("loaiGiayToTreEm.specified=true");

        // Get all the benhAnKhamBenhList where loaiGiayToTreEm is null
        defaultBenhAnKhamBenhShouldNotBeFound("loaiGiayToTreEm.specified=false");
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByLoaiGiayToTreEmIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where loaiGiayToTreEm is greater than or equal to DEFAULT_LOAI_GIAY_TO_TRE_EM
        defaultBenhAnKhamBenhShouldBeFound("loaiGiayToTreEm.greaterThanOrEqual=" + DEFAULT_LOAI_GIAY_TO_TRE_EM);

        // Get all the benhAnKhamBenhList where loaiGiayToTreEm is greater than or equal to UPDATED_LOAI_GIAY_TO_TRE_EM
        defaultBenhAnKhamBenhShouldNotBeFound("loaiGiayToTreEm.greaterThanOrEqual=" + UPDATED_LOAI_GIAY_TO_TRE_EM);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByLoaiGiayToTreEmIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where loaiGiayToTreEm is less than or equal to DEFAULT_LOAI_GIAY_TO_TRE_EM
        defaultBenhAnKhamBenhShouldBeFound("loaiGiayToTreEm.lessThanOrEqual=" + DEFAULT_LOAI_GIAY_TO_TRE_EM);

        // Get all the benhAnKhamBenhList where loaiGiayToTreEm is less than or equal to SMALLER_LOAI_GIAY_TO_TRE_EM
        defaultBenhAnKhamBenhShouldNotBeFound("loaiGiayToTreEm.lessThanOrEqual=" + SMALLER_LOAI_GIAY_TO_TRE_EM);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByLoaiGiayToTreEmIsLessThanSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where loaiGiayToTreEm is less than DEFAULT_LOAI_GIAY_TO_TRE_EM
        defaultBenhAnKhamBenhShouldNotBeFound("loaiGiayToTreEm.lessThan=" + DEFAULT_LOAI_GIAY_TO_TRE_EM);

        // Get all the benhAnKhamBenhList where loaiGiayToTreEm is less than UPDATED_LOAI_GIAY_TO_TRE_EM
        defaultBenhAnKhamBenhShouldBeFound("loaiGiayToTreEm.lessThan=" + UPDATED_LOAI_GIAY_TO_TRE_EM);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByLoaiGiayToTreEmIsGreaterThanSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where loaiGiayToTreEm is greater than DEFAULT_LOAI_GIAY_TO_TRE_EM
        defaultBenhAnKhamBenhShouldNotBeFound("loaiGiayToTreEm.greaterThan=" + DEFAULT_LOAI_GIAY_TO_TRE_EM);

        // Get all the benhAnKhamBenhList where loaiGiayToTreEm is greater than SMALLER_LOAI_GIAY_TO_TRE_EM
        defaultBenhAnKhamBenhShouldBeFound("loaiGiayToTreEm.greaterThan=" + SMALLER_LOAI_GIAY_TO_TRE_EM);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNgayCapCmndIsEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where ngayCapCmnd equals to DEFAULT_NGAY_CAP_CMND
        defaultBenhAnKhamBenhShouldBeFound("ngayCapCmnd.equals=" + DEFAULT_NGAY_CAP_CMND);

        // Get all the benhAnKhamBenhList where ngayCapCmnd equals to UPDATED_NGAY_CAP_CMND
        defaultBenhAnKhamBenhShouldNotBeFound("ngayCapCmnd.equals=" + UPDATED_NGAY_CAP_CMND);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNgayCapCmndIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where ngayCapCmnd not equals to DEFAULT_NGAY_CAP_CMND
        defaultBenhAnKhamBenhShouldNotBeFound("ngayCapCmnd.notEquals=" + DEFAULT_NGAY_CAP_CMND);

        // Get all the benhAnKhamBenhList where ngayCapCmnd not equals to UPDATED_NGAY_CAP_CMND
        defaultBenhAnKhamBenhShouldBeFound("ngayCapCmnd.notEquals=" + UPDATED_NGAY_CAP_CMND);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNgayCapCmndIsInShouldWork() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where ngayCapCmnd in DEFAULT_NGAY_CAP_CMND or UPDATED_NGAY_CAP_CMND
        defaultBenhAnKhamBenhShouldBeFound("ngayCapCmnd.in=" + DEFAULT_NGAY_CAP_CMND + "," + UPDATED_NGAY_CAP_CMND);

        // Get all the benhAnKhamBenhList where ngayCapCmnd equals to UPDATED_NGAY_CAP_CMND
        defaultBenhAnKhamBenhShouldNotBeFound("ngayCapCmnd.in=" + UPDATED_NGAY_CAP_CMND);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNgayCapCmndIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where ngayCapCmnd is not null
        defaultBenhAnKhamBenhShouldBeFound("ngayCapCmnd.specified=true");

        // Get all the benhAnKhamBenhList where ngayCapCmnd is null
        defaultBenhAnKhamBenhShouldNotBeFound("ngayCapCmnd.specified=false");
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNgayCapCmndIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where ngayCapCmnd is greater than or equal to DEFAULT_NGAY_CAP_CMND
        defaultBenhAnKhamBenhShouldBeFound("ngayCapCmnd.greaterThanOrEqual=" + DEFAULT_NGAY_CAP_CMND);

        // Get all the benhAnKhamBenhList where ngayCapCmnd is greater than or equal to UPDATED_NGAY_CAP_CMND
        defaultBenhAnKhamBenhShouldNotBeFound("ngayCapCmnd.greaterThanOrEqual=" + UPDATED_NGAY_CAP_CMND);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNgayCapCmndIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where ngayCapCmnd is less than or equal to DEFAULT_NGAY_CAP_CMND
        defaultBenhAnKhamBenhShouldBeFound("ngayCapCmnd.lessThanOrEqual=" + DEFAULT_NGAY_CAP_CMND);

        // Get all the benhAnKhamBenhList where ngayCapCmnd is less than or equal to SMALLER_NGAY_CAP_CMND
        defaultBenhAnKhamBenhShouldNotBeFound("ngayCapCmnd.lessThanOrEqual=" + SMALLER_NGAY_CAP_CMND);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNgayCapCmndIsLessThanSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where ngayCapCmnd is less than DEFAULT_NGAY_CAP_CMND
        defaultBenhAnKhamBenhShouldNotBeFound("ngayCapCmnd.lessThan=" + DEFAULT_NGAY_CAP_CMND);

        // Get all the benhAnKhamBenhList where ngayCapCmnd is less than UPDATED_NGAY_CAP_CMND
        defaultBenhAnKhamBenhShouldBeFound("ngayCapCmnd.lessThan=" + UPDATED_NGAY_CAP_CMND);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNgayCapCmndIsGreaterThanSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where ngayCapCmnd is greater than DEFAULT_NGAY_CAP_CMND
        defaultBenhAnKhamBenhShouldNotBeFound("ngayCapCmnd.greaterThan=" + DEFAULT_NGAY_CAP_CMND);

        // Get all the benhAnKhamBenhList where ngayCapCmnd is greater than SMALLER_NGAY_CAP_CMND
        defaultBenhAnKhamBenhShouldBeFound("ngayCapCmnd.greaterThan=" + SMALLER_NGAY_CAP_CMND);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNgayMienCungChiTraIsEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where ngayMienCungChiTra equals to DEFAULT_NGAY_MIEN_CUNG_CHI_TRA
        defaultBenhAnKhamBenhShouldBeFound("ngayMienCungChiTra.equals=" + DEFAULT_NGAY_MIEN_CUNG_CHI_TRA);

        // Get all the benhAnKhamBenhList where ngayMienCungChiTra equals to UPDATED_NGAY_MIEN_CUNG_CHI_TRA
        defaultBenhAnKhamBenhShouldNotBeFound("ngayMienCungChiTra.equals=" + UPDATED_NGAY_MIEN_CUNG_CHI_TRA);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNgayMienCungChiTraIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where ngayMienCungChiTra not equals to DEFAULT_NGAY_MIEN_CUNG_CHI_TRA
        defaultBenhAnKhamBenhShouldNotBeFound("ngayMienCungChiTra.notEquals=" + DEFAULT_NGAY_MIEN_CUNG_CHI_TRA);

        // Get all the benhAnKhamBenhList where ngayMienCungChiTra not equals to UPDATED_NGAY_MIEN_CUNG_CHI_TRA
        defaultBenhAnKhamBenhShouldBeFound("ngayMienCungChiTra.notEquals=" + UPDATED_NGAY_MIEN_CUNG_CHI_TRA);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNgayMienCungChiTraIsInShouldWork() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where ngayMienCungChiTra in DEFAULT_NGAY_MIEN_CUNG_CHI_TRA or UPDATED_NGAY_MIEN_CUNG_CHI_TRA
        defaultBenhAnKhamBenhShouldBeFound("ngayMienCungChiTra.in=" + DEFAULT_NGAY_MIEN_CUNG_CHI_TRA + "," + UPDATED_NGAY_MIEN_CUNG_CHI_TRA);

        // Get all the benhAnKhamBenhList where ngayMienCungChiTra equals to UPDATED_NGAY_MIEN_CUNG_CHI_TRA
        defaultBenhAnKhamBenhShouldNotBeFound("ngayMienCungChiTra.in=" + UPDATED_NGAY_MIEN_CUNG_CHI_TRA);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNgayMienCungChiTraIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where ngayMienCungChiTra is not null
        defaultBenhAnKhamBenhShouldBeFound("ngayMienCungChiTra.specified=true");

        // Get all the benhAnKhamBenhList where ngayMienCungChiTra is null
        defaultBenhAnKhamBenhShouldNotBeFound("ngayMienCungChiTra.specified=false");
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNgayMienCungChiTraIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where ngayMienCungChiTra is greater than or equal to DEFAULT_NGAY_MIEN_CUNG_CHI_TRA
        defaultBenhAnKhamBenhShouldBeFound("ngayMienCungChiTra.greaterThanOrEqual=" + DEFAULT_NGAY_MIEN_CUNG_CHI_TRA);

        // Get all the benhAnKhamBenhList where ngayMienCungChiTra is greater than or equal to UPDATED_NGAY_MIEN_CUNG_CHI_TRA
        defaultBenhAnKhamBenhShouldNotBeFound("ngayMienCungChiTra.greaterThanOrEqual=" + UPDATED_NGAY_MIEN_CUNG_CHI_TRA);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNgayMienCungChiTraIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where ngayMienCungChiTra is less than or equal to DEFAULT_NGAY_MIEN_CUNG_CHI_TRA
        defaultBenhAnKhamBenhShouldBeFound("ngayMienCungChiTra.lessThanOrEqual=" + DEFAULT_NGAY_MIEN_CUNG_CHI_TRA);

        // Get all the benhAnKhamBenhList where ngayMienCungChiTra is less than or equal to SMALLER_NGAY_MIEN_CUNG_CHI_TRA
        defaultBenhAnKhamBenhShouldNotBeFound("ngayMienCungChiTra.lessThanOrEqual=" + SMALLER_NGAY_MIEN_CUNG_CHI_TRA);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNgayMienCungChiTraIsLessThanSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where ngayMienCungChiTra is less than DEFAULT_NGAY_MIEN_CUNG_CHI_TRA
        defaultBenhAnKhamBenhShouldNotBeFound("ngayMienCungChiTra.lessThan=" + DEFAULT_NGAY_MIEN_CUNG_CHI_TRA);

        // Get all the benhAnKhamBenhList where ngayMienCungChiTra is less than UPDATED_NGAY_MIEN_CUNG_CHI_TRA
        defaultBenhAnKhamBenhShouldBeFound("ngayMienCungChiTra.lessThan=" + UPDATED_NGAY_MIEN_CUNG_CHI_TRA);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNgayMienCungChiTraIsGreaterThanSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where ngayMienCungChiTra is greater than DEFAULT_NGAY_MIEN_CUNG_CHI_TRA
        defaultBenhAnKhamBenhShouldNotBeFound("ngayMienCungChiTra.greaterThan=" + DEFAULT_NGAY_MIEN_CUNG_CHI_TRA);

        // Get all the benhAnKhamBenhList where ngayMienCungChiTra is greater than SMALLER_NGAY_MIEN_CUNG_CHI_TRA
        defaultBenhAnKhamBenhShouldBeFound("ngayMienCungChiTra.greaterThan=" + SMALLER_NGAY_MIEN_CUNG_CHI_TRA);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNgaySinhIsEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where ngaySinh equals to DEFAULT_NGAY_SINH
        defaultBenhAnKhamBenhShouldBeFound("ngaySinh.equals=" + DEFAULT_NGAY_SINH);

        // Get all the benhAnKhamBenhList where ngaySinh equals to UPDATED_NGAY_SINH
        defaultBenhAnKhamBenhShouldNotBeFound("ngaySinh.equals=" + UPDATED_NGAY_SINH);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNgaySinhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where ngaySinh not equals to DEFAULT_NGAY_SINH
        defaultBenhAnKhamBenhShouldNotBeFound("ngaySinh.notEquals=" + DEFAULT_NGAY_SINH);

        // Get all the benhAnKhamBenhList where ngaySinh not equals to UPDATED_NGAY_SINH
        defaultBenhAnKhamBenhShouldBeFound("ngaySinh.notEquals=" + UPDATED_NGAY_SINH);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNgaySinhIsInShouldWork() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where ngaySinh in DEFAULT_NGAY_SINH or UPDATED_NGAY_SINH
        defaultBenhAnKhamBenhShouldBeFound("ngaySinh.in=" + DEFAULT_NGAY_SINH + "," + UPDATED_NGAY_SINH);

        // Get all the benhAnKhamBenhList where ngaySinh equals to UPDATED_NGAY_SINH
        defaultBenhAnKhamBenhShouldNotBeFound("ngaySinh.in=" + UPDATED_NGAY_SINH);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNgaySinhIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where ngaySinh is not null
        defaultBenhAnKhamBenhShouldBeFound("ngaySinh.specified=true");

        // Get all the benhAnKhamBenhList where ngaySinh is null
        defaultBenhAnKhamBenhShouldNotBeFound("ngaySinh.specified=false");
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNgaySinhIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where ngaySinh is greater than or equal to DEFAULT_NGAY_SINH
        defaultBenhAnKhamBenhShouldBeFound("ngaySinh.greaterThanOrEqual=" + DEFAULT_NGAY_SINH);

        // Get all the benhAnKhamBenhList where ngaySinh is greater than or equal to UPDATED_NGAY_SINH
        defaultBenhAnKhamBenhShouldNotBeFound("ngaySinh.greaterThanOrEqual=" + UPDATED_NGAY_SINH);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNgaySinhIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where ngaySinh is less than or equal to DEFAULT_NGAY_SINH
        defaultBenhAnKhamBenhShouldBeFound("ngaySinh.lessThanOrEqual=" + DEFAULT_NGAY_SINH);

        // Get all the benhAnKhamBenhList where ngaySinh is less than or equal to SMALLER_NGAY_SINH
        defaultBenhAnKhamBenhShouldNotBeFound("ngaySinh.lessThanOrEqual=" + SMALLER_NGAY_SINH);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNgaySinhIsLessThanSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where ngaySinh is less than DEFAULT_NGAY_SINH
        defaultBenhAnKhamBenhShouldNotBeFound("ngaySinh.lessThan=" + DEFAULT_NGAY_SINH);

        // Get all the benhAnKhamBenhList where ngaySinh is less than UPDATED_NGAY_SINH
        defaultBenhAnKhamBenhShouldBeFound("ngaySinh.lessThan=" + UPDATED_NGAY_SINH);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNgaySinhIsGreaterThanSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where ngaySinh is greater than DEFAULT_NGAY_SINH
        defaultBenhAnKhamBenhShouldNotBeFound("ngaySinh.greaterThan=" + DEFAULT_NGAY_SINH);

        // Get all the benhAnKhamBenhList where ngaySinh is greater than SMALLER_NGAY_SINH
        defaultBenhAnKhamBenhShouldBeFound("ngaySinh.greaterThan=" + SMALLER_NGAY_SINH);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNhomMauIsEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where nhomMau equals to DEFAULT_NHOM_MAU
        defaultBenhAnKhamBenhShouldBeFound("nhomMau.equals=" + DEFAULT_NHOM_MAU);

        // Get all the benhAnKhamBenhList where nhomMau equals to UPDATED_NHOM_MAU
        defaultBenhAnKhamBenhShouldNotBeFound("nhomMau.equals=" + UPDATED_NHOM_MAU);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNhomMauIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where nhomMau not equals to DEFAULT_NHOM_MAU
        defaultBenhAnKhamBenhShouldNotBeFound("nhomMau.notEquals=" + DEFAULT_NHOM_MAU);

        // Get all the benhAnKhamBenhList where nhomMau not equals to UPDATED_NHOM_MAU
        defaultBenhAnKhamBenhShouldBeFound("nhomMau.notEquals=" + UPDATED_NHOM_MAU);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNhomMauIsInShouldWork() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where nhomMau in DEFAULT_NHOM_MAU or UPDATED_NHOM_MAU
        defaultBenhAnKhamBenhShouldBeFound("nhomMau.in=" + DEFAULT_NHOM_MAU + "," + UPDATED_NHOM_MAU);

        // Get all the benhAnKhamBenhList where nhomMau equals to UPDATED_NHOM_MAU
        defaultBenhAnKhamBenhShouldNotBeFound("nhomMau.in=" + UPDATED_NHOM_MAU);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNhomMauIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where nhomMau is not null
        defaultBenhAnKhamBenhShouldBeFound("nhomMau.specified=true");

        // Get all the benhAnKhamBenhList where nhomMau is null
        defaultBenhAnKhamBenhShouldNotBeFound("nhomMau.specified=false");
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNhomMauContainsSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where nhomMau contains DEFAULT_NHOM_MAU
        defaultBenhAnKhamBenhShouldBeFound("nhomMau.contains=" + DEFAULT_NHOM_MAU);

        // Get all the benhAnKhamBenhList where nhomMau contains UPDATED_NHOM_MAU
        defaultBenhAnKhamBenhShouldNotBeFound("nhomMau.contains=" + UPDATED_NHOM_MAU);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNhomMauNotContainsSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where nhomMau does not contain DEFAULT_NHOM_MAU
        defaultBenhAnKhamBenhShouldNotBeFound("nhomMau.doesNotContain=" + DEFAULT_NHOM_MAU);

        // Get all the benhAnKhamBenhList where nhomMau does not contain UPDATED_NHOM_MAU
        defaultBenhAnKhamBenhShouldBeFound("nhomMau.doesNotContain=" + UPDATED_NHOM_MAU);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNoiCapCmndIsEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where noiCapCmnd equals to DEFAULT_NOI_CAP_CMND
        defaultBenhAnKhamBenhShouldBeFound("noiCapCmnd.equals=" + DEFAULT_NOI_CAP_CMND);

        // Get all the benhAnKhamBenhList where noiCapCmnd equals to UPDATED_NOI_CAP_CMND
        defaultBenhAnKhamBenhShouldNotBeFound("noiCapCmnd.equals=" + UPDATED_NOI_CAP_CMND);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNoiCapCmndIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where noiCapCmnd not equals to DEFAULT_NOI_CAP_CMND
        defaultBenhAnKhamBenhShouldNotBeFound("noiCapCmnd.notEquals=" + DEFAULT_NOI_CAP_CMND);

        // Get all the benhAnKhamBenhList where noiCapCmnd not equals to UPDATED_NOI_CAP_CMND
        defaultBenhAnKhamBenhShouldBeFound("noiCapCmnd.notEquals=" + UPDATED_NOI_CAP_CMND);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNoiCapCmndIsInShouldWork() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where noiCapCmnd in DEFAULT_NOI_CAP_CMND or UPDATED_NOI_CAP_CMND
        defaultBenhAnKhamBenhShouldBeFound("noiCapCmnd.in=" + DEFAULT_NOI_CAP_CMND + "," + UPDATED_NOI_CAP_CMND);

        // Get all the benhAnKhamBenhList where noiCapCmnd equals to UPDATED_NOI_CAP_CMND
        defaultBenhAnKhamBenhShouldNotBeFound("noiCapCmnd.in=" + UPDATED_NOI_CAP_CMND);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNoiCapCmndIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where noiCapCmnd is not null
        defaultBenhAnKhamBenhShouldBeFound("noiCapCmnd.specified=true");

        // Get all the benhAnKhamBenhList where noiCapCmnd is null
        defaultBenhAnKhamBenhShouldNotBeFound("noiCapCmnd.specified=false");
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNoiCapCmndContainsSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where noiCapCmnd contains DEFAULT_NOI_CAP_CMND
        defaultBenhAnKhamBenhShouldBeFound("noiCapCmnd.contains=" + DEFAULT_NOI_CAP_CMND);

        // Get all the benhAnKhamBenhList where noiCapCmnd contains UPDATED_NOI_CAP_CMND
        defaultBenhAnKhamBenhShouldNotBeFound("noiCapCmnd.contains=" + UPDATED_NOI_CAP_CMND);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNoiCapCmndNotContainsSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where noiCapCmnd does not contain DEFAULT_NOI_CAP_CMND
        defaultBenhAnKhamBenhShouldNotBeFound("noiCapCmnd.doesNotContain=" + DEFAULT_NOI_CAP_CMND);

        // Get all the benhAnKhamBenhList where noiCapCmnd does not contain UPDATED_NOI_CAP_CMND
        defaultBenhAnKhamBenhShouldBeFound("noiCapCmnd.doesNotContain=" + UPDATED_NOI_CAP_CMND);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNoiLamViecIsEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where noiLamViec equals to DEFAULT_NOI_LAM_VIEC
        defaultBenhAnKhamBenhShouldBeFound("noiLamViec.equals=" + DEFAULT_NOI_LAM_VIEC);

        // Get all the benhAnKhamBenhList where noiLamViec equals to UPDATED_NOI_LAM_VIEC
        defaultBenhAnKhamBenhShouldNotBeFound("noiLamViec.equals=" + UPDATED_NOI_LAM_VIEC);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNoiLamViecIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where noiLamViec not equals to DEFAULT_NOI_LAM_VIEC
        defaultBenhAnKhamBenhShouldNotBeFound("noiLamViec.notEquals=" + DEFAULT_NOI_LAM_VIEC);

        // Get all the benhAnKhamBenhList where noiLamViec not equals to UPDATED_NOI_LAM_VIEC
        defaultBenhAnKhamBenhShouldBeFound("noiLamViec.notEquals=" + UPDATED_NOI_LAM_VIEC);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNoiLamViecIsInShouldWork() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where noiLamViec in DEFAULT_NOI_LAM_VIEC or UPDATED_NOI_LAM_VIEC
        defaultBenhAnKhamBenhShouldBeFound("noiLamViec.in=" + DEFAULT_NOI_LAM_VIEC + "," + UPDATED_NOI_LAM_VIEC);

        // Get all the benhAnKhamBenhList where noiLamViec equals to UPDATED_NOI_LAM_VIEC
        defaultBenhAnKhamBenhShouldNotBeFound("noiLamViec.in=" + UPDATED_NOI_LAM_VIEC);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNoiLamViecIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where noiLamViec is not null
        defaultBenhAnKhamBenhShouldBeFound("noiLamViec.specified=true");

        // Get all the benhAnKhamBenhList where noiLamViec is null
        defaultBenhAnKhamBenhShouldNotBeFound("noiLamViec.specified=false");
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNoiLamViecContainsSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where noiLamViec contains DEFAULT_NOI_LAM_VIEC
        defaultBenhAnKhamBenhShouldBeFound("noiLamViec.contains=" + DEFAULT_NOI_LAM_VIEC);

        // Get all the benhAnKhamBenhList where noiLamViec contains UPDATED_NOI_LAM_VIEC
        defaultBenhAnKhamBenhShouldNotBeFound("noiLamViec.contains=" + UPDATED_NOI_LAM_VIEC);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNoiLamViecNotContainsSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where noiLamViec does not contain DEFAULT_NOI_LAM_VIEC
        defaultBenhAnKhamBenhShouldNotBeFound("noiLamViec.doesNotContain=" + DEFAULT_NOI_LAM_VIEC);

        // Get all the benhAnKhamBenhList where noiLamViec does not contain UPDATED_NOI_LAM_VIEC
        defaultBenhAnKhamBenhShouldBeFound("noiLamViec.doesNotContain=" + UPDATED_NOI_LAM_VIEC);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByPhoneIsEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where phone equals to DEFAULT_PHONE
        defaultBenhAnKhamBenhShouldBeFound("phone.equals=" + DEFAULT_PHONE);

        // Get all the benhAnKhamBenhList where phone equals to UPDATED_PHONE
        defaultBenhAnKhamBenhShouldNotBeFound("phone.equals=" + UPDATED_PHONE);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByPhoneIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where phone not equals to DEFAULT_PHONE
        defaultBenhAnKhamBenhShouldNotBeFound("phone.notEquals=" + DEFAULT_PHONE);

        // Get all the benhAnKhamBenhList where phone not equals to UPDATED_PHONE
        defaultBenhAnKhamBenhShouldBeFound("phone.notEquals=" + UPDATED_PHONE);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByPhoneIsInShouldWork() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where phone in DEFAULT_PHONE or UPDATED_PHONE
        defaultBenhAnKhamBenhShouldBeFound("phone.in=" + DEFAULT_PHONE + "," + UPDATED_PHONE);

        // Get all the benhAnKhamBenhList where phone equals to UPDATED_PHONE
        defaultBenhAnKhamBenhShouldNotBeFound("phone.in=" + UPDATED_PHONE);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByPhoneIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where phone is not null
        defaultBenhAnKhamBenhShouldBeFound("phone.specified=true");

        // Get all the benhAnKhamBenhList where phone is null
        defaultBenhAnKhamBenhShouldNotBeFound("phone.specified=false");
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByPhoneContainsSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where phone contains DEFAULT_PHONE
        defaultBenhAnKhamBenhShouldBeFound("phone.contains=" + DEFAULT_PHONE);

        // Get all the benhAnKhamBenhList where phone contains UPDATED_PHONE
        defaultBenhAnKhamBenhShouldNotBeFound("phone.contains=" + UPDATED_PHONE);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByPhoneNotContainsSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where phone does not contain DEFAULT_PHONE
        defaultBenhAnKhamBenhShouldNotBeFound("phone.doesNotContain=" + DEFAULT_PHONE);

        // Get all the benhAnKhamBenhList where phone does not contain UPDATED_PHONE
        defaultBenhAnKhamBenhShouldBeFound("phone.doesNotContain=" + UPDATED_PHONE);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByPhoneNguoiNhaIsEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where phoneNguoiNha equals to DEFAULT_PHONE_NGUOI_NHA
        defaultBenhAnKhamBenhShouldBeFound("phoneNguoiNha.equals=" + DEFAULT_PHONE_NGUOI_NHA);

        // Get all the benhAnKhamBenhList where phoneNguoiNha equals to UPDATED_PHONE_NGUOI_NHA
        defaultBenhAnKhamBenhShouldNotBeFound("phoneNguoiNha.equals=" + UPDATED_PHONE_NGUOI_NHA);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByPhoneNguoiNhaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where phoneNguoiNha not equals to DEFAULT_PHONE_NGUOI_NHA
        defaultBenhAnKhamBenhShouldNotBeFound("phoneNguoiNha.notEquals=" + DEFAULT_PHONE_NGUOI_NHA);

        // Get all the benhAnKhamBenhList where phoneNguoiNha not equals to UPDATED_PHONE_NGUOI_NHA
        defaultBenhAnKhamBenhShouldBeFound("phoneNguoiNha.notEquals=" + UPDATED_PHONE_NGUOI_NHA);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByPhoneNguoiNhaIsInShouldWork() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where phoneNguoiNha in DEFAULT_PHONE_NGUOI_NHA or UPDATED_PHONE_NGUOI_NHA
        defaultBenhAnKhamBenhShouldBeFound("phoneNguoiNha.in=" + DEFAULT_PHONE_NGUOI_NHA + "," + UPDATED_PHONE_NGUOI_NHA);

        // Get all the benhAnKhamBenhList where phoneNguoiNha equals to UPDATED_PHONE_NGUOI_NHA
        defaultBenhAnKhamBenhShouldNotBeFound("phoneNguoiNha.in=" + UPDATED_PHONE_NGUOI_NHA);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByPhoneNguoiNhaIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where phoneNguoiNha is not null
        defaultBenhAnKhamBenhShouldBeFound("phoneNguoiNha.specified=true");

        // Get all the benhAnKhamBenhList where phoneNguoiNha is null
        defaultBenhAnKhamBenhShouldNotBeFound("phoneNguoiNha.specified=false");
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByPhoneNguoiNhaContainsSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where phoneNguoiNha contains DEFAULT_PHONE_NGUOI_NHA
        defaultBenhAnKhamBenhShouldBeFound("phoneNguoiNha.contains=" + DEFAULT_PHONE_NGUOI_NHA);

        // Get all the benhAnKhamBenhList where phoneNguoiNha contains UPDATED_PHONE_NGUOI_NHA
        defaultBenhAnKhamBenhShouldNotBeFound("phoneNguoiNha.contains=" + UPDATED_PHONE_NGUOI_NHA);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByPhoneNguoiNhaNotContainsSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where phoneNguoiNha does not contain DEFAULT_PHONE_NGUOI_NHA
        defaultBenhAnKhamBenhShouldNotBeFound("phoneNguoiNha.doesNotContain=" + DEFAULT_PHONE_NGUOI_NHA);

        // Get all the benhAnKhamBenhList where phoneNguoiNha does not contain UPDATED_PHONE_NGUOI_NHA
        defaultBenhAnKhamBenhShouldBeFound("phoneNguoiNha.doesNotContain=" + UPDATED_PHONE_NGUOI_NHA);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByQuocTichIsEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where quocTich equals to DEFAULT_QUOC_TICH
        defaultBenhAnKhamBenhShouldBeFound("quocTich.equals=" + DEFAULT_QUOC_TICH);

        // Get all the benhAnKhamBenhList where quocTich equals to UPDATED_QUOC_TICH
        defaultBenhAnKhamBenhShouldNotBeFound("quocTich.equals=" + UPDATED_QUOC_TICH);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByQuocTichIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where quocTich not equals to DEFAULT_QUOC_TICH
        defaultBenhAnKhamBenhShouldNotBeFound("quocTich.notEquals=" + DEFAULT_QUOC_TICH);

        // Get all the benhAnKhamBenhList where quocTich not equals to UPDATED_QUOC_TICH
        defaultBenhAnKhamBenhShouldBeFound("quocTich.notEquals=" + UPDATED_QUOC_TICH);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByQuocTichIsInShouldWork() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where quocTich in DEFAULT_QUOC_TICH or UPDATED_QUOC_TICH
        defaultBenhAnKhamBenhShouldBeFound("quocTich.in=" + DEFAULT_QUOC_TICH + "," + UPDATED_QUOC_TICH);

        // Get all the benhAnKhamBenhList where quocTich equals to UPDATED_QUOC_TICH
        defaultBenhAnKhamBenhShouldNotBeFound("quocTich.in=" + UPDATED_QUOC_TICH);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByQuocTichIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where quocTich is not null
        defaultBenhAnKhamBenhShouldBeFound("quocTich.specified=true");

        // Get all the benhAnKhamBenhList where quocTich is null
        defaultBenhAnKhamBenhShouldNotBeFound("quocTich.specified=false");
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByQuocTichContainsSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where quocTich contains DEFAULT_QUOC_TICH
        defaultBenhAnKhamBenhShouldBeFound("quocTich.contains=" + DEFAULT_QUOC_TICH);

        // Get all the benhAnKhamBenhList where quocTich contains UPDATED_QUOC_TICH
        defaultBenhAnKhamBenhShouldNotBeFound("quocTich.contains=" + UPDATED_QUOC_TICH);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByQuocTichNotContainsSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where quocTich does not contain DEFAULT_QUOC_TICH
        defaultBenhAnKhamBenhShouldNotBeFound("quocTich.doesNotContain=" + DEFAULT_QUOC_TICH);

        // Get all the benhAnKhamBenhList where quocTich does not contain UPDATED_QUOC_TICH
        defaultBenhAnKhamBenhShouldBeFound("quocTich.doesNotContain=" + UPDATED_QUOC_TICH);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByTenBenhNhanIsEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where tenBenhNhan equals to DEFAULT_TEN_BENH_NHAN
        defaultBenhAnKhamBenhShouldBeFound("tenBenhNhan.equals=" + DEFAULT_TEN_BENH_NHAN);

        // Get all the benhAnKhamBenhList where tenBenhNhan equals to UPDATED_TEN_BENH_NHAN
        defaultBenhAnKhamBenhShouldNotBeFound("tenBenhNhan.equals=" + UPDATED_TEN_BENH_NHAN);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByTenBenhNhanIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where tenBenhNhan not equals to DEFAULT_TEN_BENH_NHAN
        defaultBenhAnKhamBenhShouldNotBeFound("tenBenhNhan.notEquals=" + DEFAULT_TEN_BENH_NHAN);

        // Get all the benhAnKhamBenhList where tenBenhNhan not equals to UPDATED_TEN_BENH_NHAN
        defaultBenhAnKhamBenhShouldBeFound("tenBenhNhan.notEquals=" + UPDATED_TEN_BENH_NHAN);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByTenBenhNhanIsInShouldWork() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where tenBenhNhan in DEFAULT_TEN_BENH_NHAN or UPDATED_TEN_BENH_NHAN
        defaultBenhAnKhamBenhShouldBeFound("tenBenhNhan.in=" + DEFAULT_TEN_BENH_NHAN + "," + UPDATED_TEN_BENH_NHAN);

        // Get all the benhAnKhamBenhList where tenBenhNhan equals to UPDATED_TEN_BENH_NHAN
        defaultBenhAnKhamBenhShouldNotBeFound("tenBenhNhan.in=" + UPDATED_TEN_BENH_NHAN);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByTenBenhNhanIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where tenBenhNhan is not null
        defaultBenhAnKhamBenhShouldBeFound("tenBenhNhan.specified=true");

        // Get all the benhAnKhamBenhList where tenBenhNhan is null
        defaultBenhAnKhamBenhShouldNotBeFound("tenBenhNhan.specified=false");
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByTenBenhNhanContainsSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where tenBenhNhan contains DEFAULT_TEN_BENH_NHAN
        defaultBenhAnKhamBenhShouldBeFound("tenBenhNhan.contains=" + DEFAULT_TEN_BENH_NHAN);

        // Get all the benhAnKhamBenhList where tenBenhNhan contains UPDATED_TEN_BENH_NHAN
        defaultBenhAnKhamBenhShouldNotBeFound("tenBenhNhan.contains=" + UPDATED_TEN_BENH_NHAN);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByTenBenhNhanNotContainsSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where tenBenhNhan does not contain DEFAULT_TEN_BENH_NHAN
        defaultBenhAnKhamBenhShouldNotBeFound("tenBenhNhan.doesNotContain=" + DEFAULT_TEN_BENH_NHAN);

        // Get all the benhAnKhamBenhList where tenBenhNhan does not contain UPDATED_TEN_BENH_NHAN
        defaultBenhAnKhamBenhShouldBeFound("tenBenhNhan.doesNotContain=" + UPDATED_TEN_BENH_NHAN);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByTenNguoiNhaIsEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where tenNguoiNha equals to DEFAULT_TEN_NGUOI_NHA
        defaultBenhAnKhamBenhShouldBeFound("tenNguoiNha.equals=" + DEFAULT_TEN_NGUOI_NHA);

        // Get all the benhAnKhamBenhList where tenNguoiNha equals to UPDATED_TEN_NGUOI_NHA
        defaultBenhAnKhamBenhShouldNotBeFound("tenNguoiNha.equals=" + UPDATED_TEN_NGUOI_NHA);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByTenNguoiNhaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where tenNguoiNha not equals to DEFAULT_TEN_NGUOI_NHA
        defaultBenhAnKhamBenhShouldNotBeFound("tenNguoiNha.notEquals=" + DEFAULT_TEN_NGUOI_NHA);

        // Get all the benhAnKhamBenhList where tenNguoiNha not equals to UPDATED_TEN_NGUOI_NHA
        defaultBenhAnKhamBenhShouldBeFound("tenNguoiNha.notEquals=" + UPDATED_TEN_NGUOI_NHA);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByTenNguoiNhaIsInShouldWork() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where tenNguoiNha in DEFAULT_TEN_NGUOI_NHA or UPDATED_TEN_NGUOI_NHA
        defaultBenhAnKhamBenhShouldBeFound("tenNguoiNha.in=" + DEFAULT_TEN_NGUOI_NHA + "," + UPDATED_TEN_NGUOI_NHA);

        // Get all the benhAnKhamBenhList where tenNguoiNha equals to UPDATED_TEN_NGUOI_NHA
        defaultBenhAnKhamBenhShouldNotBeFound("tenNguoiNha.in=" + UPDATED_TEN_NGUOI_NHA);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByTenNguoiNhaIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where tenNguoiNha is not null
        defaultBenhAnKhamBenhShouldBeFound("tenNguoiNha.specified=true");

        // Get all the benhAnKhamBenhList where tenNguoiNha is null
        defaultBenhAnKhamBenhShouldNotBeFound("tenNguoiNha.specified=false");
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByTenNguoiNhaContainsSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where tenNguoiNha contains DEFAULT_TEN_NGUOI_NHA
        defaultBenhAnKhamBenhShouldBeFound("tenNguoiNha.contains=" + DEFAULT_TEN_NGUOI_NHA);

        // Get all the benhAnKhamBenhList where tenNguoiNha contains UPDATED_TEN_NGUOI_NHA
        defaultBenhAnKhamBenhShouldNotBeFound("tenNguoiNha.contains=" + UPDATED_TEN_NGUOI_NHA);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByTenNguoiNhaNotContainsSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where tenNguoiNha does not contain DEFAULT_TEN_NGUOI_NHA
        defaultBenhAnKhamBenhShouldNotBeFound("tenNguoiNha.doesNotContain=" + DEFAULT_TEN_NGUOI_NHA);

        // Get all the benhAnKhamBenhList where tenNguoiNha does not contain UPDATED_TEN_NGUOI_NHA
        defaultBenhAnKhamBenhShouldBeFound("tenNguoiNha.doesNotContain=" + UPDATED_TEN_NGUOI_NHA);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByThangTuoiIsEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where thangTuoi equals to DEFAULT_THANG_TUOI
        defaultBenhAnKhamBenhShouldBeFound("thangTuoi.equals=" + DEFAULT_THANG_TUOI);

        // Get all the benhAnKhamBenhList where thangTuoi equals to UPDATED_THANG_TUOI
        defaultBenhAnKhamBenhShouldNotBeFound("thangTuoi.equals=" + UPDATED_THANG_TUOI);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByThangTuoiIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where thangTuoi not equals to DEFAULT_THANG_TUOI
        defaultBenhAnKhamBenhShouldNotBeFound("thangTuoi.notEquals=" + DEFAULT_THANG_TUOI);

        // Get all the benhAnKhamBenhList where thangTuoi not equals to UPDATED_THANG_TUOI
        defaultBenhAnKhamBenhShouldBeFound("thangTuoi.notEquals=" + UPDATED_THANG_TUOI);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByThangTuoiIsInShouldWork() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where thangTuoi in DEFAULT_THANG_TUOI or UPDATED_THANG_TUOI
        defaultBenhAnKhamBenhShouldBeFound("thangTuoi.in=" + DEFAULT_THANG_TUOI + "," + UPDATED_THANG_TUOI);

        // Get all the benhAnKhamBenhList where thangTuoi equals to UPDATED_THANG_TUOI
        defaultBenhAnKhamBenhShouldNotBeFound("thangTuoi.in=" + UPDATED_THANG_TUOI);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByThangTuoiIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where thangTuoi is not null
        defaultBenhAnKhamBenhShouldBeFound("thangTuoi.specified=true");

        // Get all the benhAnKhamBenhList where thangTuoi is null
        defaultBenhAnKhamBenhShouldNotBeFound("thangTuoi.specified=false");
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByThangTuoiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where thangTuoi is greater than or equal to DEFAULT_THANG_TUOI
        defaultBenhAnKhamBenhShouldBeFound("thangTuoi.greaterThanOrEqual=" + DEFAULT_THANG_TUOI);

        // Get all the benhAnKhamBenhList where thangTuoi is greater than or equal to UPDATED_THANG_TUOI
        defaultBenhAnKhamBenhShouldNotBeFound("thangTuoi.greaterThanOrEqual=" + UPDATED_THANG_TUOI);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByThangTuoiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where thangTuoi is less than or equal to DEFAULT_THANG_TUOI
        defaultBenhAnKhamBenhShouldBeFound("thangTuoi.lessThanOrEqual=" + DEFAULT_THANG_TUOI);

        // Get all the benhAnKhamBenhList where thangTuoi is less than or equal to SMALLER_THANG_TUOI
        defaultBenhAnKhamBenhShouldNotBeFound("thangTuoi.lessThanOrEqual=" + SMALLER_THANG_TUOI);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByThangTuoiIsLessThanSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where thangTuoi is less than DEFAULT_THANG_TUOI
        defaultBenhAnKhamBenhShouldNotBeFound("thangTuoi.lessThan=" + DEFAULT_THANG_TUOI);

        // Get all the benhAnKhamBenhList where thangTuoi is less than UPDATED_THANG_TUOI
        defaultBenhAnKhamBenhShouldBeFound("thangTuoi.lessThan=" + UPDATED_THANG_TUOI);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByThangTuoiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where thangTuoi is greater than DEFAULT_THANG_TUOI
        defaultBenhAnKhamBenhShouldNotBeFound("thangTuoi.greaterThan=" + DEFAULT_THANG_TUOI);

        // Get all the benhAnKhamBenhList where thangTuoi is greater than SMALLER_THANG_TUOI
        defaultBenhAnKhamBenhShouldBeFound("thangTuoi.greaterThan=" + SMALLER_THANG_TUOI);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByThoiGianTiepNhanIsEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where thoiGianTiepNhan equals to DEFAULT_THOI_GIAN_TIEP_NHAN
        defaultBenhAnKhamBenhShouldBeFound("thoiGianTiepNhan.equals=" + DEFAULT_THOI_GIAN_TIEP_NHAN);

        // Get all the benhAnKhamBenhList where thoiGianTiepNhan equals to UPDATED_THOI_GIAN_TIEP_NHAN
        defaultBenhAnKhamBenhShouldNotBeFound("thoiGianTiepNhan.equals=" + UPDATED_THOI_GIAN_TIEP_NHAN);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByThoiGianTiepNhanIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where thoiGianTiepNhan not equals to DEFAULT_THOI_GIAN_TIEP_NHAN
        defaultBenhAnKhamBenhShouldNotBeFound("thoiGianTiepNhan.notEquals=" + DEFAULT_THOI_GIAN_TIEP_NHAN);

        // Get all the benhAnKhamBenhList where thoiGianTiepNhan not equals to UPDATED_THOI_GIAN_TIEP_NHAN
        defaultBenhAnKhamBenhShouldBeFound("thoiGianTiepNhan.notEquals=" + UPDATED_THOI_GIAN_TIEP_NHAN);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByThoiGianTiepNhanIsInShouldWork() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where thoiGianTiepNhan in DEFAULT_THOI_GIAN_TIEP_NHAN or UPDATED_THOI_GIAN_TIEP_NHAN
        defaultBenhAnKhamBenhShouldBeFound("thoiGianTiepNhan.in=" + DEFAULT_THOI_GIAN_TIEP_NHAN + "," + UPDATED_THOI_GIAN_TIEP_NHAN);

        // Get all the benhAnKhamBenhList where thoiGianTiepNhan equals to UPDATED_THOI_GIAN_TIEP_NHAN
        defaultBenhAnKhamBenhShouldNotBeFound("thoiGianTiepNhan.in=" + UPDATED_THOI_GIAN_TIEP_NHAN);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByThoiGianTiepNhanIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where thoiGianTiepNhan is not null
        defaultBenhAnKhamBenhShouldBeFound("thoiGianTiepNhan.specified=true");

        // Get all the benhAnKhamBenhList where thoiGianTiepNhan is null
        defaultBenhAnKhamBenhShouldNotBeFound("thoiGianTiepNhan.specified=false");
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByThoiGianTiepNhanIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where thoiGianTiepNhan is greater than or equal to DEFAULT_THOI_GIAN_TIEP_NHAN
        defaultBenhAnKhamBenhShouldBeFound("thoiGianTiepNhan.greaterThanOrEqual=" + DEFAULT_THOI_GIAN_TIEP_NHAN);

        // Get all the benhAnKhamBenhList where thoiGianTiepNhan is greater than or equal to UPDATED_THOI_GIAN_TIEP_NHAN
        defaultBenhAnKhamBenhShouldNotBeFound("thoiGianTiepNhan.greaterThanOrEqual=" + UPDATED_THOI_GIAN_TIEP_NHAN);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByThoiGianTiepNhanIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where thoiGianTiepNhan is less than or equal to DEFAULT_THOI_GIAN_TIEP_NHAN
        defaultBenhAnKhamBenhShouldBeFound("thoiGianTiepNhan.lessThanOrEqual=" + DEFAULT_THOI_GIAN_TIEP_NHAN);

        // Get all the benhAnKhamBenhList where thoiGianTiepNhan is less than or equal to SMALLER_THOI_GIAN_TIEP_NHAN
        defaultBenhAnKhamBenhShouldNotBeFound("thoiGianTiepNhan.lessThanOrEqual=" + SMALLER_THOI_GIAN_TIEP_NHAN);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByThoiGianTiepNhanIsLessThanSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where thoiGianTiepNhan is less than DEFAULT_THOI_GIAN_TIEP_NHAN
        defaultBenhAnKhamBenhShouldNotBeFound("thoiGianTiepNhan.lessThan=" + DEFAULT_THOI_GIAN_TIEP_NHAN);

        // Get all the benhAnKhamBenhList where thoiGianTiepNhan is less than UPDATED_THOI_GIAN_TIEP_NHAN
        defaultBenhAnKhamBenhShouldBeFound("thoiGianTiepNhan.lessThan=" + UPDATED_THOI_GIAN_TIEP_NHAN);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByThoiGianTiepNhanIsGreaterThanSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where thoiGianTiepNhan is greater than DEFAULT_THOI_GIAN_TIEP_NHAN
        defaultBenhAnKhamBenhShouldNotBeFound("thoiGianTiepNhan.greaterThan=" + DEFAULT_THOI_GIAN_TIEP_NHAN);

        // Get all the benhAnKhamBenhList where thoiGianTiepNhan is greater than SMALLER_THOI_GIAN_TIEP_NHAN
        defaultBenhAnKhamBenhShouldBeFound("thoiGianTiepNhan.greaterThan=" + SMALLER_THOI_GIAN_TIEP_NHAN);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByTuoiIsEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where tuoi equals to DEFAULT_TUOI
        defaultBenhAnKhamBenhShouldBeFound("tuoi.equals=" + DEFAULT_TUOI);

        // Get all the benhAnKhamBenhList where tuoi equals to UPDATED_TUOI
        defaultBenhAnKhamBenhShouldNotBeFound("tuoi.equals=" + UPDATED_TUOI);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByTuoiIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where tuoi not equals to DEFAULT_TUOI
        defaultBenhAnKhamBenhShouldNotBeFound("tuoi.notEquals=" + DEFAULT_TUOI);

        // Get all the benhAnKhamBenhList where tuoi not equals to UPDATED_TUOI
        defaultBenhAnKhamBenhShouldBeFound("tuoi.notEquals=" + UPDATED_TUOI);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByTuoiIsInShouldWork() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where tuoi in DEFAULT_TUOI or UPDATED_TUOI
        defaultBenhAnKhamBenhShouldBeFound("tuoi.in=" + DEFAULT_TUOI + "," + UPDATED_TUOI);

        // Get all the benhAnKhamBenhList where tuoi equals to UPDATED_TUOI
        defaultBenhAnKhamBenhShouldNotBeFound("tuoi.in=" + UPDATED_TUOI);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByTuoiIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where tuoi is not null
        defaultBenhAnKhamBenhShouldBeFound("tuoi.specified=true");

        // Get all the benhAnKhamBenhList where tuoi is null
        defaultBenhAnKhamBenhShouldNotBeFound("tuoi.specified=false");
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByTuoiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where tuoi is greater than or equal to DEFAULT_TUOI
        defaultBenhAnKhamBenhShouldBeFound("tuoi.greaterThanOrEqual=" + DEFAULT_TUOI);

        // Get all the benhAnKhamBenhList where tuoi is greater than or equal to UPDATED_TUOI
        defaultBenhAnKhamBenhShouldNotBeFound("tuoi.greaterThanOrEqual=" + UPDATED_TUOI);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByTuoiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where tuoi is less than or equal to DEFAULT_TUOI
        defaultBenhAnKhamBenhShouldBeFound("tuoi.lessThanOrEqual=" + DEFAULT_TUOI);

        // Get all the benhAnKhamBenhList where tuoi is less than or equal to SMALLER_TUOI
        defaultBenhAnKhamBenhShouldNotBeFound("tuoi.lessThanOrEqual=" + SMALLER_TUOI);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByTuoiIsLessThanSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where tuoi is less than DEFAULT_TUOI
        defaultBenhAnKhamBenhShouldNotBeFound("tuoi.lessThan=" + DEFAULT_TUOI);

        // Get all the benhAnKhamBenhList where tuoi is less than UPDATED_TUOI
        defaultBenhAnKhamBenhShouldBeFound("tuoi.lessThan=" + UPDATED_TUOI);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByTuoiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where tuoi is greater than DEFAULT_TUOI
        defaultBenhAnKhamBenhShouldNotBeFound("tuoi.greaterThan=" + DEFAULT_TUOI);

        // Get all the benhAnKhamBenhList where tuoi is greater than SMALLER_TUOI
        defaultBenhAnKhamBenhShouldBeFound("tuoi.greaterThan=" + SMALLER_TUOI);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByUuTienIsEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where uuTien equals to DEFAULT_UU_TIEN
        defaultBenhAnKhamBenhShouldBeFound("uuTien.equals=" + DEFAULT_UU_TIEN);

        // Get all the benhAnKhamBenhList where uuTien equals to UPDATED_UU_TIEN
        defaultBenhAnKhamBenhShouldNotBeFound("uuTien.equals=" + UPDATED_UU_TIEN);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByUuTienIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where uuTien not equals to DEFAULT_UU_TIEN
        defaultBenhAnKhamBenhShouldNotBeFound("uuTien.notEquals=" + DEFAULT_UU_TIEN);

        // Get all the benhAnKhamBenhList where uuTien not equals to UPDATED_UU_TIEN
        defaultBenhAnKhamBenhShouldBeFound("uuTien.notEquals=" + UPDATED_UU_TIEN);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByUuTienIsInShouldWork() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where uuTien in DEFAULT_UU_TIEN or UPDATED_UU_TIEN
        defaultBenhAnKhamBenhShouldBeFound("uuTien.in=" + DEFAULT_UU_TIEN + "," + UPDATED_UU_TIEN);

        // Get all the benhAnKhamBenhList where uuTien equals to UPDATED_UU_TIEN
        defaultBenhAnKhamBenhShouldNotBeFound("uuTien.in=" + UPDATED_UU_TIEN);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByUuTienIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where uuTien is not null
        defaultBenhAnKhamBenhShouldBeFound("uuTien.specified=true");

        // Get all the benhAnKhamBenhList where uuTien is null
        defaultBenhAnKhamBenhShouldNotBeFound("uuTien.specified=false");
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByUuTienIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where uuTien is greater than or equal to DEFAULT_UU_TIEN
        defaultBenhAnKhamBenhShouldBeFound("uuTien.greaterThanOrEqual=" + DEFAULT_UU_TIEN);

        // Get all the benhAnKhamBenhList where uuTien is greater than or equal to UPDATED_UU_TIEN
        defaultBenhAnKhamBenhShouldNotBeFound("uuTien.greaterThanOrEqual=" + UPDATED_UU_TIEN);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByUuTienIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where uuTien is less than or equal to DEFAULT_UU_TIEN
        defaultBenhAnKhamBenhShouldBeFound("uuTien.lessThanOrEqual=" + DEFAULT_UU_TIEN);

        // Get all the benhAnKhamBenhList where uuTien is less than or equal to SMALLER_UU_TIEN
        defaultBenhAnKhamBenhShouldNotBeFound("uuTien.lessThanOrEqual=" + SMALLER_UU_TIEN);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByUuTienIsLessThanSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where uuTien is less than DEFAULT_UU_TIEN
        defaultBenhAnKhamBenhShouldNotBeFound("uuTien.lessThan=" + DEFAULT_UU_TIEN);

        // Get all the benhAnKhamBenhList where uuTien is less than UPDATED_UU_TIEN
        defaultBenhAnKhamBenhShouldBeFound("uuTien.lessThan=" + UPDATED_UU_TIEN);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByUuTienIsGreaterThanSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where uuTien is greater than DEFAULT_UU_TIEN
        defaultBenhAnKhamBenhShouldNotBeFound("uuTien.greaterThan=" + DEFAULT_UU_TIEN);

        // Get all the benhAnKhamBenhList where uuTien is greater than SMALLER_UU_TIEN
        defaultBenhAnKhamBenhShouldBeFound("uuTien.greaterThan=" + SMALLER_UU_TIEN);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByUuidIsEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where uuid equals to DEFAULT_UUID
        defaultBenhAnKhamBenhShouldBeFound("uuid.equals=" + DEFAULT_UUID);

        // Get all the benhAnKhamBenhList where uuid equals to UPDATED_UUID
        defaultBenhAnKhamBenhShouldNotBeFound("uuid.equals=" + UPDATED_UUID);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByUuidIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where uuid not equals to DEFAULT_UUID
        defaultBenhAnKhamBenhShouldNotBeFound("uuid.notEquals=" + DEFAULT_UUID);

        // Get all the benhAnKhamBenhList where uuid not equals to UPDATED_UUID
        defaultBenhAnKhamBenhShouldBeFound("uuid.notEquals=" + UPDATED_UUID);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByUuidIsInShouldWork() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where uuid in DEFAULT_UUID or UPDATED_UUID
        defaultBenhAnKhamBenhShouldBeFound("uuid.in=" + DEFAULT_UUID + "," + UPDATED_UUID);

        // Get all the benhAnKhamBenhList where uuid equals to UPDATED_UUID
        defaultBenhAnKhamBenhShouldNotBeFound("uuid.in=" + UPDATED_UUID);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByUuidIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where uuid is not null
        defaultBenhAnKhamBenhShouldBeFound("uuid.specified=true");

        // Get all the benhAnKhamBenhList where uuid is null
        defaultBenhAnKhamBenhShouldNotBeFound("uuid.specified=false");
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByUuidContainsSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where uuid contains DEFAULT_UUID
        defaultBenhAnKhamBenhShouldBeFound("uuid.contains=" + DEFAULT_UUID);

        // Get all the benhAnKhamBenhList where uuid contains UPDATED_UUID
        defaultBenhAnKhamBenhShouldNotBeFound("uuid.contains=" + UPDATED_UUID);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByUuidNotContainsSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where uuid does not contain DEFAULT_UUID
        defaultBenhAnKhamBenhShouldNotBeFound("uuid.doesNotContain=" + DEFAULT_UUID);

        // Get all the benhAnKhamBenhList where uuid does not contain UPDATED_UUID
        defaultBenhAnKhamBenhShouldBeFound("uuid.doesNotContain=" + UPDATED_UUID);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByTrangThaiIsEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where trangThai equals to DEFAULT_TRANG_THAI
        defaultBenhAnKhamBenhShouldBeFound("trangThai.equals=" + DEFAULT_TRANG_THAI);

        // Get all the benhAnKhamBenhList where trangThai equals to UPDATED_TRANG_THAI
        defaultBenhAnKhamBenhShouldNotBeFound("trangThai.equals=" + UPDATED_TRANG_THAI);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByTrangThaiIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where trangThai not equals to DEFAULT_TRANG_THAI
        defaultBenhAnKhamBenhShouldNotBeFound("trangThai.notEquals=" + DEFAULT_TRANG_THAI);

        // Get all the benhAnKhamBenhList where trangThai not equals to UPDATED_TRANG_THAI
        defaultBenhAnKhamBenhShouldBeFound("trangThai.notEquals=" + UPDATED_TRANG_THAI);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByTrangThaiIsInShouldWork() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where trangThai in DEFAULT_TRANG_THAI or UPDATED_TRANG_THAI
        defaultBenhAnKhamBenhShouldBeFound("trangThai.in=" + DEFAULT_TRANG_THAI + "," + UPDATED_TRANG_THAI);

        // Get all the benhAnKhamBenhList where trangThai equals to UPDATED_TRANG_THAI
        defaultBenhAnKhamBenhShouldNotBeFound("trangThai.in=" + UPDATED_TRANG_THAI);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByTrangThaiIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where trangThai is not null
        defaultBenhAnKhamBenhShouldBeFound("trangThai.specified=true");

        // Get all the benhAnKhamBenhList where trangThai is null
        defaultBenhAnKhamBenhShouldNotBeFound("trangThai.specified=false");
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByTrangThaiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where trangThai is greater than or equal to DEFAULT_TRANG_THAI
        defaultBenhAnKhamBenhShouldBeFound("trangThai.greaterThanOrEqual=" + DEFAULT_TRANG_THAI);

        // Get all the benhAnKhamBenhList where trangThai is greater than or equal to UPDATED_TRANG_THAI
        defaultBenhAnKhamBenhShouldNotBeFound("trangThai.greaterThanOrEqual=" + UPDATED_TRANG_THAI);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByTrangThaiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where trangThai is less than or equal to DEFAULT_TRANG_THAI
        defaultBenhAnKhamBenhShouldBeFound("trangThai.lessThanOrEqual=" + DEFAULT_TRANG_THAI);

        // Get all the benhAnKhamBenhList where trangThai is less than or equal to SMALLER_TRANG_THAI
        defaultBenhAnKhamBenhShouldNotBeFound("trangThai.lessThanOrEqual=" + SMALLER_TRANG_THAI);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByTrangThaiIsLessThanSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where trangThai is less than DEFAULT_TRANG_THAI
        defaultBenhAnKhamBenhShouldNotBeFound("trangThai.lessThan=" + DEFAULT_TRANG_THAI);

        // Get all the benhAnKhamBenhList where trangThai is less than UPDATED_TRANG_THAI
        defaultBenhAnKhamBenhShouldBeFound("trangThai.lessThan=" + UPDATED_TRANG_THAI);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByTrangThaiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where trangThai is greater than DEFAULT_TRANG_THAI
        defaultBenhAnKhamBenhShouldNotBeFound("trangThai.greaterThan=" + DEFAULT_TRANG_THAI);

        // Get all the benhAnKhamBenhList where trangThai is greater than SMALLER_TRANG_THAI
        defaultBenhAnKhamBenhShouldBeFound("trangThai.greaterThan=" + SMALLER_TRANG_THAI);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByMaKhoaHoanTatKhamIsEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where maKhoaHoanTatKham equals to DEFAULT_MA_KHOA_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldBeFound("maKhoaHoanTatKham.equals=" + DEFAULT_MA_KHOA_HOAN_TAT_KHAM);

        // Get all the benhAnKhamBenhList where maKhoaHoanTatKham equals to UPDATED_MA_KHOA_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldNotBeFound("maKhoaHoanTatKham.equals=" + UPDATED_MA_KHOA_HOAN_TAT_KHAM);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByMaKhoaHoanTatKhamIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where maKhoaHoanTatKham not equals to DEFAULT_MA_KHOA_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldNotBeFound("maKhoaHoanTatKham.notEquals=" + DEFAULT_MA_KHOA_HOAN_TAT_KHAM);

        // Get all the benhAnKhamBenhList where maKhoaHoanTatKham not equals to UPDATED_MA_KHOA_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldBeFound("maKhoaHoanTatKham.notEquals=" + UPDATED_MA_KHOA_HOAN_TAT_KHAM);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByMaKhoaHoanTatKhamIsInShouldWork() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where maKhoaHoanTatKham in DEFAULT_MA_KHOA_HOAN_TAT_KHAM or UPDATED_MA_KHOA_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldBeFound("maKhoaHoanTatKham.in=" + DEFAULT_MA_KHOA_HOAN_TAT_KHAM + "," + UPDATED_MA_KHOA_HOAN_TAT_KHAM);

        // Get all the benhAnKhamBenhList where maKhoaHoanTatKham equals to UPDATED_MA_KHOA_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldNotBeFound("maKhoaHoanTatKham.in=" + UPDATED_MA_KHOA_HOAN_TAT_KHAM);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByMaKhoaHoanTatKhamIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where maKhoaHoanTatKham is not null
        defaultBenhAnKhamBenhShouldBeFound("maKhoaHoanTatKham.specified=true");

        // Get all the benhAnKhamBenhList where maKhoaHoanTatKham is null
        defaultBenhAnKhamBenhShouldNotBeFound("maKhoaHoanTatKham.specified=false");
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByMaKhoaHoanTatKhamIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where maKhoaHoanTatKham is greater than or equal to DEFAULT_MA_KHOA_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldBeFound("maKhoaHoanTatKham.greaterThanOrEqual=" + DEFAULT_MA_KHOA_HOAN_TAT_KHAM);

        // Get all the benhAnKhamBenhList where maKhoaHoanTatKham is greater than or equal to UPDATED_MA_KHOA_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldNotBeFound("maKhoaHoanTatKham.greaterThanOrEqual=" + UPDATED_MA_KHOA_HOAN_TAT_KHAM);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByMaKhoaHoanTatKhamIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where maKhoaHoanTatKham is less than or equal to DEFAULT_MA_KHOA_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldBeFound("maKhoaHoanTatKham.lessThanOrEqual=" + DEFAULT_MA_KHOA_HOAN_TAT_KHAM);

        // Get all the benhAnKhamBenhList where maKhoaHoanTatKham is less than or equal to SMALLER_MA_KHOA_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldNotBeFound("maKhoaHoanTatKham.lessThanOrEqual=" + SMALLER_MA_KHOA_HOAN_TAT_KHAM);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByMaKhoaHoanTatKhamIsLessThanSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where maKhoaHoanTatKham is less than DEFAULT_MA_KHOA_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldNotBeFound("maKhoaHoanTatKham.lessThan=" + DEFAULT_MA_KHOA_HOAN_TAT_KHAM);

        // Get all the benhAnKhamBenhList where maKhoaHoanTatKham is less than UPDATED_MA_KHOA_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldBeFound("maKhoaHoanTatKham.lessThan=" + UPDATED_MA_KHOA_HOAN_TAT_KHAM);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByMaKhoaHoanTatKhamIsGreaterThanSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where maKhoaHoanTatKham is greater than DEFAULT_MA_KHOA_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldNotBeFound("maKhoaHoanTatKham.greaterThan=" + DEFAULT_MA_KHOA_HOAN_TAT_KHAM);

        // Get all the benhAnKhamBenhList where maKhoaHoanTatKham is greater than SMALLER_MA_KHOA_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldBeFound("maKhoaHoanTatKham.greaterThan=" + SMALLER_MA_KHOA_HOAN_TAT_KHAM);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByMaPhongHoanTatKhamIsEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where maPhongHoanTatKham equals to DEFAULT_MA_PHONG_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldBeFound("maPhongHoanTatKham.equals=" + DEFAULT_MA_PHONG_HOAN_TAT_KHAM);

        // Get all the benhAnKhamBenhList where maPhongHoanTatKham equals to UPDATED_MA_PHONG_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldNotBeFound("maPhongHoanTatKham.equals=" + UPDATED_MA_PHONG_HOAN_TAT_KHAM);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByMaPhongHoanTatKhamIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where maPhongHoanTatKham not equals to DEFAULT_MA_PHONG_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldNotBeFound("maPhongHoanTatKham.notEquals=" + DEFAULT_MA_PHONG_HOAN_TAT_KHAM);

        // Get all the benhAnKhamBenhList where maPhongHoanTatKham not equals to UPDATED_MA_PHONG_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldBeFound("maPhongHoanTatKham.notEquals=" + UPDATED_MA_PHONG_HOAN_TAT_KHAM);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByMaPhongHoanTatKhamIsInShouldWork() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where maPhongHoanTatKham in DEFAULT_MA_PHONG_HOAN_TAT_KHAM or UPDATED_MA_PHONG_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldBeFound("maPhongHoanTatKham.in=" + DEFAULT_MA_PHONG_HOAN_TAT_KHAM + "," + UPDATED_MA_PHONG_HOAN_TAT_KHAM);

        // Get all the benhAnKhamBenhList where maPhongHoanTatKham equals to UPDATED_MA_PHONG_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldNotBeFound("maPhongHoanTatKham.in=" + UPDATED_MA_PHONG_HOAN_TAT_KHAM);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByMaPhongHoanTatKhamIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where maPhongHoanTatKham is not null
        defaultBenhAnKhamBenhShouldBeFound("maPhongHoanTatKham.specified=true");

        // Get all the benhAnKhamBenhList where maPhongHoanTatKham is null
        defaultBenhAnKhamBenhShouldNotBeFound("maPhongHoanTatKham.specified=false");
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByMaPhongHoanTatKhamIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where maPhongHoanTatKham is greater than or equal to DEFAULT_MA_PHONG_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldBeFound("maPhongHoanTatKham.greaterThanOrEqual=" + DEFAULT_MA_PHONG_HOAN_TAT_KHAM);

        // Get all the benhAnKhamBenhList where maPhongHoanTatKham is greater than or equal to UPDATED_MA_PHONG_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldNotBeFound("maPhongHoanTatKham.greaterThanOrEqual=" + UPDATED_MA_PHONG_HOAN_TAT_KHAM);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByMaPhongHoanTatKhamIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where maPhongHoanTatKham is less than or equal to DEFAULT_MA_PHONG_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldBeFound("maPhongHoanTatKham.lessThanOrEqual=" + DEFAULT_MA_PHONG_HOAN_TAT_KHAM);

        // Get all the benhAnKhamBenhList where maPhongHoanTatKham is less than or equal to SMALLER_MA_PHONG_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldNotBeFound("maPhongHoanTatKham.lessThanOrEqual=" + SMALLER_MA_PHONG_HOAN_TAT_KHAM);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByMaPhongHoanTatKhamIsLessThanSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where maPhongHoanTatKham is less than DEFAULT_MA_PHONG_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldNotBeFound("maPhongHoanTatKham.lessThan=" + DEFAULT_MA_PHONG_HOAN_TAT_KHAM);

        // Get all the benhAnKhamBenhList where maPhongHoanTatKham is less than UPDATED_MA_PHONG_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldBeFound("maPhongHoanTatKham.lessThan=" + UPDATED_MA_PHONG_HOAN_TAT_KHAM);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByMaPhongHoanTatKhamIsGreaterThanSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where maPhongHoanTatKham is greater than DEFAULT_MA_PHONG_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldNotBeFound("maPhongHoanTatKham.greaterThan=" + DEFAULT_MA_PHONG_HOAN_TAT_KHAM);

        // Get all the benhAnKhamBenhList where maPhongHoanTatKham is greater than SMALLER_MA_PHONG_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldBeFound("maPhongHoanTatKham.greaterThan=" + SMALLER_MA_PHONG_HOAN_TAT_KHAM);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByTenKhoaHoanTatKhamIsEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where tenKhoaHoanTatKham equals to DEFAULT_TEN_KHOA_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldBeFound("tenKhoaHoanTatKham.equals=" + DEFAULT_TEN_KHOA_HOAN_TAT_KHAM);

        // Get all the benhAnKhamBenhList where tenKhoaHoanTatKham equals to UPDATED_TEN_KHOA_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldNotBeFound("tenKhoaHoanTatKham.equals=" + UPDATED_TEN_KHOA_HOAN_TAT_KHAM);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByTenKhoaHoanTatKhamIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where tenKhoaHoanTatKham not equals to DEFAULT_TEN_KHOA_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldNotBeFound("tenKhoaHoanTatKham.notEquals=" + DEFAULT_TEN_KHOA_HOAN_TAT_KHAM);

        // Get all the benhAnKhamBenhList where tenKhoaHoanTatKham not equals to UPDATED_TEN_KHOA_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldBeFound("tenKhoaHoanTatKham.notEquals=" + UPDATED_TEN_KHOA_HOAN_TAT_KHAM);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByTenKhoaHoanTatKhamIsInShouldWork() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where tenKhoaHoanTatKham in DEFAULT_TEN_KHOA_HOAN_TAT_KHAM or UPDATED_TEN_KHOA_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldBeFound("tenKhoaHoanTatKham.in=" + DEFAULT_TEN_KHOA_HOAN_TAT_KHAM + "," + UPDATED_TEN_KHOA_HOAN_TAT_KHAM);

        // Get all the benhAnKhamBenhList where tenKhoaHoanTatKham equals to UPDATED_TEN_KHOA_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldNotBeFound("tenKhoaHoanTatKham.in=" + UPDATED_TEN_KHOA_HOAN_TAT_KHAM);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByTenKhoaHoanTatKhamIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where tenKhoaHoanTatKham is not null
        defaultBenhAnKhamBenhShouldBeFound("tenKhoaHoanTatKham.specified=true");

        // Get all the benhAnKhamBenhList where tenKhoaHoanTatKham is null
        defaultBenhAnKhamBenhShouldNotBeFound("tenKhoaHoanTatKham.specified=false");
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByTenKhoaHoanTatKhamContainsSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where tenKhoaHoanTatKham contains DEFAULT_TEN_KHOA_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldBeFound("tenKhoaHoanTatKham.contains=" + DEFAULT_TEN_KHOA_HOAN_TAT_KHAM);

        // Get all the benhAnKhamBenhList where tenKhoaHoanTatKham contains UPDATED_TEN_KHOA_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldNotBeFound("tenKhoaHoanTatKham.contains=" + UPDATED_TEN_KHOA_HOAN_TAT_KHAM);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByTenKhoaHoanTatKhamNotContainsSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where tenKhoaHoanTatKham does not contain DEFAULT_TEN_KHOA_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldNotBeFound("tenKhoaHoanTatKham.doesNotContain=" + DEFAULT_TEN_KHOA_HOAN_TAT_KHAM);

        // Get all the benhAnKhamBenhList where tenKhoaHoanTatKham does not contain UPDATED_TEN_KHOA_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldBeFound("tenKhoaHoanTatKham.doesNotContain=" + UPDATED_TEN_KHOA_HOAN_TAT_KHAM);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByTenPhongHoanTatKhamIsEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where tenPhongHoanTatKham equals to DEFAULT_TEN_PHONG_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldBeFound("tenPhongHoanTatKham.equals=" + DEFAULT_TEN_PHONG_HOAN_TAT_KHAM);

        // Get all the benhAnKhamBenhList where tenPhongHoanTatKham equals to UPDATED_TEN_PHONG_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldNotBeFound("tenPhongHoanTatKham.equals=" + UPDATED_TEN_PHONG_HOAN_TAT_KHAM);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByTenPhongHoanTatKhamIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where tenPhongHoanTatKham not equals to DEFAULT_TEN_PHONG_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldNotBeFound("tenPhongHoanTatKham.notEquals=" + DEFAULT_TEN_PHONG_HOAN_TAT_KHAM);

        // Get all the benhAnKhamBenhList where tenPhongHoanTatKham not equals to UPDATED_TEN_PHONG_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldBeFound("tenPhongHoanTatKham.notEquals=" + UPDATED_TEN_PHONG_HOAN_TAT_KHAM);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByTenPhongHoanTatKhamIsInShouldWork() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where tenPhongHoanTatKham in DEFAULT_TEN_PHONG_HOAN_TAT_KHAM or UPDATED_TEN_PHONG_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldBeFound("tenPhongHoanTatKham.in=" + DEFAULT_TEN_PHONG_HOAN_TAT_KHAM + "," + UPDATED_TEN_PHONG_HOAN_TAT_KHAM);

        // Get all the benhAnKhamBenhList where tenPhongHoanTatKham equals to UPDATED_TEN_PHONG_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldNotBeFound("tenPhongHoanTatKham.in=" + UPDATED_TEN_PHONG_HOAN_TAT_KHAM);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByTenPhongHoanTatKhamIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where tenPhongHoanTatKham is not null
        defaultBenhAnKhamBenhShouldBeFound("tenPhongHoanTatKham.specified=true");

        // Get all the benhAnKhamBenhList where tenPhongHoanTatKham is null
        defaultBenhAnKhamBenhShouldNotBeFound("tenPhongHoanTatKham.specified=false");
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByTenPhongHoanTatKhamContainsSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where tenPhongHoanTatKham contains DEFAULT_TEN_PHONG_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldBeFound("tenPhongHoanTatKham.contains=" + DEFAULT_TEN_PHONG_HOAN_TAT_KHAM);

        // Get all the benhAnKhamBenhList where tenPhongHoanTatKham contains UPDATED_TEN_PHONG_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldNotBeFound("tenPhongHoanTatKham.contains=" + UPDATED_TEN_PHONG_HOAN_TAT_KHAM);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByTenPhongHoanTatKhamNotContainsSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where tenPhongHoanTatKham does not contain DEFAULT_TEN_PHONG_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldNotBeFound("tenPhongHoanTatKham.doesNotContain=" + DEFAULT_TEN_PHONG_HOAN_TAT_KHAM);

        // Get all the benhAnKhamBenhList where tenPhongHoanTatKham does not contain UPDATED_TEN_PHONG_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldBeFound("tenPhongHoanTatKham.doesNotContain=" + UPDATED_TEN_PHONG_HOAN_TAT_KHAM);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByThoiGianHoanTatKhamIsEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where thoiGianHoanTatKham equals to DEFAULT_THOI_GIAN_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldBeFound("thoiGianHoanTatKham.equals=" + DEFAULT_THOI_GIAN_HOAN_TAT_KHAM);

        // Get all the benhAnKhamBenhList where thoiGianHoanTatKham equals to UPDATED_THOI_GIAN_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldNotBeFound("thoiGianHoanTatKham.equals=" + UPDATED_THOI_GIAN_HOAN_TAT_KHAM);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByThoiGianHoanTatKhamIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where thoiGianHoanTatKham not equals to DEFAULT_THOI_GIAN_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldNotBeFound("thoiGianHoanTatKham.notEquals=" + DEFAULT_THOI_GIAN_HOAN_TAT_KHAM);

        // Get all the benhAnKhamBenhList where thoiGianHoanTatKham not equals to UPDATED_THOI_GIAN_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldBeFound("thoiGianHoanTatKham.notEquals=" + UPDATED_THOI_GIAN_HOAN_TAT_KHAM);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByThoiGianHoanTatKhamIsInShouldWork() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where thoiGianHoanTatKham in DEFAULT_THOI_GIAN_HOAN_TAT_KHAM or UPDATED_THOI_GIAN_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldBeFound("thoiGianHoanTatKham.in=" + DEFAULT_THOI_GIAN_HOAN_TAT_KHAM + "," + UPDATED_THOI_GIAN_HOAN_TAT_KHAM);

        // Get all the benhAnKhamBenhList where thoiGianHoanTatKham equals to UPDATED_THOI_GIAN_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldNotBeFound("thoiGianHoanTatKham.in=" + UPDATED_THOI_GIAN_HOAN_TAT_KHAM);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByThoiGianHoanTatKhamIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where thoiGianHoanTatKham is not null
        defaultBenhAnKhamBenhShouldBeFound("thoiGianHoanTatKham.specified=true");

        // Get all the benhAnKhamBenhList where thoiGianHoanTatKham is null
        defaultBenhAnKhamBenhShouldNotBeFound("thoiGianHoanTatKham.specified=false");
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByThoiGianHoanTatKhamIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where thoiGianHoanTatKham is greater than or equal to DEFAULT_THOI_GIAN_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldBeFound("thoiGianHoanTatKham.greaterThanOrEqual=" + DEFAULT_THOI_GIAN_HOAN_TAT_KHAM);

        // Get all the benhAnKhamBenhList where thoiGianHoanTatKham is greater than or equal to UPDATED_THOI_GIAN_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldNotBeFound("thoiGianHoanTatKham.greaterThanOrEqual=" + UPDATED_THOI_GIAN_HOAN_TAT_KHAM);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByThoiGianHoanTatKhamIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where thoiGianHoanTatKham is less than or equal to DEFAULT_THOI_GIAN_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldBeFound("thoiGianHoanTatKham.lessThanOrEqual=" + DEFAULT_THOI_GIAN_HOAN_TAT_KHAM);

        // Get all the benhAnKhamBenhList where thoiGianHoanTatKham is less than or equal to SMALLER_THOI_GIAN_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldNotBeFound("thoiGianHoanTatKham.lessThanOrEqual=" + SMALLER_THOI_GIAN_HOAN_TAT_KHAM);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByThoiGianHoanTatKhamIsLessThanSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where thoiGianHoanTatKham is less than DEFAULT_THOI_GIAN_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldNotBeFound("thoiGianHoanTatKham.lessThan=" + DEFAULT_THOI_GIAN_HOAN_TAT_KHAM);

        // Get all the benhAnKhamBenhList where thoiGianHoanTatKham is less than UPDATED_THOI_GIAN_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldBeFound("thoiGianHoanTatKham.lessThan=" + UPDATED_THOI_GIAN_HOAN_TAT_KHAM);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByThoiGianHoanTatKhamIsGreaterThanSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where thoiGianHoanTatKham is greater than DEFAULT_THOI_GIAN_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldNotBeFound("thoiGianHoanTatKham.greaterThan=" + DEFAULT_THOI_GIAN_HOAN_TAT_KHAM);

        // Get all the benhAnKhamBenhList where thoiGianHoanTatKham is greater than SMALLER_THOI_GIAN_HOAN_TAT_KHAM
        defaultBenhAnKhamBenhShouldBeFound("thoiGianHoanTatKham.greaterThan=" + SMALLER_THOI_GIAN_HOAN_TAT_KHAM);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsBySoBenhAnIsEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where soBenhAn equals to DEFAULT_SO_BENH_AN
        defaultBenhAnKhamBenhShouldBeFound("soBenhAn.equals=" + DEFAULT_SO_BENH_AN);

        // Get all the benhAnKhamBenhList where soBenhAn equals to UPDATED_SO_BENH_AN
        defaultBenhAnKhamBenhShouldNotBeFound("soBenhAn.equals=" + UPDATED_SO_BENH_AN);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsBySoBenhAnIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where soBenhAn not equals to DEFAULT_SO_BENH_AN
        defaultBenhAnKhamBenhShouldNotBeFound("soBenhAn.notEquals=" + DEFAULT_SO_BENH_AN);

        // Get all the benhAnKhamBenhList where soBenhAn not equals to UPDATED_SO_BENH_AN
        defaultBenhAnKhamBenhShouldBeFound("soBenhAn.notEquals=" + UPDATED_SO_BENH_AN);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsBySoBenhAnIsInShouldWork() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where soBenhAn in DEFAULT_SO_BENH_AN or UPDATED_SO_BENH_AN
        defaultBenhAnKhamBenhShouldBeFound("soBenhAn.in=" + DEFAULT_SO_BENH_AN + "," + UPDATED_SO_BENH_AN);

        // Get all the benhAnKhamBenhList where soBenhAn equals to UPDATED_SO_BENH_AN
        defaultBenhAnKhamBenhShouldNotBeFound("soBenhAn.in=" + UPDATED_SO_BENH_AN);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsBySoBenhAnIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where soBenhAn is not null
        defaultBenhAnKhamBenhShouldBeFound("soBenhAn.specified=true");

        // Get all the benhAnKhamBenhList where soBenhAn is null
        defaultBenhAnKhamBenhShouldNotBeFound("soBenhAn.specified=false");
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsBySoBenhAnContainsSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where soBenhAn contains DEFAULT_SO_BENH_AN
        defaultBenhAnKhamBenhShouldBeFound("soBenhAn.contains=" + DEFAULT_SO_BENH_AN);

        // Get all the benhAnKhamBenhList where soBenhAn contains UPDATED_SO_BENH_AN
        defaultBenhAnKhamBenhShouldNotBeFound("soBenhAn.contains=" + UPDATED_SO_BENH_AN);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsBySoBenhAnNotContainsSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where soBenhAn does not contain DEFAULT_SO_BENH_AN
        defaultBenhAnKhamBenhShouldNotBeFound("soBenhAn.doesNotContain=" + DEFAULT_SO_BENH_AN);

        // Get all the benhAnKhamBenhList where soBenhAn does not contain UPDATED_SO_BENH_AN
        defaultBenhAnKhamBenhShouldBeFound("soBenhAn.doesNotContain=" + UPDATED_SO_BENH_AN);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsBySoBenhAnKhoaIsEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where soBenhAnKhoa equals to DEFAULT_SO_BENH_AN_KHOA
        defaultBenhAnKhamBenhShouldBeFound("soBenhAnKhoa.equals=" + DEFAULT_SO_BENH_AN_KHOA);

        // Get all the benhAnKhamBenhList where soBenhAnKhoa equals to UPDATED_SO_BENH_AN_KHOA
        defaultBenhAnKhamBenhShouldNotBeFound("soBenhAnKhoa.equals=" + UPDATED_SO_BENH_AN_KHOA);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsBySoBenhAnKhoaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where soBenhAnKhoa not equals to DEFAULT_SO_BENH_AN_KHOA
        defaultBenhAnKhamBenhShouldNotBeFound("soBenhAnKhoa.notEquals=" + DEFAULT_SO_BENH_AN_KHOA);

        // Get all the benhAnKhamBenhList where soBenhAnKhoa not equals to UPDATED_SO_BENH_AN_KHOA
        defaultBenhAnKhamBenhShouldBeFound("soBenhAnKhoa.notEquals=" + UPDATED_SO_BENH_AN_KHOA);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsBySoBenhAnKhoaIsInShouldWork() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where soBenhAnKhoa in DEFAULT_SO_BENH_AN_KHOA or UPDATED_SO_BENH_AN_KHOA
        defaultBenhAnKhamBenhShouldBeFound("soBenhAnKhoa.in=" + DEFAULT_SO_BENH_AN_KHOA + "," + UPDATED_SO_BENH_AN_KHOA);

        // Get all the benhAnKhamBenhList where soBenhAnKhoa equals to UPDATED_SO_BENH_AN_KHOA
        defaultBenhAnKhamBenhShouldNotBeFound("soBenhAnKhoa.in=" + UPDATED_SO_BENH_AN_KHOA);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsBySoBenhAnKhoaIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where soBenhAnKhoa is not null
        defaultBenhAnKhamBenhShouldBeFound("soBenhAnKhoa.specified=true");

        // Get all the benhAnKhamBenhList where soBenhAnKhoa is null
        defaultBenhAnKhamBenhShouldNotBeFound("soBenhAnKhoa.specified=false");
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsBySoBenhAnKhoaContainsSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where soBenhAnKhoa contains DEFAULT_SO_BENH_AN_KHOA
        defaultBenhAnKhamBenhShouldBeFound("soBenhAnKhoa.contains=" + DEFAULT_SO_BENH_AN_KHOA);

        // Get all the benhAnKhamBenhList where soBenhAnKhoa contains UPDATED_SO_BENH_AN_KHOA
        defaultBenhAnKhamBenhShouldNotBeFound("soBenhAnKhoa.contains=" + UPDATED_SO_BENH_AN_KHOA);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsBySoBenhAnKhoaNotContainsSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where soBenhAnKhoa does not contain DEFAULT_SO_BENH_AN_KHOA
        defaultBenhAnKhamBenhShouldNotBeFound("soBenhAnKhoa.doesNotContain=" + DEFAULT_SO_BENH_AN_KHOA);

        // Get all the benhAnKhamBenhList where soBenhAnKhoa does not contain UPDATED_SO_BENH_AN_KHOA
        defaultBenhAnKhamBenhShouldBeFound("soBenhAnKhoa.doesNotContain=" + UPDATED_SO_BENH_AN_KHOA);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNamIsEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where nam equals to DEFAULT_NAM
        defaultBenhAnKhamBenhShouldBeFound("nam.equals=" + DEFAULT_NAM);

        // Get all the benhAnKhamBenhList where nam equals to UPDATED_NAM
        defaultBenhAnKhamBenhShouldNotBeFound("nam.equals=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNamIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where nam not equals to DEFAULT_NAM
        defaultBenhAnKhamBenhShouldNotBeFound("nam.notEquals=" + DEFAULT_NAM);

        // Get all the benhAnKhamBenhList where nam not equals to UPDATED_NAM
        defaultBenhAnKhamBenhShouldBeFound("nam.notEquals=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNamIsInShouldWork() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where nam in DEFAULT_NAM or UPDATED_NAM
        defaultBenhAnKhamBenhShouldBeFound("nam.in=" + DEFAULT_NAM + "," + UPDATED_NAM);

        // Get all the benhAnKhamBenhList where nam equals to UPDATED_NAM
        defaultBenhAnKhamBenhShouldNotBeFound("nam.in=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNamIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where nam is not null
        defaultBenhAnKhamBenhShouldBeFound("nam.specified=true");

        // Get all the benhAnKhamBenhList where nam is null
        defaultBenhAnKhamBenhShouldNotBeFound("nam.specified=false");
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNamIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where nam is greater than or equal to DEFAULT_NAM
        defaultBenhAnKhamBenhShouldBeFound("nam.greaterThanOrEqual=" + DEFAULT_NAM);

        // Get all the benhAnKhamBenhList where nam is greater than or equal to UPDATED_NAM
        defaultBenhAnKhamBenhShouldNotBeFound("nam.greaterThanOrEqual=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNamIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where nam is less than or equal to DEFAULT_NAM
        defaultBenhAnKhamBenhShouldBeFound("nam.lessThanOrEqual=" + DEFAULT_NAM);

        // Get all the benhAnKhamBenhList where nam is less than or equal to SMALLER_NAM
        defaultBenhAnKhamBenhShouldNotBeFound("nam.lessThanOrEqual=" + SMALLER_NAM);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNamIsLessThanSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where nam is less than DEFAULT_NAM
        defaultBenhAnKhamBenhShouldNotBeFound("nam.lessThan=" + DEFAULT_NAM);

        // Get all the benhAnKhamBenhList where nam is less than UPDATED_NAM
        defaultBenhAnKhamBenhShouldBeFound("nam.lessThan=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNamIsGreaterThanSomething() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        // Get all the benhAnKhamBenhList where nam is greater than DEFAULT_NAM
        defaultBenhAnKhamBenhShouldNotBeFound("nam.greaterThan=" + DEFAULT_NAM);

        // Get all the benhAnKhamBenhList where nam is greater than SMALLER_NAM
        defaultBenhAnKhamBenhShouldBeFound("nam.greaterThan=" + SMALLER_NAM);
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByBenhNhanIdIsEqualToSomething() throws Exception {
        // Get already existing entity
        BenhNhan benhNhan = benhAnKhamBenh.getBenhNhan();
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);
        Long benhNhanId = benhNhan.getId();

        // Get all the benhAnKhamBenhList where benhNhanId equals to benhNhanId
        defaultBenhAnKhamBenhShouldBeFound("benhNhanId.equals=" + benhNhanId);

        // Get all the benhAnKhamBenhList where benhNhanId equals to benhNhanId + 1
        defaultBenhAnKhamBenhShouldNotBeFound("benhNhanId.equals=" + (benhNhanId + 1));
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByDonViIdIsEqualToSomething() throws Exception {
        // Get already existing entity
        DonVi donVi = benhAnKhamBenh.getDonVi();
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);
        Long donViId = donVi.getId();

        // Get all the benhAnKhamBenhList where donViId equals to donViId
        defaultBenhAnKhamBenhShouldBeFound("donViId.equals=" + donViId);

        // Get all the benhAnKhamBenhList where donViId equals to donViId + 1
        defaultBenhAnKhamBenhShouldNotBeFound("donViId.equals=" + (donViId + 1));
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByNhanVienTiepNhanIdIsEqualToSomething() throws Exception {
        // Get already existing entity
        NhanVien nhanVienTiepNhan = benhAnKhamBenh.getNhanVienTiepNhan();
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);
        Long nhanVienTiepNhanId = nhanVienTiepNhan.getId();

        // Get all the benhAnKhamBenhList where nhanVienTiepNhanId equals to nhanVienTiepNhanId
        defaultBenhAnKhamBenhShouldBeFound("nhanVienTiepNhanId.equals=" + nhanVienTiepNhanId);

        // Get all the benhAnKhamBenhList where nhanVienTiepNhanId equals to nhanVienTiepNhanId + 1
        defaultBenhAnKhamBenhShouldNotBeFound("nhanVienTiepNhanId.equals=" + (nhanVienTiepNhanId + 1));
    }

    @Test
    @Transactional
    public void getAllBenhAnKhamBenhsByPhongTiepNhanIdIsEqualToSomething() throws Exception {
        // Get already existing entity
        Phong phongTiepNhan = benhAnKhamBenh.getPhongTiepNhan();
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);
        Long phongTiepNhanId = phongTiepNhan.getId();

        // Get all the benhAnKhamBenhList where phongTiepNhanId equals to phongTiepNhanId
        defaultBenhAnKhamBenhShouldBeFound("phongTiepNhanId.equals=" + phongTiepNhanId);

        // Get all the benhAnKhamBenhList where phongTiepNhanId equals to phongTiepNhanId + 1
        defaultBenhAnKhamBenhShouldNotBeFound("phongTiepNhanId.equals=" + (phongTiepNhanId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultBenhAnKhamBenhShouldBeFound(String filter) throws Exception {
        restBenhAnKhamBenhMockMvc.perform(get("/api/benh-an-kham-benhs?" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].benhNhanCu").value(hasItem(DEFAULT_BENH_NHAN_CU.booleanValue())))
            .andExpect(jsonPath("$.[*].canhBao").value(hasItem(DEFAULT_CANH_BAO.booleanValue())))
            .andExpect(jsonPath("$.[*].capCuu").value(hasItem(DEFAULT_CAP_CUU.booleanValue())))
            .andExpect(jsonPath("$.[*].chiCoNamSinh").value(hasItem(DEFAULT_CHI_CO_NAM_SINH.booleanValue())))
            .andExpect(jsonPath("$.[*].cmndBenhNhan").value(hasItem(DEFAULT_CMND_BENH_NHAN)))
            .andExpect(jsonPath("$.[*].cmndNguoiNha").value(hasItem(DEFAULT_CMND_NGUOI_NHA)))
            .andExpect(jsonPath("$.[*].coBaoHiem").value(hasItem(DEFAULT_CO_BAO_HIEM.booleanValue())))
            .andExpect(jsonPath("$.[*].diaChi").value(hasItem(DEFAULT_DIA_CHI)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].gioiTinh").value(hasItem(DEFAULT_GIOI_TINH)))
            .andExpect(jsonPath("$.[*].khangThe").value(hasItem(DEFAULT_KHANG_THE)))
            .andExpect(jsonPath("$.[*].lanKhamTrongNgay").value(hasItem(DEFAULT_LAN_KHAM_TRONG_NGAY)))
            .andExpect(jsonPath("$.[*].loaiGiayToTreEm").value(hasItem(DEFAULT_LOAI_GIAY_TO_TRE_EM)))
            .andExpect(jsonPath("$.[*].ngayCapCmnd").value(hasItem(DEFAULT_NGAY_CAP_CMND.toString())))
            .andExpect(jsonPath("$.[*].ngayMienCungChiTra").value(hasItem(DEFAULT_NGAY_MIEN_CUNG_CHI_TRA.toString())))
            .andExpect(jsonPath("$.[*].ngaySinh").value(hasItem(DEFAULT_NGAY_SINH.toString())))
            .andExpect(jsonPath("$.[*].nhomMau").value(hasItem(DEFAULT_NHOM_MAU)))
            .andExpect(jsonPath("$.[*].noiCapCmnd").value(hasItem(DEFAULT_NOI_CAP_CMND)))
            .andExpect(jsonPath("$.[*].noiLamViec").value(hasItem(DEFAULT_NOI_LAM_VIEC)))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE)))
            .andExpect(jsonPath("$.[*].phoneNguoiNha").value(hasItem(DEFAULT_PHONE_NGUOI_NHA)))
            .andExpect(jsonPath("$.[*].quocTich").value(hasItem(DEFAULT_QUOC_TICH)))
            .andExpect(jsonPath("$.[*].tenBenhNhan").value(hasItem(DEFAULT_TEN_BENH_NHAN)))
            .andExpect(jsonPath("$.[*].tenNguoiNha").value(hasItem(DEFAULT_TEN_NGUOI_NHA)))
            .andExpect(jsonPath("$.[*].thangTuoi").value(hasItem(DEFAULT_THANG_TUOI)))
            .andExpect(jsonPath("$.[*].thoiGianTiepNhan").value(hasItem(DEFAULT_THOI_GIAN_TIEP_NHAN.toString())))
            .andExpect(jsonPath("$.[*].tuoi").value(hasItem(DEFAULT_TUOI)))
            .andExpect(jsonPath("$.[*].uuTien").value(hasItem(DEFAULT_UU_TIEN)))
            .andExpect(jsonPath("$.[*].uuid").value(hasItem(DEFAULT_UUID)))
            .andExpect(jsonPath("$.[*].trangThai").value(hasItem(DEFAULT_TRANG_THAI)))
            .andExpect(jsonPath("$.[*].maKhoaHoanTatKham").value(hasItem(DEFAULT_MA_KHOA_HOAN_TAT_KHAM)))
            .andExpect(jsonPath("$.[*].maPhongHoanTatKham").value(hasItem(DEFAULT_MA_PHONG_HOAN_TAT_KHAM)))
            .andExpect(jsonPath("$.[*].tenKhoaHoanTatKham").value(hasItem(DEFAULT_TEN_KHOA_HOAN_TAT_KHAM)))
            .andExpect(jsonPath("$.[*].tenPhongHoanTatKham").value(hasItem(DEFAULT_TEN_PHONG_HOAN_TAT_KHAM)))
            .andExpect(jsonPath("$.[*].thoiGianHoanTatKham").value(hasItem(DEFAULT_THOI_GIAN_HOAN_TAT_KHAM.toString())))
            .andExpect(jsonPath("$.[*].soBenhAn").value(hasItem(DEFAULT_SO_BENH_AN)))
            .andExpect(jsonPath("$.[*].soBenhAnKhoa").value(hasItem(DEFAULT_SO_BENH_AN_KHOA)))
            .andExpect(jsonPath("$.[*].nam").value(hasItem(DEFAULT_NAM)));

        // Check, that the count call also returns 1
        restBenhAnKhamBenhMockMvc.perform(get("/api/benh-an-kham-benhs/count?" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultBenhAnKhamBenhShouldNotBeFound(String filter) throws Exception {
        restBenhAnKhamBenhMockMvc.perform(get("/api/benh-an-kham-benhs?" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restBenhAnKhamBenhMockMvc.perform(get("/api/benh-an-kham-benhs/count?" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingBenhAnKhamBenh() throws Exception {
        // Get the benhAnKhamBenh
        BenhAnKhamBenh benhAnKhamBenh = createUpdatedEntity(em);
        restBenhAnKhamBenhMockMvc.perform(get("/api/benh-an-kham-benhs/{id}", "benhNhanId=" + benhAnKhamBenh.getId().getBenhNhanId() + ";" + "donViId=" + benhAnKhamBenh.getId().getDonViId()))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBenhAnKhamBenh() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        int databaseSizeBeforeUpdate = benhAnKhamBenhRepository.findAll().size();

        // Update the benhAnKhamBenh
        BenhAnKhamBenh updatedBenhAnKhamBenh = benhAnKhamBenhRepository.findById(benhAnKhamBenh.getId()).get();
        // Disconnect from session so that the updates on updatedBenhAnKhamBenh are not directly saved in db
        em.detach(updatedBenhAnKhamBenh);
        updatedBenhAnKhamBenh
            .benhNhanCu(UPDATED_BENH_NHAN_CU)
            .canhBao(UPDATED_CANH_BAO)
            .capCuu(UPDATED_CAP_CUU)
            .chiCoNamSinh(UPDATED_CHI_CO_NAM_SINH)
            .cmndBenhNhan(UPDATED_CMND_BENH_NHAN)
            .cmndNguoiNha(UPDATED_CMND_NGUOI_NHA)
            .coBaoHiem(UPDATED_CO_BAO_HIEM)
            .diaChi(UPDATED_DIA_CHI)
            .email(UPDATED_EMAIL)
            .gioiTinh(UPDATED_GIOI_TINH)
            .khangThe(UPDATED_KHANG_THE)
            .lanKhamTrongNgay(UPDATED_LAN_KHAM_TRONG_NGAY)
            .loaiGiayToTreEm(UPDATED_LOAI_GIAY_TO_TRE_EM)
            .ngayCapCmnd(UPDATED_NGAY_CAP_CMND)
            .ngayMienCungChiTra(UPDATED_NGAY_MIEN_CUNG_CHI_TRA)
            .ngaySinh(UPDATED_NGAY_SINH)
            .nhomMau(UPDATED_NHOM_MAU)
            .noiCapCmnd(UPDATED_NOI_CAP_CMND)
            .noiLamViec(UPDATED_NOI_LAM_VIEC)
            .phone(UPDATED_PHONE)
            .phoneNguoiNha(UPDATED_PHONE_NGUOI_NHA)
            .quocTich(UPDATED_QUOC_TICH)
            .tenBenhNhan(UPDATED_TEN_BENH_NHAN)
            .tenNguoiNha(UPDATED_TEN_NGUOI_NHA)
            .thangTuoi(UPDATED_THANG_TUOI)
            .thoiGianTiepNhan(UPDATED_THOI_GIAN_TIEP_NHAN)
            .tuoi(UPDATED_TUOI)
            .uuTien(UPDATED_UU_TIEN)
            .uuid(UPDATED_UUID)
            .trangThai(UPDATED_TRANG_THAI)
            .maKhoaHoanTatKham(UPDATED_MA_KHOA_HOAN_TAT_KHAM)
            .maPhongHoanTatKham(UPDATED_MA_PHONG_HOAN_TAT_KHAM)
            .tenKhoaHoanTatKham(UPDATED_TEN_KHOA_HOAN_TAT_KHAM)
            .tenPhongHoanTatKham(UPDATED_TEN_PHONG_HOAN_TAT_KHAM)
            .thoiGianHoanTatKham(UPDATED_THOI_GIAN_HOAN_TAT_KHAM)
            .soBenhAn(UPDATED_SO_BENH_AN)
            .soBenhAnKhoa(UPDATED_SO_BENH_AN_KHOA)
            .nam(UPDATED_NAM);
        BenhAnKhamBenhDTO benhAnKhamBenhDTO = benhAnKhamBenhMapper.toDto(updatedBenhAnKhamBenh);

        restBenhAnKhamBenhMockMvc.perform(put("/api/benh-an-kham-benhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(benhAnKhamBenhDTO)))
            .andExpect(status().isOk());

        // Validate the BenhAnKhamBenh in the database
        List<BenhAnKhamBenh> benhAnKhamBenhList = benhAnKhamBenhRepository.findAll();
        assertThat(benhAnKhamBenhList).hasSize(databaseSizeBeforeUpdate);
        BenhAnKhamBenh testBenhAnKhamBenh = benhAnKhamBenhList.get(benhAnKhamBenhList.size() - 1);
        assertThat(testBenhAnKhamBenh.isBenhNhanCu()).isEqualTo(UPDATED_BENH_NHAN_CU);
        assertThat(testBenhAnKhamBenh.isCanhBao()).isEqualTo(UPDATED_CANH_BAO);
        assertThat(testBenhAnKhamBenh.isCapCuu()).isEqualTo(UPDATED_CAP_CUU);
        assertThat(testBenhAnKhamBenh.isChiCoNamSinh()).isEqualTo(UPDATED_CHI_CO_NAM_SINH);
        assertThat(testBenhAnKhamBenh.getCmndBenhNhan()).isEqualTo(UPDATED_CMND_BENH_NHAN);
        assertThat(testBenhAnKhamBenh.getCmndNguoiNha()).isEqualTo(UPDATED_CMND_NGUOI_NHA);
        assertThat(testBenhAnKhamBenh.isCoBaoHiem()).isEqualTo(UPDATED_CO_BAO_HIEM);
        assertThat(testBenhAnKhamBenh.getDiaChi()).isEqualTo(UPDATED_DIA_CHI);
        assertThat(testBenhAnKhamBenh.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testBenhAnKhamBenh.getGioiTinh()).isEqualTo(UPDATED_GIOI_TINH);
        assertThat(testBenhAnKhamBenh.getKhangThe()).isEqualTo(UPDATED_KHANG_THE);
        assertThat(testBenhAnKhamBenh.getLanKhamTrongNgay()).isEqualTo(UPDATED_LAN_KHAM_TRONG_NGAY);
        assertThat(testBenhAnKhamBenh.getLoaiGiayToTreEm()).isEqualTo(UPDATED_LOAI_GIAY_TO_TRE_EM);
        assertThat(testBenhAnKhamBenh.getNgayCapCmnd()).isEqualTo(UPDATED_NGAY_CAP_CMND);
        assertThat(testBenhAnKhamBenh.getNgayMienCungChiTra()).isEqualTo(UPDATED_NGAY_MIEN_CUNG_CHI_TRA);
        assertThat(testBenhAnKhamBenh.getNgaySinh()).isEqualTo(UPDATED_NGAY_SINH);
        assertThat(testBenhAnKhamBenh.getNhomMau()).isEqualTo(UPDATED_NHOM_MAU);
        assertThat(testBenhAnKhamBenh.getNoiCapCmnd()).isEqualTo(UPDATED_NOI_CAP_CMND);
        assertThat(testBenhAnKhamBenh.getNoiLamViec()).isEqualTo(UPDATED_NOI_LAM_VIEC);
        assertThat(testBenhAnKhamBenh.getPhone()).isEqualTo(UPDATED_PHONE);
        assertThat(testBenhAnKhamBenh.getPhoneNguoiNha()).isEqualTo(UPDATED_PHONE_NGUOI_NHA);
        assertThat(testBenhAnKhamBenh.getQuocTich()).isEqualTo(UPDATED_QUOC_TICH);
        assertThat(testBenhAnKhamBenh.getTenBenhNhan()).isEqualTo(UPDATED_TEN_BENH_NHAN);
        assertThat(testBenhAnKhamBenh.getTenNguoiNha()).isEqualTo(UPDATED_TEN_NGUOI_NHA);
        assertThat(testBenhAnKhamBenh.getThangTuoi()).isEqualTo(UPDATED_THANG_TUOI);
        assertThat(testBenhAnKhamBenh.getThoiGianTiepNhan()).isEqualTo(UPDATED_THOI_GIAN_TIEP_NHAN);
        assertThat(testBenhAnKhamBenh.getTuoi()).isEqualTo(UPDATED_TUOI);
        assertThat(testBenhAnKhamBenh.getUuTien()).isEqualTo(UPDATED_UU_TIEN);
        assertThat(testBenhAnKhamBenh.getUuid()).isEqualTo(UPDATED_UUID);
        assertThat(testBenhAnKhamBenh.getTrangThai()).isEqualTo(UPDATED_TRANG_THAI);
        assertThat(testBenhAnKhamBenh.getMaKhoaHoanTatKham()).isEqualTo(UPDATED_MA_KHOA_HOAN_TAT_KHAM);
        assertThat(testBenhAnKhamBenh.getMaPhongHoanTatKham()).isEqualTo(UPDATED_MA_PHONG_HOAN_TAT_KHAM);
        assertThat(testBenhAnKhamBenh.getTenKhoaHoanTatKham()).isEqualTo(UPDATED_TEN_KHOA_HOAN_TAT_KHAM);
        assertThat(testBenhAnKhamBenh.getTenPhongHoanTatKham()).isEqualTo(UPDATED_TEN_PHONG_HOAN_TAT_KHAM);
        assertThat(testBenhAnKhamBenh.getThoiGianHoanTatKham()).isEqualTo(UPDATED_THOI_GIAN_HOAN_TAT_KHAM);
        assertThat(testBenhAnKhamBenh.getSoBenhAn()).isEqualTo(UPDATED_SO_BENH_AN);
        assertThat(testBenhAnKhamBenh.getSoBenhAnKhoa()).isEqualTo(UPDATED_SO_BENH_AN_KHOA);
        assertThat(testBenhAnKhamBenh.getNam()).isEqualTo(UPDATED_NAM);
    }

    @Test
    @Transactional
    public void updateNonExistingBenhAnKhamBenh() throws Exception {
        int databaseSizeBeforeUpdate = benhAnKhamBenhRepository.findAll().size();

        // Create the BenhAnKhamBenh
        BenhAnKhamBenhDTO benhAnKhamBenhDTO = benhAnKhamBenhMapper.toDto(benhAnKhamBenh);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBenhAnKhamBenhMockMvc.perform(put("/api/benh-an-kham-benhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(benhAnKhamBenhDTO)))
            .andExpect(status().isBadRequest());

        // Validate the BenhAnKhamBenh in the database
        List<BenhAnKhamBenh> benhAnKhamBenhList = benhAnKhamBenhRepository.findAll();
        assertThat(benhAnKhamBenhList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBenhAnKhamBenh() throws Exception {
        // Initialize the database
        benhAnKhamBenhRepository.saveAndFlush(benhAnKhamBenh);

        int databaseSizeBeforeDelete = benhAnKhamBenhRepository.findAll().size();

        // Delete the benhAnKhamBenh
        restBenhAnKhamBenhMockMvc.perform(delete("/api/benh-an-kham-benhs/{id}", "benhNhanId=" + benhAnKhamBenh.getId().getBenhNhanId() + ";" + "donViId=" + benhAnKhamBenh.getId().getDonViId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<BenhAnKhamBenh> benhAnKhamBenhList = benhAnKhamBenhRepository.findAll();
        assertThat(benhAnKhamBenhList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.DichVuKhamApDung;
import vn.vnpt.domain.DotGiaDichVuBhxh;
import vn.vnpt.domain.DichVuKham;
import vn.vnpt.repository.DichVuKhamApDungRepository;
import vn.vnpt.service.DichVuKhamApDungService;
import vn.vnpt.service.dto.DichVuKhamApDungDTO;
import vn.vnpt.service.mapper.DichVuKhamApDungMapper;
import vn.vnpt.service.dto.DichVuKhamApDungCriteria;
import vn.vnpt.service.DichVuKhamApDungQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link DichVuKhamApDungResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class DichVuKhamApDungResourceIT {

    public static final Integer DEFAULT_ENABLED = 1;
    public static final Integer UPDATED_ENABLED = 2;
    public static final Integer SMALLER_ENABLED = 1 - 1;

    public static final BigDecimal DEFAULT_GIA_BHYT = new BigDecimal(1);
    public static final BigDecimal UPDATED_GIA_BHYT = new BigDecimal(2);
    public static final BigDecimal SMALLER_GIA_BHYT = new BigDecimal(1 - 1);

    public static final BigDecimal DEFAULT_GIA_KHONG_BHYT = new BigDecimal(1);
    public static final BigDecimal UPDATED_GIA_KHONG_BHYT = new BigDecimal(2);
    public static final BigDecimal SMALLER_GIA_KHONG_BHYT = new BigDecimal(1 - 1);

    public static final String DEFAULT_MA_BAO_CAO_BHYT = "AAAAAAAAAA";
    public static final String UPDATED_MA_BAO_CAO_BHYT = "BBBBBBBBBB";

    public static final String DEFAULT_MA_BAO_CAO_BYT = "AAAAAAAAAA";
    public static final String UPDATED_MA_BAO_CAO_BYT = "BBBBBBBBBB";

    public static final String DEFAULT_SO_CONG_VAN_BHXH = "AAAAAAAAAA";
    public static final String UPDATED_SO_CONG_VAN_BHXH = "BBBBBBBBBB";

    public static final String DEFAULT_SO_QUYET_DINH = "AAAAAAAAAA";
    public static final String UPDATED_SO_QUYET_DINH = "BBBBBBBBBB";

    public static final String DEFAULT_TEN_BAO_CAO_BHXH = "AAAAAAAAAA";
    public static final String UPDATED_TEN_BAO_CAO_BHXH = "BBBBBBBBBB";

    public static final String DEFAULT_TEN_DICH_VU_KHONG_BAO_HIEM = "AAAAAAAAAA";
    public static final String UPDATED_TEN_DICH_VU_KHONG_BAO_HIEM = "BBBBBBBBBB";

    public static final BigDecimal DEFAULT_TIEN_BENH_NHAN_CHI = new BigDecimal(1);
    public static final BigDecimal UPDATED_TIEN_BENH_NHAN_CHI = new BigDecimal(2);
    public static final BigDecimal SMALLER_TIEN_BENH_NHAN_CHI = new BigDecimal(1 - 1);

    public static final BigDecimal DEFAULT_TIEN_BHXH_CHI = new BigDecimal(1);
    public static final BigDecimal UPDATED_TIEN_BHXH_CHI = new BigDecimal(2);
    public static final BigDecimal SMALLER_TIEN_BHXH_CHI = new BigDecimal(1 - 1);

    public static final BigDecimal DEFAULT_TIEN_NGOAI_BHYT = new BigDecimal(1);
    public static final BigDecimal UPDATED_TIEN_NGOAI_BHYT = new BigDecimal(2);
    public static final BigDecimal SMALLER_TIEN_NGOAI_BHYT = new BigDecimal(1 - 1);

    public static final BigDecimal DEFAULT_TONG_TIEN_THANH_TOAN = new BigDecimal(1);
    public static final BigDecimal UPDATED_TONG_TIEN_THANH_TOAN = new BigDecimal(2);
    public static final BigDecimal SMALLER_TONG_TIEN_THANH_TOAN = new BigDecimal(1 - 1);

    public static final Integer DEFAULT_TY_LE_BHXH_THANH_TOAN = 1;
    public static final Integer UPDATED_TY_LE_BHXH_THANH_TOAN = 2;
    public static final Integer SMALLER_TY_LE_BHXH_THANH_TOAN = 1 - 1;

    public static final Integer DEFAULT_DOI_TUONG_DAC_BIET = 1;
    public static final Integer UPDATED_DOI_TUONG_DAC_BIET = 2;
    public static final Integer SMALLER_DOI_TUONG_DAC_BIET = 1 - 1;

    public static final Integer DEFAULT_NGUON_CHI = 1;
    public static final Integer UPDATED_NGUON_CHI = 2;
    public static final Integer SMALLER_NGUON_CHI = 1 - 1;

    @Autowired
    private DichVuKhamApDungRepository dichVuKhamApDungRepository;

    @Autowired
    private DichVuKhamApDungMapper dichVuKhamApDungMapper;

    @Autowired
    private DichVuKhamApDungService dichVuKhamApDungService;

    @Autowired
    private DichVuKhamApDungQueryService dichVuKhamApDungQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDichVuKhamApDungMockMvc;

    private DichVuKhamApDung dichVuKhamApDung;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DichVuKhamApDung createEntity(EntityManager em) {
        DichVuKhamApDung dichVuKhamApDung = new DichVuKhamApDung()
            .enabled(DEFAULT_ENABLED)
            .giaBhyt(DEFAULT_GIA_BHYT)
            .giaKhongBhyt(DEFAULT_GIA_KHONG_BHYT)
            .maBaoCaoBhyt(DEFAULT_MA_BAO_CAO_BHYT)
            .maBaoCaoByt(DEFAULT_MA_BAO_CAO_BYT)
            .soCongVanBhxh(DEFAULT_SO_CONG_VAN_BHXH)
            .soQuyetDinh(DEFAULT_SO_QUYET_DINH)
            .tenBaoCaoBhxh(DEFAULT_TEN_BAO_CAO_BHXH)
            .tenDichVuKhongBaoHiem(DEFAULT_TEN_DICH_VU_KHONG_BAO_HIEM)
            .tienBenhNhanChi(DEFAULT_TIEN_BENH_NHAN_CHI)
            .tienBhxhChi(DEFAULT_TIEN_BHXH_CHI)
            .tienNgoaiBhyt(DEFAULT_TIEN_NGOAI_BHYT)
            .tongTienThanhToan(DEFAULT_TONG_TIEN_THANH_TOAN)
            .tyLeBhxhThanhToan(DEFAULT_TY_LE_BHXH_THANH_TOAN)
            .doiTuongDacBiet(DEFAULT_DOI_TUONG_DAC_BIET)
            .nguonChi(DEFAULT_NGUON_CHI);
        // Add required entity
        DotGiaDichVuBhxh newDotGiaDichVuBhxh = DotGiaDichVuBhxhResourceIT.createEntity(em);
        DotGiaDichVuBhxh dotGiaDichVuBhxh = TestUtil.findAll(em, DotGiaDichVuBhxh.class).stream()
            .filter(x -> x.getDoiTuongApDung().equals(newDotGiaDichVuBhxh.getDoiTuongApDung()))
            .findAny().orElse(null);
        if (dotGiaDichVuBhxh == null) {
            dotGiaDichVuBhxh = newDotGiaDichVuBhxh;
            em.persist(dotGiaDichVuBhxh);
            em.flush();
        }
        dichVuKhamApDung.setDotGiaDichVuBhxh(dotGiaDichVuBhxh);
        // Add required entity
        DichVuKham newDichVuKham = DichVuKhamResourceIT.createEntity(em);
        DichVuKham dichVuKham = TestUtil.findAll(em, DichVuKham.class).stream()
            .filter(x -> x.getDeleted().equals(newDichVuKham.getDeleted()))
            .findAny().orElse(null);
        if (dichVuKham == null) {
            dichVuKham = newDichVuKham;
            em.persist(dichVuKham);
            em.flush();
        }
        dichVuKhamApDung.setDichVuKham(dichVuKham);
        return dichVuKhamApDung;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DichVuKhamApDung createUpdatedEntity(EntityManager em) {
        DichVuKhamApDung dichVuKhamApDung = new DichVuKhamApDung()
            .enabled(UPDATED_ENABLED)
            .giaBhyt(UPDATED_GIA_BHYT)
            .giaKhongBhyt(UPDATED_GIA_KHONG_BHYT)
            .maBaoCaoBhyt(UPDATED_MA_BAO_CAO_BHYT)
            .maBaoCaoByt(UPDATED_MA_BAO_CAO_BYT)
            .soCongVanBhxh(UPDATED_SO_CONG_VAN_BHXH)
            .soQuyetDinh(UPDATED_SO_QUYET_DINH)
            .tenBaoCaoBhxh(UPDATED_TEN_BAO_CAO_BHXH)
            .tenDichVuKhongBaoHiem(UPDATED_TEN_DICH_VU_KHONG_BAO_HIEM)
            .tienBenhNhanChi(UPDATED_TIEN_BENH_NHAN_CHI)
            .tienBhxhChi(UPDATED_TIEN_BHXH_CHI)
            .tienNgoaiBhyt(UPDATED_TIEN_NGOAI_BHYT)
            .tongTienThanhToan(UPDATED_TONG_TIEN_THANH_TOAN)
            .tyLeBhxhThanhToan(UPDATED_TY_LE_BHXH_THANH_TOAN)
            .doiTuongDacBiet(UPDATED_DOI_TUONG_DAC_BIET)
            .nguonChi(UPDATED_NGUON_CHI);
        // Add required entity
        DotGiaDichVuBhxh newDotGiaDichVuBhxh = DotGiaDichVuBhxhResourceIT.createUpdatedEntity(em);
        DotGiaDichVuBhxh dotGiaDichVuBhxh = TestUtil.findAll(em, DotGiaDichVuBhxh.class).stream()
            .filter(x -> x.getDoiTuongApDung().equals(newDotGiaDichVuBhxh.getDoiTuongApDung()))
            .findAny().orElse(null);
        if (dotGiaDichVuBhxh == null) {
            dotGiaDichVuBhxh = newDotGiaDichVuBhxh;
            em.persist(dotGiaDichVuBhxh);
            em.flush();
        }
        dichVuKhamApDung.setDotGiaDichVuBhxh(dotGiaDichVuBhxh);
        // Add required entity
        DichVuKham newDichVuKham = DichVuKhamResourceIT.createUpdatedEntity(em);
        DichVuKham dichVuKham = TestUtil.findAll(em, DichVuKham.class).stream()
            .filter(x -> x.getDeleted().equals(newDichVuKham.getDeleted()))
            .findAny().orElse(null);
        if (dichVuKham == null) {
            dichVuKham = newDichVuKham;
            em.persist(dichVuKham);
            em.flush();
        }
        dichVuKhamApDung.setDichVuKham(dichVuKham);
        return dichVuKhamApDung;
    }

    @BeforeEach
    public void initTest() {
        dichVuKhamApDung = createEntity(em);
    }

    @Test
    @Transactional
    public void createDichVuKhamApDung() throws Exception {
        int databaseSizeBeforeCreate = dichVuKhamApDungRepository.findAll().size();

        // Create the DichVuKhamApDung
        DichVuKhamApDungDTO dichVuKhamApDungDTO = dichVuKhamApDungMapper.toDto(dichVuKhamApDung);
        restDichVuKhamApDungMockMvc.perform(post("/api/dich-vu-kham-ap-dungs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dichVuKhamApDungDTO)))
            .andExpect(status().isCreated());

        // Validate the DichVuKhamApDung in the database
        List<DichVuKhamApDung> dichVuKhamApDungList = dichVuKhamApDungRepository.findAll();
        assertThat(dichVuKhamApDungList).hasSize(databaseSizeBeforeCreate + 1);
        DichVuKhamApDung testDichVuKhamApDung = dichVuKhamApDungList.get(dichVuKhamApDungList.size() - 1);
        assertThat(testDichVuKhamApDung.getEnabled()).isEqualTo(DEFAULT_ENABLED);
        assertThat(testDichVuKhamApDung.getGiaBhyt()).isEqualTo(DEFAULT_GIA_BHYT);
        assertThat(testDichVuKhamApDung.getGiaKhongBhyt()).isEqualTo(DEFAULT_GIA_KHONG_BHYT);
        assertThat(testDichVuKhamApDung.getMaBaoCaoBhyt()).isEqualTo(DEFAULT_MA_BAO_CAO_BHYT);
        assertThat(testDichVuKhamApDung.getMaBaoCaoByt()).isEqualTo(DEFAULT_MA_BAO_CAO_BYT);
        assertThat(testDichVuKhamApDung.getSoCongVanBhxh()).isEqualTo(DEFAULT_SO_CONG_VAN_BHXH);
        assertThat(testDichVuKhamApDung.getSoQuyetDinh()).isEqualTo(DEFAULT_SO_QUYET_DINH);
        assertThat(testDichVuKhamApDung.getTenBaoCaoBhxh()).isEqualTo(DEFAULT_TEN_BAO_CAO_BHXH);
        assertThat(testDichVuKhamApDung.getTenDichVuKhongBaoHiem()).isEqualTo(DEFAULT_TEN_DICH_VU_KHONG_BAO_HIEM);
        assertThat(testDichVuKhamApDung.getTienBenhNhanChi()).isEqualTo(DEFAULT_TIEN_BENH_NHAN_CHI);
        assertThat(testDichVuKhamApDung.getTienBhxhChi()).isEqualTo(DEFAULT_TIEN_BHXH_CHI);
        assertThat(testDichVuKhamApDung.getTienNgoaiBhyt()).isEqualTo(DEFAULT_TIEN_NGOAI_BHYT);
        assertThat(testDichVuKhamApDung.getTongTienThanhToan()).isEqualTo(DEFAULT_TONG_TIEN_THANH_TOAN);
        assertThat(testDichVuKhamApDung.getTyLeBhxhThanhToan()).isEqualTo(DEFAULT_TY_LE_BHXH_THANH_TOAN);
        assertThat(testDichVuKhamApDung.getDoiTuongDacBiet()).isEqualTo(DEFAULT_DOI_TUONG_DAC_BIET);
        assertThat(testDichVuKhamApDung.getNguonChi()).isEqualTo(DEFAULT_NGUON_CHI);
    }

    @Test
    @Transactional
    public void createDichVuKhamApDungWithExistingId() throws Exception {
        dichVuKhamApDungRepository.save(dichVuKhamApDung);
        int databaseSizeBeforeCreate = dichVuKhamApDungRepository.findAll().size();

        // Create the DichVuKhamApDung with an existing ID
        dichVuKhamApDung.setId(dichVuKhamApDung.getId());
        DichVuKhamApDungDTO dichVuKhamApDungDTO = dichVuKhamApDungMapper.toDto(dichVuKhamApDung);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDichVuKhamApDungMockMvc.perform(post("/api/dich-vu-kham-ap-dungs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dichVuKhamApDungDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DichVuKhamApDung in the database
        List<DichVuKhamApDung> dichVuKhamApDungList = dichVuKhamApDungRepository.findAll();
        assertThat(dichVuKhamApDungList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkGiaBhytIsRequired() throws Exception {
        int databaseSizeBeforeTest = dichVuKhamApDungRepository.findAll().size();
        // set the field null
        dichVuKhamApDung.setGiaBhyt(null);

        // Create the DichVuKhamApDung, which fails.
        DichVuKhamApDungDTO dichVuKhamApDungDTO = dichVuKhamApDungMapper.toDto(dichVuKhamApDung);

        restDichVuKhamApDungMockMvc.perform(post("/api/dich-vu-kham-ap-dungs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dichVuKhamApDungDTO)))
            .andExpect(status().isBadRequest());

        List<DichVuKhamApDung> dichVuKhamApDungList = dichVuKhamApDungRepository.findAll();
        assertThat(dichVuKhamApDungList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkGiaKhongBhytIsRequired() throws Exception {
        int databaseSizeBeforeTest = dichVuKhamApDungRepository.findAll().size();
        // set the field null
        dichVuKhamApDung.setGiaKhongBhyt(null);

        // Create the DichVuKhamApDung, which fails.
        DichVuKhamApDungDTO dichVuKhamApDungDTO = dichVuKhamApDungMapper.toDto(dichVuKhamApDung);

        restDichVuKhamApDungMockMvc.perform(post("/api/dich-vu-kham-ap-dungs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dichVuKhamApDungDTO)))
            .andExpect(status().isBadRequest());

        List<DichVuKhamApDung> dichVuKhamApDungList = dichVuKhamApDungRepository.findAll();
        assertThat(dichVuKhamApDungList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTienBenhNhanChiIsRequired() throws Exception {
        int databaseSizeBeforeTest = dichVuKhamApDungRepository.findAll().size();
        // set the field null
        dichVuKhamApDung.setTienBenhNhanChi(null);

        // Create the DichVuKhamApDung, which fails.
        DichVuKhamApDungDTO dichVuKhamApDungDTO = dichVuKhamApDungMapper.toDto(dichVuKhamApDung);

        restDichVuKhamApDungMockMvc.perform(post("/api/dich-vu-kham-ap-dungs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dichVuKhamApDungDTO)))
            .andExpect(status().isBadRequest());

        List<DichVuKhamApDung> dichVuKhamApDungList = dichVuKhamApDungRepository.findAll();
        assertThat(dichVuKhamApDungList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTienBhxhChiIsRequired() throws Exception {
        int databaseSizeBeforeTest = dichVuKhamApDungRepository.findAll().size();
        // set the field null
        dichVuKhamApDung.setTienBhxhChi(null);

        // Create the DichVuKhamApDung, which fails.
        DichVuKhamApDungDTO dichVuKhamApDungDTO = dichVuKhamApDungMapper.toDto(dichVuKhamApDung);

        restDichVuKhamApDungMockMvc.perform(post("/api/dich-vu-kham-ap-dungs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dichVuKhamApDungDTO)))
            .andExpect(status().isBadRequest());

        List<DichVuKhamApDung> dichVuKhamApDungList = dichVuKhamApDungRepository.findAll();
        assertThat(dichVuKhamApDungList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTienNgoaiBhytIsRequired() throws Exception {
        int databaseSizeBeforeTest = dichVuKhamApDungRepository.findAll().size();
        // set the field null
        dichVuKhamApDung.setTienNgoaiBhyt(null);

        // Create the DichVuKhamApDung, which fails.
        DichVuKhamApDungDTO dichVuKhamApDungDTO = dichVuKhamApDungMapper.toDto(dichVuKhamApDung);

        restDichVuKhamApDungMockMvc.perform(post("/api/dich-vu-kham-ap-dungs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dichVuKhamApDungDTO)))
            .andExpect(status().isBadRequest());

        List<DichVuKhamApDung> dichVuKhamApDungList = dichVuKhamApDungRepository.findAll();
        assertThat(dichVuKhamApDungList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTongTienThanhToanIsRequired() throws Exception {
        int databaseSizeBeforeTest = dichVuKhamApDungRepository.findAll().size();
        // set the field null
        dichVuKhamApDung.setTongTienThanhToan(null);

        // Create the DichVuKhamApDung, which fails.
        DichVuKhamApDungDTO dichVuKhamApDungDTO = dichVuKhamApDungMapper.toDto(dichVuKhamApDung);

        restDichVuKhamApDungMockMvc.perform(post("/api/dich-vu-kham-ap-dungs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dichVuKhamApDungDTO)))
            .andExpect(status().isBadRequest());

        List<DichVuKhamApDung> dichVuKhamApDungList = dichVuKhamApDungRepository.findAll();
        assertThat(dichVuKhamApDungList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungs() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList
        restDichVuKhamApDungMockMvc.perform(get("/api/dich-vu-kham-ap-dungs"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dichVuKhamApDung.getId().intValue())))
            .andExpect(jsonPath("$.[*].enabled").value(hasItem(DEFAULT_ENABLED)))
            .andExpect(jsonPath("$.[*].giaBhyt").value(hasItem(DEFAULT_GIA_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].giaKhongBhyt").value(hasItem(DEFAULT_GIA_KHONG_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].maBaoCaoBhyt").value(hasItem(DEFAULT_MA_BAO_CAO_BHYT)))
            .andExpect(jsonPath("$.[*].maBaoCaoByt").value(hasItem(DEFAULT_MA_BAO_CAO_BYT)))
            .andExpect(jsonPath("$.[*].soCongVanBhxh").value(hasItem(DEFAULT_SO_CONG_VAN_BHXH)))
            .andExpect(jsonPath("$.[*].soQuyetDinh").value(hasItem(DEFAULT_SO_QUYET_DINH)))
            .andExpect(jsonPath("$.[*].tenBaoCaoBhxh").value(hasItem(DEFAULT_TEN_BAO_CAO_BHXH)))
            .andExpect(jsonPath("$.[*].tenDichVuKhongBaoHiem").value(hasItem(DEFAULT_TEN_DICH_VU_KHONG_BAO_HIEM)))
            .andExpect(jsonPath("$.[*].tienBenhNhanChi").value(hasItem(DEFAULT_TIEN_BENH_NHAN_CHI.intValue())))
            .andExpect(jsonPath("$.[*].tienBhxhChi").value(hasItem(DEFAULT_TIEN_BHXH_CHI.intValue())))
            .andExpect(jsonPath("$.[*].tienNgoaiBhyt").value(hasItem(DEFAULT_TIEN_NGOAI_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].tongTienThanhToan").value(hasItem(DEFAULT_TONG_TIEN_THANH_TOAN.intValue())))
            .andExpect(jsonPath("$.[*].tyLeBhxhThanhToan").value(hasItem(DEFAULT_TY_LE_BHXH_THANH_TOAN)))
            .andExpect(jsonPath("$.[*].doiTuongDacBiet").value(hasItem(DEFAULT_DOI_TUONG_DAC_BIET)))
            .andExpect(jsonPath("$.[*].nguonChi").value(hasItem(DEFAULT_NGUON_CHI)));
    }

    @Test
    @Transactional
    public void getDichVuKhamApDung() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get the dichVuKhamApDung
        restDichVuKhamApDungMockMvc.perform(get("/api/dich-vu-kham-ap-dungs/{id}", dichVuKhamApDung.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(dichVuKhamApDung.getId().intValue()))
            .andExpect(jsonPath("$.enabled").value(DEFAULT_ENABLED))
            .andExpect(jsonPath("$.giaBhyt").value(DEFAULT_GIA_BHYT.intValue()))
            .andExpect(jsonPath("$.giaKhongBhyt").value(DEFAULT_GIA_KHONG_BHYT.intValue()))
            .andExpect(jsonPath("$.maBaoCaoBhyt").value(DEFAULT_MA_BAO_CAO_BHYT))
            .andExpect(jsonPath("$.maBaoCaoByt").value(DEFAULT_MA_BAO_CAO_BYT))
            .andExpect(jsonPath("$.soCongVanBhxh").value(DEFAULT_SO_CONG_VAN_BHXH))
            .andExpect(jsonPath("$.soQuyetDinh").value(DEFAULT_SO_QUYET_DINH))
            .andExpect(jsonPath("$.tenBaoCaoBhxh").value(DEFAULT_TEN_BAO_CAO_BHXH))
            .andExpect(jsonPath("$.tenDichVuKhongBaoHiem").value(DEFAULT_TEN_DICH_VU_KHONG_BAO_HIEM))
            .andExpect(jsonPath("$.tienBenhNhanChi").value(DEFAULT_TIEN_BENH_NHAN_CHI.intValue()))
            .andExpect(jsonPath("$.tienBhxhChi").value(DEFAULT_TIEN_BHXH_CHI.intValue()))
            .andExpect(jsonPath("$.tienNgoaiBhyt").value(DEFAULT_TIEN_NGOAI_BHYT.intValue()))
            .andExpect(jsonPath("$.tongTienThanhToan").value(DEFAULT_TONG_TIEN_THANH_TOAN.intValue()))
            .andExpect(jsonPath("$.tyLeBhxhThanhToan").value(DEFAULT_TY_LE_BHXH_THANH_TOAN))
            .andExpect(jsonPath("$.doiTuongDacBiet").value(DEFAULT_DOI_TUONG_DAC_BIET))
            .andExpect(jsonPath("$.nguonChi").value(DEFAULT_NGUON_CHI));
    }

    @Test
    @Transactional
    public void getDichVuKhamApDungsByIdFiltering() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        Long id = dichVuKhamApDung.getId();

        defaultDichVuKhamApDungShouldBeFound("id.equals=" + id);
        defaultDichVuKhamApDungShouldNotBeFound("id.notEquals=" + id);

        defaultDichVuKhamApDungShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultDichVuKhamApDungShouldNotBeFound("id.greaterThan=" + id);

        defaultDichVuKhamApDungShouldBeFound("id.lessThanOrEqual=" + id);
        defaultDichVuKhamApDungShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByEnabledIsEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where enabled equals to DEFAULT_ENABLED
        defaultDichVuKhamApDungShouldBeFound("enabled.equals=" + DEFAULT_ENABLED);

        // Get all the dichVuKhamApDungList where enabled equals to UPDATED_ENABLED
        defaultDichVuKhamApDungShouldNotBeFound("enabled.equals=" + UPDATED_ENABLED);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByEnabledIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where enabled not equals to DEFAULT_ENABLED
        defaultDichVuKhamApDungShouldNotBeFound("enabled.notEquals=" + DEFAULT_ENABLED);

        // Get all the dichVuKhamApDungList where enabled not equals to UPDATED_ENABLED
        defaultDichVuKhamApDungShouldBeFound("enabled.notEquals=" + UPDATED_ENABLED);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByEnabledIsInShouldWork() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where enabled in DEFAULT_ENABLED or UPDATED_ENABLED
        defaultDichVuKhamApDungShouldBeFound("enabled.in=" + DEFAULT_ENABLED + "," + UPDATED_ENABLED);

        // Get all the dichVuKhamApDungList where enabled equals to UPDATED_ENABLED
        defaultDichVuKhamApDungShouldNotBeFound("enabled.in=" + UPDATED_ENABLED);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByEnabledIsNullOrNotNull() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where enabled is not null
        defaultDichVuKhamApDungShouldBeFound("enabled.specified=true");

        // Get all the dichVuKhamApDungList where enabled is null
        defaultDichVuKhamApDungShouldNotBeFound("enabled.specified=false");
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByEnabledIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where enabled is greater than or equal to DEFAULT_ENABLED
        defaultDichVuKhamApDungShouldBeFound("enabled.greaterThanOrEqual=" + DEFAULT_ENABLED);

        // Get all the dichVuKhamApDungList where enabled is greater than or equal to UPDATED_ENABLED
        defaultDichVuKhamApDungShouldNotBeFound("enabled.greaterThanOrEqual=" + UPDATED_ENABLED);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByEnabledIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where enabled is less than or equal to DEFAULT_ENABLED
        defaultDichVuKhamApDungShouldBeFound("enabled.lessThanOrEqual=" + DEFAULT_ENABLED);

        // Get all the dichVuKhamApDungList where enabled is less than or equal to SMALLER_ENABLED
        defaultDichVuKhamApDungShouldNotBeFound("enabled.lessThanOrEqual=" + SMALLER_ENABLED);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByEnabledIsLessThanSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where enabled is less than DEFAULT_ENABLED
        defaultDichVuKhamApDungShouldNotBeFound("enabled.lessThan=" + DEFAULT_ENABLED);

        // Get all the dichVuKhamApDungList where enabled is less than UPDATED_ENABLED
        defaultDichVuKhamApDungShouldBeFound("enabled.lessThan=" + UPDATED_ENABLED);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByEnabledIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where enabled is greater than DEFAULT_ENABLED
        defaultDichVuKhamApDungShouldNotBeFound("enabled.greaterThan=" + DEFAULT_ENABLED);

        // Get all the dichVuKhamApDungList where enabled is greater than SMALLER_ENABLED
        defaultDichVuKhamApDungShouldBeFound("enabled.greaterThan=" + SMALLER_ENABLED);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByGiaBhytIsEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where giaBhyt equals to DEFAULT_GIA_BHYT
        defaultDichVuKhamApDungShouldBeFound("giaBhyt.equals=" + DEFAULT_GIA_BHYT);

        // Get all the dichVuKhamApDungList where giaBhyt equals to UPDATED_GIA_BHYT
        defaultDichVuKhamApDungShouldNotBeFound("giaBhyt.equals=" + UPDATED_GIA_BHYT);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByGiaBhytIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where giaBhyt not equals to DEFAULT_GIA_BHYT
        defaultDichVuKhamApDungShouldNotBeFound("giaBhyt.notEquals=" + DEFAULT_GIA_BHYT);

        // Get all the dichVuKhamApDungList where giaBhyt not equals to UPDATED_GIA_BHYT
        defaultDichVuKhamApDungShouldBeFound("giaBhyt.notEquals=" + UPDATED_GIA_BHYT);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByGiaBhytIsInShouldWork() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where giaBhyt in DEFAULT_GIA_BHYT or UPDATED_GIA_BHYT
        defaultDichVuKhamApDungShouldBeFound("giaBhyt.in=" + DEFAULT_GIA_BHYT + "," + UPDATED_GIA_BHYT);

        // Get all the dichVuKhamApDungList where giaBhyt equals to UPDATED_GIA_BHYT
        defaultDichVuKhamApDungShouldNotBeFound("giaBhyt.in=" + UPDATED_GIA_BHYT);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByGiaBhytIsNullOrNotNull() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where giaBhyt is not null
        defaultDichVuKhamApDungShouldBeFound("giaBhyt.specified=true");

        // Get all the dichVuKhamApDungList where giaBhyt is null
        defaultDichVuKhamApDungShouldNotBeFound("giaBhyt.specified=false");
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByGiaBhytIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where giaBhyt is greater than or equal to DEFAULT_GIA_BHYT
        defaultDichVuKhamApDungShouldBeFound("giaBhyt.greaterThanOrEqual=" + DEFAULT_GIA_BHYT);

        // Get all the dichVuKhamApDungList where giaBhyt is greater than or equal to UPDATED_GIA_BHYT
        defaultDichVuKhamApDungShouldNotBeFound("giaBhyt.greaterThanOrEqual=" + UPDATED_GIA_BHYT);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByGiaBhytIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where giaBhyt is less than or equal to DEFAULT_GIA_BHYT
        defaultDichVuKhamApDungShouldBeFound("giaBhyt.lessThanOrEqual=" + DEFAULT_GIA_BHYT);

        // Get all the dichVuKhamApDungList where giaBhyt is less than or equal to SMALLER_GIA_BHYT
        defaultDichVuKhamApDungShouldNotBeFound("giaBhyt.lessThanOrEqual=" + SMALLER_GIA_BHYT);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByGiaBhytIsLessThanSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where giaBhyt is less than DEFAULT_GIA_BHYT
        defaultDichVuKhamApDungShouldNotBeFound("giaBhyt.lessThan=" + DEFAULT_GIA_BHYT);

        // Get all the dichVuKhamApDungList where giaBhyt is less than UPDATED_GIA_BHYT
        defaultDichVuKhamApDungShouldBeFound("giaBhyt.lessThan=" + UPDATED_GIA_BHYT);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByGiaBhytIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where giaBhyt is greater than DEFAULT_GIA_BHYT
        defaultDichVuKhamApDungShouldNotBeFound("giaBhyt.greaterThan=" + DEFAULT_GIA_BHYT);

        // Get all the dichVuKhamApDungList where giaBhyt is greater than SMALLER_GIA_BHYT
        defaultDichVuKhamApDungShouldBeFound("giaBhyt.greaterThan=" + SMALLER_GIA_BHYT);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByGiaKhongBhytIsEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where giaKhongBhyt equals to DEFAULT_GIA_KHONG_BHYT
        defaultDichVuKhamApDungShouldBeFound("giaKhongBhyt.equals=" + DEFAULT_GIA_KHONG_BHYT);

        // Get all the dichVuKhamApDungList where giaKhongBhyt equals to UPDATED_GIA_KHONG_BHYT
        defaultDichVuKhamApDungShouldNotBeFound("giaKhongBhyt.equals=" + UPDATED_GIA_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByGiaKhongBhytIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where giaKhongBhyt not equals to DEFAULT_GIA_KHONG_BHYT
        defaultDichVuKhamApDungShouldNotBeFound("giaKhongBhyt.notEquals=" + DEFAULT_GIA_KHONG_BHYT);

        // Get all the dichVuKhamApDungList where giaKhongBhyt not equals to UPDATED_GIA_KHONG_BHYT
        defaultDichVuKhamApDungShouldBeFound("giaKhongBhyt.notEquals=" + UPDATED_GIA_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByGiaKhongBhytIsInShouldWork() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where giaKhongBhyt in DEFAULT_GIA_KHONG_BHYT or UPDATED_GIA_KHONG_BHYT
        defaultDichVuKhamApDungShouldBeFound("giaKhongBhyt.in=" + DEFAULT_GIA_KHONG_BHYT + "," + UPDATED_GIA_KHONG_BHYT);

        // Get all the dichVuKhamApDungList where giaKhongBhyt equals to UPDATED_GIA_KHONG_BHYT
        defaultDichVuKhamApDungShouldNotBeFound("giaKhongBhyt.in=" + UPDATED_GIA_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByGiaKhongBhytIsNullOrNotNull() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where giaKhongBhyt is not null
        defaultDichVuKhamApDungShouldBeFound("giaKhongBhyt.specified=true");

        // Get all the dichVuKhamApDungList where giaKhongBhyt is null
        defaultDichVuKhamApDungShouldNotBeFound("giaKhongBhyt.specified=false");
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByGiaKhongBhytIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where giaKhongBhyt is greater than or equal to DEFAULT_GIA_KHONG_BHYT
        defaultDichVuKhamApDungShouldBeFound("giaKhongBhyt.greaterThanOrEqual=" + DEFAULT_GIA_KHONG_BHYT);

        // Get all the dichVuKhamApDungList where giaKhongBhyt is greater than or equal to UPDATED_GIA_KHONG_BHYT
        defaultDichVuKhamApDungShouldNotBeFound("giaKhongBhyt.greaterThanOrEqual=" + UPDATED_GIA_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByGiaKhongBhytIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where giaKhongBhyt is less than or equal to DEFAULT_GIA_KHONG_BHYT
        defaultDichVuKhamApDungShouldBeFound("giaKhongBhyt.lessThanOrEqual=" + DEFAULT_GIA_KHONG_BHYT);

        // Get all the dichVuKhamApDungList where giaKhongBhyt is less than or equal to SMALLER_GIA_KHONG_BHYT
        defaultDichVuKhamApDungShouldNotBeFound("giaKhongBhyt.lessThanOrEqual=" + SMALLER_GIA_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByGiaKhongBhytIsLessThanSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where giaKhongBhyt is less than DEFAULT_GIA_KHONG_BHYT
        defaultDichVuKhamApDungShouldNotBeFound("giaKhongBhyt.lessThan=" + DEFAULT_GIA_KHONG_BHYT);

        // Get all the dichVuKhamApDungList where giaKhongBhyt is less than UPDATED_GIA_KHONG_BHYT
        defaultDichVuKhamApDungShouldBeFound("giaKhongBhyt.lessThan=" + UPDATED_GIA_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByGiaKhongBhytIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where giaKhongBhyt is greater than DEFAULT_GIA_KHONG_BHYT
        defaultDichVuKhamApDungShouldNotBeFound("giaKhongBhyt.greaterThan=" + DEFAULT_GIA_KHONG_BHYT);

        // Get all the dichVuKhamApDungList where giaKhongBhyt is greater than SMALLER_GIA_KHONG_BHYT
        defaultDichVuKhamApDungShouldBeFound("giaKhongBhyt.greaterThan=" + SMALLER_GIA_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByMaBaoCaoBhytIsEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where maBaoCaoBhyt equals to DEFAULT_MA_BAO_CAO_BHYT
        defaultDichVuKhamApDungShouldBeFound("maBaoCaoBhyt.equals=" + DEFAULT_MA_BAO_CAO_BHYT);

        // Get all the dichVuKhamApDungList where maBaoCaoBhyt equals to UPDATED_MA_BAO_CAO_BHYT
        defaultDichVuKhamApDungShouldNotBeFound("maBaoCaoBhyt.equals=" + UPDATED_MA_BAO_CAO_BHYT);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByMaBaoCaoBhytIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where maBaoCaoBhyt not equals to DEFAULT_MA_BAO_CAO_BHYT
        defaultDichVuKhamApDungShouldNotBeFound("maBaoCaoBhyt.notEquals=" + DEFAULT_MA_BAO_CAO_BHYT);

        // Get all the dichVuKhamApDungList where maBaoCaoBhyt not equals to UPDATED_MA_BAO_CAO_BHYT
        defaultDichVuKhamApDungShouldBeFound("maBaoCaoBhyt.notEquals=" + UPDATED_MA_BAO_CAO_BHYT);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByMaBaoCaoBhytIsInShouldWork() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where maBaoCaoBhyt in DEFAULT_MA_BAO_CAO_BHYT or UPDATED_MA_BAO_CAO_BHYT
        defaultDichVuKhamApDungShouldBeFound("maBaoCaoBhyt.in=" + DEFAULT_MA_BAO_CAO_BHYT + "," + UPDATED_MA_BAO_CAO_BHYT);

        // Get all the dichVuKhamApDungList where maBaoCaoBhyt equals to UPDATED_MA_BAO_CAO_BHYT
        defaultDichVuKhamApDungShouldNotBeFound("maBaoCaoBhyt.in=" + UPDATED_MA_BAO_CAO_BHYT);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByMaBaoCaoBhytIsNullOrNotNull() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where maBaoCaoBhyt is not null
        defaultDichVuKhamApDungShouldBeFound("maBaoCaoBhyt.specified=true");

        // Get all the dichVuKhamApDungList where maBaoCaoBhyt is null
        defaultDichVuKhamApDungShouldNotBeFound("maBaoCaoBhyt.specified=false");
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByMaBaoCaoBhytContainsSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where maBaoCaoBhyt contains DEFAULT_MA_BAO_CAO_BHYT
        defaultDichVuKhamApDungShouldBeFound("maBaoCaoBhyt.contains=" + DEFAULT_MA_BAO_CAO_BHYT);

        // Get all the dichVuKhamApDungList where maBaoCaoBhyt contains UPDATED_MA_BAO_CAO_BHYT
        defaultDichVuKhamApDungShouldNotBeFound("maBaoCaoBhyt.contains=" + UPDATED_MA_BAO_CAO_BHYT);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByMaBaoCaoBhytNotContainsSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where maBaoCaoBhyt does not contain DEFAULT_MA_BAO_CAO_BHYT
        defaultDichVuKhamApDungShouldNotBeFound("maBaoCaoBhyt.doesNotContain=" + DEFAULT_MA_BAO_CAO_BHYT);

        // Get all the dichVuKhamApDungList where maBaoCaoBhyt does not contain UPDATED_MA_BAO_CAO_BHYT
        defaultDichVuKhamApDungShouldBeFound("maBaoCaoBhyt.doesNotContain=" + UPDATED_MA_BAO_CAO_BHYT);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByMaBaoCaoBytIsEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where maBaoCaoByt equals to DEFAULT_MA_BAO_CAO_BYT
        defaultDichVuKhamApDungShouldBeFound("maBaoCaoByt.equals=" + DEFAULT_MA_BAO_CAO_BYT);

        // Get all the dichVuKhamApDungList where maBaoCaoByt equals to UPDATED_MA_BAO_CAO_BYT
        defaultDichVuKhamApDungShouldNotBeFound("maBaoCaoByt.equals=" + UPDATED_MA_BAO_CAO_BYT);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByMaBaoCaoBytIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where maBaoCaoByt not equals to DEFAULT_MA_BAO_CAO_BYT
        defaultDichVuKhamApDungShouldNotBeFound("maBaoCaoByt.notEquals=" + DEFAULT_MA_BAO_CAO_BYT);

        // Get all the dichVuKhamApDungList where maBaoCaoByt not equals to UPDATED_MA_BAO_CAO_BYT
        defaultDichVuKhamApDungShouldBeFound("maBaoCaoByt.notEquals=" + UPDATED_MA_BAO_CAO_BYT);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByMaBaoCaoBytIsInShouldWork() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where maBaoCaoByt in DEFAULT_MA_BAO_CAO_BYT or UPDATED_MA_BAO_CAO_BYT
        defaultDichVuKhamApDungShouldBeFound("maBaoCaoByt.in=" + DEFAULT_MA_BAO_CAO_BYT + "," + UPDATED_MA_BAO_CAO_BYT);

        // Get all the dichVuKhamApDungList where maBaoCaoByt equals to UPDATED_MA_BAO_CAO_BYT
        defaultDichVuKhamApDungShouldNotBeFound("maBaoCaoByt.in=" + UPDATED_MA_BAO_CAO_BYT);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByMaBaoCaoBytIsNullOrNotNull() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where maBaoCaoByt is not null
        defaultDichVuKhamApDungShouldBeFound("maBaoCaoByt.specified=true");

        // Get all the dichVuKhamApDungList where maBaoCaoByt is null
        defaultDichVuKhamApDungShouldNotBeFound("maBaoCaoByt.specified=false");
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByMaBaoCaoBytContainsSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where maBaoCaoByt contains DEFAULT_MA_BAO_CAO_BYT
        defaultDichVuKhamApDungShouldBeFound("maBaoCaoByt.contains=" + DEFAULT_MA_BAO_CAO_BYT);

        // Get all the dichVuKhamApDungList where maBaoCaoByt contains UPDATED_MA_BAO_CAO_BYT
        defaultDichVuKhamApDungShouldNotBeFound("maBaoCaoByt.contains=" + UPDATED_MA_BAO_CAO_BYT);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByMaBaoCaoBytNotContainsSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where maBaoCaoByt does not contain DEFAULT_MA_BAO_CAO_BYT
        defaultDichVuKhamApDungShouldNotBeFound("maBaoCaoByt.doesNotContain=" + DEFAULT_MA_BAO_CAO_BYT);

        // Get all the dichVuKhamApDungList where maBaoCaoByt does not contain UPDATED_MA_BAO_CAO_BYT
        defaultDichVuKhamApDungShouldBeFound("maBaoCaoByt.doesNotContain=" + UPDATED_MA_BAO_CAO_BYT);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsBySoCongVanBhxhIsEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where soCongVanBhxh equals to DEFAULT_SO_CONG_VAN_BHXH
        defaultDichVuKhamApDungShouldBeFound("soCongVanBhxh.equals=" + DEFAULT_SO_CONG_VAN_BHXH);

        // Get all the dichVuKhamApDungList where soCongVanBhxh equals to UPDATED_SO_CONG_VAN_BHXH
        defaultDichVuKhamApDungShouldNotBeFound("soCongVanBhxh.equals=" + UPDATED_SO_CONG_VAN_BHXH);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsBySoCongVanBhxhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where soCongVanBhxh not equals to DEFAULT_SO_CONG_VAN_BHXH
        defaultDichVuKhamApDungShouldNotBeFound("soCongVanBhxh.notEquals=" + DEFAULT_SO_CONG_VAN_BHXH);

        // Get all the dichVuKhamApDungList where soCongVanBhxh not equals to UPDATED_SO_CONG_VAN_BHXH
        defaultDichVuKhamApDungShouldBeFound("soCongVanBhxh.notEquals=" + UPDATED_SO_CONG_VAN_BHXH);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsBySoCongVanBhxhIsInShouldWork() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where soCongVanBhxh in DEFAULT_SO_CONG_VAN_BHXH or UPDATED_SO_CONG_VAN_BHXH
        defaultDichVuKhamApDungShouldBeFound("soCongVanBhxh.in=" + DEFAULT_SO_CONG_VAN_BHXH + "," + UPDATED_SO_CONG_VAN_BHXH);

        // Get all the dichVuKhamApDungList where soCongVanBhxh equals to UPDATED_SO_CONG_VAN_BHXH
        defaultDichVuKhamApDungShouldNotBeFound("soCongVanBhxh.in=" + UPDATED_SO_CONG_VAN_BHXH);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsBySoCongVanBhxhIsNullOrNotNull() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where soCongVanBhxh is not null
        defaultDichVuKhamApDungShouldBeFound("soCongVanBhxh.specified=true");

        // Get all the dichVuKhamApDungList where soCongVanBhxh is null
        defaultDichVuKhamApDungShouldNotBeFound("soCongVanBhxh.specified=false");
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsBySoCongVanBhxhContainsSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where soCongVanBhxh contains DEFAULT_SO_CONG_VAN_BHXH
        defaultDichVuKhamApDungShouldBeFound("soCongVanBhxh.contains=" + DEFAULT_SO_CONG_VAN_BHXH);

        // Get all the dichVuKhamApDungList where soCongVanBhxh contains UPDATED_SO_CONG_VAN_BHXH
        defaultDichVuKhamApDungShouldNotBeFound("soCongVanBhxh.contains=" + UPDATED_SO_CONG_VAN_BHXH);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsBySoCongVanBhxhNotContainsSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where soCongVanBhxh does not contain DEFAULT_SO_CONG_VAN_BHXH
        defaultDichVuKhamApDungShouldNotBeFound("soCongVanBhxh.doesNotContain=" + DEFAULT_SO_CONG_VAN_BHXH);

        // Get all the dichVuKhamApDungList where soCongVanBhxh does not contain UPDATED_SO_CONG_VAN_BHXH
        defaultDichVuKhamApDungShouldBeFound("soCongVanBhxh.doesNotContain=" + UPDATED_SO_CONG_VAN_BHXH);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsBySoQuyetDinhIsEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where soQuyetDinh equals to DEFAULT_SO_QUYET_DINH
        defaultDichVuKhamApDungShouldBeFound("soQuyetDinh.equals=" + DEFAULT_SO_QUYET_DINH);

        // Get all the dichVuKhamApDungList where soQuyetDinh equals to UPDATED_SO_QUYET_DINH
        defaultDichVuKhamApDungShouldNotBeFound("soQuyetDinh.equals=" + UPDATED_SO_QUYET_DINH);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsBySoQuyetDinhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where soQuyetDinh not equals to DEFAULT_SO_QUYET_DINH
        defaultDichVuKhamApDungShouldNotBeFound("soQuyetDinh.notEquals=" + DEFAULT_SO_QUYET_DINH);

        // Get all the dichVuKhamApDungList where soQuyetDinh not equals to UPDATED_SO_QUYET_DINH
        defaultDichVuKhamApDungShouldBeFound("soQuyetDinh.notEquals=" + UPDATED_SO_QUYET_DINH);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsBySoQuyetDinhIsInShouldWork() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where soQuyetDinh in DEFAULT_SO_QUYET_DINH or UPDATED_SO_QUYET_DINH
        defaultDichVuKhamApDungShouldBeFound("soQuyetDinh.in=" + DEFAULT_SO_QUYET_DINH + "," + UPDATED_SO_QUYET_DINH);

        // Get all the dichVuKhamApDungList where soQuyetDinh equals to UPDATED_SO_QUYET_DINH
        defaultDichVuKhamApDungShouldNotBeFound("soQuyetDinh.in=" + UPDATED_SO_QUYET_DINH);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsBySoQuyetDinhIsNullOrNotNull() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where soQuyetDinh is not null
        defaultDichVuKhamApDungShouldBeFound("soQuyetDinh.specified=true");

        // Get all the dichVuKhamApDungList where soQuyetDinh is null
        defaultDichVuKhamApDungShouldNotBeFound("soQuyetDinh.specified=false");
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsBySoQuyetDinhContainsSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where soQuyetDinh contains DEFAULT_SO_QUYET_DINH
        defaultDichVuKhamApDungShouldBeFound("soQuyetDinh.contains=" + DEFAULT_SO_QUYET_DINH);

        // Get all the dichVuKhamApDungList where soQuyetDinh contains UPDATED_SO_QUYET_DINH
        defaultDichVuKhamApDungShouldNotBeFound("soQuyetDinh.contains=" + UPDATED_SO_QUYET_DINH);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsBySoQuyetDinhNotContainsSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where soQuyetDinh does not contain DEFAULT_SO_QUYET_DINH
        defaultDichVuKhamApDungShouldNotBeFound("soQuyetDinh.doesNotContain=" + DEFAULT_SO_QUYET_DINH);

        // Get all the dichVuKhamApDungList where soQuyetDinh does not contain UPDATED_SO_QUYET_DINH
        defaultDichVuKhamApDungShouldBeFound("soQuyetDinh.doesNotContain=" + UPDATED_SO_QUYET_DINH);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTenBaoCaoBhxhIsEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tenBaoCaoBhxh equals to DEFAULT_TEN_BAO_CAO_BHXH
        defaultDichVuKhamApDungShouldBeFound("tenBaoCaoBhxh.equals=" + DEFAULT_TEN_BAO_CAO_BHXH);

        // Get all the dichVuKhamApDungList where tenBaoCaoBhxh equals to UPDATED_TEN_BAO_CAO_BHXH
        defaultDichVuKhamApDungShouldNotBeFound("tenBaoCaoBhxh.equals=" + UPDATED_TEN_BAO_CAO_BHXH);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTenBaoCaoBhxhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tenBaoCaoBhxh not equals to DEFAULT_TEN_BAO_CAO_BHXH
        defaultDichVuKhamApDungShouldNotBeFound("tenBaoCaoBhxh.notEquals=" + DEFAULT_TEN_BAO_CAO_BHXH);

        // Get all the dichVuKhamApDungList where tenBaoCaoBhxh not equals to UPDATED_TEN_BAO_CAO_BHXH
        defaultDichVuKhamApDungShouldBeFound("tenBaoCaoBhxh.notEquals=" + UPDATED_TEN_BAO_CAO_BHXH);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTenBaoCaoBhxhIsInShouldWork() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tenBaoCaoBhxh in DEFAULT_TEN_BAO_CAO_BHXH or UPDATED_TEN_BAO_CAO_BHXH
        defaultDichVuKhamApDungShouldBeFound("tenBaoCaoBhxh.in=" + DEFAULT_TEN_BAO_CAO_BHXH + "," + UPDATED_TEN_BAO_CAO_BHXH);

        // Get all the dichVuKhamApDungList where tenBaoCaoBhxh equals to UPDATED_TEN_BAO_CAO_BHXH
        defaultDichVuKhamApDungShouldNotBeFound("tenBaoCaoBhxh.in=" + UPDATED_TEN_BAO_CAO_BHXH);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTenBaoCaoBhxhIsNullOrNotNull() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tenBaoCaoBhxh is not null
        defaultDichVuKhamApDungShouldBeFound("tenBaoCaoBhxh.specified=true");

        // Get all the dichVuKhamApDungList where tenBaoCaoBhxh is null
        defaultDichVuKhamApDungShouldNotBeFound("tenBaoCaoBhxh.specified=false");
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTenBaoCaoBhxhContainsSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tenBaoCaoBhxh contains DEFAULT_TEN_BAO_CAO_BHXH
        defaultDichVuKhamApDungShouldBeFound("tenBaoCaoBhxh.contains=" + DEFAULT_TEN_BAO_CAO_BHXH);

        // Get all the dichVuKhamApDungList where tenBaoCaoBhxh contains UPDATED_TEN_BAO_CAO_BHXH
        defaultDichVuKhamApDungShouldNotBeFound("tenBaoCaoBhxh.contains=" + UPDATED_TEN_BAO_CAO_BHXH);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTenBaoCaoBhxhNotContainsSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tenBaoCaoBhxh does not contain DEFAULT_TEN_BAO_CAO_BHXH
        defaultDichVuKhamApDungShouldNotBeFound("tenBaoCaoBhxh.doesNotContain=" + DEFAULT_TEN_BAO_CAO_BHXH);

        // Get all the dichVuKhamApDungList where tenBaoCaoBhxh does not contain UPDATED_TEN_BAO_CAO_BHXH
        defaultDichVuKhamApDungShouldBeFound("tenBaoCaoBhxh.doesNotContain=" + UPDATED_TEN_BAO_CAO_BHXH);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTenDichVuKhongBaoHiemIsEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tenDichVuKhongBaoHiem equals to DEFAULT_TEN_DICH_VU_KHONG_BAO_HIEM
        defaultDichVuKhamApDungShouldBeFound("tenDichVuKhongBaoHiem.equals=" + DEFAULT_TEN_DICH_VU_KHONG_BAO_HIEM);

        // Get all the dichVuKhamApDungList where tenDichVuKhongBaoHiem equals to UPDATED_TEN_DICH_VU_KHONG_BAO_HIEM
        defaultDichVuKhamApDungShouldNotBeFound("tenDichVuKhongBaoHiem.equals=" + UPDATED_TEN_DICH_VU_KHONG_BAO_HIEM);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTenDichVuKhongBaoHiemIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tenDichVuKhongBaoHiem not equals to DEFAULT_TEN_DICH_VU_KHONG_BAO_HIEM
        defaultDichVuKhamApDungShouldNotBeFound("tenDichVuKhongBaoHiem.notEquals=" + DEFAULT_TEN_DICH_VU_KHONG_BAO_HIEM);

        // Get all the dichVuKhamApDungList where tenDichVuKhongBaoHiem not equals to UPDATED_TEN_DICH_VU_KHONG_BAO_HIEM
        defaultDichVuKhamApDungShouldBeFound("tenDichVuKhongBaoHiem.notEquals=" + UPDATED_TEN_DICH_VU_KHONG_BAO_HIEM);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTenDichVuKhongBaoHiemIsInShouldWork() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tenDichVuKhongBaoHiem in DEFAULT_TEN_DICH_VU_KHONG_BAO_HIEM or UPDATED_TEN_DICH_VU_KHONG_BAO_HIEM
        defaultDichVuKhamApDungShouldBeFound("tenDichVuKhongBaoHiem.in=" + DEFAULT_TEN_DICH_VU_KHONG_BAO_HIEM + "," + UPDATED_TEN_DICH_VU_KHONG_BAO_HIEM);

        // Get all the dichVuKhamApDungList where tenDichVuKhongBaoHiem equals to UPDATED_TEN_DICH_VU_KHONG_BAO_HIEM
        defaultDichVuKhamApDungShouldNotBeFound("tenDichVuKhongBaoHiem.in=" + UPDATED_TEN_DICH_VU_KHONG_BAO_HIEM);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTenDichVuKhongBaoHiemIsNullOrNotNull() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tenDichVuKhongBaoHiem is not null
        defaultDichVuKhamApDungShouldBeFound("tenDichVuKhongBaoHiem.specified=true");

        // Get all the dichVuKhamApDungList where tenDichVuKhongBaoHiem is null
        defaultDichVuKhamApDungShouldNotBeFound("tenDichVuKhongBaoHiem.specified=false");
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTenDichVuKhongBaoHiemContainsSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tenDichVuKhongBaoHiem contains DEFAULT_TEN_DICH_VU_KHONG_BAO_HIEM
        defaultDichVuKhamApDungShouldBeFound("tenDichVuKhongBaoHiem.contains=" + DEFAULT_TEN_DICH_VU_KHONG_BAO_HIEM);

        // Get all the dichVuKhamApDungList where tenDichVuKhongBaoHiem contains UPDATED_TEN_DICH_VU_KHONG_BAO_HIEM
        defaultDichVuKhamApDungShouldNotBeFound("tenDichVuKhongBaoHiem.contains=" + UPDATED_TEN_DICH_VU_KHONG_BAO_HIEM);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTenDichVuKhongBaoHiemNotContainsSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tenDichVuKhongBaoHiem does not contain DEFAULT_TEN_DICH_VU_KHONG_BAO_HIEM
        defaultDichVuKhamApDungShouldNotBeFound("tenDichVuKhongBaoHiem.doesNotContain=" + DEFAULT_TEN_DICH_VU_KHONG_BAO_HIEM);

        // Get all the dichVuKhamApDungList where tenDichVuKhongBaoHiem does not contain UPDATED_TEN_DICH_VU_KHONG_BAO_HIEM
        defaultDichVuKhamApDungShouldBeFound("tenDichVuKhongBaoHiem.doesNotContain=" + UPDATED_TEN_DICH_VU_KHONG_BAO_HIEM);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTienBenhNhanChiIsEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tienBenhNhanChi equals to DEFAULT_TIEN_BENH_NHAN_CHI
        defaultDichVuKhamApDungShouldBeFound("tienBenhNhanChi.equals=" + DEFAULT_TIEN_BENH_NHAN_CHI);

        // Get all the dichVuKhamApDungList where tienBenhNhanChi equals to UPDATED_TIEN_BENH_NHAN_CHI
        defaultDichVuKhamApDungShouldNotBeFound("tienBenhNhanChi.equals=" + UPDATED_TIEN_BENH_NHAN_CHI);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTienBenhNhanChiIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tienBenhNhanChi not equals to DEFAULT_TIEN_BENH_NHAN_CHI
        defaultDichVuKhamApDungShouldNotBeFound("tienBenhNhanChi.notEquals=" + DEFAULT_TIEN_BENH_NHAN_CHI);

        // Get all the dichVuKhamApDungList where tienBenhNhanChi not equals to UPDATED_TIEN_BENH_NHAN_CHI
        defaultDichVuKhamApDungShouldBeFound("tienBenhNhanChi.notEquals=" + UPDATED_TIEN_BENH_NHAN_CHI);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTienBenhNhanChiIsInShouldWork() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tienBenhNhanChi in DEFAULT_TIEN_BENH_NHAN_CHI or UPDATED_TIEN_BENH_NHAN_CHI
        defaultDichVuKhamApDungShouldBeFound("tienBenhNhanChi.in=" + DEFAULT_TIEN_BENH_NHAN_CHI + "," + UPDATED_TIEN_BENH_NHAN_CHI);

        // Get all the dichVuKhamApDungList where tienBenhNhanChi equals to UPDATED_TIEN_BENH_NHAN_CHI
        defaultDichVuKhamApDungShouldNotBeFound("tienBenhNhanChi.in=" + UPDATED_TIEN_BENH_NHAN_CHI);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTienBenhNhanChiIsNullOrNotNull() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tienBenhNhanChi is not null
        defaultDichVuKhamApDungShouldBeFound("tienBenhNhanChi.specified=true");

        // Get all the dichVuKhamApDungList where tienBenhNhanChi is null
        defaultDichVuKhamApDungShouldNotBeFound("tienBenhNhanChi.specified=false");
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTienBenhNhanChiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tienBenhNhanChi is greater than or equal to DEFAULT_TIEN_BENH_NHAN_CHI
        defaultDichVuKhamApDungShouldBeFound("tienBenhNhanChi.greaterThanOrEqual=" + DEFAULT_TIEN_BENH_NHAN_CHI);

        // Get all the dichVuKhamApDungList where tienBenhNhanChi is greater than or equal to UPDATED_TIEN_BENH_NHAN_CHI
        defaultDichVuKhamApDungShouldNotBeFound("tienBenhNhanChi.greaterThanOrEqual=" + UPDATED_TIEN_BENH_NHAN_CHI);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTienBenhNhanChiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tienBenhNhanChi is less than or equal to DEFAULT_TIEN_BENH_NHAN_CHI
        defaultDichVuKhamApDungShouldBeFound("tienBenhNhanChi.lessThanOrEqual=" + DEFAULT_TIEN_BENH_NHAN_CHI);

        // Get all the dichVuKhamApDungList where tienBenhNhanChi is less than or equal to SMALLER_TIEN_BENH_NHAN_CHI
        defaultDichVuKhamApDungShouldNotBeFound("tienBenhNhanChi.lessThanOrEqual=" + SMALLER_TIEN_BENH_NHAN_CHI);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTienBenhNhanChiIsLessThanSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tienBenhNhanChi is less than DEFAULT_TIEN_BENH_NHAN_CHI
        defaultDichVuKhamApDungShouldNotBeFound("tienBenhNhanChi.lessThan=" + DEFAULT_TIEN_BENH_NHAN_CHI);

        // Get all the dichVuKhamApDungList where tienBenhNhanChi is less than UPDATED_TIEN_BENH_NHAN_CHI
        defaultDichVuKhamApDungShouldBeFound("tienBenhNhanChi.lessThan=" + UPDATED_TIEN_BENH_NHAN_CHI);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTienBenhNhanChiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tienBenhNhanChi is greater than DEFAULT_TIEN_BENH_NHAN_CHI
        defaultDichVuKhamApDungShouldNotBeFound("tienBenhNhanChi.greaterThan=" + DEFAULT_TIEN_BENH_NHAN_CHI);

        // Get all the dichVuKhamApDungList where tienBenhNhanChi is greater than SMALLER_TIEN_BENH_NHAN_CHI
        defaultDichVuKhamApDungShouldBeFound("tienBenhNhanChi.greaterThan=" + SMALLER_TIEN_BENH_NHAN_CHI);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTienBhxhChiIsEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tienBhxhChi equals to DEFAULT_TIEN_BHXH_CHI
        defaultDichVuKhamApDungShouldBeFound("tienBhxhChi.equals=" + DEFAULT_TIEN_BHXH_CHI);

        // Get all the dichVuKhamApDungList where tienBhxhChi equals to UPDATED_TIEN_BHXH_CHI
        defaultDichVuKhamApDungShouldNotBeFound("tienBhxhChi.equals=" + UPDATED_TIEN_BHXH_CHI);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTienBhxhChiIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tienBhxhChi not equals to DEFAULT_TIEN_BHXH_CHI
        defaultDichVuKhamApDungShouldNotBeFound("tienBhxhChi.notEquals=" + DEFAULT_TIEN_BHXH_CHI);

        // Get all the dichVuKhamApDungList where tienBhxhChi not equals to UPDATED_TIEN_BHXH_CHI
        defaultDichVuKhamApDungShouldBeFound("tienBhxhChi.notEquals=" + UPDATED_TIEN_BHXH_CHI);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTienBhxhChiIsInShouldWork() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tienBhxhChi in DEFAULT_TIEN_BHXH_CHI or UPDATED_TIEN_BHXH_CHI
        defaultDichVuKhamApDungShouldBeFound("tienBhxhChi.in=" + DEFAULT_TIEN_BHXH_CHI + "," + UPDATED_TIEN_BHXH_CHI);

        // Get all the dichVuKhamApDungList where tienBhxhChi equals to UPDATED_TIEN_BHXH_CHI
        defaultDichVuKhamApDungShouldNotBeFound("tienBhxhChi.in=" + UPDATED_TIEN_BHXH_CHI);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTienBhxhChiIsNullOrNotNull() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tienBhxhChi is not null
        defaultDichVuKhamApDungShouldBeFound("tienBhxhChi.specified=true");

        // Get all the dichVuKhamApDungList where tienBhxhChi is null
        defaultDichVuKhamApDungShouldNotBeFound("tienBhxhChi.specified=false");
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTienBhxhChiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tienBhxhChi is greater than or equal to DEFAULT_TIEN_BHXH_CHI
        defaultDichVuKhamApDungShouldBeFound("tienBhxhChi.greaterThanOrEqual=" + DEFAULT_TIEN_BHXH_CHI);

        // Get all the dichVuKhamApDungList where tienBhxhChi is greater than or equal to UPDATED_TIEN_BHXH_CHI
        defaultDichVuKhamApDungShouldNotBeFound("tienBhxhChi.greaterThanOrEqual=" + UPDATED_TIEN_BHXH_CHI);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTienBhxhChiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tienBhxhChi is less than or equal to DEFAULT_TIEN_BHXH_CHI
        defaultDichVuKhamApDungShouldBeFound("tienBhxhChi.lessThanOrEqual=" + DEFAULT_TIEN_BHXH_CHI);

        // Get all the dichVuKhamApDungList where tienBhxhChi is less than or equal to SMALLER_TIEN_BHXH_CHI
        defaultDichVuKhamApDungShouldNotBeFound("tienBhxhChi.lessThanOrEqual=" + SMALLER_TIEN_BHXH_CHI);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTienBhxhChiIsLessThanSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tienBhxhChi is less than DEFAULT_TIEN_BHXH_CHI
        defaultDichVuKhamApDungShouldNotBeFound("tienBhxhChi.lessThan=" + DEFAULT_TIEN_BHXH_CHI);

        // Get all the dichVuKhamApDungList where tienBhxhChi is less than UPDATED_TIEN_BHXH_CHI
        defaultDichVuKhamApDungShouldBeFound("tienBhxhChi.lessThan=" + UPDATED_TIEN_BHXH_CHI);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTienBhxhChiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tienBhxhChi is greater than DEFAULT_TIEN_BHXH_CHI
        defaultDichVuKhamApDungShouldNotBeFound("tienBhxhChi.greaterThan=" + DEFAULT_TIEN_BHXH_CHI);

        // Get all the dichVuKhamApDungList where tienBhxhChi is greater than SMALLER_TIEN_BHXH_CHI
        defaultDichVuKhamApDungShouldBeFound("tienBhxhChi.greaterThan=" + SMALLER_TIEN_BHXH_CHI);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTienNgoaiBhytIsEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tienNgoaiBhyt equals to DEFAULT_TIEN_NGOAI_BHYT
        defaultDichVuKhamApDungShouldBeFound("tienNgoaiBhyt.equals=" + DEFAULT_TIEN_NGOAI_BHYT);

        // Get all the dichVuKhamApDungList where tienNgoaiBhyt equals to UPDATED_TIEN_NGOAI_BHYT
        defaultDichVuKhamApDungShouldNotBeFound("tienNgoaiBhyt.equals=" + UPDATED_TIEN_NGOAI_BHYT);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTienNgoaiBhytIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tienNgoaiBhyt not equals to DEFAULT_TIEN_NGOAI_BHYT
        defaultDichVuKhamApDungShouldNotBeFound("tienNgoaiBhyt.notEquals=" + DEFAULT_TIEN_NGOAI_BHYT);

        // Get all the dichVuKhamApDungList where tienNgoaiBhyt not equals to UPDATED_TIEN_NGOAI_BHYT
        defaultDichVuKhamApDungShouldBeFound("tienNgoaiBhyt.notEquals=" + UPDATED_TIEN_NGOAI_BHYT);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTienNgoaiBhytIsInShouldWork() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tienNgoaiBhyt in DEFAULT_TIEN_NGOAI_BHYT or UPDATED_TIEN_NGOAI_BHYT
        defaultDichVuKhamApDungShouldBeFound("tienNgoaiBhyt.in=" + DEFAULT_TIEN_NGOAI_BHYT + "," + UPDATED_TIEN_NGOAI_BHYT);

        // Get all the dichVuKhamApDungList where tienNgoaiBhyt equals to UPDATED_TIEN_NGOAI_BHYT
        defaultDichVuKhamApDungShouldNotBeFound("tienNgoaiBhyt.in=" + UPDATED_TIEN_NGOAI_BHYT);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTienNgoaiBhytIsNullOrNotNull() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tienNgoaiBhyt is not null
        defaultDichVuKhamApDungShouldBeFound("tienNgoaiBhyt.specified=true");

        // Get all the dichVuKhamApDungList where tienNgoaiBhyt is null
        defaultDichVuKhamApDungShouldNotBeFound("tienNgoaiBhyt.specified=false");
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTienNgoaiBhytIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tienNgoaiBhyt is greater than or equal to DEFAULT_TIEN_NGOAI_BHYT
        defaultDichVuKhamApDungShouldBeFound("tienNgoaiBhyt.greaterThanOrEqual=" + DEFAULT_TIEN_NGOAI_BHYT);

        // Get all the dichVuKhamApDungList where tienNgoaiBhyt is greater than or equal to UPDATED_TIEN_NGOAI_BHYT
        defaultDichVuKhamApDungShouldNotBeFound("tienNgoaiBhyt.greaterThanOrEqual=" + UPDATED_TIEN_NGOAI_BHYT);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTienNgoaiBhytIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tienNgoaiBhyt is less than or equal to DEFAULT_TIEN_NGOAI_BHYT
        defaultDichVuKhamApDungShouldBeFound("tienNgoaiBhyt.lessThanOrEqual=" + DEFAULT_TIEN_NGOAI_BHYT);

        // Get all the dichVuKhamApDungList where tienNgoaiBhyt is less than or equal to SMALLER_TIEN_NGOAI_BHYT
        defaultDichVuKhamApDungShouldNotBeFound("tienNgoaiBhyt.lessThanOrEqual=" + SMALLER_TIEN_NGOAI_BHYT);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTienNgoaiBhytIsLessThanSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tienNgoaiBhyt is less than DEFAULT_TIEN_NGOAI_BHYT
        defaultDichVuKhamApDungShouldNotBeFound("tienNgoaiBhyt.lessThan=" + DEFAULT_TIEN_NGOAI_BHYT);

        // Get all the dichVuKhamApDungList where tienNgoaiBhyt is less than UPDATED_TIEN_NGOAI_BHYT
        defaultDichVuKhamApDungShouldBeFound("tienNgoaiBhyt.lessThan=" + UPDATED_TIEN_NGOAI_BHYT);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTienNgoaiBhytIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tienNgoaiBhyt is greater than DEFAULT_TIEN_NGOAI_BHYT
        defaultDichVuKhamApDungShouldNotBeFound("tienNgoaiBhyt.greaterThan=" + DEFAULT_TIEN_NGOAI_BHYT);

        // Get all the dichVuKhamApDungList where tienNgoaiBhyt is greater than SMALLER_TIEN_NGOAI_BHYT
        defaultDichVuKhamApDungShouldBeFound("tienNgoaiBhyt.greaterThan=" + SMALLER_TIEN_NGOAI_BHYT);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTongTienThanhToanIsEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tongTienThanhToan equals to DEFAULT_TONG_TIEN_THANH_TOAN
        defaultDichVuKhamApDungShouldBeFound("tongTienThanhToan.equals=" + DEFAULT_TONG_TIEN_THANH_TOAN);

        // Get all the dichVuKhamApDungList where tongTienThanhToan equals to UPDATED_TONG_TIEN_THANH_TOAN
        defaultDichVuKhamApDungShouldNotBeFound("tongTienThanhToan.equals=" + UPDATED_TONG_TIEN_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTongTienThanhToanIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tongTienThanhToan not equals to DEFAULT_TONG_TIEN_THANH_TOAN
        defaultDichVuKhamApDungShouldNotBeFound("tongTienThanhToan.notEquals=" + DEFAULT_TONG_TIEN_THANH_TOAN);

        // Get all the dichVuKhamApDungList where tongTienThanhToan not equals to UPDATED_TONG_TIEN_THANH_TOAN
        defaultDichVuKhamApDungShouldBeFound("tongTienThanhToan.notEquals=" + UPDATED_TONG_TIEN_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTongTienThanhToanIsInShouldWork() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tongTienThanhToan in DEFAULT_TONG_TIEN_THANH_TOAN or UPDATED_TONG_TIEN_THANH_TOAN
        defaultDichVuKhamApDungShouldBeFound("tongTienThanhToan.in=" + DEFAULT_TONG_TIEN_THANH_TOAN + "," + UPDATED_TONG_TIEN_THANH_TOAN);

        // Get all the dichVuKhamApDungList where tongTienThanhToan equals to UPDATED_TONG_TIEN_THANH_TOAN
        defaultDichVuKhamApDungShouldNotBeFound("tongTienThanhToan.in=" + UPDATED_TONG_TIEN_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTongTienThanhToanIsNullOrNotNull() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tongTienThanhToan is not null
        defaultDichVuKhamApDungShouldBeFound("tongTienThanhToan.specified=true");

        // Get all the dichVuKhamApDungList where tongTienThanhToan is null
        defaultDichVuKhamApDungShouldNotBeFound("tongTienThanhToan.specified=false");
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTongTienThanhToanIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tongTienThanhToan is greater than or equal to DEFAULT_TONG_TIEN_THANH_TOAN
        defaultDichVuKhamApDungShouldBeFound("tongTienThanhToan.greaterThanOrEqual=" + DEFAULT_TONG_TIEN_THANH_TOAN);

        // Get all the dichVuKhamApDungList where tongTienThanhToan is greater than or equal to UPDATED_TONG_TIEN_THANH_TOAN
        defaultDichVuKhamApDungShouldNotBeFound("tongTienThanhToan.greaterThanOrEqual=" + UPDATED_TONG_TIEN_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTongTienThanhToanIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tongTienThanhToan is less than or equal to DEFAULT_TONG_TIEN_THANH_TOAN
        defaultDichVuKhamApDungShouldBeFound("tongTienThanhToan.lessThanOrEqual=" + DEFAULT_TONG_TIEN_THANH_TOAN);

        // Get all the dichVuKhamApDungList where tongTienThanhToan is less than or equal to SMALLER_TONG_TIEN_THANH_TOAN
        defaultDichVuKhamApDungShouldNotBeFound("tongTienThanhToan.lessThanOrEqual=" + SMALLER_TONG_TIEN_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTongTienThanhToanIsLessThanSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tongTienThanhToan is less than DEFAULT_TONG_TIEN_THANH_TOAN
        defaultDichVuKhamApDungShouldNotBeFound("tongTienThanhToan.lessThan=" + DEFAULT_TONG_TIEN_THANH_TOAN);

        // Get all the dichVuKhamApDungList where tongTienThanhToan is less than UPDATED_TONG_TIEN_THANH_TOAN
        defaultDichVuKhamApDungShouldBeFound("tongTienThanhToan.lessThan=" + UPDATED_TONG_TIEN_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTongTienThanhToanIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tongTienThanhToan is greater than DEFAULT_TONG_TIEN_THANH_TOAN
        defaultDichVuKhamApDungShouldNotBeFound("tongTienThanhToan.greaterThan=" + DEFAULT_TONG_TIEN_THANH_TOAN);

        // Get all the dichVuKhamApDungList where tongTienThanhToan is greater than SMALLER_TONG_TIEN_THANH_TOAN
        defaultDichVuKhamApDungShouldBeFound("tongTienThanhToan.greaterThan=" + SMALLER_TONG_TIEN_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTyLeBhxhThanhToanIsEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tyLeBhxhThanhToan equals to DEFAULT_TY_LE_BHXH_THANH_TOAN
        defaultDichVuKhamApDungShouldBeFound("tyLeBhxhThanhToan.equals=" + DEFAULT_TY_LE_BHXH_THANH_TOAN);

        // Get all the dichVuKhamApDungList where tyLeBhxhThanhToan equals to UPDATED_TY_LE_BHXH_THANH_TOAN
        defaultDichVuKhamApDungShouldNotBeFound("tyLeBhxhThanhToan.equals=" + UPDATED_TY_LE_BHXH_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTyLeBhxhThanhToanIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tyLeBhxhThanhToan not equals to DEFAULT_TY_LE_BHXH_THANH_TOAN
        defaultDichVuKhamApDungShouldNotBeFound("tyLeBhxhThanhToan.notEquals=" + DEFAULT_TY_LE_BHXH_THANH_TOAN);

        // Get all the dichVuKhamApDungList where tyLeBhxhThanhToan not equals to UPDATED_TY_LE_BHXH_THANH_TOAN
        defaultDichVuKhamApDungShouldBeFound("tyLeBhxhThanhToan.notEquals=" + UPDATED_TY_LE_BHXH_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTyLeBhxhThanhToanIsInShouldWork() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tyLeBhxhThanhToan in DEFAULT_TY_LE_BHXH_THANH_TOAN or UPDATED_TY_LE_BHXH_THANH_TOAN
        defaultDichVuKhamApDungShouldBeFound("tyLeBhxhThanhToan.in=" + DEFAULT_TY_LE_BHXH_THANH_TOAN + "," + UPDATED_TY_LE_BHXH_THANH_TOAN);

        // Get all the dichVuKhamApDungList where tyLeBhxhThanhToan equals to UPDATED_TY_LE_BHXH_THANH_TOAN
        defaultDichVuKhamApDungShouldNotBeFound("tyLeBhxhThanhToan.in=" + UPDATED_TY_LE_BHXH_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTyLeBhxhThanhToanIsNullOrNotNull() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tyLeBhxhThanhToan is not null
        defaultDichVuKhamApDungShouldBeFound("tyLeBhxhThanhToan.specified=true");

        // Get all the dichVuKhamApDungList where tyLeBhxhThanhToan is null
        defaultDichVuKhamApDungShouldNotBeFound("tyLeBhxhThanhToan.specified=false");
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTyLeBhxhThanhToanIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tyLeBhxhThanhToan is greater than or equal to DEFAULT_TY_LE_BHXH_THANH_TOAN
        defaultDichVuKhamApDungShouldBeFound("tyLeBhxhThanhToan.greaterThanOrEqual=" + DEFAULT_TY_LE_BHXH_THANH_TOAN);

        // Get all the dichVuKhamApDungList where tyLeBhxhThanhToan is greater than or equal to UPDATED_TY_LE_BHXH_THANH_TOAN
        defaultDichVuKhamApDungShouldNotBeFound("tyLeBhxhThanhToan.greaterThanOrEqual=" + UPDATED_TY_LE_BHXH_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTyLeBhxhThanhToanIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tyLeBhxhThanhToan is less than or equal to DEFAULT_TY_LE_BHXH_THANH_TOAN
        defaultDichVuKhamApDungShouldBeFound("tyLeBhxhThanhToan.lessThanOrEqual=" + DEFAULT_TY_LE_BHXH_THANH_TOAN);

        // Get all the dichVuKhamApDungList where tyLeBhxhThanhToan is less than or equal to SMALLER_TY_LE_BHXH_THANH_TOAN
        defaultDichVuKhamApDungShouldNotBeFound("tyLeBhxhThanhToan.lessThanOrEqual=" + SMALLER_TY_LE_BHXH_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTyLeBhxhThanhToanIsLessThanSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tyLeBhxhThanhToan is less than DEFAULT_TY_LE_BHXH_THANH_TOAN
        defaultDichVuKhamApDungShouldNotBeFound("tyLeBhxhThanhToan.lessThan=" + DEFAULT_TY_LE_BHXH_THANH_TOAN);

        // Get all the dichVuKhamApDungList where tyLeBhxhThanhToan is less than UPDATED_TY_LE_BHXH_THANH_TOAN
        defaultDichVuKhamApDungShouldBeFound("tyLeBhxhThanhToan.lessThan=" + UPDATED_TY_LE_BHXH_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByTyLeBhxhThanhToanIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where tyLeBhxhThanhToan is greater than DEFAULT_TY_LE_BHXH_THANH_TOAN
        defaultDichVuKhamApDungShouldNotBeFound("tyLeBhxhThanhToan.greaterThan=" + DEFAULT_TY_LE_BHXH_THANH_TOAN);

        // Get all the dichVuKhamApDungList where tyLeBhxhThanhToan is greater than SMALLER_TY_LE_BHXH_THANH_TOAN
        defaultDichVuKhamApDungShouldBeFound("tyLeBhxhThanhToan.greaterThan=" + SMALLER_TY_LE_BHXH_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByDoiTuongDacBietIsEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where doiTuongDacBiet equals to DEFAULT_DOI_TUONG_DAC_BIET
        defaultDichVuKhamApDungShouldBeFound("doiTuongDacBiet.equals=" + DEFAULT_DOI_TUONG_DAC_BIET);

        // Get all the dichVuKhamApDungList where doiTuongDacBiet equals to UPDATED_DOI_TUONG_DAC_BIET
        defaultDichVuKhamApDungShouldNotBeFound("doiTuongDacBiet.equals=" + UPDATED_DOI_TUONG_DAC_BIET);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByDoiTuongDacBietIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where doiTuongDacBiet not equals to DEFAULT_DOI_TUONG_DAC_BIET
        defaultDichVuKhamApDungShouldNotBeFound("doiTuongDacBiet.notEquals=" + DEFAULT_DOI_TUONG_DAC_BIET);

        // Get all the dichVuKhamApDungList where doiTuongDacBiet not equals to UPDATED_DOI_TUONG_DAC_BIET
        defaultDichVuKhamApDungShouldBeFound("doiTuongDacBiet.notEquals=" + UPDATED_DOI_TUONG_DAC_BIET);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByDoiTuongDacBietIsInShouldWork() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where doiTuongDacBiet in DEFAULT_DOI_TUONG_DAC_BIET or UPDATED_DOI_TUONG_DAC_BIET
        defaultDichVuKhamApDungShouldBeFound("doiTuongDacBiet.in=" + DEFAULT_DOI_TUONG_DAC_BIET + "," + UPDATED_DOI_TUONG_DAC_BIET);

        // Get all the dichVuKhamApDungList where doiTuongDacBiet equals to UPDATED_DOI_TUONG_DAC_BIET
        defaultDichVuKhamApDungShouldNotBeFound("doiTuongDacBiet.in=" + UPDATED_DOI_TUONG_DAC_BIET);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByDoiTuongDacBietIsNullOrNotNull() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where doiTuongDacBiet is not null
        defaultDichVuKhamApDungShouldBeFound("doiTuongDacBiet.specified=true");

        // Get all the dichVuKhamApDungList where doiTuongDacBiet is null
        defaultDichVuKhamApDungShouldNotBeFound("doiTuongDacBiet.specified=false");
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByDoiTuongDacBietIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where doiTuongDacBiet is greater than or equal to DEFAULT_DOI_TUONG_DAC_BIET
        defaultDichVuKhamApDungShouldBeFound("doiTuongDacBiet.greaterThanOrEqual=" + DEFAULT_DOI_TUONG_DAC_BIET);

        // Get all the dichVuKhamApDungList where doiTuongDacBiet is greater than or equal to UPDATED_DOI_TUONG_DAC_BIET
        defaultDichVuKhamApDungShouldNotBeFound("doiTuongDacBiet.greaterThanOrEqual=" + UPDATED_DOI_TUONG_DAC_BIET);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByDoiTuongDacBietIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where doiTuongDacBiet is less than or equal to DEFAULT_DOI_TUONG_DAC_BIET
        defaultDichVuKhamApDungShouldBeFound("doiTuongDacBiet.lessThanOrEqual=" + DEFAULT_DOI_TUONG_DAC_BIET);

        // Get all the dichVuKhamApDungList where doiTuongDacBiet is less than or equal to SMALLER_DOI_TUONG_DAC_BIET
        defaultDichVuKhamApDungShouldNotBeFound("doiTuongDacBiet.lessThanOrEqual=" + SMALLER_DOI_TUONG_DAC_BIET);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByDoiTuongDacBietIsLessThanSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where doiTuongDacBiet is less than DEFAULT_DOI_TUONG_DAC_BIET
        defaultDichVuKhamApDungShouldNotBeFound("doiTuongDacBiet.lessThan=" + DEFAULT_DOI_TUONG_DAC_BIET);

        // Get all the dichVuKhamApDungList where doiTuongDacBiet is less than UPDATED_DOI_TUONG_DAC_BIET
        defaultDichVuKhamApDungShouldBeFound("doiTuongDacBiet.lessThan=" + UPDATED_DOI_TUONG_DAC_BIET);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByDoiTuongDacBietIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where doiTuongDacBiet is greater than DEFAULT_DOI_TUONG_DAC_BIET
        defaultDichVuKhamApDungShouldNotBeFound("doiTuongDacBiet.greaterThan=" + DEFAULT_DOI_TUONG_DAC_BIET);

        // Get all the dichVuKhamApDungList where doiTuongDacBiet is greater than SMALLER_DOI_TUONG_DAC_BIET
        defaultDichVuKhamApDungShouldBeFound("doiTuongDacBiet.greaterThan=" + SMALLER_DOI_TUONG_DAC_BIET);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByNguonChiIsEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where nguonChi equals to DEFAULT_NGUON_CHI
        defaultDichVuKhamApDungShouldBeFound("nguonChi.equals=" + DEFAULT_NGUON_CHI);

        // Get all the dichVuKhamApDungList where nguonChi equals to UPDATED_NGUON_CHI
        defaultDichVuKhamApDungShouldNotBeFound("nguonChi.equals=" + UPDATED_NGUON_CHI);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByNguonChiIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where nguonChi not equals to DEFAULT_NGUON_CHI
        defaultDichVuKhamApDungShouldNotBeFound("nguonChi.notEquals=" + DEFAULT_NGUON_CHI);

        // Get all the dichVuKhamApDungList where nguonChi not equals to UPDATED_NGUON_CHI
        defaultDichVuKhamApDungShouldBeFound("nguonChi.notEquals=" + UPDATED_NGUON_CHI);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByNguonChiIsInShouldWork() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where nguonChi in DEFAULT_NGUON_CHI or UPDATED_NGUON_CHI
        defaultDichVuKhamApDungShouldBeFound("nguonChi.in=" + DEFAULT_NGUON_CHI + "," + UPDATED_NGUON_CHI);

        // Get all the dichVuKhamApDungList where nguonChi equals to UPDATED_NGUON_CHI
        defaultDichVuKhamApDungShouldNotBeFound("nguonChi.in=" + UPDATED_NGUON_CHI);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByNguonChiIsNullOrNotNull() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where nguonChi is not null
        defaultDichVuKhamApDungShouldBeFound("nguonChi.specified=true");

        // Get all the dichVuKhamApDungList where nguonChi is null
        defaultDichVuKhamApDungShouldNotBeFound("nguonChi.specified=false");
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByNguonChiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where nguonChi is greater than or equal to DEFAULT_NGUON_CHI
        defaultDichVuKhamApDungShouldBeFound("nguonChi.greaterThanOrEqual=" + DEFAULT_NGUON_CHI);

        // Get all the dichVuKhamApDungList where nguonChi is greater than or equal to UPDATED_NGUON_CHI
        defaultDichVuKhamApDungShouldNotBeFound("nguonChi.greaterThanOrEqual=" + UPDATED_NGUON_CHI);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByNguonChiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where nguonChi is less than or equal to DEFAULT_NGUON_CHI
        defaultDichVuKhamApDungShouldBeFound("nguonChi.lessThanOrEqual=" + DEFAULT_NGUON_CHI);

        // Get all the dichVuKhamApDungList where nguonChi is less than or equal to SMALLER_NGUON_CHI
        defaultDichVuKhamApDungShouldNotBeFound("nguonChi.lessThanOrEqual=" + SMALLER_NGUON_CHI);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByNguonChiIsLessThanSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where nguonChi is less than DEFAULT_NGUON_CHI
        defaultDichVuKhamApDungShouldNotBeFound("nguonChi.lessThan=" + DEFAULT_NGUON_CHI);

        // Get all the dichVuKhamApDungList where nguonChi is less than UPDATED_NGUON_CHI
        defaultDichVuKhamApDungShouldBeFound("nguonChi.lessThan=" + UPDATED_NGUON_CHI);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByNguonChiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        // Get all the dichVuKhamApDungList where nguonChi is greater than DEFAULT_NGUON_CHI
        defaultDichVuKhamApDungShouldNotBeFound("nguonChi.greaterThan=" + DEFAULT_NGUON_CHI);

        // Get all the dichVuKhamApDungList where nguonChi is greater than SMALLER_NGUON_CHI
        defaultDichVuKhamApDungShouldBeFound("nguonChi.greaterThan=" + SMALLER_NGUON_CHI);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByDotGiaDichVuBhxhIdIsEqualToSomething() throws Exception {
        // Get already existing entity
        DotGiaDichVuBhxh dotGiaDichVuBhxh = dichVuKhamApDung.getDotGiaDichVuBhxh();
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);
        Long dotGiaDichVuBhxhId = dotGiaDichVuBhxh.getId();

        // Get all the dichVuKhamApDungList where dotGiaDichVuBhxhId equals to dotGiaDichVuBhxhId
        defaultDichVuKhamApDungShouldBeFound("dotGiaDichVuBhxhId.equals=" + dotGiaDichVuBhxhId);

        // Get all the dichVuKhamApDungList where dotGiaDichVuBhxhId equals to dotGiaDichVuBhxhId + 1
        defaultDichVuKhamApDungShouldNotBeFound("dotGiaDichVuBhxhId.equals=" + (dotGiaDichVuBhxhId + 1));
    }

    @Test
    @Transactional
    public void getAllDichVuKhamApDungsByDichVuKhamIdIsEqualToSomething() throws Exception {
        // Get already existing entity
        DichVuKham dichVuKham = dichVuKhamApDung.getDichVuKham();
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);
        Long dichVuKhamId = dichVuKham.getId();

        // Get all the dichVuKhamApDungList where dichVuKhamId equals to dichVuKhamId
        defaultDichVuKhamApDungShouldBeFound("dichVuKhamId.equals=" + dichVuKhamId);

        // Get all the dichVuKhamApDungList where dichVuKhamId equals to dichVuKhamId + 1
        defaultDichVuKhamApDungShouldNotBeFound("dichVuKhamId.equals=" + (dichVuKhamId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultDichVuKhamApDungShouldBeFound(String filter) throws Exception {
        restDichVuKhamApDungMockMvc.perform(get("/api/dich-vu-kham-ap-dungs?" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dichVuKhamApDung.getId().intValue())))
            .andExpect(jsonPath("$.[*].enabled").value(hasItem(DEFAULT_ENABLED)))
            .andExpect(jsonPath("$.[*].giaBhyt").value(hasItem(DEFAULT_GIA_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].giaKhongBhyt").value(hasItem(DEFAULT_GIA_KHONG_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].maBaoCaoBhyt").value(hasItem(DEFAULT_MA_BAO_CAO_BHYT)))
            .andExpect(jsonPath("$.[*].maBaoCaoByt").value(hasItem(DEFAULT_MA_BAO_CAO_BYT)))
            .andExpect(jsonPath("$.[*].soCongVanBhxh").value(hasItem(DEFAULT_SO_CONG_VAN_BHXH)))
            .andExpect(jsonPath("$.[*].soQuyetDinh").value(hasItem(DEFAULT_SO_QUYET_DINH)))
            .andExpect(jsonPath("$.[*].tenBaoCaoBhxh").value(hasItem(DEFAULT_TEN_BAO_CAO_BHXH)))
            .andExpect(jsonPath("$.[*].tenDichVuKhongBaoHiem").value(hasItem(DEFAULT_TEN_DICH_VU_KHONG_BAO_HIEM)))
            .andExpect(jsonPath("$.[*].tienBenhNhanChi").value(hasItem(DEFAULT_TIEN_BENH_NHAN_CHI.intValue())))
            .andExpect(jsonPath("$.[*].tienBhxhChi").value(hasItem(DEFAULT_TIEN_BHXH_CHI.intValue())))
            .andExpect(jsonPath("$.[*].tienNgoaiBhyt").value(hasItem(DEFAULT_TIEN_NGOAI_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].tongTienThanhToan").value(hasItem(DEFAULT_TONG_TIEN_THANH_TOAN.intValue())))
            .andExpect(jsonPath("$.[*].tyLeBhxhThanhToan").value(hasItem(DEFAULT_TY_LE_BHXH_THANH_TOAN)))
            .andExpect(jsonPath("$.[*].doiTuongDacBiet").value(hasItem(DEFAULT_DOI_TUONG_DAC_BIET)))
            .andExpect(jsonPath("$.[*].nguonChi").value(hasItem(DEFAULT_NGUON_CHI)));

        // Check, that the count call also returns 1
        restDichVuKhamApDungMockMvc.perform(get("/api/dich-vu-kham-ap-dungs/count?" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultDichVuKhamApDungShouldNotBeFound(String filter) throws Exception {
        restDichVuKhamApDungMockMvc.perform(get("/api/dich-vu-kham-ap-dungs?" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restDichVuKhamApDungMockMvc.perform(get("/api/dich-vu-kham-ap-dungs/count?" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingDichVuKhamApDung() throws Exception {
        // Get the dichVuKhamApDung
        restDichVuKhamApDungMockMvc.perform(get("/api/dich-vu-kham-ap-dungs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDichVuKhamApDung() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        int databaseSizeBeforeUpdate = dichVuKhamApDungRepository.findAll().size();

        // Update the dichVuKhamApDung
        DichVuKhamApDung updatedDichVuKhamApDung = dichVuKhamApDungRepository.findById(dichVuKhamApDung.getId()).get();
        // Disconnect from session so that the updates on updatedDichVuKhamApDung are not directly saved in db
        em.detach(updatedDichVuKhamApDung);
        updatedDichVuKhamApDung
            .enabled(UPDATED_ENABLED)
            .giaBhyt(UPDATED_GIA_BHYT)
            .giaKhongBhyt(UPDATED_GIA_KHONG_BHYT)
            .maBaoCaoBhyt(UPDATED_MA_BAO_CAO_BHYT)
            .maBaoCaoByt(UPDATED_MA_BAO_CAO_BYT)
            .soCongVanBhxh(UPDATED_SO_CONG_VAN_BHXH)
            .soQuyetDinh(UPDATED_SO_QUYET_DINH)
            .tenBaoCaoBhxh(UPDATED_TEN_BAO_CAO_BHXH)
            .tenDichVuKhongBaoHiem(UPDATED_TEN_DICH_VU_KHONG_BAO_HIEM)
            .tienBenhNhanChi(UPDATED_TIEN_BENH_NHAN_CHI)
            .tienBhxhChi(UPDATED_TIEN_BHXH_CHI)
            .tienNgoaiBhyt(UPDATED_TIEN_NGOAI_BHYT)
            .tongTienThanhToan(UPDATED_TONG_TIEN_THANH_TOAN)
            .tyLeBhxhThanhToan(UPDATED_TY_LE_BHXH_THANH_TOAN)
            .doiTuongDacBiet(UPDATED_DOI_TUONG_DAC_BIET)
            .nguonChi(UPDATED_NGUON_CHI);
        DichVuKhamApDungDTO dichVuKhamApDungDTO = dichVuKhamApDungMapper.toDto(updatedDichVuKhamApDung);

        restDichVuKhamApDungMockMvc.perform(put("/api/dich-vu-kham-ap-dungs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dichVuKhamApDungDTO)))
            .andExpect(status().isOk());

        // Validate the DichVuKhamApDung in the database
        List<DichVuKhamApDung> dichVuKhamApDungList = dichVuKhamApDungRepository.findAll();
        assertThat(dichVuKhamApDungList).hasSize(databaseSizeBeforeUpdate);
        DichVuKhamApDung testDichVuKhamApDung = dichVuKhamApDungList.get(dichVuKhamApDungList.size() - 1);
        assertThat(testDichVuKhamApDung.getEnabled()).isEqualTo(UPDATED_ENABLED);
        assertThat(testDichVuKhamApDung.getGiaBhyt()).isEqualTo(UPDATED_GIA_BHYT);
        assertThat(testDichVuKhamApDung.getGiaKhongBhyt()).isEqualTo(UPDATED_GIA_KHONG_BHYT);
        assertThat(testDichVuKhamApDung.getMaBaoCaoBhyt()).isEqualTo(UPDATED_MA_BAO_CAO_BHYT);
        assertThat(testDichVuKhamApDung.getMaBaoCaoByt()).isEqualTo(UPDATED_MA_BAO_CAO_BYT);
        assertThat(testDichVuKhamApDung.getSoCongVanBhxh()).isEqualTo(UPDATED_SO_CONG_VAN_BHXH);
        assertThat(testDichVuKhamApDung.getSoQuyetDinh()).isEqualTo(UPDATED_SO_QUYET_DINH);
        assertThat(testDichVuKhamApDung.getTenBaoCaoBhxh()).isEqualTo(UPDATED_TEN_BAO_CAO_BHXH);
        assertThat(testDichVuKhamApDung.getTenDichVuKhongBaoHiem()).isEqualTo(UPDATED_TEN_DICH_VU_KHONG_BAO_HIEM);
        assertThat(testDichVuKhamApDung.getTienBenhNhanChi()).isEqualTo(UPDATED_TIEN_BENH_NHAN_CHI);
        assertThat(testDichVuKhamApDung.getTienBhxhChi()).isEqualTo(UPDATED_TIEN_BHXH_CHI);
        assertThat(testDichVuKhamApDung.getTienNgoaiBhyt()).isEqualTo(UPDATED_TIEN_NGOAI_BHYT);
        assertThat(testDichVuKhamApDung.getTongTienThanhToan()).isEqualTo(UPDATED_TONG_TIEN_THANH_TOAN);
        assertThat(testDichVuKhamApDung.getTyLeBhxhThanhToan()).isEqualTo(UPDATED_TY_LE_BHXH_THANH_TOAN);
        assertThat(testDichVuKhamApDung.getDoiTuongDacBiet()).isEqualTo(UPDATED_DOI_TUONG_DAC_BIET);
        assertThat(testDichVuKhamApDung.getNguonChi()).isEqualTo(UPDATED_NGUON_CHI);
    }

    @Test
    @Transactional
    public void updateNonExistingDichVuKhamApDung() throws Exception {
        int databaseSizeBeforeUpdate = dichVuKhamApDungRepository.findAll().size();

        // Create the DichVuKhamApDung
        DichVuKhamApDungDTO dichVuKhamApDungDTO = dichVuKhamApDungMapper.toDto(dichVuKhamApDung);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDichVuKhamApDungMockMvc.perform(put("/api/dich-vu-kham-ap-dungs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dichVuKhamApDungDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DichVuKhamApDung in the database
        List<DichVuKhamApDung> dichVuKhamApDungList = dichVuKhamApDungRepository.findAll();
        assertThat(dichVuKhamApDungList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDichVuKhamApDung() throws Exception {
        // Initialize the database
        dichVuKhamApDungRepository.saveAndFlush(dichVuKhamApDung);

        int databaseSizeBeforeDelete = dichVuKhamApDungRepository.findAll().size();

        // Delete the dichVuKhamApDung
        restDichVuKhamApDungMockMvc.perform(delete("/api/dich-vu-kham-ap-dungs/{id}", dichVuKhamApDung.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DichVuKhamApDung> dichVuKhamApDungList = dichVuKhamApDungRepository.findAll();
        assertThat(dichVuKhamApDungList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

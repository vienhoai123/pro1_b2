package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.TPhongNhanVien;
import vn.vnpt.domain.Phong;
import vn.vnpt.domain.NhanVien;
import vn.vnpt.repository.TPhongNhanVienRepository;
import vn.vnpt.service.TPhongNhanVienService;
import vn.vnpt.service.dto.TPhongNhanVienDTO;
import vn.vnpt.service.mapper.TPhongNhanVienMapper;
import vn.vnpt.service.dto.TPhongNhanVienCriteria;
import vn.vnpt.service.TPhongNhanVienQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TPhongNhanVienResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class TPhongNhanVienResourceIT {

    private static final Boolean DEFAULT_ENABLED = false;
    private static final Boolean UPDATED_ENABLED = true;

    @Autowired
    private TPhongNhanVienRepository tPhongNhanVienRepository;

    @Autowired
    private TPhongNhanVienMapper tPhongNhanVienMapper;

    @Autowired
    private TPhongNhanVienService tPhongNhanVienService;

    @Autowired
    private TPhongNhanVienQueryService tPhongNhanVienQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restTPhongNhanVienMockMvc;

    private TPhongNhanVien tPhongNhanVien;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TPhongNhanVien createEntity(EntityManager em) {
        TPhongNhanVien tPhongNhanVien = new TPhongNhanVien()
            .enabled(DEFAULT_ENABLED);
        // Add required entity
        Phong phong;
        if (TestUtil.findAll(em, Phong.class).isEmpty()) {
            phong = PhongResourceIT.createEntity(em);
            em.persist(phong);
            em.flush();
        } else {
            phong = TestUtil.findAll(em, Phong.class).get(0);
        }
        tPhongNhanVien.setPhong(phong);
        // Add required entity
        NhanVien nhanVien;
        if (TestUtil.findAll(em, NhanVien.class).isEmpty()) {
            nhanVien = NhanVienResourceIT.createEntity(em);
            em.persist(nhanVien);
            em.flush();
        } else {
            nhanVien = TestUtil.findAll(em, NhanVien.class).get(0);
        }
        tPhongNhanVien.setNhanVien(nhanVien);
        return tPhongNhanVien;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TPhongNhanVien createUpdatedEntity(EntityManager em) {
        TPhongNhanVien tPhongNhanVien = new TPhongNhanVien()
            .enabled(UPDATED_ENABLED);
        // Add required entity
        Phong phong;
        if (TestUtil.findAll(em, Phong.class).isEmpty()) {
            phong = PhongResourceIT.createUpdatedEntity(em);
            em.persist(phong);
            em.flush();
        } else {
            phong = TestUtil.findAll(em, Phong.class).get(0);
        }
        tPhongNhanVien.setPhong(phong);
        // Add required entity
        NhanVien nhanVien;
        if (TestUtil.findAll(em, NhanVien.class).isEmpty()) {
            nhanVien = NhanVienResourceIT.createUpdatedEntity(em);
            em.persist(nhanVien);
            em.flush();
        } else {
            nhanVien = TestUtil.findAll(em, NhanVien.class).get(0);
        }
        tPhongNhanVien.setNhanVien(nhanVien);
        return tPhongNhanVien;
    }

    @BeforeEach
    public void initTest() {
        tPhongNhanVien = createEntity(em);
    }

    @Test
    @Transactional
    public void createTPhongNhanVien() throws Exception {
        int databaseSizeBeforeCreate = tPhongNhanVienRepository.findAll().size();

        // Create the TPhongNhanVien
        TPhongNhanVienDTO tPhongNhanVienDTO = tPhongNhanVienMapper.toDto(tPhongNhanVien);
        restTPhongNhanVienMockMvc.perform(post("/api/t-phong-nhan-viens").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tPhongNhanVienDTO)))
            .andExpect(status().isCreated());

        // Validate the TPhongNhanVien in the database
        List<TPhongNhanVien> tPhongNhanVienList = tPhongNhanVienRepository.findAll();
        assertThat(tPhongNhanVienList).hasSize(databaseSizeBeforeCreate + 1);
        TPhongNhanVien testTPhongNhanVien = tPhongNhanVienList.get(tPhongNhanVienList.size() - 1);
        assertThat(testTPhongNhanVien.isEnabled()).isEqualTo(DEFAULT_ENABLED);
    }

    @Test
    @Transactional
    public void createTPhongNhanVienWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tPhongNhanVienRepository.findAll().size();

        // Create the TPhongNhanVien with an existing ID
        tPhongNhanVien.setId(1L);
        TPhongNhanVienDTO tPhongNhanVienDTO = tPhongNhanVienMapper.toDto(tPhongNhanVien);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTPhongNhanVienMockMvc.perform(post("/api/t-phong-nhan-viens").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tPhongNhanVienDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TPhongNhanVien in the database
        List<TPhongNhanVien> tPhongNhanVienList = tPhongNhanVienRepository.findAll();
        assertThat(tPhongNhanVienList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkEnabledIsRequired() throws Exception {
        int databaseSizeBeforeTest = tPhongNhanVienRepository.findAll().size();
        // set the field null
        tPhongNhanVien.setEnabled(null);

        // Create the TPhongNhanVien, which fails.
        TPhongNhanVienDTO tPhongNhanVienDTO = tPhongNhanVienMapper.toDto(tPhongNhanVien);

        restTPhongNhanVienMockMvc.perform(post("/api/t-phong-nhan-viens").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tPhongNhanVienDTO)))
            .andExpect(status().isBadRequest());

        List<TPhongNhanVien> tPhongNhanVienList = tPhongNhanVienRepository.findAll();
        assertThat(tPhongNhanVienList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTPhongNhanViens() throws Exception {
        // Initialize the database
        tPhongNhanVienRepository.saveAndFlush(tPhongNhanVien);

        // Get all the tPhongNhanVienList
        restTPhongNhanVienMockMvc.perform(get("/api/t-phong-nhan-viens?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tPhongNhanVien.getId().intValue())))
            .andExpect(jsonPath("$.[*].enabled").value(hasItem(DEFAULT_ENABLED.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getTPhongNhanVien() throws Exception {
        // Initialize the database
        tPhongNhanVienRepository.saveAndFlush(tPhongNhanVien);

        // Get the tPhongNhanVien
        restTPhongNhanVienMockMvc.perform(get("/api/t-phong-nhan-viens/{id}", tPhongNhanVien.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(tPhongNhanVien.getId().intValue()))
            .andExpect(jsonPath("$.enabled").value(DEFAULT_ENABLED.booleanValue()));
    }


    @Test
    @Transactional
    public void getTPhongNhanViensByIdFiltering() throws Exception {
        // Initialize the database
        tPhongNhanVienRepository.saveAndFlush(tPhongNhanVien);

        Long id = tPhongNhanVien.getId();

        defaultTPhongNhanVienShouldBeFound("id.equals=" + id);
        defaultTPhongNhanVienShouldNotBeFound("id.notEquals=" + id);

        defaultTPhongNhanVienShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultTPhongNhanVienShouldNotBeFound("id.greaterThan=" + id);

        defaultTPhongNhanVienShouldBeFound("id.lessThanOrEqual=" + id);
        defaultTPhongNhanVienShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllTPhongNhanViensByEnabledIsEqualToSomething() throws Exception {
        // Initialize the database
        tPhongNhanVienRepository.saveAndFlush(tPhongNhanVien);

        // Get all the tPhongNhanVienList where enabled equals to DEFAULT_ENABLED
        defaultTPhongNhanVienShouldBeFound("enabled.equals=" + DEFAULT_ENABLED);

        // Get all the tPhongNhanVienList where enabled equals to UPDATED_ENABLED
        defaultTPhongNhanVienShouldNotBeFound("enabled.equals=" + UPDATED_ENABLED);
    }

    @Test
    @Transactional
    public void getAllTPhongNhanViensByEnabledIsNotEqualToSomething() throws Exception {
        // Initialize the database
        tPhongNhanVienRepository.saveAndFlush(tPhongNhanVien);

        // Get all the tPhongNhanVienList where enabled not equals to DEFAULT_ENABLED
        defaultTPhongNhanVienShouldNotBeFound("enabled.notEquals=" + DEFAULT_ENABLED);

        // Get all the tPhongNhanVienList where enabled not equals to UPDATED_ENABLED
        defaultTPhongNhanVienShouldBeFound("enabled.notEquals=" + UPDATED_ENABLED);
    }

    @Test
    @Transactional
    public void getAllTPhongNhanViensByEnabledIsInShouldWork() throws Exception {
        // Initialize the database
        tPhongNhanVienRepository.saveAndFlush(tPhongNhanVien);

        // Get all the tPhongNhanVienList where enabled in DEFAULT_ENABLED or UPDATED_ENABLED
        defaultTPhongNhanVienShouldBeFound("enabled.in=" + DEFAULT_ENABLED + "," + UPDATED_ENABLED);

        // Get all the tPhongNhanVienList where enabled equals to UPDATED_ENABLED
        defaultTPhongNhanVienShouldNotBeFound("enabled.in=" + UPDATED_ENABLED);
    }

    @Test
    @Transactional
    public void getAllTPhongNhanViensByEnabledIsNullOrNotNull() throws Exception {
        // Initialize the database
        tPhongNhanVienRepository.saveAndFlush(tPhongNhanVien);

        // Get all the tPhongNhanVienList where enabled is not null
        defaultTPhongNhanVienShouldBeFound("enabled.specified=true");

        // Get all the tPhongNhanVienList where enabled is null
        defaultTPhongNhanVienShouldNotBeFound("enabled.specified=false");
    }

    @Test
    @Transactional
    public void getAllTPhongNhanViensByPhongIsEqualToSomething() throws Exception {
        // Get already existing entity
        Phong phong = tPhongNhanVien.getPhong();
        tPhongNhanVienRepository.saveAndFlush(tPhongNhanVien);
        Long phongId = phong.getId();

        // Get all the tPhongNhanVienList where phong equals to phongId
        defaultTPhongNhanVienShouldBeFound("phongId.equals=" + phongId);

        // Get all the tPhongNhanVienList where phong equals to phongId + 1
        defaultTPhongNhanVienShouldNotBeFound("phongId.equals=" + (phongId + 1));
    }


    @Test
    @Transactional
    public void getAllTPhongNhanViensByNhanVienIsEqualToSomething() throws Exception {
        // Get already existing entity
        NhanVien nhanVien = tPhongNhanVien.getNhanVien();
        tPhongNhanVienRepository.saveAndFlush(tPhongNhanVien);
        Long nhanVienId = nhanVien.getId();

        // Get all the tPhongNhanVienList where nhanVien equals to nhanVienId
        defaultTPhongNhanVienShouldBeFound("nhanVienId.equals=" + nhanVienId);

        // Get all the tPhongNhanVienList where nhanVien equals to nhanVienId + 1
        defaultTPhongNhanVienShouldNotBeFound("nhanVienId.equals=" + (nhanVienId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultTPhongNhanVienShouldBeFound(String filter) throws Exception {
        restTPhongNhanVienMockMvc.perform(get("/api/t-phong-nhan-viens?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tPhongNhanVien.getId().intValue())))
            .andExpect(jsonPath("$.[*].enabled").value(hasItem(DEFAULT_ENABLED.booleanValue())));

        // Check, that the count call also returns 1
        restTPhongNhanVienMockMvc.perform(get("/api/t-phong-nhan-viens/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultTPhongNhanVienShouldNotBeFound(String filter) throws Exception {
        restTPhongNhanVienMockMvc.perform(get("/api/t-phong-nhan-viens?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restTPhongNhanVienMockMvc.perform(get("/api/t-phong-nhan-viens/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingTPhongNhanVien() throws Exception {
        // Get the tPhongNhanVien
        restTPhongNhanVienMockMvc.perform(get("/api/t-phong-nhan-viens/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTPhongNhanVien() throws Exception {
        // Initialize the database
        tPhongNhanVienRepository.saveAndFlush(tPhongNhanVien);

        int databaseSizeBeforeUpdate = tPhongNhanVienRepository.findAll().size();

        // Update the tPhongNhanVien
        TPhongNhanVien updatedTPhongNhanVien = tPhongNhanVienRepository.findById(tPhongNhanVien.getId()).get();
        // Disconnect from session so that the updates on updatedTPhongNhanVien are not directly saved in db
        em.detach(updatedTPhongNhanVien);
        updatedTPhongNhanVien
            .enabled(UPDATED_ENABLED);
        TPhongNhanVienDTO tPhongNhanVienDTO = tPhongNhanVienMapper.toDto(updatedTPhongNhanVien);

        restTPhongNhanVienMockMvc.perform(put("/api/t-phong-nhan-viens").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tPhongNhanVienDTO)))
            .andExpect(status().isOk());

        // Validate the TPhongNhanVien in the database
        List<TPhongNhanVien> tPhongNhanVienList = tPhongNhanVienRepository.findAll();
        assertThat(tPhongNhanVienList).hasSize(databaseSizeBeforeUpdate);
        TPhongNhanVien testTPhongNhanVien = tPhongNhanVienList.get(tPhongNhanVienList.size() - 1);
        assertThat(testTPhongNhanVien.isEnabled()).isEqualTo(UPDATED_ENABLED);
    }

    @Test
    @Transactional
    public void updateNonExistingTPhongNhanVien() throws Exception {
        int databaseSizeBeforeUpdate = tPhongNhanVienRepository.findAll().size();

        // Create the TPhongNhanVien
        TPhongNhanVienDTO tPhongNhanVienDTO = tPhongNhanVienMapper.toDto(tPhongNhanVien);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTPhongNhanVienMockMvc.perform(put("/api/t-phong-nhan-viens").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tPhongNhanVienDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TPhongNhanVien in the database
        List<TPhongNhanVien> tPhongNhanVienList = tPhongNhanVienRepository.findAll();
        assertThat(tPhongNhanVienList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTPhongNhanVien() throws Exception {
        // Initialize the database
        tPhongNhanVienRepository.saveAndFlush(tPhongNhanVien);

        int databaseSizeBeforeDelete = tPhongNhanVienRepository.findAll().size();

        // Delete the tPhongNhanVien
        restTPhongNhanVienMockMvc.perform(delete("/api/t-phong-nhan-viens/{id}", tPhongNhanVien.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TPhongNhanVien> tPhongNhanVienList = tPhongNhanVienRepository.findAll();
        assertThat(tPhongNhanVienList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

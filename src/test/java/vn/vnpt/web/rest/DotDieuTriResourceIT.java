package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.DotDieuTri;
import vn.vnpt.domain.DotDieuTriId;
import vn.vnpt.domain.BenhAnKhamBenh;
import vn.vnpt.repository.DotDieuTriRepository;
import vn.vnpt.service.DotDieuTriService;
import vn.vnpt.service.dto.DotDieuTriDTO;
import vn.vnpt.service.mapper.DotDieuTriMapper;
import vn.vnpt.service.dto.DotDieuTriCriteria;
import vn.vnpt.service.DotDieuTriQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link DotDieuTriResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class DotDieuTriResourceIT {

    public static final String DEFAULT_SO_THU_TU = "AAAAAAAAAA";
    public static final String UPDATED_SO_THU_TU = "BBBBBBBBBB";

    public static final Integer DEFAULT_BHYT_CAN_TREN = 1;
    public static final Integer UPDATED_BHYT_CAN_TREN = 2;
    public static final Integer SMALLER_BHYT_CAN_TREN = 1 - 1;

    public static final Integer DEFAULT_BHYT_CAN_TREN_KTC = 1;
    public static final Integer UPDATED_BHYT_CAN_TREN_KTC = 2;
    public static final Integer SMALLER_BHYT_CAN_TREN_KTC = 1 - 1;

    public static final String DEFAULT_BHYT_MA_KHU_VUC = "AAAAAAAAAA";
    public static final String UPDATED_BHYT_MA_KHU_VUC = "BBBBBBBBBB";

    public static final LocalDate DEFAULT_BHYT_NGAY_BAT_DAU = LocalDate.ofEpochDay(0L);
    public static final LocalDate UPDATED_BHYT_NGAY_BAT_DAU = LocalDate.now(ZoneId.systemDefault());
    public static final LocalDate SMALLER_BHYT_NGAY_BAT_DAU = LocalDate.ofEpochDay(-1L);

    public static final LocalDate DEFAULT_BHYT_NGAY_DU_NAM_NAM = LocalDate.ofEpochDay(0L);
    public static final LocalDate UPDATED_BHYT_NGAY_DU_NAM_NAM = LocalDate.now(ZoneId.systemDefault());
    public static final LocalDate SMALLER_BHYT_NGAY_DU_NAM_NAM = LocalDate.ofEpochDay(-1L);

    public static final LocalDate DEFAULT_BHYT_NGAY_HET_HAN = LocalDate.ofEpochDay(0L);
    public static final LocalDate UPDATED_BHYT_NGAY_HET_HAN = LocalDate.now(ZoneId.systemDefault());
    public static final LocalDate SMALLER_BHYT_NGAY_HET_HAN = LocalDate.ofEpochDay(-1L);

    public static final Boolean DEFAULT_BHYT_NOI_TINH = false;
    public static final Boolean UPDATED_BHYT_NOI_TINH = true;

    public static final String DEFAULT_BHYT_SO_THE = "AAAAAAAAAA";
    public static final String UPDATED_BHYT_SO_THE = "BBBBBBBBBB";

    public static final Integer DEFAULT_BHYT_TY_LE_MIEN_GIAM = 1;
    public static final Integer UPDATED_BHYT_TY_LE_MIEN_GIAM = 2;
    public static final Integer SMALLER_BHYT_TY_LE_MIEN_GIAM = 1 - 1;

    public static final Integer DEFAULT_BHYT_TY_LE_MIEN_GIAM_KTC = 1;
    public static final Integer UPDATED_BHYT_TY_LE_MIEN_GIAM_KTC = 2;
    public static final Integer SMALLER_BHYT_TY_LE_MIEN_GIAM_KTC = 1 - 1;

    public static final Boolean DEFAULT_CO_BAO_HIEM = false;
    public static final Boolean UPDATED_CO_BAO_HIEM = true;

    public static final String DEFAULT_BHYT_DIA_CHI = "AAAAAAAAAA";
    public static final String UPDATED_BHYT_DIA_CHI = "BBBBBBBBBB";

    public static final String DEFAULT_DOI_TUONG_BHYT_TEN = "AAAAAAAAAA";
    public static final String UPDATED_DOI_TUONG_BHYT_TEN = "BBBBBBBBBB";

    public static final Boolean DEFAULT_DUNG_TUYEN = false;
    public static final Boolean UPDATED_DUNG_TUYEN = true;

    public static final String DEFAULT_GIAY_TO_TRE_EM = "AAAAAAAAAA";
    public static final String UPDATED_GIAY_TO_TRE_EM = "BBBBBBBBBB";

    public static final Integer DEFAULT_LOAI_GIAY_TO_TRE_EM = 1;
    public static final Integer UPDATED_LOAI_GIAY_TO_TRE_EM = 2;
    public static final Integer SMALLER_LOAI_GIAY_TO_TRE_EM = 1 - 1;

    public static final LocalDate DEFAULT_NGAY_MIEN_CUNG_CHI_TRA = LocalDate.ofEpochDay(0L);
    public static final LocalDate UPDATED_NGAY_MIEN_CUNG_CHI_TRA = LocalDate.now(ZoneId.systemDefault());
    public static final LocalDate SMALLER_NGAY_MIEN_CUNG_CHI_TRA = LocalDate.ofEpochDay(-1L);

    public static final Boolean DEFAULT_THE_TRE_EM = false;
    public static final Boolean UPDATED_THE_TRE_EM = true;

    public static final Boolean DEFAULT_THONG_TUYEN_BHXH_XML_4210 = false;
    public static final Boolean UPDATED_THONG_TUYEN_BHXH_XML_4210 = true;

    public static final Integer DEFAULT_TRANG_THAI = 1;
    public static final Integer UPDATED_TRANG_THAI = 2;
    public static final Integer SMALLER_TRANG_THAI = 1 - 1;

    public static final Integer DEFAULT_LOAI = 1;
    public static final Integer UPDATED_LOAI = 2;
    public static final Integer SMALLER_LOAI = 1 - 1;

    public static final String DEFAULT_BHYT_NOI_DK_KCBBD = "AAAAAAAAAA";
    public static final String UPDATED_BHYT_NOI_DK_KCBBD = "BBBBBBBBBB";

    public static final Integer DEFAULT_NAM = 1;
    public static final Integer UPDATED_NAM = 2;
    public static final Integer SMALLER_NAM = 1 - 1;

    @Autowired
    private DotDieuTriRepository dotDieuTriRepository;

    @Autowired
    private DotDieuTriMapper dotDieuTriMapper;

    @Autowired
    private DotDieuTriService dotDieuTriService;

    @Autowired
    private DotDieuTriQueryService dotDieuTriQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDotDieuTriMockMvc;

    private DotDieuTri dotDieuTri;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DotDieuTri createEntity(EntityManager em) {
        DotDieuTri dotDieuTri = new DotDieuTri()
            .soThuTu(DEFAULT_SO_THU_TU)
            .bhytCanTren(DEFAULT_BHYT_CAN_TREN)
            .bhytCanTrenKtc(DEFAULT_BHYT_CAN_TREN_KTC)
            .bhytMaKhuVuc(DEFAULT_BHYT_MA_KHU_VUC)
            .bhytNgayBatDau(DEFAULT_BHYT_NGAY_BAT_DAU)
            .bhytNgayDuNamNam(DEFAULT_BHYT_NGAY_DU_NAM_NAM)
            .bhytNgayHetHan(DEFAULT_BHYT_NGAY_HET_HAN)
            .bhytNoiTinh(DEFAULT_BHYT_NOI_TINH)
            .bhytSoThe(DEFAULT_BHYT_SO_THE)
            .bhytTyLeMienGiam(DEFAULT_BHYT_TY_LE_MIEN_GIAM)
            .bhytTyLeMienGiamKtc(DEFAULT_BHYT_TY_LE_MIEN_GIAM_KTC)
            .coBaoHiem(DEFAULT_CO_BAO_HIEM)
            .bhytDiaChi(DEFAULT_BHYT_DIA_CHI)
            .doiTuongBhytTen(DEFAULT_DOI_TUONG_BHYT_TEN)
            .dungTuyen(DEFAULT_DUNG_TUYEN)
            .giayToTreEm(DEFAULT_GIAY_TO_TRE_EM)
            .loaiGiayToTreEm(DEFAULT_LOAI_GIAY_TO_TRE_EM)
            .ngayMienCungChiTra(DEFAULT_NGAY_MIEN_CUNG_CHI_TRA)
            .theTreEm(DEFAULT_THE_TRE_EM)
            .thongTuyenBhxhXml4210(DEFAULT_THONG_TUYEN_BHXH_XML_4210)
            .trangThai(DEFAULT_TRANG_THAI)
            .loai(DEFAULT_LOAI)
            .bhytNoiDkKcbbd(DEFAULT_BHYT_NOI_DK_KCBBD)
            .nam(DEFAULT_NAM);
        // Add required entity
        BenhAnKhamBenh newBenhAnKhamBenh = BenhAnKhamBenhResourceIT.createEntity(em);
        BenhAnKhamBenh benhAnKhamBenh = TestUtil.findAll(em, BenhAnKhamBenh.class).stream()
            .filter(x -> x.getId().equals(newBenhAnKhamBenh.getId()))
            .findAny().orElse(null);
        if (benhAnKhamBenh == null) {
            benhAnKhamBenh = newBenhAnKhamBenh;
            em.persist(benhAnKhamBenh);
            em.flush();
        }
        dotDieuTri.setBakb(benhAnKhamBenh);
        dotDieuTri.setId(new DotDieuTriId(benhAnKhamBenh.getId().getBenhNhanId(), benhAnKhamBenh.getId().getBenhNhanId()));
        return dotDieuTri;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DotDieuTri createUpdatedEntity(EntityManager em) {
        DotDieuTri dotDieuTri = new DotDieuTri()
            .soThuTu(UPDATED_SO_THU_TU)
            .bhytCanTren(UPDATED_BHYT_CAN_TREN)
            .bhytCanTrenKtc(UPDATED_BHYT_CAN_TREN_KTC)
            .bhytMaKhuVuc(UPDATED_BHYT_MA_KHU_VUC)
            .bhytNgayBatDau(UPDATED_BHYT_NGAY_BAT_DAU)
            .bhytNgayDuNamNam(UPDATED_BHYT_NGAY_DU_NAM_NAM)
            .bhytNgayHetHan(UPDATED_BHYT_NGAY_HET_HAN)
            .bhytNoiTinh(UPDATED_BHYT_NOI_TINH)
            .bhytSoThe(UPDATED_BHYT_SO_THE)
            .bhytTyLeMienGiam(UPDATED_BHYT_TY_LE_MIEN_GIAM)
            .bhytTyLeMienGiamKtc(UPDATED_BHYT_TY_LE_MIEN_GIAM_KTC)
            .coBaoHiem(UPDATED_CO_BAO_HIEM)
            .bhytDiaChi(UPDATED_BHYT_DIA_CHI)
            .doiTuongBhytTen(UPDATED_DOI_TUONG_BHYT_TEN)
            .dungTuyen(UPDATED_DUNG_TUYEN)
            .giayToTreEm(UPDATED_GIAY_TO_TRE_EM)
            .loaiGiayToTreEm(UPDATED_LOAI_GIAY_TO_TRE_EM)
            .ngayMienCungChiTra(UPDATED_NGAY_MIEN_CUNG_CHI_TRA)
            .theTreEm(UPDATED_THE_TRE_EM)
            .thongTuyenBhxhXml4210(UPDATED_THONG_TUYEN_BHXH_XML_4210)
            .trangThai(UPDATED_TRANG_THAI)
            .loai(UPDATED_LOAI)
            .bhytNoiDkKcbbd(UPDATED_BHYT_NOI_DK_KCBBD)
            .nam(UPDATED_NAM);
        // Add required entity
        BenhAnKhamBenh newBenhAnKhamBenh = BenhAnKhamBenhResourceIT.createUpdatedEntity(em);
        BenhAnKhamBenh benhAnKhamBenh = TestUtil.findAll(em, BenhAnKhamBenh.class).stream()
            .filter(x -> x.getId().equals(newBenhAnKhamBenh.getId()))
            .findAny().orElse(null);
        if (benhAnKhamBenh == null) {
            benhAnKhamBenh = newBenhAnKhamBenh;
            em.persist(benhAnKhamBenh);
            em.flush();
        }
        dotDieuTri.setBakb(benhAnKhamBenh);
        dotDieuTri.setId(new DotDieuTriId(benhAnKhamBenh.getId().getBenhNhanId(), benhAnKhamBenh.getId().getBenhNhanId()));
        return dotDieuTri;
    }

    @BeforeEach
    public void initTest() {
        dotDieuTri = createEntity(em);
    }

    @Test
    @Transactional
    public void createDotDieuTri() throws Exception {
        int databaseSizeBeforeCreate = dotDieuTriRepository.findAll().size();

        // Create the DotDieuTri
        DotDieuTriDTO dotDieuTriDTO = dotDieuTriMapper.toDto(dotDieuTri);
        restDotDieuTriMockMvc.perform(post("/api/dot-dieu-tris").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dotDieuTriDTO)))
            .andExpect(status().isCreated());

        // Validate the DotDieuTri in the database
        List<DotDieuTri> dotDieuTriList = dotDieuTriRepository.findAll();
        assertThat(dotDieuTriList).hasSize(databaseSizeBeforeCreate + 1);
        DotDieuTri testDotDieuTri = dotDieuTriList.get(dotDieuTriList.size() - 1);
        assertThat(testDotDieuTri.getSoThuTu()).isEqualTo(DEFAULT_SO_THU_TU);
        assertThat(testDotDieuTri.getBhytCanTren()).isEqualTo(DEFAULT_BHYT_CAN_TREN);
        assertThat(testDotDieuTri.getBhytCanTrenKtc()).isEqualTo(DEFAULT_BHYT_CAN_TREN_KTC);
        assertThat(testDotDieuTri.getBhytMaKhuVuc()).isEqualTo(DEFAULT_BHYT_MA_KHU_VUC);
        assertThat(testDotDieuTri.getBhytNgayBatDau()).isEqualTo(DEFAULT_BHYT_NGAY_BAT_DAU);
        assertThat(testDotDieuTri.getBhytNgayDuNamNam()).isEqualTo(DEFAULT_BHYT_NGAY_DU_NAM_NAM);
        assertThat(testDotDieuTri.getBhytNgayHetHan()).isEqualTo(DEFAULT_BHYT_NGAY_HET_HAN);
        assertThat(testDotDieuTri.isBhytNoiTinh()).isEqualTo(DEFAULT_BHYT_NOI_TINH);
        assertThat(testDotDieuTri.getBhytSoThe()).isEqualTo(DEFAULT_BHYT_SO_THE);
        assertThat(testDotDieuTri.getBhytTyLeMienGiam()).isEqualTo(DEFAULT_BHYT_TY_LE_MIEN_GIAM);
        assertThat(testDotDieuTri.getBhytTyLeMienGiamKtc()).isEqualTo(DEFAULT_BHYT_TY_LE_MIEN_GIAM_KTC);
        assertThat(testDotDieuTri.isCoBaoHiem()).isEqualTo(DEFAULT_CO_BAO_HIEM);
        assertThat(testDotDieuTri.getBhytDiaChi()).isEqualTo(DEFAULT_BHYT_DIA_CHI);
        assertThat(testDotDieuTri.getDoiTuongBhytTen()).isEqualTo(DEFAULT_DOI_TUONG_BHYT_TEN);
        assertThat(testDotDieuTri.isDungTuyen()).isEqualTo(DEFAULT_DUNG_TUYEN);
        assertThat(testDotDieuTri.getGiayToTreEm()).isEqualTo(DEFAULT_GIAY_TO_TRE_EM);
        assertThat(testDotDieuTri.getLoaiGiayToTreEm()).isEqualTo(DEFAULT_LOAI_GIAY_TO_TRE_EM);
        assertThat(testDotDieuTri.getNgayMienCungChiTra()).isEqualTo(DEFAULT_NGAY_MIEN_CUNG_CHI_TRA);
        assertThat(testDotDieuTri.isTheTreEm()).isEqualTo(DEFAULT_THE_TRE_EM);
        assertThat(testDotDieuTri.isThongTuyenBhxhXml4210()).isEqualTo(DEFAULT_THONG_TUYEN_BHXH_XML_4210);
        assertThat(testDotDieuTri.getTrangThai()).isEqualTo(DEFAULT_TRANG_THAI);
        assertThat(testDotDieuTri.getLoai()).isEqualTo(DEFAULT_LOAI);
        assertThat(testDotDieuTri.getBhytNoiDkKcbbd()).isEqualTo(DEFAULT_BHYT_NOI_DK_KCBBD);
        assertThat(testDotDieuTri.getNam()).isEqualTo(DEFAULT_NAM);
    }

    @Test
    @Transactional
    public void createDotDieuTriWithExistingId() throws Exception {
        dotDieuTriRepository.save(dotDieuTri);
        int databaseSizeBeforeCreate = dotDieuTriRepository.findAll().size();

        // Create the DotDieuTri with an existing ID
        dotDieuTri.setId(dotDieuTri.getId());
        DotDieuTriDTO dotDieuTriDTO = dotDieuTriMapper.toDto(dotDieuTri);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDotDieuTriMockMvc.perform(post("/api/dot-dieu-tris").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dotDieuTriDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DotDieuTri in the database
        List<DotDieuTri> dotDieuTriList = dotDieuTriRepository.findAll();
        assertThat(dotDieuTriList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkCoBaoHiemIsRequired() throws Exception {
        int databaseSizeBeforeTest = dotDieuTriRepository.findAll().size();
        // set the field null
        dotDieuTri.setCoBaoHiem(null);

        // Create the DotDieuTri, which fails.
        DotDieuTriDTO dotDieuTriDTO = dotDieuTriMapper.toDto(dotDieuTri);

        restDotDieuTriMockMvc.perform(post("/api/dot-dieu-tris").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dotDieuTriDTO)))
            .andExpect(status().isBadRequest());

        List<DotDieuTri> dotDieuTriList = dotDieuTriRepository.findAll();
        assertThat(dotDieuTriList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNamIsRequired() throws Exception {
        int databaseSizeBeforeTest = dotDieuTriRepository.findAll().size();
        // set the field null
        dotDieuTri.setNam(null);

        // Create the DotDieuTri, which fails.
        DotDieuTriDTO dotDieuTriDTO = dotDieuTriMapper.toDto(dotDieuTri);

        restDotDieuTriMockMvc.perform(post("/api/dot-dieu-tris").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dotDieuTriDTO)))
            .andExpect(status().isBadRequest());

        List<DotDieuTri> dotDieuTriList = dotDieuTriRepository.findAll();
        assertThat(dotDieuTriList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDotDieuTris() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList
        restDotDieuTriMockMvc.perform(get("/api/dot-dieu-tris"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].soThuTu").value(hasItem(DEFAULT_SO_THU_TU)))
            .andExpect(jsonPath("$.[*].bhytCanTren").value(hasItem(DEFAULT_BHYT_CAN_TREN)))
            .andExpect(jsonPath("$.[*].bhytCanTrenKtc").value(hasItem(DEFAULT_BHYT_CAN_TREN_KTC)))
            .andExpect(jsonPath("$.[*].bhytMaKhuVuc").value(hasItem(DEFAULT_BHYT_MA_KHU_VUC)))
            .andExpect(jsonPath("$.[*].bhytNgayBatDau").value(hasItem(DEFAULT_BHYT_NGAY_BAT_DAU.toString())))
            .andExpect(jsonPath("$.[*].bhytNgayDuNamNam").value(hasItem(DEFAULT_BHYT_NGAY_DU_NAM_NAM.toString())))
            .andExpect(jsonPath("$.[*].bhytNgayHetHan").value(hasItem(DEFAULT_BHYT_NGAY_HET_HAN.toString())))
            .andExpect(jsonPath("$.[*].bhytNoiTinh").value(hasItem(DEFAULT_BHYT_NOI_TINH.booleanValue())))
            .andExpect(jsonPath("$.[*].bhytSoThe").value(hasItem(DEFAULT_BHYT_SO_THE)))
            .andExpect(jsonPath("$.[*].bhytTyLeMienGiam").value(hasItem(DEFAULT_BHYT_TY_LE_MIEN_GIAM)))
            .andExpect(jsonPath("$.[*].bhytTyLeMienGiamKtc").value(hasItem(DEFAULT_BHYT_TY_LE_MIEN_GIAM_KTC)))
            .andExpect(jsonPath("$.[*].coBaoHiem").value(hasItem(DEFAULT_CO_BAO_HIEM.booleanValue())))
            .andExpect(jsonPath("$.[*].bhytDiaChi").value(hasItem(DEFAULT_BHYT_DIA_CHI)))
            .andExpect(jsonPath("$.[*].doiTuongBhytTen").value(hasItem(DEFAULT_DOI_TUONG_BHYT_TEN)))
            .andExpect(jsonPath("$.[*].dungTuyen").value(hasItem(DEFAULT_DUNG_TUYEN.booleanValue())))
            .andExpect(jsonPath("$.[*].giayToTreEm").value(hasItem(DEFAULT_GIAY_TO_TRE_EM)))
            .andExpect(jsonPath("$.[*].loaiGiayToTreEm").value(hasItem(DEFAULT_LOAI_GIAY_TO_TRE_EM)))
            .andExpect(jsonPath("$.[*].ngayMienCungChiTra").value(hasItem(DEFAULT_NGAY_MIEN_CUNG_CHI_TRA.toString())))
            .andExpect(jsonPath("$.[*].theTreEm").value(hasItem(DEFAULT_THE_TRE_EM.booleanValue())))
            .andExpect(jsonPath("$.[*].thongTuyenBhxhXml4210").value(hasItem(DEFAULT_THONG_TUYEN_BHXH_XML_4210.booleanValue())))
            .andExpect(jsonPath("$.[*].trangThai").value(hasItem(DEFAULT_TRANG_THAI)))
            .andExpect(jsonPath("$.[*].loai").value(hasItem(DEFAULT_LOAI)))
            .andExpect(jsonPath("$.[*].bhytNoiDkKcbbd").value(hasItem(DEFAULT_BHYT_NOI_DK_KCBBD)))
            .andExpect(jsonPath("$.[*].nam").value(hasItem(DEFAULT_NAM)));
    }

    @Test
    @Transactional
    public void getDotDieuTri() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get the dotDieuTri
        restDotDieuTriMockMvc.perform(get("/api/dot-dieu-tris/{id}", "bakbBenhNhanId=" + dotDieuTri.getId().getBakbBenhNhanId() + ";" + "bakbDonViId=" + dotDieuTri.getId().getBakbDonViId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.soThuTu").value(DEFAULT_SO_THU_TU))
            .andExpect(jsonPath("$.bhytCanTren").value(DEFAULT_BHYT_CAN_TREN))
            .andExpect(jsonPath("$.bhytCanTrenKtc").value(DEFAULT_BHYT_CAN_TREN_KTC))
            .andExpect(jsonPath("$.bhytMaKhuVuc").value(DEFAULT_BHYT_MA_KHU_VUC))
            .andExpect(jsonPath("$.bhytNgayBatDau").value(DEFAULT_BHYT_NGAY_BAT_DAU.toString()))
            .andExpect(jsonPath("$.bhytNgayDuNamNam").value(DEFAULT_BHYT_NGAY_DU_NAM_NAM.toString()))
            .andExpect(jsonPath("$.bhytNgayHetHan").value(DEFAULT_BHYT_NGAY_HET_HAN.toString()))
            .andExpect(jsonPath("$.bhytNoiTinh").value(DEFAULT_BHYT_NOI_TINH.booleanValue()))
            .andExpect(jsonPath("$.bhytSoThe").value(DEFAULT_BHYT_SO_THE))
            .andExpect(jsonPath("$.bhytTyLeMienGiam").value(DEFAULT_BHYT_TY_LE_MIEN_GIAM))
            .andExpect(jsonPath("$.bhytTyLeMienGiamKtc").value(DEFAULT_BHYT_TY_LE_MIEN_GIAM_KTC))
            .andExpect(jsonPath("$.coBaoHiem").value(DEFAULT_CO_BAO_HIEM.booleanValue()))
            .andExpect(jsonPath("$.bhytDiaChi").value(DEFAULT_BHYT_DIA_CHI))
            .andExpect(jsonPath("$.doiTuongBhytTen").value(DEFAULT_DOI_TUONG_BHYT_TEN))
            .andExpect(jsonPath("$.dungTuyen").value(DEFAULT_DUNG_TUYEN.booleanValue()))
            .andExpect(jsonPath("$.giayToTreEm").value(DEFAULT_GIAY_TO_TRE_EM))
            .andExpect(jsonPath("$.loaiGiayToTreEm").value(DEFAULT_LOAI_GIAY_TO_TRE_EM))
            .andExpect(jsonPath("$.ngayMienCungChiTra").value(DEFAULT_NGAY_MIEN_CUNG_CHI_TRA.toString()))
            .andExpect(jsonPath("$.theTreEm").value(DEFAULT_THE_TRE_EM.booleanValue()))
            .andExpect(jsonPath("$.thongTuyenBhxhXml4210").value(DEFAULT_THONG_TUYEN_BHXH_XML_4210.booleanValue()))
            .andExpect(jsonPath("$.trangThai").value(DEFAULT_TRANG_THAI))
            .andExpect(jsonPath("$.loai").value(DEFAULT_LOAI))
            .andExpect(jsonPath("$.bhytNoiDkKcbbd").value(DEFAULT_BHYT_NOI_DK_KCBBD))
            .andExpect(jsonPath("$.nam").value(DEFAULT_NAM));
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisBySoThuTuIsEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where soThuTu equals to DEFAULT_SO_THU_TU
        defaultDotDieuTriShouldBeFound("soThuTu.equals=" + DEFAULT_SO_THU_TU);

        // Get all the dotDieuTriList where soThuTu equals to UPDATED_SO_THU_TU
        defaultDotDieuTriShouldNotBeFound("soThuTu.equals=" + UPDATED_SO_THU_TU);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisBySoThuTuIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where soThuTu not equals to DEFAULT_SO_THU_TU
        defaultDotDieuTriShouldNotBeFound("soThuTu.notEquals=" + DEFAULT_SO_THU_TU);

        // Get all the dotDieuTriList where soThuTu not equals to UPDATED_SO_THU_TU
        defaultDotDieuTriShouldBeFound("soThuTu.notEquals=" + UPDATED_SO_THU_TU);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisBySoThuTuIsInShouldWork() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where soThuTu in DEFAULT_SO_THU_TU or UPDATED_SO_THU_TU
        defaultDotDieuTriShouldBeFound("soThuTu.in=" + DEFAULT_SO_THU_TU + "," + UPDATED_SO_THU_TU);

        // Get all the dotDieuTriList where soThuTu equals to UPDATED_SO_THU_TU
        defaultDotDieuTriShouldNotBeFound("soThuTu.in=" + UPDATED_SO_THU_TU);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisBySoThuTuIsNullOrNotNull() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where soThuTu is not null
        defaultDotDieuTriShouldBeFound("soThuTu.specified=true");

        // Get all the dotDieuTriList where soThuTu is null
        defaultDotDieuTriShouldNotBeFound("soThuTu.specified=false");
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisBySoThuTuContainsSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where soThuTu contains DEFAULT_SO_THU_TU
        defaultDotDieuTriShouldBeFound("soThuTu.contains=" + DEFAULT_SO_THU_TU);

        // Get all the dotDieuTriList where soThuTu contains UPDATED_SO_THU_TU
        defaultDotDieuTriShouldNotBeFound("soThuTu.contains=" + UPDATED_SO_THU_TU);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisBySoThuTuNotContainsSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where soThuTu does not contain DEFAULT_SO_THU_TU
        defaultDotDieuTriShouldNotBeFound("soThuTu.doesNotContain=" + DEFAULT_SO_THU_TU);

        // Get all the dotDieuTriList where soThuTu does not contain UPDATED_SO_THU_TU
        defaultDotDieuTriShouldBeFound("soThuTu.doesNotContain=" + UPDATED_SO_THU_TU);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytCanTrenIsEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytCanTren equals to DEFAULT_BHYT_CAN_TREN
        defaultDotDieuTriShouldBeFound("bhytCanTren.equals=" + DEFAULT_BHYT_CAN_TREN);

        // Get all the dotDieuTriList where bhytCanTren equals to UPDATED_BHYT_CAN_TREN
        defaultDotDieuTriShouldNotBeFound("bhytCanTren.equals=" + UPDATED_BHYT_CAN_TREN);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytCanTrenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytCanTren not equals to DEFAULT_BHYT_CAN_TREN
        defaultDotDieuTriShouldNotBeFound("bhytCanTren.notEquals=" + DEFAULT_BHYT_CAN_TREN);

        // Get all the dotDieuTriList where bhytCanTren not equals to UPDATED_BHYT_CAN_TREN
        defaultDotDieuTriShouldBeFound("bhytCanTren.notEquals=" + UPDATED_BHYT_CAN_TREN);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytCanTrenIsInShouldWork() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytCanTren in DEFAULT_BHYT_CAN_TREN or UPDATED_BHYT_CAN_TREN
        defaultDotDieuTriShouldBeFound("bhytCanTren.in=" + DEFAULT_BHYT_CAN_TREN + "," + UPDATED_BHYT_CAN_TREN);

        // Get all the dotDieuTriList where bhytCanTren equals to UPDATED_BHYT_CAN_TREN
        defaultDotDieuTriShouldNotBeFound("bhytCanTren.in=" + UPDATED_BHYT_CAN_TREN);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytCanTrenIsNullOrNotNull() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytCanTren is not null
        defaultDotDieuTriShouldBeFound("bhytCanTren.specified=true");

        // Get all the dotDieuTriList where bhytCanTren is null
        defaultDotDieuTriShouldNotBeFound("bhytCanTren.specified=false");
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytCanTrenIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytCanTren is greater than or equal to DEFAULT_BHYT_CAN_TREN
        defaultDotDieuTriShouldBeFound("bhytCanTren.greaterThanOrEqual=" + DEFAULT_BHYT_CAN_TREN);

        // Get all the dotDieuTriList where bhytCanTren is greater than or equal to UPDATED_BHYT_CAN_TREN
        defaultDotDieuTriShouldNotBeFound("bhytCanTren.greaterThanOrEqual=" + UPDATED_BHYT_CAN_TREN);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytCanTrenIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytCanTren is less than or equal to DEFAULT_BHYT_CAN_TREN
        defaultDotDieuTriShouldBeFound("bhytCanTren.lessThanOrEqual=" + DEFAULT_BHYT_CAN_TREN);

        // Get all the dotDieuTriList where bhytCanTren is less than or equal to SMALLER_BHYT_CAN_TREN
        defaultDotDieuTriShouldNotBeFound("bhytCanTren.lessThanOrEqual=" + SMALLER_BHYT_CAN_TREN);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytCanTrenIsLessThanSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytCanTren is less than DEFAULT_BHYT_CAN_TREN
        defaultDotDieuTriShouldNotBeFound("bhytCanTren.lessThan=" + DEFAULT_BHYT_CAN_TREN);

        // Get all the dotDieuTriList where bhytCanTren is less than UPDATED_BHYT_CAN_TREN
        defaultDotDieuTriShouldBeFound("bhytCanTren.lessThan=" + UPDATED_BHYT_CAN_TREN);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytCanTrenIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytCanTren is greater than DEFAULT_BHYT_CAN_TREN
        defaultDotDieuTriShouldNotBeFound("bhytCanTren.greaterThan=" + DEFAULT_BHYT_CAN_TREN);

        // Get all the dotDieuTriList where bhytCanTren is greater than SMALLER_BHYT_CAN_TREN
        defaultDotDieuTriShouldBeFound("bhytCanTren.greaterThan=" + SMALLER_BHYT_CAN_TREN);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytCanTrenKtcIsEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytCanTrenKtc equals to DEFAULT_BHYT_CAN_TREN_KTC
        defaultDotDieuTriShouldBeFound("bhytCanTrenKtc.equals=" + DEFAULT_BHYT_CAN_TREN_KTC);

        // Get all the dotDieuTriList where bhytCanTrenKtc equals to UPDATED_BHYT_CAN_TREN_KTC
        defaultDotDieuTriShouldNotBeFound("bhytCanTrenKtc.equals=" + UPDATED_BHYT_CAN_TREN_KTC);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytCanTrenKtcIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytCanTrenKtc not equals to DEFAULT_BHYT_CAN_TREN_KTC
        defaultDotDieuTriShouldNotBeFound("bhytCanTrenKtc.notEquals=" + DEFAULT_BHYT_CAN_TREN_KTC);

        // Get all the dotDieuTriList where bhytCanTrenKtc not equals to UPDATED_BHYT_CAN_TREN_KTC
        defaultDotDieuTriShouldBeFound("bhytCanTrenKtc.notEquals=" + UPDATED_BHYT_CAN_TREN_KTC);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytCanTrenKtcIsInShouldWork() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytCanTrenKtc in DEFAULT_BHYT_CAN_TREN_KTC or UPDATED_BHYT_CAN_TREN_KTC
        defaultDotDieuTriShouldBeFound("bhytCanTrenKtc.in=" + DEFAULT_BHYT_CAN_TREN_KTC + "," + UPDATED_BHYT_CAN_TREN_KTC);

        // Get all the dotDieuTriList where bhytCanTrenKtc equals to UPDATED_BHYT_CAN_TREN_KTC
        defaultDotDieuTriShouldNotBeFound("bhytCanTrenKtc.in=" + UPDATED_BHYT_CAN_TREN_KTC);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytCanTrenKtcIsNullOrNotNull() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytCanTrenKtc is not null
        defaultDotDieuTriShouldBeFound("bhytCanTrenKtc.specified=true");

        // Get all the dotDieuTriList where bhytCanTrenKtc is null
        defaultDotDieuTriShouldNotBeFound("bhytCanTrenKtc.specified=false");
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytCanTrenKtcIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytCanTrenKtc is greater than or equal to DEFAULT_BHYT_CAN_TREN_KTC
        defaultDotDieuTriShouldBeFound("bhytCanTrenKtc.greaterThanOrEqual=" + DEFAULT_BHYT_CAN_TREN_KTC);

        // Get all the dotDieuTriList where bhytCanTrenKtc is greater than or equal to UPDATED_BHYT_CAN_TREN_KTC
        defaultDotDieuTriShouldNotBeFound("bhytCanTrenKtc.greaterThanOrEqual=" + UPDATED_BHYT_CAN_TREN_KTC);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytCanTrenKtcIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytCanTrenKtc is less than or equal to DEFAULT_BHYT_CAN_TREN_KTC
        defaultDotDieuTriShouldBeFound("bhytCanTrenKtc.lessThanOrEqual=" + DEFAULT_BHYT_CAN_TREN_KTC);

        // Get all the dotDieuTriList where bhytCanTrenKtc is less than or equal to SMALLER_BHYT_CAN_TREN_KTC
        defaultDotDieuTriShouldNotBeFound("bhytCanTrenKtc.lessThanOrEqual=" + SMALLER_BHYT_CAN_TREN_KTC);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytCanTrenKtcIsLessThanSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytCanTrenKtc is less than DEFAULT_BHYT_CAN_TREN_KTC
        defaultDotDieuTriShouldNotBeFound("bhytCanTrenKtc.lessThan=" + DEFAULT_BHYT_CAN_TREN_KTC);

        // Get all the dotDieuTriList where bhytCanTrenKtc is less than UPDATED_BHYT_CAN_TREN_KTC
        defaultDotDieuTriShouldBeFound("bhytCanTrenKtc.lessThan=" + UPDATED_BHYT_CAN_TREN_KTC);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytCanTrenKtcIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytCanTrenKtc is greater than DEFAULT_BHYT_CAN_TREN_KTC
        defaultDotDieuTriShouldNotBeFound("bhytCanTrenKtc.greaterThan=" + DEFAULT_BHYT_CAN_TREN_KTC);

        // Get all the dotDieuTriList where bhytCanTrenKtc is greater than SMALLER_BHYT_CAN_TREN_KTC
        defaultDotDieuTriShouldBeFound("bhytCanTrenKtc.greaterThan=" + SMALLER_BHYT_CAN_TREN_KTC);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytMaKhuVucIsEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytMaKhuVuc equals to DEFAULT_BHYT_MA_KHU_VUC
        defaultDotDieuTriShouldBeFound("bhytMaKhuVuc.equals=" + DEFAULT_BHYT_MA_KHU_VUC);

        // Get all the dotDieuTriList where bhytMaKhuVuc equals to UPDATED_BHYT_MA_KHU_VUC
        defaultDotDieuTriShouldNotBeFound("bhytMaKhuVuc.equals=" + UPDATED_BHYT_MA_KHU_VUC);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytMaKhuVucIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytMaKhuVuc not equals to DEFAULT_BHYT_MA_KHU_VUC
        defaultDotDieuTriShouldNotBeFound("bhytMaKhuVuc.notEquals=" + DEFAULT_BHYT_MA_KHU_VUC);

        // Get all the dotDieuTriList where bhytMaKhuVuc not equals to UPDATED_BHYT_MA_KHU_VUC
        defaultDotDieuTriShouldBeFound("bhytMaKhuVuc.notEquals=" + UPDATED_BHYT_MA_KHU_VUC);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytMaKhuVucIsInShouldWork() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytMaKhuVuc in DEFAULT_BHYT_MA_KHU_VUC or UPDATED_BHYT_MA_KHU_VUC
        defaultDotDieuTriShouldBeFound("bhytMaKhuVuc.in=" + DEFAULT_BHYT_MA_KHU_VUC + "," + UPDATED_BHYT_MA_KHU_VUC);

        // Get all the dotDieuTriList where bhytMaKhuVuc equals to UPDATED_BHYT_MA_KHU_VUC
        defaultDotDieuTriShouldNotBeFound("bhytMaKhuVuc.in=" + UPDATED_BHYT_MA_KHU_VUC);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytMaKhuVucIsNullOrNotNull() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytMaKhuVuc is not null
        defaultDotDieuTriShouldBeFound("bhytMaKhuVuc.specified=true");

        // Get all the dotDieuTriList where bhytMaKhuVuc is null
        defaultDotDieuTriShouldNotBeFound("bhytMaKhuVuc.specified=false");
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytMaKhuVucContainsSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytMaKhuVuc contains DEFAULT_BHYT_MA_KHU_VUC
        defaultDotDieuTriShouldBeFound("bhytMaKhuVuc.contains=" + DEFAULT_BHYT_MA_KHU_VUC);

        // Get all the dotDieuTriList where bhytMaKhuVuc contains UPDATED_BHYT_MA_KHU_VUC
        defaultDotDieuTriShouldNotBeFound("bhytMaKhuVuc.contains=" + UPDATED_BHYT_MA_KHU_VUC);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytMaKhuVucNotContainsSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytMaKhuVuc does not contain DEFAULT_BHYT_MA_KHU_VUC
        defaultDotDieuTriShouldNotBeFound("bhytMaKhuVuc.doesNotContain=" + DEFAULT_BHYT_MA_KHU_VUC);

        // Get all the dotDieuTriList where bhytMaKhuVuc does not contain UPDATED_BHYT_MA_KHU_VUC
        defaultDotDieuTriShouldBeFound("bhytMaKhuVuc.doesNotContain=" + UPDATED_BHYT_MA_KHU_VUC);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytNgayBatDauIsEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytNgayBatDau equals to DEFAULT_BHYT_NGAY_BAT_DAU
        defaultDotDieuTriShouldBeFound("bhytNgayBatDau.equals=" + DEFAULT_BHYT_NGAY_BAT_DAU);

        // Get all the dotDieuTriList where bhytNgayBatDau equals to UPDATED_BHYT_NGAY_BAT_DAU
        defaultDotDieuTriShouldNotBeFound("bhytNgayBatDau.equals=" + UPDATED_BHYT_NGAY_BAT_DAU);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytNgayBatDauIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytNgayBatDau not equals to DEFAULT_BHYT_NGAY_BAT_DAU
        defaultDotDieuTriShouldNotBeFound("bhytNgayBatDau.notEquals=" + DEFAULT_BHYT_NGAY_BAT_DAU);

        // Get all the dotDieuTriList where bhytNgayBatDau not equals to UPDATED_BHYT_NGAY_BAT_DAU
        defaultDotDieuTriShouldBeFound("bhytNgayBatDau.notEquals=" + UPDATED_BHYT_NGAY_BAT_DAU);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytNgayBatDauIsInShouldWork() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytNgayBatDau in DEFAULT_BHYT_NGAY_BAT_DAU or UPDATED_BHYT_NGAY_BAT_DAU
        defaultDotDieuTriShouldBeFound("bhytNgayBatDau.in=" + DEFAULT_BHYT_NGAY_BAT_DAU + "," + UPDATED_BHYT_NGAY_BAT_DAU);

        // Get all the dotDieuTriList where bhytNgayBatDau equals to UPDATED_BHYT_NGAY_BAT_DAU
        defaultDotDieuTriShouldNotBeFound("bhytNgayBatDau.in=" + UPDATED_BHYT_NGAY_BAT_DAU);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytNgayBatDauIsNullOrNotNull() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytNgayBatDau is not null
        defaultDotDieuTriShouldBeFound("bhytNgayBatDau.specified=true");

        // Get all the dotDieuTriList where bhytNgayBatDau is null
        defaultDotDieuTriShouldNotBeFound("bhytNgayBatDau.specified=false");
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytNgayBatDauIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytNgayBatDau is greater than or equal to DEFAULT_BHYT_NGAY_BAT_DAU
        defaultDotDieuTriShouldBeFound("bhytNgayBatDau.greaterThanOrEqual=" + DEFAULT_BHYT_NGAY_BAT_DAU);

        // Get all the dotDieuTriList where bhytNgayBatDau is greater than or equal to UPDATED_BHYT_NGAY_BAT_DAU
        defaultDotDieuTriShouldNotBeFound("bhytNgayBatDau.greaterThanOrEqual=" + UPDATED_BHYT_NGAY_BAT_DAU);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytNgayBatDauIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytNgayBatDau is less than or equal to DEFAULT_BHYT_NGAY_BAT_DAU
        defaultDotDieuTriShouldBeFound("bhytNgayBatDau.lessThanOrEqual=" + DEFAULT_BHYT_NGAY_BAT_DAU);

        // Get all the dotDieuTriList where bhytNgayBatDau is less than or equal to SMALLER_BHYT_NGAY_BAT_DAU
        defaultDotDieuTriShouldNotBeFound("bhytNgayBatDau.lessThanOrEqual=" + SMALLER_BHYT_NGAY_BAT_DAU);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytNgayBatDauIsLessThanSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytNgayBatDau is less than DEFAULT_BHYT_NGAY_BAT_DAU
        defaultDotDieuTriShouldNotBeFound("bhytNgayBatDau.lessThan=" + DEFAULT_BHYT_NGAY_BAT_DAU);

        // Get all the dotDieuTriList where bhytNgayBatDau is less than UPDATED_BHYT_NGAY_BAT_DAU
        defaultDotDieuTriShouldBeFound("bhytNgayBatDau.lessThan=" + UPDATED_BHYT_NGAY_BAT_DAU);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytNgayBatDauIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytNgayBatDau is greater than DEFAULT_BHYT_NGAY_BAT_DAU
        defaultDotDieuTriShouldNotBeFound("bhytNgayBatDau.greaterThan=" + DEFAULT_BHYT_NGAY_BAT_DAU);

        // Get all the dotDieuTriList where bhytNgayBatDau is greater than SMALLER_BHYT_NGAY_BAT_DAU
        defaultDotDieuTriShouldBeFound("bhytNgayBatDau.greaterThan=" + SMALLER_BHYT_NGAY_BAT_DAU);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytNgayDuNamNamIsEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytNgayDuNamNam equals to DEFAULT_BHYT_NGAY_DU_NAM_NAM
        defaultDotDieuTriShouldBeFound("bhytNgayDuNamNam.equals=" + DEFAULT_BHYT_NGAY_DU_NAM_NAM);

        // Get all the dotDieuTriList where bhytNgayDuNamNam equals to UPDATED_BHYT_NGAY_DU_NAM_NAM
        defaultDotDieuTriShouldNotBeFound("bhytNgayDuNamNam.equals=" + UPDATED_BHYT_NGAY_DU_NAM_NAM);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytNgayDuNamNamIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytNgayDuNamNam not equals to DEFAULT_BHYT_NGAY_DU_NAM_NAM
        defaultDotDieuTriShouldNotBeFound("bhytNgayDuNamNam.notEquals=" + DEFAULT_BHYT_NGAY_DU_NAM_NAM);

        // Get all the dotDieuTriList where bhytNgayDuNamNam not equals to UPDATED_BHYT_NGAY_DU_NAM_NAM
        defaultDotDieuTriShouldBeFound("bhytNgayDuNamNam.notEquals=" + UPDATED_BHYT_NGAY_DU_NAM_NAM);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytNgayDuNamNamIsInShouldWork() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytNgayDuNamNam in DEFAULT_BHYT_NGAY_DU_NAM_NAM or UPDATED_BHYT_NGAY_DU_NAM_NAM
        defaultDotDieuTriShouldBeFound("bhytNgayDuNamNam.in=" + DEFAULT_BHYT_NGAY_DU_NAM_NAM + "," + UPDATED_BHYT_NGAY_DU_NAM_NAM);

        // Get all the dotDieuTriList where bhytNgayDuNamNam equals to UPDATED_BHYT_NGAY_DU_NAM_NAM
        defaultDotDieuTriShouldNotBeFound("bhytNgayDuNamNam.in=" + UPDATED_BHYT_NGAY_DU_NAM_NAM);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytNgayDuNamNamIsNullOrNotNull() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytNgayDuNamNam is not null
        defaultDotDieuTriShouldBeFound("bhytNgayDuNamNam.specified=true");

        // Get all the dotDieuTriList where bhytNgayDuNamNam is null
        defaultDotDieuTriShouldNotBeFound("bhytNgayDuNamNam.specified=false");
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytNgayDuNamNamIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytNgayDuNamNam is greater than or equal to DEFAULT_BHYT_NGAY_DU_NAM_NAM
        defaultDotDieuTriShouldBeFound("bhytNgayDuNamNam.greaterThanOrEqual=" + DEFAULT_BHYT_NGAY_DU_NAM_NAM);

        // Get all the dotDieuTriList where bhytNgayDuNamNam is greater than or equal to UPDATED_BHYT_NGAY_DU_NAM_NAM
        defaultDotDieuTriShouldNotBeFound("bhytNgayDuNamNam.greaterThanOrEqual=" + UPDATED_BHYT_NGAY_DU_NAM_NAM);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytNgayDuNamNamIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytNgayDuNamNam is less than or equal to DEFAULT_BHYT_NGAY_DU_NAM_NAM
        defaultDotDieuTriShouldBeFound("bhytNgayDuNamNam.lessThanOrEqual=" + DEFAULT_BHYT_NGAY_DU_NAM_NAM);

        // Get all the dotDieuTriList where bhytNgayDuNamNam is less than or equal to SMALLER_BHYT_NGAY_DU_NAM_NAM
        defaultDotDieuTriShouldNotBeFound("bhytNgayDuNamNam.lessThanOrEqual=" + SMALLER_BHYT_NGAY_DU_NAM_NAM);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytNgayDuNamNamIsLessThanSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytNgayDuNamNam is less than DEFAULT_BHYT_NGAY_DU_NAM_NAM
        defaultDotDieuTriShouldNotBeFound("bhytNgayDuNamNam.lessThan=" + DEFAULT_BHYT_NGAY_DU_NAM_NAM);

        // Get all the dotDieuTriList where bhytNgayDuNamNam is less than UPDATED_BHYT_NGAY_DU_NAM_NAM
        defaultDotDieuTriShouldBeFound("bhytNgayDuNamNam.lessThan=" + UPDATED_BHYT_NGAY_DU_NAM_NAM);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytNgayDuNamNamIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytNgayDuNamNam is greater than DEFAULT_BHYT_NGAY_DU_NAM_NAM
        defaultDotDieuTriShouldNotBeFound("bhytNgayDuNamNam.greaterThan=" + DEFAULT_BHYT_NGAY_DU_NAM_NAM);

        // Get all the dotDieuTriList where bhytNgayDuNamNam is greater than SMALLER_BHYT_NGAY_DU_NAM_NAM
        defaultDotDieuTriShouldBeFound("bhytNgayDuNamNam.greaterThan=" + SMALLER_BHYT_NGAY_DU_NAM_NAM);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytNgayHetHanIsEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytNgayHetHan equals to DEFAULT_BHYT_NGAY_HET_HAN
        defaultDotDieuTriShouldBeFound("bhytNgayHetHan.equals=" + DEFAULT_BHYT_NGAY_HET_HAN);

        // Get all the dotDieuTriList where bhytNgayHetHan equals to UPDATED_BHYT_NGAY_HET_HAN
        defaultDotDieuTriShouldNotBeFound("bhytNgayHetHan.equals=" + UPDATED_BHYT_NGAY_HET_HAN);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytNgayHetHanIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytNgayHetHan not equals to DEFAULT_BHYT_NGAY_HET_HAN
        defaultDotDieuTriShouldNotBeFound("bhytNgayHetHan.notEquals=" + DEFAULT_BHYT_NGAY_HET_HAN);

        // Get all the dotDieuTriList where bhytNgayHetHan not equals to UPDATED_BHYT_NGAY_HET_HAN
        defaultDotDieuTriShouldBeFound("bhytNgayHetHan.notEquals=" + UPDATED_BHYT_NGAY_HET_HAN);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytNgayHetHanIsInShouldWork() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytNgayHetHan in DEFAULT_BHYT_NGAY_HET_HAN or UPDATED_BHYT_NGAY_HET_HAN
        defaultDotDieuTriShouldBeFound("bhytNgayHetHan.in=" + DEFAULT_BHYT_NGAY_HET_HAN + "," + UPDATED_BHYT_NGAY_HET_HAN);

        // Get all the dotDieuTriList where bhytNgayHetHan equals to UPDATED_BHYT_NGAY_HET_HAN
        defaultDotDieuTriShouldNotBeFound("bhytNgayHetHan.in=" + UPDATED_BHYT_NGAY_HET_HAN);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytNgayHetHanIsNullOrNotNull() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytNgayHetHan is not null
        defaultDotDieuTriShouldBeFound("bhytNgayHetHan.specified=true");

        // Get all the dotDieuTriList where bhytNgayHetHan is null
        defaultDotDieuTriShouldNotBeFound("bhytNgayHetHan.specified=false");
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytNgayHetHanIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytNgayHetHan is greater than or equal to DEFAULT_BHYT_NGAY_HET_HAN
        defaultDotDieuTriShouldBeFound("bhytNgayHetHan.greaterThanOrEqual=" + DEFAULT_BHYT_NGAY_HET_HAN);

        // Get all the dotDieuTriList where bhytNgayHetHan is greater than or equal to UPDATED_BHYT_NGAY_HET_HAN
        defaultDotDieuTriShouldNotBeFound("bhytNgayHetHan.greaterThanOrEqual=" + UPDATED_BHYT_NGAY_HET_HAN);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytNgayHetHanIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytNgayHetHan is less than or equal to DEFAULT_BHYT_NGAY_HET_HAN
        defaultDotDieuTriShouldBeFound("bhytNgayHetHan.lessThanOrEqual=" + DEFAULT_BHYT_NGAY_HET_HAN);

        // Get all the dotDieuTriList where bhytNgayHetHan is less than or equal to SMALLER_BHYT_NGAY_HET_HAN
        defaultDotDieuTriShouldNotBeFound("bhytNgayHetHan.lessThanOrEqual=" + SMALLER_BHYT_NGAY_HET_HAN);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytNgayHetHanIsLessThanSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytNgayHetHan is less than DEFAULT_BHYT_NGAY_HET_HAN
        defaultDotDieuTriShouldNotBeFound("bhytNgayHetHan.lessThan=" + DEFAULT_BHYT_NGAY_HET_HAN);

        // Get all the dotDieuTriList where bhytNgayHetHan is less than UPDATED_BHYT_NGAY_HET_HAN
        defaultDotDieuTriShouldBeFound("bhytNgayHetHan.lessThan=" + UPDATED_BHYT_NGAY_HET_HAN);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytNgayHetHanIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytNgayHetHan is greater than DEFAULT_BHYT_NGAY_HET_HAN
        defaultDotDieuTriShouldNotBeFound("bhytNgayHetHan.greaterThan=" + DEFAULT_BHYT_NGAY_HET_HAN);

        // Get all the dotDieuTriList where bhytNgayHetHan is greater than SMALLER_BHYT_NGAY_HET_HAN
        defaultDotDieuTriShouldBeFound("bhytNgayHetHan.greaterThan=" + SMALLER_BHYT_NGAY_HET_HAN);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytNoiTinhIsEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytNoiTinh equals to DEFAULT_BHYT_NOI_TINH
        defaultDotDieuTriShouldBeFound("bhytNoiTinh.equals=" + DEFAULT_BHYT_NOI_TINH);

        // Get all the dotDieuTriList where bhytNoiTinh equals to UPDATED_BHYT_NOI_TINH
        defaultDotDieuTriShouldNotBeFound("bhytNoiTinh.equals=" + UPDATED_BHYT_NOI_TINH);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytNoiTinhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytNoiTinh not equals to DEFAULT_BHYT_NOI_TINH
        defaultDotDieuTriShouldNotBeFound("bhytNoiTinh.notEquals=" + DEFAULT_BHYT_NOI_TINH);

        // Get all the dotDieuTriList where bhytNoiTinh not equals to UPDATED_BHYT_NOI_TINH
        defaultDotDieuTriShouldBeFound("bhytNoiTinh.notEquals=" + UPDATED_BHYT_NOI_TINH);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytNoiTinhIsInShouldWork() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytNoiTinh in DEFAULT_BHYT_NOI_TINH or UPDATED_BHYT_NOI_TINH
        defaultDotDieuTriShouldBeFound("bhytNoiTinh.in=" + DEFAULT_BHYT_NOI_TINH + "," + UPDATED_BHYT_NOI_TINH);

        // Get all the dotDieuTriList where bhytNoiTinh equals to UPDATED_BHYT_NOI_TINH
        defaultDotDieuTriShouldNotBeFound("bhytNoiTinh.in=" + UPDATED_BHYT_NOI_TINH);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytNoiTinhIsNullOrNotNull() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytNoiTinh is not null
        defaultDotDieuTriShouldBeFound("bhytNoiTinh.specified=true");

        // Get all the dotDieuTriList where bhytNoiTinh is null
        defaultDotDieuTriShouldNotBeFound("bhytNoiTinh.specified=false");
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytSoTheIsEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytSoThe equals to DEFAULT_BHYT_SO_THE
        defaultDotDieuTriShouldBeFound("bhytSoThe.equals=" + DEFAULT_BHYT_SO_THE);

        // Get all the dotDieuTriList where bhytSoThe equals to UPDATED_BHYT_SO_THE
        defaultDotDieuTriShouldNotBeFound("bhytSoThe.equals=" + UPDATED_BHYT_SO_THE);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytSoTheIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytSoThe not equals to DEFAULT_BHYT_SO_THE
        defaultDotDieuTriShouldNotBeFound("bhytSoThe.notEquals=" + DEFAULT_BHYT_SO_THE);

        // Get all the dotDieuTriList where bhytSoThe not equals to UPDATED_BHYT_SO_THE
        defaultDotDieuTriShouldBeFound("bhytSoThe.notEquals=" + UPDATED_BHYT_SO_THE);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytSoTheIsInShouldWork() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytSoThe in DEFAULT_BHYT_SO_THE or UPDATED_BHYT_SO_THE
        defaultDotDieuTriShouldBeFound("bhytSoThe.in=" + DEFAULT_BHYT_SO_THE + "," + UPDATED_BHYT_SO_THE);

        // Get all the dotDieuTriList where bhytSoThe equals to UPDATED_BHYT_SO_THE
        defaultDotDieuTriShouldNotBeFound("bhytSoThe.in=" + UPDATED_BHYT_SO_THE);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytSoTheIsNullOrNotNull() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytSoThe is not null
        defaultDotDieuTriShouldBeFound("bhytSoThe.specified=true");

        // Get all the dotDieuTriList where bhytSoThe is null
        defaultDotDieuTriShouldNotBeFound("bhytSoThe.specified=false");
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytSoTheContainsSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytSoThe contains DEFAULT_BHYT_SO_THE
        defaultDotDieuTriShouldBeFound("bhytSoThe.contains=" + DEFAULT_BHYT_SO_THE);

        // Get all the dotDieuTriList where bhytSoThe contains UPDATED_BHYT_SO_THE
        defaultDotDieuTriShouldNotBeFound("bhytSoThe.contains=" + UPDATED_BHYT_SO_THE);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytSoTheNotContainsSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytSoThe does not contain DEFAULT_BHYT_SO_THE
        defaultDotDieuTriShouldNotBeFound("bhytSoThe.doesNotContain=" + DEFAULT_BHYT_SO_THE);

        // Get all the dotDieuTriList where bhytSoThe does not contain UPDATED_BHYT_SO_THE
        defaultDotDieuTriShouldBeFound("bhytSoThe.doesNotContain=" + UPDATED_BHYT_SO_THE);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytTyLeMienGiamIsEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytTyLeMienGiam equals to DEFAULT_BHYT_TY_LE_MIEN_GIAM
        defaultDotDieuTriShouldBeFound("bhytTyLeMienGiam.equals=" + DEFAULT_BHYT_TY_LE_MIEN_GIAM);

        // Get all the dotDieuTriList where bhytTyLeMienGiam equals to UPDATED_BHYT_TY_LE_MIEN_GIAM
        defaultDotDieuTriShouldNotBeFound("bhytTyLeMienGiam.equals=" + UPDATED_BHYT_TY_LE_MIEN_GIAM);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytTyLeMienGiamIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytTyLeMienGiam not equals to DEFAULT_BHYT_TY_LE_MIEN_GIAM
        defaultDotDieuTriShouldNotBeFound("bhytTyLeMienGiam.notEquals=" + DEFAULT_BHYT_TY_LE_MIEN_GIAM);

        // Get all the dotDieuTriList where bhytTyLeMienGiam not equals to UPDATED_BHYT_TY_LE_MIEN_GIAM
        defaultDotDieuTriShouldBeFound("bhytTyLeMienGiam.notEquals=" + UPDATED_BHYT_TY_LE_MIEN_GIAM);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytTyLeMienGiamIsInShouldWork() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytTyLeMienGiam in DEFAULT_BHYT_TY_LE_MIEN_GIAM or UPDATED_BHYT_TY_LE_MIEN_GIAM
        defaultDotDieuTriShouldBeFound("bhytTyLeMienGiam.in=" + DEFAULT_BHYT_TY_LE_MIEN_GIAM + "," + UPDATED_BHYT_TY_LE_MIEN_GIAM);

        // Get all the dotDieuTriList where bhytTyLeMienGiam equals to UPDATED_BHYT_TY_LE_MIEN_GIAM
        defaultDotDieuTriShouldNotBeFound("bhytTyLeMienGiam.in=" + UPDATED_BHYT_TY_LE_MIEN_GIAM);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytTyLeMienGiamIsNullOrNotNull() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytTyLeMienGiam is not null
        defaultDotDieuTriShouldBeFound("bhytTyLeMienGiam.specified=true");

        // Get all the dotDieuTriList where bhytTyLeMienGiam is null
        defaultDotDieuTriShouldNotBeFound("bhytTyLeMienGiam.specified=false");
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytTyLeMienGiamIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytTyLeMienGiam is greater than or equal to DEFAULT_BHYT_TY_LE_MIEN_GIAM
        defaultDotDieuTriShouldBeFound("bhytTyLeMienGiam.greaterThanOrEqual=" + DEFAULT_BHYT_TY_LE_MIEN_GIAM);

        // Get all the dotDieuTriList where bhytTyLeMienGiam is greater than or equal to UPDATED_BHYT_TY_LE_MIEN_GIAM
        defaultDotDieuTriShouldNotBeFound("bhytTyLeMienGiam.greaterThanOrEqual=" + UPDATED_BHYT_TY_LE_MIEN_GIAM);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytTyLeMienGiamIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytTyLeMienGiam is less than or equal to DEFAULT_BHYT_TY_LE_MIEN_GIAM
        defaultDotDieuTriShouldBeFound("bhytTyLeMienGiam.lessThanOrEqual=" + DEFAULT_BHYT_TY_LE_MIEN_GIAM);

        // Get all the dotDieuTriList where bhytTyLeMienGiam is less than or equal to SMALLER_BHYT_TY_LE_MIEN_GIAM
        defaultDotDieuTriShouldNotBeFound("bhytTyLeMienGiam.lessThanOrEqual=" + SMALLER_BHYT_TY_LE_MIEN_GIAM);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytTyLeMienGiamIsLessThanSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytTyLeMienGiam is less than DEFAULT_BHYT_TY_LE_MIEN_GIAM
        defaultDotDieuTriShouldNotBeFound("bhytTyLeMienGiam.lessThan=" + DEFAULT_BHYT_TY_LE_MIEN_GIAM);

        // Get all the dotDieuTriList where bhytTyLeMienGiam is less than UPDATED_BHYT_TY_LE_MIEN_GIAM
        defaultDotDieuTriShouldBeFound("bhytTyLeMienGiam.lessThan=" + UPDATED_BHYT_TY_LE_MIEN_GIAM);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytTyLeMienGiamIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytTyLeMienGiam is greater than DEFAULT_BHYT_TY_LE_MIEN_GIAM
        defaultDotDieuTriShouldNotBeFound("bhytTyLeMienGiam.greaterThan=" + DEFAULT_BHYT_TY_LE_MIEN_GIAM);

        // Get all the dotDieuTriList where bhytTyLeMienGiam is greater than SMALLER_BHYT_TY_LE_MIEN_GIAM
        defaultDotDieuTriShouldBeFound("bhytTyLeMienGiam.greaterThan=" + SMALLER_BHYT_TY_LE_MIEN_GIAM);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytTyLeMienGiamKtcIsEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytTyLeMienGiamKtc equals to DEFAULT_BHYT_TY_LE_MIEN_GIAM_KTC
        defaultDotDieuTriShouldBeFound("bhytTyLeMienGiamKtc.equals=" + DEFAULT_BHYT_TY_LE_MIEN_GIAM_KTC);

        // Get all the dotDieuTriList where bhytTyLeMienGiamKtc equals to UPDATED_BHYT_TY_LE_MIEN_GIAM_KTC
        defaultDotDieuTriShouldNotBeFound("bhytTyLeMienGiamKtc.equals=" + UPDATED_BHYT_TY_LE_MIEN_GIAM_KTC);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytTyLeMienGiamKtcIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytTyLeMienGiamKtc not equals to DEFAULT_BHYT_TY_LE_MIEN_GIAM_KTC
        defaultDotDieuTriShouldNotBeFound("bhytTyLeMienGiamKtc.notEquals=" + DEFAULT_BHYT_TY_LE_MIEN_GIAM_KTC);

        // Get all the dotDieuTriList where bhytTyLeMienGiamKtc not equals to UPDATED_BHYT_TY_LE_MIEN_GIAM_KTC
        defaultDotDieuTriShouldBeFound("bhytTyLeMienGiamKtc.notEquals=" + UPDATED_BHYT_TY_LE_MIEN_GIAM_KTC);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytTyLeMienGiamKtcIsInShouldWork() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytTyLeMienGiamKtc in DEFAULT_BHYT_TY_LE_MIEN_GIAM_KTC or UPDATED_BHYT_TY_LE_MIEN_GIAM_KTC
        defaultDotDieuTriShouldBeFound("bhytTyLeMienGiamKtc.in=" + DEFAULT_BHYT_TY_LE_MIEN_GIAM_KTC + "," + UPDATED_BHYT_TY_LE_MIEN_GIAM_KTC);

        // Get all the dotDieuTriList where bhytTyLeMienGiamKtc equals to UPDATED_BHYT_TY_LE_MIEN_GIAM_KTC
        defaultDotDieuTriShouldNotBeFound("bhytTyLeMienGiamKtc.in=" + UPDATED_BHYT_TY_LE_MIEN_GIAM_KTC);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytTyLeMienGiamKtcIsNullOrNotNull() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytTyLeMienGiamKtc is not null
        defaultDotDieuTriShouldBeFound("bhytTyLeMienGiamKtc.specified=true");

        // Get all the dotDieuTriList where bhytTyLeMienGiamKtc is null
        defaultDotDieuTriShouldNotBeFound("bhytTyLeMienGiamKtc.specified=false");
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytTyLeMienGiamKtcIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytTyLeMienGiamKtc is greater than or equal to DEFAULT_BHYT_TY_LE_MIEN_GIAM_KTC
        defaultDotDieuTriShouldBeFound("bhytTyLeMienGiamKtc.greaterThanOrEqual=" + DEFAULT_BHYT_TY_LE_MIEN_GIAM_KTC);

        // Get all the dotDieuTriList where bhytTyLeMienGiamKtc is greater than or equal to UPDATED_BHYT_TY_LE_MIEN_GIAM_KTC
        defaultDotDieuTriShouldNotBeFound("bhytTyLeMienGiamKtc.greaterThanOrEqual=" + UPDATED_BHYT_TY_LE_MIEN_GIAM_KTC);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytTyLeMienGiamKtcIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytTyLeMienGiamKtc is less than or equal to DEFAULT_BHYT_TY_LE_MIEN_GIAM_KTC
        defaultDotDieuTriShouldBeFound("bhytTyLeMienGiamKtc.lessThanOrEqual=" + DEFAULT_BHYT_TY_LE_MIEN_GIAM_KTC);

        // Get all the dotDieuTriList where bhytTyLeMienGiamKtc is less than or equal to SMALLER_BHYT_TY_LE_MIEN_GIAM_KTC
        defaultDotDieuTriShouldNotBeFound("bhytTyLeMienGiamKtc.lessThanOrEqual=" + SMALLER_BHYT_TY_LE_MIEN_GIAM_KTC);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytTyLeMienGiamKtcIsLessThanSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytTyLeMienGiamKtc is less than DEFAULT_BHYT_TY_LE_MIEN_GIAM_KTC
        defaultDotDieuTriShouldNotBeFound("bhytTyLeMienGiamKtc.lessThan=" + DEFAULT_BHYT_TY_LE_MIEN_GIAM_KTC);

        // Get all the dotDieuTriList where bhytTyLeMienGiamKtc is less than UPDATED_BHYT_TY_LE_MIEN_GIAM_KTC
        defaultDotDieuTriShouldBeFound("bhytTyLeMienGiamKtc.lessThan=" + UPDATED_BHYT_TY_LE_MIEN_GIAM_KTC);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytTyLeMienGiamKtcIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytTyLeMienGiamKtc is greater than DEFAULT_BHYT_TY_LE_MIEN_GIAM_KTC
        defaultDotDieuTriShouldNotBeFound("bhytTyLeMienGiamKtc.greaterThan=" + DEFAULT_BHYT_TY_LE_MIEN_GIAM_KTC);

        // Get all the dotDieuTriList where bhytTyLeMienGiamKtc is greater than SMALLER_BHYT_TY_LE_MIEN_GIAM_KTC
        defaultDotDieuTriShouldBeFound("bhytTyLeMienGiamKtc.greaterThan=" + SMALLER_BHYT_TY_LE_MIEN_GIAM_KTC);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByCoBaoHiemIsEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where coBaoHiem equals to DEFAULT_CO_BAO_HIEM
        defaultDotDieuTriShouldBeFound("coBaoHiem.equals=" + DEFAULT_CO_BAO_HIEM);

        // Get all the dotDieuTriList where coBaoHiem equals to UPDATED_CO_BAO_HIEM
        defaultDotDieuTriShouldNotBeFound("coBaoHiem.equals=" + UPDATED_CO_BAO_HIEM);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByCoBaoHiemIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where coBaoHiem not equals to DEFAULT_CO_BAO_HIEM
        defaultDotDieuTriShouldNotBeFound("coBaoHiem.notEquals=" + DEFAULT_CO_BAO_HIEM);

        // Get all the dotDieuTriList where coBaoHiem not equals to UPDATED_CO_BAO_HIEM
        defaultDotDieuTriShouldBeFound("coBaoHiem.notEquals=" + UPDATED_CO_BAO_HIEM);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByCoBaoHiemIsInShouldWork() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where coBaoHiem in DEFAULT_CO_BAO_HIEM or UPDATED_CO_BAO_HIEM
        defaultDotDieuTriShouldBeFound("coBaoHiem.in=" + DEFAULT_CO_BAO_HIEM + "," + UPDATED_CO_BAO_HIEM);

        // Get all the dotDieuTriList where coBaoHiem equals to UPDATED_CO_BAO_HIEM
        defaultDotDieuTriShouldNotBeFound("coBaoHiem.in=" + UPDATED_CO_BAO_HIEM);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByCoBaoHiemIsNullOrNotNull() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where coBaoHiem is not null
        defaultDotDieuTriShouldBeFound("coBaoHiem.specified=true");

        // Get all the dotDieuTriList where coBaoHiem is null
        defaultDotDieuTriShouldNotBeFound("coBaoHiem.specified=false");
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytDiaChiIsEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytDiaChi equals to DEFAULT_BHYT_DIA_CHI
        defaultDotDieuTriShouldBeFound("bhytDiaChi.equals=" + DEFAULT_BHYT_DIA_CHI);

        // Get all the dotDieuTriList where bhytDiaChi equals to UPDATED_BHYT_DIA_CHI
        defaultDotDieuTriShouldNotBeFound("bhytDiaChi.equals=" + UPDATED_BHYT_DIA_CHI);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytDiaChiIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytDiaChi not equals to DEFAULT_BHYT_DIA_CHI
        defaultDotDieuTriShouldNotBeFound("bhytDiaChi.notEquals=" + DEFAULT_BHYT_DIA_CHI);

        // Get all the dotDieuTriList where bhytDiaChi not equals to UPDATED_BHYT_DIA_CHI
        defaultDotDieuTriShouldBeFound("bhytDiaChi.notEquals=" + UPDATED_BHYT_DIA_CHI);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytDiaChiIsInShouldWork() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytDiaChi in DEFAULT_BHYT_DIA_CHI or UPDATED_BHYT_DIA_CHI
        defaultDotDieuTriShouldBeFound("bhytDiaChi.in=" + DEFAULT_BHYT_DIA_CHI + "," + UPDATED_BHYT_DIA_CHI);

        // Get all the dotDieuTriList where bhytDiaChi equals to UPDATED_BHYT_DIA_CHI
        defaultDotDieuTriShouldNotBeFound("bhytDiaChi.in=" + UPDATED_BHYT_DIA_CHI);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytDiaChiIsNullOrNotNull() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytDiaChi is not null
        defaultDotDieuTriShouldBeFound("bhytDiaChi.specified=true");

        // Get all the dotDieuTriList where bhytDiaChi is null
        defaultDotDieuTriShouldNotBeFound("bhytDiaChi.specified=false");
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytDiaChiContainsSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytDiaChi contains DEFAULT_BHYT_DIA_CHI
        defaultDotDieuTriShouldBeFound("bhytDiaChi.contains=" + DEFAULT_BHYT_DIA_CHI);

        // Get all the dotDieuTriList where bhytDiaChi contains UPDATED_BHYT_DIA_CHI
        defaultDotDieuTriShouldNotBeFound("bhytDiaChi.contains=" + UPDATED_BHYT_DIA_CHI);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytDiaChiNotContainsSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytDiaChi does not contain DEFAULT_BHYT_DIA_CHI
        defaultDotDieuTriShouldNotBeFound("bhytDiaChi.doesNotContain=" + DEFAULT_BHYT_DIA_CHI);

        // Get all the dotDieuTriList where bhytDiaChi does not contain UPDATED_BHYT_DIA_CHI
        defaultDotDieuTriShouldBeFound("bhytDiaChi.doesNotContain=" + UPDATED_BHYT_DIA_CHI);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByDoiTuongBhytTenIsEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where doiTuongBhytTen equals to DEFAULT_DOI_TUONG_BHYT_TEN
        defaultDotDieuTriShouldBeFound("doiTuongBhytTen.equals=" + DEFAULT_DOI_TUONG_BHYT_TEN);

        // Get all the dotDieuTriList where doiTuongBhytTen equals to UPDATED_DOI_TUONG_BHYT_TEN
        defaultDotDieuTriShouldNotBeFound("doiTuongBhytTen.equals=" + UPDATED_DOI_TUONG_BHYT_TEN);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByDoiTuongBhytTenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where doiTuongBhytTen not equals to DEFAULT_DOI_TUONG_BHYT_TEN
        defaultDotDieuTriShouldNotBeFound("doiTuongBhytTen.notEquals=" + DEFAULT_DOI_TUONG_BHYT_TEN);

        // Get all the dotDieuTriList where doiTuongBhytTen not equals to UPDATED_DOI_TUONG_BHYT_TEN
        defaultDotDieuTriShouldBeFound("doiTuongBhytTen.notEquals=" + UPDATED_DOI_TUONG_BHYT_TEN);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByDoiTuongBhytTenIsInShouldWork() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where doiTuongBhytTen in DEFAULT_DOI_TUONG_BHYT_TEN or UPDATED_DOI_TUONG_BHYT_TEN
        defaultDotDieuTriShouldBeFound("doiTuongBhytTen.in=" + DEFAULT_DOI_TUONG_BHYT_TEN + "," + UPDATED_DOI_TUONG_BHYT_TEN);

        // Get all the dotDieuTriList where doiTuongBhytTen equals to UPDATED_DOI_TUONG_BHYT_TEN
        defaultDotDieuTriShouldNotBeFound("doiTuongBhytTen.in=" + UPDATED_DOI_TUONG_BHYT_TEN);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByDoiTuongBhytTenIsNullOrNotNull() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where doiTuongBhytTen is not null
        defaultDotDieuTriShouldBeFound("doiTuongBhytTen.specified=true");

        // Get all the dotDieuTriList where doiTuongBhytTen is null
        defaultDotDieuTriShouldNotBeFound("doiTuongBhytTen.specified=false");
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByDoiTuongBhytTenContainsSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where doiTuongBhytTen contains DEFAULT_DOI_TUONG_BHYT_TEN
        defaultDotDieuTriShouldBeFound("doiTuongBhytTen.contains=" + DEFAULT_DOI_TUONG_BHYT_TEN);

        // Get all the dotDieuTriList where doiTuongBhytTen contains UPDATED_DOI_TUONG_BHYT_TEN
        defaultDotDieuTriShouldNotBeFound("doiTuongBhytTen.contains=" + UPDATED_DOI_TUONG_BHYT_TEN);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByDoiTuongBhytTenNotContainsSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where doiTuongBhytTen does not contain DEFAULT_DOI_TUONG_BHYT_TEN
        defaultDotDieuTriShouldNotBeFound("doiTuongBhytTen.doesNotContain=" + DEFAULT_DOI_TUONG_BHYT_TEN);

        // Get all the dotDieuTriList where doiTuongBhytTen does not contain UPDATED_DOI_TUONG_BHYT_TEN
        defaultDotDieuTriShouldBeFound("doiTuongBhytTen.doesNotContain=" + UPDATED_DOI_TUONG_BHYT_TEN);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByDungTuyenIsEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where dungTuyen equals to DEFAULT_DUNG_TUYEN
        defaultDotDieuTriShouldBeFound("dungTuyen.equals=" + DEFAULT_DUNG_TUYEN);

        // Get all the dotDieuTriList where dungTuyen equals to UPDATED_DUNG_TUYEN
        defaultDotDieuTriShouldNotBeFound("dungTuyen.equals=" + UPDATED_DUNG_TUYEN);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByDungTuyenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where dungTuyen not equals to DEFAULT_DUNG_TUYEN
        defaultDotDieuTriShouldNotBeFound("dungTuyen.notEquals=" + DEFAULT_DUNG_TUYEN);

        // Get all the dotDieuTriList where dungTuyen not equals to UPDATED_DUNG_TUYEN
        defaultDotDieuTriShouldBeFound("dungTuyen.notEquals=" + UPDATED_DUNG_TUYEN);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByDungTuyenIsInShouldWork() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where dungTuyen in DEFAULT_DUNG_TUYEN or UPDATED_DUNG_TUYEN
        defaultDotDieuTriShouldBeFound("dungTuyen.in=" + DEFAULT_DUNG_TUYEN + "," + UPDATED_DUNG_TUYEN);

        // Get all the dotDieuTriList where dungTuyen equals to UPDATED_DUNG_TUYEN
        defaultDotDieuTriShouldNotBeFound("dungTuyen.in=" + UPDATED_DUNG_TUYEN);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByDungTuyenIsNullOrNotNull() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where dungTuyen is not null
        defaultDotDieuTriShouldBeFound("dungTuyen.specified=true");

        // Get all the dotDieuTriList where dungTuyen is null
        defaultDotDieuTriShouldNotBeFound("dungTuyen.specified=false");
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByGiayToTreEmIsEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where giayToTreEm equals to DEFAULT_GIAY_TO_TRE_EM
        defaultDotDieuTriShouldBeFound("giayToTreEm.equals=" + DEFAULT_GIAY_TO_TRE_EM);

        // Get all the dotDieuTriList where giayToTreEm equals to UPDATED_GIAY_TO_TRE_EM
        defaultDotDieuTriShouldNotBeFound("giayToTreEm.equals=" + UPDATED_GIAY_TO_TRE_EM);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByGiayToTreEmIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where giayToTreEm not equals to DEFAULT_GIAY_TO_TRE_EM
        defaultDotDieuTriShouldNotBeFound("giayToTreEm.notEquals=" + DEFAULT_GIAY_TO_TRE_EM);

        // Get all the dotDieuTriList where giayToTreEm not equals to UPDATED_GIAY_TO_TRE_EM
        defaultDotDieuTriShouldBeFound("giayToTreEm.notEquals=" + UPDATED_GIAY_TO_TRE_EM);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByGiayToTreEmIsInShouldWork() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where giayToTreEm in DEFAULT_GIAY_TO_TRE_EM or UPDATED_GIAY_TO_TRE_EM
        defaultDotDieuTriShouldBeFound("giayToTreEm.in=" + DEFAULT_GIAY_TO_TRE_EM + "," + UPDATED_GIAY_TO_TRE_EM);

        // Get all the dotDieuTriList where giayToTreEm equals to UPDATED_GIAY_TO_TRE_EM
        defaultDotDieuTriShouldNotBeFound("giayToTreEm.in=" + UPDATED_GIAY_TO_TRE_EM);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByGiayToTreEmIsNullOrNotNull() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where giayToTreEm is not null
        defaultDotDieuTriShouldBeFound("giayToTreEm.specified=true");

        // Get all the dotDieuTriList where giayToTreEm is null
        defaultDotDieuTriShouldNotBeFound("giayToTreEm.specified=false");
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByGiayToTreEmContainsSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where giayToTreEm contains DEFAULT_GIAY_TO_TRE_EM
        defaultDotDieuTriShouldBeFound("giayToTreEm.contains=" + DEFAULT_GIAY_TO_TRE_EM);

        // Get all the dotDieuTriList where giayToTreEm contains UPDATED_GIAY_TO_TRE_EM
        defaultDotDieuTriShouldNotBeFound("giayToTreEm.contains=" + UPDATED_GIAY_TO_TRE_EM);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByGiayToTreEmNotContainsSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where giayToTreEm does not contain DEFAULT_GIAY_TO_TRE_EM
        defaultDotDieuTriShouldNotBeFound("giayToTreEm.doesNotContain=" + DEFAULT_GIAY_TO_TRE_EM);

        // Get all the dotDieuTriList where giayToTreEm does not contain UPDATED_GIAY_TO_TRE_EM
        defaultDotDieuTriShouldBeFound("giayToTreEm.doesNotContain=" + UPDATED_GIAY_TO_TRE_EM);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByLoaiGiayToTreEmIsEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where loaiGiayToTreEm equals to DEFAULT_LOAI_GIAY_TO_TRE_EM
        defaultDotDieuTriShouldBeFound("loaiGiayToTreEm.equals=" + DEFAULT_LOAI_GIAY_TO_TRE_EM);

        // Get all the dotDieuTriList where loaiGiayToTreEm equals to UPDATED_LOAI_GIAY_TO_TRE_EM
        defaultDotDieuTriShouldNotBeFound("loaiGiayToTreEm.equals=" + UPDATED_LOAI_GIAY_TO_TRE_EM);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByLoaiGiayToTreEmIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where loaiGiayToTreEm not equals to DEFAULT_LOAI_GIAY_TO_TRE_EM
        defaultDotDieuTriShouldNotBeFound("loaiGiayToTreEm.notEquals=" + DEFAULT_LOAI_GIAY_TO_TRE_EM);

        // Get all the dotDieuTriList where loaiGiayToTreEm not equals to UPDATED_LOAI_GIAY_TO_TRE_EM
        defaultDotDieuTriShouldBeFound("loaiGiayToTreEm.notEquals=" + UPDATED_LOAI_GIAY_TO_TRE_EM);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByLoaiGiayToTreEmIsInShouldWork() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where loaiGiayToTreEm in DEFAULT_LOAI_GIAY_TO_TRE_EM or UPDATED_LOAI_GIAY_TO_TRE_EM
        defaultDotDieuTriShouldBeFound("loaiGiayToTreEm.in=" + DEFAULT_LOAI_GIAY_TO_TRE_EM + "," + UPDATED_LOAI_GIAY_TO_TRE_EM);

        // Get all the dotDieuTriList where loaiGiayToTreEm equals to UPDATED_LOAI_GIAY_TO_TRE_EM
        defaultDotDieuTriShouldNotBeFound("loaiGiayToTreEm.in=" + UPDATED_LOAI_GIAY_TO_TRE_EM);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByLoaiGiayToTreEmIsNullOrNotNull() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where loaiGiayToTreEm is not null
        defaultDotDieuTriShouldBeFound("loaiGiayToTreEm.specified=true");

        // Get all the dotDieuTriList where loaiGiayToTreEm is null
        defaultDotDieuTriShouldNotBeFound("loaiGiayToTreEm.specified=false");
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByLoaiGiayToTreEmIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where loaiGiayToTreEm is greater than or equal to DEFAULT_LOAI_GIAY_TO_TRE_EM
        defaultDotDieuTriShouldBeFound("loaiGiayToTreEm.greaterThanOrEqual=" + DEFAULT_LOAI_GIAY_TO_TRE_EM);

        // Get all the dotDieuTriList where loaiGiayToTreEm is greater than or equal to UPDATED_LOAI_GIAY_TO_TRE_EM
        defaultDotDieuTriShouldNotBeFound("loaiGiayToTreEm.greaterThanOrEqual=" + UPDATED_LOAI_GIAY_TO_TRE_EM);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByLoaiGiayToTreEmIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where loaiGiayToTreEm is less than or equal to DEFAULT_LOAI_GIAY_TO_TRE_EM
        defaultDotDieuTriShouldBeFound("loaiGiayToTreEm.lessThanOrEqual=" + DEFAULT_LOAI_GIAY_TO_TRE_EM);

        // Get all the dotDieuTriList where loaiGiayToTreEm is less than or equal to SMALLER_LOAI_GIAY_TO_TRE_EM
        defaultDotDieuTriShouldNotBeFound("loaiGiayToTreEm.lessThanOrEqual=" + SMALLER_LOAI_GIAY_TO_TRE_EM);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByLoaiGiayToTreEmIsLessThanSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where loaiGiayToTreEm is less than DEFAULT_LOAI_GIAY_TO_TRE_EM
        defaultDotDieuTriShouldNotBeFound("loaiGiayToTreEm.lessThan=" + DEFAULT_LOAI_GIAY_TO_TRE_EM);

        // Get all the dotDieuTriList where loaiGiayToTreEm is less than UPDATED_LOAI_GIAY_TO_TRE_EM
        defaultDotDieuTriShouldBeFound("loaiGiayToTreEm.lessThan=" + UPDATED_LOAI_GIAY_TO_TRE_EM);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByLoaiGiayToTreEmIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where loaiGiayToTreEm is greater than DEFAULT_LOAI_GIAY_TO_TRE_EM
        defaultDotDieuTriShouldNotBeFound("loaiGiayToTreEm.greaterThan=" + DEFAULT_LOAI_GIAY_TO_TRE_EM);

        // Get all the dotDieuTriList where loaiGiayToTreEm is greater than SMALLER_LOAI_GIAY_TO_TRE_EM
        defaultDotDieuTriShouldBeFound("loaiGiayToTreEm.greaterThan=" + SMALLER_LOAI_GIAY_TO_TRE_EM);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByNgayMienCungChiTraIsEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where ngayMienCungChiTra equals to DEFAULT_NGAY_MIEN_CUNG_CHI_TRA
        defaultDotDieuTriShouldBeFound("ngayMienCungChiTra.equals=" + DEFAULT_NGAY_MIEN_CUNG_CHI_TRA);

        // Get all the dotDieuTriList where ngayMienCungChiTra equals to UPDATED_NGAY_MIEN_CUNG_CHI_TRA
        defaultDotDieuTriShouldNotBeFound("ngayMienCungChiTra.equals=" + UPDATED_NGAY_MIEN_CUNG_CHI_TRA);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByNgayMienCungChiTraIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where ngayMienCungChiTra not equals to DEFAULT_NGAY_MIEN_CUNG_CHI_TRA
        defaultDotDieuTriShouldNotBeFound("ngayMienCungChiTra.notEquals=" + DEFAULT_NGAY_MIEN_CUNG_CHI_TRA);

        // Get all the dotDieuTriList where ngayMienCungChiTra not equals to UPDATED_NGAY_MIEN_CUNG_CHI_TRA
        defaultDotDieuTriShouldBeFound("ngayMienCungChiTra.notEquals=" + UPDATED_NGAY_MIEN_CUNG_CHI_TRA);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByNgayMienCungChiTraIsInShouldWork() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where ngayMienCungChiTra in DEFAULT_NGAY_MIEN_CUNG_CHI_TRA or UPDATED_NGAY_MIEN_CUNG_CHI_TRA
        defaultDotDieuTriShouldBeFound("ngayMienCungChiTra.in=" + DEFAULT_NGAY_MIEN_CUNG_CHI_TRA + "," + UPDATED_NGAY_MIEN_CUNG_CHI_TRA);

        // Get all the dotDieuTriList where ngayMienCungChiTra equals to UPDATED_NGAY_MIEN_CUNG_CHI_TRA
        defaultDotDieuTriShouldNotBeFound("ngayMienCungChiTra.in=" + UPDATED_NGAY_MIEN_CUNG_CHI_TRA);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByNgayMienCungChiTraIsNullOrNotNull() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where ngayMienCungChiTra is not null
        defaultDotDieuTriShouldBeFound("ngayMienCungChiTra.specified=true");

        // Get all the dotDieuTriList where ngayMienCungChiTra is null
        defaultDotDieuTriShouldNotBeFound("ngayMienCungChiTra.specified=false");
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByNgayMienCungChiTraIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where ngayMienCungChiTra is greater than or equal to DEFAULT_NGAY_MIEN_CUNG_CHI_TRA
        defaultDotDieuTriShouldBeFound("ngayMienCungChiTra.greaterThanOrEqual=" + DEFAULT_NGAY_MIEN_CUNG_CHI_TRA);

        // Get all the dotDieuTriList where ngayMienCungChiTra is greater than or equal to UPDATED_NGAY_MIEN_CUNG_CHI_TRA
        defaultDotDieuTriShouldNotBeFound("ngayMienCungChiTra.greaterThanOrEqual=" + UPDATED_NGAY_MIEN_CUNG_CHI_TRA);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByNgayMienCungChiTraIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where ngayMienCungChiTra is less than or equal to DEFAULT_NGAY_MIEN_CUNG_CHI_TRA
        defaultDotDieuTriShouldBeFound("ngayMienCungChiTra.lessThanOrEqual=" + DEFAULT_NGAY_MIEN_CUNG_CHI_TRA);

        // Get all the dotDieuTriList where ngayMienCungChiTra is less than or equal to SMALLER_NGAY_MIEN_CUNG_CHI_TRA
        defaultDotDieuTriShouldNotBeFound("ngayMienCungChiTra.lessThanOrEqual=" + SMALLER_NGAY_MIEN_CUNG_CHI_TRA);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByNgayMienCungChiTraIsLessThanSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where ngayMienCungChiTra is less than DEFAULT_NGAY_MIEN_CUNG_CHI_TRA
        defaultDotDieuTriShouldNotBeFound("ngayMienCungChiTra.lessThan=" + DEFAULT_NGAY_MIEN_CUNG_CHI_TRA);

        // Get all the dotDieuTriList where ngayMienCungChiTra is less than UPDATED_NGAY_MIEN_CUNG_CHI_TRA
        defaultDotDieuTriShouldBeFound("ngayMienCungChiTra.lessThan=" + UPDATED_NGAY_MIEN_CUNG_CHI_TRA);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByNgayMienCungChiTraIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where ngayMienCungChiTra is greater than DEFAULT_NGAY_MIEN_CUNG_CHI_TRA
        defaultDotDieuTriShouldNotBeFound("ngayMienCungChiTra.greaterThan=" + DEFAULT_NGAY_MIEN_CUNG_CHI_TRA);

        // Get all the dotDieuTriList where ngayMienCungChiTra is greater than SMALLER_NGAY_MIEN_CUNG_CHI_TRA
        defaultDotDieuTriShouldBeFound("ngayMienCungChiTra.greaterThan=" + SMALLER_NGAY_MIEN_CUNG_CHI_TRA);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByTheTreEmIsEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where theTreEm equals to DEFAULT_THE_TRE_EM
        defaultDotDieuTriShouldBeFound("theTreEm.equals=" + DEFAULT_THE_TRE_EM);

        // Get all the dotDieuTriList where theTreEm equals to UPDATED_THE_TRE_EM
        defaultDotDieuTriShouldNotBeFound("theTreEm.equals=" + UPDATED_THE_TRE_EM);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByTheTreEmIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where theTreEm not equals to DEFAULT_THE_TRE_EM
        defaultDotDieuTriShouldNotBeFound("theTreEm.notEquals=" + DEFAULT_THE_TRE_EM);

        // Get all the dotDieuTriList where theTreEm not equals to UPDATED_THE_TRE_EM
        defaultDotDieuTriShouldBeFound("theTreEm.notEquals=" + UPDATED_THE_TRE_EM);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByTheTreEmIsInShouldWork() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where theTreEm in DEFAULT_THE_TRE_EM or UPDATED_THE_TRE_EM
        defaultDotDieuTriShouldBeFound("theTreEm.in=" + DEFAULT_THE_TRE_EM + "," + UPDATED_THE_TRE_EM);

        // Get all the dotDieuTriList where theTreEm equals to UPDATED_THE_TRE_EM
        defaultDotDieuTriShouldNotBeFound("theTreEm.in=" + UPDATED_THE_TRE_EM);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByTheTreEmIsNullOrNotNull() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where theTreEm is not null
        defaultDotDieuTriShouldBeFound("theTreEm.specified=true");

        // Get all the dotDieuTriList where theTreEm is null
        defaultDotDieuTriShouldNotBeFound("theTreEm.specified=false");
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByThongTuyenBhxhXml4210IsEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where thongTuyenBhxhXml4210 equals to DEFAULT_THONG_TUYEN_BHXH_XML_4210
        defaultDotDieuTriShouldBeFound("thongTuyenBhxhXml4210.equals=" + DEFAULT_THONG_TUYEN_BHXH_XML_4210);

        // Get all the dotDieuTriList where thongTuyenBhxhXml4210 equals to UPDATED_THONG_TUYEN_BHXH_XML_4210
        defaultDotDieuTriShouldNotBeFound("thongTuyenBhxhXml4210.equals=" + UPDATED_THONG_TUYEN_BHXH_XML_4210);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByThongTuyenBhxhXml4210IsNotEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where thongTuyenBhxhXml4210 not equals to DEFAULT_THONG_TUYEN_BHXH_XML_4210
        defaultDotDieuTriShouldNotBeFound("thongTuyenBhxhXml4210.notEquals=" + DEFAULT_THONG_TUYEN_BHXH_XML_4210);

        // Get all the dotDieuTriList where thongTuyenBhxhXml4210 not equals to UPDATED_THONG_TUYEN_BHXH_XML_4210
        defaultDotDieuTriShouldBeFound("thongTuyenBhxhXml4210.notEquals=" + UPDATED_THONG_TUYEN_BHXH_XML_4210);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByThongTuyenBhxhXml4210IsInShouldWork() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where thongTuyenBhxhXml4210 in DEFAULT_THONG_TUYEN_BHXH_XML_4210 or UPDATED_THONG_TUYEN_BHXH_XML_4210
        defaultDotDieuTriShouldBeFound("thongTuyenBhxhXml4210.in=" + DEFAULT_THONG_TUYEN_BHXH_XML_4210 + "," + UPDATED_THONG_TUYEN_BHXH_XML_4210);

        // Get all the dotDieuTriList where thongTuyenBhxhXml4210 equals to UPDATED_THONG_TUYEN_BHXH_XML_4210
        defaultDotDieuTriShouldNotBeFound("thongTuyenBhxhXml4210.in=" + UPDATED_THONG_TUYEN_BHXH_XML_4210);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByThongTuyenBhxhXml4210IsNullOrNotNull() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where thongTuyenBhxhXml4210 is not null
        defaultDotDieuTriShouldBeFound("thongTuyenBhxhXml4210.specified=true");

        // Get all the dotDieuTriList where thongTuyenBhxhXml4210 is null
        defaultDotDieuTriShouldNotBeFound("thongTuyenBhxhXml4210.specified=false");
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByTrangThaiIsEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where trangThai equals to DEFAULT_TRANG_THAI
        defaultDotDieuTriShouldBeFound("trangThai.equals=" + DEFAULT_TRANG_THAI);

        // Get all the dotDieuTriList where trangThai equals to UPDATED_TRANG_THAI
        defaultDotDieuTriShouldNotBeFound("trangThai.equals=" + UPDATED_TRANG_THAI);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByTrangThaiIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where trangThai not equals to DEFAULT_TRANG_THAI
        defaultDotDieuTriShouldNotBeFound("trangThai.notEquals=" + DEFAULT_TRANG_THAI);

        // Get all the dotDieuTriList where trangThai not equals to UPDATED_TRANG_THAI
        defaultDotDieuTriShouldBeFound("trangThai.notEquals=" + UPDATED_TRANG_THAI);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByTrangThaiIsInShouldWork() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where trangThai in DEFAULT_TRANG_THAI or UPDATED_TRANG_THAI
        defaultDotDieuTriShouldBeFound("trangThai.in=" + DEFAULT_TRANG_THAI + "," + UPDATED_TRANG_THAI);

        // Get all the dotDieuTriList where trangThai equals to UPDATED_TRANG_THAI
        defaultDotDieuTriShouldNotBeFound("trangThai.in=" + UPDATED_TRANG_THAI);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByTrangThaiIsNullOrNotNull() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where trangThai is not null
        defaultDotDieuTriShouldBeFound("trangThai.specified=true");

        // Get all the dotDieuTriList where trangThai is null
        defaultDotDieuTriShouldNotBeFound("trangThai.specified=false");
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByTrangThaiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where trangThai is greater than or equal to DEFAULT_TRANG_THAI
        defaultDotDieuTriShouldBeFound("trangThai.greaterThanOrEqual=" + DEFAULT_TRANG_THAI);

        // Get all the dotDieuTriList where trangThai is greater than or equal to UPDATED_TRANG_THAI
        defaultDotDieuTriShouldNotBeFound("trangThai.greaterThanOrEqual=" + UPDATED_TRANG_THAI);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByTrangThaiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where trangThai is less than or equal to DEFAULT_TRANG_THAI
        defaultDotDieuTriShouldBeFound("trangThai.lessThanOrEqual=" + DEFAULT_TRANG_THAI);

        // Get all the dotDieuTriList where trangThai is less than or equal to SMALLER_TRANG_THAI
        defaultDotDieuTriShouldNotBeFound("trangThai.lessThanOrEqual=" + SMALLER_TRANG_THAI);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByTrangThaiIsLessThanSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where trangThai is less than DEFAULT_TRANG_THAI
        defaultDotDieuTriShouldNotBeFound("trangThai.lessThan=" + DEFAULT_TRANG_THAI);

        // Get all the dotDieuTriList where trangThai is less than UPDATED_TRANG_THAI
        defaultDotDieuTriShouldBeFound("trangThai.lessThan=" + UPDATED_TRANG_THAI);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByTrangThaiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where trangThai is greater than DEFAULT_TRANG_THAI
        defaultDotDieuTriShouldNotBeFound("trangThai.greaterThan=" + DEFAULT_TRANG_THAI);

        // Get all the dotDieuTriList where trangThai is greater than SMALLER_TRANG_THAI
        defaultDotDieuTriShouldBeFound("trangThai.greaterThan=" + SMALLER_TRANG_THAI);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByLoaiIsEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where loai equals to DEFAULT_LOAI
        defaultDotDieuTriShouldBeFound("loai.equals=" + DEFAULT_LOAI);

        // Get all the dotDieuTriList where loai equals to UPDATED_LOAI
        defaultDotDieuTriShouldNotBeFound("loai.equals=" + UPDATED_LOAI);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByLoaiIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where loai not equals to DEFAULT_LOAI
        defaultDotDieuTriShouldNotBeFound("loai.notEquals=" + DEFAULT_LOAI);

        // Get all the dotDieuTriList where loai not equals to UPDATED_LOAI
        defaultDotDieuTriShouldBeFound("loai.notEquals=" + UPDATED_LOAI);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByLoaiIsInShouldWork() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where loai in DEFAULT_LOAI or UPDATED_LOAI
        defaultDotDieuTriShouldBeFound("loai.in=" + DEFAULT_LOAI + "," + UPDATED_LOAI);

        // Get all the dotDieuTriList where loai equals to UPDATED_LOAI
        defaultDotDieuTriShouldNotBeFound("loai.in=" + UPDATED_LOAI);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByLoaiIsNullOrNotNull() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where loai is not null
        defaultDotDieuTriShouldBeFound("loai.specified=true");

        // Get all the dotDieuTriList where loai is null
        defaultDotDieuTriShouldNotBeFound("loai.specified=false");
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByLoaiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where loai is greater than or equal to DEFAULT_LOAI
        defaultDotDieuTriShouldBeFound("loai.greaterThanOrEqual=" + DEFAULT_LOAI);

        // Get all the dotDieuTriList where loai is greater than or equal to UPDATED_LOAI
        defaultDotDieuTriShouldNotBeFound("loai.greaterThanOrEqual=" + UPDATED_LOAI);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByLoaiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where loai is less than or equal to DEFAULT_LOAI
        defaultDotDieuTriShouldBeFound("loai.lessThanOrEqual=" + DEFAULT_LOAI);

        // Get all the dotDieuTriList where loai is less than or equal to SMALLER_LOAI
        defaultDotDieuTriShouldNotBeFound("loai.lessThanOrEqual=" + SMALLER_LOAI);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByLoaiIsLessThanSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where loai is less than DEFAULT_LOAI
        defaultDotDieuTriShouldNotBeFound("loai.lessThan=" + DEFAULT_LOAI);

        // Get all the dotDieuTriList where loai is less than UPDATED_LOAI
        defaultDotDieuTriShouldBeFound("loai.lessThan=" + UPDATED_LOAI);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByLoaiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where loai is greater than DEFAULT_LOAI
        defaultDotDieuTriShouldNotBeFound("loai.greaterThan=" + DEFAULT_LOAI);

        // Get all the dotDieuTriList where loai is greater than SMALLER_LOAI
        defaultDotDieuTriShouldBeFound("loai.greaterThan=" + SMALLER_LOAI);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytNoiDkKcbbdIsEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytNoiDkKcbbd equals to DEFAULT_BHYT_NOI_DK_KCBBD
        defaultDotDieuTriShouldBeFound("bhytNoiDkKcbbd.equals=" + DEFAULT_BHYT_NOI_DK_KCBBD);

        // Get all the dotDieuTriList where bhytNoiDkKcbbd equals to UPDATED_BHYT_NOI_DK_KCBBD
        defaultDotDieuTriShouldNotBeFound("bhytNoiDkKcbbd.equals=" + UPDATED_BHYT_NOI_DK_KCBBD);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytNoiDkKcbbdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytNoiDkKcbbd not equals to DEFAULT_BHYT_NOI_DK_KCBBD
        defaultDotDieuTriShouldNotBeFound("bhytNoiDkKcbbd.notEquals=" + DEFAULT_BHYT_NOI_DK_KCBBD);

        // Get all the dotDieuTriList where bhytNoiDkKcbbd not equals to UPDATED_BHYT_NOI_DK_KCBBD
        defaultDotDieuTriShouldBeFound("bhytNoiDkKcbbd.notEquals=" + UPDATED_BHYT_NOI_DK_KCBBD);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytNoiDkKcbbdIsInShouldWork() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytNoiDkKcbbd in DEFAULT_BHYT_NOI_DK_KCBBD or UPDATED_BHYT_NOI_DK_KCBBD
        defaultDotDieuTriShouldBeFound("bhytNoiDkKcbbd.in=" + DEFAULT_BHYT_NOI_DK_KCBBD + "," + UPDATED_BHYT_NOI_DK_KCBBD);

        // Get all the dotDieuTriList where bhytNoiDkKcbbd equals to UPDATED_BHYT_NOI_DK_KCBBD
        defaultDotDieuTriShouldNotBeFound("bhytNoiDkKcbbd.in=" + UPDATED_BHYT_NOI_DK_KCBBD);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytNoiDkKcbbdIsNullOrNotNull() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytNoiDkKcbbd is not null
        defaultDotDieuTriShouldBeFound("bhytNoiDkKcbbd.specified=true");

        // Get all the dotDieuTriList where bhytNoiDkKcbbd is null
        defaultDotDieuTriShouldNotBeFound("bhytNoiDkKcbbd.specified=false");
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytNoiDkKcbbdContainsSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytNoiDkKcbbd contains DEFAULT_BHYT_NOI_DK_KCBBD
        defaultDotDieuTriShouldBeFound("bhytNoiDkKcbbd.contains=" + DEFAULT_BHYT_NOI_DK_KCBBD);

        // Get all the dotDieuTriList where bhytNoiDkKcbbd contains UPDATED_BHYT_NOI_DK_KCBBD
        defaultDotDieuTriShouldNotBeFound("bhytNoiDkKcbbd.contains=" + UPDATED_BHYT_NOI_DK_KCBBD);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBhytNoiDkKcbbdNotContainsSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where bhytNoiDkKcbbd does not contain DEFAULT_BHYT_NOI_DK_KCBBD
        defaultDotDieuTriShouldNotBeFound("bhytNoiDkKcbbd.doesNotContain=" + DEFAULT_BHYT_NOI_DK_KCBBD);

        // Get all the dotDieuTriList where bhytNoiDkKcbbd does not contain UPDATED_BHYT_NOI_DK_KCBBD
        defaultDotDieuTriShouldBeFound("bhytNoiDkKcbbd.doesNotContain=" + UPDATED_BHYT_NOI_DK_KCBBD);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByNamIsEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where nam equals to DEFAULT_NAM
        defaultDotDieuTriShouldBeFound("nam.equals=" + DEFAULT_NAM);

        // Get all the dotDieuTriList where nam equals to UPDATED_NAM
        defaultDotDieuTriShouldNotBeFound("nam.equals=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByNamIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where nam not equals to DEFAULT_NAM
        defaultDotDieuTriShouldNotBeFound("nam.notEquals=" + DEFAULT_NAM);

        // Get all the dotDieuTriList where nam not equals to UPDATED_NAM
        defaultDotDieuTriShouldBeFound("nam.notEquals=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByNamIsInShouldWork() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where nam in DEFAULT_NAM or UPDATED_NAM
        defaultDotDieuTriShouldBeFound("nam.in=" + DEFAULT_NAM + "," + UPDATED_NAM);

        // Get all the dotDieuTriList where nam equals to UPDATED_NAM
        defaultDotDieuTriShouldNotBeFound("nam.in=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByNamIsNullOrNotNull() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where nam is not null
        defaultDotDieuTriShouldBeFound("nam.specified=true");

        // Get all the dotDieuTriList where nam is null
        defaultDotDieuTriShouldNotBeFound("nam.specified=false");
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByNamIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where nam is greater than or equal to DEFAULT_NAM
        defaultDotDieuTriShouldBeFound("nam.greaterThanOrEqual=" + DEFAULT_NAM);

        // Get all the dotDieuTriList where nam is greater than or equal to UPDATED_NAM
        defaultDotDieuTriShouldNotBeFound("nam.greaterThanOrEqual=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByNamIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where nam is less than or equal to DEFAULT_NAM
        defaultDotDieuTriShouldBeFound("nam.lessThanOrEqual=" + DEFAULT_NAM);

        // Get all the dotDieuTriList where nam is less than or equal to SMALLER_NAM
        defaultDotDieuTriShouldNotBeFound("nam.lessThanOrEqual=" + SMALLER_NAM);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByNamIsLessThanSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where nam is less than DEFAULT_NAM
        defaultDotDieuTriShouldNotBeFound("nam.lessThan=" + DEFAULT_NAM);

        // Get all the dotDieuTriList where nam is less than UPDATED_NAM
        defaultDotDieuTriShouldBeFound("nam.lessThan=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByNamIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        // Get all the dotDieuTriList where nam is greater than DEFAULT_NAM
        defaultDotDieuTriShouldNotBeFound("nam.greaterThan=" + DEFAULT_NAM);

        // Get all the dotDieuTriList where nam is greater than SMALLER_NAM
        defaultDotDieuTriShouldBeFound("nam.greaterThan=" + SMALLER_NAM);
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBakbBenhNhanIdIsEqualToSomething() throws Exception {
        // Get already existing entity
        BenhAnKhamBenh bakb = dotDieuTri.getBakb();
        dotDieuTriRepository.saveAndFlush(dotDieuTri);
        Long bakbBenhNhanId = bakb.getId().getBenhNhanId();

        // Get all the dotDieuTriList where bakbBenhNhanId equals to bakbBenhNhanId
        defaultDotDieuTriShouldBeFound("bakbBenhNhanId.equals=" + bakbBenhNhanId);

        // Get all the dotDieuTriList where bakbBenhNhanId equals to a different bakbBenhNhanId
        defaultDotDieuTriShouldNotBeFound("bakbBenhNhanId.equals=" + BenhAnKhamBenhResourceIT.createUpdatedEntity(em).getId().getBenhNhanId());
    }

    @Test
    @Transactional
    public void getAllDotDieuTrisByBakbDonViIdIsEqualToSomething() throws Exception {
        // Get already existing entity
        BenhAnKhamBenh bakb = dotDieuTri.getBakb();
        dotDieuTriRepository.saveAndFlush(dotDieuTri);
        Long bakbDonViId = bakb.getId().getDonViId();

        // Get all the dotDieuTriList where bakbDonViId equals to bakbDonViId
        defaultDotDieuTriShouldBeFound("bakbDonViId.equals=" + bakbDonViId);

        // Get all the dotDieuTriList where bakbDonViId equals to a different bakbDonViId
        defaultDotDieuTriShouldNotBeFound("bakbDonViId.equals=" + BenhAnKhamBenhResourceIT.createUpdatedEntity(em).getId().getDonViId());
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultDotDieuTriShouldBeFound(String filter) throws Exception {
        restDotDieuTriMockMvc.perform(get("/api/dot-dieu-tris?" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].soThuTu").value(hasItem(DEFAULT_SO_THU_TU)))
            .andExpect(jsonPath("$.[*].bhytCanTren").value(hasItem(DEFAULT_BHYT_CAN_TREN)))
            .andExpect(jsonPath("$.[*].bhytCanTrenKtc").value(hasItem(DEFAULT_BHYT_CAN_TREN_KTC)))
            .andExpect(jsonPath("$.[*].bhytMaKhuVuc").value(hasItem(DEFAULT_BHYT_MA_KHU_VUC)))
            .andExpect(jsonPath("$.[*].bhytNgayBatDau").value(hasItem(DEFAULT_BHYT_NGAY_BAT_DAU.toString())))
            .andExpect(jsonPath("$.[*].bhytNgayDuNamNam").value(hasItem(DEFAULT_BHYT_NGAY_DU_NAM_NAM.toString())))
            .andExpect(jsonPath("$.[*].bhytNgayHetHan").value(hasItem(DEFAULT_BHYT_NGAY_HET_HAN.toString())))
            .andExpect(jsonPath("$.[*].bhytNoiTinh").value(hasItem(DEFAULT_BHYT_NOI_TINH.booleanValue())))
            .andExpect(jsonPath("$.[*].bhytSoThe").value(hasItem(DEFAULT_BHYT_SO_THE)))
            .andExpect(jsonPath("$.[*].bhytTyLeMienGiam").value(hasItem(DEFAULT_BHYT_TY_LE_MIEN_GIAM)))
            .andExpect(jsonPath("$.[*].bhytTyLeMienGiamKtc").value(hasItem(DEFAULT_BHYT_TY_LE_MIEN_GIAM_KTC)))
            .andExpect(jsonPath("$.[*].coBaoHiem").value(hasItem(DEFAULT_CO_BAO_HIEM.booleanValue())))
            .andExpect(jsonPath("$.[*].bhytDiaChi").value(hasItem(DEFAULT_BHYT_DIA_CHI)))
            .andExpect(jsonPath("$.[*].doiTuongBhytTen").value(hasItem(DEFAULT_DOI_TUONG_BHYT_TEN)))
            .andExpect(jsonPath("$.[*].dungTuyen").value(hasItem(DEFAULT_DUNG_TUYEN.booleanValue())))
            .andExpect(jsonPath("$.[*].giayToTreEm").value(hasItem(DEFAULT_GIAY_TO_TRE_EM)))
            .andExpect(jsonPath("$.[*].loaiGiayToTreEm").value(hasItem(DEFAULT_LOAI_GIAY_TO_TRE_EM)))
            .andExpect(jsonPath("$.[*].ngayMienCungChiTra").value(hasItem(DEFAULT_NGAY_MIEN_CUNG_CHI_TRA.toString())))
            .andExpect(jsonPath("$.[*].theTreEm").value(hasItem(DEFAULT_THE_TRE_EM.booleanValue())))
            .andExpect(jsonPath("$.[*].thongTuyenBhxhXml4210").value(hasItem(DEFAULT_THONG_TUYEN_BHXH_XML_4210.booleanValue())))
            .andExpect(jsonPath("$.[*].trangThai").value(hasItem(DEFAULT_TRANG_THAI)))
            .andExpect(jsonPath("$.[*].loai").value(hasItem(DEFAULT_LOAI)))
            .andExpect(jsonPath("$.[*].bhytNoiDkKcbbd").value(hasItem(DEFAULT_BHYT_NOI_DK_KCBBD)))
            .andExpect(jsonPath("$.[*].nam").value(hasItem(DEFAULT_NAM)));

        // Check, that the count call also returns 1
        restDotDieuTriMockMvc.perform(get("/api/dot-dieu-tris/count?" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultDotDieuTriShouldNotBeFound(String filter) throws Exception {
        restDotDieuTriMockMvc.perform(get("/api/dot-dieu-tris?" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restDotDieuTriMockMvc.perform(get("/api/dot-dieu-tris/count?" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingDotDieuTri() throws Exception {
        // Get the dotDieuTri
        DotDieuTri dotDieuTri = createUpdatedEntity(em);
        restDotDieuTriMockMvc.perform(get("/api/dot-dieu-tris/{id}", "bakbBenhNhanId=" + dotDieuTri.getId().getBakbBenhNhanId() + ";" + "bakbDonViId=" + dotDieuTri.getId().getBakbDonViId()))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDotDieuTri() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        int databaseSizeBeforeUpdate = dotDieuTriRepository.findAll().size();

        // Update the dotDieuTri
        DotDieuTri updatedDotDieuTri = dotDieuTriRepository.findById(dotDieuTri.getId()).get();
        // Disconnect from session so that the updates on updatedDotDieuTri are not directly saved in db
        em.detach(updatedDotDieuTri);
        updatedDotDieuTri
            .soThuTu(UPDATED_SO_THU_TU)
            .bhytCanTren(UPDATED_BHYT_CAN_TREN)
            .bhytCanTrenKtc(UPDATED_BHYT_CAN_TREN_KTC)
            .bhytMaKhuVuc(UPDATED_BHYT_MA_KHU_VUC)
            .bhytNgayBatDau(UPDATED_BHYT_NGAY_BAT_DAU)
            .bhytNgayDuNamNam(UPDATED_BHYT_NGAY_DU_NAM_NAM)
            .bhytNgayHetHan(UPDATED_BHYT_NGAY_HET_HAN)
            .bhytNoiTinh(UPDATED_BHYT_NOI_TINH)
            .bhytSoThe(UPDATED_BHYT_SO_THE)
            .bhytTyLeMienGiam(UPDATED_BHYT_TY_LE_MIEN_GIAM)
            .bhytTyLeMienGiamKtc(UPDATED_BHYT_TY_LE_MIEN_GIAM_KTC)
            .coBaoHiem(UPDATED_CO_BAO_HIEM)
            .bhytDiaChi(UPDATED_BHYT_DIA_CHI)
            .doiTuongBhytTen(UPDATED_DOI_TUONG_BHYT_TEN)
            .dungTuyen(UPDATED_DUNG_TUYEN)
            .giayToTreEm(UPDATED_GIAY_TO_TRE_EM)
            .loaiGiayToTreEm(UPDATED_LOAI_GIAY_TO_TRE_EM)
            .ngayMienCungChiTra(UPDATED_NGAY_MIEN_CUNG_CHI_TRA)
            .theTreEm(UPDATED_THE_TRE_EM)
            .thongTuyenBhxhXml4210(UPDATED_THONG_TUYEN_BHXH_XML_4210)
            .trangThai(UPDATED_TRANG_THAI)
            .loai(UPDATED_LOAI)
            .bhytNoiDkKcbbd(UPDATED_BHYT_NOI_DK_KCBBD)
            .nam(UPDATED_NAM);
        DotDieuTriDTO dotDieuTriDTO = dotDieuTriMapper.toDto(updatedDotDieuTri);

        restDotDieuTriMockMvc.perform(put("/api/dot-dieu-tris").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dotDieuTriDTO)))
            .andExpect(status().isOk());

        // Validate the DotDieuTri in the database
        List<DotDieuTri> dotDieuTriList = dotDieuTriRepository.findAll();
        assertThat(dotDieuTriList).hasSize(databaseSizeBeforeUpdate);
        DotDieuTri testDotDieuTri = dotDieuTriList.get(dotDieuTriList.size() - 1);
        assertThat(testDotDieuTri.getSoThuTu()).isEqualTo(UPDATED_SO_THU_TU);
        assertThat(testDotDieuTri.getBhytCanTren()).isEqualTo(UPDATED_BHYT_CAN_TREN);
        assertThat(testDotDieuTri.getBhytCanTrenKtc()).isEqualTo(UPDATED_BHYT_CAN_TREN_KTC);
        assertThat(testDotDieuTri.getBhytMaKhuVuc()).isEqualTo(UPDATED_BHYT_MA_KHU_VUC);
        assertThat(testDotDieuTri.getBhytNgayBatDau()).isEqualTo(UPDATED_BHYT_NGAY_BAT_DAU);
        assertThat(testDotDieuTri.getBhytNgayDuNamNam()).isEqualTo(UPDATED_BHYT_NGAY_DU_NAM_NAM);
        assertThat(testDotDieuTri.getBhytNgayHetHan()).isEqualTo(UPDATED_BHYT_NGAY_HET_HAN);
        assertThat(testDotDieuTri.isBhytNoiTinh()).isEqualTo(UPDATED_BHYT_NOI_TINH);
        assertThat(testDotDieuTri.getBhytSoThe()).isEqualTo(UPDATED_BHYT_SO_THE);
        assertThat(testDotDieuTri.getBhytTyLeMienGiam()).isEqualTo(UPDATED_BHYT_TY_LE_MIEN_GIAM);
        assertThat(testDotDieuTri.getBhytTyLeMienGiamKtc()).isEqualTo(UPDATED_BHYT_TY_LE_MIEN_GIAM_KTC);
        assertThat(testDotDieuTri.isCoBaoHiem()).isEqualTo(UPDATED_CO_BAO_HIEM);
        assertThat(testDotDieuTri.getBhytDiaChi()).isEqualTo(UPDATED_BHYT_DIA_CHI);
        assertThat(testDotDieuTri.getDoiTuongBhytTen()).isEqualTo(UPDATED_DOI_TUONG_BHYT_TEN);
        assertThat(testDotDieuTri.isDungTuyen()).isEqualTo(UPDATED_DUNG_TUYEN);
        assertThat(testDotDieuTri.getGiayToTreEm()).isEqualTo(UPDATED_GIAY_TO_TRE_EM);
        assertThat(testDotDieuTri.getLoaiGiayToTreEm()).isEqualTo(UPDATED_LOAI_GIAY_TO_TRE_EM);
        assertThat(testDotDieuTri.getNgayMienCungChiTra()).isEqualTo(UPDATED_NGAY_MIEN_CUNG_CHI_TRA);
        assertThat(testDotDieuTri.isTheTreEm()).isEqualTo(UPDATED_THE_TRE_EM);
        assertThat(testDotDieuTri.isThongTuyenBhxhXml4210()).isEqualTo(UPDATED_THONG_TUYEN_BHXH_XML_4210);
        assertThat(testDotDieuTri.getTrangThai()).isEqualTo(UPDATED_TRANG_THAI);
        assertThat(testDotDieuTri.getLoai()).isEqualTo(UPDATED_LOAI);
        assertThat(testDotDieuTri.getBhytNoiDkKcbbd()).isEqualTo(UPDATED_BHYT_NOI_DK_KCBBD);
        assertThat(testDotDieuTri.getNam()).isEqualTo(UPDATED_NAM);
    }

    @Test
    @Transactional
    public void updateNonExistingDotDieuTri() throws Exception {
        int databaseSizeBeforeUpdate = dotDieuTriRepository.findAll().size();

        // Create the DotDieuTri
        DotDieuTriDTO dotDieuTriDTO = dotDieuTriMapper.toDto(dotDieuTri);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDotDieuTriMockMvc.perform(put("/api/dot-dieu-tris").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dotDieuTriDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DotDieuTri in the database
        List<DotDieuTri> dotDieuTriList = dotDieuTriRepository.findAll();
        assertThat(dotDieuTriList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDotDieuTri() throws Exception {
        // Initialize the database
        dotDieuTriRepository.saveAndFlush(dotDieuTri);

        int databaseSizeBeforeDelete = dotDieuTriRepository.findAll().size();

        // Delete the dotDieuTri
        restDotDieuTriMockMvc.perform(delete("/api/dot-dieu-tris/{id}", "bakbBenhNhanId=" + dotDieuTri.getId().getBakbBenhNhanId() + ";" + "bakbDonViId=" + dotDieuTri.getId().getBakbDonViId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DotDieuTri> dotDieuTriList = dotDieuTriRepository.findAll();
        assertThat(dotDieuTriList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.ChucDanh;
import vn.vnpt.repository.ChucDanhRepository;
import vn.vnpt.service.ChucDanhService;
import vn.vnpt.service.dto.ChucDanhDTO;
import vn.vnpt.service.mapper.ChucDanhMapper;
import vn.vnpt.service.dto.ChucDanhCriteria;
import vn.vnpt.service.ChucDanhQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ChucDanhResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class ChucDanhResourceIT {

    private static final String DEFAULT_MO_TA = "AAAAAAAAAA";
    private static final String UPDATED_MO_TA = "BBBBBBBBBB";

    private static final String DEFAULT_TEN = "AAAAAAAAAA";
    private static final String UPDATED_TEN = "BBBBBBBBBB";

    @Autowired
    private ChucDanhRepository chucDanhRepository;

    @Autowired
    private ChucDanhMapper chucDanhMapper;

    @Autowired
    private ChucDanhService chucDanhService;

    @Autowired
    private ChucDanhQueryService chucDanhQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restChucDanhMockMvc;

    private ChucDanh chucDanh;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ChucDanh createEntity(EntityManager em) {
        ChucDanh chucDanh = new ChucDanh()
            .moTa(DEFAULT_MO_TA)
            .ten(DEFAULT_TEN);
        return chucDanh;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ChucDanh createUpdatedEntity(EntityManager em) {
        ChucDanh chucDanh = new ChucDanh()
            .moTa(UPDATED_MO_TA)
            .ten(UPDATED_TEN);
        return chucDanh;
    }

    @BeforeEach
    public void initTest() {
        chucDanh = createEntity(em);
    }

    @Test
    @Transactional
    public void createChucDanh() throws Exception {
        int databaseSizeBeforeCreate = chucDanhRepository.findAll().size();

        // Create the ChucDanh
        ChucDanhDTO chucDanhDTO = chucDanhMapper.toDto(chucDanh);
        restChucDanhMockMvc.perform(post("/api/chuc-danhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chucDanhDTO)))
            .andExpect(status().isCreated());

        // Validate the ChucDanh in the database
        List<ChucDanh> chucDanhList = chucDanhRepository.findAll();
        assertThat(chucDanhList).hasSize(databaseSizeBeforeCreate + 1);
        ChucDanh testChucDanh = chucDanhList.get(chucDanhList.size() - 1);
        assertThat(testChucDanh.getMoTa()).isEqualTo(DEFAULT_MO_TA);
        assertThat(testChucDanh.getTen()).isEqualTo(DEFAULT_TEN);
    }

    @Test
    @Transactional
    public void createChucDanhWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = chucDanhRepository.findAll().size();

        // Create the ChucDanh with an existing ID
        chucDanh.setId(1L);
        ChucDanhDTO chucDanhDTO = chucDanhMapper.toDto(chucDanh);

        // An entity with an existing ID cannot be created, so this API call must fail
        restChucDanhMockMvc.perform(post("/api/chuc-danhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chucDanhDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ChucDanh in the database
        List<ChucDanh> chucDanhList = chucDanhRepository.findAll();
        assertThat(chucDanhList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkTenIsRequired() throws Exception {
        int databaseSizeBeforeTest = chucDanhRepository.findAll().size();
        // set the field null
        chucDanh.setTen(null);

        // Create the ChucDanh, which fails.
        ChucDanhDTO chucDanhDTO = chucDanhMapper.toDto(chucDanh);

        restChucDanhMockMvc.perform(post("/api/chuc-danhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chucDanhDTO)))
            .andExpect(status().isBadRequest());

        List<ChucDanh> chucDanhList = chucDanhRepository.findAll();
        assertThat(chucDanhList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllChucDanhs() throws Exception {
        // Initialize the database
        chucDanhRepository.saveAndFlush(chucDanh);

        // Get all the chucDanhList
        restChucDanhMockMvc.perform(get("/api/chuc-danhs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(chucDanh.getId().intValue())))
            .andExpect(jsonPath("$.[*].moTa").value(hasItem(DEFAULT_MO_TA)))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)));
    }
    
    @Test
    @Transactional
    public void getChucDanh() throws Exception {
        // Initialize the database
        chucDanhRepository.saveAndFlush(chucDanh);

        // Get the chucDanh
        restChucDanhMockMvc.perform(get("/api/chuc-danhs/{id}", chucDanh.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(chucDanh.getId().intValue()))
            .andExpect(jsonPath("$.moTa").value(DEFAULT_MO_TA))
            .andExpect(jsonPath("$.ten").value(DEFAULT_TEN));
    }


    @Test
    @Transactional
    public void getChucDanhsByIdFiltering() throws Exception {
        // Initialize the database
        chucDanhRepository.saveAndFlush(chucDanh);

        Long id = chucDanh.getId();

        defaultChucDanhShouldBeFound("id.equals=" + id);
        defaultChucDanhShouldNotBeFound("id.notEquals=" + id);

        defaultChucDanhShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultChucDanhShouldNotBeFound("id.greaterThan=" + id);

        defaultChucDanhShouldBeFound("id.lessThanOrEqual=" + id);
        defaultChucDanhShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllChucDanhsByMoTaIsEqualToSomething() throws Exception {
        // Initialize the database
        chucDanhRepository.saveAndFlush(chucDanh);

        // Get all the chucDanhList where moTa equals to DEFAULT_MO_TA
        defaultChucDanhShouldBeFound("moTa.equals=" + DEFAULT_MO_TA);

        // Get all the chucDanhList where moTa equals to UPDATED_MO_TA
        defaultChucDanhShouldNotBeFound("moTa.equals=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    public void getAllChucDanhsByMoTaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chucDanhRepository.saveAndFlush(chucDanh);

        // Get all the chucDanhList where moTa not equals to DEFAULT_MO_TA
        defaultChucDanhShouldNotBeFound("moTa.notEquals=" + DEFAULT_MO_TA);

        // Get all the chucDanhList where moTa not equals to UPDATED_MO_TA
        defaultChucDanhShouldBeFound("moTa.notEquals=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    public void getAllChucDanhsByMoTaIsInShouldWork() throws Exception {
        // Initialize the database
        chucDanhRepository.saveAndFlush(chucDanh);

        // Get all the chucDanhList where moTa in DEFAULT_MO_TA or UPDATED_MO_TA
        defaultChucDanhShouldBeFound("moTa.in=" + DEFAULT_MO_TA + "," + UPDATED_MO_TA);

        // Get all the chucDanhList where moTa equals to UPDATED_MO_TA
        defaultChucDanhShouldNotBeFound("moTa.in=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    public void getAllChucDanhsByMoTaIsNullOrNotNull() throws Exception {
        // Initialize the database
        chucDanhRepository.saveAndFlush(chucDanh);

        // Get all the chucDanhList where moTa is not null
        defaultChucDanhShouldBeFound("moTa.specified=true");

        // Get all the chucDanhList where moTa is null
        defaultChucDanhShouldNotBeFound("moTa.specified=false");
    }
                @Test
    @Transactional
    public void getAllChucDanhsByMoTaContainsSomething() throws Exception {
        // Initialize the database
        chucDanhRepository.saveAndFlush(chucDanh);

        // Get all the chucDanhList where moTa contains DEFAULT_MO_TA
        defaultChucDanhShouldBeFound("moTa.contains=" + DEFAULT_MO_TA);

        // Get all the chucDanhList where moTa contains UPDATED_MO_TA
        defaultChucDanhShouldNotBeFound("moTa.contains=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    public void getAllChucDanhsByMoTaNotContainsSomething() throws Exception {
        // Initialize the database
        chucDanhRepository.saveAndFlush(chucDanh);

        // Get all the chucDanhList where moTa does not contain DEFAULT_MO_TA
        defaultChucDanhShouldNotBeFound("moTa.doesNotContain=" + DEFAULT_MO_TA);

        // Get all the chucDanhList where moTa does not contain UPDATED_MO_TA
        defaultChucDanhShouldBeFound("moTa.doesNotContain=" + UPDATED_MO_TA);
    }


    @Test
    @Transactional
    public void getAllChucDanhsByTenIsEqualToSomething() throws Exception {
        // Initialize the database
        chucDanhRepository.saveAndFlush(chucDanh);

        // Get all the chucDanhList where ten equals to DEFAULT_TEN
        defaultChucDanhShouldBeFound("ten.equals=" + DEFAULT_TEN);

        // Get all the chucDanhList where ten equals to UPDATED_TEN
        defaultChucDanhShouldNotBeFound("ten.equals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllChucDanhsByTenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chucDanhRepository.saveAndFlush(chucDanh);

        // Get all the chucDanhList where ten not equals to DEFAULT_TEN
        defaultChucDanhShouldNotBeFound("ten.notEquals=" + DEFAULT_TEN);

        // Get all the chucDanhList where ten not equals to UPDATED_TEN
        defaultChucDanhShouldBeFound("ten.notEquals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllChucDanhsByTenIsInShouldWork() throws Exception {
        // Initialize the database
        chucDanhRepository.saveAndFlush(chucDanh);

        // Get all the chucDanhList where ten in DEFAULT_TEN or UPDATED_TEN
        defaultChucDanhShouldBeFound("ten.in=" + DEFAULT_TEN + "," + UPDATED_TEN);

        // Get all the chucDanhList where ten equals to UPDATED_TEN
        defaultChucDanhShouldNotBeFound("ten.in=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllChucDanhsByTenIsNullOrNotNull() throws Exception {
        // Initialize the database
        chucDanhRepository.saveAndFlush(chucDanh);

        // Get all the chucDanhList where ten is not null
        defaultChucDanhShouldBeFound("ten.specified=true");

        // Get all the chucDanhList where ten is null
        defaultChucDanhShouldNotBeFound("ten.specified=false");
    }
                @Test
    @Transactional
    public void getAllChucDanhsByTenContainsSomething() throws Exception {
        // Initialize the database
        chucDanhRepository.saveAndFlush(chucDanh);

        // Get all the chucDanhList where ten contains DEFAULT_TEN
        defaultChucDanhShouldBeFound("ten.contains=" + DEFAULT_TEN);

        // Get all the chucDanhList where ten contains UPDATED_TEN
        defaultChucDanhShouldNotBeFound("ten.contains=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllChucDanhsByTenNotContainsSomething() throws Exception {
        // Initialize the database
        chucDanhRepository.saveAndFlush(chucDanh);

        // Get all the chucDanhList where ten does not contain DEFAULT_TEN
        defaultChucDanhShouldNotBeFound("ten.doesNotContain=" + DEFAULT_TEN);

        // Get all the chucDanhList where ten does not contain UPDATED_TEN
        defaultChucDanhShouldBeFound("ten.doesNotContain=" + UPDATED_TEN);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultChucDanhShouldBeFound(String filter) throws Exception {
        restChucDanhMockMvc.perform(get("/api/chuc-danhs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(chucDanh.getId().intValue())))
            .andExpect(jsonPath("$.[*].moTa").value(hasItem(DEFAULT_MO_TA)))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)));

        // Check, that the count call also returns 1
        restChucDanhMockMvc.perform(get("/api/chuc-danhs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultChucDanhShouldNotBeFound(String filter) throws Exception {
        restChucDanhMockMvc.perform(get("/api/chuc-danhs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restChucDanhMockMvc.perform(get("/api/chuc-danhs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingChucDanh() throws Exception {
        // Get the chucDanh
        restChucDanhMockMvc.perform(get("/api/chuc-danhs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateChucDanh() throws Exception {
        // Initialize the database
        chucDanhRepository.saveAndFlush(chucDanh);

        int databaseSizeBeforeUpdate = chucDanhRepository.findAll().size();

        // Update the chucDanh
        ChucDanh updatedChucDanh = chucDanhRepository.findById(chucDanh.getId()).get();
        // Disconnect from session so that the updates on updatedChucDanh are not directly saved in db
        em.detach(updatedChucDanh);
        updatedChucDanh
            .moTa(UPDATED_MO_TA)
            .ten(UPDATED_TEN);
        ChucDanhDTO chucDanhDTO = chucDanhMapper.toDto(updatedChucDanh);

        restChucDanhMockMvc.perform(put("/api/chuc-danhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chucDanhDTO)))
            .andExpect(status().isOk());

        // Validate the ChucDanh in the database
        List<ChucDanh> chucDanhList = chucDanhRepository.findAll();
        assertThat(chucDanhList).hasSize(databaseSizeBeforeUpdate);
        ChucDanh testChucDanh = chucDanhList.get(chucDanhList.size() - 1);
        assertThat(testChucDanh.getMoTa()).isEqualTo(UPDATED_MO_TA);
        assertThat(testChucDanh.getTen()).isEqualTo(UPDATED_TEN);
    }

    @Test
    @Transactional
    public void updateNonExistingChucDanh() throws Exception {
        int databaseSizeBeforeUpdate = chucDanhRepository.findAll().size();

        // Create the ChucDanh
        ChucDanhDTO chucDanhDTO = chucDanhMapper.toDto(chucDanh);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restChucDanhMockMvc.perform(put("/api/chuc-danhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chucDanhDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ChucDanh in the database
        List<ChucDanh> chucDanhList = chucDanhRepository.findAll();
        assertThat(chucDanhList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteChucDanh() throws Exception {
        // Initialize the database
        chucDanhRepository.saveAndFlush(chucDanh);

        int databaseSizeBeforeDelete = chucDanhRepository.findAll().size();

        // Delete the chucDanh
        restChucDanhMockMvc.perform(delete("/api/chuc-danhs/{id}", chucDanh.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ChucDanh> chucDanhList = chucDanhRepository.findAll();
        assertThat(chucDanhList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.LoaiXetNghiem;
import vn.vnpt.domain.DonVi;
import vn.vnpt.repository.LoaiXetNghiemRepository;
import vn.vnpt.service.LoaiXetNghiemService;
import vn.vnpt.service.dto.LoaiXetNghiemDTO;
import vn.vnpt.service.mapper.LoaiXetNghiemMapper;
import vn.vnpt.service.dto.LoaiXetNghiemCriteria;
import vn.vnpt.service.LoaiXetNghiemQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link LoaiXetNghiemResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class LoaiXetNghiemResourceIT {

    private static final String DEFAULT_TEN = "AAAAAAAAAA";
    private static final String UPDATED_TEN = "BBBBBBBBBB";

    private static final String DEFAULT_MO_TA = "AAAAAAAAAA";
    private static final String UPDATED_MO_TA = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ENABLE = false;
    private static final Boolean UPDATED_ENABLE = true;

    private static final Integer DEFAULT_UU_TIEN = 1;
    private static final Integer UPDATED_UU_TIEN = 2;
    private static final Integer SMALLER_UU_TIEN = 1 - 1;

    private static final String DEFAULT_MA_PHAN_LOAI = "AAAAAAAAAA";
    private static final String UPDATED_MA_PHAN_LOAI = "BBBBBBBBBB";

    @Autowired
    private LoaiXetNghiemRepository loaiXetNghiemRepository;

    @Autowired
    private LoaiXetNghiemMapper loaiXetNghiemMapper;

    @Autowired
    private LoaiXetNghiemService loaiXetNghiemService;

    @Autowired
    private LoaiXetNghiemQueryService loaiXetNghiemQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restLoaiXetNghiemMockMvc;

    private LoaiXetNghiem loaiXetNghiem;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LoaiXetNghiem createEntity(EntityManager em) {
        LoaiXetNghiem loaiXetNghiem = new LoaiXetNghiem()
            .ten(DEFAULT_TEN)
            .moTa(DEFAULT_MO_TA)
            .enable(DEFAULT_ENABLE)
            .uuTien(DEFAULT_UU_TIEN)
            .maPhanLoai(DEFAULT_MA_PHAN_LOAI);
        // Add required entity
        DonVi donVi;
        if (TestUtil.findAll(em, DonVi.class).isEmpty()) {
            donVi = DonViResourceIT.createEntity(em);
            em.persist(donVi);
            em.flush();
        } else {
            donVi = TestUtil.findAll(em, DonVi.class).get(0);
        }
        loaiXetNghiem.setDonVi(donVi);
        return loaiXetNghiem;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LoaiXetNghiem createUpdatedEntity(EntityManager em) {
        LoaiXetNghiem loaiXetNghiem = new LoaiXetNghiem()
            .ten(UPDATED_TEN)
            .moTa(UPDATED_MO_TA)
            .enable(UPDATED_ENABLE)
            .uuTien(UPDATED_UU_TIEN)
            .maPhanLoai(UPDATED_MA_PHAN_LOAI);
        // Add required entity
        DonVi donVi;
        if (TestUtil.findAll(em, DonVi.class).isEmpty()) {
            donVi = DonViResourceIT.createUpdatedEntity(em);
            em.persist(donVi);
            em.flush();
        } else {
            donVi = TestUtil.findAll(em, DonVi.class).get(0);
        }
        loaiXetNghiem.setDonVi(donVi);
        return loaiXetNghiem;
    }

    @BeforeEach
    public void initTest() {
        loaiXetNghiem = createEntity(em);
    }

    @Test
    @Transactional
    public void createLoaiXetNghiem() throws Exception {
        int databaseSizeBeforeCreate = loaiXetNghiemRepository.findAll().size();

        // Create the LoaiXetNghiem
        LoaiXetNghiemDTO loaiXetNghiemDTO = loaiXetNghiemMapper.toDto(loaiXetNghiem);
        restLoaiXetNghiemMockMvc.perform(post("/api/loai-xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(loaiXetNghiemDTO)))
            .andExpect(status().isCreated());

        // Validate the LoaiXetNghiem in the database
        List<LoaiXetNghiem> loaiXetNghiemList = loaiXetNghiemRepository.findAll();
        assertThat(loaiXetNghiemList).hasSize(databaseSizeBeforeCreate + 1);
        LoaiXetNghiem testLoaiXetNghiem = loaiXetNghiemList.get(loaiXetNghiemList.size() - 1);
        assertThat(testLoaiXetNghiem.getTen()).isEqualTo(DEFAULT_TEN);
        assertThat(testLoaiXetNghiem.getMoTa()).isEqualTo(DEFAULT_MO_TA);
        assertThat(testLoaiXetNghiem.isEnable()).isEqualTo(DEFAULT_ENABLE);
        assertThat(testLoaiXetNghiem.getUuTien()).isEqualTo(DEFAULT_UU_TIEN);
        assertThat(testLoaiXetNghiem.getMaPhanLoai()).isEqualTo(DEFAULT_MA_PHAN_LOAI);
    }

    @Test
    @Transactional
    public void createLoaiXetNghiemWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = loaiXetNghiemRepository.findAll().size();

        // Create the LoaiXetNghiem with an existing ID
        loaiXetNghiem.setId(1L);
        LoaiXetNghiemDTO loaiXetNghiemDTO = loaiXetNghiemMapper.toDto(loaiXetNghiem);

        // An entity with an existing ID cannot be created, so this API call must fail
        restLoaiXetNghiemMockMvc.perform(post("/api/loai-xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(loaiXetNghiemDTO)))
            .andExpect(status().isBadRequest());

        // Validate the LoaiXetNghiem in the database
        List<LoaiXetNghiem> loaiXetNghiemList = loaiXetNghiemRepository.findAll();
        assertThat(loaiXetNghiemList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkUuTienIsRequired() throws Exception {
        int databaseSizeBeforeTest = loaiXetNghiemRepository.findAll().size();
        // set the field null
        loaiXetNghiem.setUuTien(null);

        // Create the LoaiXetNghiem, which fails.
        LoaiXetNghiemDTO loaiXetNghiemDTO = loaiXetNghiemMapper.toDto(loaiXetNghiem);

        restLoaiXetNghiemMockMvc.perform(post("/api/loai-xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(loaiXetNghiemDTO)))
            .andExpect(status().isBadRequest());

        List<LoaiXetNghiem> loaiXetNghiemList = loaiXetNghiemRepository.findAll();
        assertThat(loaiXetNghiemList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllLoaiXetNghiems() throws Exception {
        // Initialize the database
        loaiXetNghiemRepository.saveAndFlush(loaiXetNghiem);

        // Get all the loaiXetNghiemList
        restLoaiXetNghiemMockMvc.perform(get("/api/loai-xet-nghiems?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(loaiXetNghiem.getId().intValue())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].moTa").value(hasItem(DEFAULT_MO_TA)))
            .andExpect(jsonPath("$.[*].enable").value(hasItem(DEFAULT_ENABLE.booleanValue())))
            .andExpect(jsonPath("$.[*].uuTien").value(hasItem(DEFAULT_UU_TIEN)))
            .andExpect(jsonPath("$.[*].maPhanLoai").value(hasItem(DEFAULT_MA_PHAN_LOAI)));
    }
    
    @Test
    @Transactional
    public void getLoaiXetNghiem() throws Exception {
        // Initialize the database
        loaiXetNghiemRepository.saveAndFlush(loaiXetNghiem);

        // Get the loaiXetNghiem
        restLoaiXetNghiemMockMvc.perform(get("/api/loai-xet-nghiems/{id}", loaiXetNghiem.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(loaiXetNghiem.getId().intValue()))
            .andExpect(jsonPath("$.ten").value(DEFAULT_TEN))
            .andExpect(jsonPath("$.moTa").value(DEFAULT_MO_TA))
            .andExpect(jsonPath("$.enable").value(DEFAULT_ENABLE.booleanValue()))
            .andExpect(jsonPath("$.uuTien").value(DEFAULT_UU_TIEN))
            .andExpect(jsonPath("$.maPhanLoai").value(DEFAULT_MA_PHAN_LOAI));
    }


    @Test
    @Transactional
    public void getLoaiXetNghiemsByIdFiltering() throws Exception {
        // Initialize the database
        loaiXetNghiemRepository.saveAndFlush(loaiXetNghiem);

        Long id = loaiXetNghiem.getId();

        defaultLoaiXetNghiemShouldBeFound("id.equals=" + id);
        defaultLoaiXetNghiemShouldNotBeFound("id.notEquals=" + id);

        defaultLoaiXetNghiemShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultLoaiXetNghiemShouldNotBeFound("id.greaterThan=" + id);

        defaultLoaiXetNghiemShouldBeFound("id.lessThanOrEqual=" + id);
        defaultLoaiXetNghiemShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllLoaiXetNghiemsByTenIsEqualToSomething() throws Exception {
        // Initialize the database
        loaiXetNghiemRepository.saveAndFlush(loaiXetNghiem);

        // Get all the loaiXetNghiemList where ten equals to DEFAULT_TEN
        defaultLoaiXetNghiemShouldBeFound("ten.equals=" + DEFAULT_TEN);

        // Get all the loaiXetNghiemList where ten equals to UPDATED_TEN
        defaultLoaiXetNghiemShouldNotBeFound("ten.equals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllLoaiXetNghiemsByTenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        loaiXetNghiemRepository.saveAndFlush(loaiXetNghiem);

        // Get all the loaiXetNghiemList where ten not equals to DEFAULT_TEN
        defaultLoaiXetNghiemShouldNotBeFound("ten.notEquals=" + DEFAULT_TEN);

        // Get all the loaiXetNghiemList where ten not equals to UPDATED_TEN
        defaultLoaiXetNghiemShouldBeFound("ten.notEquals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllLoaiXetNghiemsByTenIsInShouldWork() throws Exception {
        // Initialize the database
        loaiXetNghiemRepository.saveAndFlush(loaiXetNghiem);

        // Get all the loaiXetNghiemList where ten in DEFAULT_TEN or UPDATED_TEN
        defaultLoaiXetNghiemShouldBeFound("ten.in=" + DEFAULT_TEN + "," + UPDATED_TEN);

        // Get all the loaiXetNghiemList where ten equals to UPDATED_TEN
        defaultLoaiXetNghiemShouldNotBeFound("ten.in=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllLoaiXetNghiemsByTenIsNullOrNotNull() throws Exception {
        // Initialize the database
        loaiXetNghiemRepository.saveAndFlush(loaiXetNghiem);

        // Get all the loaiXetNghiemList where ten is not null
        defaultLoaiXetNghiemShouldBeFound("ten.specified=true");

        // Get all the loaiXetNghiemList where ten is null
        defaultLoaiXetNghiemShouldNotBeFound("ten.specified=false");
    }
                @Test
    @Transactional
    public void getAllLoaiXetNghiemsByTenContainsSomething() throws Exception {
        // Initialize the database
        loaiXetNghiemRepository.saveAndFlush(loaiXetNghiem);

        // Get all the loaiXetNghiemList where ten contains DEFAULT_TEN
        defaultLoaiXetNghiemShouldBeFound("ten.contains=" + DEFAULT_TEN);

        // Get all the loaiXetNghiemList where ten contains UPDATED_TEN
        defaultLoaiXetNghiemShouldNotBeFound("ten.contains=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllLoaiXetNghiemsByTenNotContainsSomething() throws Exception {
        // Initialize the database
        loaiXetNghiemRepository.saveAndFlush(loaiXetNghiem);

        // Get all the loaiXetNghiemList where ten does not contain DEFAULT_TEN
        defaultLoaiXetNghiemShouldNotBeFound("ten.doesNotContain=" + DEFAULT_TEN);

        // Get all the loaiXetNghiemList where ten does not contain UPDATED_TEN
        defaultLoaiXetNghiemShouldBeFound("ten.doesNotContain=" + UPDATED_TEN);
    }


    @Test
    @Transactional
    public void getAllLoaiXetNghiemsByMoTaIsEqualToSomething() throws Exception {
        // Initialize the database
        loaiXetNghiemRepository.saveAndFlush(loaiXetNghiem);

        // Get all the loaiXetNghiemList where moTa equals to DEFAULT_MO_TA
        defaultLoaiXetNghiemShouldBeFound("moTa.equals=" + DEFAULT_MO_TA);

        // Get all the loaiXetNghiemList where moTa equals to UPDATED_MO_TA
        defaultLoaiXetNghiemShouldNotBeFound("moTa.equals=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    public void getAllLoaiXetNghiemsByMoTaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        loaiXetNghiemRepository.saveAndFlush(loaiXetNghiem);

        // Get all the loaiXetNghiemList where moTa not equals to DEFAULT_MO_TA
        defaultLoaiXetNghiemShouldNotBeFound("moTa.notEquals=" + DEFAULT_MO_TA);

        // Get all the loaiXetNghiemList where moTa not equals to UPDATED_MO_TA
        defaultLoaiXetNghiemShouldBeFound("moTa.notEquals=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    public void getAllLoaiXetNghiemsByMoTaIsInShouldWork() throws Exception {
        // Initialize the database
        loaiXetNghiemRepository.saveAndFlush(loaiXetNghiem);

        // Get all the loaiXetNghiemList where moTa in DEFAULT_MO_TA or UPDATED_MO_TA
        defaultLoaiXetNghiemShouldBeFound("moTa.in=" + DEFAULT_MO_TA + "," + UPDATED_MO_TA);

        // Get all the loaiXetNghiemList where moTa equals to UPDATED_MO_TA
        defaultLoaiXetNghiemShouldNotBeFound("moTa.in=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    public void getAllLoaiXetNghiemsByMoTaIsNullOrNotNull() throws Exception {
        // Initialize the database
        loaiXetNghiemRepository.saveAndFlush(loaiXetNghiem);

        // Get all the loaiXetNghiemList where moTa is not null
        defaultLoaiXetNghiemShouldBeFound("moTa.specified=true");

        // Get all the loaiXetNghiemList where moTa is null
        defaultLoaiXetNghiemShouldNotBeFound("moTa.specified=false");
    }
                @Test
    @Transactional
    public void getAllLoaiXetNghiemsByMoTaContainsSomething() throws Exception {
        // Initialize the database
        loaiXetNghiemRepository.saveAndFlush(loaiXetNghiem);

        // Get all the loaiXetNghiemList where moTa contains DEFAULT_MO_TA
        defaultLoaiXetNghiemShouldBeFound("moTa.contains=" + DEFAULT_MO_TA);

        // Get all the loaiXetNghiemList where moTa contains UPDATED_MO_TA
        defaultLoaiXetNghiemShouldNotBeFound("moTa.contains=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    public void getAllLoaiXetNghiemsByMoTaNotContainsSomething() throws Exception {
        // Initialize the database
        loaiXetNghiemRepository.saveAndFlush(loaiXetNghiem);

        // Get all the loaiXetNghiemList where moTa does not contain DEFAULT_MO_TA
        defaultLoaiXetNghiemShouldNotBeFound("moTa.doesNotContain=" + DEFAULT_MO_TA);

        // Get all the loaiXetNghiemList where moTa does not contain UPDATED_MO_TA
        defaultLoaiXetNghiemShouldBeFound("moTa.doesNotContain=" + UPDATED_MO_TA);
    }


    @Test
    @Transactional
    public void getAllLoaiXetNghiemsByEnableIsEqualToSomething() throws Exception {
        // Initialize the database
        loaiXetNghiemRepository.saveAndFlush(loaiXetNghiem);

        // Get all the loaiXetNghiemList where enable equals to DEFAULT_ENABLE
        defaultLoaiXetNghiemShouldBeFound("enable.equals=" + DEFAULT_ENABLE);

        // Get all the loaiXetNghiemList where enable equals to UPDATED_ENABLE
        defaultLoaiXetNghiemShouldNotBeFound("enable.equals=" + UPDATED_ENABLE);
    }

    @Test
    @Transactional
    public void getAllLoaiXetNghiemsByEnableIsNotEqualToSomething() throws Exception {
        // Initialize the database
        loaiXetNghiemRepository.saveAndFlush(loaiXetNghiem);

        // Get all the loaiXetNghiemList where enable not equals to DEFAULT_ENABLE
        defaultLoaiXetNghiemShouldNotBeFound("enable.notEquals=" + DEFAULT_ENABLE);

        // Get all the loaiXetNghiemList where enable not equals to UPDATED_ENABLE
        defaultLoaiXetNghiemShouldBeFound("enable.notEquals=" + UPDATED_ENABLE);
    }

    @Test
    @Transactional
    public void getAllLoaiXetNghiemsByEnableIsInShouldWork() throws Exception {
        // Initialize the database
        loaiXetNghiemRepository.saveAndFlush(loaiXetNghiem);

        // Get all the loaiXetNghiemList where enable in DEFAULT_ENABLE or UPDATED_ENABLE
        defaultLoaiXetNghiemShouldBeFound("enable.in=" + DEFAULT_ENABLE + "," + UPDATED_ENABLE);

        // Get all the loaiXetNghiemList where enable equals to UPDATED_ENABLE
        defaultLoaiXetNghiemShouldNotBeFound("enable.in=" + UPDATED_ENABLE);
    }

    @Test
    @Transactional
    public void getAllLoaiXetNghiemsByEnableIsNullOrNotNull() throws Exception {
        // Initialize the database
        loaiXetNghiemRepository.saveAndFlush(loaiXetNghiem);

        // Get all the loaiXetNghiemList where enable is not null
        defaultLoaiXetNghiemShouldBeFound("enable.specified=true");

        // Get all the loaiXetNghiemList where enable is null
        defaultLoaiXetNghiemShouldNotBeFound("enable.specified=false");
    }

    @Test
    @Transactional
    public void getAllLoaiXetNghiemsByUuTienIsEqualToSomething() throws Exception {
        // Initialize the database
        loaiXetNghiemRepository.saveAndFlush(loaiXetNghiem);

        // Get all the loaiXetNghiemList where uuTien equals to DEFAULT_UU_TIEN
        defaultLoaiXetNghiemShouldBeFound("uuTien.equals=" + DEFAULT_UU_TIEN);

        // Get all the loaiXetNghiemList where uuTien equals to UPDATED_UU_TIEN
        defaultLoaiXetNghiemShouldNotBeFound("uuTien.equals=" + UPDATED_UU_TIEN);
    }

    @Test
    @Transactional
    public void getAllLoaiXetNghiemsByUuTienIsNotEqualToSomething() throws Exception {
        // Initialize the database
        loaiXetNghiemRepository.saveAndFlush(loaiXetNghiem);

        // Get all the loaiXetNghiemList where uuTien not equals to DEFAULT_UU_TIEN
        defaultLoaiXetNghiemShouldNotBeFound("uuTien.notEquals=" + DEFAULT_UU_TIEN);

        // Get all the loaiXetNghiemList where uuTien not equals to UPDATED_UU_TIEN
        defaultLoaiXetNghiemShouldBeFound("uuTien.notEquals=" + UPDATED_UU_TIEN);
    }

    @Test
    @Transactional
    public void getAllLoaiXetNghiemsByUuTienIsInShouldWork() throws Exception {
        // Initialize the database
        loaiXetNghiemRepository.saveAndFlush(loaiXetNghiem);

        // Get all the loaiXetNghiemList where uuTien in DEFAULT_UU_TIEN or UPDATED_UU_TIEN
        defaultLoaiXetNghiemShouldBeFound("uuTien.in=" + DEFAULT_UU_TIEN + "," + UPDATED_UU_TIEN);

        // Get all the loaiXetNghiemList where uuTien equals to UPDATED_UU_TIEN
        defaultLoaiXetNghiemShouldNotBeFound("uuTien.in=" + UPDATED_UU_TIEN);
    }

    @Test
    @Transactional
    public void getAllLoaiXetNghiemsByUuTienIsNullOrNotNull() throws Exception {
        // Initialize the database
        loaiXetNghiemRepository.saveAndFlush(loaiXetNghiem);

        // Get all the loaiXetNghiemList where uuTien is not null
        defaultLoaiXetNghiemShouldBeFound("uuTien.specified=true");

        // Get all the loaiXetNghiemList where uuTien is null
        defaultLoaiXetNghiemShouldNotBeFound("uuTien.specified=false");
    }

    @Test
    @Transactional
    public void getAllLoaiXetNghiemsByUuTienIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        loaiXetNghiemRepository.saveAndFlush(loaiXetNghiem);

        // Get all the loaiXetNghiemList where uuTien is greater than or equal to DEFAULT_UU_TIEN
        defaultLoaiXetNghiemShouldBeFound("uuTien.greaterThanOrEqual=" + DEFAULT_UU_TIEN);

        // Get all the loaiXetNghiemList where uuTien is greater than or equal to UPDATED_UU_TIEN
        defaultLoaiXetNghiemShouldNotBeFound("uuTien.greaterThanOrEqual=" + UPDATED_UU_TIEN);
    }

    @Test
    @Transactional
    public void getAllLoaiXetNghiemsByUuTienIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        loaiXetNghiemRepository.saveAndFlush(loaiXetNghiem);

        // Get all the loaiXetNghiemList where uuTien is less than or equal to DEFAULT_UU_TIEN
        defaultLoaiXetNghiemShouldBeFound("uuTien.lessThanOrEqual=" + DEFAULT_UU_TIEN);

        // Get all the loaiXetNghiemList where uuTien is less than or equal to SMALLER_UU_TIEN
        defaultLoaiXetNghiemShouldNotBeFound("uuTien.lessThanOrEqual=" + SMALLER_UU_TIEN);
    }

    @Test
    @Transactional
    public void getAllLoaiXetNghiemsByUuTienIsLessThanSomething() throws Exception {
        // Initialize the database
        loaiXetNghiemRepository.saveAndFlush(loaiXetNghiem);

        // Get all the loaiXetNghiemList where uuTien is less than DEFAULT_UU_TIEN
        defaultLoaiXetNghiemShouldNotBeFound("uuTien.lessThan=" + DEFAULT_UU_TIEN);

        // Get all the loaiXetNghiemList where uuTien is less than UPDATED_UU_TIEN
        defaultLoaiXetNghiemShouldBeFound("uuTien.lessThan=" + UPDATED_UU_TIEN);
    }

    @Test
    @Transactional
    public void getAllLoaiXetNghiemsByUuTienIsGreaterThanSomething() throws Exception {
        // Initialize the database
        loaiXetNghiemRepository.saveAndFlush(loaiXetNghiem);

        // Get all the loaiXetNghiemList where uuTien is greater than DEFAULT_UU_TIEN
        defaultLoaiXetNghiemShouldNotBeFound("uuTien.greaterThan=" + DEFAULT_UU_TIEN);

        // Get all the loaiXetNghiemList where uuTien is greater than SMALLER_UU_TIEN
        defaultLoaiXetNghiemShouldBeFound("uuTien.greaterThan=" + SMALLER_UU_TIEN);
    }


    @Test
    @Transactional
    public void getAllLoaiXetNghiemsByMaPhanLoaiIsEqualToSomething() throws Exception {
        // Initialize the database
        loaiXetNghiemRepository.saveAndFlush(loaiXetNghiem);

        // Get all the loaiXetNghiemList where maPhanLoai equals to DEFAULT_MA_PHAN_LOAI
        defaultLoaiXetNghiemShouldBeFound("maPhanLoai.equals=" + DEFAULT_MA_PHAN_LOAI);

        // Get all the loaiXetNghiemList where maPhanLoai equals to UPDATED_MA_PHAN_LOAI
        defaultLoaiXetNghiemShouldNotBeFound("maPhanLoai.equals=" + UPDATED_MA_PHAN_LOAI);
    }

    @Test
    @Transactional
    public void getAllLoaiXetNghiemsByMaPhanLoaiIsNotEqualToSomething() throws Exception {
        // Initialize the database
        loaiXetNghiemRepository.saveAndFlush(loaiXetNghiem);

        // Get all the loaiXetNghiemList where maPhanLoai not equals to DEFAULT_MA_PHAN_LOAI
        defaultLoaiXetNghiemShouldNotBeFound("maPhanLoai.notEquals=" + DEFAULT_MA_PHAN_LOAI);

        // Get all the loaiXetNghiemList where maPhanLoai not equals to UPDATED_MA_PHAN_LOAI
        defaultLoaiXetNghiemShouldBeFound("maPhanLoai.notEquals=" + UPDATED_MA_PHAN_LOAI);
    }

    @Test
    @Transactional
    public void getAllLoaiXetNghiemsByMaPhanLoaiIsInShouldWork() throws Exception {
        // Initialize the database
        loaiXetNghiemRepository.saveAndFlush(loaiXetNghiem);

        // Get all the loaiXetNghiemList where maPhanLoai in DEFAULT_MA_PHAN_LOAI or UPDATED_MA_PHAN_LOAI
        defaultLoaiXetNghiemShouldBeFound("maPhanLoai.in=" + DEFAULT_MA_PHAN_LOAI + "," + UPDATED_MA_PHAN_LOAI);

        // Get all the loaiXetNghiemList where maPhanLoai equals to UPDATED_MA_PHAN_LOAI
        defaultLoaiXetNghiemShouldNotBeFound("maPhanLoai.in=" + UPDATED_MA_PHAN_LOAI);
    }

    @Test
    @Transactional
    public void getAllLoaiXetNghiemsByMaPhanLoaiIsNullOrNotNull() throws Exception {
        // Initialize the database
        loaiXetNghiemRepository.saveAndFlush(loaiXetNghiem);

        // Get all the loaiXetNghiemList where maPhanLoai is not null
        defaultLoaiXetNghiemShouldBeFound("maPhanLoai.specified=true");

        // Get all the loaiXetNghiemList where maPhanLoai is null
        defaultLoaiXetNghiemShouldNotBeFound("maPhanLoai.specified=false");
    }
                @Test
    @Transactional
    public void getAllLoaiXetNghiemsByMaPhanLoaiContainsSomething() throws Exception {
        // Initialize the database
        loaiXetNghiemRepository.saveAndFlush(loaiXetNghiem);

        // Get all the loaiXetNghiemList where maPhanLoai contains DEFAULT_MA_PHAN_LOAI
        defaultLoaiXetNghiemShouldBeFound("maPhanLoai.contains=" + DEFAULT_MA_PHAN_LOAI);

        // Get all the loaiXetNghiemList where maPhanLoai contains UPDATED_MA_PHAN_LOAI
        defaultLoaiXetNghiemShouldNotBeFound("maPhanLoai.contains=" + UPDATED_MA_PHAN_LOAI);
    }

    @Test
    @Transactional
    public void getAllLoaiXetNghiemsByMaPhanLoaiNotContainsSomething() throws Exception {
        // Initialize the database
        loaiXetNghiemRepository.saveAndFlush(loaiXetNghiem);

        // Get all the loaiXetNghiemList where maPhanLoai does not contain DEFAULT_MA_PHAN_LOAI
        defaultLoaiXetNghiemShouldNotBeFound("maPhanLoai.doesNotContain=" + DEFAULT_MA_PHAN_LOAI);

        // Get all the loaiXetNghiemList where maPhanLoai does not contain UPDATED_MA_PHAN_LOAI
        defaultLoaiXetNghiemShouldBeFound("maPhanLoai.doesNotContain=" + UPDATED_MA_PHAN_LOAI);
    }


    @Test
    @Transactional
    public void getAllLoaiXetNghiemsByDonViIsEqualToSomething() throws Exception {
        // Get already existing entity
        DonVi donVi = loaiXetNghiem.getDonVi();
        loaiXetNghiemRepository.saveAndFlush(loaiXetNghiem);
        Long donViId = donVi.getId();

        // Get all the loaiXetNghiemList where donVi equals to donViId
        defaultLoaiXetNghiemShouldBeFound("donViId.equals=" + donViId);

        // Get all the loaiXetNghiemList where donVi equals to donViId + 1
        defaultLoaiXetNghiemShouldNotBeFound("donViId.equals=" + (donViId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultLoaiXetNghiemShouldBeFound(String filter) throws Exception {
        restLoaiXetNghiemMockMvc.perform(get("/api/loai-xet-nghiems?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(loaiXetNghiem.getId().intValue())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].moTa").value(hasItem(DEFAULT_MO_TA)))
            .andExpect(jsonPath("$.[*].enable").value(hasItem(DEFAULT_ENABLE.booleanValue())))
            .andExpect(jsonPath("$.[*].uuTien").value(hasItem(DEFAULT_UU_TIEN)))
            .andExpect(jsonPath("$.[*].maPhanLoai").value(hasItem(DEFAULT_MA_PHAN_LOAI)));

        // Check, that the count call also returns 1
        restLoaiXetNghiemMockMvc.perform(get("/api/loai-xet-nghiems/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultLoaiXetNghiemShouldNotBeFound(String filter) throws Exception {
        restLoaiXetNghiemMockMvc.perform(get("/api/loai-xet-nghiems?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restLoaiXetNghiemMockMvc.perform(get("/api/loai-xet-nghiems/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingLoaiXetNghiem() throws Exception {
        // Get the loaiXetNghiem
        restLoaiXetNghiemMockMvc.perform(get("/api/loai-xet-nghiems/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLoaiXetNghiem() throws Exception {
        // Initialize the database
        loaiXetNghiemRepository.saveAndFlush(loaiXetNghiem);

        int databaseSizeBeforeUpdate = loaiXetNghiemRepository.findAll().size();

        // Update the loaiXetNghiem
        LoaiXetNghiem updatedLoaiXetNghiem = loaiXetNghiemRepository.findById(loaiXetNghiem.getId()).get();
        // Disconnect from session so that the updates on updatedLoaiXetNghiem are not directly saved in db
        em.detach(updatedLoaiXetNghiem);
        updatedLoaiXetNghiem
            .ten(UPDATED_TEN)
            .moTa(UPDATED_MO_TA)
            .enable(UPDATED_ENABLE)
            .uuTien(UPDATED_UU_TIEN)
            .maPhanLoai(UPDATED_MA_PHAN_LOAI);
        LoaiXetNghiemDTO loaiXetNghiemDTO = loaiXetNghiemMapper.toDto(updatedLoaiXetNghiem);

        restLoaiXetNghiemMockMvc.perform(put("/api/loai-xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(loaiXetNghiemDTO)))
            .andExpect(status().isOk());

        // Validate the LoaiXetNghiem in the database
        List<LoaiXetNghiem> loaiXetNghiemList = loaiXetNghiemRepository.findAll();
        assertThat(loaiXetNghiemList).hasSize(databaseSizeBeforeUpdate);
        LoaiXetNghiem testLoaiXetNghiem = loaiXetNghiemList.get(loaiXetNghiemList.size() - 1);
        assertThat(testLoaiXetNghiem.getTen()).isEqualTo(UPDATED_TEN);
        assertThat(testLoaiXetNghiem.getMoTa()).isEqualTo(UPDATED_MO_TA);
        assertThat(testLoaiXetNghiem.isEnable()).isEqualTo(UPDATED_ENABLE);
        assertThat(testLoaiXetNghiem.getUuTien()).isEqualTo(UPDATED_UU_TIEN);
        assertThat(testLoaiXetNghiem.getMaPhanLoai()).isEqualTo(UPDATED_MA_PHAN_LOAI);
    }

    @Test
    @Transactional
    public void updateNonExistingLoaiXetNghiem() throws Exception {
        int databaseSizeBeforeUpdate = loaiXetNghiemRepository.findAll().size();

        // Create the LoaiXetNghiem
        LoaiXetNghiemDTO loaiXetNghiemDTO = loaiXetNghiemMapper.toDto(loaiXetNghiem);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLoaiXetNghiemMockMvc.perform(put("/api/loai-xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(loaiXetNghiemDTO)))
            .andExpect(status().isBadRequest());

        // Validate the LoaiXetNghiem in the database
        List<LoaiXetNghiem> loaiXetNghiemList = loaiXetNghiemRepository.findAll();
        assertThat(loaiXetNghiemList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteLoaiXetNghiem() throws Exception {
        // Initialize the database
        loaiXetNghiemRepository.saveAndFlush(loaiXetNghiem);

        int databaseSizeBeforeDelete = loaiXetNghiemRepository.findAll().size();

        // Delete the loaiXetNghiem
        restLoaiXetNghiemMockMvc.perform(delete("/api/loai-xet-nghiems/{id}", loaiXetNghiem.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<LoaiXetNghiem> loaiXetNghiemList = loaiXetNghiemRepository.findAll();
        assertThat(loaiXetNghiemList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

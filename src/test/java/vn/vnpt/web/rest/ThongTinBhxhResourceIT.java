package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.ThongTinBhxh;
import vn.vnpt.domain.DonVi;
import vn.vnpt.repository.ThongTinBhxhRepository;
import vn.vnpt.service.ThongTinBhxhService;
import vn.vnpt.service.dto.ThongTinBhxhDTO;
import vn.vnpt.service.mapper.ThongTinBhxhMapper;
import vn.vnpt.service.dto.ThongTinBhxhCriteria;
import vn.vnpt.service.ThongTinBhxhQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ThongTinBhxhResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class ThongTinBhxhResourceIT {

    private static final String DEFAULT_DVTT = "AAAAAAAAAA";
    private static final String UPDATED_DVTT = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ENABLED = false;
    private static final Boolean UPDATED_ENABLED = true;

    private static final LocalDate DEFAULT_NGAY_AP_DUNG_BHYT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_AP_DUNG_BHYT = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_NGAY_AP_DUNG_BHYT = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_TEN = "AAAAAAAAAA";
    private static final String UPDATED_TEN = "BBBBBBBBBB";

    @Autowired
    private ThongTinBhxhRepository thongTinBhxhRepository;

    @Autowired
    private ThongTinBhxhMapper thongTinBhxhMapper;

    @Autowired
    private ThongTinBhxhService thongTinBhxhService;

    @Autowired
    private ThongTinBhxhQueryService thongTinBhxhQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restThongTinBhxhMockMvc;

    private ThongTinBhxh thongTinBhxh;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ThongTinBhxh createEntity(EntityManager em) {
        ThongTinBhxh thongTinBhxh = new ThongTinBhxh()
            .dvtt(DEFAULT_DVTT)
            .enabled(DEFAULT_ENABLED)
            .ngayApDungBhyt(DEFAULT_NGAY_AP_DUNG_BHYT)
            .ten(DEFAULT_TEN);
        // Add required entity
        DonVi donVi;
        if (TestUtil.findAll(em, DonVi.class).isEmpty()) {
            donVi = DonViResourceIT.createEntity(em);
            em.persist(donVi);
            em.flush();
        } else {
            donVi = TestUtil.findAll(em, DonVi.class).get(0);
        }
        thongTinBhxh.setDonVi(donVi);
        return thongTinBhxh;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ThongTinBhxh createUpdatedEntity(EntityManager em) {
        ThongTinBhxh thongTinBhxh = new ThongTinBhxh()
            .dvtt(UPDATED_DVTT)
            .enabled(UPDATED_ENABLED)
            .ngayApDungBhyt(UPDATED_NGAY_AP_DUNG_BHYT)
            .ten(UPDATED_TEN);
        // Add required entity
        DonVi donVi;
        if (TestUtil.findAll(em, DonVi.class).isEmpty()) {
            donVi = DonViResourceIT.createUpdatedEntity(em);
            em.persist(donVi);
            em.flush();
        } else {
            donVi = TestUtil.findAll(em, DonVi.class).get(0);
        }
        thongTinBhxh.setDonVi(donVi);
        return thongTinBhxh;
    }

    @BeforeEach
    public void initTest() {
        thongTinBhxh = createEntity(em);
    }

    @Test
    @Transactional
    public void createThongTinBhxh() throws Exception {
        int databaseSizeBeforeCreate = thongTinBhxhRepository.findAll().size();

        // Create the ThongTinBhxh
        ThongTinBhxhDTO thongTinBhxhDTO = thongTinBhxhMapper.toDto(thongTinBhxh);
        restThongTinBhxhMockMvc.perform(post("/api/thong-tin-bhxhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(thongTinBhxhDTO)))
            .andExpect(status().isCreated());

        // Validate the ThongTinBhxh in the database
        List<ThongTinBhxh> thongTinBhxhList = thongTinBhxhRepository.findAll();
        assertThat(thongTinBhxhList).hasSize(databaseSizeBeforeCreate + 1);
        ThongTinBhxh testThongTinBhxh = thongTinBhxhList.get(thongTinBhxhList.size() - 1);
        assertThat(testThongTinBhxh.getDvtt()).isEqualTo(DEFAULT_DVTT);
        assertThat(testThongTinBhxh.isEnabled()).isEqualTo(DEFAULT_ENABLED);
        assertThat(testThongTinBhxh.getNgayApDungBhyt()).isEqualTo(DEFAULT_NGAY_AP_DUNG_BHYT);
        assertThat(testThongTinBhxh.getTen()).isEqualTo(DEFAULT_TEN);
    }

    @Test
    @Transactional
    public void createThongTinBhxhWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = thongTinBhxhRepository.findAll().size();

        // Create the ThongTinBhxh with an existing ID
        thongTinBhxh.setId(1L);
        ThongTinBhxhDTO thongTinBhxhDTO = thongTinBhxhMapper.toDto(thongTinBhxh);

        // An entity with an existing ID cannot be created, so this API call must fail
        restThongTinBhxhMockMvc.perform(post("/api/thong-tin-bhxhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(thongTinBhxhDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ThongTinBhxh in the database
        List<ThongTinBhxh> thongTinBhxhList = thongTinBhxhRepository.findAll();
        assertThat(thongTinBhxhList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkDvttIsRequired() throws Exception {
        int databaseSizeBeforeTest = thongTinBhxhRepository.findAll().size();
        // set the field null
        thongTinBhxh.setDvtt(null);

        // Create the ThongTinBhxh, which fails.
        ThongTinBhxhDTO thongTinBhxhDTO = thongTinBhxhMapper.toDto(thongTinBhxh);

        restThongTinBhxhMockMvc.perform(post("/api/thong-tin-bhxhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(thongTinBhxhDTO)))
            .andExpect(status().isBadRequest());

        List<ThongTinBhxh> thongTinBhxhList = thongTinBhxhRepository.findAll();
        assertThat(thongTinBhxhList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEnabledIsRequired() throws Exception {
        int databaseSizeBeforeTest = thongTinBhxhRepository.findAll().size();
        // set the field null
        thongTinBhxh.setEnabled(null);

        // Create the ThongTinBhxh, which fails.
        ThongTinBhxhDTO thongTinBhxhDTO = thongTinBhxhMapper.toDto(thongTinBhxh);

        restThongTinBhxhMockMvc.perform(post("/api/thong-tin-bhxhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(thongTinBhxhDTO)))
            .andExpect(status().isBadRequest());

        List<ThongTinBhxh> thongTinBhxhList = thongTinBhxhRepository.findAll();
        assertThat(thongTinBhxhList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNgayApDungBhytIsRequired() throws Exception {
        int databaseSizeBeforeTest = thongTinBhxhRepository.findAll().size();
        // set the field null
        thongTinBhxh.setNgayApDungBhyt(null);

        // Create the ThongTinBhxh, which fails.
        ThongTinBhxhDTO thongTinBhxhDTO = thongTinBhxhMapper.toDto(thongTinBhxh);

        restThongTinBhxhMockMvc.perform(post("/api/thong-tin-bhxhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(thongTinBhxhDTO)))
            .andExpect(status().isBadRequest());

        List<ThongTinBhxh> thongTinBhxhList = thongTinBhxhRepository.findAll();
        assertThat(thongTinBhxhList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTenIsRequired() throws Exception {
        int databaseSizeBeforeTest = thongTinBhxhRepository.findAll().size();
        // set the field null
        thongTinBhxh.setTen(null);

        // Create the ThongTinBhxh, which fails.
        ThongTinBhxhDTO thongTinBhxhDTO = thongTinBhxhMapper.toDto(thongTinBhxh);

        restThongTinBhxhMockMvc.perform(post("/api/thong-tin-bhxhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(thongTinBhxhDTO)))
            .andExpect(status().isBadRequest());

        List<ThongTinBhxh> thongTinBhxhList = thongTinBhxhRepository.findAll();
        assertThat(thongTinBhxhList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllThongTinBhxhs() throws Exception {
        // Initialize the database
        thongTinBhxhRepository.saveAndFlush(thongTinBhxh);

        // Get all the thongTinBhxhList
        restThongTinBhxhMockMvc.perform(get("/api/thong-tin-bhxhs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(thongTinBhxh.getId().intValue())))
            .andExpect(jsonPath("$.[*].dvtt").value(hasItem(DEFAULT_DVTT)))
            .andExpect(jsonPath("$.[*].enabled").value(hasItem(DEFAULT_ENABLED.booleanValue())))
            .andExpect(jsonPath("$.[*].ngayApDungBhyt").value(hasItem(DEFAULT_NGAY_AP_DUNG_BHYT.toString())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)));
    }
    
    @Test
    @Transactional
    public void getThongTinBhxh() throws Exception {
        // Initialize the database
        thongTinBhxhRepository.saveAndFlush(thongTinBhxh);

        // Get the thongTinBhxh
        restThongTinBhxhMockMvc.perform(get("/api/thong-tin-bhxhs/{id}", thongTinBhxh.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(thongTinBhxh.getId().intValue()))
            .andExpect(jsonPath("$.dvtt").value(DEFAULT_DVTT))
            .andExpect(jsonPath("$.enabled").value(DEFAULT_ENABLED.booleanValue()))
            .andExpect(jsonPath("$.ngayApDungBhyt").value(DEFAULT_NGAY_AP_DUNG_BHYT.toString()))
            .andExpect(jsonPath("$.ten").value(DEFAULT_TEN));
    }


    @Test
    @Transactional
    public void getThongTinBhxhsByIdFiltering() throws Exception {
        // Initialize the database
        thongTinBhxhRepository.saveAndFlush(thongTinBhxh);

        Long id = thongTinBhxh.getId();

        defaultThongTinBhxhShouldBeFound("id.equals=" + id);
        defaultThongTinBhxhShouldNotBeFound("id.notEquals=" + id);

        defaultThongTinBhxhShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultThongTinBhxhShouldNotBeFound("id.greaterThan=" + id);

        defaultThongTinBhxhShouldBeFound("id.lessThanOrEqual=" + id);
        defaultThongTinBhxhShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllThongTinBhxhsByDvttIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinBhxhRepository.saveAndFlush(thongTinBhxh);

        // Get all the thongTinBhxhList where dvtt equals to DEFAULT_DVTT
        defaultThongTinBhxhShouldBeFound("dvtt.equals=" + DEFAULT_DVTT);

        // Get all the thongTinBhxhList where dvtt equals to UPDATED_DVTT
        defaultThongTinBhxhShouldNotBeFound("dvtt.equals=" + UPDATED_DVTT);
    }

    @Test
    @Transactional
    public void getAllThongTinBhxhsByDvttIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinBhxhRepository.saveAndFlush(thongTinBhxh);

        // Get all the thongTinBhxhList where dvtt not equals to DEFAULT_DVTT
        defaultThongTinBhxhShouldNotBeFound("dvtt.notEquals=" + DEFAULT_DVTT);

        // Get all the thongTinBhxhList where dvtt not equals to UPDATED_DVTT
        defaultThongTinBhxhShouldBeFound("dvtt.notEquals=" + UPDATED_DVTT);
    }

    @Test
    @Transactional
    public void getAllThongTinBhxhsByDvttIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinBhxhRepository.saveAndFlush(thongTinBhxh);

        // Get all the thongTinBhxhList where dvtt in DEFAULT_DVTT or UPDATED_DVTT
        defaultThongTinBhxhShouldBeFound("dvtt.in=" + DEFAULT_DVTT + "," + UPDATED_DVTT);

        // Get all the thongTinBhxhList where dvtt equals to UPDATED_DVTT
        defaultThongTinBhxhShouldNotBeFound("dvtt.in=" + UPDATED_DVTT);
    }

    @Test
    @Transactional
    public void getAllThongTinBhxhsByDvttIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinBhxhRepository.saveAndFlush(thongTinBhxh);

        // Get all the thongTinBhxhList where dvtt is not null
        defaultThongTinBhxhShouldBeFound("dvtt.specified=true");

        // Get all the thongTinBhxhList where dvtt is null
        defaultThongTinBhxhShouldNotBeFound("dvtt.specified=false");
    }
                @Test
    @Transactional
    public void getAllThongTinBhxhsByDvttContainsSomething() throws Exception {
        // Initialize the database
        thongTinBhxhRepository.saveAndFlush(thongTinBhxh);

        // Get all the thongTinBhxhList where dvtt contains DEFAULT_DVTT
        defaultThongTinBhxhShouldBeFound("dvtt.contains=" + DEFAULT_DVTT);

        // Get all the thongTinBhxhList where dvtt contains UPDATED_DVTT
        defaultThongTinBhxhShouldNotBeFound("dvtt.contains=" + UPDATED_DVTT);
    }

    @Test
    @Transactional
    public void getAllThongTinBhxhsByDvttNotContainsSomething() throws Exception {
        // Initialize the database
        thongTinBhxhRepository.saveAndFlush(thongTinBhxh);

        // Get all the thongTinBhxhList where dvtt does not contain DEFAULT_DVTT
        defaultThongTinBhxhShouldNotBeFound("dvtt.doesNotContain=" + DEFAULT_DVTT);

        // Get all the thongTinBhxhList where dvtt does not contain UPDATED_DVTT
        defaultThongTinBhxhShouldBeFound("dvtt.doesNotContain=" + UPDATED_DVTT);
    }


    @Test
    @Transactional
    public void getAllThongTinBhxhsByEnabledIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinBhxhRepository.saveAndFlush(thongTinBhxh);

        // Get all the thongTinBhxhList where enabled equals to DEFAULT_ENABLED
        defaultThongTinBhxhShouldBeFound("enabled.equals=" + DEFAULT_ENABLED);

        // Get all the thongTinBhxhList where enabled equals to UPDATED_ENABLED
        defaultThongTinBhxhShouldNotBeFound("enabled.equals=" + UPDATED_ENABLED);
    }

    @Test
    @Transactional
    public void getAllThongTinBhxhsByEnabledIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinBhxhRepository.saveAndFlush(thongTinBhxh);

        // Get all the thongTinBhxhList where enabled not equals to DEFAULT_ENABLED
        defaultThongTinBhxhShouldNotBeFound("enabled.notEquals=" + DEFAULT_ENABLED);

        // Get all the thongTinBhxhList where enabled not equals to UPDATED_ENABLED
        defaultThongTinBhxhShouldBeFound("enabled.notEquals=" + UPDATED_ENABLED);
    }

    @Test
    @Transactional
    public void getAllThongTinBhxhsByEnabledIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinBhxhRepository.saveAndFlush(thongTinBhxh);

        // Get all the thongTinBhxhList where enabled in DEFAULT_ENABLED or UPDATED_ENABLED
        defaultThongTinBhxhShouldBeFound("enabled.in=" + DEFAULT_ENABLED + "," + UPDATED_ENABLED);

        // Get all the thongTinBhxhList where enabled equals to UPDATED_ENABLED
        defaultThongTinBhxhShouldNotBeFound("enabled.in=" + UPDATED_ENABLED);
    }

    @Test
    @Transactional
    public void getAllThongTinBhxhsByEnabledIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinBhxhRepository.saveAndFlush(thongTinBhxh);

        // Get all the thongTinBhxhList where enabled is not null
        defaultThongTinBhxhShouldBeFound("enabled.specified=true");

        // Get all the thongTinBhxhList where enabled is null
        defaultThongTinBhxhShouldNotBeFound("enabled.specified=false");
    }

    @Test
    @Transactional
    public void getAllThongTinBhxhsByNgayApDungBhytIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinBhxhRepository.saveAndFlush(thongTinBhxh);

        // Get all the thongTinBhxhList where ngayApDungBhyt equals to DEFAULT_NGAY_AP_DUNG_BHYT
        defaultThongTinBhxhShouldBeFound("ngayApDungBhyt.equals=" + DEFAULT_NGAY_AP_DUNG_BHYT);

        // Get all the thongTinBhxhList where ngayApDungBhyt equals to UPDATED_NGAY_AP_DUNG_BHYT
        defaultThongTinBhxhShouldNotBeFound("ngayApDungBhyt.equals=" + UPDATED_NGAY_AP_DUNG_BHYT);
    }

    @Test
    @Transactional
    public void getAllThongTinBhxhsByNgayApDungBhytIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinBhxhRepository.saveAndFlush(thongTinBhxh);

        // Get all the thongTinBhxhList where ngayApDungBhyt not equals to DEFAULT_NGAY_AP_DUNG_BHYT
        defaultThongTinBhxhShouldNotBeFound("ngayApDungBhyt.notEquals=" + DEFAULT_NGAY_AP_DUNG_BHYT);

        // Get all the thongTinBhxhList where ngayApDungBhyt not equals to UPDATED_NGAY_AP_DUNG_BHYT
        defaultThongTinBhxhShouldBeFound("ngayApDungBhyt.notEquals=" + UPDATED_NGAY_AP_DUNG_BHYT);
    }

    @Test
    @Transactional
    public void getAllThongTinBhxhsByNgayApDungBhytIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinBhxhRepository.saveAndFlush(thongTinBhxh);

        // Get all the thongTinBhxhList where ngayApDungBhyt in DEFAULT_NGAY_AP_DUNG_BHYT or UPDATED_NGAY_AP_DUNG_BHYT
        defaultThongTinBhxhShouldBeFound("ngayApDungBhyt.in=" + DEFAULT_NGAY_AP_DUNG_BHYT + "," + UPDATED_NGAY_AP_DUNG_BHYT);

        // Get all the thongTinBhxhList where ngayApDungBhyt equals to UPDATED_NGAY_AP_DUNG_BHYT
        defaultThongTinBhxhShouldNotBeFound("ngayApDungBhyt.in=" + UPDATED_NGAY_AP_DUNG_BHYT);
    }

    @Test
    @Transactional
    public void getAllThongTinBhxhsByNgayApDungBhytIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinBhxhRepository.saveAndFlush(thongTinBhxh);

        // Get all the thongTinBhxhList where ngayApDungBhyt is not null
        defaultThongTinBhxhShouldBeFound("ngayApDungBhyt.specified=true");

        // Get all the thongTinBhxhList where ngayApDungBhyt is null
        defaultThongTinBhxhShouldNotBeFound("ngayApDungBhyt.specified=false");
    }

    @Test
    @Transactional
    public void getAllThongTinBhxhsByNgayApDungBhytIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinBhxhRepository.saveAndFlush(thongTinBhxh);

        // Get all the thongTinBhxhList where ngayApDungBhyt is greater than or equal to DEFAULT_NGAY_AP_DUNG_BHYT
        defaultThongTinBhxhShouldBeFound("ngayApDungBhyt.greaterThanOrEqual=" + DEFAULT_NGAY_AP_DUNG_BHYT);

        // Get all the thongTinBhxhList where ngayApDungBhyt is greater than or equal to UPDATED_NGAY_AP_DUNG_BHYT
        defaultThongTinBhxhShouldNotBeFound("ngayApDungBhyt.greaterThanOrEqual=" + UPDATED_NGAY_AP_DUNG_BHYT);
    }

    @Test
    @Transactional
    public void getAllThongTinBhxhsByNgayApDungBhytIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinBhxhRepository.saveAndFlush(thongTinBhxh);

        // Get all the thongTinBhxhList where ngayApDungBhyt is less than or equal to DEFAULT_NGAY_AP_DUNG_BHYT
        defaultThongTinBhxhShouldBeFound("ngayApDungBhyt.lessThanOrEqual=" + DEFAULT_NGAY_AP_DUNG_BHYT);

        // Get all the thongTinBhxhList where ngayApDungBhyt is less than or equal to SMALLER_NGAY_AP_DUNG_BHYT
        defaultThongTinBhxhShouldNotBeFound("ngayApDungBhyt.lessThanOrEqual=" + SMALLER_NGAY_AP_DUNG_BHYT);
    }

    @Test
    @Transactional
    public void getAllThongTinBhxhsByNgayApDungBhytIsLessThanSomething() throws Exception {
        // Initialize the database
        thongTinBhxhRepository.saveAndFlush(thongTinBhxh);

        // Get all the thongTinBhxhList where ngayApDungBhyt is less than DEFAULT_NGAY_AP_DUNG_BHYT
        defaultThongTinBhxhShouldNotBeFound("ngayApDungBhyt.lessThan=" + DEFAULT_NGAY_AP_DUNG_BHYT);

        // Get all the thongTinBhxhList where ngayApDungBhyt is less than UPDATED_NGAY_AP_DUNG_BHYT
        defaultThongTinBhxhShouldBeFound("ngayApDungBhyt.lessThan=" + UPDATED_NGAY_AP_DUNG_BHYT);
    }

    @Test
    @Transactional
    public void getAllThongTinBhxhsByNgayApDungBhytIsGreaterThanSomething() throws Exception {
        // Initialize the database
        thongTinBhxhRepository.saveAndFlush(thongTinBhxh);

        // Get all the thongTinBhxhList where ngayApDungBhyt is greater than DEFAULT_NGAY_AP_DUNG_BHYT
        defaultThongTinBhxhShouldNotBeFound("ngayApDungBhyt.greaterThan=" + DEFAULT_NGAY_AP_DUNG_BHYT);

        // Get all the thongTinBhxhList where ngayApDungBhyt is greater than SMALLER_NGAY_AP_DUNG_BHYT
        defaultThongTinBhxhShouldBeFound("ngayApDungBhyt.greaterThan=" + SMALLER_NGAY_AP_DUNG_BHYT);
    }


    @Test
    @Transactional
    public void getAllThongTinBhxhsByTenIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinBhxhRepository.saveAndFlush(thongTinBhxh);

        // Get all the thongTinBhxhList where ten equals to DEFAULT_TEN
        defaultThongTinBhxhShouldBeFound("ten.equals=" + DEFAULT_TEN);

        // Get all the thongTinBhxhList where ten equals to UPDATED_TEN
        defaultThongTinBhxhShouldNotBeFound("ten.equals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllThongTinBhxhsByTenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinBhxhRepository.saveAndFlush(thongTinBhxh);

        // Get all the thongTinBhxhList where ten not equals to DEFAULT_TEN
        defaultThongTinBhxhShouldNotBeFound("ten.notEquals=" + DEFAULT_TEN);

        // Get all the thongTinBhxhList where ten not equals to UPDATED_TEN
        defaultThongTinBhxhShouldBeFound("ten.notEquals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllThongTinBhxhsByTenIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinBhxhRepository.saveAndFlush(thongTinBhxh);

        // Get all the thongTinBhxhList where ten in DEFAULT_TEN or UPDATED_TEN
        defaultThongTinBhxhShouldBeFound("ten.in=" + DEFAULT_TEN + "," + UPDATED_TEN);

        // Get all the thongTinBhxhList where ten equals to UPDATED_TEN
        defaultThongTinBhxhShouldNotBeFound("ten.in=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllThongTinBhxhsByTenIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinBhxhRepository.saveAndFlush(thongTinBhxh);

        // Get all the thongTinBhxhList where ten is not null
        defaultThongTinBhxhShouldBeFound("ten.specified=true");

        // Get all the thongTinBhxhList where ten is null
        defaultThongTinBhxhShouldNotBeFound("ten.specified=false");
    }
                @Test
    @Transactional
    public void getAllThongTinBhxhsByTenContainsSomething() throws Exception {
        // Initialize the database
        thongTinBhxhRepository.saveAndFlush(thongTinBhxh);

        // Get all the thongTinBhxhList where ten contains DEFAULT_TEN
        defaultThongTinBhxhShouldBeFound("ten.contains=" + DEFAULT_TEN);

        // Get all the thongTinBhxhList where ten contains UPDATED_TEN
        defaultThongTinBhxhShouldNotBeFound("ten.contains=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllThongTinBhxhsByTenNotContainsSomething() throws Exception {
        // Initialize the database
        thongTinBhxhRepository.saveAndFlush(thongTinBhxh);

        // Get all the thongTinBhxhList where ten does not contain DEFAULT_TEN
        defaultThongTinBhxhShouldNotBeFound("ten.doesNotContain=" + DEFAULT_TEN);

        // Get all the thongTinBhxhList where ten does not contain UPDATED_TEN
        defaultThongTinBhxhShouldBeFound("ten.doesNotContain=" + UPDATED_TEN);
    }


    @Test
    @Transactional
    public void getAllThongTinBhxhsByDonViIsEqualToSomething() throws Exception {
        // Get already existing entity
        DonVi donVi = thongTinBhxh.getDonVi();
        thongTinBhxhRepository.saveAndFlush(thongTinBhxh);
        Long donViId = donVi.getId();

        // Get all the thongTinBhxhList where donVi equals to donViId
        defaultThongTinBhxhShouldBeFound("donViId.equals=" + donViId);

        // Get all the thongTinBhxhList where donVi equals to donViId + 1
        defaultThongTinBhxhShouldNotBeFound("donViId.equals=" + (donViId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultThongTinBhxhShouldBeFound(String filter) throws Exception {
        restThongTinBhxhMockMvc.perform(get("/api/thong-tin-bhxhs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(thongTinBhxh.getId().intValue())))
            .andExpect(jsonPath("$.[*].dvtt").value(hasItem(DEFAULT_DVTT)))
            .andExpect(jsonPath("$.[*].enabled").value(hasItem(DEFAULT_ENABLED.booleanValue())))
            .andExpect(jsonPath("$.[*].ngayApDungBhyt").value(hasItem(DEFAULT_NGAY_AP_DUNG_BHYT.toString())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)));

        // Check, that the count call also returns 1
        restThongTinBhxhMockMvc.perform(get("/api/thong-tin-bhxhs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultThongTinBhxhShouldNotBeFound(String filter) throws Exception {
        restThongTinBhxhMockMvc.perform(get("/api/thong-tin-bhxhs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restThongTinBhxhMockMvc.perform(get("/api/thong-tin-bhxhs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingThongTinBhxh() throws Exception {
        // Get the thongTinBhxh
        restThongTinBhxhMockMvc.perform(get("/api/thong-tin-bhxhs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateThongTinBhxh() throws Exception {
        // Initialize the database
        thongTinBhxhRepository.saveAndFlush(thongTinBhxh);

        int databaseSizeBeforeUpdate = thongTinBhxhRepository.findAll().size();

        // Update the thongTinBhxh
        ThongTinBhxh updatedThongTinBhxh = thongTinBhxhRepository.findById(thongTinBhxh.getId()).get();
        // Disconnect from session so that the updates on updatedThongTinBhxh are not directly saved in db
        em.detach(updatedThongTinBhxh);
        updatedThongTinBhxh
            .dvtt(UPDATED_DVTT)
            .enabled(UPDATED_ENABLED)
            .ngayApDungBhyt(UPDATED_NGAY_AP_DUNG_BHYT)
            .ten(UPDATED_TEN);
        ThongTinBhxhDTO thongTinBhxhDTO = thongTinBhxhMapper.toDto(updatedThongTinBhxh);

        restThongTinBhxhMockMvc.perform(put("/api/thong-tin-bhxhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(thongTinBhxhDTO)))
            .andExpect(status().isOk());

        // Validate the ThongTinBhxh in the database
        List<ThongTinBhxh> thongTinBhxhList = thongTinBhxhRepository.findAll();
        assertThat(thongTinBhxhList).hasSize(databaseSizeBeforeUpdate);
        ThongTinBhxh testThongTinBhxh = thongTinBhxhList.get(thongTinBhxhList.size() - 1);
        assertThat(testThongTinBhxh.getDvtt()).isEqualTo(UPDATED_DVTT);
        assertThat(testThongTinBhxh.isEnabled()).isEqualTo(UPDATED_ENABLED);
        assertThat(testThongTinBhxh.getNgayApDungBhyt()).isEqualTo(UPDATED_NGAY_AP_DUNG_BHYT);
        assertThat(testThongTinBhxh.getTen()).isEqualTo(UPDATED_TEN);
    }

    @Test
    @Transactional
    public void updateNonExistingThongTinBhxh() throws Exception {
        int databaseSizeBeforeUpdate = thongTinBhxhRepository.findAll().size();

        // Create the ThongTinBhxh
        ThongTinBhxhDTO thongTinBhxhDTO = thongTinBhxhMapper.toDto(thongTinBhxh);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restThongTinBhxhMockMvc.perform(put("/api/thong-tin-bhxhs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(thongTinBhxhDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ThongTinBhxh in the database
        List<ThongTinBhxh> thongTinBhxhList = thongTinBhxhRepository.findAll();
        assertThat(thongTinBhxhList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteThongTinBhxh() throws Exception {
        // Initialize the database
        thongTinBhxhRepository.saveAndFlush(thongTinBhxh);

        int databaseSizeBeforeDelete = thongTinBhxhRepository.findAll().size();

        // Delete the thongTinBhxh
        restThongTinBhxhMockMvc.perform(delete("/api/thong-tin-bhxhs/{id}", thongTinBhxh.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ThongTinBhxh> thongTinBhxhList = thongTinBhxhRepository.findAll();
        assertThat(thongTinBhxhList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

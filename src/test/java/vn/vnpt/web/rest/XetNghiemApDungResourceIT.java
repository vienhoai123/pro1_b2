package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.XetNghiemApDung;
import vn.vnpt.domain.DotGiaDichVuBhxh;
import vn.vnpt.domain.XetNghiem;
import vn.vnpt.repository.XetNghiemApDungRepository;
import vn.vnpt.service.XetNghiemApDungService;
import vn.vnpt.service.dto.XetNghiemApDungDTO;
import vn.vnpt.service.mapper.XetNghiemApDungMapper;
import vn.vnpt.service.dto.XetNghiemApDungCriteria;
import vn.vnpt.service.XetNghiemApDungQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link XetNghiemApDungResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class XetNghiemApDungResourceIT {

    private static final String DEFAULT_MA_BAO_CAO_BHXH = "AAAAAAAAAA";
    private static final String UPDATED_MA_BAO_CAO_BHXH = "BBBBBBBBBB";

    private static final String DEFAULT_MA_BAO_CAO_BHYT = "AAAAAAAAAA";
    private static final String UPDATED_MA_BAO_CAO_BHYT = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_NGAY_AP_DUNG = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_AP_DUNG = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_NGAY_AP_DUNG = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_SO_CONG_VAN_BHXH = "AAAAAAAAAA";
    private static final String UPDATED_SO_CONG_VAN_BHXH = "BBBBBBBBBB";

    private static final String DEFAULT_SO_QUYET_DINH = "AAAAAAAAAA";
    private static final String UPDATED_SO_QUYET_DINH = "BBBBBBBBBB";

    private static final String DEFAULT_TEN_BAO_CAO_BHXH = "AAAAAAAAAA";
    private static final String UPDATED_TEN_BAO_CAO_BHXH = "BBBBBBBBBB";

    private static final String DEFAULT_TEN_DICH_VU_KHONG_BHYT = "AAAAAAAAAA";
    private static final String UPDATED_TEN_DICH_VU_KHONG_BHYT = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_TIEN_BENH_NHAN_CHI = new BigDecimal(1);
    private static final BigDecimal UPDATED_TIEN_BENH_NHAN_CHI = new BigDecimal(2);
    private static final BigDecimal SMALLER_TIEN_BENH_NHAN_CHI = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_TIEN_BHXH_CHI = new BigDecimal(1);
    private static final BigDecimal UPDATED_TIEN_BHXH_CHI = new BigDecimal(2);
    private static final BigDecimal SMALLER_TIEN_BHXH_CHI = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_TIEN_NGOAI_BHYT = new BigDecimal(1);
    private static final BigDecimal UPDATED_TIEN_NGOAI_BHYT = new BigDecimal(2);
    private static final BigDecimal SMALLER_TIEN_NGOAI_BHYT = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_TONG_TIEN_THANH_TOAN = new BigDecimal(1);
    private static final BigDecimal UPDATED_TONG_TIEN_THANH_TOAN = new BigDecimal(2);
    private static final BigDecimal SMALLER_TONG_TIEN_THANH_TOAN = new BigDecimal(1 - 1);

    private static final Integer DEFAULT_TY_LE_BHXH_THANH_TOAN = 1;
    private static final Integer UPDATED_TY_LE_BHXH_THANH_TOAN = 2;
    private static final Integer SMALLER_TY_LE_BHXH_THANH_TOAN = 1 - 1;

    private static final BigDecimal DEFAULT_GIA_BHYT = new BigDecimal(1);
    private static final BigDecimal UPDATED_GIA_BHYT = new BigDecimal(2);
    private static final BigDecimal SMALLER_GIA_BHYT = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_GIA_KHONG_BHYT = new BigDecimal(1);
    private static final BigDecimal UPDATED_GIA_KHONG_BHYT = new BigDecimal(2);
    private static final BigDecimal SMALLER_GIA_KHONG_BHYT = new BigDecimal(1 - 1);

    private static final Boolean DEFAULT_DOI_TUONG_DAC_BIET = false;
    private static final Boolean UPDATED_DOI_TUONG_DAC_BIET = true;

    private static final Integer DEFAULT_NGUON_CHI = 1;
    private static final Integer UPDATED_NGUON_CHI = 2;
    private static final Integer SMALLER_NGUON_CHI = 1 - 1;

    private static final Boolean DEFAULT_ENABLE = false;
    private static final Boolean UPDATED_ENABLE = true;

    @Autowired
    private XetNghiemApDungRepository xetNghiemApDungRepository;

    @Autowired
    private XetNghiemApDungMapper xetNghiemApDungMapper;

    @Autowired
    private XetNghiemApDungService xetNghiemApDungService;

    @Autowired
    private XetNghiemApDungQueryService xetNghiemApDungQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restXetNghiemApDungMockMvc;

    private XetNghiemApDung xetNghiemApDung;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static XetNghiemApDung createEntity(EntityManager em) {
        XetNghiemApDung xetNghiemApDung = new XetNghiemApDung()
            .maBaoCaoBhxh(DEFAULT_MA_BAO_CAO_BHXH)
            .maBaoCaoBhyt(DEFAULT_MA_BAO_CAO_BHYT)
            .ngayApDung(DEFAULT_NGAY_AP_DUNG)
            .soCongVanBhxh(DEFAULT_SO_CONG_VAN_BHXH)
            .soQuyetDinh(DEFAULT_SO_QUYET_DINH)
            .tenBaoCaoBhxh(DEFAULT_TEN_BAO_CAO_BHXH)
            .tenDichVuKhongBhyt(DEFAULT_TEN_DICH_VU_KHONG_BHYT)
            .tienBenhNhanChi(DEFAULT_TIEN_BENH_NHAN_CHI)
            .tienBhxhChi(DEFAULT_TIEN_BHXH_CHI)
            .tienNgoaiBhyt(DEFAULT_TIEN_NGOAI_BHYT)
            .tongTienThanhToan(DEFAULT_TONG_TIEN_THANH_TOAN)
            .tyLeBhxhThanhToan(DEFAULT_TY_LE_BHXH_THANH_TOAN)
            .giaBhyt(DEFAULT_GIA_BHYT)
            .giaKhongBhyt(DEFAULT_GIA_KHONG_BHYT)
            .doiTuongDacBiet(DEFAULT_DOI_TUONG_DAC_BIET)
            .nguonChi(DEFAULT_NGUON_CHI)
            .enable(DEFAULT_ENABLE);
        // Add required entity
        DotGiaDichVuBhxh dotGiaDichVuBhxh;
        if (TestUtil.findAll(em, DotGiaDichVuBhxh.class).isEmpty()) {
            dotGiaDichVuBhxh = DotGiaDichVuBhxhResourceIT.createEntity(em);
            em.persist(dotGiaDichVuBhxh);
            em.flush();
        } else {
            dotGiaDichVuBhxh = TestUtil.findAll(em, DotGiaDichVuBhxh.class).get(0);
        }
        xetNghiemApDung.setDotGia(dotGiaDichVuBhxh);
        // Add required entity
        XetNghiem xetNghiem;
        if (TestUtil.findAll(em, XetNghiem.class).isEmpty()) {
            xetNghiem = XetNghiemResourceIT.createEntity(em);
            em.persist(xetNghiem);
            em.flush();
        } else {
            xetNghiem = TestUtil.findAll(em, XetNghiem.class).get(0);
        }
        xetNghiemApDung.setXN(xetNghiem);
        return xetNghiemApDung;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static XetNghiemApDung createUpdatedEntity(EntityManager em) {
        XetNghiemApDung xetNghiemApDung = new XetNghiemApDung()
            .maBaoCaoBhxh(UPDATED_MA_BAO_CAO_BHXH)
            .maBaoCaoBhyt(UPDATED_MA_BAO_CAO_BHYT)
            .ngayApDung(UPDATED_NGAY_AP_DUNG)
            .soCongVanBhxh(UPDATED_SO_CONG_VAN_BHXH)
            .soQuyetDinh(UPDATED_SO_QUYET_DINH)
            .tenBaoCaoBhxh(UPDATED_TEN_BAO_CAO_BHXH)
            .tenDichVuKhongBhyt(UPDATED_TEN_DICH_VU_KHONG_BHYT)
            .tienBenhNhanChi(UPDATED_TIEN_BENH_NHAN_CHI)
            .tienBhxhChi(UPDATED_TIEN_BHXH_CHI)
            .tienNgoaiBhyt(UPDATED_TIEN_NGOAI_BHYT)
            .tongTienThanhToan(UPDATED_TONG_TIEN_THANH_TOAN)
            .tyLeBhxhThanhToan(UPDATED_TY_LE_BHXH_THANH_TOAN)
            .giaBhyt(UPDATED_GIA_BHYT)
            .giaKhongBhyt(UPDATED_GIA_KHONG_BHYT)
            .doiTuongDacBiet(UPDATED_DOI_TUONG_DAC_BIET)
            .nguonChi(UPDATED_NGUON_CHI)
            .enable(UPDATED_ENABLE);
        // Add required entity
        DotGiaDichVuBhxh dotGiaDichVuBhxh;
        if (TestUtil.findAll(em, DotGiaDichVuBhxh.class).isEmpty()) {
            dotGiaDichVuBhxh = DotGiaDichVuBhxhResourceIT.createUpdatedEntity(em);
            em.persist(dotGiaDichVuBhxh);
            em.flush();
        } else {
            dotGiaDichVuBhxh = TestUtil.findAll(em, DotGiaDichVuBhxh.class).get(0);
        }
        xetNghiemApDung.setDotGia(dotGiaDichVuBhxh);
        // Add required entity
        XetNghiem xetNghiem;
        if (TestUtil.findAll(em, XetNghiem.class).isEmpty()) {
            xetNghiem = XetNghiemResourceIT.createUpdatedEntity(em);
            em.persist(xetNghiem);
            em.flush();
        } else {
            xetNghiem = TestUtil.findAll(em, XetNghiem.class).get(0);
        }
        xetNghiemApDung.setXN(xetNghiem);
        return xetNghiemApDung;
    }

    @BeforeEach
    public void initTest() {
        xetNghiemApDung = createEntity(em);
    }

    @Test
    @Transactional
    public void createXetNghiemApDung() throws Exception {
        int databaseSizeBeforeCreate = xetNghiemApDungRepository.findAll().size();

        // Create the XetNghiemApDung
        XetNghiemApDungDTO xetNghiemApDungDTO = xetNghiemApDungMapper.toDto(xetNghiemApDung);
        restXetNghiemApDungMockMvc.perform(post("/api/xet-nghiem-ap-dungs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(xetNghiemApDungDTO)))
            .andExpect(status().isCreated());

        // Validate the XetNghiemApDung in the database
        List<XetNghiemApDung> xetNghiemApDungList = xetNghiemApDungRepository.findAll();
        assertThat(xetNghiemApDungList).hasSize(databaseSizeBeforeCreate + 1);
        XetNghiemApDung testXetNghiemApDung = xetNghiemApDungList.get(xetNghiemApDungList.size() - 1);
        assertThat(testXetNghiemApDung.getMaBaoCaoBhxh()).isEqualTo(DEFAULT_MA_BAO_CAO_BHXH);
        assertThat(testXetNghiemApDung.getMaBaoCaoBhyt()).isEqualTo(DEFAULT_MA_BAO_CAO_BHYT);
        assertThat(testXetNghiemApDung.getNgayApDung()).isEqualTo(DEFAULT_NGAY_AP_DUNG);
        assertThat(testXetNghiemApDung.getSoCongVanBhxh()).isEqualTo(DEFAULT_SO_CONG_VAN_BHXH);
        assertThat(testXetNghiemApDung.getSoQuyetDinh()).isEqualTo(DEFAULT_SO_QUYET_DINH);
        assertThat(testXetNghiemApDung.getTenBaoCaoBhxh()).isEqualTo(DEFAULT_TEN_BAO_CAO_BHXH);
        assertThat(testXetNghiemApDung.getTenDichVuKhongBhyt()).isEqualTo(DEFAULT_TEN_DICH_VU_KHONG_BHYT);
        assertThat(testXetNghiemApDung.getTienBenhNhanChi()).isEqualTo(DEFAULT_TIEN_BENH_NHAN_CHI);
        assertThat(testXetNghiemApDung.getTienBhxhChi()).isEqualTo(DEFAULT_TIEN_BHXH_CHI);
        assertThat(testXetNghiemApDung.getTienNgoaiBhyt()).isEqualTo(DEFAULT_TIEN_NGOAI_BHYT);
        assertThat(testXetNghiemApDung.getTongTienThanhToan()).isEqualTo(DEFAULT_TONG_TIEN_THANH_TOAN);
        assertThat(testXetNghiemApDung.getTyLeBhxhThanhToan()).isEqualTo(DEFAULT_TY_LE_BHXH_THANH_TOAN);
        assertThat(testXetNghiemApDung.getGiaBhyt()).isEqualTo(DEFAULT_GIA_BHYT);
        assertThat(testXetNghiemApDung.getGiaKhongBhyt()).isEqualTo(DEFAULT_GIA_KHONG_BHYT);
        assertThat(testXetNghiemApDung.isDoiTuongDacBiet()).isEqualTo(DEFAULT_DOI_TUONG_DAC_BIET);
        assertThat(testXetNghiemApDung.getNguonChi()).isEqualTo(DEFAULT_NGUON_CHI);
        assertThat(testXetNghiemApDung.isEnable()).isEqualTo(DEFAULT_ENABLE);
    }

    @Test
    @Transactional
    public void createXetNghiemApDungWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = xetNghiemApDungRepository.findAll().size();

        // Create the XetNghiemApDung with an existing ID
        xetNghiemApDung.setId(1L);
        XetNghiemApDungDTO xetNghiemApDungDTO = xetNghiemApDungMapper.toDto(xetNghiemApDung);

        // An entity with an existing ID cannot be created, so this API call must fail
        restXetNghiemApDungMockMvc.perform(post("/api/xet-nghiem-ap-dungs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(xetNghiemApDungDTO)))
            .andExpect(status().isBadRequest());

        // Validate the XetNghiemApDung in the database
        List<XetNghiemApDung> xetNghiemApDungList = xetNghiemApDungRepository.findAll();
        assertThat(xetNghiemApDungList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNgayApDungIsRequired() throws Exception {
        int databaseSizeBeforeTest = xetNghiemApDungRepository.findAll().size();
        // set the field null
        xetNghiemApDung.setNgayApDung(null);

        // Create the XetNghiemApDung, which fails.
        XetNghiemApDungDTO xetNghiemApDungDTO = xetNghiemApDungMapper.toDto(xetNghiemApDung);

        restXetNghiemApDungMockMvc.perform(post("/api/xet-nghiem-ap-dungs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(xetNghiemApDungDTO)))
            .andExpect(status().isBadRequest());

        List<XetNghiemApDung> xetNghiemApDungList = xetNghiemApDungRepository.findAll();
        assertThat(xetNghiemApDungList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkGiaBhytIsRequired() throws Exception {
        int databaseSizeBeforeTest = xetNghiemApDungRepository.findAll().size();
        // set the field null
        xetNghiemApDung.setGiaBhyt(null);

        // Create the XetNghiemApDung, which fails.
        XetNghiemApDungDTO xetNghiemApDungDTO = xetNghiemApDungMapper.toDto(xetNghiemApDung);

        restXetNghiemApDungMockMvc.perform(post("/api/xet-nghiem-ap-dungs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(xetNghiemApDungDTO)))
            .andExpect(status().isBadRequest());

        List<XetNghiemApDung> xetNghiemApDungList = xetNghiemApDungRepository.findAll();
        assertThat(xetNghiemApDungList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkGiaKhongBhytIsRequired() throws Exception {
        int databaseSizeBeforeTest = xetNghiemApDungRepository.findAll().size();
        // set the field null
        xetNghiemApDung.setGiaKhongBhyt(null);

        // Create the XetNghiemApDung, which fails.
        XetNghiemApDungDTO xetNghiemApDungDTO = xetNghiemApDungMapper.toDto(xetNghiemApDung);

        restXetNghiemApDungMockMvc.perform(post("/api/xet-nghiem-ap-dungs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(xetNghiemApDungDTO)))
            .andExpect(status().isBadRequest());

        List<XetNghiemApDung> xetNghiemApDungList = xetNghiemApDungRepository.findAll();
        assertThat(xetNghiemApDungList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungs() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList
        restXetNghiemApDungMockMvc.perform(get("/api/xet-nghiem-ap-dungs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(xetNghiemApDung.getId().intValue())))
            .andExpect(jsonPath("$.[*].maBaoCaoBhxh").value(hasItem(DEFAULT_MA_BAO_CAO_BHXH)))
            .andExpect(jsonPath("$.[*].maBaoCaoBhyt").value(hasItem(DEFAULT_MA_BAO_CAO_BHYT)))
            .andExpect(jsonPath("$.[*].ngayApDung").value(hasItem(DEFAULT_NGAY_AP_DUNG.toString())))
            .andExpect(jsonPath("$.[*].soCongVanBhxh").value(hasItem(DEFAULT_SO_CONG_VAN_BHXH)))
            .andExpect(jsonPath("$.[*].soQuyetDinh").value(hasItem(DEFAULT_SO_QUYET_DINH)))
            .andExpect(jsonPath("$.[*].tenBaoCaoBhxh").value(hasItem(DEFAULT_TEN_BAO_CAO_BHXH)))
            .andExpect(jsonPath("$.[*].tenDichVuKhongBhyt").value(hasItem(DEFAULT_TEN_DICH_VU_KHONG_BHYT)))
            .andExpect(jsonPath("$.[*].tienBenhNhanChi").value(hasItem(DEFAULT_TIEN_BENH_NHAN_CHI.intValue())))
            .andExpect(jsonPath("$.[*].tienBhxhChi").value(hasItem(DEFAULT_TIEN_BHXH_CHI.intValue())))
            .andExpect(jsonPath("$.[*].tienNgoaiBhyt").value(hasItem(DEFAULT_TIEN_NGOAI_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].tongTienThanhToan").value(hasItem(DEFAULT_TONG_TIEN_THANH_TOAN.intValue())))
            .andExpect(jsonPath("$.[*].tyLeBhxhThanhToan").value(hasItem(DEFAULT_TY_LE_BHXH_THANH_TOAN)))
            .andExpect(jsonPath("$.[*].giaBhyt").value(hasItem(DEFAULT_GIA_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].giaKhongBhyt").value(hasItem(DEFAULT_GIA_KHONG_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].doiTuongDacBiet").value(hasItem(DEFAULT_DOI_TUONG_DAC_BIET.booleanValue())))
            .andExpect(jsonPath("$.[*].nguonChi").value(hasItem(DEFAULT_NGUON_CHI)))
            .andExpect(jsonPath("$.[*].enable").value(hasItem(DEFAULT_ENABLE.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getXetNghiemApDung() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get the xetNghiemApDung
        restXetNghiemApDungMockMvc.perform(get("/api/xet-nghiem-ap-dungs/{id}", xetNghiemApDung.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(xetNghiemApDung.getId().intValue()))
            .andExpect(jsonPath("$.maBaoCaoBhxh").value(DEFAULT_MA_BAO_CAO_BHXH))
            .andExpect(jsonPath("$.maBaoCaoBhyt").value(DEFAULT_MA_BAO_CAO_BHYT))
            .andExpect(jsonPath("$.ngayApDung").value(DEFAULT_NGAY_AP_DUNG.toString()))
            .andExpect(jsonPath("$.soCongVanBhxh").value(DEFAULT_SO_CONG_VAN_BHXH))
            .andExpect(jsonPath("$.soQuyetDinh").value(DEFAULT_SO_QUYET_DINH))
            .andExpect(jsonPath("$.tenBaoCaoBhxh").value(DEFAULT_TEN_BAO_CAO_BHXH))
            .andExpect(jsonPath("$.tenDichVuKhongBhyt").value(DEFAULT_TEN_DICH_VU_KHONG_BHYT))
            .andExpect(jsonPath("$.tienBenhNhanChi").value(DEFAULT_TIEN_BENH_NHAN_CHI.intValue()))
            .andExpect(jsonPath("$.tienBhxhChi").value(DEFAULT_TIEN_BHXH_CHI.intValue()))
            .andExpect(jsonPath("$.tienNgoaiBhyt").value(DEFAULT_TIEN_NGOAI_BHYT.intValue()))
            .andExpect(jsonPath("$.tongTienThanhToan").value(DEFAULT_TONG_TIEN_THANH_TOAN.intValue()))
            .andExpect(jsonPath("$.tyLeBhxhThanhToan").value(DEFAULT_TY_LE_BHXH_THANH_TOAN))
            .andExpect(jsonPath("$.giaBhyt").value(DEFAULT_GIA_BHYT.intValue()))
            .andExpect(jsonPath("$.giaKhongBhyt").value(DEFAULT_GIA_KHONG_BHYT.intValue()))
            .andExpect(jsonPath("$.doiTuongDacBiet").value(DEFAULT_DOI_TUONG_DAC_BIET.booleanValue()))
            .andExpect(jsonPath("$.nguonChi").value(DEFAULT_NGUON_CHI))
            .andExpect(jsonPath("$.enable").value(DEFAULT_ENABLE.booleanValue()));
    }


    @Test
    @Transactional
    public void getXetNghiemApDungsByIdFiltering() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        Long id = xetNghiemApDung.getId();

        defaultXetNghiemApDungShouldBeFound("id.equals=" + id);
        defaultXetNghiemApDungShouldNotBeFound("id.notEquals=" + id);

        defaultXetNghiemApDungShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultXetNghiemApDungShouldNotBeFound("id.greaterThan=" + id);

        defaultXetNghiemApDungShouldBeFound("id.lessThanOrEqual=" + id);
        defaultXetNghiemApDungShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllXetNghiemApDungsByMaBaoCaoBhxhIsEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where maBaoCaoBhxh equals to DEFAULT_MA_BAO_CAO_BHXH
        defaultXetNghiemApDungShouldBeFound("maBaoCaoBhxh.equals=" + DEFAULT_MA_BAO_CAO_BHXH);

        // Get all the xetNghiemApDungList where maBaoCaoBhxh equals to UPDATED_MA_BAO_CAO_BHXH
        defaultXetNghiemApDungShouldNotBeFound("maBaoCaoBhxh.equals=" + UPDATED_MA_BAO_CAO_BHXH);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByMaBaoCaoBhxhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where maBaoCaoBhxh not equals to DEFAULT_MA_BAO_CAO_BHXH
        defaultXetNghiemApDungShouldNotBeFound("maBaoCaoBhxh.notEquals=" + DEFAULT_MA_BAO_CAO_BHXH);

        // Get all the xetNghiemApDungList where maBaoCaoBhxh not equals to UPDATED_MA_BAO_CAO_BHXH
        defaultXetNghiemApDungShouldBeFound("maBaoCaoBhxh.notEquals=" + UPDATED_MA_BAO_CAO_BHXH);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByMaBaoCaoBhxhIsInShouldWork() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where maBaoCaoBhxh in DEFAULT_MA_BAO_CAO_BHXH or UPDATED_MA_BAO_CAO_BHXH
        defaultXetNghiemApDungShouldBeFound("maBaoCaoBhxh.in=" + DEFAULT_MA_BAO_CAO_BHXH + "," + UPDATED_MA_BAO_CAO_BHXH);

        // Get all the xetNghiemApDungList where maBaoCaoBhxh equals to UPDATED_MA_BAO_CAO_BHXH
        defaultXetNghiemApDungShouldNotBeFound("maBaoCaoBhxh.in=" + UPDATED_MA_BAO_CAO_BHXH);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByMaBaoCaoBhxhIsNullOrNotNull() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where maBaoCaoBhxh is not null
        defaultXetNghiemApDungShouldBeFound("maBaoCaoBhxh.specified=true");

        // Get all the xetNghiemApDungList where maBaoCaoBhxh is null
        defaultXetNghiemApDungShouldNotBeFound("maBaoCaoBhxh.specified=false");
    }
                @Test
    @Transactional
    public void getAllXetNghiemApDungsByMaBaoCaoBhxhContainsSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where maBaoCaoBhxh contains DEFAULT_MA_BAO_CAO_BHXH
        defaultXetNghiemApDungShouldBeFound("maBaoCaoBhxh.contains=" + DEFAULT_MA_BAO_CAO_BHXH);

        // Get all the xetNghiemApDungList where maBaoCaoBhxh contains UPDATED_MA_BAO_CAO_BHXH
        defaultXetNghiemApDungShouldNotBeFound("maBaoCaoBhxh.contains=" + UPDATED_MA_BAO_CAO_BHXH);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByMaBaoCaoBhxhNotContainsSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where maBaoCaoBhxh does not contain DEFAULT_MA_BAO_CAO_BHXH
        defaultXetNghiemApDungShouldNotBeFound("maBaoCaoBhxh.doesNotContain=" + DEFAULT_MA_BAO_CAO_BHXH);

        // Get all the xetNghiemApDungList where maBaoCaoBhxh does not contain UPDATED_MA_BAO_CAO_BHXH
        defaultXetNghiemApDungShouldBeFound("maBaoCaoBhxh.doesNotContain=" + UPDATED_MA_BAO_CAO_BHXH);
    }


    @Test
    @Transactional
    public void getAllXetNghiemApDungsByMaBaoCaoBhytIsEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where maBaoCaoBhyt equals to DEFAULT_MA_BAO_CAO_BHYT
        defaultXetNghiemApDungShouldBeFound("maBaoCaoBhyt.equals=" + DEFAULT_MA_BAO_CAO_BHYT);

        // Get all the xetNghiemApDungList where maBaoCaoBhyt equals to UPDATED_MA_BAO_CAO_BHYT
        defaultXetNghiemApDungShouldNotBeFound("maBaoCaoBhyt.equals=" + UPDATED_MA_BAO_CAO_BHYT);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByMaBaoCaoBhytIsNotEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where maBaoCaoBhyt not equals to DEFAULT_MA_BAO_CAO_BHYT
        defaultXetNghiemApDungShouldNotBeFound("maBaoCaoBhyt.notEquals=" + DEFAULT_MA_BAO_CAO_BHYT);

        // Get all the xetNghiemApDungList where maBaoCaoBhyt not equals to UPDATED_MA_BAO_CAO_BHYT
        defaultXetNghiemApDungShouldBeFound("maBaoCaoBhyt.notEquals=" + UPDATED_MA_BAO_CAO_BHYT);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByMaBaoCaoBhytIsInShouldWork() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where maBaoCaoBhyt in DEFAULT_MA_BAO_CAO_BHYT or UPDATED_MA_BAO_CAO_BHYT
        defaultXetNghiemApDungShouldBeFound("maBaoCaoBhyt.in=" + DEFAULT_MA_BAO_CAO_BHYT + "," + UPDATED_MA_BAO_CAO_BHYT);

        // Get all the xetNghiemApDungList where maBaoCaoBhyt equals to UPDATED_MA_BAO_CAO_BHYT
        defaultXetNghiemApDungShouldNotBeFound("maBaoCaoBhyt.in=" + UPDATED_MA_BAO_CAO_BHYT);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByMaBaoCaoBhytIsNullOrNotNull() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where maBaoCaoBhyt is not null
        defaultXetNghiemApDungShouldBeFound("maBaoCaoBhyt.specified=true");

        // Get all the xetNghiemApDungList where maBaoCaoBhyt is null
        defaultXetNghiemApDungShouldNotBeFound("maBaoCaoBhyt.specified=false");
    }
                @Test
    @Transactional
    public void getAllXetNghiemApDungsByMaBaoCaoBhytContainsSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where maBaoCaoBhyt contains DEFAULT_MA_BAO_CAO_BHYT
        defaultXetNghiemApDungShouldBeFound("maBaoCaoBhyt.contains=" + DEFAULT_MA_BAO_CAO_BHYT);

        // Get all the xetNghiemApDungList where maBaoCaoBhyt contains UPDATED_MA_BAO_CAO_BHYT
        defaultXetNghiemApDungShouldNotBeFound("maBaoCaoBhyt.contains=" + UPDATED_MA_BAO_CAO_BHYT);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByMaBaoCaoBhytNotContainsSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where maBaoCaoBhyt does not contain DEFAULT_MA_BAO_CAO_BHYT
        defaultXetNghiemApDungShouldNotBeFound("maBaoCaoBhyt.doesNotContain=" + DEFAULT_MA_BAO_CAO_BHYT);

        // Get all the xetNghiemApDungList where maBaoCaoBhyt does not contain UPDATED_MA_BAO_CAO_BHYT
        defaultXetNghiemApDungShouldBeFound("maBaoCaoBhyt.doesNotContain=" + UPDATED_MA_BAO_CAO_BHYT);
    }


    @Test
    @Transactional
    public void getAllXetNghiemApDungsByNgayApDungIsEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where ngayApDung equals to DEFAULT_NGAY_AP_DUNG
        defaultXetNghiemApDungShouldBeFound("ngayApDung.equals=" + DEFAULT_NGAY_AP_DUNG);

        // Get all the xetNghiemApDungList where ngayApDung equals to UPDATED_NGAY_AP_DUNG
        defaultXetNghiemApDungShouldNotBeFound("ngayApDung.equals=" + UPDATED_NGAY_AP_DUNG);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByNgayApDungIsNotEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where ngayApDung not equals to DEFAULT_NGAY_AP_DUNG
        defaultXetNghiemApDungShouldNotBeFound("ngayApDung.notEquals=" + DEFAULT_NGAY_AP_DUNG);

        // Get all the xetNghiemApDungList where ngayApDung not equals to UPDATED_NGAY_AP_DUNG
        defaultXetNghiemApDungShouldBeFound("ngayApDung.notEquals=" + UPDATED_NGAY_AP_DUNG);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByNgayApDungIsInShouldWork() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where ngayApDung in DEFAULT_NGAY_AP_DUNG or UPDATED_NGAY_AP_DUNG
        defaultXetNghiemApDungShouldBeFound("ngayApDung.in=" + DEFAULT_NGAY_AP_DUNG + "," + UPDATED_NGAY_AP_DUNG);

        // Get all the xetNghiemApDungList where ngayApDung equals to UPDATED_NGAY_AP_DUNG
        defaultXetNghiemApDungShouldNotBeFound("ngayApDung.in=" + UPDATED_NGAY_AP_DUNG);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByNgayApDungIsNullOrNotNull() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where ngayApDung is not null
        defaultXetNghiemApDungShouldBeFound("ngayApDung.specified=true");

        // Get all the xetNghiemApDungList where ngayApDung is null
        defaultXetNghiemApDungShouldNotBeFound("ngayApDung.specified=false");
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByNgayApDungIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where ngayApDung is greater than or equal to DEFAULT_NGAY_AP_DUNG
        defaultXetNghiemApDungShouldBeFound("ngayApDung.greaterThanOrEqual=" + DEFAULT_NGAY_AP_DUNG);

        // Get all the xetNghiemApDungList where ngayApDung is greater than or equal to UPDATED_NGAY_AP_DUNG
        defaultXetNghiemApDungShouldNotBeFound("ngayApDung.greaterThanOrEqual=" + UPDATED_NGAY_AP_DUNG);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByNgayApDungIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where ngayApDung is less than or equal to DEFAULT_NGAY_AP_DUNG
        defaultXetNghiemApDungShouldBeFound("ngayApDung.lessThanOrEqual=" + DEFAULT_NGAY_AP_DUNG);

        // Get all the xetNghiemApDungList where ngayApDung is less than or equal to SMALLER_NGAY_AP_DUNG
        defaultXetNghiemApDungShouldNotBeFound("ngayApDung.lessThanOrEqual=" + SMALLER_NGAY_AP_DUNG);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByNgayApDungIsLessThanSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where ngayApDung is less than DEFAULT_NGAY_AP_DUNG
        defaultXetNghiemApDungShouldNotBeFound("ngayApDung.lessThan=" + DEFAULT_NGAY_AP_DUNG);

        // Get all the xetNghiemApDungList where ngayApDung is less than UPDATED_NGAY_AP_DUNG
        defaultXetNghiemApDungShouldBeFound("ngayApDung.lessThan=" + UPDATED_NGAY_AP_DUNG);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByNgayApDungIsGreaterThanSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where ngayApDung is greater than DEFAULT_NGAY_AP_DUNG
        defaultXetNghiemApDungShouldNotBeFound("ngayApDung.greaterThan=" + DEFAULT_NGAY_AP_DUNG);

        // Get all the xetNghiemApDungList where ngayApDung is greater than SMALLER_NGAY_AP_DUNG
        defaultXetNghiemApDungShouldBeFound("ngayApDung.greaterThan=" + SMALLER_NGAY_AP_DUNG);
    }


    @Test
    @Transactional
    public void getAllXetNghiemApDungsBySoCongVanBhxhIsEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where soCongVanBhxh equals to DEFAULT_SO_CONG_VAN_BHXH
        defaultXetNghiemApDungShouldBeFound("soCongVanBhxh.equals=" + DEFAULT_SO_CONG_VAN_BHXH);

        // Get all the xetNghiemApDungList where soCongVanBhxh equals to UPDATED_SO_CONG_VAN_BHXH
        defaultXetNghiemApDungShouldNotBeFound("soCongVanBhxh.equals=" + UPDATED_SO_CONG_VAN_BHXH);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsBySoCongVanBhxhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where soCongVanBhxh not equals to DEFAULT_SO_CONG_VAN_BHXH
        defaultXetNghiemApDungShouldNotBeFound("soCongVanBhxh.notEquals=" + DEFAULT_SO_CONG_VAN_BHXH);

        // Get all the xetNghiemApDungList where soCongVanBhxh not equals to UPDATED_SO_CONG_VAN_BHXH
        defaultXetNghiemApDungShouldBeFound("soCongVanBhxh.notEquals=" + UPDATED_SO_CONG_VAN_BHXH);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsBySoCongVanBhxhIsInShouldWork() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where soCongVanBhxh in DEFAULT_SO_CONG_VAN_BHXH or UPDATED_SO_CONG_VAN_BHXH
        defaultXetNghiemApDungShouldBeFound("soCongVanBhxh.in=" + DEFAULT_SO_CONG_VAN_BHXH + "," + UPDATED_SO_CONG_VAN_BHXH);

        // Get all the xetNghiemApDungList where soCongVanBhxh equals to UPDATED_SO_CONG_VAN_BHXH
        defaultXetNghiemApDungShouldNotBeFound("soCongVanBhxh.in=" + UPDATED_SO_CONG_VAN_BHXH);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsBySoCongVanBhxhIsNullOrNotNull() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where soCongVanBhxh is not null
        defaultXetNghiemApDungShouldBeFound("soCongVanBhxh.specified=true");

        // Get all the xetNghiemApDungList where soCongVanBhxh is null
        defaultXetNghiemApDungShouldNotBeFound("soCongVanBhxh.specified=false");
    }
                @Test
    @Transactional
    public void getAllXetNghiemApDungsBySoCongVanBhxhContainsSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where soCongVanBhxh contains DEFAULT_SO_CONG_VAN_BHXH
        defaultXetNghiemApDungShouldBeFound("soCongVanBhxh.contains=" + DEFAULT_SO_CONG_VAN_BHXH);

        // Get all the xetNghiemApDungList where soCongVanBhxh contains UPDATED_SO_CONG_VAN_BHXH
        defaultXetNghiemApDungShouldNotBeFound("soCongVanBhxh.contains=" + UPDATED_SO_CONG_VAN_BHXH);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsBySoCongVanBhxhNotContainsSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where soCongVanBhxh does not contain DEFAULT_SO_CONG_VAN_BHXH
        defaultXetNghiemApDungShouldNotBeFound("soCongVanBhxh.doesNotContain=" + DEFAULT_SO_CONG_VAN_BHXH);

        // Get all the xetNghiemApDungList where soCongVanBhxh does not contain UPDATED_SO_CONG_VAN_BHXH
        defaultXetNghiemApDungShouldBeFound("soCongVanBhxh.doesNotContain=" + UPDATED_SO_CONG_VAN_BHXH);
    }


    @Test
    @Transactional
    public void getAllXetNghiemApDungsBySoQuyetDinhIsEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where soQuyetDinh equals to DEFAULT_SO_QUYET_DINH
        defaultXetNghiemApDungShouldBeFound("soQuyetDinh.equals=" + DEFAULT_SO_QUYET_DINH);

        // Get all the xetNghiemApDungList where soQuyetDinh equals to UPDATED_SO_QUYET_DINH
        defaultXetNghiemApDungShouldNotBeFound("soQuyetDinh.equals=" + UPDATED_SO_QUYET_DINH);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsBySoQuyetDinhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where soQuyetDinh not equals to DEFAULT_SO_QUYET_DINH
        defaultXetNghiemApDungShouldNotBeFound("soQuyetDinh.notEquals=" + DEFAULT_SO_QUYET_DINH);

        // Get all the xetNghiemApDungList where soQuyetDinh not equals to UPDATED_SO_QUYET_DINH
        defaultXetNghiemApDungShouldBeFound("soQuyetDinh.notEquals=" + UPDATED_SO_QUYET_DINH);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsBySoQuyetDinhIsInShouldWork() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where soQuyetDinh in DEFAULT_SO_QUYET_DINH or UPDATED_SO_QUYET_DINH
        defaultXetNghiemApDungShouldBeFound("soQuyetDinh.in=" + DEFAULT_SO_QUYET_DINH + "," + UPDATED_SO_QUYET_DINH);

        // Get all the xetNghiemApDungList where soQuyetDinh equals to UPDATED_SO_QUYET_DINH
        defaultXetNghiemApDungShouldNotBeFound("soQuyetDinh.in=" + UPDATED_SO_QUYET_DINH);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsBySoQuyetDinhIsNullOrNotNull() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where soQuyetDinh is not null
        defaultXetNghiemApDungShouldBeFound("soQuyetDinh.specified=true");

        // Get all the xetNghiemApDungList where soQuyetDinh is null
        defaultXetNghiemApDungShouldNotBeFound("soQuyetDinh.specified=false");
    }
                @Test
    @Transactional
    public void getAllXetNghiemApDungsBySoQuyetDinhContainsSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where soQuyetDinh contains DEFAULT_SO_QUYET_DINH
        defaultXetNghiemApDungShouldBeFound("soQuyetDinh.contains=" + DEFAULT_SO_QUYET_DINH);

        // Get all the xetNghiemApDungList where soQuyetDinh contains UPDATED_SO_QUYET_DINH
        defaultXetNghiemApDungShouldNotBeFound("soQuyetDinh.contains=" + UPDATED_SO_QUYET_DINH);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsBySoQuyetDinhNotContainsSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where soQuyetDinh does not contain DEFAULT_SO_QUYET_DINH
        defaultXetNghiemApDungShouldNotBeFound("soQuyetDinh.doesNotContain=" + DEFAULT_SO_QUYET_DINH);

        // Get all the xetNghiemApDungList where soQuyetDinh does not contain UPDATED_SO_QUYET_DINH
        defaultXetNghiemApDungShouldBeFound("soQuyetDinh.doesNotContain=" + UPDATED_SO_QUYET_DINH);
    }


    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTenBaoCaoBhxhIsEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tenBaoCaoBhxh equals to DEFAULT_TEN_BAO_CAO_BHXH
        defaultXetNghiemApDungShouldBeFound("tenBaoCaoBhxh.equals=" + DEFAULT_TEN_BAO_CAO_BHXH);

        // Get all the xetNghiemApDungList where tenBaoCaoBhxh equals to UPDATED_TEN_BAO_CAO_BHXH
        defaultXetNghiemApDungShouldNotBeFound("tenBaoCaoBhxh.equals=" + UPDATED_TEN_BAO_CAO_BHXH);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTenBaoCaoBhxhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tenBaoCaoBhxh not equals to DEFAULT_TEN_BAO_CAO_BHXH
        defaultXetNghiemApDungShouldNotBeFound("tenBaoCaoBhxh.notEquals=" + DEFAULT_TEN_BAO_CAO_BHXH);

        // Get all the xetNghiemApDungList where tenBaoCaoBhxh not equals to UPDATED_TEN_BAO_CAO_BHXH
        defaultXetNghiemApDungShouldBeFound("tenBaoCaoBhxh.notEquals=" + UPDATED_TEN_BAO_CAO_BHXH);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTenBaoCaoBhxhIsInShouldWork() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tenBaoCaoBhxh in DEFAULT_TEN_BAO_CAO_BHXH or UPDATED_TEN_BAO_CAO_BHXH
        defaultXetNghiemApDungShouldBeFound("tenBaoCaoBhxh.in=" + DEFAULT_TEN_BAO_CAO_BHXH + "," + UPDATED_TEN_BAO_CAO_BHXH);

        // Get all the xetNghiemApDungList where tenBaoCaoBhxh equals to UPDATED_TEN_BAO_CAO_BHXH
        defaultXetNghiemApDungShouldNotBeFound("tenBaoCaoBhxh.in=" + UPDATED_TEN_BAO_CAO_BHXH);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTenBaoCaoBhxhIsNullOrNotNull() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tenBaoCaoBhxh is not null
        defaultXetNghiemApDungShouldBeFound("tenBaoCaoBhxh.specified=true");

        // Get all the xetNghiemApDungList where tenBaoCaoBhxh is null
        defaultXetNghiemApDungShouldNotBeFound("tenBaoCaoBhxh.specified=false");
    }
                @Test
    @Transactional
    public void getAllXetNghiemApDungsByTenBaoCaoBhxhContainsSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tenBaoCaoBhxh contains DEFAULT_TEN_BAO_CAO_BHXH
        defaultXetNghiemApDungShouldBeFound("tenBaoCaoBhxh.contains=" + DEFAULT_TEN_BAO_CAO_BHXH);

        // Get all the xetNghiemApDungList where tenBaoCaoBhxh contains UPDATED_TEN_BAO_CAO_BHXH
        defaultXetNghiemApDungShouldNotBeFound("tenBaoCaoBhxh.contains=" + UPDATED_TEN_BAO_CAO_BHXH);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTenBaoCaoBhxhNotContainsSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tenBaoCaoBhxh does not contain DEFAULT_TEN_BAO_CAO_BHXH
        defaultXetNghiemApDungShouldNotBeFound("tenBaoCaoBhxh.doesNotContain=" + DEFAULT_TEN_BAO_CAO_BHXH);

        // Get all the xetNghiemApDungList where tenBaoCaoBhxh does not contain UPDATED_TEN_BAO_CAO_BHXH
        defaultXetNghiemApDungShouldBeFound("tenBaoCaoBhxh.doesNotContain=" + UPDATED_TEN_BAO_CAO_BHXH);
    }


    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTenDichVuKhongBhytIsEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tenDichVuKhongBhyt equals to DEFAULT_TEN_DICH_VU_KHONG_BHYT
        defaultXetNghiemApDungShouldBeFound("tenDichVuKhongBhyt.equals=" + DEFAULT_TEN_DICH_VU_KHONG_BHYT);

        // Get all the xetNghiemApDungList where tenDichVuKhongBhyt equals to UPDATED_TEN_DICH_VU_KHONG_BHYT
        defaultXetNghiemApDungShouldNotBeFound("tenDichVuKhongBhyt.equals=" + UPDATED_TEN_DICH_VU_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTenDichVuKhongBhytIsNotEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tenDichVuKhongBhyt not equals to DEFAULT_TEN_DICH_VU_KHONG_BHYT
        defaultXetNghiemApDungShouldNotBeFound("tenDichVuKhongBhyt.notEquals=" + DEFAULT_TEN_DICH_VU_KHONG_BHYT);

        // Get all the xetNghiemApDungList where tenDichVuKhongBhyt not equals to UPDATED_TEN_DICH_VU_KHONG_BHYT
        defaultXetNghiemApDungShouldBeFound("tenDichVuKhongBhyt.notEquals=" + UPDATED_TEN_DICH_VU_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTenDichVuKhongBhytIsInShouldWork() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tenDichVuKhongBhyt in DEFAULT_TEN_DICH_VU_KHONG_BHYT or UPDATED_TEN_DICH_VU_KHONG_BHYT
        defaultXetNghiemApDungShouldBeFound("tenDichVuKhongBhyt.in=" + DEFAULT_TEN_DICH_VU_KHONG_BHYT + "," + UPDATED_TEN_DICH_VU_KHONG_BHYT);

        // Get all the xetNghiemApDungList where tenDichVuKhongBhyt equals to UPDATED_TEN_DICH_VU_KHONG_BHYT
        defaultXetNghiemApDungShouldNotBeFound("tenDichVuKhongBhyt.in=" + UPDATED_TEN_DICH_VU_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTenDichVuKhongBhytIsNullOrNotNull() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tenDichVuKhongBhyt is not null
        defaultXetNghiemApDungShouldBeFound("tenDichVuKhongBhyt.specified=true");

        // Get all the xetNghiemApDungList where tenDichVuKhongBhyt is null
        defaultXetNghiemApDungShouldNotBeFound("tenDichVuKhongBhyt.specified=false");
    }
                @Test
    @Transactional
    public void getAllXetNghiemApDungsByTenDichVuKhongBhytContainsSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tenDichVuKhongBhyt contains DEFAULT_TEN_DICH_VU_KHONG_BHYT
        defaultXetNghiemApDungShouldBeFound("tenDichVuKhongBhyt.contains=" + DEFAULT_TEN_DICH_VU_KHONG_BHYT);

        // Get all the xetNghiemApDungList where tenDichVuKhongBhyt contains UPDATED_TEN_DICH_VU_KHONG_BHYT
        defaultXetNghiemApDungShouldNotBeFound("tenDichVuKhongBhyt.contains=" + UPDATED_TEN_DICH_VU_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTenDichVuKhongBhytNotContainsSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tenDichVuKhongBhyt does not contain DEFAULT_TEN_DICH_VU_KHONG_BHYT
        defaultXetNghiemApDungShouldNotBeFound("tenDichVuKhongBhyt.doesNotContain=" + DEFAULT_TEN_DICH_VU_KHONG_BHYT);

        // Get all the xetNghiemApDungList where tenDichVuKhongBhyt does not contain UPDATED_TEN_DICH_VU_KHONG_BHYT
        defaultXetNghiemApDungShouldBeFound("tenDichVuKhongBhyt.doesNotContain=" + UPDATED_TEN_DICH_VU_KHONG_BHYT);
    }


    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTienBenhNhanChiIsEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tienBenhNhanChi equals to DEFAULT_TIEN_BENH_NHAN_CHI
        defaultXetNghiemApDungShouldBeFound("tienBenhNhanChi.equals=" + DEFAULT_TIEN_BENH_NHAN_CHI);

        // Get all the xetNghiemApDungList where tienBenhNhanChi equals to UPDATED_TIEN_BENH_NHAN_CHI
        defaultXetNghiemApDungShouldNotBeFound("tienBenhNhanChi.equals=" + UPDATED_TIEN_BENH_NHAN_CHI);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTienBenhNhanChiIsNotEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tienBenhNhanChi not equals to DEFAULT_TIEN_BENH_NHAN_CHI
        defaultXetNghiemApDungShouldNotBeFound("tienBenhNhanChi.notEquals=" + DEFAULT_TIEN_BENH_NHAN_CHI);

        // Get all the xetNghiemApDungList where tienBenhNhanChi not equals to UPDATED_TIEN_BENH_NHAN_CHI
        defaultXetNghiemApDungShouldBeFound("tienBenhNhanChi.notEquals=" + UPDATED_TIEN_BENH_NHAN_CHI);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTienBenhNhanChiIsInShouldWork() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tienBenhNhanChi in DEFAULT_TIEN_BENH_NHAN_CHI or UPDATED_TIEN_BENH_NHAN_CHI
        defaultXetNghiemApDungShouldBeFound("tienBenhNhanChi.in=" + DEFAULT_TIEN_BENH_NHAN_CHI + "," + UPDATED_TIEN_BENH_NHAN_CHI);

        // Get all the xetNghiemApDungList where tienBenhNhanChi equals to UPDATED_TIEN_BENH_NHAN_CHI
        defaultXetNghiemApDungShouldNotBeFound("tienBenhNhanChi.in=" + UPDATED_TIEN_BENH_NHAN_CHI);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTienBenhNhanChiIsNullOrNotNull() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tienBenhNhanChi is not null
        defaultXetNghiemApDungShouldBeFound("tienBenhNhanChi.specified=true");

        // Get all the xetNghiemApDungList where tienBenhNhanChi is null
        defaultXetNghiemApDungShouldNotBeFound("tienBenhNhanChi.specified=false");
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTienBenhNhanChiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tienBenhNhanChi is greater than or equal to DEFAULT_TIEN_BENH_NHAN_CHI
        defaultXetNghiemApDungShouldBeFound("tienBenhNhanChi.greaterThanOrEqual=" + DEFAULT_TIEN_BENH_NHAN_CHI);

        // Get all the xetNghiemApDungList where tienBenhNhanChi is greater than or equal to UPDATED_TIEN_BENH_NHAN_CHI
        defaultXetNghiemApDungShouldNotBeFound("tienBenhNhanChi.greaterThanOrEqual=" + UPDATED_TIEN_BENH_NHAN_CHI);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTienBenhNhanChiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tienBenhNhanChi is less than or equal to DEFAULT_TIEN_BENH_NHAN_CHI
        defaultXetNghiemApDungShouldBeFound("tienBenhNhanChi.lessThanOrEqual=" + DEFAULT_TIEN_BENH_NHAN_CHI);

        // Get all the xetNghiemApDungList where tienBenhNhanChi is less than or equal to SMALLER_TIEN_BENH_NHAN_CHI
        defaultXetNghiemApDungShouldNotBeFound("tienBenhNhanChi.lessThanOrEqual=" + SMALLER_TIEN_BENH_NHAN_CHI);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTienBenhNhanChiIsLessThanSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tienBenhNhanChi is less than DEFAULT_TIEN_BENH_NHAN_CHI
        defaultXetNghiemApDungShouldNotBeFound("tienBenhNhanChi.lessThan=" + DEFAULT_TIEN_BENH_NHAN_CHI);

        // Get all the xetNghiemApDungList where tienBenhNhanChi is less than UPDATED_TIEN_BENH_NHAN_CHI
        defaultXetNghiemApDungShouldBeFound("tienBenhNhanChi.lessThan=" + UPDATED_TIEN_BENH_NHAN_CHI);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTienBenhNhanChiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tienBenhNhanChi is greater than DEFAULT_TIEN_BENH_NHAN_CHI
        defaultXetNghiemApDungShouldNotBeFound("tienBenhNhanChi.greaterThan=" + DEFAULT_TIEN_BENH_NHAN_CHI);

        // Get all the xetNghiemApDungList where tienBenhNhanChi is greater than SMALLER_TIEN_BENH_NHAN_CHI
        defaultXetNghiemApDungShouldBeFound("tienBenhNhanChi.greaterThan=" + SMALLER_TIEN_BENH_NHAN_CHI);
    }


    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTienBhxhChiIsEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tienBhxhChi equals to DEFAULT_TIEN_BHXH_CHI
        defaultXetNghiemApDungShouldBeFound("tienBhxhChi.equals=" + DEFAULT_TIEN_BHXH_CHI);

        // Get all the xetNghiemApDungList where tienBhxhChi equals to UPDATED_TIEN_BHXH_CHI
        defaultXetNghiemApDungShouldNotBeFound("tienBhxhChi.equals=" + UPDATED_TIEN_BHXH_CHI);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTienBhxhChiIsNotEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tienBhxhChi not equals to DEFAULT_TIEN_BHXH_CHI
        defaultXetNghiemApDungShouldNotBeFound("tienBhxhChi.notEquals=" + DEFAULT_TIEN_BHXH_CHI);

        // Get all the xetNghiemApDungList where tienBhxhChi not equals to UPDATED_TIEN_BHXH_CHI
        defaultXetNghiemApDungShouldBeFound("tienBhxhChi.notEquals=" + UPDATED_TIEN_BHXH_CHI);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTienBhxhChiIsInShouldWork() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tienBhxhChi in DEFAULT_TIEN_BHXH_CHI or UPDATED_TIEN_BHXH_CHI
        defaultXetNghiemApDungShouldBeFound("tienBhxhChi.in=" + DEFAULT_TIEN_BHXH_CHI + "," + UPDATED_TIEN_BHXH_CHI);

        // Get all the xetNghiemApDungList where tienBhxhChi equals to UPDATED_TIEN_BHXH_CHI
        defaultXetNghiemApDungShouldNotBeFound("tienBhxhChi.in=" + UPDATED_TIEN_BHXH_CHI);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTienBhxhChiIsNullOrNotNull() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tienBhxhChi is not null
        defaultXetNghiemApDungShouldBeFound("tienBhxhChi.specified=true");

        // Get all the xetNghiemApDungList where tienBhxhChi is null
        defaultXetNghiemApDungShouldNotBeFound("tienBhxhChi.specified=false");
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTienBhxhChiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tienBhxhChi is greater than or equal to DEFAULT_TIEN_BHXH_CHI
        defaultXetNghiemApDungShouldBeFound("tienBhxhChi.greaterThanOrEqual=" + DEFAULT_TIEN_BHXH_CHI);

        // Get all the xetNghiemApDungList where tienBhxhChi is greater than or equal to UPDATED_TIEN_BHXH_CHI
        defaultXetNghiemApDungShouldNotBeFound("tienBhxhChi.greaterThanOrEqual=" + UPDATED_TIEN_BHXH_CHI);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTienBhxhChiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tienBhxhChi is less than or equal to DEFAULT_TIEN_BHXH_CHI
        defaultXetNghiemApDungShouldBeFound("tienBhxhChi.lessThanOrEqual=" + DEFAULT_TIEN_BHXH_CHI);

        // Get all the xetNghiemApDungList where tienBhxhChi is less than or equal to SMALLER_TIEN_BHXH_CHI
        defaultXetNghiemApDungShouldNotBeFound("tienBhxhChi.lessThanOrEqual=" + SMALLER_TIEN_BHXH_CHI);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTienBhxhChiIsLessThanSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tienBhxhChi is less than DEFAULT_TIEN_BHXH_CHI
        defaultXetNghiemApDungShouldNotBeFound("tienBhxhChi.lessThan=" + DEFAULT_TIEN_BHXH_CHI);

        // Get all the xetNghiemApDungList where tienBhxhChi is less than UPDATED_TIEN_BHXH_CHI
        defaultXetNghiemApDungShouldBeFound("tienBhxhChi.lessThan=" + UPDATED_TIEN_BHXH_CHI);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTienBhxhChiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tienBhxhChi is greater than DEFAULT_TIEN_BHXH_CHI
        defaultXetNghiemApDungShouldNotBeFound("tienBhxhChi.greaterThan=" + DEFAULT_TIEN_BHXH_CHI);

        // Get all the xetNghiemApDungList where tienBhxhChi is greater than SMALLER_TIEN_BHXH_CHI
        defaultXetNghiemApDungShouldBeFound("tienBhxhChi.greaterThan=" + SMALLER_TIEN_BHXH_CHI);
    }


    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTienNgoaiBhytIsEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tienNgoaiBhyt equals to DEFAULT_TIEN_NGOAI_BHYT
        defaultXetNghiemApDungShouldBeFound("tienNgoaiBhyt.equals=" + DEFAULT_TIEN_NGOAI_BHYT);

        // Get all the xetNghiemApDungList where tienNgoaiBhyt equals to UPDATED_TIEN_NGOAI_BHYT
        defaultXetNghiemApDungShouldNotBeFound("tienNgoaiBhyt.equals=" + UPDATED_TIEN_NGOAI_BHYT);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTienNgoaiBhytIsNotEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tienNgoaiBhyt not equals to DEFAULT_TIEN_NGOAI_BHYT
        defaultXetNghiemApDungShouldNotBeFound("tienNgoaiBhyt.notEquals=" + DEFAULT_TIEN_NGOAI_BHYT);

        // Get all the xetNghiemApDungList where tienNgoaiBhyt not equals to UPDATED_TIEN_NGOAI_BHYT
        defaultXetNghiemApDungShouldBeFound("tienNgoaiBhyt.notEquals=" + UPDATED_TIEN_NGOAI_BHYT);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTienNgoaiBhytIsInShouldWork() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tienNgoaiBhyt in DEFAULT_TIEN_NGOAI_BHYT or UPDATED_TIEN_NGOAI_BHYT
        defaultXetNghiemApDungShouldBeFound("tienNgoaiBhyt.in=" + DEFAULT_TIEN_NGOAI_BHYT + "," + UPDATED_TIEN_NGOAI_BHYT);

        // Get all the xetNghiemApDungList where tienNgoaiBhyt equals to UPDATED_TIEN_NGOAI_BHYT
        defaultXetNghiemApDungShouldNotBeFound("tienNgoaiBhyt.in=" + UPDATED_TIEN_NGOAI_BHYT);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTienNgoaiBhytIsNullOrNotNull() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tienNgoaiBhyt is not null
        defaultXetNghiemApDungShouldBeFound("tienNgoaiBhyt.specified=true");

        // Get all the xetNghiemApDungList where tienNgoaiBhyt is null
        defaultXetNghiemApDungShouldNotBeFound("tienNgoaiBhyt.specified=false");
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTienNgoaiBhytIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tienNgoaiBhyt is greater than or equal to DEFAULT_TIEN_NGOAI_BHYT
        defaultXetNghiemApDungShouldBeFound("tienNgoaiBhyt.greaterThanOrEqual=" + DEFAULT_TIEN_NGOAI_BHYT);

        // Get all the xetNghiemApDungList where tienNgoaiBhyt is greater than or equal to UPDATED_TIEN_NGOAI_BHYT
        defaultXetNghiemApDungShouldNotBeFound("tienNgoaiBhyt.greaterThanOrEqual=" + UPDATED_TIEN_NGOAI_BHYT);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTienNgoaiBhytIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tienNgoaiBhyt is less than or equal to DEFAULT_TIEN_NGOAI_BHYT
        defaultXetNghiemApDungShouldBeFound("tienNgoaiBhyt.lessThanOrEqual=" + DEFAULT_TIEN_NGOAI_BHYT);

        // Get all the xetNghiemApDungList where tienNgoaiBhyt is less than or equal to SMALLER_TIEN_NGOAI_BHYT
        defaultXetNghiemApDungShouldNotBeFound("tienNgoaiBhyt.lessThanOrEqual=" + SMALLER_TIEN_NGOAI_BHYT);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTienNgoaiBhytIsLessThanSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tienNgoaiBhyt is less than DEFAULT_TIEN_NGOAI_BHYT
        defaultXetNghiemApDungShouldNotBeFound("tienNgoaiBhyt.lessThan=" + DEFAULT_TIEN_NGOAI_BHYT);

        // Get all the xetNghiemApDungList where tienNgoaiBhyt is less than UPDATED_TIEN_NGOAI_BHYT
        defaultXetNghiemApDungShouldBeFound("tienNgoaiBhyt.lessThan=" + UPDATED_TIEN_NGOAI_BHYT);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTienNgoaiBhytIsGreaterThanSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tienNgoaiBhyt is greater than DEFAULT_TIEN_NGOAI_BHYT
        defaultXetNghiemApDungShouldNotBeFound("tienNgoaiBhyt.greaterThan=" + DEFAULT_TIEN_NGOAI_BHYT);

        // Get all the xetNghiemApDungList where tienNgoaiBhyt is greater than SMALLER_TIEN_NGOAI_BHYT
        defaultXetNghiemApDungShouldBeFound("tienNgoaiBhyt.greaterThan=" + SMALLER_TIEN_NGOAI_BHYT);
    }


    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTongTienThanhToanIsEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tongTienThanhToan equals to DEFAULT_TONG_TIEN_THANH_TOAN
        defaultXetNghiemApDungShouldBeFound("tongTienThanhToan.equals=" + DEFAULT_TONG_TIEN_THANH_TOAN);

        // Get all the xetNghiemApDungList where tongTienThanhToan equals to UPDATED_TONG_TIEN_THANH_TOAN
        defaultXetNghiemApDungShouldNotBeFound("tongTienThanhToan.equals=" + UPDATED_TONG_TIEN_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTongTienThanhToanIsNotEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tongTienThanhToan not equals to DEFAULT_TONG_TIEN_THANH_TOAN
        defaultXetNghiemApDungShouldNotBeFound("tongTienThanhToan.notEquals=" + DEFAULT_TONG_TIEN_THANH_TOAN);

        // Get all the xetNghiemApDungList where tongTienThanhToan not equals to UPDATED_TONG_TIEN_THANH_TOAN
        defaultXetNghiemApDungShouldBeFound("tongTienThanhToan.notEquals=" + UPDATED_TONG_TIEN_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTongTienThanhToanIsInShouldWork() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tongTienThanhToan in DEFAULT_TONG_TIEN_THANH_TOAN or UPDATED_TONG_TIEN_THANH_TOAN
        defaultXetNghiemApDungShouldBeFound("tongTienThanhToan.in=" + DEFAULT_TONG_TIEN_THANH_TOAN + "," + UPDATED_TONG_TIEN_THANH_TOAN);

        // Get all the xetNghiemApDungList where tongTienThanhToan equals to UPDATED_TONG_TIEN_THANH_TOAN
        defaultXetNghiemApDungShouldNotBeFound("tongTienThanhToan.in=" + UPDATED_TONG_TIEN_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTongTienThanhToanIsNullOrNotNull() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tongTienThanhToan is not null
        defaultXetNghiemApDungShouldBeFound("tongTienThanhToan.specified=true");

        // Get all the xetNghiemApDungList where tongTienThanhToan is null
        defaultXetNghiemApDungShouldNotBeFound("tongTienThanhToan.specified=false");
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTongTienThanhToanIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tongTienThanhToan is greater than or equal to DEFAULT_TONG_TIEN_THANH_TOAN
        defaultXetNghiemApDungShouldBeFound("tongTienThanhToan.greaterThanOrEqual=" + DEFAULT_TONG_TIEN_THANH_TOAN);

        // Get all the xetNghiemApDungList where tongTienThanhToan is greater than or equal to UPDATED_TONG_TIEN_THANH_TOAN
        defaultXetNghiemApDungShouldNotBeFound("tongTienThanhToan.greaterThanOrEqual=" + UPDATED_TONG_TIEN_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTongTienThanhToanIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tongTienThanhToan is less than or equal to DEFAULT_TONG_TIEN_THANH_TOAN
        defaultXetNghiemApDungShouldBeFound("tongTienThanhToan.lessThanOrEqual=" + DEFAULT_TONG_TIEN_THANH_TOAN);

        // Get all the xetNghiemApDungList where tongTienThanhToan is less than or equal to SMALLER_TONG_TIEN_THANH_TOAN
        defaultXetNghiemApDungShouldNotBeFound("tongTienThanhToan.lessThanOrEqual=" + SMALLER_TONG_TIEN_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTongTienThanhToanIsLessThanSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tongTienThanhToan is less than DEFAULT_TONG_TIEN_THANH_TOAN
        defaultXetNghiemApDungShouldNotBeFound("tongTienThanhToan.lessThan=" + DEFAULT_TONG_TIEN_THANH_TOAN);

        // Get all the xetNghiemApDungList where tongTienThanhToan is less than UPDATED_TONG_TIEN_THANH_TOAN
        defaultXetNghiemApDungShouldBeFound("tongTienThanhToan.lessThan=" + UPDATED_TONG_TIEN_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTongTienThanhToanIsGreaterThanSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tongTienThanhToan is greater than DEFAULT_TONG_TIEN_THANH_TOAN
        defaultXetNghiemApDungShouldNotBeFound("tongTienThanhToan.greaterThan=" + DEFAULT_TONG_TIEN_THANH_TOAN);

        // Get all the xetNghiemApDungList where tongTienThanhToan is greater than SMALLER_TONG_TIEN_THANH_TOAN
        defaultXetNghiemApDungShouldBeFound("tongTienThanhToan.greaterThan=" + SMALLER_TONG_TIEN_THANH_TOAN);
    }


    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTyLeBhxhThanhToanIsEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tyLeBhxhThanhToan equals to DEFAULT_TY_LE_BHXH_THANH_TOAN
        defaultXetNghiemApDungShouldBeFound("tyLeBhxhThanhToan.equals=" + DEFAULT_TY_LE_BHXH_THANH_TOAN);

        // Get all the xetNghiemApDungList where tyLeBhxhThanhToan equals to UPDATED_TY_LE_BHXH_THANH_TOAN
        defaultXetNghiemApDungShouldNotBeFound("tyLeBhxhThanhToan.equals=" + UPDATED_TY_LE_BHXH_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTyLeBhxhThanhToanIsNotEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tyLeBhxhThanhToan not equals to DEFAULT_TY_LE_BHXH_THANH_TOAN
        defaultXetNghiemApDungShouldNotBeFound("tyLeBhxhThanhToan.notEquals=" + DEFAULT_TY_LE_BHXH_THANH_TOAN);

        // Get all the xetNghiemApDungList where tyLeBhxhThanhToan not equals to UPDATED_TY_LE_BHXH_THANH_TOAN
        defaultXetNghiemApDungShouldBeFound("tyLeBhxhThanhToan.notEquals=" + UPDATED_TY_LE_BHXH_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTyLeBhxhThanhToanIsInShouldWork() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tyLeBhxhThanhToan in DEFAULT_TY_LE_BHXH_THANH_TOAN or UPDATED_TY_LE_BHXH_THANH_TOAN
        defaultXetNghiemApDungShouldBeFound("tyLeBhxhThanhToan.in=" + DEFAULT_TY_LE_BHXH_THANH_TOAN + "," + UPDATED_TY_LE_BHXH_THANH_TOAN);

        // Get all the xetNghiemApDungList where tyLeBhxhThanhToan equals to UPDATED_TY_LE_BHXH_THANH_TOAN
        defaultXetNghiemApDungShouldNotBeFound("tyLeBhxhThanhToan.in=" + UPDATED_TY_LE_BHXH_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTyLeBhxhThanhToanIsNullOrNotNull() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tyLeBhxhThanhToan is not null
        defaultXetNghiemApDungShouldBeFound("tyLeBhxhThanhToan.specified=true");

        // Get all the xetNghiemApDungList where tyLeBhxhThanhToan is null
        defaultXetNghiemApDungShouldNotBeFound("tyLeBhxhThanhToan.specified=false");
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTyLeBhxhThanhToanIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tyLeBhxhThanhToan is greater than or equal to DEFAULT_TY_LE_BHXH_THANH_TOAN
        defaultXetNghiemApDungShouldBeFound("tyLeBhxhThanhToan.greaterThanOrEqual=" + DEFAULT_TY_LE_BHXH_THANH_TOAN);

        // Get all the xetNghiemApDungList where tyLeBhxhThanhToan is greater than or equal to UPDATED_TY_LE_BHXH_THANH_TOAN
        defaultXetNghiemApDungShouldNotBeFound("tyLeBhxhThanhToan.greaterThanOrEqual=" + UPDATED_TY_LE_BHXH_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTyLeBhxhThanhToanIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tyLeBhxhThanhToan is less than or equal to DEFAULT_TY_LE_BHXH_THANH_TOAN
        defaultXetNghiemApDungShouldBeFound("tyLeBhxhThanhToan.lessThanOrEqual=" + DEFAULT_TY_LE_BHXH_THANH_TOAN);

        // Get all the xetNghiemApDungList where tyLeBhxhThanhToan is less than or equal to SMALLER_TY_LE_BHXH_THANH_TOAN
        defaultXetNghiemApDungShouldNotBeFound("tyLeBhxhThanhToan.lessThanOrEqual=" + SMALLER_TY_LE_BHXH_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTyLeBhxhThanhToanIsLessThanSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tyLeBhxhThanhToan is less than DEFAULT_TY_LE_BHXH_THANH_TOAN
        defaultXetNghiemApDungShouldNotBeFound("tyLeBhxhThanhToan.lessThan=" + DEFAULT_TY_LE_BHXH_THANH_TOAN);

        // Get all the xetNghiemApDungList where tyLeBhxhThanhToan is less than UPDATED_TY_LE_BHXH_THANH_TOAN
        defaultXetNghiemApDungShouldBeFound("tyLeBhxhThanhToan.lessThan=" + UPDATED_TY_LE_BHXH_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByTyLeBhxhThanhToanIsGreaterThanSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where tyLeBhxhThanhToan is greater than DEFAULT_TY_LE_BHXH_THANH_TOAN
        defaultXetNghiemApDungShouldNotBeFound("tyLeBhxhThanhToan.greaterThan=" + DEFAULT_TY_LE_BHXH_THANH_TOAN);

        // Get all the xetNghiemApDungList where tyLeBhxhThanhToan is greater than SMALLER_TY_LE_BHXH_THANH_TOAN
        defaultXetNghiemApDungShouldBeFound("tyLeBhxhThanhToan.greaterThan=" + SMALLER_TY_LE_BHXH_THANH_TOAN);
    }


    @Test
    @Transactional
    public void getAllXetNghiemApDungsByGiaBhytIsEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where giaBhyt equals to DEFAULT_GIA_BHYT
        defaultXetNghiemApDungShouldBeFound("giaBhyt.equals=" + DEFAULT_GIA_BHYT);

        // Get all the xetNghiemApDungList where giaBhyt equals to UPDATED_GIA_BHYT
        defaultXetNghiemApDungShouldNotBeFound("giaBhyt.equals=" + UPDATED_GIA_BHYT);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByGiaBhytIsNotEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where giaBhyt not equals to DEFAULT_GIA_BHYT
        defaultXetNghiemApDungShouldNotBeFound("giaBhyt.notEquals=" + DEFAULT_GIA_BHYT);

        // Get all the xetNghiemApDungList where giaBhyt not equals to UPDATED_GIA_BHYT
        defaultXetNghiemApDungShouldBeFound("giaBhyt.notEquals=" + UPDATED_GIA_BHYT);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByGiaBhytIsInShouldWork() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where giaBhyt in DEFAULT_GIA_BHYT or UPDATED_GIA_BHYT
        defaultXetNghiemApDungShouldBeFound("giaBhyt.in=" + DEFAULT_GIA_BHYT + "," + UPDATED_GIA_BHYT);

        // Get all the xetNghiemApDungList where giaBhyt equals to UPDATED_GIA_BHYT
        defaultXetNghiemApDungShouldNotBeFound("giaBhyt.in=" + UPDATED_GIA_BHYT);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByGiaBhytIsNullOrNotNull() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where giaBhyt is not null
        defaultXetNghiemApDungShouldBeFound("giaBhyt.specified=true");

        // Get all the xetNghiemApDungList where giaBhyt is null
        defaultXetNghiemApDungShouldNotBeFound("giaBhyt.specified=false");
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByGiaBhytIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where giaBhyt is greater than or equal to DEFAULT_GIA_BHYT
        defaultXetNghiemApDungShouldBeFound("giaBhyt.greaterThanOrEqual=" + DEFAULT_GIA_BHYT);

        // Get all the xetNghiemApDungList where giaBhyt is greater than or equal to UPDATED_GIA_BHYT
        defaultXetNghiemApDungShouldNotBeFound("giaBhyt.greaterThanOrEqual=" + UPDATED_GIA_BHYT);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByGiaBhytIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where giaBhyt is less than or equal to DEFAULT_GIA_BHYT
        defaultXetNghiemApDungShouldBeFound("giaBhyt.lessThanOrEqual=" + DEFAULT_GIA_BHYT);

        // Get all the xetNghiemApDungList where giaBhyt is less than or equal to SMALLER_GIA_BHYT
        defaultXetNghiemApDungShouldNotBeFound("giaBhyt.lessThanOrEqual=" + SMALLER_GIA_BHYT);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByGiaBhytIsLessThanSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where giaBhyt is less than DEFAULT_GIA_BHYT
        defaultXetNghiemApDungShouldNotBeFound("giaBhyt.lessThan=" + DEFAULT_GIA_BHYT);

        // Get all the xetNghiemApDungList where giaBhyt is less than UPDATED_GIA_BHYT
        defaultXetNghiemApDungShouldBeFound("giaBhyt.lessThan=" + UPDATED_GIA_BHYT);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByGiaBhytIsGreaterThanSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where giaBhyt is greater than DEFAULT_GIA_BHYT
        defaultXetNghiemApDungShouldNotBeFound("giaBhyt.greaterThan=" + DEFAULT_GIA_BHYT);

        // Get all the xetNghiemApDungList where giaBhyt is greater than SMALLER_GIA_BHYT
        defaultXetNghiemApDungShouldBeFound("giaBhyt.greaterThan=" + SMALLER_GIA_BHYT);
    }


    @Test
    @Transactional
    public void getAllXetNghiemApDungsByGiaKhongBhytIsEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where giaKhongBhyt equals to DEFAULT_GIA_KHONG_BHYT
        defaultXetNghiemApDungShouldBeFound("giaKhongBhyt.equals=" + DEFAULT_GIA_KHONG_BHYT);

        // Get all the xetNghiemApDungList where giaKhongBhyt equals to UPDATED_GIA_KHONG_BHYT
        defaultXetNghiemApDungShouldNotBeFound("giaKhongBhyt.equals=" + UPDATED_GIA_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByGiaKhongBhytIsNotEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where giaKhongBhyt not equals to DEFAULT_GIA_KHONG_BHYT
        defaultXetNghiemApDungShouldNotBeFound("giaKhongBhyt.notEquals=" + DEFAULT_GIA_KHONG_BHYT);

        // Get all the xetNghiemApDungList where giaKhongBhyt not equals to UPDATED_GIA_KHONG_BHYT
        defaultXetNghiemApDungShouldBeFound("giaKhongBhyt.notEquals=" + UPDATED_GIA_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByGiaKhongBhytIsInShouldWork() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where giaKhongBhyt in DEFAULT_GIA_KHONG_BHYT or UPDATED_GIA_KHONG_BHYT
        defaultXetNghiemApDungShouldBeFound("giaKhongBhyt.in=" + DEFAULT_GIA_KHONG_BHYT + "," + UPDATED_GIA_KHONG_BHYT);

        // Get all the xetNghiemApDungList where giaKhongBhyt equals to UPDATED_GIA_KHONG_BHYT
        defaultXetNghiemApDungShouldNotBeFound("giaKhongBhyt.in=" + UPDATED_GIA_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByGiaKhongBhytIsNullOrNotNull() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where giaKhongBhyt is not null
        defaultXetNghiemApDungShouldBeFound("giaKhongBhyt.specified=true");

        // Get all the xetNghiemApDungList where giaKhongBhyt is null
        defaultXetNghiemApDungShouldNotBeFound("giaKhongBhyt.specified=false");
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByGiaKhongBhytIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where giaKhongBhyt is greater than or equal to DEFAULT_GIA_KHONG_BHYT
        defaultXetNghiemApDungShouldBeFound("giaKhongBhyt.greaterThanOrEqual=" + DEFAULT_GIA_KHONG_BHYT);

        // Get all the xetNghiemApDungList where giaKhongBhyt is greater than or equal to UPDATED_GIA_KHONG_BHYT
        defaultXetNghiemApDungShouldNotBeFound("giaKhongBhyt.greaterThanOrEqual=" + UPDATED_GIA_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByGiaKhongBhytIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where giaKhongBhyt is less than or equal to DEFAULT_GIA_KHONG_BHYT
        defaultXetNghiemApDungShouldBeFound("giaKhongBhyt.lessThanOrEqual=" + DEFAULT_GIA_KHONG_BHYT);

        // Get all the xetNghiemApDungList where giaKhongBhyt is less than or equal to SMALLER_GIA_KHONG_BHYT
        defaultXetNghiemApDungShouldNotBeFound("giaKhongBhyt.lessThanOrEqual=" + SMALLER_GIA_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByGiaKhongBhytIsLessThanSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where giaKhongBhyt is less than DEFAULT_GIA_KHONG_BHYT
        defaultXetNghiemApDungShouldNotBeFound("giaKhongBhyt.lessThan=" + DEFAULT_GIA_KHONG_BHYT);

        // Get all the xetNghiemApDungList where giaKhongBhyt is less than UPDATED_GIA_KHONG_BHYT
        defaultXetNghiemApDungShouldBeFound("giaKhongBhyt.lessThan=" + UPDATED_GIA_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByGiaKhongBhytIsGreaterThanSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where giaKhongBhyt is greater than DEFAULT_GIA_KHONG_BHYT
        defaultXetNghiemApDungShouldNotBeFound("giaKhongBhyt.greaterThan=" + DEFAULT_GIA_KHONG_BHYT);

        // Get all the xetNghiemApDungList where giaKhongBhyt is greater than SMALLER_GIA_KHONG_BHYT
        defaultXetNghiemApDungShouldBeFound("giaKhongBhyt.greaterThan=" + SMALLER_GIA_KHONG_BHYT);
    }


    @Test
    @Transactional
    public void getAllXetNghiemApDungsByDoiTuongDacBietIsEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where doiTuongDacBiet equals to DEFAULT_DOI_TUONG_DAC_BIET
        defaultXetNghiemApDungShouldBeFound("doiTuongDacBiet.equals=" + DEFAULT_DOI_TUONG_DAC_BIET);

        // Get all the xetNghiemApDungList where doiTuongDacBiet equals to UPDATED_DOI_TUONG_DAC_BIET
        defaultXetNghiemApDungShouldNotBeFound("doiTuongDacBiet.equals=" + UPDATED_DOI_TUONG_DAC_BIET);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByDoiTuongDacBietIsNotEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where doiTuongDacBiet not equals to DEFAULT_DOI_TUONG_DAC_BIET
        defaultXetNghiemApDungShouldNotBeFound("doiTuongDacBiet.notEquals=" + DEFAULT_DOI_TUONG_DAC_BIET);

        // Get all the xetNghiemApDungList where doiTuongDacBiet not equals to UPDATED_DOI_TUONG_DAC_BIET
        defaultXetNghiemApDungShouldBeFound("doiTuongDacBiet.notEquals=" + UPDATED_DOI_TUONG_DAC_BIET);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByDoiTuongDacBietIsInShouldWork() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where doiTuongDacBiet in DEFAULT_DOI_TUONG_DAC_BIET or UPDATED_DOI_TUONG_DAC_BIET
        defaultXetNghiemApDungShouldBeFound("doiTuongDacBiet.in=" + DEFAULT_DOI_TUONG_DAC_BIET + "," + UPDATED_DOI_TUONG_DAC_BIET);

        // Get all the xetNghiemApDungList where doiTuongDacBiet equals to UPDATED_DOI_TUONG_DAC_BIET
        defaultXetNghiemApDungShouldNotBeFound("doiTuongDacBiet.in=" + UPDATED_DOI_TUONG_DAC_BIET);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByDoiTuongDacBietIsNullOrNotNull() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where doiTuongDacBiet is not null
        defaultXetNghiemApDungShouldBeFound("doiTuongDacBiet.specified=true");

        // Get all the xetNghiemApDungList where doiTuongDacBiet is null
        defaultXetNghiemApDungShouldNotBeFound("doiTuongDacBiet.specified=false");
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByNguonChiIsEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where nguonChi equals to DEFAULT_NGUON_CHI
        defaultXetNghiemApDungShouldBeFound("nguonChi.equals=" + DEFAULT_NGUON_CHI);

        // Get all the xetNghiemApDungList where nguonChi equals to UPDATED_NGUON_CHI
        defaultXetNghiemApDungShouldNotBeFound("nguonChi.equals=" + UPDATED_NGUON_CHI);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByNguonChiIsNotEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where nguonChi not equals to DEFAULT_NGUON_CHI
        defaultXetNghiemApDungShouldNotBeFound("nguonChi.notEquals=" + DEFAULT_NGUON_CHI);

        // Get all the xetNghiemApDungList where nguonChi not equals to UPDATED_NGUON_CHI
        defaultXetNghiemApDungShouldBeFound("nguonChi.notEquals=" + UPDATED_NGUON_CHI);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByNguonChiIsInShouldWork() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where nguonChi in DEFAULT_NGUON_CHI or UPDATED_NGUON_CHI
        defaultXetNghiemApDungShouldBeFound("nguonChi.in=" + DEFAULT_NGUON_CHI + "," + UPDATED_NGUON_CHI);

        // Get all the xetNghiemApDungList where nguonChi equals to UPDATED_NGUON_CHI
        defaultXetNghiemApDungShouldNotBeFound("nguonChi.in=" + UPDATED_NGUON_CHI);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByNguonChiIsNullOrNotNull() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where nguonChi is not null
        defaultXetNghiemApDungShouldBeFound("nguonChi.specified=true");

        // Get all the xetNghiemApDungList where nguonChi is null
        defaultXetNghiemApDungShouldNotBeFound("nguonChi.specified=false");
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByNguonChiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where nguonChi is greater than or equal to DEFAULT_NGUON_CHI
        defaultXetNghiemApDungShouldBeFound("nguonChi.greaterThanOrEqual=" + DEFAULT_NGUON_CHI);

        // Get all the xetNghiemApDungList where nguonChi is greater than or equal to UPDATED_NGUON_CHI
        defaultXetNghiemApDungShouldNotBeFound("nguonChi.greaterThanOrEqual=" + UPDATED_NGUON_CHI);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByNguonChiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where nguonChi is less than or equal to DEFAULT_NGUON_CHI
        defaultXetNghiemApDungShouldBeFound("nguonChi.lessThanOrEqual=" + DEFAULT_NGUON_CHI);

        // Get all the xetNghiemApDungList where nguonChi is less than or equal to SMALLER_NGUON_CHI
        defaultXetNghiemApDungShouldNotBeFound("nguonChi.lessThanOrEqual=" + SMALLER_NGUON_CHI);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByNguonChiIsLessThanSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where nguonChi is less than DEFAULT_NGUON_CHI
        defaultXetNghiemApDungShouldNotBeFound("nguonChi.lessThan=" + DEFAULT_NGUON_CHI);

        // Get all the xetNghiemApDungList where nguonChi is less than UPDATED_NGUON_CHI
        defaultXetNghiemApDungShouldBeFound("nguonChi.lessThan=" + UPDATED_NGUON_CHI);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByNguonChiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where nguonChi is greater than DEFAULT_NGUON_CHI
        defaultXetNghiemApDungShouldNotBeFound("nguonChi.greaterThan=" + DEFAULT_NGUON_CHI);

        // Get all the xetNghiemApDungList where nguonChi is greater than SMALLER_NGUON_CHI
        defaultXetNghiemApDungShouldBeFound("nguonChi.greaterThan=" + SMALLER_NGUON_CHI);
    }


    @Test
    @Transactional
    public void getAllXetNghiemApDungsByEnableIsEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where enable equals to DEFAULT_ENABLE
        defaultXetNghiemApDungShouldBeFound("enable.equals=" + DEFAULT_ENABLE);

        // Get all the xetNghiemApDungList where enable equals to UPDATED_ENABLE
        defaultXetNghiemApDungShouldNotBeFound("enable.equals=" + UPDATED_ENABLE);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByEnableIsNotEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where enable not equals to DEFAULT_ENABLE
        defaultXetNghiemApDungShouldNotBeFound("enable.notEquals=" + DEFAULT_ENABLE);

        // Get all the xetNghiemApDungList where enable not equals to UPDATED_ENABLE
        defaultXetNghiemApDungShouldBeFound("enable.notEquals=" + UPDATED_ENABLE);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByEnableIsInShouldWork() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where enable in DEFAULT_ENABLE or UPDATED_ENABLE
        defaultXetNghiemApDungShouldBeFound("enable.in=" + DEFAULT_ENABLE + "," + UPDATED_ENABLE);

        // Get all the xetNghiemApDungList where enable equals to UPDATED_ENABLE
        defaultXetNghiemApDungShouldNotBeFound("enable.in=" + UPDATED_ENABLE);
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByEnableIsNullOrNotNull() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        // Get all the xetNghiemApDungList where enable is not null
        defaultXetNghiemApDungShouldBeFound("enable.specified=true");

        // Get all the xetNghiemApDungList where enable is null
        defaultXetNghiemApDungShouldNotBeFound("enable.specified=false");
    }

    @Test
    @Transactional
    public void getAllXetNghiemApDungsByDotGiaIsEqualToSomething() throws Exception {
        // Get already existing entity
        DotGiaDichVuBhxh dotGia = xetNghiemApDung.getDotGia();
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);
        Long dotGiaId = dotGia.getId();

        // Get all the xetNghiemApDungList where dotGia equals to dotGiaId
        defaultXetNghiemApDungShouldBeFound("dotGiaId.equals=" + dotGiaId);

        // Get all the xetNghiemApDungList where dotGia equals to dotGiaId + 1
        defaultXetNghiemApDungShouldNotBeFound("dotGiaId.equals=" + (dotGiaId + 1));
    }


    @Test
    @Transactional
    public void getAllXetNghiemApDungsByXNIsEqualToSomething() throws Exception {
        // Get already existing entity
        XetNghiem xN = xetNghiemApDung.getXN();
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);
        Long xNId = xN.getId();

        // Get all the xetNghiemApDungList where xN equals to xNId
        defaultXetNghiemApDungShouldBeFound("xNId.equals=" + xNId);

        // Get all the xetNghiemApDungList where xN equals to xNId + 1
        defaultXetNghiemApDungShouldNotBeFound("xNId.equals=" + (xNId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultXetNghiemApDungShouldBeFound(String filter) throws Exception {
        restXetNghiemApDungMockMvc.perform(get("/api/xet-nghiem-ap-dungs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(xetNghiemApDung.getId().intValue())))
            .andExpect(jsonPath("$.[*].maBaoCaoBhxh").value(hasItem(DEFAULT_MA_BAO_CAO_BHXH)))
            .andExpect(jsonPath("$.[*].maBaoCaoBhyt").value(hasItem(DEFAULT_MA_BAO_CAO_BHYT)))
            .andExpect(jsonPath("$.[*].ngayApDung").value(hasItem(DEFAULT_NGAY_AP_DUNG.toString())))
            .andExpect(jsonPath("$.[*].soCongVanBhxh").value(hasItem(DEFAULT_SO_CONG_VAN_BHXH)))
            .andExpect(jsonPath("$.[*].soQuyetDinh").value(hasItem(DEFAULT_SO_QUYET_DINH)))
            .andExpect(jsonPath("$.[*].tenBaoCaoBhxh").value(hasItem(DEFAULT_TEN_BAO_CAO_BHXH)))
            .andExpect(jsonPath("$.[*].tenDichVuKhongBhyt").value(hasItem(DEFAULT_TEN_DICH_VU_KHONG_BHYT)))
            .andExpect(jsonPath("$.[*].tienBenhNhanChi").value(hasItem(DEFAULT_TIEN_BENH_NHAN_CHI.intValue())))
            .andExpect(jsonPath("$.[*].tienBhxhChi").value(hasItem(DEFAULT_TIEN_BHXH_CHI.intValue())))
            .andExpect(jsonPath("$.[*].tienNgoaiBhyt").value(hasItem(DEFAULT_TIEN_NGOAI_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].tongTienThanhToan").value(hasItem(DEFAULT_TONG_TIEN_THANH_TOAN.intValue())))
            .andExpect(jsonPath("$.[*].tyLeBhxhThanhToan").value(hasItem(DEFAULT_TY_LE_BHXH_THANH_TOAN)))
            .andExpect(jsonPath("$.[*].giaBhyt").value(hasItem(DEFAULT_GIA_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].giaKhongBhyt").value(hasItem(DEFAULT_GIA_KHONG_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].doiTuongDacBiet").value(hasItem(DEFAULT_DOI_TUONG_DAC_BIET.booleanValue())))
            .andExpect(jsonPath("$.[*].nguonChi").value(hasItem(DEFAULT_NGUON_CHI)))
            .andExpect(jsonPath("$.[*].enable").value(hasItem(DEFAULT_ENABLE.booleanValue())));

        // Check, that the count call also returns 1
        restXetNghiemApDungMockMvc.perform(get("/api/xet-nghiem-ap-dungs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultXetNghiemApDungShouldNotBeFound(String filter) throws Exception {
        restXetNghiemApDungMockMvc.perform(get("/api/xet-nghiem-ap-dungs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restXetNghiemApDungMockMvc.perform(get("/api/xet-nghiem-ap-dungs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingXetNghiemApDung() throws Exception {
        // Get the xetNghiemApDung
        restXetNghiemApDungMockMvc.perform(get("/api/xet-nghiem-ap-dungs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateXetNghiemApDung() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        int databaseSizeBeforeUpdate = xetNghiemApDungRepository.findAll().size();

        // Update the xetNghiemApDung
        XetNghiemApDung updatedXetNghiemApDung = xetNghiemApDungRepository.findById(xetNghiemApDung.getId()).get();
        // Disconnect from session so that the updates on updatedXetNghiemApDung are not directly saved in db
        em.detach(updatedXetNghiemApDung);
        updatedXetNghiemApDung
            .maBaoCaoBhxh(UPDATED_MA_BAO_CAO_BHXH)
            .maBaoCaoBhyt(UPDATED_MA_BAO_CAO_BHYT)
            .ngayApDung(UPDATED_NGAY_AP_DUNG)
            .soCongVanBhxh(UPDATED_SO_CONG_VAN_BHXH)
            .soQuyetDinh(UPDATED_SO_QUYET_DINH)
            .tenBaoCaoBhxh(UPDATED_TEN_BAO_CAO_BHXH)
            .tenDichVuKhongBhyt(UPDATED_TEN_DICH_VU_KHONG_BHYT)
            .tienBenhNhanChi(UPDATED_TIEN_BENH_NHAN_CHI)
            .tienBhxhChi(UPDATED_TIEN_BHXH_CHI)
            .tienNgoaiBhyt(UPDATED_TIEN_NGOAI_BHYT)
            .tongTienThanhToan(UPDATED_TONG_TIEN_THANH_TOAN)
            .tyLeBhxhThanhToan(UPDATED_TY_LE_BHXH_THANH_TOAN)
            .giaBhyt(UPDATED_GIA_BHYT)
            .giaKhongBhyt(UPDATED_GIA_KHONG_BHYT)
            .doiTuongDacBiet(UPDATED_DOI_TUONG_DAC_BIET)
            .nguonChi(UPDATED_NGUON_CHI)
            .enable(UPDATED_ENABLE);
        XetNghiemApDungDTO xetNghiemApDungDTO = xetNghiemApDungMapper.toDto(updatedXetNghiemApDung);

        restXetNghiemApDungMockMvc.perform(put("/api/xet-nghiem-ap-dungs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(xetNghiemApDungDTO)))
            .andExpect(status().isOk());

        // Validate the XetNghiemApDung in the database
        List<XetNghiemApDung> xetNghiemApDungList = xetNghiemApDungRepository.findAll();
        assertThat(xetNghiemApDungList).hasSize(databaseSizeBeforeUpdate);
        XetNghiemApDung testXetNghiemApDung = xetNghiemApDungList.get(xetNghiemApDungList.size() - 1);
        assertThat(testXetNghiemApDung.getMaBaoCaoBhxh()).isEqualTo(UPDATED_MA_BAO_CAO_BHXH);
        assertThat(testXetNghiemApDung.getMaBaoCaoBhyt()).isEqualTo(UPDATED_MA_BAO_CAO_BHYT);
        assertThat(testXetNghiemApDung.getNgayApDung()).isEqualTo(UPDATED_NGAY_AP_DUNG);
        assertThat(testXetNghiemApDung.getSoCongVanBhxh()).isEqualTo(UPDATED_SO_CONG_VAN_BHXH);
        assertThat(testXetNghiemApDung.getSoQuyetDinh()).isEqualTo(UPDATED_SO_QUYET_DINH);
        assertThat(testXetNghiemApDung.getTenBaoCaoBhxh()).isEqualTo(UPDATED_TEN_BAO_CAO_BHXH);
        assertThat(testXetNghiemApDung.getTenDichVuKhongBhyt()).isEqualTo(UPDATED_TEN_DICH_VU_KHONG_BHYT);
        assertThat(testXetNghiemApDung.getTienBenhNhanChi()).isEqualTo(UPDATED_TIEN_BENH_NHAN_CHI);
        assertThat(testXetNghiemApDung.getTienBhxhChi()).isEqualTo(UPDATED_TIEN_BHXH_CHI);
        assertThat(testXetNghiemApDung.getTienNgoaiBhyt()).isEqualTo(UPDATED_TIEN_NGOAI_BHYT);
        assertThat(testXetNghiemApDung.getTongTienThanhToan()).isEqualTo(UPDATED_TONG_TIEN_THANH_TOAN);
        assertThat(testXetNghiemApDung.getTyLeBhxhThanhToan()).isEqualTo(UPDATED_TY_LE_BHXH_THANH_TOAN);
        assertThat(testXetNghiemApDung.getGiaBhyt()).isEqualTo(UPDATED_GIA_BHYT);
        assertThat(testXetNghiemApDung.getGiaKhongBhyt()).isEqualTo(UPDATED_GIA_KHONG_BHYT);
        assertThat(testXetNghiemApDung.isDoiTuongDacBiet()).isEqualTo(UPDATED_DOI_TUONG_DAC_BIET);
        assertThat(testXetNghiemApDung.getNguonChi()).isEqualTo(UPDATED_NGUON_CHI);
        assertThat(testXetNghiemApDung.isEnable()).isEqualTo(UPDATED_ENABLE);
    }

    @Test
    @Transactional
    public void updateNonExistingXetNghiemApDung() throws Exception {
        int databaseSizeBeforeUpdate = xetNghiemApDungRepository.findAll().size();

        // Create the XetNghiemApDung
        XetNghiemApDungDTO xetNghiemApDungDTO = xetNghiemApDungMapper.toDto(xetNghiemApDung);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restXetNghiemApDungMockMvc.perform(put("/api/xet-nghiem-ap-dungs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(xetNghiemApDungDTO)))
            .andExpect(status().isBadRequest());

        // Validate the XetNghiemApDung in the database
        List<XetNghiemApDung> xetNghiemApDungList = xetNghiemApDungRepository.findAll();
        assertThat(xetNghiemApDungList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteXetNghiemApDung() throws Exception {
        // Initialize the database
        xetNghiemApDungRepository.saveAndFlush(xetNghiemApDung);

        int databaseSizeBeforeDelete = xetNghiemApDungRepository.findAll().size();

        // Delete the xetNghiemApDung
        restXetNghiemApDungMockMvc.perform(delete("/api/xet-nghiem-ap-dungs/{id}", xetNghiemApDung.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<XetNghiemApDung> xetNghiemApDungList = xetNghiemApDungRepository.findAll();
        assertThat(xetNghiemApDungList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

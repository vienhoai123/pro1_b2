package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.ThongTinKhoa;
import vn.vnpt.domain.DotDieuTri;
import vn.vnpt.repository.ThongTinKhoaRepository;
import vn.vnpt.service.ThongTinKhoaService;
import vn.vnpt.service.dto.ThongTinKhoaDTO;
import vn.vnpt.service.mapper.ThongTinKhoaMapper;
import vn.vnpt.service.dto.ThongTinKhoaCriteria;
import vn.vnpt.service.ThongTinKhoaQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ThongTinKhoaResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class ThongTinKhoaResourceIT {

    private static final LocalDate DEFAULT_THOI_GIAN_NHAN_BENH = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_THOI_GIAN_NHAN_BENH = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_THOI_GIAN_NHAN_BENH = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_THOI_GIAN_CHUYEN_KHOA = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_THOI_GIAN_CHUYEN_KHOA = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_THOI_GIAN_CHUYEN_KHOA = LocalDate.ofEpochDay(-1L);

    private static final Integer DEFAULT_KHOA_ID = 1;
    private static final Integer UPDATED_KHOA_ID = 2;
    private static final Integer SMALLER_KHOA_ID = 1 - 1;

    private static final Integer DEFAULT_KHOA_CHUYEN_DEN = 1;
    private static final Integer UPDATED_KHOA_CHUYEN_DEN = 2;
    private static final Integer SMALLER_KHOA_CHUYEN_DEN = 1 - 1;

    private static final Integer DEFAULT_KHOA_CHUYEN_DI = 1;
    private static final Integer UPDATED_KHOA_CHUYEN_DI = 2;
    private static final Integer SMALLER_KHOA_CHUYEN_DI = 1 - 1;

    private static final Integer DEFAULT_NHAN_VIEN_NHAN_BENH = 1;
    private static final Integer UPDATED_NHAN_VIEN_NHAN_BENH = 2;
    private static final Integer SMALLER_NHAN_VIEN_NHAN_BENH = 1 - 1;

    private static final Integer DEFAULT_NHAN_VIEN_CHUYEN_KHOA = 1;
    private static final Integer UPDATED_NHAN_VIEN_CHUYEN_KHOA = 2;
    private static final Integer SMALLER_NHAN_VIEN_CHUYEN_KHOA = 1 - 1;

    private static final BigDecimal DEFAULT_SO_NGAY = new BigDecimal(1);
    private static final BigDecimal UPDATED_SO_NGAY = new BigDecimal(2);
    private static final BigDecimal SMALLER_SO_NGAY = new BigDecimal(1 - 1);

    private static final Integer DEFAULT_TRANG_THAI = 1;
    private static final Integer UPDATED_TRANG_THAI = 2;
    private static final Integer SMALLER_TRANG_THAI = 1 - 1;

    private static final Integer DEFAULT_NAM = 1;
    private static final Integer UPDATED_NAM = 2;
    private static final Integer SMALLER_NAM = 1 - 1;

    @Autowired
    private ThongTinKhoaRepository thongTinKhoaRepository;

    @Autowired
    private ThongTinKhoaMapper thongTinKhoaMapper;

    @Autowired
    private ThongTinKhoaService thongTinKhoaService;

    @Autowired
    private ThongTinKhoaQueryService thongTinKhoaQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restThongTinKhoaMockMvc;

    private ThongTinKhoa thongTinKhoa;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ThongTinKhoa createEntity(EntityManager em) {
        ThongTinKhoa thongTinKhoa = new ThongTinKhoa()
            .thoiGianNhanBenh(DEFAULT_THOI_GIAN_NHAN_BENH)
            .thoiGianChuyenKhoa(DEFAULT_THOI_GIAN_CHUYEN_KHOA)
            .khoaId(DEFAULT_KHOA_ID)
            .khoaChuyenDen(DEFAULT_KHOA_CHUYEN_DEN)
            .khoaChuyenDi(DEFAULT_KHOA_CHUYEN_DI)
            .nhanVienNhanBenh(DEFAULT_NHAN_VIEN_NHAN_BENH)
            .nhanVienChuyenKhoa(DEFAULT_NHAN_VIEN_CHUYEN_KHOA)
            .soNgay(DEFAULT_SO_NGAY)
            .trangThai(DEFAULT_TRANG_THAI)
            .nam(DEFAULT_NAM);
        // Add required entity
        DotDieuTri dotDieuTri;
        if (TestUtil.findAll(em, DotDieuTri.class).isEmpty()) {
            dotDieuTri = DotDieuTriResourceIT.createEntity(em);
            em.persist(dotDieuTri);
            em.flush();
        } else {
            dotDieuTri = TestUtil.findAll(em, DotDieuTri.class).get(0);
        }
        thongTinKhoa.setBakb(dotDieuTri);
        // Add required entity
        thongTinKhoa.setDonVi(dotDieuTri);
        // Add required entity
        thongTinKhoa.setBenhNhan(dotDieuTri);
        // Add required entity
        thongTinKhoa.setDotDieuTri(dotDieuTri);
        return thongTinKhoa;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ThongTinKhoa createUpdatedEntity(EntityManager em) {
        ThongTinKhoa thongTinKhoa = new ThongTinKhoa()
            .thoiGianNhanBenh(UPDATED_THOI_GIAN_NHAN_BENH)
            .thoiGianChuyenKhoa(UPDATED_THOI_GIAN_CHUYEN_KHOA)
            .khoaId(UPDATED_KHOA_ID)
            .khoaChuyenDen(UPDATED_KHOA_CHUYEN_DEN)
            .khoaChuyenDi(UPDATED_KHOA_CHUYEN_DI)
            .nhanVienNhanBenh(UPDATED_NHAN_VIEN_NHAN_BENH)
            .nhanVienChuyenKhoa(UPDATED_NHAN_VIEN_CHUYEN_KHOA)
            .soNgay(UPDATED_SO_NGAY)
            .trangThai(UPDATED_TRANG_THAI)
            .nam(UPDATED_NAM);
        // Add required entity
        DotDieuTri dotDieuTri;
        if (TestUtil.findAll(em, DotDieuTri.class).isEmpty()) {
            dotDieuTri = DotDieuTriResourceIT.createUpdatedEntity(em);
            em.persist(dotDieuTri);
            em.flush();
        } else {
            dotDieuTri = TestUtil.findAll(em, DotDieuTri.class).get(0);
        }
        thongTinKhoa.setBakb(dotDieuTri);
        // Add required entity
        thongTinKhoa.setDonVi(dotDieuTri);
        // Add required entity
        thongTinKhoa.setBenhNhan(dotDieuTri);
        // Add required entity
        thongTinKhoa.setDotDieuTri(dotDieuTri);
        return thongTinKhoa;
    }

    @BeforeEach
    public void initTest() {
        thongTinKhoa = createEntity(em);
    }

    @Test
    @Transactional
    public void createThongTinKhoa() throws Exception {
        int databaseSizeBeforeCreate = thongTinKhoaRepository.findAll().size();

        // Create the ThongTinKhoa
        ThongTinKhoaDTO thongTinKhoaDTO = thongTinKhoaMapper.toDto(thongTinKhoa);
        restThongTinKhoaMockMvc.perform(post("/api/thong-tin-khoas").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(thongTinKhoaDTO)))
            .andExpect(status().isCreated());

        // Validate the ThongTinKhoa in the database
        List<ThongTinKhoa> thongTinKhoaList = thongTinKhoaRepository.findAll();
        assertThat(thongTinKhoaList).hasSize(databaseSizeBeforeCreate + 1);
        ThongTinKhoa testThongTinKhoa = thongTinKhoaList.get(thongTinKhoaList.size() - 1);
        assertThat(testThongTinKhoa.getThoiGianNhanBenh()).isEqualTo(DEFAULT_THOI_GIAN_NHAN_BENH);
        assertThat(testThongTinKhoa.getThoiGianChuyenKhoa()).isEqualTo(DEFAULT_THOI_GIAN_CHUYEN_KHOA);
        assertThat(testThongTinKhoa.getKhoaId()).isEqualTo(DEFAULT_KHOA_ID);
        assertThat(testThongTinKhoa.getKhoaChuyenDen()).isEqualTo(DEFAULT_KHOA_CHUYEN_DEN);
        assertThat(testThongTinKhoa.getKhoaChuyenDi()).isEqualTo(DEFAULT_KHOA_CHUYEN_DI);
        assertThat(testThongTinKhoa.getNhanVienNhanBenh()).isEqualTo(DEFAULT_NHAN_VIEN_NHAN_BENH);
        assertThat(testThongTinKhoa.getNhanVienChuyenKhoa()).isEqualTo(DEFAULT_NHAN_VIEN_CHUYEN_KHOA);
        assertThat(testThongTinKhoa.getSoNgay()).isEqualTo(DEFAULT_SO_NGAY);
        assertThat(testThongTinKhoa.getTrangThai()).isEqualTo(DEFAULT_TRANG_THAI);
        assertThat(testThongTinKhoa.getNam()).isEqualTo(DEFAULT_NAM);
    }

    @Test
    @Transactional
    public void createThongTinKhoaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = thongTinKhoaRepository.findAll().size();

        // Create the ThongTinKhoa with an existing ID
        thongTinKhoa.setId(1L);
        ThongTinKhoaDTO thongTinKhoaDTO = thongTinKhoaMapper.toDto(thongTinKhoa);

        // An entity with an existing ID cannot be created, so this API call must fail
        restThongTinKhoaMockMvc.perform(post("/api/thong-tin-khoas").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(thongTinKhoaDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ThongTinKhoa in the database
        List<ThongTinKhoa> thongTinKhoaList = thongTinKhoaRepository.findAll();
        assertThat(thongTinKhoaList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNamIsRequired() throws Exception {
        int databaseSizeBeforeTest = thongTinKhoaRepository.findAll().size();
        // set the field null
        thongTinKhoa.setNam(null);

        // Create the ThongTinKhoa, which fails.
        ThongTinKhoaDTO thongTinKhoaDTO = thongTinKhoaMapper.toDto(thongTinKhoa);

        restThongTinKhoaMockMvc.perform(post("/api/thong-tin-khoas").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(thongTinKhoaDTO)))
            .andExpect(status().isBadRequest());

        List<ThongTinKhoa> thongTinKhoaList = thongTinKhoaRepository.findAll();
        assertThat(thongTinKhoaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoas() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList
        restThongTinKhoaMockMvc.perform(get("/api/thong-tin-khoas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(thongTinKhoa.getId().intValue())))
            .andExpect(jsonPath("$.[*].thoiGianNhanBenh").value(hasItem(DEFAULT_THOI_GIAN_NHAN_BENH.toString())))
            .andExpect(jsonPath("$.[*].thoiGianChuyenKhoa").value(hasItem(DEFAULT_THOI_GIAN_CHUYEN_KHOA.toString())))
            .andExpect(jsonPath("$.[*].khoaId").value(hasItem(DEFAULT_KHOA_ID)))
            .andExpect(jsonPath("$.[*].khoaChuyenDen").value(hasItem(DEFAULT_KHOA_CHUYEN_DEN)))
            .andExpect(jsonPath("$.[*].khoaChuyenDi").value(hasItem(DEFAULT_KHOA_CHUYEN_DI)))
            .andExpect(jsonPath("$.[*].nhanVienNhanBenh").value(hasItem(DEFAULT_NHAN_VIEN_NHAN_BENH)))
            .andExpect(jsonPath("$.[*].nhanVienChuyenKhoa").value(hasItem(DEFAULT_NHAN_VIEN_CHUYEN_KHOA)))
            .andExpect(jsonPath("$.[*].soNgay").value(hasItem(DEFAULT_SO_NGAY.intValue())))
            .andExpect(jsonPath("$.[*].trangThai").value(hasItem(DEFAULT_TRANG_THAI)))
            .andExpect(jsonPath("$.[*].nam").value(hasItem(DEFAULT_NAM)));
    }
    
    @Test
    @Transactional
    public void getThongTinKhoa() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get the thongTinKhoa
        restThongTinKhoaMockMvc.perform(get("/api/thong-tin-khoas/{id}", thongTinKhoa.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(thongTinKhoa.getId().intValue()))
            .andExpect(jsonPath("$.thoiGianNhanBenh").value(DEFAULT_THOI_GIAN_NHAN_BENH.toString()))
            .andExpect(jsonPath("$.thoiGianChuyenKhoa").value(DEFAULT_THOI_GIAN_CHUYEN_KHOA.toString()))
            .andExpect(jsonPath("$.khoaId").value(DEFAULT_KHOA_ID))
            .andExpect(jsonPath("$.khoaChuyenDen").value(DEFAULT_KHOA_CHUYEN_DEN))
            .andExpect(jsonPath("$.khoaChuyenDi").value(DEFAULT_KHOA_CHUYEN_DI))
            .andExpect(jsonPath("$.nhanVienNhanBenh").value(DEFAULT_NHAN_VIEN_NHAN_BENH))
            .andExpect(jsonPath("$.nhanVienChuyenKhoa").value(DEFAULT_NHAN_VIEN_CHUYEN_KHOA))
            .andExpect(jsonPath("$.soNgay").value(DEFAULT_SO_NGAY.intValue()))
            .andExpect(jsonPath("$.trangThai").value(DEFAULT_TRANG_THAI))
            .andExpect(jsonPath("$.nam").value(DEFAULT_NAM));
    }


    @Test
    @Transactional
    public void getThongTinKhoasByIdFiltering() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        Long id = thongTinKhoa.getId();

        defaultThongTinKhoaShouldBeFound("id.equals=" + id);
        defaultThongTinKhoaShouldNotBeFound("id.notEquals=" + id);

        defaultThongTinKhoaShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultThongTinKhoaShouldNotBeFound("id.greaterThan=" + id);

        defaultThongTinKhoaShouldBeFound("id.lessThanOrEqual=" + id);
        defaultThongTinKhoaShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllThongTinKhoasByThoiGianNhanBenhIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where thoiGianNhanBenh equals to DEFAULT_THOI_GIAN_NHAN_BENH
        defaultThongTinKhoaShouldBeFound("thoiGianNhanBenh.equals=" + DEFAULT_THOI_GIAN_NHAN_BENH);

        // Get all the thongTinKhoaList where thoiGianNhanBenh equals to UPDATED_THOI_GIAN_NHAN_BENH
        defaultThongTinKhoaShouldNotBeFound("thoiGianNhanBenh.equals=" + UPDATED_THOI_GIAN_NHAN_BENH);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByThoiGianNhanBenhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where thoiGianNhanBenh not equals to DEFAULT_THOI_GIAN_NHAN_BENH
        defaultThongTinKhoaShouldNotBeFound("thoiGianNhanBenh.notEquals=" + DEFAULT_THOI_GIAN_NHAN_BENH);

        // Get all the thongTinKhoaList where thoiGianNhanBenh not equals to UPDATED_THOI_GIAN_NHAN_BENH
        defaultThongTinKhoaShouldBeFound("thoiGianNhanBenh.notEquals=" + UPDATED_THOI_GIAN_NHAN_BENH);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByThoiGianNhanBenhIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where thoiGianNhanBenh in DEFAULT_THOI_GIAN_NHAN_BENH or UPDATED_THOI_GIAN_NHAN_BENH
        defaultThongTinKhoaShouldBeFound("thoiGianNhanBenh.in=" + DEFAULT_THOI_GIAN_NHAN_BENH + "," + UPDATED_THOI_GIAN_NHAN_BENH);

        // Get all the thongTinKhoaList where thoiGianNhanBenh equals to UPDATED_THOI_GIAN_NHAN_BENH
        defaultThongTinKhoaShouldNotBeFound("thoiGianNhanBenh.in=" + UPDATED_THOI_GIAN_NHAN_BENH);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByThoiGianNhanBenhIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where thoiGianNhanBenh is not null
        defaultThongTinKhoaShouldBeFound("thoiGianNhanBenh.specified=true");

        // Get all the thongTinKhoaList where thoiGianNhanBenh is null
        defaultThongTinKhoaShouldNotBeFound("thoiGianNhanBenh.specified=false");
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByThoiGianNhanBenhIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where thoiGianNhanBenh is greater than or equal to DEFAULT_THOI_GIAN_NHAN_BENH
        defaultThongTinKhoaShouldBeFound("thoiGianNhanBenh.greaterThanOrEqual=" + DEFAULT_THOI_GIAN_NHAN_BENH);

        // Get all the thongTinKhoaList where thoiGianNhanBenh is greater than or equal to UPDATED_THOI_GIAN_NHAN_BENH
        defaultThongTinKhoaShouldNotBeFound("thoiGianNhanBenh.greaterThanOrEqual=" + UPDATED_THOI_GIAN_NHAN_BENH);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByThoiGianNhanBenhIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where thoiGianNhanBenh is less than or equal to DEFAULT_THOI_GIAN_NHAN_BENH
        defaultThongTinKhoaShouldBeFound("thoiGianNhanBenh.lessThanOrEqual=" + DEFAULT_THOI_GIAN_NHAN_BENH);

        // Get all the thongTinKhoaList where thoiGianNhanBenh is less than or equal to SMALLER_THOI_GIAN_NHAN_BENH
        defaultThongTinKhoaShouldNotBeFound("thoiGianNhanBenh.lessThanOrEqual=" + SMALLER_THOI_GIAN_NHAN_BENH);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByThoiGianNhanBenhIsLessThanSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where thoiGianNhanBenh is less than DEFAULT_THOI_GIAN_NHAN_BENH
        defaultThongTinKhoaShouldNotBeFound("thoiGianNhanBenh.lessThan=" + DEFAULT_THOI_GIAN_NHAN_BENH);

        // Get all the thongTinKhoaList where thoiGianNhanBenh is less than UPDATED_THOI_GIAN_NHAN_BENH
        defaultThongTinKhoaShouldBeFound("thoiGianNhanBenh.lessThan=" + UPDATED_THOI_GIAN_NHAN_BENH);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByThoiGianNhanBenhIsGreaterThanSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where thoiGianNhanBenh is greater than DEFAULT_THOI_GIAN_NHAN_BENH
        defaultThongTinKhoaShouldNotBeFound("thoiGianNhanBenh.greaterThan=" + DEFAULT_THOI_GIAN_NHAN_BENH);

        // Get all the thongTinKhoaList where thoiGianNhanBenh is greater than SMALLER_THOI_GIAN_NHAN_BENH
        defaultThongTinKhoaShouldBeFound("thoiGianNhanBenh.greaterThan=" + SMALLER_THOI_GIAN_NHAN_BENH);
    }


    @Test
    @Transactional
    public void getAllThongTinKhoasByThoiGianChuyenKhoaIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where thoiGianChuyenKhoa equals to DEFAULT_THOI_GIAN_CHUYEN_KHOA
        defaultThongTinKhoaShouldBeFound("thoiGianChuyenKhoa.equals=" + DEFAULT_THOI_GIAN_CHUYEN_KHOA);

        // Get all the thongTinKhoaList where thoiGianChuyenKhoa equals to UPDATED_THOI_GIAN_CHUYEN_KHOA
        defaultThongTinKhoaShouldNotBeFound("thoiGianChuyenKhoa.equals=" + UPDATED_THOI_GIAN_CHUYEN_KHOA);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByThoiGianChuyenKhoaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where thoiGianChuyenKhoa not equals to DEFAULT_THOI_GIAN_CHUYEN_KHOA
        defaultThongTinKhoaShouldNotBeFound("thoiGianChuyenKhoa.notEquals=" + DEFAULT_THOI_GIAN_CHUYEN_KHOA);

        // Get all the thongTinKhoaList where thoiGianChuyenKhoa not equals to UPDATED_THOI_GIAN_CHUYEN_KHOA
        defaultThongTinKhoaShouldBeFound("thoiGianChuyenKhoa.notEquals=" + UPDATED_THOI_GIAN_CHUYEN_KHOA);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByThoiGianChuyenKhoaIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where thoiGianChuyenKhoa in DEFAULT_THOI_GIAN_CHUYEN_KHOA or UPDATED_THOI_GIAN_CHUYEN_KHOA
        defaultThongTinKhoaShouldBeFound("thoiGianChuyenKhoa.in=" + DEFAULT_THOI_GIAN_CHUYEN_KHOA + "," + UPDATED_THOI_GIAN_CHUYEN_KHOA);

        // Get all the thongTinKhoaList where thoiGianChuyenKhoa equals to UPDATED_THOI_GIAN_CHUYEN_KHOA
        defaultThongTinKhoaShouldNotBeFound("thoiGianChuyenKhoa.in=" + UPDATED_THOI_GIAN_CHUYEN_KHOA);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByThoiGianChuyenKhoaIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where thoiGianChuyenKhoa is not null
        defaultThongTinKhoaShouldBeFound("thoiGianChuyenKhoa.specified=true");

        // Get all the thongTinKhoaList where thoiGianChuyenKhoa is null
        defaultThongTinKhoaShouldNotBeFound("thoiGianChuyenKhoa.specified=false");
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByThoiGianChuyenKhoaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where thoiGianChuyenKhoa is greater than or equal to DEFAULT_THOI_GIAN_CHUYEN_KHOA
        defaultThongTinKhoaShouldBeFound("thoiGianChuyenKhoa.greaterThanOrEqual=" + DEFAULT_THOI_GIAN_CHUYEN_KHOA);

        // Get all the thongTinKhoaList where thoiGianChuyenKhoa is greater than or equal to UPDATED_THOI_GIAN_CHUYEN_KHOA
        defaultThongTinKhoaShouldNotBeFound("thoiGianChuyenKhoa.greaterThanOrEqual=" + UPDATED_THOI_GIAN_CHUYEN_KHOA);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByThoiGianChuyenKhoaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where thoiGianChuyenKhoa is less than or equal to DEFAULT_THOI_GIAN_CHUYEN_KHOA
        defaultThongTinKhoaShouldBeFound("thoiGianChuyenKhoa.lessThanOrEqual=" + DEFAULT_THOI_GIAN_CHUYEN_KHOA);

        // Get all the thongTinKhoaList where thoiGianChuyenKhoa is less than or equal to SMALLER_THOI_GIAN_CHUYEN_KHOA
        defaultThongTinKhoaShouldNotBeFound("thoiGianChuyenKhoa.lessThanOrEqual=" + SMALLER_THOI_GIAN_CHUYEN_KHOA);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByThoiGianChuyenKhoaIsLessThanSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where thoiGianChuyenKhoa is less than DEFAULT_THOI_GIAN_CHUYEN_KHOA
        defaultThongTinKhoaShouldNotBeFound("thoiGianChuyenKhoa.lessThan=" + DEFAULT_THOI_GIAN_CHUYEN_KHOA);

        // Get all the thongTinKhoaList where thoiGianChuyenKhoa is less than UPDATED_THOI_GIAN_CHUYEN_KHOA
        defaultThongTinKhoaShouldBeFound("thoiGianChuyenKhoa.lessThan=" + UPDATED_THOI_GIAN_CHUYEN_KHOA);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByThoiGianChuyenKhoaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where thoiGianChuyenKhoa is greater than DEFAULT_THOI_GIAN_CHUYEN_KHOA
        defaultThongTinKhoaShouldNotBeFound("thoiGianChuyenKhoa.greaterThan=" + DEFAULT_THOI_GIAN_CHUYEN_KHOA);

        // Get all the thongTinKhoaList where thoiGianChuyenKhoa is greater than SMALLER_THOI_GIAN_CHUYEN_KHOA
        defaultThongTinKhoaShouldBeFound("thoiGianChuyenKhoa.greaterThan=" + SMALLER_THOI_GIAN_CHUYEN_KHOA);
    }


    @Test
    @Transactional
    public void getAllThongTinKhoasByKhoaIdIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where khoaId equals to DEFAULT_KHOA_ID
        defaultThongTinKhoaShouldBeFound("khoaId.equals=" + DEFAULT_KHOA_ID);

        // Get all the thongTinKhoaList where khoaId equals to UPDATED_KHOA_ID
        defaultThongTinKhoaShouldNotBeFound("khoaId.equals=" + UPDATED_KHOA_ID);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByKhoaIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where khoaId not equals to DEFAULT_KHOA_ID
        defaultThongTinKhoaShouldNotBeFound("khoaId.notEquals=" + DEFAULT_KHOA_ID);

        // Get all the thongTinKhoaList where khoaId not equals to UPDATED_KHOA_ID
        defaultThongTinKhoaShouldBeFound("khoaId.notEquals=" + UPDATED_KHOA_ID);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByKhoaIdIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where khoaId in DEFAULT_KHOA_ID or UPDATED_KHOA_ID
        defaultThongTinKhoaShouldBeFound("khoaId.in=" + DEFAULT_KHOA_ID + "," + UPDATED_KHOA_ID);

        // Get all the thongTinKhoaList where khoaId equals to UPDATED_KHOA_ID
        defaultThongTinKhoaShouldNotBeFound("khoaId.in=" + UPDATED_KHOA_ID);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByKhoaIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where khoaId is not null
        defaultThongTinKhoaShouldBeFound("khoaId.specified=true");

        // Get all the thongTinKhoaList where khoaId is null
        defaultThongTinKhoaShouldNotBeFound("khoaId.specified=false");
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByKhoaIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where khoaId is greater than or equal to DEFAULT_KHOA_ID
        defaultThongTinKhoaShouldBeFound("khoaId.greaterThanOrEqual=" + DEFAULT_KHOA_ID);

        // Get all the thongTinKhoaList where khoaId is greater than or equal to UPDATED_KHOA_ID
        defaultThongTinKhoaShouldNotBeFound("khoaId.greaterThanOrEqual=" + UPDATED_KHOA_ID);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByKhoaIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where khoaId is less than or equal to DEFAULT_KHOA_ID
        defaultThongTinKhoaShouldBeFound("khoaId.lessThanOrEqual=" + DEFAULT_KHOA_ID);

        // Get all the thongTinKhoaList where khoaId is less than or equal to SMALLER_KHOA_ID
        defaultThongTinKhoaShouldNotBeFound("khoaId.lessThanOrEqual=" + SMALLER_KHOA_ID);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByKhoaIdIsLessThanSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where khoaId is less than DEFAULT_KHOA_ID
        defaultThongTinKhoaShouldNotBeFound("khoaId.lessThan=" + DEFAULT_KHOA_ID);

        // Get all the thongTinKhoaList where khoaId is less than UPDATED_KHOA_ID
        defaultThongTinKhoaShouldBeFound("khoaId.lessThan=" + UPDATED_KHOA_ID);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByKhoaIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where khoaId is greater than DEFAULT_KHOA_ID
        defaultThongTinKhoaShouldNotBeFound("khoaId.greaterThan=" + DEFAULT_KHOA_ID);

        // Get all the thongTinKhoaList where khoaId is greater than SMALLER_KHOA_ID
        defaultThongTinKhoaShouldBeFound("khoaId.greaterThan=" + SMALLER_KHOA_ID);
    }


    @Test
    @Transactional
    public void getAllThongTinKhoasByKhoaChuyenDenIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where khoaChuyenDen equals to DEFAULT_KHOA_CHUYEN_DEN
        defaultThongTinKhoaShouldBeFound("khoaChuyenDen.equals=" + DEFAULT_KHOA_CHUYEN_DEN);

        // Get all the thongTinKhoaList where khoaChuyenDen equals to UPDATED_KHOA_CHUYEN_DEN
        defaultThongTinKhoaShouldNotBeFound("khoaChuyenDen.equals=" + UPDATED_KHOA_CHUYEN_DEN);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByKhoaChuyenDenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where khoaChuyenDen not equals to DEFAULT_KHOA_CHUYEN_DEN
        defaultThongTinKhoaShouldNotBeFound("khoaChuyenDen.notEquals=" + DEFAULT_KHOA_CHUYEN_DEN);

        // Get all the thongTinKhoaList where khoaChuyenDen not equals to UPDATED_KHOA_CHUYEN_DEN
        defaultThongTinKhoaShouldBeFound("khoaChuyenDen.notEquals=" + UPDATED_KHOA_CHUYEN_DEN);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByKhoaChuyenDenIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where khoaChuyenDen in DEFAULT_KHOA_CHUYEN_DEN or UPDATED_KHOA_CHUYEN_DEN
        defaultThongTinKhoaShouldBeFound("khoaChuyenDen.in=" + DEFAULT_KHOA_CHUYEN_DEN + "," + UPDATED_KHOA_CHUYEN_DEN);

        // Get all the thongTinKhoaList where khoaChuyenDen equals to UPDATED_KHOA_CHUYEN_DEN
        defaultThongTinKhoaShouldNotBeFound("khoaChuyenDen.in=" + UPDATED_KHOA_CHUYEN_DEN);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByKhoaChuyenDenIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where khoaChuyenDen is not null
        defaultThongTinKhoaShouldBeFound("khoaChuyenDen.specified=true");

        // Get all the thongTinKhoaList where khoaChuyenDen is null
        defaultThongTinKhoaShouldNotBeFound("khoaChuyenDen.specified=false");
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByKhoaChuyenDenIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where khoaChuyenDen is greater than or equal to DEFAULT_KHOA_CHUYEN_DEN
        defaultThongTinKhoaShouldBeFound("khoaChuyenDen.greaterThanOrEqual=" + DEFAULT_KHOA_CHUYEN_DEN);

        // Get all the thongTinKhoaList where khoaChuyenDen is greater than or equal to UPDATED_KHOA_CHUYEN_DEN
        defaultThongTinKhoaShouldNotBeFound("khoaChuyenDen.greaterThanOrEqual=" + UPDATED_KHOA_CHUYEN_DEN);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByKhoaChuyenDenIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where khoaChuyenDen is less than or equal to DEFAULT_KHOA_CHUYEN_DEN
        defaultThongTinKhoaShouldBeFound("khoaChuyenDen.lessThanOrEqual=" + DEFAULT_KHOA_CHUYEN_DEN);

        // Get all the thongTinKhoaList where khoaChuyenDen is less than or equal to SMALLER_KHOA_CHUYEN_DEN
        defaultThongTinKhoaShouldNotBeFound("khoaChuyenDen.lessThanOrEqual=" + SMALLER_KHOA_CHUYEN_DEN);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByKhoaChuyenDenIsLessThanSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where khoaChuyenDen is less than DEFAULT_KHOA_CHUYEN_DEN
        defaultThongTinKhoaShouldNotBeFound("khoaChuyenDen.lessThan=" + DEFAULT_KHOA_CHUYEN_DEN);

        // Get all the thongTinKhoaList where khoaChuyenDen is less than UPDATED_KHOA_CHUYEN_DEN
        defaultThongTinKhoaShouldBeFound("khoaChuyenDen.lessThan=" + UPDATED_KHOA_CHUYEN_DEN);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByKhoaChuyenDenIsGreaterThanSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where khoaChuyenDen is greater than DEFAULT_KHOA_CHUYEN_DEN
        defaultThongTinKhoaShouldNotBeFound("khoaChuyenDen.greaterThan=" + DEFAULT_KHOA_CHUYEN_DEN);

        // Get all the thongTinKhoaList where khoaChuyenDen is greater than SMALLER_KHOA_CHUYEN_DEN
        defaultThongTinKhoaShouldBeFound("khoaChuyenDen.greaterThan=" + SMALLER_KHOA_CHUYEN_DEN);
    }


    @Test
    @Transactional
    public void getAllThongTinKhoasByKhoaChuyenDiIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where khoaChuyenDi equals to DEFAULT_KHOA_CHUYEN_DI
        defaultThongTinKhoaShouldBeFound("khoaChuyenDi.equals=" + DEFAULT_KHOA_CHUYEN_DI);

        // Get all the thongTinKhoaList where khoaChuyenDi equals to UPDATED_KHOA_CHUYEN_DI
        defaultThongTinKhoaShouldNotBeFound("khoaChuyenDi.equals=" + UPDATED_KHOA_CHUYEN_DI);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByKhoaChuyenDiIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where khoaChuyenDi not equals to DEFAULT_KHOA_CHUYEN_DI
        defaultThongTinKhoaShouldNotBeFound("khoaChuyenDi.notEquals=" + DEFAULT_KHOA_CHUYEN_DI);

        // Get all the thongTinKhoaList where khoaChuyenDi not equals to UPDATED_KHOA_CHUYEN_DI
        defaultThongTinKhoaShouldBeFound("khoaChuyenDi.notEquals=" + UPDATED_KHOA_CHUYEN_DI);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByKhoaChuyenDiIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where khoaChuyenDi in DEFAULT_KHOA_CHUYEN_DI or UPDATED_KHOA_CHUYEN_DI
        defaultThongTinKhoaShouldBeFound("khoaChuyenDi.in=" + DEFAULT_KHOA_CHUYEN_DI + "," + UPDATED_KHOA_CHUYEN_DI);

        // Get all the thongTinKhoaList where khoaChuyenDi equals to UPDATED_KHOA_CHUYEN_DI
        defaultThongTinKhoaShouldNotBeFound("khoaChuyenDi.in=" + UPDATED_KHOA_CHUYEN_DI);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByKhoaChuyenDiIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where khoaChuyenDi is not null
        defaultThongTinKhoaShouldBeFound("khoaChuyenDi.specified=true");

        // Get all the thongTinKhoaList where khoaChuyenDi is null
        defaultThongTinKhoaShouldNotBeFound("khoaChuyenDi.specified=false");
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByKhoaChuyenDiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where khoaChuyenDi is greater than or equal to DEFAULT_KHOA_CHUYEN_DI
        defaultThongTinKhoaShouldBeFound("khoaChuyenDi.greaterThanOrEqual=" + DEFAULT_KHOA_CHUYEN_DI);

        // Get all the thongTinKhoaList where khoaChuyenDi is greater than or equal to UPDATED_KHOA_CHUYEN_DI
        defaultThongTinKhoaShouldNotBeFound("khoaChuyenDi.greaterThanOrEqual=" + UPDATED_KHOA_CHUYEN_DI);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByKhoaChuyenDiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where khoaChuyenDi is less than or equal to DEFAULT_KHOA_CHUYEN_DI
        defaultThongTinKhoaShouldBeFound("khoaChuyenDi.lessThanOrEqual=" + DEFAULT_KHOA_CHUYEN_DI);

        // Get all the thongTinKhoaList where khoaChuyenDi is less than or equal to SMALLER_KHOA_CHUYEN_DI
        defaultThongTinKhoaShouldNotBeFound("khoaChuyenDi.lessThanOrEqual=" + SMALLER_KHOA_CHUYEN_DI);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByKhoaChuyenDiIsLessThanSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where khoaChuyenDi is less than DEFAULT_KHOA_CHUYEN_DI
        defaultThongTinKhoaShouldNotBeFound("khoaChuyenDi.lessThan=" + DEFAULT_KHOA_CHUYEN_DI);

        // Get all the thongTinKhoaList where khoaChuyenDi is less than UPDATED_KHOA_CHUYEN_DI
        defaultThongTinKhoaShouldBeFound("khoaChuyenDi.lessThan=" + UPDATED_KHOA_CHUYEN_DI);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByKhoaChuyenDiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where khoaChuyenDi is greater than DEFAULT_KHOA_CHUYEN_DI
        defaultThongTinKhoaShouldNotBeFound("khoaChuyenDi.greaterThan=" + DEFAULT_KHOA_CHUYEN_DI);

        // Get all the thongTinKhoaList where khoaChuyenDi is greater than SMALLER_KHOA_CHUYEN_DI
        defaultThongTinKhoaShouldBeFound("khoaChuyenDi.greaterThan=" + SMALLER_KHOA_CHUYEN_DI);
    }


    @Test
    @Transactional
    public void getAllThongTinKhoasByNhanVienNhanBenhIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where nhanVienNhanBenh equals to DEFAULT_NHAN_VIEN_NHAN_BENH
        defaultThongTinKhoaShouldBeFound("nhanVienNhanBenh.equals=" + DEFAULT_NHAN_VIEN_NHAN_BENH);

        // Get all the thongTinKhoaList where nhanVienNhanBenh equals to UPDATED_NHAN_VIEN_NHAN_BENH
        defaultThongTinKhoaShouldNotBeFound("nhanVienNhanBenh.equals=" + UPDATED_NHAN_VIEN_NHAN_BENH);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByNhanVienNhanBenhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where nhanVienNhanBenh not equals to DEFAULT_NHAN_VIEN_NHAN_BENH
        defaultThongTinKhoaShouldNotBeFound("nhanVienNhanBenh.notEquals=" + DEFAULT_NHAN_VIEN_NHAN_BENH);

        // Get all the thongTinKhoaList where nhanVienNhanBenh not equals to UPDATED_NHAN_VIEN_NHAN_BENH
        defaultThongTinKhoaShouldBeFound("nhanVienNhanBenh.notEquals=" + UPDATED_NHAN_VIEN_NHAN_BENH);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByNhanVienNhanBenhIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where nhanVienNhanBenh in DEFAULT_NHAN_VIEN_NHAN_BENH or UPDATED_NHAN_VIEN_NHAN_BENH
        defaultThongTinKhoaShouldBeFound("nhanVienNhanBenh.in=" + DEFAULT_NHAN_VIEN_NHAN_BENH + "," + UPDATED_NHAN_VIEN_NHAN_BENH);

        // Get all the thongTinKhoaList where nhanVienNhanBenh equals to UPDATED_NHAN_VIEN_NHAN_BENH
        defaultThongTinKhoaShouldNotBeFound("nhanVienNhanBenh.in=" + UPDATED_NHAN_VIEN_NHAN_BENH);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByNhanVienNhanBenhIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where nhanVienNhanBenh is not null
        defaultThongTinKhoaShouldBeFound("nhanVienNhanBenh.specified=true");

        // Get all the thongTinKhoaList where nhanVienNhanBenh is null
        defaultThongTinKhoaShouldNotBeFound("nhanVienNhanBenh.specified=false");
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByNhanVienNhanBenhIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where nhanVienNhanBenh is greater than or equal to DEFAULT_NHAN_VIEN_NHAN_BENH
        defaultThongTinKhoaShouldBeFound("nhanVienNhanBenh.greaterThanOrEqual=" + DEFAULT_NHAN_VIEN_NHAN_BENH);

        // Get all the thongTinKhoaList where nhanVienNhanBenh is greater than or equal to UPDATED_NHAN_VIEN_NHAN_BENH
        defaultThongTinKhoaShouldNotBeFound("nhanVienNhanBenh.greaterThanOrEqual=" + UPDATED_NHAN_VIEN_NHAN_BENH);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByNhanVienNhanBenhIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where nhanVienNhanBenh is less than or equal to DEFAULT_NHAN_VIEN_NHAN_BENH
        defaultThongTinKhoaShouldBeFound("nhanVienNhanBenh.lessThanOrEqual=" + DEFAULT_NHAN_VIEN_NHAN_BENH);

        // Get all the thongTinKhoaList where nhanVienNhanBenh is less than or equal to SMALLER_NHAN_VIEN_NHAN_BENH
        defaultThongTinKhoaShouldNotBeFound("nhanVienNhanBenh.lessThanOrEqual=" + SMALLER_NHAN_VIEN_NHAN_BENH);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByNhanVienNhanBenhIsLessThanSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where nhanVienNhanBenh is less than DEFAULT_NHAN_VIEN_NHAN_BENH
        defaultThongTinKhoaShouldNotBeFound("nhanVienNhanBenh.lessThan=" + DEFAULT_NHAN_VIEN_NHAN_BENH);

        // Get all the thongTinKhoaList where nhanVienNhanBenh is less than UPDATED_NHAN_VIEN_NHAN_BENH
        defaultThongTinKhoaShouldBeFound("nhanVienNhanBenh.lessThan=" + UPDATED_NHAN_VIEN_NHAN_BENH);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByNhanVienNhanBenhIsGreaterThanSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where nhanVienNhanBenh is greater than DEFAULT_NHAN_VIEN_NHAN_BENH
        defaultThongTinKhoaShouldNotBeFound("nhanVienNhanBenh.greaterThan=" + DEFAULT_NHAN_VIEN_NHAN_BENH);

        // Get all the thongTinKhoaList where nhanVienNhanBenh is greater than SMALLER_NHAN_VIEN_NHAN_BENH
        defaultThongTinKhoaShouldBeFound("nhanVienNhanBenh.greaterThan=" + SMALLER_NHAN_VIEN_NHAN_BENH);
    }


    @Test
    @Transactional
    public void getAllThongTinKhoasByNhanVienChuyenKhoaIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where nhanVienChuyenKhoa equals to DEFAULT_NHAN_VIEN_CHUYEN_KHOA
        defaultThongTinKhoaShouldBeFound("nhanVienChuyenKhoa.equals=" + DEFAULT_NHAN_VIEN_CHUYEN_KHOA);

        // Get all the thongTinKhoaList where nhanVienChuyenKhoa equals to UPDATED_NHAN_VIEN_CHUYEN_KHOA
        defaultThongTinKhoaShouldNotBeFound("nhanVienChuyenKhoa.equals=" + UPDATED_NHAN_VIEN_CHUYEN_KHOA);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByNhanVienChuyenKhoaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where nhanVienChuyenKhoa not equals to DEFAULT_NHAN_VIEN_CHUYEN_KHOA
        defaultThongTinKhoaShouldNotBeFound("nhanVienChuyenKhoa.notEquals=" + DEFAULT_NHAN_VIEN_CHUYEN_KHOA);

        // Get all the thongTinKhoaList where nhanVienChuyenKhoa not equals to UPDATED_NHAN_VIEN_CHUYEN_KHOA
        defaultThongTinKhoaShouldBeFound("nhanVienChuyenKhoa.notEquals=" + UPDATED_NHAN_VIEN_CHUYEN_KHOA);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByNhanVienChuyenKhoaIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where nhanVienChuyenKhoa in DEFAULT_NHAN_VIEN_CHUYEN_KHOA or UPDATED_NHAN_VIEN_CHUYEN_KHOA
        defaultThongTinKhoaShouldBeFound("nhanVienChuyenKhoa.in=" + DEFAULT_NHAN_VIEN_CHUYEN_KHOA + "," + UPDATED_NHAN_VIEN_CHUYEN_KHOA);

        // Get all the thongTinKhoaList where nhanVienChuyenKhoa equals to UPDATED_NHAN_VIEN_CHUYEN_KHOA
        defaultThongTinKhoaShouldNotBeFound("nhanVienChuyenKhoa.in=" + UPDATED_NHAN_VIEN_CHUYEN_KHOA);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByNhanVienChuyenKhoaIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where nhanVienChuyenKhoa is not null
        defaultThongTinKhoaShouldBeFound("nhanVienChuyenKhoa.specified=true");

        // Get all the thongTinKhoaList where nhanVienChuyenKhoa is null
        defaultThongTinKhoaShouldNotBeFound("nhanVienChuyenKhoa.specified=false");
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByNhanVienChuyenKhoaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where nhanVienChuyenKhoa is greater than or equal to DEFAULT_NHAN_VIEN_CHUYEN_KHOA
        defaultThongTinKhoaShouldBeFound("nhanVienChuyenKhoa.greaterThanOrEqual=" + DEFAULT_NHAN_VIEN_CHUYEN_KHOA);

        // Get all the thongTinKhoaList where nhanVienChuyenKhoa is greater than or equal to UPDATED_NHAN_VIEN_CHUYEN_KHOA
        defaultThongTinKhoaShouldNotBeFound("nhanVienChuyenKhoa.greaterThanOrEqual=" + UPDATED_NHAN_VIEN_CHUYEN_KHOA);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByNhanVienChuyenKhoaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where nhanVienChuyenKhoa is less than or equal to DEFAULT_NHAN_VIEN_CHUYEN_KHOA
        defaultThongTinKhoaShouldBeFound("nhanVienChuyenKhoa.lessThanOrEqual=" + DEFAULT_NHAN_VIEN_CHUYEN_KHOA);

        // Get all the thongTinKhoaList where nhanVienChuyenKhoa is less than or equal to SMALLER_NHAN_VIEN_CHUYEN_KHOA
        defaultThongTinKhoaShouldNotBeFound("nhanVienChuyenKhoa.lessThanOrEqual=" + SMALLER_NHAN_VIEN_CHUYEN_KHOA);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByNhanVienChuyenKhoaIsLessThanSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where nhanVienChuyenKhoa is less than DEFAULT_NHAN_VIEN_CHUYEN_KHOA
        defaultThongTinKhoaShouldNotBeFound("nhanVienChuyenKhoa.lessThan=" + DEFAULT_NHAN_VIEN_CHUYEN_KHOA);

        // Get all the thongTinKhoaList where nhanVienChuyenKhoa is less than UPDATED_NHAN_VIEN_CHUYEN_KHOA
        defaultThongTinKhoaShouldBeFound("nhanVienChuyenKhoa.lessThan=" + UPDATED_NHAN_VIEN_CHUYEN_KHOA);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByNhanVienChuyenKhoaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where nhanVienChuyenKhoa is greater than DEFAULT_NHAN_VIEN_CHUYEN_KHOA
        defaultThongTinKhoaShouldNotBeFound("nhanVienChuyenKhoa.greaterThan=" + DEFAULT_NHAN_VIEN_CHUYEN_KHOA);

        // Get all the thongTinKhoaList where nhanVienChuyenKhoa is greater than SMALLER_NHAN_VIEN_CHUYEN_KHOA
        defaultThongTinKhoaShouldBeFound("nhanVienChuyenKhoa.greaterThan=" + SMALLER_NHAN_VIEN_CHUYEN_KHOA);
    }


    @Test
    @Transactional
    public void getAllThongTinKhoasBySoNgayIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where soNgay equals to DEFAULT_SO_NGAY
        defaultThongTinKhoaShouldBeFound("soNgay.equals=" + DEFAULT_SO_NGAY);

        // Get all the thongTinKhoaList where soNgay equals to UPDATED_SO_NGAY
        defaultThongTinKhoaShouldNotBeFound("soNgay.equals=" + UPDATED_SO_NGAY);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasBySoNgayIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where soNgay not equals to DEFAULT_SO_NGAY
        defaultThongTinKhoaShouldNotBeFound("soNgay.notEquals=" + DEFAULT_SO_NGAY);

        // Get all the thongTinKhoaList where soNgay not equals to UPDATED_SO_NGAY
        defaultThongTinKhoaShouldBeFound("soNgay.notEquals=" + UPDATED_SO_NGAY);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasBySoNgayIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where soNgay in DEFAULT_SO_NGAY or UPDATED_SO_NGAY
        defaultThongTinKhoaShouldBeFound("soNgay.in=" + DEFAULT_SO_NGAY + "," + UPDATED_SO_NGAY);

        // Get all the thongTinKhoaList where soNgay equals to UPDATED_SO_NGAY
        defaultThongTinKhoaShouldNotBeFound("soNgay.in=" + UPDATED_SO_NGAY);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasBySoNgayIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where soNgay is not null
        defaultThongTinKhoaShouldBeFound("soNgay.specified=true");

        // Get all the thongTinKhoaList where soNgay is null
        defaultThongTinKhoaShouldNotBeFound("soNgay.specified=false");
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasBySoNgayIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where soNgay is greater than or equal to DEFAULT_SO_NGAY
        defaultThongTinKhoaShouldBeFound("soNgay.greaterThanOrEqual=" + DEFAULT_SO_NGAY);

        // Get all the thongTinKhoaList where soNgay is greater than or equal to UPDATED_SO_NGAY
        defaultThongTinKhoaShouldNotBeFound("soNgay.greaterThanOrEqual=" + UPDATED_SO_NGAY);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasBySoNgayIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where soNgay is less than or equal to DEFAULT_SO_NGAY
        defaultThongTinKhoaShouldBeFound("soNgay.lessThanOrEqual=" + DEFAULT_SO_NGAY);

        // Get all the thongTinKhoaList where soNgay is less than or equal to SMALLER_SO_NGAY
        defaultThongTinKhoaShouldNotBeFound("soNgay.lessThanOrEqual=" + SMALLER_SO_NGAY);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasBySoNgayIsLessThanSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where soNgay is less than DEFAULT_SO_NGAY
        defaultThongTinKhoaShouldNotBeFound("soNgay.lessThan=" + DEFAULT_SO_NGAY);

        // Get all the thongTinKhoaList where soNgay is less than UPDATED_SO_NGAY
        defaultThongTinKhoaShouldBeFound("soNgay.lessThan=" + UPDATED_SO_NGAY);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasBySoNgayIsGreaterThanSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where soNgay is greater than DEFAULT_SO_NGAY
        defaultThongTinKhoaShouldNotBeFound("soNgay.greaterThan=" + DEFAULT_SO_NGAY);

        // Get all the thongTinKhoaList where soNgay is greater than SMALLER_SO_NGAY
        defaultThongTinKhoaShouldBeFound("soNgay.greaterThan=" + SMALLER_SO_NGAY);
    }


    @Test
    @Transactional
    public void getAllThongTinKhoasByTrangThaiIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where trangThai equals to DEFAULT_TRANG_THAI
        defaultThongTinKhoaShouldBeFound("trangThai.equals=" + DEFAULT_TRANG_THAI);

        // Get all the thongTinKhoaList where trangThai equals to UPDATED_TRANG_THAI
        defaultThongTinKhoaShouldNotBeFound("trangThai.equals=" + UPDATED_TRANG_THAI);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByTrangThaiIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where trangThai not equals to DEFAULT_TRANG_THAI
        defaultThongTinKhoaShouldNotBeFound("trangThai.notEquals=" + DEFAULT_TRANG_THAI);

        // Get all the thongTinKhoaList where trangThai not equals to UPDATED_TRANG_THAI
        defaultThongTinKhoaShouldBeFound("trangThai.notEquals=" + UPDATED_TRANG_THAI);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByTrangThaiIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where trangThai in DEFAULT_TRANG_THAI or UPDATED_TRANG_THAI
        defaultThongTinKhoaShouldBeFound("trangThai.in=" + DEFAULT_TRANG_THAI + "," + UPDATED_TRANG_THAI);

        // Get all the thongTinKhoaList where trangThai equals to UPDATED_TRANG_THAI
        defaultThongTinKhoaShouldNotBeFound("trangThai.in=" + UPDATED_TRANG_THAI);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByTrangThaiIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where trangThai is not null
        defaultThongTinKhoaShouldBeFound("trangThai.specified=true");

        // Get all the thongTinKhoaList where trangThai is null
        defaultThongTinKhoaShouldNotBeFound("trangThai.specified=false");
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByTrangThaiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where trangThai is greater than or equal to DEFAULT_TRANG_THAI
        defaultThongTinKhoaShouldBeFound("trangThai.greaterThanOrEqual=" + DEFAULT_TRANG_THAI);

        // Get all the thongTinKhoaList where trangThai is greater than or equal to UPDATED_TRANG_THAI
        defaultThongTinKhoaShouldNotBeFound("trangThai.greaterThanOrEqual=" + UPDATED_TRANG_THAI);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByTrangThaiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where trangThai is less than or equal to DEFAULT_TRANG_THAI
        defaultThongTinKhoaShouldBeFound("trangThai.lessThanOrEqual=" + DEFAULT_TRANG_THAI);

        // Get all the thongTinKhoaList where trangThai is less than or equal to SMALLER_TRANG_THAI
        defaultThongTinKhoaShouldNotBeFound("trangThai.lessThanOrEqual=" + SMALLER_TRANG_THAI);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByTrangThaiIsLessThanSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where trangThai is less than DEFAULT_TRANG_THAI
        defaultThongTinKhoaShouldNotBeFound("trangThai.lessThan=" + DEFAULT_TRANG_THAI);

        // Get all the thongTinKhoaList where trangThai is less than UPDATED_TRANG_THAI
        defaultThongTinKhoaShouldBeFound("trangThai.lessThan=" + UPDATED_TRANG_THAI);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByTrangThaiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where trangThai is greater than DEFAULT_TRANG_THAI
        defaultThongTinKhoaShouldNotBeFound("trangThai.greaterThan=" + DEFAULT_TRANG_THAI);

        // Get all the thongTinKhoaList where trangThai is greater than SMALLER_TRANG_THAI
        defaultThongTinKhoaShouldBeFound("trangThai.greaterThan=" + SMALLER_TRANG_THAI);
    }


    @Test
    @Transactional
    public void getAllThongTinKhoasByNamIsEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where nam equals to DEFAULT_NAM
        defaultThongTinKhoaShouldBeFound("nam.equals=" + DEFAULT_NAM);

        // Get all the thongTinKhoaList where nam equals to UPDATED_NAM
        defaultThongTinKhoaShouldNotBeFound("nam.equals=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByNamIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where nam not equals to DEFAULT_NAM
        defaultThongTinKhoaShouldNotBeFound("nam.notEquals=" + DEFAULT_NAM);

        // Get all the thongTinKhoaList where nam not equals to UPDATED_NAM
        defaultThongTinKhoaShouldBeFound("nam.notEquals=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByNamIsInShouldWork() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where nam in DEFAULT_NAM or UPDATED_NAM
        defaultThongTinKhoaShouldBeFound("nam.in=" + DEFAULT_NAM + "," + UPDATED_NAM);

        // Get all the thongTinKhoaList where nam equals to UPDATED_NAM
        defaultThongTinKhoaShouldNotBeFound("nam.in=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByNamIsNullOrNotNull() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where nam is not null
        defaultThongTinKhoaShouldBeFound("nam.specified=true");

        // Get all the thongTinKhoaList where nam is null
        defaultThongTinKhoaShouldNotBeFound("nam.specified=false");
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByNamIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where nam is greater than or equal to DEFAULT_NAM
        defaultThongTinKhoaShouldBeFound("nam.greaterThanOrEqual=" + DEFAULT_NAM);

        // Get all the thongTinKhoaList where nam is greater than or equal to UPDATED_NAM
        defaultThongTinKhoaShouldNotBeFound("nam.greaterThanOrEqual=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByNamIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where nam is less than or equal to DEFAULT_NAM
        defaultThongTinKhoaShouldBeFound("nam.lessThanOrEqual=" + DEFAULT_NAM);

        // Get all the thongTinKhoaList where nam is less than or equal to SMALLER_NAM
        defaultThongTinKhoaShouldNotBeFound("nam.lessThanOrEqual=" + SMALLER_NAM);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByNamIsLessThanSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where nam is less than DEFAULT_NAM
        defaultThongTinKhoaShouldNotBeFound("nam.lessThan=" + DEFAULT_NAM);

        // Get all the thongTinKhoaList where nam is less than UPDATED_NAM
        defaultThongTinKhoaShouldBeFound("nam.lessThan=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllThongTinKhoasByNamIsGreaterThanSomething() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        // Get all the thongTinKhoaList where nam is greater than DEFAULT_NAM
        defaultThongTinKhoaShouldNotBeFound("nam.greaterThan=" + DEFAULT_NAM);

        // Get all the thongTinKhoaList where nam is greater than SMALLER_NAM
        defaultThongTinKhoaShouldBeFound("nam.greaterThan=" + SMALLER_NAM);
    }


    @Test
    @Transactional
    public void getAllThongTinKhoasByBakbIsEqualToSomething() throws Exception {
        // Get already existing entity
        DotDieuTri bakb = thongTinKhoa.getBakb();
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);
        Long bakbId = bakb.getId();

        // Get all the thongTinKhoaList where bakb equals to bakbId
        defaultThongTinKhoaShouldBeFound("bakbId.equals=" + bakbId);

        // Get all the thongTinKhoaList where bakb equals to bakbId + 1
        defaultThongTinKhoaShouldNotBeFound("bakbId.equals=" + (bakbId + 1));
    }


    @Test
    @Transactional
    public void getAllThongTinKhoasByDonViIsEqualToSomething() throws Exception {
        // Get already existing entity
        DotDieuTri donVi = thongTinKhoa.getDonVi();
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);
        Long donViId = donVi.getId();

        // Get all the thongTinKhoaList where donVi equals to donViId
        defaultThongTinKhoaShouldBeFound("donViId.equals=" + donViId);

        // Get all the thongTinKhoaList where donVi equals to donViId + 1
        defaultThongTinKhoaShouldNotBeFound("donViId.equals=" + (donViId + 1));
    }


    @Test
    @Transactional
    public void getAllThongTinKhoasByBenhNhanIsEqualToSomething() throws Exception {
        // Get already existing entity
        DotDieuTri benhNhan = thongTinKhoa.getBenhNhan();
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);
        Long benhNhanId = benhNhan.getId();

        // Get all the thongTinKhoaList where benhNhan equals to benhNhanId
        defaultThongTinKhoaShouldBeFound("benhNhanId.equals=" + benhNhanId);

        // Get all the thongTinKhoaList where benhNhan equals to benhNhanId + 1
        defaultThongTinKhoaShouldNotBeFound("benhNhanId.equals=" + (benhNhanId + 1));
    }


    @Test
    @Transactional
    public void getAllThongTinKhoasByDotDieuTriIsEqualToSomething() throws Exception {
        // Get already existing entity
        DotDieuTri dotDieuTri = thongTinKhoa.getDotDieuTri();
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);
        Long dotDieuTriId = dotDieuTri.getId();

        // Get all the thongTinKhoaList where dotDieuTri equals to dotDieuTriId
        defaultThongTinKhoaShouldBeFound("dotDieuTriId.equals=" + dotDieuTriId);

        // Get all the thongTinKhoaList where dotDieuTri equals to dotDieuTriId + 1
        defaultThongTinKhoaShouldNotBeFound("dotDieuTriId.equals=" + (dotDieuTriId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultThongTinKhoaShouldBeFound(String filter) throws Exception {
        restThongTinKhoaMockMvc.perform(get("/api/thong-tin-khoas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(thongTinKhoa.getId().intValue())))
            .andExpect(jsonPath("$.[*].thoiGianNhanBenh").value(hasItem(DEFAULT_THOI_GIAN_NHAN_BENH.toString())))
            .andExpect(jsonPath("$.[*].thoiGianChuyenKhoa").value(hasItem(DEFAULT_THOI_GIAN_CHUYEN_KHOA.toString())))
            .andExpect(jsonPath("$.[*].khoaId").value(hasItem(DEFAULT_KHOA_ID)))
            .andExpect(jsonPath("$.[*].khoaChuyenDen").value(hasItem(DEFAULT_KHOA_CHUYEN_DEN)))
            .andExpect(jsonPath("$.[*].khoaChuyenDi").value(hasItem(DEFAULT_KHOA_CHUYEN_DI)))
            .andExpect(jsonPath("$.[*].nhanVienNhanBenh").value(hasItem(DEFAULT_NHAN_VIEN_NHAN_BENH)))
            .andExpect(jsonPath("$.[*].nhanVienChuyenKhoa").value(hasItem(DEFAULT_NHAN_VIEN_CHUYEN_KHOA)))
            .andExpect(jsonPath("$.[*].soNgay").value(hasItem(DEFAULT_SO_NGAY.intValue())))
            .andExpect(jsonPath("$.[*].trangThai").value(hasItem(DEFAULT_TRANG_THAI)))
            .andExpect(jsonPath("$.[*].nam").value(hasItem(DEFAULT_NAM)));

        // Check, that the count call also returns 1
        restThongTinKhoaMockMvc.perform(get("/api/thong-tin-khoas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultThongTinKhoaShouldNotBeFound(String filter) throws Exception {
        restThongTinKhoaMockMvc.perform(get("/api/thong-tin-khoas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restThongTinKhoaMockMvc.perform(get("/api/thong-tin-khoas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingThongTinKhoa() throws Exception {
        // Get the thongTinKhoa
        restThongTinKhoaMockMvc.perform(get("/api/thong-tin-khoas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateThongTinKhoa() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        int databaseSizeBeforeUpdate = thongTinKhoaRepository.findAll().size();

        // Update the thongTinKhoa
        ThongTinKhoa updatedThongTinKhoa = thongTinKhoaRepository.findById(thongTinKhoa.getId()).get();
        // Disconnect from session so that the updates on updatedThongTinKhoa are not directly saved in db
        em.detach(updatedThongTinKhoa);
        updatedThongTinKhoa
            .thoiGianNhanBenh(UPDATED_THOI_GIAN_NHAN_BENH)
            .thoiGianChuyenKhoa(UPDATED_THOI_GIAN_CHUYEN_KHOA)
            .khoaId(UPDATED_KHOA_ID)
            .khoaChuyenDen(UPDATED_KHOA_CHUYEN_DEN)
            .khoaChuyenDi(UPDATED_KHOA_CHUYEN_DI)
            .nhanVienNhanBenh(UPDATED_NHAN_VIEN_NHAN_BENH)
            .nhanVienChuyenKhoa(UPDATED_NHAN_VIEN_CHUYEN_KHOA)
            .soNgay(UPDATED_SO_NGAY)
            .trangThai(UPDATED_TRANG_THAI)
            .nam(UPDATED_NAM);
        ThongTinKhoaDTO thongTinKhoaDTO = thongTinKhoaMapper.toDto(updatedThongTinKhoa);

        restThongTinKhoaMockMvc.perform(put("/api/thong-tin-khoas").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(thongTinKhoaDTO)))
            .andExpect(status().isOk());

        // Validate the ThongTinKhoa in the database
        List<ThongTinKhoa> thongTinKhoaList = thongTinKhoaRepository.findAll();
        assertThat(thongTinKhoaList).hasSize(databaseSizeBeforeUpdate);
        ThongTinKhoa testThongTinKhoa = thongTinKhoaList.get(thongTinKhoaList.size() - 1);
        assertThat(testThongTinKhoa.getThoiGianNhanBenh()).isEqualTo(UPDATED_THOI_GIAN_NHAN_BENH);
        assertThat(testThongTinKhoa.getThoiGianChuyenKhoa()).isEqualTo(UPDATED_THOI_GIAN_CHUYEN_KHOA);
        assertThat(testThongTinKhoa.getKhoaId()).isEqualTo(UPDATED_KHOA_ID);
        assertThat(testThongTinKhoa.getKhoaChuyenDen()).isEqualTo(UPDATED_KHOA_CHUYEN_DEN);
        assertThat(testThongTinKhoa.getKhoaChuyenDi()).isEqualTo(UPDATED_KHOA_CHUYEN_DI);
        assertThat(testThongTinKhoa.getNhanVienNhanBenh()).isEqualTo(UPDATED_NHAN_VIEN_NHAN_BENH);
        assertThat(testThongTinKhoa.getNhanVienChuyenKhoa()).isEqualTo(UPDATED_NHAN_VIEN_CHUYEN_KHOA);
        assertThat(testThongTinKhoa.getSoNgay()).isEqualTo(UPDATED_SO_NGAY);
        assertThat(testThongTinKhoa.getTrangThai()).isEqualTo(UPDATED_TRANG_THAI);
        assertThat(testThongTinKhoa.getNam()).isEqualTo(UPDATED_NAM);
    }

    @Test
    @Transactional
    public void updateNonExistingThongTinKhoa() throws Exception {
        int databaseSizeBeforeUpdate = thongTinKhoaRepository.findAll().size();

        // Create the ThongTinKhoa
        ThongTinKhoaDTO thongTinKhoaDTO = thongTinKhoaMapper.toDto(thongTinKhoa);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restThongTinKhoaMockMvc.perform(put("/api/thong-tin-khoas").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(thongTinKhoaDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ThongTinKhoa in the database
        List<ThongTinKhoa> thongTinKhoaList = thongTinKhoaRepository.findAll();
        assertThat(thongTinKhoaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteThongTinKhoa() throws Exception {
        // Initialize the database
        thongTinKhoaRepository.saveAndFlush(thongTinKhoa);

        int databaseSizeBeforeDelete = thongTinKhoaRepository.findAll().size();

        // Delete the thongTinKhoa
        restThongTinKhoaMockMvc.perform(delete("/api/thong-tin-khoas/{id}", thongTinKhoa.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ThongTinKhoa> thongTinKhoaList = thongTinKhoaRepository.findAll();
        assertThat(thongTinKhoaList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

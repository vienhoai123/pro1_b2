package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.Khoa;
import vn.vnpt.domain.DonVi;
import vn.vnpt.repository.KhoaRepository;
import vn.vnpt.service.KhoaService;
import vn.vnpt.service.dto.KhoaDTO;
import vn.vnpt.service.mapper.KhoaMapper;
import vn.vnpt.service.dto.KhoaCriteria;
import vn.vnpt.service.KhoaQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link KhoaResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class KhoaResourceIT {

    private static final Integer DEFAULT_CAP = 1;
    private static final Integer UPDATED_CAP = 2;
    private static final Integer SMALLER_CAP = 1 - 1;

    private static final Boolean DEFAULT_ENABLED = false;
    private static final Boolean UPDATED_ENABLED = true;

    private static final String DEFAULT_KY_HIEU = "AAAAAAAAAA";
    private static final String UPDATED_KY_HIEU = "BBBBBBBBBB";

    private static final String DEFAULT_PHONE = "AAAAAAAAAA";
    private static final String UPDATED_PHONE = "BBBBBBBBBB";

    private static final String DEFAULT_TEN = "AAAAAAAAAA";
    private static final String UPDATED_TEN = "BBBBBBBBBB";

    @Autowired
    private KhoaRepository khoaRepository;

    @Autowired
    private KhoaMapper khoaMapper;

    @Autowired
    private KhoaService khoaService;

    @Autowired
    private KhoaQueryService khoaQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restKhoaMockMvc;

    private Khoa khoa;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Khoa createEntity(EntityManager em) {
        Khoa khoa = new Khoa()
            .cap(DEFAULT_CAP)
            .enabled(DEFAULT_ENABLED)
            .kyHieu(DEFAULT_KY_HIEU)
            .phone(DEFAULT_PHONE)
            .ten(DEFAULT_TEN);
        // Add required entity
        DonVi donVi;
        if (TestUtil.findAll(em, DonVi.class).isEmpty()) {
            donVi = DonViResourceIT.createEntity(em);
            em.persist(donVi);
            em.flush();
        } else {
            donVi = TestUtil.findAll(em, DonVi.class).get(0);
        }
        khoa.setDonVi(donVi);
        return khoa;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Khoa createUpdatedEntity(EntityManager em) {
        Khoa khoa = new Khoa()
            .cap(UPDATED_CAP)
            .enabled(UPDATED_ENABLED)
            .kyHieu(UPDATED_KY_HIEU)
            .phone(UPDATED_PHONE)
            .ten(UPDATED_TEN);
        // Add required entity
        DonVi donVi;
        if (TestUtil.findAll(em, DonVi.class).isEmpty()) {
            donVi = DonViResourceIT.createUpdatedEntity(em);
            em.persist(donVi);
            em.flush();
        } else {
            donVi = TestUtil.findAll(em, DonVi.class).get(0);
        }
        khoa.setDonVi(donVi);
        return khoa;
    }

    @BeforeEach
    public void initTest() {
        khoa = createEntity(em);
    }

    @Test
    @Transactional
    public void createKhoa() throws Exception {
        int databaseSizeBeforeCreate = khoaRepository.findAll().size();

        // Create the Khoa
        KhoaDTO khoaDTO = khoaMapper.toDto(khoa);
        restKhoaMockMvc.perform(post("/api/khoas").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(khoaDTO)))
            .andExpect(status().isCreated());

        // Validate the Khoa in the database
        List<Khoa> khoaList = khoaRepository.findAll();
        assertThat(khoaList).hasSize(databaseSizeBeforeCreate + 1);
        Khoa testKhoa = khoaList.get(khoaList.size() - 1);
        assertThat(testKhoa.getCap()).isEqualTo(DEFAULT_CAP);
        assertThat(testKhoa.isEnabled()).isEqualTo(DEFAULT_ENABLED);
        assertThat(testKhoa.getKyHieu()).isEqualTo(DEFAULT_KY_HIEU);
        assertThat(testKhoa.getPhone()).isEqualTo(DEFAULT_PHONE);
        assertThat(testKhoa.getTen()).isEqualTo(DEFAULT_TEN);
    }

    @Test
    @Transactional
    public void createKhoaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = khoaRepository.findAll().size();

        // Create the Khoa with an existing ID
        khoa.setId(1L);
        KhoaDTO khoaDTO = khoaMapper.toDto(khoa);

        // An entity with an existing ID cannot be created, so this API call must fail
        restKhoaMockMvc.perform(post("/api/khoas").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(khoaDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Khoa in the database
        List<Khoa> khoaList = khoaRepository.findAll();
        assertThat(khoaList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkCapIsRequired() throws Exception {
        int databaseSizeBeforeTest = khoaRepository.findAll().size();
        // set the field null
        khoa.setCap(null);

        // Create the Khoa, which fails.
        KhoaDTO khoaDTO = khoaMapper.toDto(khoa);

        restKhoaMockMvc.perform(post("/api/khoas").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(khoaDTO)))
            .andExpect(status().isBadRequest());

        List<Khoa> khoaList = khoaRepository.findAll();
        assertThat(khoaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEnabledIsRequired() throws Exception {
        int databaseSizeBeforeTest = khoaRepository.findAll().size();
        // set the field null
        khoa.setEnabled(null);

        // Create the Khoa, which fails.
        KhoaDTO khoaDTO = khoaMapper.toDto(khoa);

        restKhoaMockMvc.perform(post("/api/khoas").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(khoaDTO)))
            .andExpect(status().isBadRequest());

        List<Khoa> khoaList = khoaRepository.findAll();
        assertThat(khoaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkKyHieuIsRequired() throws Exception {
        int databaseSizeBeforeTest = khoaRepository.findAll().size();
        // set the field null
        khoa.setKyHieu(null);

        // Create the Khoa, which fails.
        KhoaDTO khoaDTO = khoaMapper.toDto(khoa);

        restKhoaMockMvc.perform(post("/api/khoas").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(khoaDTO)))
            .andExpect(status().isBadRequest());

        List<Khoa> khoaList = khoaRepository.findAll();
        assertThat(khoaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTenIsRequired() throws Exception {
        int databaseSizeBeforeTest = khoaRepository.findAll().size();
        // set the field null
        khoa.setTen(null);

        // Create the Khoa, which fails.
        KhoaDTO khoaDTO = khoaMapper.toDto(khoa);

        restKhoaMockMvc.perform(post("/api/khoas").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(khoaDTO)))
            .andExpect(status().isBadRequest());

        List<Khoa> khoaList = khoaRepository.findAll();
        assertThat(khoaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllKhoas() throws Exception {
        // Initialize the database
        khoaRepository.saveAndFlush(khoa);

        // Get all the khoaList
        restKhoaMockMvc.perform(get("/api/khoas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(khoa.getId().intValue())))
            .andExpect(jsonPath("$.[*].cap").value(hasItem(DEFAULT_CAP)))
            .andExpect(jsonPath("$.[*].enabled").value(hasItem(DEFAULT_ENABLED.booleanValue())))
            .andExpect(jsonPath("$.[*].kyHieu").value(hasItem(DEFAULT_KY_HIEU)))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE)))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)));
    }
    
    @Test
    @Transactional
    public void getKhoa() throws Exception {
        // Initialize the database
        khoaRepository.saveAndFlush(khoa);

        // Get the khoa
        restKhoaMockMvc.perform(get("/api/khoas/{id}", khoa.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(khoa.getId().intValue()))
            .andExpect(jsonPath("$.cap").value(DEFAULT_CAP))
            .andExpect(jsonPath("$.enabled").value(DEFAULT_ENABLED.booleanValue()))
            .andExpect(jsonPath("$.kyHieu").value(DEFAULT_KY_HIEU))
            .andExpect(jsonPath("$.phone").value(DEFAULT_PHONE))
            .andExpect(jsonPath("$.ten").value(DEFAULT_TEN));
    }


    @Test
    @Transactional
    public void getKhoasByIdFiltering() throws Exception {
        // Initialize the database
        khoaRepository.saveAndFlush(khoa);

        Long id = khoa.getId();

        defaultKhoaShouldBeFound("id.equals=" + id);
        defaultKhoaShouldNotBeFound("id.notEquals=" + id);

        defaultKhoaShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultKhoaShouldNotBeFound("id.greaterThan=" + id);

        defaultKhoaShouldBeFound("id.lessThanOrEqual=" + id);
        defaultKhoaShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllKhoasByCapIsEqualToSomething() throws Exception {
        // Initialize the database
        khoaRepository.saveAndFlush(khoa);

        // Get all the khoaList where cap equals to DEFAULT_CAP
        defaultKhoaShouldBeFound("cap.equals=" + DEFAULT_CAP);

        // Get all the khoaList where cap equals to UPDATED_CAP
        defaultKhoaShouldNotBeFound("cap.equals=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    public void getAllKhoasByCapIsNotEqualToSomething() throws Exception {
        // Initialize the database
        khoaRepository.saveAndFlush(khoa);

        // Get all the khoaList where cap not equals to DEFAULT_CAP
        defaultKhoaShouldNotBeFound("cap.notEquals=" + DEFAULT_CAP);

        // Get all the khoaList where cap not equals to UPDATED_CAP
        defaultKhoaShouldBeFound("cap.notEquals=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    public void getAllKhoasByCapIsInShouldWork() throws Exception {
        // Initialize the database
        khoaRepository.saveAndFlush(khoa);

        // Get all the khoaList where cap in DEFAULT_CAP or UPDATED_CAP
        defaultKhoaShouldBeFound("cap.in=" + DEFAULT_CAP + "," + UPDATED_CAP);

        // Get all the khoaList where cap equals to UPDATED_CAP
        defaultKhoaShouldNotBeFound("cap.in=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    public void getAllKhoasByCapIsNullOrNotNull() throws Exception {
        // Initialize the database
        khoaRepository.saveAndFlush(khoa);

        // Get all the khoaList where cap is not null
        defaultKhoaShouldBeFound("cap.specified=true");

        // Get all the khoaList where cap is null
        defaultKhoaShouldNotBeFound("cap.specified=false");
    }

    @Test
    @Transactional
    public void getAllKhoasByCapIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        khoaRepository.saveAndFlush(khoa);

        // Get all the khoaList where cap is greater than or equal to DEFAULT_CAP
        defaultKhoaShouldBeFound("cap.greaterThanOrEqual=" + DEFAULT_CAP);

        // Get all the khoaList where cap is greater than or equal to UPDATED_CAP
        defaultKhoaShouldNotBeFound("cap.greaterThanOrEqual=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    public void getAllKhoasByCapIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        khoaRepository.saveAndFlush(khoa);

        // Get all the khoaList where cap is less than or equal to DEFAULT_CAP
        defaultKhoaShouldBeFound("cap.lessThanOrEqual=" + DEFAULT_CAP);

        // Get all the khoaList where cap is less than or equal to SMALLER_CAP
        defaultKhoaShouldNotBeFound("cap.lessThanOrEqual=" + SMALLER_CAP);
    }

    @Test
    @Transactional
    public void getAllKhoasByCapIsLessThanSomething() throws Exception {
        // Initialize the database
        khoaRepository.saveAndFlush(khoa);

        // Get all the khoaList where cap is less than DEFAULT_CAP
        defaultKhoaShouldNotBeFound("cap.lessThan=" + DEFAULT_CAP);

        // Get all the khoaList where cap is less than UPDATED_CAP
        defaultKhoaShouldBeFound("cap.lessThan=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    public void getAllKhoasByCapIsGreaterThanSomething() throws Exception {
        // Initialize the database
        khoaRepository.saveAndFlush(khoa);

        // Get all the khoaList where cap is greater than DEFAULT_CAP
        defaultKhoaShouldNotBeFound("cap.greaterThan=" + DEFAULT_CAP);

        // Get all the khoaList where cap is greater than SMALLER_CAP
        defaultKhoaShouldBeFound("cap.greaterThan=" + SMALLER_CAP);
    }


    @Test
    @Transactional
    public void getAllKhoasByEnabledIsEqualToSomething() throws Exception {
        // Initialize the database
        khoaRepository.saveAndFlush(khoa);

        // Get all the khoaList where enabled equals to DEFAULT_ENABLED
        defaultKhoaShouldBeFound("enabled.equals=" + DEFAULT_ENABLED);

        // Get all the khoaList where enabled equals to UPDATED_ENABLED
        defaultKhoaShouldNotBeFound("enabled.equals=" + UPDATED_ENABLED);
    }

    @Test
    @Transactional
    public void getAllKhoasByEnabledIsNotEqualToSomething() throws Exception {
        // Initialize the database
        khoaRepository.saveAndFlush(khoa);

        // Get all the khoaList where enabled not equals to DEFAULT_ENABLED
        defaultKhoaShouldNotBeFound("enabled.notEquals=" + DEFAULT_ENABLED);

        // Get all the khoaList where enabled not equals to UPDATED_ENABLED
        defaultKhoaShouldBeFound("enabled.notEquals=" + UPDATED_ENABLED);
    }

    @Test
    @Transactional
    public void getAllKhoasByEnabledIsInShouldWork() throws Exception {
        // Initialize the database
        khoaRepository.saveAndFlush(khoa);

        // Get all the khoaList where enabled in DEFAULT_ENABLED or UPDATED_ENABLED
        defaultKhoaShouldBeFound("enabled.in=" + DEFAULT_ENABLED + "," + UPDATED_ENABLED);

        // Get all the khoaList where enabled equals to UPDATED_ENABLED
        defaultKhoaShouldNotBeFound("enabled.in=" + UPDATED_ENABLED);
    }

    @Test
    @Transactional
    public void getAllKhoasByEnabledIsNullOrNotNull() throws Exception {
        // Initialize the database
        khoaRepository.saveAndFlush(khoa);

        // Get all the khoaList where enabled is not null
        defaultKhoaShouldBeFound("enabled.specified=true");

        // Get all the khoaList where enabled is null
        defaultKhoaShouldNotBeFound("enabled.specified=false");
    }

    @Test
    @Transactional
    public void getAllKhoasByKyHieuIsEqualToSomething() throws Exception {
        // Initialize the database
        khoaRepository.saveAndFlush(khoa);

        // Get all the khoaList where kyHieu equals to DEFAULT_KY_HIEU
        defaultKhoaShouldBeFound("kyHieu.equals=" + DEFAULT_KY_HIEU);

        // Get all the khoaList where kyHieu equals to UPDATED_KY_HIEU
        defaultKhoaShouldNotBeFound("kyHieu.equals=" + UPDATED_KY_HIEU);
    }

    @Test
    @Transactional
    public void getAllKhoasByKyHieuIsNotEqualToSomething() throws Exception {
        // Initialize the database
        khoaRepository.saveAndFlush(khoa);

        // Get all the khoaList where kyHieu not equals to DEFAULT_KY_HIEU
        defaultKhoaShouldNotBeFound("kyHieu.notEquals=" + DEFAULT_KY_HIEU);

        // Get all the khoaList where kyHieu not equals to UPDATED_KY_HIEU
        defaultKhoaShouldBeFound("kyHieu.notEquals=" + UPDATED_KY_HIEU);
    }

    @Test
    @Transactional
    public void getAllKhoasByKyHieuIsInShouldWork() throws Exception {
        // Initialize the database
        khoaRepository.saveAndFlush(khoa);

        // Get all the khoaList where kyHieu in DEFAULT_KY_HIEU or UPDATED_KY_HIEU
        defaultKhoaShouldBeFound("kyHieu.in=" + DEFAULT_KY_HIEU + "," + UPDATED_KY_HIEU);

        // Get all the khoaList where kyHieu equals to UPDATED_KY_HIEU
        defaultKhoaShouldNotBeFound("kyHieu.in=" + UPDATED_KY_HIEU);
    }

    @Test
    @Transactional
    public void getAllKhoasByKyHieuIsNullOrNotNull() throws Exception {
        // Initialize the database
        khoaRepository.saveAndFlush(khoa);

        // Get all the khoaList where kyHieu is not null
        defaultKhoaShouldBeFound("kyHieu.specified=true");

        // Get all the khoaList where kyHieu is null
        defaultKhoaShouldNotBeFound("kyHieu.specified=false");
    }
                @Test
    @Transactional
    public void getAllKhoasByKyHieuContainsSomething() throws Exception {
        // Initialize the database
        khoaRepository.saveAndFlush(khoa);

        // Get all the khoaList where kyHieu contains DEFAULT_KY_HIEU
        defaultKhoaShouldBeFound("kyHieu.contains=" + DEFAULT_KY_HIEU);

        // Get all the khoaList where kyHieu contains UPDATED_KY_HIEU
        defaultKhoaShouldNotBeFound("kyHieu.contains=" + UPDATED_KY_HIEU);
    }

    @Test
    @Transactional
    public void getAllKhoasByKyHieuNotContainsSomething() throws Exception {
        // Initialize the database
        khoaRepository.saveAndFlush(khoa);

        // Get all the khoaList where kyHieu does not contain DEFAULT_KY_HIEU
        defaultKhoaShouldNotBeFound("kyHieu.doesNotContain=" + DEFAULT_KY_HIEU);

        // Get all the khoaList where kyHieu does not contain UPDATED_KY_HIEU
        defaultKhoaShouldBeFound("kyHieu.doesNotContain=" + UPDATED_KY_HIEU);
    }


    @Test
    @Transactional
    public void getAllKhoasByPhoneIsEqualToSomething() throws Exception {
        // Initialize the database
        khoaRepository.saveAndFlush(khoa);

        // Get all the khoaList where phone equals to DEFAULT_PHONE
        defaultKhoaShouldBeFound("phone.equals=" + DEFAULT_PHONE);

        // Get all the khoaList where phone equals to UPDATED_PHONE
        defaultKhoaShouldNotBeFound("phone.equals=" + UPDATED_PHONE);
    }

    @Test
    @Transactional
    public void getAllKhoasByPhoneIsNotEqualToSomething() throws Exception {
        // Initialize the database
        khoaRepository.saveAndFlush(khoa);

        // Get all the khoaList where phone not equals to DEFAULT_PHONE
        defaultKhoaShouldNotBeFound("phone.notEquals=" + DEFAULT_PHONE);

        // Get all the khoaList where phone not equals to UPDATED_PHONE
        defaultKhoaShouldBeFound("phone.notEquals=" + UPDATED_PHONE);
    }

    @Test
    @Transactional
    public void getAllKhoasByPhoneIsInShouldWork() throws Exception {
        // Initialize the database
        khoaRepository.saveAndFlush(khoa);

        // Get all the khoaList where phone in DEFAULT_PHONE or UPDATED_PHONE
        defaultKhoaShouldBeFound("phone.in=" + DEFAULT_PHONE + "," + UPDATED_PHONE);

        // Get all the khoaList where phone equals to UPDATED_PHONE
        defaultKhoaShouldNotBeFound("phone.in=" + UPDATED_PHONE);
    }

    @Test
    @Transactional
    public void getAllKhoasByPhoneIsNullOrNotNull() throws Exception {
        // Initialize the database
        khoaRepository.saveAndFlush(khoa);

        // Get all the khoaList where phone is not null
        defaultKhoaShouldBeFound("phone.specified=true");

        // Get all the khoaList where phone is null
        defaultKhoaShouldNotBeFound("phone.specified=false");
    }
                @Test
    @Transactional
    public void getAllKhoasByPhoneContainsSomething() throws Exception {
        // Initialize the database
        khoaRepository.saveAndFlush(khoa);

        // Get all the khoaList where phone contains DEFAULT_PHONE
        defaultKhoaShouldBeFound("phone.contains=" + DEFAULT_PHONE);

        // Get all the khoaList where phone contains UPDATED_PHONE
        defaultKhoaShouldNotBeFound("phone.contains=" + UPDATED_PHONE);
    }

    @Test
    @Transactional
    public void getAllKhoasByPhoneNotContainsSomething() throws Exception {
        // Initialize the database
        khoaRepository.saveAndFlush(khoa);

        // Get all the khoaList where phone does not contain DEFAULT_PHONE
        defaultKhoaShouldNotBeFound("phone.doesNotContain=" + DEFAULT_PHONE);

        // Get all the khoaList where phone does not contain UPDATED_PHONE
        defaultKhoaShouldBeFound("phone.doesNotContain=" + UPDATED_PHONE);
    }


    @Test
    @Transactional
    public void getAllKhoasByTenIsEqualToSomething() throws Exception {
        // Initialize the database
        khoaRepository.saveAndFlush(khoa);

        // Get all the khoaList where ten equals to DEFAULT_TEN
        defaultKhoaShouldBeFound("ten.equals=" + DEFAULT_TEN);

        // Get all the khoaList where ten equals to UPDATED_TEN
        defaultKhoaShouldNotBeFound("ten.equals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllKhoasByTenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        khoaRepository.saveAndFlush(khoa);

        // Get all the khoaList where ten not equals to DEFAULT_TEN
        defaultKhoaShouldNotBeFound("ten.notEquals=" + DEFAULT_TEN);

        // Get all the khoaList where ten not equals to UPDATED_TEN
        defaultKhoaShouldBeFound("ten.notEquals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllKhoasByTenIsInShouldWork() throws Exception {
        // Initialize the database
        khoaRepository.saveAndFlush(khoa);

        // Get all the khoaList where ten in DEFAULT_TEN or UPDATED_TEN
        defaultKhoaShouldBeFound("ten.in=" + DEFAULT_TEN + "," + UPDATED_TEN);

        // Get all the khoaList where ten equals to UPDATED_TEN
        defaultKhoaShouldNotBeFound("ten.in=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllKhoasByTenIsNullOrNotNull() throws Exception {
        // Initialize the database
        khoaRepository.saveAndFlush(khoa);

        // Get all the khoaList where ten is not null
        defaultKhoaShouldBeFound("ten.specified=true");

        // Get all the khoaList where ten is null
        defaultKhoaShouldNotBeFound("ten.specified=false");
    }
                @Test
    @Transactional
    public void getAllKhoasByTenContainsSomething() throws Exception {
        // Initialize the database
        khoaRepository.saveAndFlush(khoa);

        // Get all the khoaList where ten contains DEFAULT_TEN
        defaultKhoaShouldBeFound("ten.contains=" + DEFAULT_TEN);

        // Get all the khoaList where ten contains UPDATED_TEN
        defaultKhoaShouldNotBeFound("ten.contains=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllKhoasByTenNotContainsSomething() throws Exception {
        // Initialize the database
        khoaRepository.saveAndFlush(khoa);

        // Get all the khoaList where ten does not contain DEFAULT_TEN
        defaultKhoaShouldNotBeFound("ten.doesNotContain=" + DEFAULT_TEN);

        // Get all the khoaList where ten does not contain UPDATED_TEN
        defaultKhoaShouldBeFound("ten.doesNotContain=" + UPDATED_TEN);
    }


    @Test
    @Transactional
    public void getAllKhoasByDonViIsEqualToSomething() throws Exception {
        // Get already existing entity
        DonVi donVi = khoa.getDonVi();
        khoaRepository.saveAndFlush(khoa);
        Long donViId = donVi.getId();

        // Get all the khoaList where donVi equals to donViId
        defaultKhoaShouldBeFound("donViId.equals=" + donViId);

        // Get all the khoaList where donVi equals to donViId + 1
        defaultKhoaShouldNotBeFound("donViId.equals=" + (donViId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultKhoaShouldBeFound(String filter) throws Exception {
        restKhoaMockMvc.perform(get("/api/khoas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(khoa.getId().intValue())))
            .andExpect(jsonPath("$.[*].cap").value(hasItem(DEFAULT_CAP)))
            .andExpect(jsonPath("$.[*].enabled").value(hasItem(DEFAULT_ENABLED.booleanValue())))
            .andExpect(jsonPath("$.[*].kyHieu").value(hasItem(DEFAULT_KY_HIEU)))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE)))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)));

        // Check, that the count call also returns 1
        restKhoaMockMvc.perform(get("/api/khoas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultKhoaShouldNotBeFound(String filter) throws Exception {
        restKhoaMockMvc.perform(get("/api/khoas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restKhoaMockMvc.perform(get("/api/khoas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingKhoa() throws Exception {
        // Get the khoa
        restKhoaMockMvc.perform(get("/api/khoas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateKhoa() throws Exception {
        // Initialize the database
        khoaRepository.saveAndFlush(khoa);

        int databaseSizeBeforeUpdate = khoaRepository.findAll().size();

        // Update the khoa
        Khoa updatedKhoa = khoaRepository.findById(khoa.getId()).get();
        // Disconnect from session so that the updates on updatedKhoa are not directly saved in db
        em.detach(updatedKhoa);
        updatedKhoa
            .cap(UPDATED_CAP)
            .enabled(UPDATED_ENABLED)
            .kyHieu(UPDATED_KY_HIEU)
            .phone(UPDATED_PHONE)
            .ten(UPDATED_TEN);
        KhoaDTO khoaDTO = khoaMapper.toDto(updatedKhoa);

        restKhoaMockMvc.perform(put("/api/khoas").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(khoaDTO)))
            .andExpect(status().isOk());

        // Validate the Khoa in the database
        List<Khoa> khoaList = khoaRepository.findAll();
        assertThat(khoaList).hasSize(databaseSizeBeforeUpdate);
        Khoa testKhoa = khoaList.get(khoaList.size() - 1);
        assertThat(testKhoa.getCap()).isEqualTo(UPDATED_CAP);
        assertThat(testKhoa.isEnabled()).isEqualTo(UPDATED_ENABLED);
        assertThat(testKhoa.getKyHieu()).isEqualTo(UPDATED_KY_HIEU);
        assertThat(testKhoa.getPhone()).isEqualTo(UPDATED_PHONE);
        assertThat(testKhoa.getTen()).isEqualTo(UPDATED_TEN);
    }

    @Test
    @Transactional
    public void updateNonExistingKhoa() throws Exception {
        int databaseSizeBeforeUpdate = khoaRepository.findAll().size();

        // Create the Khoa
        KhoaDTO khoaDTO = khoaMapper.toDto(khoa);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restKhoaMockMvc.perform(put("/api/khoas").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(khoaDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Khoa in the database
        List<Khoa> khoaList = khoaRepository.findAll();
        assertThat(khoaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteKhoa() throws Exception {
        // Initialize the database
        khoaRepository.saveAndFlush(khoa);

        int databaseSizeBeforeDelete = khoaRepository.findAll().size();

        // Delete the khoa
        restKhoaMockMvc.perform(delete("/api/khoas/{id}", khoa.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Khoa> khoaList = khoaRepository.findAll();
        assertThat(khoaList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.XetNghiem;
import vn.vnpt.domain.DonVi;
import vn.vnpt.domain.DotThayDoiMaDichVu;
import vn.vnpt.domain.LoaiXetNghiem;
import vn.vnpt.repository.XetNghiemRepository;
import vn.vnpt.service.XetNghiemService;
import vn.vnpt.service.dto.XetNghiemDTO;
import vn.vnpt.service.mapper.XetNghiemMapper;
import vn.vnpt.service.dto.XetNghiemCriteria;
import vn.vnpt.service.XetNghiemQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link XetNghiemResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class XetNghiemResourceIT {

    private static final BigDecimal DEFAULT_CAN_DUOI_NAM = new BigDecimal(1);
    private static final BigDecimal UPDATED_CAN_DUOI_NAM = new BigDecimal(2);
    private static final BigDecimal SMALLER_CAN_DUOI_NAM = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_CAN_DUOI_NU = new BigDecimal(1);
    private static final BigDecimal UPDATED_CAN_DUOI_NU = new BigDecimal(2);
    private static final BigDecimal SMALLER_CAN_DUOI_NU = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_CAN_TREN_NAM = new BigDecimal(1);
    private static final BigDecimal UPDATED_CAN_TREN_NAM = new BigDecimal(2);
    private static final BigDecimal SMALLER_CAN_TREN_NAM = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_CAN_TREN_NU = new BigDecimal(1);
    private static final BigDecimal UPDATED_CAN_TREN_NU = new BigDecimal(2);
    private static final BigDecimal SMALLER_CAN_TREN_NU = new BigDecimal(1 - 1);

    private static final String DEFAULT_CHI_SO_BINH_THUONG_NAM = "AAAAAAAAAA";
    private static final String UPDATED_CHI_SO_BINH_THUONG_NAM = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_CHI_SO_MAX = new BigDecimal(1);
    private static final BigDecimal UPDATED_CHI_SO_MAX = new BigDecimal(2);
    private static final BigDecimal SMALLER_CHI_SO_MAX = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_CHI_SO_MIN = new BigDecimal(1);
    private static final BigDecimal UPDATED_CHI_SO_MIN = new BigDecimal(2);
    private static final BigDecimal SMALLER_CHI_SO_MIN = new BigDecimal(1 - 1);

    private static final String DEFAULT_CONG_THUC = "AAAAAAAAAA";
    private static final String UPDATED_CONG_THUC = "BBBBBBBBBB";

    private static final Boolean DEFAULT_DELETED = false;
    private static final Boolean UPDATED_DELETED = true;

    private static final String DEFAULT_DO_PHA_LOANG = "AAAAAAAAAA";
    private static final String UPDATED_DO_PHA_LOANG = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_DON_GIA_BENH_VIEN = new BigDecimal(1);
    private static final BigDecimal UPDATED_DON_GIA_BENH_VIEN = new BigDecimal(2);
    private static final BigDecimal SMALLER_DON_GIA_BENH_VIEN = new BigDecimal(1 - 1);

    private static final String DEFAULT_DON_VI_TINH = "AAAAAAAAAA";
    private static final String UPDATED_DON_VI_TINH = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ENABLED = false;
    private static final Boolean UPDATED_ENABLED = true;

    private static final BigDecimal DEFAULT_GIOI_HAN_CHI_DINH = new BigDecimal(1);
    private static final BigDecimal UPDATED_GIOI_HAN_CHI_DINH = new BigDecimal(2);
    private static final BigDecimal SMALLER_GIOI_HAN_CHI_DINH = new BigDecimal(1 - 1);

    private static final String DEFAULT_KET_QUA_BAT_THUONG = "AAAAAAAAAA";
    private static final String UPDATED_KET_QUA_BAT_THUONG = "BBBBBBBBBB";

    private static final String DEFAULT_MA_DUNG_CHUNG = "AAAAAAAAAA";
    private static final String UPDATED_MA_DUNG_CHUNG = "BBBBBBBBBB";

    private static final String DEFAULT_KET_QUA_MAC_DINH = "AAAAAAAAAA";
    private static final String UPDATED_KET_QUA_MAC_DINH = "BBBBBBBBBB";

    private static final Boolean DEFAULT_PHAM_VI_CHI_DINH = false;
    private static final Boolean UPDATED_PHAM_VI_CHI_DINH = true;

    private static final Boolean DEFAULT_PHAN_THEO_GIOI_T_INH = false;
    private static final Boolean UPDATED_PHAN_THEO_GIOI_T_INH = true;

    private static final Integer DEFAULT_SO_LE_LAM_TRON = 1;
    private static final Integer UPDATED_SO_LE_LAM_TRON = 2;
    private static final Integer SMALLER_SO_LE_LAM_TRON = 1 - 1;

    private static final BigDecimal DEFAULT_SO_LUONG_THUC_HIEN = new BigDecimal(1);
    private static final BigDecimal UPDATED_SO_LUONG_THUC_HIEN = new BigDecimal(2);
    private static final BigDecimal SMALLER_SO_LUONG_THUC_HIEN = new BigDecimal(1 - 1);

    private static final String DEFAULT_TEN = "AAAAAAAAAA";
    private static final String UPDATED_TEN = "BBBBBBBBBB";

    private static final String DEFAULT_TEN_HIEN_THI = "AAAAAAAAAA";
    private static final String UPDATED_TEN_HIEN_THI = "BBBBBBBBBB";

    private static final String DEFAULT_MA_NOI_BO = "AAAAAAAAAA";
    private static final String UPDATED_MA_NOI_BO = "BBBBBBBBBB";

    @Autowired
    private XetNghiemRepository xetNghiemRepository;

    @Autowired
    private XetNghiemMapper xetNghiemMapper;

    @Autowired
    private XetNghiemService xetNghiemService;

    @Autowired
    private XetNghiemQueryService xetNghiemQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restXetNghiemMockMvc;

    private XetNghiem xetNghiem;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static XetNghiem createEntity(EntityManager em) {
        XetNghiem xetNghiem = new XetNghiem()
            .canDuoiNam(DEFAULT_CAN_DUOI_NAM)
            .canDuoiNu(DEFAULT_CAN_DUOI_NU)
            .canTrenNam(DEFAULT_CAN_TREN_NAM)
            .canTrenNu(DEFAULT_CAN_TREN_NU)
            .chiSoBinhThuongNam(DEFAULT_CHI_SO_BINH_THUONG_NAM)
            .chiSoMax(DEFAULT_CHI_SO_MAX)
            .chiSoMin(DEFAULT_CHI_SO_MIN)
            .congThuc(DEFAULT_CONG_THUC)
            .deleted(DEFAULT_DELETED)
            .doPhaLoang(DEFAULT_DO_PHA_LOANG)
            .donGiaBenhVien(DEFAULT_DON_GIA_BENH_VIEN)
            .donViTinh(DEFAULT_DON_VI_TINH)
            .enabled(DEFAULT_ENABLED)
            .gioiHanChiDinh(DEFAULT_GIOI_HAN_CHI_DINH)
            .ketQuaBatThuong(DEFAULT_KET_QUA_BAT_THUONG)
            .maDungChung(DEFAULT_MA_DUNG_CHUNG)
            .ketQuaMacDinh(DEFAULT_KET_QUA_MAC_DINH)
            .phamViChiDinh(DEFAULT_PHAM_VI_CHI_DINH)
            .phanTheoGioiTInh(DEFAULT_PHAN_THEO_GIOI_T_INH)
            .soLeLamTron(DEFAULT_SO_LE_LAM_TRON)
            .soLuongThucHien(DEFAULT_SO_LUONG_THUC_HIEN)
            .ten(DEFAULT_TEN)
            .tenHienThi(DEFAULT_TEN_HIEN_THI)
            .maNoiBo(DEFAULT_MA_NOI_BO);
        // Add required entity
        DonVi donVi;
        if (TestUtil.findAll(em, DonVi.class).isEmpty()) {
            donVi = DonViResourceIT.createEntity(em);
            em.persist(donVi);
            em.flush();
        } else {
            donVi = TestUtil.findAll(em, DonVi.class).get(0);
        }
        xetNghiem.setDonVi(donVi);
        // Add required entity
        DotThayDoiMaDichVu dotThayDoiMaDichVu;
        if (TestUtil.findAll(em, DotThayDoiMaDichVu.class).isEmpty()) {
            dotThayDoiMaDichVu = DotThayDoiMaDichVuResourceIT.createEntity(em);
            em.persist(dotThayDoiMaDichVu);
            em.flush();
        } else {
            dotThayDoiMaDichVu = TestUtil.findAll(em, DotThayDoiMaDichVu.class).get(0);
        }
        xetNghiem.setDotMa(dotThayDoiMaDichVu);
        // Add required entity
        LoaiXetNghiem loaiXetNghiem;
        if (TestUtil.findAll(em, LoaiXetNghiem.class).isEmpty()) {
            loaiXetNghiem = LoaiXetNghiemResourceIT.createEntity(em);
            em.persist(loaiXetNghiem);
            em.flush();
        } else {
            loaiXetNghiem = TestUtil.findAll(em, LoaiXetNghiem.class).get(0);
        }
        xetNghiem.setLoaiXN(loaiXetNghiem);
        return xetNghiem;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static XetNghiem createUpdatedEntity(EntityManager em) {
        XetNghiem xetNghiem = new XetNghiem()
            .canDuoiNam(UPDATED_CAN_DUOI_NAM)
            .canDuoiNu(UPDATED_CAN_DUOI_NU)
            .canTrenNam(UPDATED_CAN_TREN_NAM)
            .canTrenNu(UPDATED_CAN_TREN_NU)
            .chiSoBinhThuongNam(UPDATED_CHI_SO_BINH_THUONG_NAM)
            .chiSoMax(UPDATED_CHI_SO_MAX)
            .chiSoMin(UPDATED_CHI_SO_MIN)
            .congThuc(UPDATED_CONG_THUC)
            .deleted(UPDATED_DELETED)
            .doPhaLoang(UPDATED_DO_PHA_LOANG)
            .donGiaBenhVien(UPDATED_DON_GIA_BENH_VIEN)
            .donViTinh(UPDATED_DON_VI_TINH)
            .enabled(UPDATED_ENABLED)
            .gioiHanChiDinh(UPDATED_GIOI_HAN_CHI_DINH)
            .ketQuaBatThuong(UPDATED_KET_QUA_BAT_THUONG)
            .maDungChung(UPDATED_MA_DUNG_CHUNG)
            .ketQuaMacDinh(UPDATED_KET_QUA_MAC_DINH)
            .phamViChiDinh(UPDATED_PHAM_VI_CHI_DINH)
            .phanTheoGioiTInh(UPDATED_PHAN_THEO_GIOI_T_INH)
            .soLeLamTron(UPDATED_SO_LE_LAM_TRON)
            .soLuongThucHien(UPDATED_SO_LUONG_THUC_HIEN)
            .ten(UPDATED_TEN)
            .tenHienThi(UPDATED_TEN_HIEN_THI)
            .maNoiBo(UPDATED_MA_NOI_BO);
        // Add required entity
        DonVi donVi;
        if (TestUtil.findAll(em, DonVi.class).isEmpty()) {
            donVi = DonViResourceIT.createUpdatedEntity(em);
            em.persist(donVi);
            em.flush();
        } else {
            donVi = TestUtil.findAll(em, DonVi.class).get(0);
        }
        xetNghiem.setDonVi(donVi);
        // Add required entity
        DotThayDoiMaDichVu dotThayDoiMaDichVu;
        if (TestUtil.findAll(em, DotThayDoiMaDichVu.class).isEmpty()) {
            dotThayDoiMaDichVu = DotThayDoiMaDichVuResourceIT.createUpdatedEntity(em);
            em.persist(dotThayDoiMaDichVu);
            em.flush();
        } else {
            dotThayDoiMaDichVu = TestUtil.findAll(em, DotThayDoiMaDichVu.class).get(0);
        }
        xetNghiem.setDotMa(dotThayDoiMaDichVu);
        // Add required entity
        LoaiXetNghiem loaiXetNghiem;
        if (TestUtil.findAll(em, LoaiXetNghiem.class).isEmpty()) {
            loaiXetNghiem = LoaiXetNghiemResourceIT.createUpdatedEntity(em);
            em.persist(loaiXetNghiem);
            em.flush();
        } else {
            loaiXetNghiem = TestUtil.findAll(em, LoaiXetNghiem.class).get(0);
        }
        xetNghiem.setLoaiXN(loaiXetNghiem);
        return xetNghiem;
    }

    @BeforeEach
    public void initTest() {
        xetNghiem = createEntity(em);
    }

    @Test
    @Transactional
    public void createXetNghiem() throws Exception {
        int databaseSizeBeforeCreate = xetNghiemRepository.findAll().size();

        // Create the XetNghiem
        XetNghiemDTO xetNghiemDTO = xetNghiemMapper.toDto(xetNghiem);
        restXetNghiemMockMvc.perform(post("/api/xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(xetNghiemDTO)))
            .andExpect(status().isCreated());

        // Validate the XetNghiem in the database
        List<XetNghiem> xetNghiemList = xetNghiemRepository.findAll();
        assertThat(xetNghiemList).hasSize(databaseSizeBeforeCreate + 1);
        XetNghiem testXetNghiem = xetNghiemList.get(xetNghiemList.size() - 1);
        assertThat(testXetNghiem.getCanDuoiNam()).isEqualTo(DEFAULT_CAN_DUOI_NAM);
        assertThat(testXetNghiem.getCanDuoiNu()).isEqualTo(DEFAULT_CAN_DUOI_NU);
        assertThat(testXetNghiem.getCanTrenNam()).isEqualTo(DEFAULT_CAN_TREN_NAM);
        assertThat(testXetNghiem.getCanTrenNu()).isEqualTo(DEFAULT_CAN_TREN_NU);
        assertThat(testXetNghiem.getChiSoBinhThuongNam()).isEqualTo(DEFAULT_CHI_SO_BINH_THUONG_NAM);
        assertThat(testXetNghiem.getChiSoMax()).isEqualTo(DEFAULT_CHI_SO_MAX);
        assertThat(testXetNghiem.getChiSoMin()).isEqualTo(DEFAULT_CHI_SO_MIN);
        assertThat(testXetNghiem.getCongThuc()).isEqualTo(DEFAULT_CONG_THUC);
        assertThat(testXetNghiem.isDeleted()).isEqualTo(DEFAULT_DELETED);
        assertThat(testXetNghiem.getDoPhaLoang()).isEqualTo(DEFAULT_DO_PHA_LOANG);
        assertThat(testXetNghiem.getDonGiaBenhVien()).isEqualTo(DEFAULT_DON_GIA_BENH_VIEN);
        assertThat(testXetNghiem.getDonViTinh()).isEqualTo(DEFAULT_DON_VI_TINH);
        assertThat(testXetNghiem.isEnabled()).isEqualTo(DEFAULT_ENABLED);
        assertThat(testXetNghiem.getGioiHanChiDinh()).isEqualTo(DEFAULT_GIOI_HAN_CHI_DINH);
        assertThat(testXetNghiem.getKetQuaBatThuong()).isEqualTo(DEFAULT_KET_QUA_BAT_THUONG);
        assertThat(testXetNghiem.getMaDungChung()).isEqualTo(DEFAULT_MA_DUNG_CHUNG);
        assertThat(testXetNghiem.getKetQuaMacDinh()).isEqualTo(DEFAULT_KET_QUA_MAC_DINH);
        assertThat(testXetNghiem.isPhamViChiDinh()).isEqualTo(DEFAULT_PHAM_VI_CHI_DINH);
        assertThat(testXetNghiem.isPhanTheoGioiTInh()).isEqualTo(DEFAULT_PHAN_THEO_GIOI_T_INH);
        assertThat(testXetNghiem.getSoLeLamTron()).isEqualTo(DEFAULT_SO_LE_LAM_TRON);
        assertThat(testXetNghiem.getSoLuongThucHien()).isEqualTo(DEFAULT_SO_LUONG_THUC_HIEN);
        assertThat(testXetNghiem.getTen()).isEqualTo(DEFAULT_TEN);
        assertThat(testXetNghiem.getTenHienThi()).isEqualTo(DEFAULT_TEN_HIEN_THI);
        assertThat(testXetNghiem.getMaNoiBo()).isEqualTo(DEFAULT_MA_NOI_BO);
    }

    @Test
    @Transactional
    public void createXetNghiemWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = xetNghiemRepository.findAll().size();

        // Create the XetNghiem with an existing ID
        xetNghiem.setId(1L);
        XetNghiemDTO xetNghiemDTO = xetNghiemMapper.toDto(xetNghiem);

        // An entity with an existing ID cannot be created, so this API call must fail
        restXetNghiemMockMvc.perform(post("/api/xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(xetNghiemDTO)))
            .andExpect(status().isBadRequest());

        // Validate the XetNghiem in the database
        List<XetNghiem> xetNghiemList = xetNghiemRepository.findAll();
        assertThat(xetNghiemList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkCanDuoiNamIsRequired() throws Exception {
        int databaseSizeBeforeTest = xetNghiemRepository.findAll().size();
        // set the field null
        xetNghiem.setCanDuoiNam(null);

        // Create the XetNghiem, which fails.
        XetNghiemDTO xetNghiemDTO = xetNghiemMapper.toDto(xetNghiem);

        restXetNghiemMockMvc.perform(post("/api/xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(xetNghiemDTO)))
            .andExpect(status().isBadRequest());

        List<XetNghiem> xetNghiemList = xetNghiemRepository.findAll();
        assertThat(xetNghiemList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCanDuoiNuIsRequired() throws Exception {
        int databaseSizeBeforeTest = xetNghiemRepository.findAll().size();
        // set the field null
        xetNghiem.setCanDuoiNu(null);

        // Create the XetNghiem, which fails.
        XetNghiemDTO xetNghiemDTO = xetNghiemMapper.toDto(xetNghiem);

        restXetNghiemMockMvc.perform(post("/api/xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(xetNghiemDTO)))
            .andExpect(status().isBadRequest());

        List<XetNghiem> xetNghiemList = xetNghiemRepository.findAll();
        assertThat(xetNghiemList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCanTrenNamIsRequired() throws Exception {
        int databaseSizeBeforeTest = xetNghiemRepository.findAll().size();
        // set the field null
        xetNghiem.setCanTrenNam(null);

        // Create the XetNghiem, which fails.
        XetNghiemDTO xetNghiemDTO = xetNghiemMapper.toDto(xetNghiem);

        restXetNghiemMockMvc.perform(post("/api/xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(xetNghiemDTO)))
            .andExpect(status().isBadRequest());

        List<XetNghiem> xetNghiemList = xetNghiemRepository.findAll();
        assertThat(xetNghiemList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCanTrenNuIsRequired() throws Exception {
        int databaseSizeBeforeTest = xetNghiemRepository.findAll().size();
        // set the field null
        xetNghiem.setCanTrenNu(null);

        // Create the XetNghiem, which fails.
        XetNghiemDTO xetNghiemDTO = xetNghiemMapper.toDto(xetNghiem);

        restXetNghiemMockMvc.perform(post("/api/xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(xetNghiemDTO)))
            .andExpect(status().isBadRequest());

        List<XetNghiem> xetNghiemList = xetNghiemRepository.findAll();
        assertThat(xetNghiemList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkChiSoMaxIsRequired() throws Exception {
        int databaseSizeBeforeTest = xetNghiemRepository.findAll().size();
        // set the field null
        xetNghiem.setChiSoMax(null);

        // Create the XetNghiem, which fails.
        XetNghiemDTO xetNghiemDTO = xetNghiemMapper.toDto(xetNghiem);

        restXetNghiemMockMvc.perform(post("/api/xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(xetNghiemDTO)))
            .andExpect(status().isBadRequest());

        List<XetNghiem> xetNghiemList = xetNghiemRepository.findAll();
        assertThat(xetNghiemList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkChiSoMinIsRequired() throws Exception {
        int databaseSizeBeforeTest = xetNghiemRepository.findAll().size();
        // set the field null
        xetNghiem.setChiSoMin(null);

        // Create the XetNghiem, which fails.
        XetNghiemDTO xetNghiemDTO = xetNghiemMapper.toDto(xetNghiem);

        restXetNghiemMockMvc.perform(post("/api/xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(xetNghiemDTO)))
            .andExpect(status().isBadRequest());

        List<XetNghiem> xetNghiemList = xetNghiemRepository.findAll();
        assertThat(xetNghiemList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDeletedIsRequired() throws Exception {
        int databaseSizeBeforeTest = xetNghiemRepository.findAll().size();
        // set the field null
        xetNghiem.setDeleted(null);

        // Create the XetNghiem, which fails.
        XetNghiemDTO xetNghiemDTO = xetNghiemMapper.toDto(xetNghiem);

        restXetNghiemMockMvc.perform(post("/api/xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(xetNghiemDTO)))
            .andExpect(status().isBadRequest());

        List<XetNghiem> xetNghiemList = xetNghiemRepository.findAll();
        assertThat(xetNghiemList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDonGiaBenhVienIsRequired() throws Exception {
        int databaseSizeBeforeTest = xetNghiemRepository.findAll().size();
        // set the field null
        xetNghiem.setDonGiaBenhVien(null);

        // Create the XetNghiem, which fails.
        XetNghiemDTO xetNghiemDTO = xetNghiemMapper.toDto(xetNghiem);

        restXetNghiemMockMvc.perform(post("/api/xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(xetNghiemDTO)))
            .andExpect(status().isBadRequest());

        List<XetNghiem> xetNghiemList = xetNghiemRepository.findAll();
        assertThat(xetNghiemList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEnabledIsRequired() throws Exception {
        int databaseSizeBeforeTest = xetNghiemRepository.findAll().size();
        // set the field null
        xetNghiem.setEnabled(null);

        // Create the XetNghiem, which fails.
        XetNghiemDTO xetNghiemDTO = xetNghiemMapper.toDto(xetNghiem);

        restXetNghiemMockMvc.perform(post("/api/xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(xetNghiemDTO)))
            .andExpect(status().isBadRequest());

        List<XetNghiem> xetNghiemList = xetNghiemRepository.findAll();
        assertThat(xetNghiemList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPhamViChiDinhIsRequired() throws Exception {
        int databaseSizeBeforeTest = xetNghiemRepository.findAll().size();
        // set the field null
        xetNghiem.setPhamViChiDinh(null);

        // Create the XetNghiem, which fails.
        XetNghiemDTO xetNghiemDTO = xetNghiemMapper.toDto(xetNghiem);

        restXetNghiemMockMvc.perform(post("/api/xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(xetNghiemDTO)))
            .andExpect(status().isBadRequest());

        List<XetNghiem> xetNghiemList = xetNghiemRepository.findAll();
        assertThat(xetNghiemList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPhanTheoGioiTInhIsRequired() throws Exception {
        int databaseSizeBeforeTest = xetNghiemRepository.findAll().size();
        // set the field null
        xetNghiem.setPhanTheoGioiTInh(null);

        // Create the XetNghiem, which fails.
        XetNghiemDTO xetNghiemDTO = xetNghiemMapper.toDto(xetNghiem);

        restXetNghiemMockMvc.perform(post("/api/xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(xetNghiemDTO)))
            .andExpect(status().isBadRequest());

        List<XetNghiem> xetNghiemList = xetNghiemRepository.findAll();
        assertThat(xetNghiemList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSoLeLamTronIsRequired() throws Exception {
        int databaseSizeBeforeTest = xetNghiemRepository.findAll().size();
        // set the field null
        xetNghiem.setSoLeLamTron(null);

        // Create the XetNghiem, which fails.
        XetNghiemDTO xetNghiemDTO = xetNghiemMapper.toDto(xetNghiem);

        restXetNghiemMockMvc.perform(post("/api/xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(xetNghiemDTO)))
            .andExpect(status().isBadRequest());

        List<XetNghiem> xetNghiemList = xetNghiemRepository.findAll();
        assertThat(xetNghiemList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSoLuongThucHienIsRequired() throws Exception {
        int databaseSizeBeforeTest = xetNghiemRepository.findAll().size();
        // set the field null
        xetNghiem.setSoLuongThucHien(null);

        // Create the XetNghiem, which fails.
        XetNghiemDTO xetNghiemDTO = xetNghiemMapper.toDto(xetNghiem);

        restXetNghiemMockMvc.perform(post("/api/xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(xetNghiemDTO)))
            .andExpect(status().isBadRequest());

        List<XetNghiem> xetNghiemList = xetNghiemRepository.findAll();
        assertThat(xetNghiemList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTenIsRequired() throws Exception {
        int databaseSizeBeforeTest = xetNghiemRepository.findAll().size();
        // set the field null
        xetNghiem.setTen(null);

        // Create the XetNghiem, which fails.
        XetNghiemDTO xetNghiemDTO = xetNghiemMapper.toDto(xetNghiem);

        restXetNghiemMockMvc.perform(post("/api/xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(xetNghiemDTO)))
            .andExpect(status().isBadRequest());

        List<XetNghiem> xetNghiemList = xetNghiemRepository.findAll();
        assertThat(xetNghiemList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTenHienThiIsRequired() throws Exception {
        int databaseSizeBeforeTest = xetNghiemRepository.findAll().size();
        // set the field null
        xetNghiem.setTenHienThi(null);

        // Create the XetNghiem, which fails.
        XetNghiemDTO xetNghiemDTO = xetNghiemMapper.toDto(xetNghiem);

        restXetNghiemMockMvc.perform(post("/api/xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(xetNghiemDTO)))
            .andExpect(status().isBadRequest());

        List<XetNghiem> xetNghiemList = xetNghiemRepository.findAll();
        assertThat(xetNghiemList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkMaNoiBoIsRequired() throws Exception {
        int databaseSizeBeforeTest = xetNghiemRepository.findAll().size();
        // set the field null
        xetNghiem.setMaNoiBo(null);

        // Create the XetNghiem, which fails.
        XetNghiemDTO xetNghiemDTO = xetNghiemMapper.toDto(xetNghiem);

        restXetNghiemMockMvc.perform(post("/api/xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(xetNghiemDTO)))
            .andExpect(status().isBadRequest());

        List<XetNghiem> xetNghiemList = xetNghiemRepository.findAll();
        assertThat(xetNghiemList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllXetNghiems() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList
        restXetNghiemMockMvc.perform(get("/api/xet-nghiems?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(xetNghiem.getId().intValue())))
            .andExpect(jsonPath("$.[*].canDuoiNam").value(hasItem(DEFAULT_CAN_DUOI_NAM.intValue())))
            .andExpect(jsonPath("$.[*].canDuoiNu").value(hasItem(DEFAULT_CAN_DUOI_NU.intValue())))
            .andExpect(jsonPath("$.[*].canTrenNam").value(hasItem(DEFAULT_CAN_TREN_NAM.intValue())))
            .andExpect(jsonPath("$.[*].canTrenNu").value(hasItem(DEFAULT_CAN_TREN_NU.intValue())))
            .andExpect(jsonPath("$.[*].chiSoBinhThuongNam").value(hasItem(DEFAULT_CHI_SO_BINH_THUONG_NAM)))
            .andExpect(jsonPath("$.[*].chiSoMax").value(hasItem(DEFAULT_CHI_SO_MAX.intValue())))
            .andExpect(jsonPath("$.[*].chiSoMin").value(hasItem(DEFAULT_CHI_SO_MIN.intValue())))
            .andExpect(jsonPath("$.[*].congThuc").value(hasItem(DEFAULT_CONG_THUC)))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].doPhaLoang").value(hasItem(DEFAULT_DO_PHA_LOANG)))
            .andExpect(jsonPath("$.[*].donGiaBenhVien").value(hasItem(DEFAULT_DON_GIA_BENH_VIEN.intValue())))
            .andExpect(jsonPath("$.[*].donViTinh").value(hasItem(DEFAULT_DON_VI_TINH)))
            .andExpect(jsonPath("$.[*].enabled").value(hasItem(DEFAULT_ENABLED.booleanValue())))
            .andExpect(jsonPath("$.[*].gioiHanChiDinh").value(hasItem(DEFAULT_GIOI_HAN_CHI_DINH.intValue())))
            .andExpect(jsonPath("$.[*].ketQuaBatThuong").value(hasItem(DEFAULT_KET_QUA_BAT_THUONG)))
            .andExpect(jsonPath("$.[*].maDungChung").value(hasItem(DEFAULT_MA_DUNG_CHUNG)))
            .andExpect(jsonPath("$.[*].ketQuaMacDinh").value(hasItem(DEFAULT_KET_QUA_MAC_DINH)))
            .andExpect(jsonPath("$.[*].phamViChiDinh").value(hasItem(DEFAULT_PHAM_VI_CHI_DINH.booleanValue())))
            .andExpect(jsonPath("$.[*].phanTheoGioiTInh").value(hasItem(DEFAULT_PHAN_THEO_GIOI_T_INH.booleanValue())))
            .andExpect(jsonPath("$.[*].soLeLamTron").value(hasItem(DEFAULT_SO_LE_LAM_TRON)))
            .andExpect(jsonPath("$.[*].soLuongThucHien").value(hasItem(DEFAULT_SO_LUONG_THUC_HIEN.intValue())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].tenHienThi").value(hasItem(DEFAULT_TEN_HIEN_THI)))
            .andExpect(jsonPath("$.[*].maNoiBo").value(hasItem(DEFAULT_MA_NOI_BO)));
    }
    
    @Test
    @Transactional
    public void getXetNghiem() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get the xetNghiem
        restXetNghiemMockMvc.perform(get("/api/xet-nghiems/{id}", xetNghiem.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(xetNghiem.getId().intValue()))
            .andExpect(jsonPath("$.canDuoiNam").value(DEFAULT_CAN_DUOI_NAM.intValue()))
            .andExpect(jsonPath("$.canDuoiNu").value(DEFAULT_CAN_DUOI_NU.intValue()))
            .andExpect(jsonPath("$.canTrenNam").value(DEFAULT_CAN_TREN_NAM.intValue()))
            .andExpect(jsonPath("$.canTrenNu").value(DEFAULT_CAN_TREN_NU.intValue()))
            .andExpect(jsonPath("$.chiSoBinhThuongNam").value(DEFAULT_CHI_SO_BINH_THUONG_NAM))
            .andExpect(jsonPath("$.chiSoMax").value(DEFAULT_CHI_SO_MAX.intValue()))
            .andExpect(jsonPath("$.chiSoMin").value(DEFAULT_CHI_SO_MIN.intValue()))
            .andExpect(jsonPath("$.congThuc").value(DEFAULT_CONG_THUC))
            .andExpect(jsonPath("$.deleted").value(DEFAULT_DELETED.booleanValue()))
            .andExpect(jsonPath("$.doPhaLoang").value(DEFAULT_DO_PHA_LOANG))
            .andExpect(jsonPath("$.donGiaBenhVien").value(DEFAULT_DON_GIA_BENH_VIEN.intValue()))
            .andExpect(jsonPath("$.donViTinh").value(DEFAULT_DON_VI_TINH))
            .andExpect(jsonPath("$.enabled").value(DEFAULT_ENABLED.booleanValue()))
            .andExpect(jsonPath("$.gioiHanChiDinh").value(DEFAULT_GIOI_HAN_CHI_DINH.intValue()))
            .andExpect(jsonPath("$.ketQuaBatThuong").value(DEFAULT_KET_QUA_BAT_THUONG))
            .andExpect(jsonPath("$.maDungChung").value(DEFAULT_MA_DUNG_CHUNG))
            .andExpect(jsonPath("$.ketQuaMacDinh").value(DEFAULT_KET_QUA_MAC_DINH))
            .andExpect(jsonPath("$.phamViChiDinh").value(DEFAULT_PHAM_VI_CHI_DINH.booleanValue()))
            .andExpect(jsonPath("$.phanTheoGioiTInh").value(DEFAULT_PHAN_THEO_GIOI_T_INH.booleanValue()))
            .andExpect(jsonPath("$.soLeLamTron").value(DEFAULT_SO_LE_LAM_TRON))
            .andExpect(jsonPath("$.soLuongThucHien").value(DEFAULT_SO_LUONG_THUC_HIEN.intValue()))
            .andExpect(jsonPath("$.ten").value(DEFAULT_TEN))
            .andExpect(jsonPath("$.tenHienThi").value(DEFAULT_TEN_HIEN_THI))
            .andExpect(jsonPath("$.maNoiBo").value(DEFAULT_MA_NOI_BO));
    }


    @Test
    @Transactional
    public void getXetNghiemsByIdFiltering() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        Long id = xetNghiem.getId();

        defaultXetNghiemShouldBeFound("id.equals=" + id);
        defaultXetNghiemShouldNotBeFound("id.notEquals=" + id);

        defaultXetNghiemShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultXetNghiemShouldNotBeFound("id.greaterThan=" + id);

        defaultXetNghiemShouldBeFound("id.lessThanOrEqual=" + id);
        defaultXetNghiemShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllXetNghiemsByCanDuoiNamIsEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where canDuoiNam equals to DEFAULT_CAN_DUOI_NAM
        defaultXetNghiemShouldBeFound("canDuoiNam.equals=" + DEFAULT_CAN_DUOI_NAM);

        // Get all the xetNghiemList where canDuoiNam equals to UPDATED_CAN_DUOI_NAM
        defaultXetNghiemShouldNotBeFound("canDuoiNam.equals=" + UPDATED_CAN_DUOI_NAM);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByCanDuoiNamIsNotEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where canDuoiNam not equals to DEFAULT_CAN_DUOI_NAM
        defaultXetNghiemShouldNotBeFound("canDuoiNam.notEquals=" + DEFAULT_CAN_DUOI_NAM);

        // Get all the xetNghiemList where canDuoiNam not equals to UPDATED_CAN_DUOI_NAM
        defaultXetNghiemShouldBeFound("canDuoiNam.notEquals=" + UPDATED_CAN_DUOI_NAM);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByCanDuoiNamIsInShouldWork() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where canDuoiNam in DEFAULT_CAN_DUOI_NAM or UPDATED_CAN_DUOI_NAM
        defaultXetNghiemShouldBeFound("canDuoiNam.in=" + DEFAULT_CAN_DUOI_NAM + "," + UPDATED_CAN_DUOI_NAM);

        // Get all the xetNghiemList where canDuoiNam equals to UPDATED_CAN_DUOI_NAM
        defaultXetNghiemShouldNotBeFound("canDuoiNam.in=" + UPDATED_CAN_DUOI_NAM);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByCanDuoiNamIsNullOrNotNull() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where canDuoiNam is not null
        defaultXetNghiemShouldBeFound("canDuoiNam.specified=true");

        // Get all the xetNghiemList where canDuoiNam is null
        defaultXetNghiemShouldNotBeFound("canDuoiNam.specified=false");
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByCanDuoiNamIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where canDuoiNam is greater than or equal to DEFAULT_CAN_DUOI_NAM
        defaultXetNghiemShouldBeFound("canDuoiNam.greaterThanOrEqual=" + DEFAULT_CAN_DUOI_NAM);

        // Get all the xetNghiemList where canDuoiNam is greater than or equal to UPDATED_CAN_DUOI_NAM
        defaultXetNghiemShouldNotBeFound("canDuoiNam.greaterThanOrEqual=" + UPDATED_CAN_DUOI_NAM);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByCanDuoiNamIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where canDuoiNam is less than or equal to DEFAULT_CAN_DUOI_NAM
        defaultXetNghiemShouldBeFound("canDuoiNam.lessThanOrEqual=" + DEFAULT_CAN_DUOI_NAM);

        // Get all the xetNghiemList where canDuoiNam is less than or equal to SMALLER_CAN_DUOI_NAM
        defaultXetNghiemShouldNotBeFound("canDuoiNam.lessThanOrEqual=" + SMALLER_CAN_DUOI_NAM);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByCanDuoiNamIsLessThanSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where canDuoiNam is less than DEFAULT_CAN_DUOI_NAM
        defaultXetNghiemShouldNotBeFound("canDuoiNam.lessThan=" + DEFAULT_CAN_DUOI_NAM);

        // Get all the xetNghiemList where canDuoiNam is less than UPDATED_CAN_DUOI_NAM
        defaultXetNghiemShouldBeFound("canDuoiNam.lessThan=" + UPDATED_CAN_DUOI_NAM);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByCanDuoiNamIsGreaterThanSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where canDuoiNam is greater than DEFAULT_CAN_DUOI_NAM
        defaultXetNghiemShouldNotBeFound("canDuoiNam.greaterThan=" + DEFAULT_CAN_DUOI_NAM);

        // Get all the xetNghiemList where canDuoiNam is greater than SMALLER_CAN_DUOI_NAM
        defaultXetNghiemShouldBeFound("canDuoiNam.greaterThan=" + SMALLER_CAN_DUOI_NAM);
    }


    @Test
    @Transactional
    public void getAllXetNghiemsByCanDuoiNuIsEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where canDuoiNu equals to DEFAULT_CAN_DUOI_NU
        defaultXetNghiemShouldBeFound("canDuoiNu.equals=" + DEFAULT_CAN_DUOI_NU);

        // Get all the xetNghiemList where canDuoiNu equals to UPDATED_CAN_DUOI_NU
        defaultXetNghiemShouldNotBeFound("canDuoiNu.equals=" + UPDATED_CAN_DUOI_NU);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByCanDuoiNuIsNotEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where canDuoiNu not equals to DEFAULT_CAN_DUOI_NU
        defaultXetNghiemShouldNotBeFound("canDuoiNu.notEquals=" + DEFAULT_CAN_DUOI_NU);

        // Get all the xetNghiemList where canDuoiNu not equals to UPDATED_CAN_DUOI_NU
        defaultXetNghiemShouldBeFound("canDuoiNu.notEquals=" + UPDATED_CAN_DUOI_NU);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByCanDuoiNuIsInShouldWork() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where canDuoiNu in DEFAULT_CAN_DUOI_NU or UPDATED_CAN_DUOI_NU
        defaultXetNghiemShouldBeFound("canDuoiNu.in=" + DEFAULT_CAN_DUOI_NU + "," + UPDATED_CAN_DUOI_NU);

        // Get all the xetNghiemList where canDuoiNu equals to UPDATED_CAN_DUOI_NU
        defaultXetNghiemShouldNotBeFound("canDuoiNu.in=" + UPDATED_CAN_DUOI_NU);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByCanDuoiNuIsNullOrNotNull() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where canDuoiNu is not null
        defaultXetNghiemShouldBeFound("canDuoiNu.specified=true");

        // Get all the xetNghiemList where canDuoiNu is null
        defaultXetNghiemShouldNotBeFound("canDuoiNu.specified=false");
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByCanDuoiNuIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where canDuoiNu is greater than or equal to DEFAULT_CAN_DUOI_NU
        defaultXetNghiemShouldBeFound("canDuoiNu.greaterThanOrEqual=" + DEFAULT_CAN_DUOI_NU);

        // Get all the xetNghiemList where canDuoiNu is greater than or equal to UPDATED_CAN_DUOI_NU
        defaultXetNghiemShouldNotBeFound("canDuoiNu.greaterThanOrEqual=" + UPDATED_CAN_DUOI_NU);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByCanDuoiNuIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where canDuoiNu is less than or equal to DEFAULT_CAN_DUOI_NU
        defaultXetNghiemShouldBeFound("canDuoiNu.lessThanOrEqual=" + DEFAULT_CAN_DUOI_NU);

        // Get all the xetNghiemList where canDuoiNu is less than or equal to SMALLER_CAN_DUOI_NU
        defaultXetNghiemShouldNotBeFound("canDuoiNu.lessThanOrEqual=" + SMALLER_CAN_DUOI_NU);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByCanDuoiNuIsLessThanSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where canDuoiNu is less than DEFAULT_CAN_DUOI_NU
        defaultXetNghiemShouldNotBeFound("canDuoiNu.lessThan=" + DEFAULT_CAN_DUOI_NU);

        // Get all the xetNghiemList where canDuoiNu is less than UPDATED_CAN_DUOI_NU
        defaultXetNghiemShouldBeFound("canDuoiNu.lessThan=" + UPDATED_CAN_DUOI_NU);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByCanDuoiNuIsGreaterThanSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where canDuoiNu is greater than DEFAULT_CAN_DUOI_NU
        defaultXetNghiemShouldNotBeFound("canDuoiNu.greaterThan=" + DEFAULT_CAN_DUOI_NU);

        // Get all the xetNghiemList where canDuoiNu is greater than SMALLER_CAN_DUOI_NU
        defaultXetNghiemShouldBeFound("canDuoiNu.greaterThan=" + SMALLER_CAN_DUOI_NU);
    }


    @Test
    @Transactional
    public void getAllXetNghiemsByCanTrenNamIsEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where canTrenNam equals to DEFAULT_CAN_TREN_NAM
        defaultXetNghiemShouldBeFound("canTrenNam.equals=" + DEFAULT_CAN_TREN_NAM);

        // Get all the xetNghiemList where canTrenNam equals to UPDATED_CAN_TREN_NAM
        defaultXetNghiemShouldNotBeFound("canTrenNam.equals=" + UPDATED_CAN_TREN_NAM);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByCanTrenNamIsNotEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where canTrenNam not equals to DEFAULT_CAN_TREN_NAM
        defaultXetNghiemShouldNotBeFound("canTrenNam.notEquals=" + DEFAULT_CAN_TREN_NAM);

        // Get all the xetNghiemList where canTrenNam not equals to UPDATED_CAN_TREN_NAM
        defaultXetNghiemShouldBeFound("canTrenNam.notEquals=" + UPDATED_CAN_TREN_NAM);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByCanTrenNamIsInShouldWork() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where canTrenNam in DEFAULT_CAN_TREN_NAM or UPDATED_CAN_TREN_NAM
        defaultXetNghiemShouldBeFound("canTrenNam.in=" + DEFAULT_CAN_TREN_NAM + "," + UPDATED_CAN_TREN_NAM);

        // Get all the xetNghiemList where canTrenNam equals to UPDATED_CAN_TREN_NAM
        defaultXetNghiemShouldNotBeFound("canTrenNam.in=" + UPDATED_CAN_TREN_NAM);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByCanTrenNamIsNullOrNotNull() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where canTrenNam is not null
        defaultXetNghiemShouldBeFound("canTrenNam.specified=true");

        // Get all the xetNghiemList where canTrenNam is null
        defaultXetNghiemShouldNotBeFound("canTrenNam.specified=false");
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByCanTrenNamIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where canTrenNam is greater than or equal to DEFAULT_CAN_TREN_NAM
        defaultXetNghiemShouldBeFound("canTrenNam.greaterThanOrEqual=" + DEFAULT_CAN_TREN_NAM);

        // Get all the xetNghiemList where canTrenNam is greater than or equal to UPDATED_CAN_TREN_NAM
        defaultXetNghiemShouldNotBeFound("canTrenNam.greaterThanOrEqual=" + UPDATED_CAN_TREN_NAM);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByCanTrenNamIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where canTrenNam is less than or equal to DEFAULT_CAN_TREN_NAM
        defaultXetNghiemShouldBeFound("canTrenNam.lessThanOrEqual=" + DEFAULT_CAN_TREN_NAM);

        // Get all the xetNghiemList where canTrenNam is less than or equal to SMALLER_CAN_TREN_NAM
        defaultXetNghiemShouldNotBeFound("canTrenNam.lessThanOrEqual=" + SMALLER_CAN_TREN_NAM);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByCanTrenNamIsLessThanSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where canTrenNam is less than DEFAULT_CAN_TREN_NAM
        defaultXetNghiemShouldNotBeFound("canTrenNam.lessThan=" + DEFAULT_CAN_TREN_NAM);

        // Get all the xetNghiemList where canTrenNam is less than UPDATED_CAN_TREN_NAM
        defaultXetNghiemShouldBeFound("canTrenNam.lessThan=" + UPDATED_CAN_TREN_NAM);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByCanTrenNamIsGreaterThanSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where canTrenNam is greater than DEFAULT_CAN_TREN_NAM
        defaultXetNghiemShouldNotBeFound("canTrenNam.greaterThan=" + DEFAULT_CAN_TREN_NAM);

        // Get all the xetNghiemList where canTrenNam is greater than SMALLER_CAN_TREN_NAM
        defaultXetNghiemShouldBeFound("canTrenNam.greaterThan=" + SMALLER_CAN_TREN_NAM);
    }


    @Test
    @Transactional
    public void getAllXetNghiemsByCanTrenNuIsEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where canTrenNu equals to DEFAULT_CAN_TREN_NU
        defaultXetNghiemShouldBeFound("canTrenNu.equals=" + DEFAULT_CAN_TREN_NU);

        // Get all the xetNghiemList where canTrenNu equals to UPDATED_CAN_TREN_NU
        defaultXetNghiemShouldNotBeFound("canTrenNu.equals=" + UPDATED_CAN_TREN_NU);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByCanTrenNuIsNotEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where canTrenNu not equals to DEFAULT_CAN_TREN_NU
        defaultXetNghiemShouldNotBeFound("canTrenNu.notEquals=" + DEFAULT_CAN_TREN_NU);

        // Get all the xetNghiemList where canTrenNu not equals to UPDATED_CAN_TREN_NU
        defaultXetNghiemShouldBeFound("canTrenNu.notEquals=" + UPDATED_CAN_TREN_NU);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByCanTrenNuIsInShouldWork() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where canTrenNu in DEFAULT_CAN_TREN_NU or UPDATED_CAN_TREN_NU
        defaultXetNghiemShouldBeFound("canTrenNu.in=" + DEFAULT_CAN_TREN_NU + "," + UPDATED_CAN_TREN_NU);

        // Get all the xetNghiemList where canTrenNu equals to UPDATED_CAN_TREN_NU
        defaultXetNghiemShouldNotBeFound("canTrenNu.in=" + UPDATED_CAN_TREN_NU);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByCanTrenNuIsNullOrNotNull() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where canTrenNu is not null
        defaultXetNghiemShouldBeFound("canTrenNu.specified=true");

        // Get all the xetNghiemList where canTrenNu is null
        defaultXetNghiemShouldNotBeFound("canTrenNu.specified=false");
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByCanTrenNuIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where canTrenNu is greater than or equal to DEFAULT_CAN_TREN_NU
        defaultXetNghiemShouldBeFound("canTrenNu.greaterThanOrEqual=" + DEFAULT_CAN_TREN_NU);

        // Get all the xetNghiemList where canTrenNu is greater than or equal to UPDATED_CAN_TREN_NU
        defaultXetNghiemShouldNotBeFound("canTrenNu.greaterThanOrEqual=" + UPDATED_CAN_TREN_NU);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByCanTrenNuIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where canTrenNu is less than or equal to DEFAULT_CAN_TREN_NU
        defaultXetNghiemShouldBeFound("canTrenNu.lessThanOrEqual=" + DEFAULT_CAN_TREN_NU);

        // Get all the xetNghiemList where canTrenNu is less than or equal to SMALLER_CAN_TREN_NU
        defaultXetNghiemShouldNotBeFound("canTrenNu.lessThanOrEqual=" + SMALLER_CAN_TREN_NU);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByCanTrenNuIsLessThanSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where canTrenNu is less than DEFAULT_CAN_TREN_NU
        defaultXetNghiemShouldNotBeFound("canTrenNu.lessThan=" + DEFAULT_CAN_TREN_NU);

        // Get all the xetNghiemList where canTrenNu is less than UPDATED_CAN_TREN_NU
        defaultXetNghiemShouldBeFound("canTrenNu.lessThan=" + UPDATED_CAN_TREN_NU);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByCanTrenNuIsGreaterThanSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where canTrenNu is greater than DEFAULT_CAN_TREN_NU
        defaultXetNghiemShouldNotBeFound("canTrenNu.greaterThan=" + DEFAULT_CAN_TREN_NU);

        // Get all the xetNghiemList where canTrenNu is greater than SMALLER_CAN_TREN_NU
        defaultXetNghiemShouldBeFound("canTrenNu.greaterThan=" + SMALLER_CAN_TREN_NU);
    }


    @Test
    @Transactional
    public void getAllXetNghiemsByChiSoBinhThuongNamIsEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where chiSoBinhThuongNam equals to DEFAULT_CHI_SO_BINH_THUONG_NAM
        defaultXetNghiemShouldBeFound("chiSoBinhThuongNam.equals=" + DEFAULT_CHI_SO_BINH_THUONG_NAM);

        // Get all the xetNghiemList where chiSoBinhThuongNam equals to UPDATED_CHI_SO_BINH_THUONG_NAM
        defaultXetNghiemShouldNotBeFound("chiSoBinhThuongNam.equals=" + UPDATED_CHI_SO_BINH_THUONG_NAM);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByChiSoBinhThuongNamIsNotEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where chiSoBinhThuongNam not equals to DEFAULT_CHI_SO_BINH_THUONG_NAM
        defaultXetNghiemShouldNotBeFound("chiSoBinhThuongNam.notEquals=" + DEFAULT_CHI_SO_BINH_THUONG_NAM);

        // Get all the xetNghiemList where chiSoBinhThuongNam not equals to UPDATED_CHI_SO_BINH_THUONG_NAM
        defaultXetNghiemShouldBeFound("chiSoBinhThuongNam.notEquals=" + UPDATED_CHI_SO_BINH_THUONG_NAM);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByChiSoBinhThuongNamIsInShouldWork() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where chiSoBinhThuongNam in DEFAULT_CHI_SO_BINH_THUONG_NAM or UPDATED_CHI_SO_BINH_THUONG_NAM
        defaultXetNghiemShouldBeFound("chiSoBinhThuongNam.in=" + DEFAULT_CHI_SO_BINH_THUONG_NAM + "," + UPDATED_CHI_SO_BINH_THUONG_NAM);

        // Get all the xetNghiemList where chiSoBinhThuongNam equals to UPDATED_CHI_SO_BINH_THUONG_NAM
        defaultXetNghiemShouldNotBeFound("chiSoBinhThuongNam.in=" + UPDATED_CHI_SO_BINH_THUONG_NAM);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByChiSoBinhThuongNamIsNullOrNotNull() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where chiSoBinhThuongNam is not null
        defaultXetNghiemShouldBeFound("chiSoBinhThuongNam.specified=true");

        // Get all the xetNghiemList where chiSoBinhThuongNam is null
        defaultXetNghiemShouldNotBeFound("chiSoBinhThuongNam.specified=false");
    }
                @Test
    @Transactional
    public void getAllXetNghiemsByChiSoBinhThuongNamContainsSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where chiSoBinhThuongNam contains DEFAULT_CHI_SO_BINH_THUONG_NAM
        defaultXetNghiemShouldBeFound("chiSoBinhThuongNam.contains=" + DEFAULT_CHI_SO_BINH_THUONG_NAM);

        // Get all the xetNghiemList where chiSoBinhThuongNam contains UPDATED_CHI_SO_BINH_THUONG_NAM
        defaultXetNghiemShouldNotBeFound("chiSoBinhThuongNam.contains=" + UPDATED_CHI_SO_BINH_THUONG_NAM);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByChiSoBinhThuongNamNotContainsSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where chiSoBinhThuongNam does not contain DEFAULT_CHI_SO_BINH_THUONG_NAM
        defaultXetNghiemShouldNotBeFound("chiSoBinhThuongNam.doesNotContain=" + DEFAULT_CHI_SO_BINH_THUONG_NAM);

        // Get all the xetNghiemList where chiSoBinhThuongNam does not contain UPDATED_CHI_SO_BINH_THUONG_NAM
        defaultXetNghiemShouldBeFound("chiSoBinhThuongNam.doesNotContain=" + UPDATED_CHI_SO_BINH_THUONG_NAM);
    }


    @Test
    @Transactional
    public void getAllXetNghiemsByChiSoMaxIsEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where chiSoMax equals to DEFAULT_CHI_SO_MAX
        defaultXetNghiemShouldBeFound("chiSoMax.equals=" + DEFAULT_CHI_SO_MAX);

        // Get all the xetNghiemList where chiSoMax equals to UPDATED_CHI_SO_MAX
        defaultXetNghiemShouldNotBeFound("chiSoMax.equals=" + UPDATED_CHI_SO_MAX);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByChiSoMaxIsNotEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where chiSoMax not equals to DEFAULT_CHI_SO_MAX
        defaultXetNghiemShouldNotBeFound("chiSoMax.notEquals=" + DEFAULT_CHI_SO_MAX);

        // Get all the xetNghiemList where chiSoMax not equals to UPDATED_CHI_SO_MAX
        defaultXetNghiemShouldBeFound("chiSoMax.notEquals=" + UPDATED_CHI_SO_MAX);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByChiSoMaxIsInShouldWork() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where chiSoMax in DEFAULT_CHI_SO_MAX or UPDATED_CHI_SO_MAX
        defaultXetNghiemShouldBeFound("chiSoMax.in=" + DEFAULT_CHI_SO_MAX + "," + UPDATED_CHI_SO_MAX);

        // Get all the xetNghiemList where chiSoMax equals to UPDATED_CHI_SO_MAX
        defaultXetNghiemShouldNotBeFound("chiSoMax.in=" + UPDATED_CHI_SO_MAX);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByChiSoMaxIsNullOrNotNull() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where chiSoMax is not null
        defaultXetNghiemShouldBeFound("chiSoMax.specified=true");

        // Get all the xetNghiemList where chiSoMax is null
        defaultXetNghiemShouldNotBeFound("chiSoMax.specified=false");
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByChiSoMaxIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where chiSoMax is greater than or equal to DEFAULT_CHI_SO_MAX
        defaultXetNghiemShouldBeFound("chiSoMax.greaterThanOrEqual=" + DEFAULT_CHI_SO_MAX);

        // Get all the xetNghiemList where chiSoMax is greater than or equal to UPDATED_CHI_SO_MAX
        defaultXetNghiemShouldNotBeFound("chiSoMax.greaterThanOrEqual=" + UPDATED_CHI_SO_MAX);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByChiSoMaxIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where chiSoMax is less than or equal to DEFAULT_CHI_SO_MAX
        defaultXetNghiemShouldBeFound("chiSoMax.lessThanOrEqual=" + DEFAULT_CHI_SO_MAX);

        // Get all the xetNghiemList where chiSoMax is less than or equal to SMALLER_CHI_SO_MAX
        defaultXetNghiemShouldNotBeFound("chiSoMax.lessThanOrEqual=" + SMALLER_CHI_SO_MAX);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByChiSoMaxIsLessThanSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where chiSoMax is less than DEFAULT_CHI_SO_MAX
        defaultXetNghiemShouldNotBeFound("chiSoMax.lessThan=" + DEFAULT_CHI_SO_MAX);

        // Get all the xetNghiemList where chiSoMax is less than UPDATED_CHI_SO_MAX
        defaultXetNghiemShouldBeFound("chiSoMax.lessThan=" + UPDATED_CHI_SO_MAX);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByChiSoMaxIsGreaterThanSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where chiSoMax is greater than DEFAULT_CHI_SO_MAX
        defaultXetNghiemShouldNotBeFound("chiSoMax.greaterThan=" + DEFAULT_CHI_SO_MAX);

        // Get all the xetNghiemList where chiSoMax is greater than SMALLER_CHI_SO_MAX
        defaultXetNghiemShouldBeFound("chiSoMax.greaterThan=" + SMALLER_CHI_SO_MAX);
    }


    @Test
    @Transactional
    public void getAllXetNghiemsByChiSoMinIsEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where chiSoMin equals to DEFAULT_CHI_SO_MIN
        defaultXetNghiemShouldBeFound("chiSoMin.equals=" + DEFAULT_CHI_SO_MIN);

        // Get all the xetNghiemList where chiSoMin equals to UPDATED_CHI_SO_MIN
        defaultXetNghiemShouldNotBeFound("chiSoMin.equals=" + UPDATED_CHI_SO_MIN);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByChiSoMinIsNotEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where chiSoMin not equals to DEFAULT_CHI_SO_MIN
        defaultXetNghiemShouldNotBeFound("chiSoMin.notEquals=" + DEFAULT_CHI_SO_MIN);

        // Get all the xetNghiemList where chiSoMin not equals to UPDATED_CHI_SO_MIN
        defaultXetNghiemShouldBeFound("chiSoMin.notEquals=" + UPDATED_CHI_SO_MIN);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByChiSoMinIsInShouldWork() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where chiSoMin in DEFAULT_CHI_SO_MIN or UPDATED_CHI_SO_MIN
        defaultXetNghiemShouldBeFound("chiSoMin.in=" + DEFAULT_CHI_SO_MIN + "," + UPDATED_CHI_SO_MIN);

        // Get all the xetNghiemList where chiSoMin equals to UPDATED_CHI_SO_MIN
        defaultXetNghiemShouldNotBeFound("chiSoMin.in=" + UPDATED_CHI_SO_MIN);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByChiSoMinIsNullOrNotNull() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where chiSoMin is not null
        defaultXetNghiemShouldBeFound("chiSoMin.specified=true");

        // Get all the xetNghiemList where chiSoMin is null
        defaultXetNghiemShouldNotBeFound("chiSoMin.specified=false");
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByChiSoMinIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where chiSoMin is greater than or equal to DEFAULT_CHI_SO_MIN
        defaultXetNghiemShouldBeFound("chiSoMin.greaterThanOrEqual=" + DEFAULT_CHI_SO_MIN);

        // Get all the xetNghiemList where chiSoMin is greater than or equal to UPDATED_CHI_SO_MIN
        defaultXetNghiemShouldNotBeFound("chiSoMin.greaterThanOrEqual=" + UPDATED_CHI_SO_MIN);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByChiSoMinIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where chiSoMin is less than or equal to DEFAULT_CHI_SO_MIN
        defaultXetNghiemShouldBeFound("chiSoMin.lessThanOrEqual=" + DEFAULT_CHI_SO_MIN);

        // Get all the xetNghiemList where chiSoMin is less than or equal to SMALLER_CHI_SO_MIN
        defaultXetNghiemShouldNotBeFound("chiSoMin.lessThanOrEqual=" + SMALLER_CHI_SO_MIN);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByChiSoMinIsLessThanSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where chiSoMin is less than DEFAULT_CHI_SO_MIN
        defaultXetNghiemShouldNotBeFound("chiSoMin.lessThan=" + DEFAULT_CHI_SO_MIN);

        // Get all the xetNghiemList where chiSoMin is less than UPDATED_CHI_SO_MIN
        defaultXetNghiemShouldBeFound("chiSoMin.lessThan=" + UPDATED_CHI_SO_MIN);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByChiSoMinIsGreaterThanSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where chiSoMin is greater than DEFAULT_CHI_SO_MIN
        defaultXetNghiemShouldNotBeFound("chiSoMin.greaterThan=" + DEFAULT_CHI_SO_MIN);

        // Get all the xetNghiemList where chiSoMin is greater than SMALLER_CHI_SO_MIN
        defaultXetNghiemShouldBeFound("chiSoMin.greaterThan=" + SMALLER_CHI_SO_MIN);
    }


    @Test
    @Transactional
    public void getAllXetNghiemsByCongThucIsEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where congThuc equals to DEFAULT_CONG_THUC
        defaultXetNghiemShouldBeFound("congThuc.equals=" + DEFAULT_CONG_THUC);

        // Get all the xetNghiemList where congThuc equals to UPDATED_CONG_THUC
        defaultXetNghiemShouldNotBeFound("congThuc.equals=" + UPDATED_CONG_THUC);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByCongThucIsNotEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where congThuc not equals to DEFAULT_CONG_THUC
        defaultXetNghiemShouldNotBeFound("congThuc.notEquals=" + DEFAULT_CONG_THUC);

        // Get all the xetNghiemList where congThuc not equals to UPDATED_CONG_THUC
        defaultXetNghiemShouldBeFound("congThuc.notEquals=" + UPDATED_CONG_THUC);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByCongThucIsInShouldWork() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where congThuc in DEFAULT_CONG_THUC or UPDATED_CONG_THUC
        defaultXetNghiemShouldBeFound("congThuc.in=" + DEFAULT_CONG_THUC + "," + UPDATED_CONG_THUC);

        // Get all the xetNghiemList where congThuc equals to UPDATED_CONG_THUC
        defaultXetNghiemShouldNotBeFound("congThuc.in=" + UPDATED_CONG_THUC);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByCongThucIsNullOrNotNull() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where congThuc is not null
        defaultXetNghiemShouldBeFound("congThuc.specified=true");

        // Get all the xetNghiemList where congThuc is null
        defaultXetNghiemShouldNotBeFound("congThuc.specified=false");
    }
                @Test
    @Transactional
    public void getAllXetNghiemsByCongThucContainsSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where congThuc contains DEFAULT_CONG_THUC
        defaultXetNghiemShouldBeFound("congThuc.contains=" + DEFAULT_CONG_THUC);

        // Get all the xetNghiemList where congThuc contains UPDATED_CONG_THUC
        defaultXetNghiemShouldNotBeFound("congThuc.contains=" + UPDATED_CONG_THUC);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByCongThucNotContainsSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where congThuc does not contain DEFAULT_CONG_THUC
        defaultXetNghiemShouldNotBeFound("congThuc.doesNotContain=" + DEFAULT_CONG_THUC);

        // Get all the xetNghiemList where congThuc does not contain UPDATED_CONG_THUC
        defaultXetNghiemShouldBeFound("congThuc.doesNotContain=" + UPDATED_CONG_THUC);
    }


    @Test
    @Transactional
    public void getAllXetNghiemsByDeletedIsEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where deleted equals to DEFAULT_DELETED
        defaultXetNghiemShouldBeFound("deleted.equals=" + DEFAULT_DELETED);

        // Get all the xetNghiemList where deleted equals to UPDATED_DELETED
        defaultXetNghiemShouldNotBeFound("deleted.equals=" + UPDATED_DELETED);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByDeletedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where deleted not equals to DEFAULT_DELETED
        defaultXetNghiemShouldNotBeFound("deleted.notEquals=" + DEFAULT_DELETED);

        // Get all the xetNghiemList where deleted not equals to UPDATED_DELETED
        defaultXetNghiemShouldBeFound("deleted.notEquals=" + UPDATED_DELETED);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByDeletedIsInShouldWork() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where deleted in DEFAULT_DELETED or UPDATED_DELETED
        defaultXetNghiemShouldBeFound("deleted.in=" + DEFAULT_DELETED + "," + UPDATED_DELETED);

        // Get all the xetNghiemList where deleted equals to UPDATED_DELETED
        defaultXetNghiemShouldNotBeFound("deleted.in=" + UPDATED_DELETED);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByDeletedIsNullOrNotNull() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where deleted is not null
        defaultXetNghiemShouldBeFound("deleted.specified=true");

        // Get all the xetNghiemList where deleted is null
        defaultXetNghiemShouldNotBeFound("deleted.specified=false");
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByDoPhaLoangIsEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where doPhaLoang equals to DEFAULT_DO_PHA_LOANG
        defaultXetNghiemShouldBeFound("doPhaLoang.equals=" + DEFAULT_DO_PHA_LOANG);

        // Get all the xetNghiemList where doPhaLoang equals to UPDATED_DO_PHA_LOANG
        defaultXetNghiemShouldNotBeFound("doPhaLoang.equals=" + UPDATED_DO_PHA_LOANG);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByDoPhaLoangIsNotEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where doPhaLoang not equals to DEFAULT_DO_PHA_LOANG
        defaultXetNghiemShouldNotBeFound("doPhaLoang.notEquals=" + DEFAULT_DO_PHA_LOANG);

        // Get all the xetNghiemList where doPhaLoang not equals to UPDATED_DO_PHA_LOANG
        defaultXetNghiemShouldBeFound("doPhaLoang.notEquals=" + UPDATED_DO_PHA_LOANG);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByDoPhaLoangIsInShouldWork() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where doPhaLoang in DEFAULT_DO_PHA_LOANG or UPDATED_DO_PHA_LOANG
        defaultXetNghiemShouldBeFound("doPhaLoang.in=" + DEFAULT_DO_PHA_LOANG + "," + UPDATED_DO_PHA_LOANG);

        // Get all the xetNghiemList where doPhaLoang equals to UPDATED_DO_PHA_LOANG
        defaultXetNghiemShouldNotBeFound("doPhaLoang.in=" + UPDATED_DO_PHA_LOANG);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByDoPhaLoangIsNullOrNotNull() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where doPhaLoang is not null
        defaultXetNghiemShouldBeFound("doPhaLoang.specified=true");

        // Get all the xetNghiemList where doPhaLoang is null
        defaultXetNghiemShouldNotBeFound("doPhaLoang.specified=false");
    }
                @Test
    @Transactional
    public void getAllXetNghiemsByDoPhaLoangContainsSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where doPhaLoang contains DEFAULT_DO_PHA_LOANG
        defaultXetNghiemShouldBeFound("doPhaLoang.contains=" + DEFAULT_DO_PHA_LOANG);

        // Get all the xetNghiemList where doPhaLoang contains UPDATED_DO_PHA_LOANG
        defaultXetNghiemShouldNotBeFound("doPhaLoang.contains=" + UPDATED_DO_PHA_LOANG);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByDoPhaLoangNotContainsSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where doPhaLoang does not contain DEFAULT_DO_PHA_LOANG
        defaultXetNghiemShouldNotBeFound("doPhaLoang.doesNotContain=" + DEFAULT_DO_PHA_LOANG);

        // Get all the xetNghiemList where doPhaLoang does not contain UPDATED_DO_PHA_LOANG
        defaultXetNghiemShouldBeFound("doPhaLoang.doesNotContain=" + UPDATED_DO_PHA_LOANG);
    }


    @Test
    @Transactional
    public void getAllXetNghiemsByDonGiaBenhVienIsEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where donGiaBenhVien equals to DEFAULT_DON_GIA_BENH_VIEN
        defaultXetNghiemShouldBeFound("donGiaBenhVien.equals=" + DEFAULT_DON_GIA_BENH_VIEN);

        // Get all the xetNghiemList where donGiaBenhVien equals to UPDATED_DON_GIA_BENH_VIEN
        defaultXetNghiemShouldNotBeFound("donGiaBenhVien.equals=" + UPDATED_DON_GIA_BENH_VIEN);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByDonGiaBenhVienIsNotEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where donGiaBenhVien not equals to DEFAULT_DON_GIA_BENH_VIEN
        defaultXetNghiemShouldNotBeFound("donGiaBenhVien.notEquals=" + DEFAULT_DON_GIA_BENH_VIEN);

        // Get all the xetNghiemList where donGiaBenhVien not equals to UPDATED_DON_GIA_BENH_VIEN
        defaultXetNghiemShouldBeFound("donGiaBenhVien.notEquals=" + UPDATED_DON_GIA_BENH_VIEN);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByDonGiaBenhVienIsInShouldWork() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where donGiaBenhVien in DEFAULT_DON_GIA_BENH_VIEN or UPDATED_DON_GIA_BENH_VIEN
        defaultXetNghiemShouldBeFound("donGiaBenhVien.in=" + DEFAULT_DON_GIA_BENH_VIEN + "," + UPDATED_DON_GIA_BENH_VIEN);

        // Get all the xetNghiemList where donGiaBenhVien equals to UPDATED_DON_GIA_BENH_VIEN
        defaultXetNghiemShouldNotBeFound("donGiaBenhVien.in=" + UPDATED_DON_GIA_BENH_VIEN);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByDonGiaBenhVienIsNullOrNotNull() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where donGiaBenhVien is not null
        defaultXetNghiemShouldBeFound("donGiaBenhVien.specified=true");

        // Get all the xetNghiemList where donGiaBenhVien is null
        defaultXetNghiemShouldNotBeFound("donGiaBenhVien.specified=false");
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByDonGiaBenhVienIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where donGiaBenhVien is greater than or equal to DEFAULT_DON_GIA_BENH_VIEN
        defaultXetNghiemShouldBeFound("donGiaBenhVien.greaterThanOrEqual=" + DEFAULT_DON_GIA_BENH_VIEN);

        // Get all the xetNghiemList where donGiaBenhVien is greater than or equal to UPDATED_DON_GIA_BENH_VIEN
        defaultXetNghiemShouldNotBeFound("donGiaBenhVien.greaterThanOrEqual=" + UPDATED_DON_GIA_BENH_VIEN);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByDonGiaBenhVienIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where donGiaBenhVien is less than or equal to DEFAULT_DON_GIA_BENH_VIEN
        defaultXetNghiemShouldBeFound("donGiaBenhVien.lessThanOrEqual=" + DEFAULT_DON_GIA_BENH_VIEN);

        // Get all the xetNghiemList where donGiaBenhVien is less than or equal to SMALLER_DON_GIA_BENH_VIEN
        defaultXetNghiemShouldNotBeFound("donGiaBenhVien.lessThanOrEqual=" + SMALLER_DON_GIA_BENH_VIEN);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByDonGiaBenhVienIsLessThanSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where donGiaBenhVien is less than DEFAULT_DON_GIA_BENH_VIEN
        defaultXetNghiemShouldNotBeFound("donGiaBenhVien.lessThan=" + DEFAULT_DON_GIA_BENH_VIEN);

        // Get all the xetNghiemList where donGiaBenhVien is less than UPDATED_DON_GIA_BENH_VIEN
        defaultXetNghiemShouldBeFound("donGiaBenhVien.lessThan=" + UPDATED_DON_GIA_BENH_VIEN);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByDonGiaBenhVienIsGreaterThanSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where donGiaBenhVien is greater than DEFAULT_DON_GIA_BENH_VIEN
        defaultXetNghiemShouldNotBeFound("donGiaBenhVien.greaterThan=" + DEFAULT_DON_GIA_BENH_VIEN);

        // Get all the xetNghiemList where donGiaBenhVien is greater than SMALLER_DON_GIA_BENH_VIEN
        defaultXetNghiemShouldBeFound("donGiaBenhVien.greaterThan=" + SMALLER_DON_GIA_BENH_VIEN);
    }


    @Test
    @Transactional
    public void getAllXetNghiemsByDonViTinhIsEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where donViTinh equals to DEFAULT_DON_VI_TINH
        defaultXetNghiemShouldBeFound("donViTinh.equals=" + DEFAULT_DON_VI_TINH);

        // Get all the xetNghiemList where donViTinh equals to UPDATED_DON_VI_TINH
        defaultXetNghiemShouldNotBeFound("donViTinh.equals=" + UPDATED_DON_VI_TINH);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByDonViTinhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where donViTinh not equals to DEFAULT_DON_VI_TINH
        defaultXetNghiemShouldNotBeFound("donViTinh.notEquals=" + DEFAULT_DON_VI_TINH);

        // Get all the xetNghiemList where donViTinh not equals to UPDATED_DON_VI_TINH
        defaultXetNghiemShouldBeFound("donViTinh.notEquals=" + UPDATED_DON_VI_TINH);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByDonViTinhIsInShouldWork() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where donViTinh in DEFAULT_DON_VI_TINH or UPDATED_DON_VI_TINH
        defaultXetNghiemShouldBeFound("donViTinh.in=" + DEFAULT_DON_VI_TINH + "," + UPDATED_DON_VI_TINH);

        // Get all the xetNghiemList where donViTinh equals to UPDATED_DON_VI_TINH
        defaultXetNghiemShouldNotBeFound("donViTinh.in=" + UPDATED_DON_VI_TINH);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByDonViTinhIsNullOrNotNull() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where donViTinh is not null
        defaultXetNghiemShouldBeFound("donViTinh.specified=true");

        // Get all the xetNghiemList where donViTinh is null
        defaultXetNghiemShouldNotBeFound("donViTinh.specified=false");
    }
                @Test
    @Transactional
    public void getAllXetNghiemsByDonViTinhContainsSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where donViTinh contains DEFAULT_DON_VI_TINH
        defaultXetNghiemShouldBeFound("donViTinh.contains=" + DEFAULT_DON_VI_TINH);

        // Get all the xetNghiemList where donViTinh contains UPDATED_DON_VI_TINH
        defaultXetNghiemShouldNotBeFound("donViTinh.contains=" + UPDATED_DON_VI_TINH);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByDonViTinhNotContainsSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where donViTinh does not contain DEFAULT_DON_VI_TINH
        defaultXetNghiemShouldNotBeFound("donViTinh.doesNotContain=" + DEFAULT_DON_VI_TINH);

        // Get all the xetNghiemList where donViTinh does not contain UPDATED_DON_VI_TINH
        defaultXetNghiemShouldBeFound("donViTinh.doesNotContain=" + UPDATED_DON_VI_TINH);
    }


    @Test
    @Transactional
    public void getAllXetNghiemsByEnabledIsEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where enabled equals to DEFAULT_ENABLED
        defaultXetNghiemShouldBeFound("enabled.equals=" + DEFAULT_ENABLED);

        // Get all the xetNghiemList where enabled equals to UPDATED_ENABLED
        defaultXetNghiemShouldNotBeFound("enabled.equals=" + UPDATED_ENABLED);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByEnabledIsNotEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where enabled not equals to DEFAULT_ENABLED
        defaultXetNghiemShouldNotBeFound("enabled.notEquals=" + DEFAULT_ENABLED);

        // Get all the xetNghiemList where enabled not equals to UPDATED_ENABLED
        defaultXetNghiemShouldBeFound("enabled.notEquals=" + UPDATED_ENABLED);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByEnabledIsInShouldWork() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where enabled in DEFAULT_ENABLED or UPDATED_ENABLED
        defaultXetNghiemShouldBeFound("enabled.in=" + DEFAULT_ENABLED + "," + UPDATED_ENABLED);

        // Get all the xetNghiemList where enabled equals to UPDATED_ENABLED
        defaultXetNghiemShouldNotBeFound("enabled.in=" + UPDATED_ENABLED);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByEnabledIsNullOrNotNull() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where enabled is not null
        defaultXetNghiemShouldBeFound("enabled.specified=true");

        // Get all the xetNghiemList where enabled is null
        defaultXetNghiemShouldNotBeFound("enabled.specified=false");
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByGioiHanChiDinhIsEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where gioiHanChiDinh equals to DEFAULT_GIOI_HAN_CHI_DINH
        defaultXetNghiemShouldBeFound("gioiHanChiDinh.equals=" + DEFAULT_GIOI_HAN_CHI_DINH);

        // Get all the xetNghiemList where gioiHanChiDinh equals to UPDATED_GIOI_HAN_CHI_DINH
        defaultXetNghiemShouldNotBeFound("gioiHanChiDinh.equals=" + UPDATED_GIOI_HAN_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByGioiHanChiDinhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where gioiHanChiDinh not equals to DEFAULT_GIOI_HAN_CHI_DINH
        defaultXetNghiemShouldNotBeFound("gioiHanChiDinh.notEquals=" + DEFAULT_GIOI_HAN_CHI_DINH);

        // Get all the xetNghiemList where gioiHanChiDinh not equals to UPDATED_GIOI_HAN_CHI_DINH
        defaultXetNghiemShouldBeFound("gioiHanChiDinh.notEquals=" + UPDATED_GIOI_HAN_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByGioiHanChiDinhIsInShouldWork() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where gioiHanChiDinh in DEFAULT_GIOI_HAN_CHI_DINH or UPDATED_GIOI_HAN_CHI_DINH
        defaultXetNghiemShouldBeFound("gioiHanChiDinh.in=" + DEFAULT_GIOI_HAN_CHI_DINH + "," + UPDATED_GIOI_HAN_CHI_DINH);

        // Get all the xetNghiemList where gioiHanChiDinh equals to UPDATED_GIOI_HAN_CHI_DINH
        defaultXetNghiemShouldNotBeFound("gioiHanChiDinh.in=" + UPDATED_GIOI_HAN_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByGioiHanChiDinhIsNullOrNotNull() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where gioiHanChiDinh is not null
        defaultXetNghiemShouldBeFound("gioiHanChiDinh.specified=true");

        // Get all the xetNghiemList where gioiHanChiDinh is null
        defaultXetNghiemShouldNotBeFound("gioiHanChiDinh.specified=false");
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByGioiHanChiDinhIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where gioiHanChiDinh is greater than or equal to DEFAULT_GIOI_HAN_CHI_DINH
        defaultXetNghiemShouldBeFound("gioiHanChiDinh.greaterThanOrEqual=" + DEFAULT_GIOI_HAN_CHI_DINH);

        // Get all the xetNghiemList where gioiHanChiDinh is greater than or equal to UPDATED_GIOI_HAN_CHI_DINH
        defaultXetNghiemShouldNotBeFound("gioiHanChiDinh.greaterThanOrEqual=" + UPDATED_GIOI_HAN_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByGioiHanChiDinhIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where gioiHanChiDinh is less than or equal to DEFAULT_GIOI_HAN_CHI_DINH
        defaultXetNghiemShouldBeFound("gioiHanChiDinh.lessThanOrEqual=" + DEFAULT_GIOI_HAN_CHI_DINH);

        // Get all the xetNghiemList where gioiHanChiDinh is less than or equal to SMALLER_GIOI_HAN_CHI_DINH
        defaultXetNghiemShouldNotBeFound("gioiHanChiDinh.lessThanOrEqual=" + SMALLER_GIOI_HAN_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByGioiHanChiDinhIsLessThanSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where gioiHanChiDinh is less than DEFAULT_GIOI_HAN_CHI_DINH
        defaultXetNghiemShouldNotBeFound("gioiHanChiDinh.lessThan=" + DEFAULT_GIOI_HAN_CHI_DINH);

        // Get all the xetNghiemList where gioiHanChiDinh is less than UPDATED_GIOI_HAN_CHI_DINH
        defaultXetNghiemShouldBeFound("gioiHanChiDinh.lessThan=" + UPDATED_GIOI_HAN_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByGioiHanChiDinhIsGreaterThanSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where gioiHanChiDinh is greater than DEFAULT_GIOI_HAN_CHI_DINH
        defaultXetNghiemShouldNotBeFound("gioiHanChiDinh.greaterThan=" + DEFAULT_GIOI_HAN_CHI_DINH);

        // Get all the xetNghiemList where gioiHanChiDinh is greater than SMALLER_GIOI_HAN_CHI_DINH
        defaultXetNghiemShouldBeFound("gioiHanChiDinh.greaterThan=" + SMALLER_GIOI_HAN_CHI_DINH);
    }


    @Test
    @Transactional
    public void getAllXetNghiemsByKetQuaBatThuongIsEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where ketQuaBatThuong equals to DEFAULT_KET_QUA_BAT_THUONG
        defaultXetNghiemShouldBeFound("ketQuaBatThuong.equals=" + DEFAULT_KET_QUA_BAT_THUONG);

        // Get all the xetNghiemList where ketQuaBatThuong equals to UPDATED_KET_QUA_BAT_THUONG
        defaultXetNghiemShouldNotBeFound("ketQuaBatThuong.equals=" + UPDATED_KET_QUA_BAT_THUONG);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByKetQuaBatThuongIsNotEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where ketQuaBatThuong not equals to DEFAULT_KET_QUA_BAT_THUONG
        defaultXetNghiemShouldNotBeFound("ketQuaBatThuong.notEquals=" + DEFAULT_KET_QUA_BAT_THUONG);

        // Get all the xetNghiemList where ketQuaBatThuong not equals to UPDATED_KET_QUA_BAT_THUONG
        defaultXetNghiemShouldBeFound("ketQuaBatThuong.notEquals=" + UPDATED_KET_QUA_BAT_THUONG);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByKetQuaBatThuongIsInShouldWork() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where ketQuaBatThuong in DEFAULT_KET_QUA_BAT_THUONG or UPDATED_KET_QUA_BAT_THUONG
        defaultXetNghiemShouldBeFound("ketQuaBatThuong.in=" + DEFAULT_KET_QUA_BAT_THUONG + "," + UPDATED_KET_QUA_BAT_THUONG);

        // Get all the xetNghiemList where ketQuaBatThuong equals to UPDATED_KET_QUA_BAT_THUONG
        defaultXetNghiemShouldNotBeFound("ketQuaBatThuong.in=" + UPDATED_KET_QUA_BAT_THUONG);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByKetQuaBatThuongIsNullOrNotNull() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where ketQuaBatThuong is not null
        defaultXetNghiemShouldBeFound("ketQuaBatThuong.specified=true");

        // Get all the xetNghiemList where ketQuaBatThuong is null
        defaultXetNghiemShouldNotBeFound("ketQuaBatThuong.specified=false");
    }
                @Test
    @Transactional
    public void getAllXetNghiemsByKetQuaBatThuongContainsSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where ketQuaBatThuong contains DEFAULT_KET_QUA_BAT_THUONG
        defaultXetNghiemShouldBeFound("ketQuaBatThuong.contains=" + DEFAULT_KET_QUA_BAT_THUONG);

        // Get all the xetNghiemList where ketQuaBatThuong contains UPDATED_KET_QUA_BAT_THUONG
        defaultXetNghiemShouldNotBeFound("ketQuaBatThuong.contains=" + UPDATED_KET_QUA_BAT_THUONG);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByKetQuaBatThuongNotContainsSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where ketQuaBatThuong does not contain DEFAULT_KET_QUA_BAT_THUONG
        defaultXetNghiemShouldNotBeFound("ketQuaBatThuong.doesNotContain=" + DEFAULT_KET_QUA_BAT_THUONG);

        // Get all the xetNghiemList where ketQuaBatThuong does not contain UPDATED_KET_QUA_BAT_THUONG
        defaultXetNghiemShouldBeFound("ketQuaBatThuong.doesNotContain=" + UPDATED_KET_QUA_BAT_THUONG);
    }


    @Test
    @Transactional
    public void getAllXetNghiemsByMaDungChungIsEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where maDungChung equals to DEFAULT_MA_DUNG_CHUNG
        defaultXetNghiemShouldBeFound("maDungChung.equals=" + DEFAULT_MA_DUNG_CHUNG);

        // Get all the xetNghiemList where maDungChung equals to UPDATED_MA_DUNG_CHUNG
        defaultXetNghiemShouldNotBeFound("maDungChung.equals=" + UPDATED_MA_DUNG_CHUNG);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByMaDungChungIsNotEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where maDungChung not equals to DEFAULT_MA_DUNG_CHUNG
        defaultXetNghiemShouldNotBeFound("maDungChung.notEquals=" + DEFAULT_MA_DUNG_CHUNG);

        // Get all the xetNghiemList where maDungChung not equals to UPDATED_MA_DUNG_CHUNG
        defaultXetNghiemShouldBeFound("maDungChung.notEquals=" + UPDATED_MA_DUNG_CHUNG);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByMaDungChungIsInShouldWork() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where maDungChung in DEFAULT_MA_DUNG_CHUNG or UPDATED_MA_DUNG_CHUNG
        defaultXetNghiemShouldBeFound("maDungChung.in=" + DEFAULT_MA_DUNG_CHUNG + "," + UPDATED_MA_DUNG_CHUNG);

        // Get all the xetNghiemList where maDungChung equals to UPDATED_MA_DUNG_CHUNG
        defaultXetNghiemShouldNotBeFound("maDungChung.in=" + UPDATED_MA_DUNG_CHUNG);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByMaDungChungIsNullOrNotNull() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where maDungChung is not null
        defaultXetNghiemShouldBeFound("maDungChung.specified=true");

        // Get all the xetNghiemList where maDungChung is null
        defaultXetNghiemShouldNotBeFound("maDungChung.specified=false");
    }
                @Test
    @Transactional
    public void getAllXetNghiemsByMaDungChungContainsSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where maDungChung contains DEFAULT_MA_DUNG_CHUNG
        defaultXetNghiemShouldBeFound("maDungChung.contains=" + DEFAULT_MA_DUNG_CHUNG);

        // Get all the xetNghiemList where maDungChung contains UPDATED_MA_DUNG_CHUNG
        defaultXetNghiemShouldNotBeFound("maDungChung.contains=" + UPDATED_MA_DUNG_CHUNG);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByMaDungChungNotContainsSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where maDungChung does not contain DEFAULT_MA_DUNG_CHUNG
        defaultXetNghiemShouldNotBeFound("maDungChung.doesNotContain=" + DEFAULT_MA_DUNG_CHUNG);

        // Get all the xetNghiemList where maDungChung does not contain UPDATED_MA_DUNG_CHUNG
        defaultXetNghiemShouldBeFound("maDungChung.doesNotContain=" + UPDATED_MA_DUNG_CHUNG);
    }


    @Test
    @Transactional
    public void getAllXetNghiemsByKetQuaMacDinhIsEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where ketQuaMacDinh equals to DEFAULT_KET_QUA_MAC_DINH
        defaultXetNghiemShouldBeFound("ketQuaMacDinh.equals=" + DEFAULT_KET_QUA_MAC_DINH);

        // Get all the xetNghiemList where ketQuaMacDinh equals to UPDATED_KET_QUA_MAC_DINH
        defaultXetNghiemShouldNotBeFound("ketQuaMacDinh.equals=" + UPDATED_KET_QUA_MAC_DINH);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByKetQuaMacDinhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where ketQuaMacDinh not equals to DEFAULT_KET_QUA_MAC_DINH
        defaultXetNghiemShouldNotBeFound("ketQuaMacDinh.notEquals=" + DEFAULT_KET_QUA_MAC_DINH);

        // Get all the xetNghiemList where ketQuaMacDinh not equals to UPDATED_KET_QUA_MAC_DINH
        defaultXetNghiemShouldBeFound("ketQuaMacDinh.notEquals=" + UPDATED_KET_QUA_MAC_DINH);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByKetQuaMacDinhIsInShouldWork() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where ketQuaMacDinh in DEFAULT_KET_QUA_MAC_DINH or UPDATED_KET_QUA_MAC_DINH
        defaultXetNghiemShouldBeFound("ketQuaMacDinh.in=" + DEFAULT_KET_QUA_MAC_DINH + "," + UPDATED_KET_QUA_MAC_DINH);

        // Get all the xetNghiemList where ketQuaMacDinh equals to UPDATED_KET_QUA_MAC_DINH
        defaultXetNghiemShouldNotBeFound("ketQuaMacDinh.in=" + UPDATED_KET_QUA_MAC_DINH);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByKetQuaMacDinhIsNullOrNotNull() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where ketQuaMacDinh is not null
        defaultXetNghiemShouldBeFound("ketQuaMacDinh.specified=true");

        // Get all the xetNghiemList where ketQuaMacDinh is null
        defaultXetNghiemShouldNotBeFound("ketQuaMacDinh.specified=false");
    }
                @Test
    @Transactional
    public void getAllXetNghiemsByKetQuaMacDinhContainsSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where ketQuaMacDinh contains DEFAULT_KET_QUA_MAC_DINH
        defaultXetNghiemShouldBeFound("ketQuaMacDinh.contains=" + DEFAULT_KET_QUA_MAC_DINH);

        // Get all the xetNghiemList where ketQuaMacDinh contains UPDATED_KET_QUA_MAC_DINH
        defaultXetNghiemShouldNotBeFound("ketQuaMacDinh.contains=" + UPDATED_KET_QUA_MAC_DINH);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByKetQuaMacDinhNotContainsSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where ketQuaMacDinh does not contain DEFAULT_KET_QUA_MAC_DINH
        defaultXetNghiemShouldNotBeFound("ketQuaMacDinh.doesNotContain=" + DEFAULT_KET_QUA_MAC_DINH);

        // Get all the xetNghiemList where ketQuaMacDinh does not contain UPDATED_KET_QUA_MAC_DINH
        defaultXetNghiemShouldBeFound("ketQuaMacDinh.doesNotContain=" + UPDATED_KET_QUA_MAC_DINH);
    }


    @Test
    @Transactional
    public void getAllXetNghiemsByPhamViChiDinhIsEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where phamViChiDinh equals to DEFAULT_PHAM_VI_CHI_DINH
        defaultXetNghiemShouldBeFound("phamViChiDinh.equals=" + DEFAULT_PHAM_VI_CHI_DINH);

        // Get all the xetNghiemList where phamViChiDinh equals to UPDATED_PHAM_VI_CHI_DINH
        defaultXetNghiemShouldNotBeFound("phamViChiDinh.equals=" + UPDATED_PHAM_VI_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByPhamViChiDinhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where phamViChiDinh not equals to DEFAULT_PHAM_VI_CHI_DINH
        defaultXetNghiemShouldNotBeFound("phamViChiDinh.notEquals=" + DEFAULT_PHAM_VI_CHI_DINH);

        // Get all the xetNghiemList where phamViChiDinh not equals to UPDATED_PHAM_VI_CHI_DINH
        defaultXetNghiemShouldBeFound("phamViChiDinh.notEquals=" + UPDATED_PHAM_VI_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByPhamViChiDinhIsInShouldWork() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where phamViChiDinh in DEFAULT_PHAM_VI_CHI_DINH or UPDATED_PHAM_VI_CHI_DINH
        defaultXetNghiemShouldBeFound("phamViChiDinh.in=" + DEFAULT_PHAM_VI_CHI_DINH + "," + UPDATED_PHAM_VI_CHI_DINH);

        // Get all the xetNghiemList where phamViChiDinh equals to UPDATED_PHAM_VI_CHI_DINH
        defaultXetNghiemShouldNotBeFound("phamViChiDinh.in=" + UPDATED_PHAM_VI_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByPhamViChiDinhIsNullOrNotNull() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where phamViChiDinh is not null
        defaultXetNghiemShouldBeFound("phamViChiDinh.specified=true");

        // Get all the xetNghiemList where phamViChiDinh is null
        defaultXetNghiemShouldNotBeFound("phamViChiDinh.specified=false");
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByPhanTheoGioiTInhIsEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where phanTheoGioiTInh equals to DEFAULT_PHAN_THEO_GIOI_T_INH
        defaultXetNghiemShouldBeFound("phanTheoGioiTInh.equals=" + DEFAULT_PHAN_THEO_GIOI_T_INH);

        // Get all the xetNghiemList where phanTheoGioiTInh equals to UPDATED_PHAN_THEO_GIOI_T_INH
        defaultXetNghiemShouldNotBeFound("phanTheoGioiTInh.equals=" + UPDATED_PHAN_THEO_GIOI_T_INH);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByPhanTheoGioiTInhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where phanTheoGioiTInh not equals to DEFAULT_PHAN_THEO_GIOI_T_INH
        defaultXetNghiemShouldNotBeFound("phanTheoGioiTInh.notEquals=" + DEFAULT_PHAN_THEO_GIOI_T_INH);

        // Get all the xetNghiemList where phanTheoGioiTInh not equals to UPDATED_PHAN_THEO_GIOI_T_INH
        defaultXetNghiemShouldBeFound("phanTheoGioiTInh.notEquals=" + UPDATED_PHAN_THEO_GIOI_T_INH);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByPhanTheoGioiTInhIsInShouldWork() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where phanTheoGioiTInh in DEFAULT_PHAN_THEO_GIOI_T_INH or UPDATED_PHAN_THEO_GIOI_T_INH
        defaultXetNghiemShouldBeFound("phanTheoGioiTInh.in=" + DEFAULT_PHAN_THEO_GIOI_T_INH + "," + UPDATED_PHAN_THEO_GIOI_T_INH);

        // Get all the xetNghiemList where phanTheoGioiTInh equals to UPDATED_PHAN_THEO_GIOI_T_INH
        defaultXetNghiemShouldNotBeFound("phanTheoGioiTInh.in=" + UPDATED_PHAN_THEO_GIOI_T_INH);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByPhanTheoGioiTInhIsNullOrNotNull() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where phanTheoGioiTInh is not null
        defaultXetNghiemShouldBeFound("phanTheoGioiTInh.specified=true");

        // Get all the xetNghiemList where phanTheoGioiTInh is null
        defaultXetNghiemShouldNotBeFound("phanTheoGioiTInh.specified=false");
    }

    @Test
    @Transactional
    public void getAllXetNghiemsBySoLeLamTronIsEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where soLeLamTron equals to DEFAULT_SO_LE_LAM_TRON
        defaultXetNghiemShouldBeFound("soLeLamTron.equals=" + DEFAULT_SO_LE_LAM_TRON);

        // Get all the xetNghiemList where soLeLamTron equals to UPDATED_SO_LE_LAM_TRON
        defaultXetNghiemShouldNotBeFound("soLeLamTron.equals=" + UPDATED_SO_LE_LAM_TRON);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsBySoLeLamTronIsNotEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where soLeLamTron not equals to DEFAULT_SO_LE_LAM_TRON
        defaultXetNghiemShouldNotBeFound("soLeLamTron.notEquals=" + DEFAULT_SO_LE_LAM_TRON);

        // Get all the xetNghiemList where soLeLamTron not equals to UPDATED_SO_LE_LAM_TRON
        defaultXetNghiemShouldBeFound("soLeLamTron.notEquals=" + UPDATED_SO_LE_LAM_TRON);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsBySoLeLamTronIsInShouldWork() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where soLeLamTron in DEFAULT_SO_LE_LAM_TRON or UPDATED_SO_LE_LAM_TRON
        defaultXetNghiemShouldBeFound("soLeLamTron.in=" + DEFAULT_SO_LE_LAM_TRON + "," + UPDATED_SO_LE_LAM_TRON);

        // Get all the xetNghiemList where soLeLamTron equals to UPDATED_SO_LE_LAM_TRON
        defaultXetNghiemShouldNotBeFound("soLeLamTron.in=" + UPDATED_SO_LE_LAM_TRON);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsBySoLeLamTronIsNullOrNotNull() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where soLeLamTron is not null
        defaultXetNghiemShouldBeFound("soLeLamTron.specified=true");

        // Get all the xetNghiemList where soLeLamTron is null
        defaultXetNghiemShouldNotBeFound("soLeLamTron.specified=false");
    }

    @Test
    @Transactional
    public void getAllXetNghiemsBySoLeLamTronIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where soLeLamTron is greater than or equal to DEFAULT_SO_LE_LAM_TRON
        defaultXetNghiemShouldBeFound("soLeLamTron.greaterThanOrEqual=" + DEFAULT_SO_LE_LAM_TRON);

        // Get all the xetNghiemList where soLeLamTron is greater than or equal to UPDATED_SO_LE_LAM_TRON
        defaultXetNghiemShouldNotBeFound("soLeLamTron.greaterThanOrEqual=" + UPDATED_SO_LE_LAM_TRON);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsBySoLeLamTronIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where soLeLamTron is less than or equal to DEFAULT_SO_LE_LAM_TRON
        defaultXetNghiemShouldBeFound("soLeLamTron.lessThanOrEqual=" + DEFAULT_SO_LE_LAM_TRON);

        // Get all the xetNghiemList where soLeLamTron is less than or equal to SMALLER_SO_LE_LAM_TRON
        defaultXetNghiemShouldNotBeFound("soLeLamTron.lessThanOrEqual=" + SMALLER_SO_LE_LAM_TRON);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsBySoLeLamTronIsLessThanSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where soLeLamTron is less than DEFAULT_SO_LE_LAM_TRON
        defaultXetNghiemShouldNotBeFound("soLeLamTron.lessThan=" + DEFAULT_SO_LE_LAM_TRON);

        // Get all the xetNghiemList where soLeLamTron is less than UPDATED_SO_LE_LAM_TRON
        defaultXetNghiemShouldBeFound("soLeLamTron.lessThan=" + UPDATED_SO_LE_LAM_TRON);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsBySoLeLamTronIsGreaterThanSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where soLeLamTron is greater than DEFAULT_SO_LE_LAM_TRON
        defaultXetNghiemShouldNotBeFound("soLeLamTron.greaterThan=" + DEFAULT_SO_LE_LAM_TRON);

        // Get all the xetNghiemList where soLeLamTron is greater than SMALLER_SO_LE_LAM_TRON
        defaultXetNghiemShouldBeFound("soLeLamTron.greaterThan=" + SMALLER_SO_LE_LAM_TRON);
    }


    @Test
    @Transactional
    public void getAllXetNghiemsBySoLuongThucHienIsEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where soLuongThucHien equals to DEFAULT_SO_LUONG_THUC_HIEN
        defaultXetNghiemShouldBeFound("soLuongThucHien.equals=" + DEFAULT_SO_LUONG_THUC_HIEN);

        // Get all the xetNghiemList where soLuongThucHien equals to UPDATED_SO_LUONG_THUC_HIEN
        defaultXetNghiemShouldNotBeFound("soLuongThucHien.equals=" + UPDATED_SO_LUONG_THUC_HIEN);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsBySoLuongThucHienIsNotEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where soLuongThucHien not equals to DEFAULT_SO_LUONG_THUC_HIEN
        defaultXetNghiemShouldNotBeFound("soLuongThucHien.notEquals=" + DEFAULT_SO_LUONG_THUC_HIEN);

        // Get all the xetNghiemList where soLuongThucHien not equals to UPDATED_SO_LUONG_THUC_HIEN
        defaultXetNghiemShouldBeFound("soLuongThucHien.notEquals=" + UPDATED_SO_LUONG_THUC_HIEN);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsBySoLuongThucHienIsInShouldWork() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where soLuongThucHien in DEFAULT_SO_LUONG_THUC_HIEN or UPDATED_SO_LUONG_THUC_HIEN
        defaultXetNghiemShouldBeFound("soLuongThucHien.in=" + DEFAULT_SO_LUONG_THUC_HIEN + "," + UPDATED_SO_LUONG_THUC_HIEN);

        // Get all the xetNghiemList where soLuongThucHien equals to UPDATED_SO_LUONG_THUC_HIEN
        defaultXetNghiemShouldNotBeFound("soLuongThucHien.in=" + UPDATED_SO_LUONG_THUC_HIEN);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsBySoLuongThucHienIsNullOrNotNull() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where soLuongThucHien is not null
        defaultXetNghiemShouldBeFound("soLuongThucHien.specified=true");

        // Get all the xetNghiemList where soLuongThucHien is null
        defaultXetNghiemShouldNotBeFound("soLuongThucHien.specified=false");
    }

    @Test
    @Transactional
    public void getAllXetNghiemsBySoLuongThucHienIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where soLuongThucHien is greater than or equal to DEFAULT_SO_LUONG_THUC_HIEN
        defaultXetNghiemShouldBeFound("soLuongThucHien.greaterThanOrEqual=" + DEFAULT_SO_LUONG_THUC_HIEN);

        // Get all the xetNghiemList where soLuongThucHien is greater than or equal to UPDATED_SO_LUONG_THUC_HIEN
        defaultXetNghiemShouldNotBeFound("soLuongThucHien.greaterThanOrEqual=" + UPDATED_SO_LUONG_THUC_HIEN);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsBySoLuongThucHienIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where soLuongThucHien is less than or equal to DEFAULT_SO_LUONG_THUC_HIEN
        defaultXetNghiemShouldBeFound("soLuongThucHien.lessThanOrEqual=" + DEFAULT_SO_LUONG_THUC_HIEN);

        // Get all the xetNghiemList where soLuongThucHien is less than or equal to SMALLER_SO_LUONG_THUC_HIEN
        defaultXetNghiemShouldNotBeFound("soLuongThucHien.lessThanOrEqual=" + SMALLER_SO_LUONG_THUC_HIEN);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsBySoLuongThucHienIsLessThanSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where soLuongThucHien is less than DEFAULT_SO_LUONG_THUC_HIEN
        defaultXetNghiemShouldNotBeFound("soLuongThucHien.lessThan=" + DEFAULT_SO_LUONG_THUC_HIEN);

        // Get all the xetNghiemList where soLuongThucHien is less than UPDATED_SO_LUONG_THUC_HIEN
        defaultXetNghiemShouldBeFound("soLuongThucHien.lessThan=" + UPDATED_SO_LUONG_THUC_HIEN);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsBySoLuongThucHienIsGreaterThanSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where soLuongThucHien is greater than DEFAULT_SO_LUONG_THUC_HIEN
        defaultXetNghiemShouldNotBeFound("soLuongThucHien.greaterThan=" + DEFAULT_SO_LUONG_THUC_HIEN);

        // Get all the xetNghiemList where soLuongThucHien is greater than SMALLER_SO_LUONG_THUC_HIEN
        defaultXetNghiemShouldBeFound("soLuongThucHien.greaterThan=" + SMALLER_SO_LUONG_THUC_HIEN);
    }


    @Test
    @Transactional
    public void getAllXetNghiemsByTenIsEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where ten equals to DEFAULT_TEN
        defaultXetNghiemShouldBeFound("ten.equals=" + DEFAULT_TEN);

        // Get all the xetNghiemList where ten equals to UPDATED_TEN
        defaultXetNghiemShouldNotBeFound("ten.equals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByTenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where ten not equals to DEFAULT_TEN
        defaultXetNghiemShouldNotBeFound("ten.notEquals=" + DEFAULT_TEN);

        // Get all the xetNghiemList where ten not equals to UPDATED_TEN
        defaultXetNghiemShouldBeFound("ten.notEquals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByTenIsInShouldWork() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where ten in DEFAULT_TEN or UPDATED_TEN
        defaultXetNghiemShouldBeFound("ten.in=" + DEFAULT_TEN + "," + UPDATED_TEN);

        // Get all the xetNghiemList where ten equals to UPDATED_TEN
        defaultXetNghiemShouldNotBeFound("ten.in=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByTenIsNullOrNotNull() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where ten is not null
        defaultXetNghiemShouldBeFound("ten.specified=true");

        // Get all the xetNghiemList where ten is null
        defaultXetNghiemShouldNotBeFound("ten.specified=false");
    }
                @Test
    @Transactional
    public void getAllXetNghiemsByTenContainsSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where ten contains DEFAULT_TEN
        defaultXetNghiemShouldBeFound("ten.contains=" + DEFAULT_TEN);

        // Get all the xetNghiemList where ten contains UPDATED_TEN
        defaultXetNghiemShouldNotBeFound("ten.contains=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByTenNotContainsSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where ten does not contain DEFAULT_TEN
        defaultXetNghiemShouldNotBeFound("ten.doesNotContain=" + DEFAULT_TEN);

        // Get all the xetNghiemList where ten does not contain UPDATED_TEN
        defaultXetNghiemShouldBeFound("ten.doesNotContain=" + UPDATED_TEN);
    }


    @Test
    @Transactional
    public void getAllXetNghiemsByTenHienThiIsEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where tenHienThi equals to DEFAULT_TEN_HIEN_THI
        defaultXetNghiemShouldBeFound("tenHienThi.equals=" + DEFAULT_TEN_HIEN_THI);

        // Get all the xetNghiemList where tenHienThi equals to UPDATED_TEN_HIEN_THI
        defaultXetNghiemShouldNotBeFound("tenHienThi.equals=" + UPDATED_TEN_HIEN_THI);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByTenHienThiIsNotEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where tenHienThi not equals to DEFAULT_TEN_HIEN_THI
        defaultXetNghiemShouldNotBeFound("tenHienThi.notEquals=" + DEFAULT_TEN_HIEN_THI);

        // Get all the xetNghiemList where tenHienThi not equals to UPDATED_TEN_HIEN_THI
        defaultXetNghiemShouldBeFound("tenHienThi.notEquals=" + UPDATED_TEN_HIEN_THI);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByTenHienThiIsInShouldWork() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where tenHienThi in DEFAULT_TEN_HIEN_THI or UPDATED_TEN_HIEN_THI
        defaultXetNghiemShouldBeFound("tenHienThi.in=" + DEFAULT_TEN_HIEN_THI + "," + UPDATED_TEN_HIEN_THI);

        // Get all the xetNghiemList where tenHienThi equals to UPDATED_TEN_HIEN_THI
        defaultXetNghiemShouldNotBeFound("tenHienThi.in=" + UPDATED_TEN_HIEN_THI);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByTenHienThiIsNullOrNotNull() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where tenHienThi is not null
        defaultXetNghiemShouldBeFound("tenHienThi.specified=true");

        // Get all the xetNghiemList where tenHienThi is null
        defaultXetNghiemShouldNotBeFound("tenHienThi.specified=false");
    }
                @Test
    @Transactional
    public void getAllXetNghiemsByTenHienThiContainsSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where tenHienThi contains DEFAULT_TEN_HIEN_THI
        defaultXetNghiemShouldBeFound("tenHienThi.contains=" + DEFAULT_TEN_HIEN_THI);

        // Get all the xetNghiemList where tenHienThi contains UPDATED_TEN_HIEN_THI
        defaultXetNghiemShouldNotBeFound("tenHienThi.contains=" + UPDATED_TEN_HIEN_THI);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByTenHienThiNotContainsSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where tenHienThi does not contain DEFAULT_TEN_HIEN_THI
        defaultXetNghiemShouldNotBeFound("tenHienThi.doesNotContain=" + DEFAULT_TEN_HIEN_THI);

        // Get all the xetNghiemList where tenHienThi does not contain UPDATED_TEN_HIEN_THI
        defaultXetNghiemShouldBeFound("tenHienThi.doesNotContain=" + UPDATED_TEN_HIEN_THI);
    }


    @Test
    @Transactional
    public void getAllXetNghiemsByMaNoiBoIsEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where maNoiBo equals to DEFAULT_MA_NOI_BO
        defaultXetNghiemShouldBeFound("maNoiBo.equals=" + DEFAULT_MA_NOI_BO);

        // Get all the xetNghiemList where maNoiBo equals to UPDATED_MA_NOI_BO
        defaultXetNghiemShouldNotBeFound("maNoiBo.equals=" + UPDATED_MA_NOI_BO);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByMaNoiBoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where maNoiBo not equals to DEFAULT_MA_NOI_BO
        defaultXetNghiemShouldNotBeFound("maNoiBo.notEquals=" + DEFAULT_MA_NOI_BO);

        // Get all the xetNghiemList where maNoiBo not equals to UPDATED_MA_NOI_BO
        defaultXetNghiemShouldBeFound("maNoiBo.notEquals=" + UPDATED_MA_NOI_BO);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByMaNoiBoIsInShouldWork() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where maNoiBo in DEFAULT_MA_NOI_BO or UPDATED_MA_NOI_BO
        defaultXetNghiemShouldBeFound("maNoiBo.in=" + DEFAULT_MA_NOI_BO + "," + UPDATED_MA_NOI_BO);

        // Get all the xetNghiemList where maNoiBo equals to UPDATED_MA_NOI_BO
        defaultXetNghiemShouldNotBeFound("maNoiBo.in=" + UPDATED_MA_NOI_BO);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByMaNoiBoIsNullOrNotNull() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where maNoiBo is not null
        defaultXetNghiemShouldBeFound("maNoiBo.specified=true");

        // Get all the xetNghiemList where maNoiBo is null
        defaultXetNghiemShouldNotBeFound("maNoiBo.specified=false");
    }
                @Test
    @Transactional
    public void getAllXetNghiemsByMaNoiBoContainsSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where maNoiBo contains DEFAULT_MA_NOI_BO
        defaultXetNghiemShouldBeFound("maNoiBo.contains=" + DEFAULT_MA_NOI_BO);

        // Get all the xetNghiemList where maNoiBo contains UPDATED_MA_NOI_BO
        defaultXetNghiemShouldNotBeFound("maNoiBo.contains=" + UPDATED_MA_NOI_BO);
    }

    @Test
    @Transactional
    public void getAllXetNghiemsByMaNoiBoNotContainsSomething() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        // Get all the xetNghiemList where maNoiBo does not contain DEFAULT_MA_NOI_BO
        defaultXetNghiemShouldNotBeFound("maNoiBo.doesNotContain=" + DEFAULT_MA_NOI_BO);

        // Get all the xetNghiemList where maNoiBo does not contain UPDATED_MA_NOI_BO
        defaultXetNghiemShouldBeFound("maNoiBo.doesNotContain=" + UPDATED_MA_NOI_BO);
    }


    @Test
    @Transactional
    public void getAllXetNghiemsByDonViIsEqualToSomething() throws Exception {
        // Get already existing entity
        DonVi donVi = xetNghiem.getDonVi();
        xetNghiemRepository.saveAndFlush(xetNghiem);
        Long donViId = donVi.getId();

        // Get all the xetNghiemList where donVi equals to donViId
        defaultXetNghiemShouldBeFound("donViId.equals=" + donViId);

        // Get all the xetNghiemList where donVi equals to donViId + 1
        defaultXetNghiemShouldNotBeFound("donViId.equals=" + (donViId + 1));
    }


    @Test
    @Transactional
    public void getAllXetNghiemsByDotMaIsEqualToSomething() throws Exception {
        // Get already existing entity
        DotThayDoiMaDichVu dotMa = xetNghiem.getDotMa();
        xetNghiemRepository.saveAndFlush(xetNghiem);
        Long dotMaId = dotMa.getId();

        // Get all the xetNghiemList where dotMa equals to dotMaId
        defaultXetNghiemShouldBeFound("dotMaId.equals=" + dotMaId);

        // Get all the xetNghiemList where dotMa equals to dotMaId + 1
        defaultXetNghiemShouldNotBeFound("dotMaId.equals=" + (dotMaId + 1));
    }


    @Test
    @Transactional
    public void getAllXetNghiemsByLoaiXNIsEqualToSomething() throws Exception {
        // Get already existing entity
        LoaiXetNghiem loaiXN = xetNghiem.getLoaiXN();
        xetNghiemRepository.saveAndFlush(xetNghiem);
        Long loaiXNId = loaiXN.getId();

        // Get all the xetNghiemList where loaiXN equals to loaiXNId
        defaultXetNghiemShouldBeFound("loaiXNId.equals=" + loaiXNId);

        // Get all the xetNghiemList where loaiXN equals to loaiXNId + 1
        defaultXetNghiemShouldNotBeFound("loaiXNId.equals=" + (loaiXNId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultXetNghiemShouldBeFound(String filter) throws Exception {
        restXetNghiemMockMvc.perform(get("/api/xet-nghiems?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(xetNghiem.getId().intValue())))
            .andExpect(jsonPath("$.[*].canDuoiNam").value(hasItem(DEFAULT_CAN_DUOI_NAM.intValue())))
            .andExpect(jsonPath("$.[*].canDuoiNu").value(hasItem(DEFAULT_CAN_DUOI_NU.intValue())))
            .andExpect(jsonPath("$.[*].canTrenNam").value(hasItem(DEFAULT_CAN_TREN_NAM.intValue())))
            .andExpect(jsonPath("$.[*].canTrenNu").value(hasItem(DEFAULT_CAN_TREN_NU.intValue())))
            .andExpect(jsonPath("$.[*].chiSoBinhThuongNam").value(hasItem(DEFAULT_CHI_SO_BINH_THUONG_NAM)))
            .andExpect(jsonPath("$.[*].chiSoMax").value(hasItem(DEFAULT_CHI_SO_MAX.intValue())))
            .andExpect(jsonPath("$.[*].chiSoMin").value(hasItem(DEFAULT_CHI_SO_MIN.intValue())))
            .andExpect(jsonPath("$.[*].congThuc").value(hasItem(DEFAULT_CONG_THUC)))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].doPhaLoang").value(hasItem(DEFAULT_DO_PHA_LOANG)))
            .andExpect(jsonPath("$.[*].donGiaBenhVien").value(hasItem(DEFAULT_DON_GIA_BENH_VIEN.intValue())))
            .andExpect(jsonPath("$.[*].donViTinh").value(hasItem(DEFAULT_DON_VI_TINH)))
            .andExpect(jsonPath("$.[*].enabled").value(hasItem(DEFAULT_ENABLED.booleanValue())))
            .andExpect(jsonPath("$.[*].gioiHanChiDinh").value(hasItem(DEFAULT_GIOI_HAN_CHI_DINH.intValue())))
            .andExpect(jsonPath("$.[*].ketQuaBatThuong").value(hasItem(DEFAULT_KET_QUA_BAT_THUONG)))
            .andExpect(jsonPath("$.[*].maDungChung").value(hasItem(DEFAULT_MA_DUNG_CHUNG)))
            .andExpect(jsonPath("$.[*].ketQuaMacDinh").value(hasItem(DEFAULT_KET_QUA_MAC_DINH)))
            .andExpect(jsonPath("$.[*].phamViChiDinh").value(hasItem(DEFAULT_PHAM_VI_CHI_DINH.booleanValue())))
            .andExpect(jsonPath("$.[*].phanTheoGioiTInh").value(hasItem(DEFAULT_PHAN_THEO_GIOI_T_INH.booleanValue())))
            .andExpect(jsonPath("$.[*].soLeLamTron").value(hasItem(DEFAULT_SO_LE_LAM_TRON)))
            .andExpect(jsonPath("$.[*].soLuongThucHien").value(hasItem(DEFAULT_SO_LUONG_THUC_HIEN.intValue())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].tenHienThi").value(hasItem(DEFAULT_TEN_HIEN_THI)))
            .andExpect(jsonPath("$.[*].maNoiBo").value(hasItem(DEFAULT_MA_NOI_BO)));

        // Check, that the count call also returns 1
        restXetNghiemMockMvc.perform(get("/api/xet-nghiems/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultXetNghiemShouldNotBeFound(String filter) throws Exception {
        restXetNghiemMockMvc.perform(get("/api/xet-nghiems?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restXetNghiemMockMvc.perform(get("/api/xet-nghiems/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingXetNghiem() throws Exception {
        // Get the xetNghiem
        restXetNghiemMockMvc.perform(get("/api/xet-nghiems/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateXetNghiem() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        int databaseSizeBeforeUpdate = xetNghiemRepository.findAll().size();

        // Update the xetNghiem
        XetNghiem updatedXetNghiem = xetNghiemRepository.findById(xetNghiem.getId()).get();
        // Disconnect from session so that the updates on updatedXetNghiem are not directly saved in db
        em.detach(updatedXetNghiem);
        updatedXetNghiem
            .canDuoiNam(UPDATED_CAN_DUOI_NAM)
            .canDuoiNu(UPDATED_CAN_DUOI_NU)
            .canTrenNam(UPDATED_CAN_TREN_NAM)
            .canTrenNu(UPDATED_CAN_TREN_NU)
            .chiSoBinhThuongNam(UPDATED_CHI_SO_BINH_THUONG_NAM)
            .chiSoMax(UPDATED_CHI_SO_MAX)
            .chiSoMin(UPDATED_CHI_SO_MIN)
            .congThuc(UPDATED_CONG_THUC)
            .deleted(UPDATED_DELETED)
            .doPhaLoang(UPDATED_DO_PHA_LOANG)
            .donGiaBenhVien(UPDATED_DON_GIA_BENH_VIEN)
            .donViTinh(UPDATED_DON_VI_TINH)
            .enabled(UPDATED_ENABLED)
            .gioiHanChiDinh(UPDATED_GIOI_HAN_CHI_DINH)
            .ketQuaBatThuong(UPDATED_KET_QUA_BAT_THUONG)
            .maDungChung(UPDATED_MA_DUNG_CHUNG)
            .ketQuaMacDinh(UPDATED_KET_QUA_MAC_DINH)
            .phamViChiDinh(UPDATED_PHAM_VI_CHI_DINH)
            .phanTheoGioiTInh(UPDATED_PHAN_THEO_GIOI_T_INH)
            .soLeLamTron(UPDATED_SO_LE_LAM_TRON)
            .soLuongThucHien(UPDATED_SO_LUONG_THUC_HIEN)
            .ten(UPDATED_TEN)
            .tenHienThi(UPDATED_TEN_HIEN_THI)
            .maNoiBo(UPDATED_MA_NOI_BO);
        XetNghiemDTO xetNghiemDTO = xetNghiemMapper.toDto(updatedXetNghiem);

        restXetNghiemMockMvc.perform(put("/api/xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(xetNghiemDTO)))
            .andExpect(status().isOk());

        // Validate the XetNghiem in the database
        List<XetNghiem> xetNghiemList = xetNghiemRepository.findAll();
        assertThat(xetNghiemList).hasSize(databaseSizeBeforeUpdate);
        XetNghiem testXetNghiem = xetNghiemList.get(xetNghiemList.size() - 1);
        assertThat(testXetNghiem.getCanDuoiNam()).isEqualTo(UPDATED_CAN_DUOI_NAM);
        assertThat(testXetNghiem.getCanDuoiNu()).isEqualTo(UPDATED_CAN_DUOI_NU);
        assertThat(testXetNghiem.getCanTrenNam()).isEqualTo(UPDATED_CAN_TREN_NAM);
        assertThat(testXetNghiem.getCanTrenNu()).isEqualTo(UPDATED_CAN_TREN_NU);
        assertThat(testXetNghiem.getChiSoBinhThuongNam()).isEqualTo(UPDATED_CHI_SO_BINH_THUONG_NAM);
        assertThat(testXetNghiem.getChiSoMax()).isEqualTo(UPDATED_CHI_SO_MAX);
        assertThat(testXetNghiem.getChiSoMin()).isEqualTo(UPDATED_CHI_SO_MIN);
        assertThat(testXetNghiem.getCongThuc()).isEqualTo(UPDATED_CONG_THUC);
        assertThat(testXetNghiem.isDeleted()).isEqualTo(UPDATED_DELETED);
        assertThat(testXetNghiem.getDoPhaLoang()).isEqualTo(UPDATED_DO_PHA_LOANG);
        assertThat(testXetNghiem.getDonGiaBenhVien()).isEqualTo(UPDATED_DON_GIA_BENH_VIEN);
        assertThat(testXetNghiem.getDonViTinh()).isEqualTo(UPDATED_DON_VI_TINH);
        assertThat(testXetNghiem.isEnabled()).isEqualTo(UPDATED_ENABLED);
        assertThat(testXetNghiem.getGioiHanChiDinh()).isEqualTo(UPDATED_GIOI_HAN_CHI_DINH);
        assertThat(testXetNghiem.getKetQuaBatThuong()).isEqualTo(UPDATED_KET_QUA_BAT_THUONG);
        assertThat(testXetNghiem.getMaDungChung()).isEqualTo(UPDATED_MA_DUNG_CHUNG);
        assertThat(testXetNghiem.getKetQuaMacDinh()).isEqualTo(UPDATED_KET_QUA_MAC_DINH);
        assertThat(testXetNghiem.isPhamViChiDinh()).isEqualTo(UPDATED_PHAM_VI_CHI_DINH);
        assertThat(testXetNghiem.isPhanTheoGioiTInh()).isEqualTo(UPDATED_PHAN_THEO_GIOI_T_INH);
        assertThat(testXetNghiem.getSoLeLamTron()).isEqualTo(UPDATED_SO_LE_LAM_TRON);
        assertThat(testXetNghiem.getSoLuongThucHien()).isEqualTo(UPDATED_SO_LUONG_THUC_HIEN);
        assertThat(testXetNghiem.getTen()).isEqualTo(UPDATED_TEN);
        assertThat(testXetNghiem.getTenHienThi()).isEqualTo(UPDATED_TEN_HIEN_THI);
        assertThat(testXetNghiem.getMaNoiBo()).isEqualTo(UPDATED_MA_NOI_BO);
    }

    @Test
    @Transactional
    public void updateNonExistingXetNghiem() throws Exception {
        int databaseSizeBeforeUpdate = xetNghiemRepository.findAll().size();

        // Create the XetNghiem
        XetNghiemDTO xetNghiemDTO = xetNghiemMapper.toDto(xetNghiem);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restXetNghiemMockMvc.perform(put("/api/xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(xetNghiemDTO)))
            .andExpect(status().isBadRequest());

        // Validate the XetNghiem in the database
        List<XetNghiem> xetNghiemList = xetNghiemRepository.findAll();
        assertThat(xetNghiemList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteXetNghiem() throws Exception {
        // Initialize the database
        xetNghiemRepository.saveAndFlush(xetNghiem);

        int databaseSizeBeforeDelete = xetNghiemRepository.findAll().size();

        // Delete the xetNghiem
        restXetNghiemMockMvc.perform(delete("/api/xet-nghiems/{id}", xetNghiem.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<XetNghiem> xetNghiemList = xetNghiemRepository.findAll();
        assertThat(xetNghiemList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

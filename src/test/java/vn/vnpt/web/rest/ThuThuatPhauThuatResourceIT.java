package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.ThuThuatPhauThuat;
import vn.vnpt.domain.DonVi;
import vn.vnpt.domain.DotThayDoiMaDichVu;
import vn.vnpt.domain.LoaiThuThuatPhauThuat;
import vn.vnpt.repository.ThuThuatPhauThuatRepository;
import vn.vnpt.service.ThuThuatPhauThuatService;
import vn.vnpt.service.dto.ThuThuatPhauThuatDTO;
import vn.vnpt.service.mapper.ThuThuatPhauThuatMapper;
import vn.vnpt.service.dto.ThuThuatPhauThuatCriteria;
import vn.vnpt.service.ThuThuatPhauThuatQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ThuThuatPhauThuatResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class ThuThuatPhauThuatResourceIT {

    private static final Boolean DEFAULT_DELETED = false;
    private static final Boolean UPDATED_DELETED = true;

    private static final Boolean DEFAULT_DICH_VU_YEU_CAU = false;
    private static final Boolean UPDATED_DICH_VU_YEU_CAU = true;

    private static final BigDecimal DEFAULT_DON_GIA_BENH_VIEN = new BigDecimal(1);
    private static final BigDecimal UPDATED_DON_GIA_BENH_VIEN = new BigDecimal(2);
    private static final BigDecimal SMALLER_DON_GIA_BENH_VIEN = new BigDecimal(1 - 1);

    private static final Boolean DEFAULT_ENABLED = false;
    private static final Boolean UPDATED_ENABLED = true;

    private static final BigDecimal DEFAULT_GOI_HAN_CHI_DINH = new BigDecimal(1);
    private static final BigDecimal UPDATED_GOI_HAN_CHI_DINH = new BigDecimal(2);
    private static final BigDecimal SMALLER_GOI_HAN_CHI_DINH = new BigDecimal(1 - 1);

    private static final Boolean DEFAULT_PHAM_VI_CHI_DINH = false;
    private static final Boolean UPDATED_PHAM_VI_CHI_DINH = true;

    private static final Integer DEFAULT_PHAN_THEO_GIOI_TINH = 1;
    private static final Integer UPDATED_PHAN_THEO_GIOI_TINH = 2;
    private static final Integer SMALLER_PHAN_THEO_GIOI_TINH = 1 - 1;

    private static final String DEFAULT_TEN = "AAAAAAAAAA";
    private static final String UPDATED_TEN = "BBBBBBBBBB";

    private static final String DEFAULT_TEN_HIEN_THI = "AAAAAAAAAA";
    private static final String UPDATED_TEN_HIEN_THI = "BBBBBBBBBB";

    private static final String DEFAULT_MA_NOI_BO = "AAAAAAAAAA";
    private static final String UPDATED_MA_NOI_BO = "BBBBBBBBBB";

    private static final String DEFAULT_MA_DUNG_CHUNG = "AAAAAAAAAA";
    private static final String UPDATED_MA_DUNG_CHUNG = "BBBBBBBBBB";

    @Autowired
    private ThuThuatPhauThuatRepository thuThuatPhauThuatRepository;

    @Autowired
    private ThuThuatPhauThuatMapper thuThuatPhauThuatMapper;

    @Autowired
    private ThuThuatPhauThuatService thuThuatPhauThuatService;

    @Autowired
    private ThuThuatPhauThuatQueryService thuThuatPhauThuatQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restThuThuatPhauThuatMockMvc;

    private ThuThuatPhauThuat thuThuatPhauThuat;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ThuThuatPhauThuat createEntity(EntityManager em) {
        ThuThuatPhauThuat thuThuatPhauThuat = new ThuThuatPhauThuat()
            .deleted(DEFAULT_DELETED)
            .dichVuYeuCau(DEFAULT_DICH_VU_YEU_CAU)
            .donGiaBenhVien(DEFAULT_DON_GIA_BENH_VIEN)
            .enabled(DEFAULT_ENABLED)
            .goiHanChiDinh(DEFAULT_GOI_HAN_CHI_DINH)
            .phamViChiDinh(DEFAULT_PHAM_VI_CHI_DINH)
            .phanTheoGioiTinh(DEFAULT_PHAN_THEO_GIOI_TINH)
            .ten(DEFAULT_TEN)
            .tenHienThi(DEFAULT_TEN_HIEN_THI)
            .maNoiBo(DEFAULT_MA_NOI_BO)
            .maDungChung(DEFAULT_MA_DUNG_CHUNG);
        // Add required entity
        DonVi donVi;
        if (TestUtil.findAll(em, DonVi.class).isEmpty()) {
            donVi = DonViResourceIT.createEntity(em);
            em.persist(donVi);
            em.flush();
        } else {
            donVi = TestUtil.findAll(em, DonVi.class).get(0);
        }
        thuThuatPhauThuat.setDonVi(donVi);
        // Add required entity
        DotThayDoiMaDichVu dotThayDoiMaDichVu;
        if (TestUtil.findAll(em, DotThayDoiMaDichVu.class).isEmpty()) {
            dotThayDoiMaDichVu = DotThayDoiMaDichVuResourceIT.createEntity(em);
            em.persist(dotThayDoiMaDichVu);
            em.flush();
        } else {
            dotThayDoiMaDichVu = TestUtil.findAll(em, DotThayDoiMaDichVu.class).get(0);
        }
        thuThuatPhauThuat.setDotMa(dotThayDoiMaDichVu);
        // Add required entity
        LoaiThuThuatPhauThuat loaiThuThuatPhauThuat;
        if (TestUtil.findAll(em, LoaiThuThuatPhauThuat.class).isEmpty()) {
            loaiThuThuatPhauThuat = LoaiThuThuatPhauThuatResourceIT.createEntity(em);
            em.persist(loaiThuThuatPhauThuat);
            em.flush();
        } else {
            loaiThuThuatPhauThuat = TestUtil.findAll(em, LoaiThuThuatPhauThuat.class).get(0);
        }
        thuThuatPhauThuat.setLoaittpt(loaiThuThuatPhauThuat);
        return thuThuatPhauThuat;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ThuThuatPhauThuat createUpdatedEntity(EntityManager em) {
        ThuThuatPhauThuat thuThuatPhauThuat = new ThuThuatPhauThuat()
            .deleted(UPDATED_DELETED)
            .dichVuYeuCau(UPDATED_DICH_VU_YEU_CAU)
            .donGiaBenhVien(UPDATED_DON_GIA_BENH_VIEN)
            .enabled(UPDATED_ENABLED)
            .goiHanChiDinh(UPDATED_GOI_HAN_CHI_DINH)
            .phamViChiDinh(UPDATED_PHAM_VI_CHI_DINH)
            .phanTheoGioiTinh(UPDATED_PHAN_THEO_GIOI_TINH)
            .ten(UPDATED_TEN)
            .tenHienThi(UPDATED_TEN_HIEN_THI)
            .maNoiBo(UPDATED_MA_NOI_BO)
            .maDungChung(UPDATED_MA_DUNG_CHUNG);
        // Add required entity
        DonVi donVi;
        if (TestUtil.findAll(em, DonVi.class).isEmpty()) {
            donVi = DonViResourceIT.createUpdatedEntity(em);
            em.persist(donVi);
            em.flush();
        } else {
            donVi = TestUtil.findAll(em, DonVi.class).get(0);
        }
        thuThuatPhauThuat.setDonVi(donVi);
        // Add required entity
        DotThayDoiMaDichVu dotThayDoiMaDichVu;
        if (TestUtil.findAll(em, DotThayDoiMaDichVu.class).isEmpty()) {
            dotThayDoiMaDichVu = DotThayDoiMaDichVuResourceIT.createUpdatedEntity(em);
            em.persist(dotThayDoiMaDichVu);
            em.flush();
        } else {
            dotThayDoiMaDichVu = TestUtil.findAll(em, DotThayDoiMaDichVu.class).get(0);
        }
        thuThuatPhauThuat.setDotMa(dotThayDoiMaDichVu);
        // Add required entity
        LoaiThuThuatPhauThuat loaiThuThuatPhauThuat;
        if (TestUtil.findAll(em, LoaiThuThuatPhauThuat.class).isEmpty()) {
            loaiThuThuatPhauThuat = LoaiThuThuatPhauThuatResourceIT.createUpdatedEntity(em);
            em.persist(loaiThuThuatPhauThuat);
            em.flush();
        } else {
            loaiThuThuatPhauThuat = TestUtil.findAll(em, LoaiThuThuatPhauThuat.class).get(0);
        }
        thuThuatPhauThuat.setLoaittpt(loaiThuThuatPhauThuat);
        return thuThuatPhauThuat;
    }

    @BeforeEach
    public void initTest() {
        thuThuatPhauThuat = createEntity(em);
    }

    @Test
    @Transactional
    public void createThuThuatPhauThuat() throws Exception {
        int databaseSizeBeforeCreate = thuThuatPhauThuatRepository.findAll().size();

        // Create the ThuThuatPhauThuat
        ThuThuatPhauThuatDTO thuThuatPhauThuatDTO = thuThuatPhauThuatMapper.toDto(thuThuatPhauThuat);
        restThuThuatPhauThuatMockMvc.perform(post("/api/thu-thuat-phau-thuats").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(thuThuatPhauThuatDTO)))
            .andExpect(status().isCreated());

        // Validate the ThuThuatPhauThuat in the database
        List<ThuThuatPhauThuat> thuThuatPhauThuatList = thuThuatPhauThuatRepository.findAll();
        assertThat(thuThuatPhauThuatList).hasSize(databaseSizeBeforeCreate + 1);
        ThuThuatPhauThuat testThuThuatPhauThuat = thuThuatPhauThuatList.get(thuThuatPhauThuatList.size() - 1);
        assertThat(testThuThuatPhauThuat.isDeleted()).isEqualTo(DEFAULT_DELETED);
        assertThat(testThuThuatPhauThuat.isDichVuYeuCau()).isEqualTo(DEFAULT_DICH_VU_YEU_CAU);
        assertThat(testThuThuatPhauThuat.getDonGiaBenhVien()).isEqualTo(DEFAULT_DON_GIA_BENH_VIEN);
        assertThat(testThuThuatPhauThuat.isEnabled()).isEqualTo(DEFAULT_ENABLED);
        assertThat(testThuThuatPhauThuat.getGoiHanChiDinh()).isEqualTo(DEFAULT_GOI_HAN_CHI_DINH);
        assertThat(testThuThuatPhauThuat.isPhamViChiDinh()).isEqualTo(DEFAULT_PHAM_VI_CHI_DINH);
        assertThat(testThuThuatPhauThuat.getPhanTheoGioiTinh()).isEqualTo(DEFAULT_PHAN_THEO_GIOI_TINH);
        assertThat(testThuThuatPhauThuat.getTen()).isEqualTo(DEFAULT_TEN);
        assertThat(testThuThuatPhauThuat.getTenHienThi()).isEqualTo(DEFAULT_TEN_HIEN_THI);
        assertThat(testThuThuatPhauThuat.getMaNoiBo()).isEqualTo(DEFAULT_MA_NOI_BO);
        assertThat(testThuThuatPhauThuat.getMaDungChung()).isEqualTo(DEFAULT_MA_DUNG_CHUNG);
    }

    @Test
    @Transactional
    public void createThuThuatPhauThuatWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = thuThuatPhauThuatRepository.findAll().size();

        // Create the ThuThuatPhauThuat with an existing ID
        thuThuatPhauThuat.setId(1L);
        ThuThuatPhauThuatDTO thuThuatPhauThuatDTO = thuThuatPhauThuatMapper.toDto(thuThuatPhauThuat);

        // An entity with an existing ID cannot be created, so this API call must fail
        restThuThuatPhauThuatMockMvc.perform(post("/api/thu-thuat-phau-thuats").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(thuThuatPhauThuatDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ThuThuatPhauThuat in the database
        List<ThuThuatPhauThuat> thuThuatPhauThuatList = thuThuatPhauThuatRepository.findAll();
        assertThat(thuThuatPhauThuatList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkDeletedIsRequired() throws Exception {
        int databaseSizeBeforeTest = thuThuatPhauThuatRepository.findAll().size();
        // set the field null
        thuThuatPhauThuat.setDeleted(null);

        // Create the ThuThuatPhauThuat, which fails.
        ThuThuatPhauThuatDTO thuThuatPhauThuatDTO = thuThuatPhauThuatMapper.toDto(thuThuatPhauThuat);

        restThuThuatPhauThuatMockMvc.perform(post("/api/thu-thuat-phau-thuats").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(thuThuatPhauThuatDTO)))
            .andExpect(status().isBadRequest());

        List<ThuThuatPhauThuat> thuThuatPhauThuatList = thuThuatPhauThuatRepository.findAll();
        assertThat(thuThuatPhauThuatList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDichVuYeuCauIsRequired() throws Exception {
        int databaseSizeBeforeTest = thuThuatPhauThuatRepository.findAll().size();
        // set the field null
        thuThuatPhauThuat.setDichVuYeuCau(null);

        // Create the ThuThuatPhauThuat, which fails.
        ThuThuatPhauThuatDTO thuThuatPhauThuatDTO = thuThuatPhauThuatMapper.toDto(thuThuatPhauThuat);

        restThuThuatPhauThuatMockMvc.perform(post("/api/thu-thuat-phau-thuats").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(thuThuatPhauThuatDTO)))
            .andExpect(status().isBadRequest());

        List<ThuThuatPhauThuat> thuThuatPhauThuatList = thuThuatPhauThuatRepository.findAll();
        assertThat(thuThuatPhauThuatList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDonGiaBenhVienIsRequired() throws Exception {
        int databaseSizeBeforeTest = thuThuatPhauThuatRepository.findAll().size();
        // set the field null
        thuThuatPhauThuat.setDonGiaBenhVien(null);

        // Create the ThuThuatPhauThuat, which fails.
        ThuThuatPhauThuatDTO thuThuatPhauThuatDTO = thuThuatPhauThuatMapper.toDto(thuThuatPhauThuat);

        restThuThuatPhauThuatMockMvc.perform(post("/api/thu-thuat-phau-thuats").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(thuThuatPhauThuatDTO)))
            .andExpect(status().isBadRequest());

        List<ThuThuatPhauThuat> thuThuatPhauThuatList = thuThuatPhauThuatRepository.findAll();
        assertThat(thuThuatPhauThuatList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEnabledIsRequired() throws Exception {
        int databaseSizeBeforeTest = thuThuatPhauThuatRepository.findAll().size();
        // set the field null
        thuThuatPhauThuat.setEnabled(null);

        // Create the ThuThuatPhauThuat, which fails.
        ThuThuatPhauThuatDTO thuThuatPhauThuatDTO = thuThuatPhauThuatMapper.toDto(thuThuatPhauThuat);

        restThuThuatPhauThuatMockMvc.perform(post("/api/thu-thuat-phau-thuats").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(thuThuatPhauThuatDTO)))
            .andExpect(status().isBadRequest());

        List<ThuThuatPhauThuat> thuThuatPhauThuatList = thuThuatPhauThuatRepository.findAll();
        assertThat(thuThuatPhauThuatList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPhamViChiDinhIsRequired() throws Exception {
        int databaseSizeBeforeTest = thuThuatPhauThuatRepository.findAll().size();
        // set the field null
        thuThuatPhauThuat.setPhamViChiDinh(null);

        // Create the ThuThuatPhauThuat, which fails.
        ThuThuatPhauThuatDTO thuThuatPhauThuatDTO = thuThuatPhauThuatMapper.toDto(thuThuatPhauThuat);

        restThuThuatPhauThuatMockMvc.perform(post("/api/thu-thuat-phau-thuats").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(thuThuatPhauThuatDTO)))
            .andExpect(status().isBadRequest());

        List<ThuThuatPhauThuat> thuThuatPhauThuatList = thuThuatPhauThuatRepository.findAll();
        assertThat(thuThuatPhauThuatList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPhanTheoGioiTinhIsRequired() throws Exception {
        int databaseSizeBeforeTest = thuThuatPhauThuatRepository.findAll().size();
        // set the field null
        thuThuatPhauThuat.setPhanTheoGioiTinh(null);

        // Create the ThuThuatPhauThuat, which fails.
        ThuThuatPhauThuatDTO thuThuatPhauThuatDTO = thuThuatPhauThuatMapper.toDto(thuThuatPhauThuat);

        restThuThuatPhauThuatMockMvc.perform(post("/api/thu-thuat-phau-thuats").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(thuThuatPhauThuatDTO)))
            .andExpect(status().isBadRequest());

        List<ThuThuatPhauThuat> thuThuatPhauThuatList = thuThuatPhauThuatRepository.findAll();
        assertThat(thuThuatPhauThuatList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuats() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList
        restThuThuatPhauThuatMockMvc.perform(get("/api/thu-thuat-phau-thuats?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(thuThuatPhauThuat.getId().intValue())))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].dichVuYeuCau").value(hasItem(DEFAULT_DICH_VU_YEU_CAU.booleanValue())))
            .andExpect(jsonPath("$.[*].donGiaBenhVien").value(hasItem(DEFAULT_DON_GIA_BENH_VIEN.intValue())))
            .andExpect(jsonPath("$.[*].enabled").value(hasItem(DEFAULT_ENABLED.booleanValue())))
            .andExpect(jsonPath("$.[*].goiHanChiDinh").value(hasItem(DEFAULT_GOI_HAN_CHI_DINH.intValue())))
            .andExpect(jsonPath("$.[*].phamViChiDinh").value(hasItem(DEFAULT_PHAM_VI_CHI_DINH.booleanValue())))
            .andExpect(jsonPath("$.[*].phanTheoGioiTinh").value(hasItem(DEFAULT_PHAN_THEO_GIOI_TINH)))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].tenHienThi").value(hasItem(DEFAULT_TEN_HIEN_THI)))
            .andExpect(jsonPath("$.[*].maNoiBo").value(hasItem(DEFAULT_MA_NOI_BO)))
            .andExpect(jsonPath("$.[*].maDungChung").value(hasItem(DEFAULT_MA_DUNG_CHUNG)));
    }
    
    @Test
    @Transactional
    public void getThuThuatPhauThuat() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get the thuThuatPhauThuat
        restThuThuatPhauThuatMockMvc.perform(get("/api/thu-thuat-phau-thuats/{id}", thuThuatPhauThuat.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(thuThuatPhauThuat.getId().intValue()))
            .andExpect(jsonPath("$.deleted").value(DEFAULT_DELETED.booleanValue()))
            .andExpect(jsonPath("$.dichVuYeuCau").value(DEFAULT_DICH_VU_YEU_CAU.booleanValue()))
            .andExpect(jsonPath("$.donGiaBenhVien").value(DEFAULT_DON_GIA_BENH_VIEN.intValue()))
            .andExpect(jsonPath("$.enabled").value(DEFAULT_ENABLED.booleanValue()))
            .andExpect(jsonPath("$.goiHanChiDinh").value(DEFAULT_GOI_HAN_CHI_DINH.intValue()))
            .andExpect(jsonPath("$.phamViChiDinh").value(DEFAULT_PHAM_VI_CHI_DINH.booleanValue()))
            .andExpect(jsonPath("$.phanTheoGioiTinh").value(DEFAULT_PHAN_THEO_GIOI_TINH))
            .andExpect(jsonPath("$.ten").value(DEFAULT_TEN))
            .andExpect(jsonPath("$.tenHienThi").value(DEFAULT_TEN_HIEN_THI))
            .andExpect(jsonPath("$.maNoiBo").value(DEFAULT_MA_NOI_BO))
            .andExpect(jsonPath("$.maDungChung").value(DEFAULT_MA_DUNG_CHUNG));
    }


    @Test
    @Transactional
    public void getThuThuatPhauThuatsByIdFiltering() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        Long id = thuThuatPhauThuat.getId();

        defaultThuThuatPhauThuatShouldBeFound("id.equals=" + id);
        defaultThuThuatPhauThuatShouldNotBeFound("id.notEquals=" + id);

        defaultThuThuatPhauThuatShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultThuThuatPhauThuatShouldNotBeFound("id.greaterThan=" + id);

        defaultThuThuatPhauThuatShouldBeFound("id.lessThanOrEqual=" + id);
        defaultThuThuatPhauThuatShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByDeletedIsEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where deleted equals to DEFAULT_DELETED
        defaultThuThuatPhauThuatShouldBeFound("deleted.equals=" + DEFAULT_DELETED);

        // Get all the thuThuatPhauThuatList where deleted equals to UPDATED_DELETED
        defaultThuThuatPhauThuatShouldNotBeFound("deleted.equals=" + UPDATED_DELETED);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByDeletedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where deleted not equals to DEFAULT_DELETED
        defaultThuThuatPhauThuatShouldNotBeFound("deleted.notEquals=" + DEFAULT_DELETED);

        // Get all the thuThuatPhauThuatList where deleted not equals to UPDATED_DELETED
        defaultThuThuatPhauThuatShouldBeFound("deleted.notEquals=" + UPDATED_DELETED);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByDeletedIsInShouldWork() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where deleted in DEFAULT_DELETED or UPDATED_DELETED
        defaultThuThuatPhauThuatShouldBeFound("deleted.in=" + DEFAULT_DELETED + "," + UPDATED_DELETED);

        // Get all the thuThuatPhauThuatList where deleted equals to UPDATED_DELETED
        defaultThuThuatPhauThuatShouldNotBeFound("deleted.in=" + UPDATED_DELETED);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByDeletedIsNullOrNotNull() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where deleted is not null
        defaultThuThuatPhauThuatShouldBeFound("deleted.specified=true");

        // Get all the thuThuatPhauThuatList where deleted is null
        defaultThuThuatPhauThuatShouldNotBeFound("deleted.specified=false");
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByDichVuYeuCauIsEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where dichVuYeuCau equals to DEFAULT_DICH_VU_YEU_CAU
        defaultThuThuatPhauThuatShouldBeFound("dichVuYeuCau.equals=" + DEFAULT_DICH_VU_YEU_CAU);

        // Get all the thuThuatPhauThuatList where dichVuYeuCau equals to UPDATED_DICH_VU_YEU_CAU
        defaultThuThuatPhauThuatShouldNotBeFound("dichVuYeuCau.equals=" + UPDATED_DICH_VU_YEU_CAU);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByDichVuYeuCauIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where dichVuYeuCau not equals to DEFAULT_DICH_VU_YEU_CAU
        defaultThuThuatPhauThuatShouldNotBeFound("dichVuYeuCau.notEquals=" + DEFAULT_DICH_VU_YEU_CAU);

        // Get all the thuThuatPhauThuatList where dichVuYeuCau not equals to UPDATED_DICH_VU_YEU_CAU
        defaultThuThuatPhauThuatShouldBeFound("dichVuYeuCau.notEquals=" + UPDATED_DICH_VU_YEU_CAU);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByDichVuYeuCauIsInShouldWork() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where dichVuYeuCau in DEFAULT_DICH_VU_YEU_CAU or UPDATED_DICH_VU_YEU_CAU
        defaultThuThuatPhauThuatShouldBeFound("dichVuYeuCau.in=" + DEFAULT_DICH_VU_YEU_CAU + "," + UPDATED_DICH_VU_YEU_CAU);

        // Get all the thuThuatPhauThuatList where dichVuYeuCau equals to UPDATED_DICH_VU_YEU_CAU
        defaultThuThuatPhauThuatShouldNotBeFound("dichVuYeuCau.in=" + UPDATED_DICH_VU_YEU_CAU);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByDichVuYeuCauIsNullOrNotNull() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where dichVuYeuCau is not null
        defaultThuThuatPhauThuatShouldBeFound("dichVuYeuCau.specified=true");

        // Get all the thuThuatPhauThuatList where dichVuYeuCau is null
        defaultThuThuatPhauThuatShouldNotBeFound("dichVuYeuCau.specified=false");
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByDonGiaBenhVienIsEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where donGiaBenhVien equals to DEFAULT_DON_GIA_BENH_VIEN
        defaultThuThuatPhauThuatShouldBeFound("donGiaBenhVien.equals=" + DEFAULT_DON_GIA_BENH_VIEN);

        // Get all the thuThuatPhauThuatList where donGiaBenhVien equals to UPDATED_DON_GIA_BENH_VIEN
        defaultThuThuatPhauThuatShouldNotBeFound("donGiaBenhVien.equals=" + UPDATED_DON_GIA_BENH_VIEN);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByDonGiaBenhVienIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where donGiaBenhVien not equals to DEFAULT_DON_GIA_BENH_VIEN
        defaultThuThuatPhauThuatShouldNotBeFound("donGiaBenhVien.notEquals=" + DEFAULT_DON_GIA_BENH_VIEN);

        // Get all the thuThuatPhauThuatList where donGiaBenhVien not equals to UPDATED_DON_GIA_BENH_VIEN
        defaultThuThuatPhauThuatShouldBeFound("donGiaBenhVien.notEquals=" + UPDATED_DON_GIA_BENH_VIEN);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByDonGiaBenhVienIsInShouldWork() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where donGiaBenhVien in DEFAULT_DON_GIA_BENH_VIEN or UPDATED_DON_GIA_BENH_VIEN
        defaultThuThuatPhauThuatShouldBeFound("donGiaBenhVien.in=" + DEFAULT_DON_GIA_BENH_VIEN + "," + UPDATED_DON_GIA_BENH_VIEN);

        // Get all the thuThuatPhauThuatList where donGiaBenhVien equals to UPDATED_DON_GIA_BENH_VIEN
        defaultThuThuatPhauThuatShouldNotBeFound("donGiaBenhVien.in=" + UPDATED_DON_GIA_BENH_VIEN);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByDonGiaBenhVienIsNullOrNotNull() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where donGiaBenhVien is not null
        defaultThuThuatPhauThuatShouldBeFound("donGiaBenhVien.specified=true");

        // Get all the thuThuatPhauThuatList where donGiaBenhVien is null
        defaultThuThuatPhauThuatShouldNotBeFound("donGiaBenhVien.specified=false");
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByDonGiaBenhVienIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where donGiaBenhVien is greater than or equal to DEFAULT_DON_GIA_BENH_VIEN
        defaultThuThuatPhauThuatShouldBeFound("donGiaBenhVien.greaterThanOrEqual=" + DEFAULT_DON_GIA_BENH_VIEN);

        // Get all the thuThuatPhauThuatList where donGiaBenhVien is greater than or equal to UPDATED_DON_GIA_BENH_VIEN
        defaultThuThuatPhauThuatShouldNotBeFound("donGiaBenhVien.greaterThanOrEqual=" + UPDATED_DON_GIA_BENH_VIEN);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByDonGiaBenhVienIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where donGiaBenhVien is less than or equal to DEFAULT_DON_GIA_BENH_VIEN
        defaultThuThuatPhauThuatShouldBeFound("donGiaBenhVien.lessThanOrEqual=" + DEFAULT_DON_GIA_BENH_VIEN);

        // Get all the thuThuatPhauThuatList where donGiaBenhVien is less than or equal to SMALLER_DON_GIA_BENH_VIEN
        defaultThuThuatPhauThuatShouldNotBeFound("donGiaBenhVien.lessThanOrEqual=" + SMALLER_DON_GIA_BENH_VIEN);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByDonGiaBenhVienIsLessThanSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where donGiaBenhVien is less than DEFAULT_DON_GIA_BENH_VIEN
        defaultThuThuatPhauThuatShouldNotBeFound("donGiaBenhVien.lessThan=" + DEFAULT_DON_GIA_BENH_VIEN);

        // Get all the thuThuatPhauThuatList where donGiaBenhVien is less than UPDATED_DON_GIA_BENH_VIEN
        defaultThuThuatPhauThuatShouldBeFound("donGiaBenhVien.lessThan=" + UPDATED_DON_GIA_BENH_VIEN);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByDonGiaBenhVienIsGreaterThanSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where donGiaBenhVien is greater than DEFAULT_DON_GIA_BENH_VIEN
        defaultThuThuatPhauThuatShouldNotBeFound("donGiaBenhVien.greaterThan=" + DEFAULT_DON_GIA_BENH_VIEN);

        // Get all the thuThuatPhauThuatList where donGiaBenhVien is greater than SMALLER_DON_GIA_BENH_VIEN
        defaultThuThuatPhauThuatShouldBeFound("donGiaBenhVien.greaterThan=" + SMALLER_DON_GIA_BENH_VIEN);
    }


    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByEnabledIsEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where enabled equals to DEFAULT_ENABLED
        defaultThuThuatPhauThuatShouldBeFound("enabled.equals=" + DEFAULT_ENABLED);

        // Get all the thuThuatPhauThuatList where enabled equals to UPDATED_ENABLED
        defaultThuThuatPhauThuatShouldNotBeFound("enabled.equals=" + UPDATED_ENABLED);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByEnabledIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where enabled not equals to DEFAULT_ENABLED
        defaultThuThuatPhauThuatShouldNotBeFound("enabled.notEquals=" + DEFAULT_ENABLED);

        // Get all the thuThuatPhauThuatList where enabled not equals to UPDATED_ENABLED
        defaultThuThuatPhauThuatShouldBeFound("enabled.notEquals=" + UPDATED_ENABLED);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByEnabledIsInShouldWork() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where enabled in DEFAULT_ENABLED or UPDATED_ENABLED
        defaultThuThuatPhauThuatShouldBeFound("enabled.in=" + DEFAULT_ENABLED + "," + UPDATED_ENABLED);

        // Get all the thuThuatPhauThuatList where enabled equals to UPDATED_ENABLED
        defaultThuThuatPhauThuatShouldNotBeFound("enabled.in=" + UPDATED_ENABLED);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByEnabledIsNullOrNotNull() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where enabled is not null
        defaultThuThuatPhauThuatShouldBeFound("enabled.specified=true");

        // Get all the thuThuatPhauThuatList where enabled is null
        defaultThuThuatPhauThuatShouldNotBeFound("enabled.specified=false");
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByGoiHanChiDinhIsEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where goiHanChiDinh equals to DEFAULT_GOI_HAN_CHI_DINH
        defaultThuThuatPhauThuatShouldBeFound("goiHanChiDinh.equals=" + DEFAULT_GOI_HAN_CHI_DINH);

        // Get all the thuThuatPhauThuatList where goiHanChiDinh equals to UPDATED_GOI_HAN_CHI_DINH
        defaultThuThuatPhauThuatShouldNotBeFound("goiHanChiDinh.equals=" + UPDATED_GOI_HAN_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByGoiHanChiDinhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where goiHanChiDinh not equals to DEFAULT_GOI_HAN_CHI_DINH
        defaultThuThuatPhauThuatShouldNotBeFound("goiHanChiDinh.notEquals=" + DEFAULT_GOI_HAN_CHI_DINH);

        // Get all the thuThuatPhauThuatList where goiHanChiDinh not equals to UPDATED_GOI_HAN_CHI_DINH
        defaultThuThuatPhauThuatShouldBeFound("goiHanChiDinh.notEquals=" + UPDATED_GOI_HAN_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByGoiHanChiDinhIsInShouldWork() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where goiHanChiDinh in DEFAULT_GOI_HAN_CHI_DINH or UPDATED_GOI_HAN_CHI_DINH
        defaultThuThuatPhauThuatShouldBeFound("goiHanChiDinh.in=" + DEFAULT_GOI_HAN_CHI_DINH + "," + UPDATED_GOI_HAN_CHI_DINH);

        // Get all the thuThuatPhauThuatList where goiHanChiDinh equals to UPDATED_GOI_HAN_CHI_DINH
        defaultThuThuatPhauThuatShouldNotBeFound("goiHanChiDinh.in=" + UPDATED_GOI_HAN_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByGoiHanChiDinhIsNullOrNotNull() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where goiHanChiDinh is not null
        defaultThuThuatPhauThuatShouldBeFound("goiHanChiDinh.specified=true");

        // Get all the thuThuatPhauThuatList where goiHanChiDinh is null
        defaultThuThuatPhauThuatShouldNotBeFound("goiHanChiDinh.specified=false");
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByGoiHanChiDinhIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where goiHanChiDinh is greater than or equal to DEFAULT_GOI_HAN_CHI_DINH
        defaultThuThuatPhauThuatShouldBeFound("goiHanChiDinh.greaterThanOrEqual=" + DEFAULT_GOI_HAN_CHI_DINH);

        // Get all the thuThuatPhauThuatList where goiHanChiDinh is greater than or equal to UPDATED_GOI_HAN_CHI_DINH
        defaultThuThuatPhauThuatShouldNotBeFound("goiHanChiDinh.greaterThanOrEqual=" + UPDATED_GOI_HAN_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByGoiHanChiDinhIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where goiHanChiDinh is less than or equal to DEFAULT_GOI_HAN_CHI_DINH
        defaultThuThuatPhauThuatShouldBeFound("goiHanChiDinh.lessThanOrEqual=" + DEFAULT_GOI_HAN_CHI_DINH);

        // Get all the thuThuatPhauThuatList where goiHanChiDinh is less than or equal to SMALLER_GOI_HAN_CHI_DINH
        defaultThuThuatPhauThuatShouldNotBeFound("goiHanChiDinh.lessThanOrEqual=" + SMALLER_GOI_HAN_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByGoiHanChiDinhIsLessThanSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where goiHanChiDinh is less than DEFAULT_GOI_HAN_CHI_DINH
        defaultThuThuatPhauThuatShouldNotBeFound("goiHanChiDinh.lessThan=" + DEFAULT_GOI_HAN_CHI_DINH);

        // Get all the thuThuatPhauThuatList where goiHanChiDinh is less than UPDATED_GOI_HAN_CHI_DINH
        defaultThuThuatPhauThuatShouldBeFound("goiHanChiDinh.lessThan=" + UPDATED_GOI_HAN_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByGoiHanChiDinhIsGreaterThanSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where goiHanChiDinh is greater than DEFAULT_GOI_HAN_CHI_DINH
        defaultThuThuatPhauThuatShouldNotBeFound("goiHanChiDinh.greaterThan=" + DEFAULT_GOI_HAN_CHI_DINH);

        // Get all the thuThuatPhauThuatList where goiHanChiDinh is greater than SMALLER_GOI_HAN_CHI_DINH
        defaultThuThuatPhauThuatShouldBeFound("goiHanChiDinh.greaterThan=" + SMALLER_GOI_HAN_CHI_DINH);
    }


    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByPhamViChiDinhIsEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where phamViChiDinh equals to DEFAULT_PHAM_VI_CHI_DINH
        defaultThuThuatPhauThuatShouldBeFound("phamViChiDinh.equals=" + DEFAULT_PHAM_VI_CHI_DINH);

        // Get all the thuThuatPhauThuatList where phamViChiDinh equals to UPDATED_PHAM_VI_CHI_DINH
        defaultThuThuatPhauThuatShouldNotBeFound("phamViChiDinh.equals=" + UPDATED_PHAM_VI_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByPhamViChiDinhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where phamViChiDinh not equals to DEFAULT_PHAM_VI_CHI_DINH
        defaultThuThuatPhauThuatShouldNotBeFound("phamViChiDinh.notEquals=" + DEFAULT_PHAM_VI_CHI_DINH);

        // Get all the thuThuatPhauThuatList where phamViChiDinh not equals to UPDATED_PHAM_VI_CHI_DINH
        defaultThuThuatPhauThuatShouldBeFound("phamViChiDinh.notEquals=" + UPDATED_PHAM_VI_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByPhamViChiDinhIsInShouldWork() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where phamViChiDinh in DEFAULT_PHAM_VI_CHI_DINH or UPDATED_PHAM_VI_CHI_DINH
        defaultThuThuatPhauThuatShouldBeFound("phamViChiDinh.in=" + DEFAULT_PHAM_VI_CHI_DINH + "," + UPDATED_PHAM_VI_CHI_DINH);

        // Get all the thuThuatPhauThuatList where phamViChiDinh equals to UPDATED_PHAM_VI_CHI_DINH
        defaultThuThuatPhauThuatShouldNotBeFound("phamViChiDinh.in=" + UPDATED_PHAM_VI_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByPhamViChiDinhIsNullOrNotNull() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where phamViChiDinh is not null
        defaultThuThuatPhauThuatShouldBeFound("phamViChiDinh.specified=true");

        // Get all the thuThuatPhauThuatList where phamViChiDinh is null
        defaultThuThuatPhauThuatShouldNotBeFound("phamViChiDinh.specified=false");
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByPhanTheoGioiTinhIsEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where phanTheoGioiTinh equals to DEFAULT_PHAN_THEO_GIOI_TINH
        defaultThuThuatPhauThuatShouldBeFound("phanTheoGioiTinh.equals=" + DEFAULT_PHAN_THEO_GIOI_TINH);

        // Get all the thuThuatPhauThuatList where phanTheoGioiTinh equals to UPDATED_PHAN_THEO_GIOI_TINH
        defaultThuThuatPhauThuatShouldNotBeFound("phanTheoGioiTinh.equals=" + UPDATED_PHAN_THEO_GIOI_TINH);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByPhanTheoGioiTinhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where phanTheoGioiTinh not equals to DEFAULT_PHAN_THEO_GIOI_TINH
        defaultThuThuatPhauThuatShouldNotBeFound("phanTheoGioiTinh.notEquals=" + DEFAULT_PHAN_THEO_GIOI_TINH);

        // Get all the thuThuatPhauThuatList where phanTheoGioiTinh not equals to UPDATED_PHAN_THEO_GIOI_TINH
        defaultThuThuatPhauThuatShouldBeFound("phanTheoGioiTinh.notEquals=" + UPDATED_PHAN_THEO_GIOI_TINH);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByPhanTheoGioiTinhIsInShouldWork() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where phanTheoGioiTinh in DEFAULT_PHAN_THEO_GIOI_TINH or UPDATED_PHAN_THEO_GIOI_TINH
        defaultThuThuatPhauThuatShouldBeFound("phanTheoGioiTinh.in=" + DEFAULT_PHAN_THEO_GIOI_TINH + "," + UPDATED_PHAN_THEO_GIOI_TINH);

        // Get all the thuThuatPhauThuatList where phanTheoGioiTinh equals to UPDATED_PHAN_THEO_GIOI_TINH
        defaultThuThuatPhauThuatShouldNotBeFound("phanTheoGioiTinh.in=" + UPDATED_PHAN_THEO_GIOI_TINH);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByPhanTheoGioiTinhIsNullOrNotNull() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where phanTheoGioiTinh is not null
        defaultThuThuatPhauThuatShouldBeFound("phanTheoGioiTinh.specified=true");

        // Get all the thuThuatPhauThuatList where phanTheoGioiTinh is null
        defaultThuThuatPhauThuatShouldNotBeFound("phanTheoGioiTinh.specified=false");
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByPhanTheoGioiTinhIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where phanTheoGioiTinh is greater than or equal to DEFAULT_PHAN_THEO_GIOI_TINH
        defaultThuThuatPhauThuatShouldBeFound("phanTheoGioiTinh.greaterThanOrEqual=" + DEFAULT_PHAN_THEO_GIOI_TINH);

        // Get all the thuThuatPhauThuatList where phanTheoGioiTinh is greater than or equal to UPDATED_PHAN_THEO_GIOI_TINH
        defaultThuThuatPhauThuatShouldNotBeFound("phanTheoGioiTinh.greaterThanOrEqual=" + UPDATED_PHAN_THEO_GIOI_TINH);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByPhanTheoGioiTinhIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where phanTheoGioiTinh is less than or equal to DEFAULT_PHAN_THEO_GIOI_TINH
        defaultThuThuatPhauThuatShouldBeFound("phanTheoGioiTinh.lessThanOrEqual=" + DEFAULT_PHAN_THEO_GIOI_TINH);

        // Get all the thuThuatPhauThuatList where phanTheoGioiTinh is less than or equal to SMALLER_PHAN_THEO_GIOI_TINH
        defaultThuThuatPhauThuatShouldNotBeFound("phanTheoGioiTinh.lessThanOrEqual=" + SMALLER_PHAN_THEO_GIOI_TINH);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByPhanTheoGioiTinhIsLessThanSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where phanTheoGioiTinh is less than DEFAULT_PHAN_THEO_GIOI_TINH
        defaultThuThuatPhauThuatShouldNotBeFound("phanTheoGioiTinh.lessThan=" + DEFAULT_PHAN_THEO_GIOI_TINH);

        // Get all the thuThuatPhauThuatList where phanTheoGioiTinh is less than UPDATED_PHAN_THEO_GIOI_TINH
        defaultThuThuatPhauThuatShouldBeFound("phanTheoGioiTinh.lessThan=" + UPDATED_PHAN_THEO_GIOI_TINH);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByPhanTheoGioiTinhIsGreaterThanSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where phanTheoGioiTinh is greater than DEFAULT_PHAN_THEO_GIOI_TINH
        defaultThuThuatPhauThuatShouldNotBeFound("phanTheoGioiTinh.greaterThan=" + DEFAULT_PHAN_THEO_GIOI_TINH);

        // Get all the thuThuatPhauThuatList where phanTheoGioiTinh is greater than SMALLER_PHAN_THEO_GIOI_TINH
        defaultThuThuatPhauThuatShouldBeFound("phanTheoGioiTinh.greaterThan=" + SMALLER_PHAN_THEO_GIOI_TINH);
    }


    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByTenIsEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where ten equals to DEFAULT_TEN
        defaultThuThuatPhauThuatShouldBeFound("ten.equals=" + DEFAULT_TEN);

        // Get all the thuThuatPhauThuatList where ten equals to UPDATED_TEN
        defaultThuThuatPhauThuatShouldNotBeFound("ten.equals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByTenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where ten not equals to DEFAULT_TEN
        defaultThuThuatPhauThuatShouldNotBeFound("ten.notEquals=" + DEFAULT_TEN);

        // Get all the thuThuatPhauThuatList where ten not equals to UPDATED_TEN
        defaultThuThuatPhauThuatShouldBeFound("ten.notEquals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByTenIsInShouldWork() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where ten in DEFAULT_TEN or UPDATED_TEN
        defaultThuThuatPhauThuatShouldBeFound("ten.in=" + DEFAULT_TEN + "," + UPDATED_TEN);

        // Get all the thuThuatPhauThuatList where ten equals to UPDATED_TEN
        defaultThuThuatPhauThuatShouldNotBeFound("ten.in=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByTenIsNullOrNotNull() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where ten is not null
        defaultThuThuatPhauThuatShouldBeFound("ten.specified=true");

        // Get all the thuThuatPhauThuatList where ten is null
        defaultThuThuatPhauThuatShouldNotBeFound("ten.specified=false");
    }
                @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByTenContainsSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where ten contains DEFAULT_TEN
        defaultThuThuatPhauThuatShouldBeFound("ten.contains=" + DEFAULT_TEN);

        // Get all the thuThuatPhauThuatList where ten contains UPDATED_TEN
        defaultThuThuatPhauThuatShouldNotBeFound("ten.contains=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByTenNotContainsSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where ten does not contain DEFAULT_TEN
        defaultThuThuatPhauThuatShouldNotBeFound("ten.doesNotContain=" + DEFAULT_TEN);

        // Get all the thuThuatPhauThuatList where ten does not contain UPDATED_TEN
        defaultThuThuatPhauThuatShouldBeFound("ten.doesNotContain=" + UPDATED_TEN);
    }


    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByTenHienThiIsEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where tenHienThi equals to DEFAULT_TEN_HIEN_THI
        defaultThuThuatPhauThuatShouldBeFound("tenHienThi.equals=" + DEFAULT_TEN_HIEN_THI);

        // Get all the thuThuatPhauThuatList where tenHienThi equals to UPDATED_TEN_HIEN_THI
        defaultThuThuatPhauThuatShouldNotBeFound("tenHienThi.equals=" + UPDATED_TEN_HIEN_THI);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByTenHienThiIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where tenHienThi not equals to DEFAULT_TEN_HIEN_THI
        defaultThuThuatPhauThuatShouldNotBeFound("tenHienThi.notEquals=" + DEFAULT_TEN_HIEN_THI);

        // Get all the thuThuatPhauThuatList where tenHienThi not equals to UPDATED_TEN_HIEN_THI
        defaultThuThuatPhauThuatShouldBeFound("tenHienThi.notEquals=" + UPDATED_TEN_HIEN_THI);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByTenHienThiIsInShouldWork() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where tenHienThi in DEFAULT_TEN_HIEN_THI or UPDATED_TEN_HIEN_THI
        defaultThuThuatPhauThuatShouldBeFound("tenHienThi.in=" + DEFAULT_TEN_HIEN_THI + "," + UPDATED_TEN_HIEN_THI);

        // Get all the thuThuatPhauThuatList where tenHienThi equals to UPDATED_TEN_HIEN_THI
        defaultThuThuatPhauThuatShouldNotBeFound("tenHienThi.in=" + UPDATED_TEN_HIEN_THI);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByTenHienThiIsNullOrNotNull() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where tenHienThi is not null
        defaultThuThuatPhauThuatShouldBeFound("tenHienThi.specified=true");

        // Get all the thuThuatPhauThuatList where tenHienThi is null
        defaultThuThuatPhauThuatShouldNotBeFound("tenHienThi.specified=false");
    }
                @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByTenHienThiContainsSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where tenHienThi contains DEFAULT_TEN_HIEN_THI
        defaultThuThuatPhauThuatShouldBeFound("tenHienThi.contains=" + DEFAULT_TEN_HIEN_THI);

        // Get all the thuThuatPhauThuatList where tenHienThi contains UPDATED_TEN_HIEN_THI
        defaultThuThuatPhauThuatShouldNotBeFound("tenHienThi.contains=" + UPDATED_TEN_HIEN_THI);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByTenHienThiNotContainsSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where tenHienThi does not contain DEFAULT_TEN_HIEN_THI
        defaultThuThuatPhauThuatShouldNotBeFound("tenHienThi.doesNotContain=" + DEFAULT_TEN_HIEN_THI);

        // Get all the thuThuatPhauThuatList where tenHienThi does not contain UPDATED_TEN_HIEN_THI
        defaultThuThuatPhauThuatShouldBeFound("tenHienThi.doesNotContain=" + UPDATED_TEN_HIEN_THI);
    }


    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByMaNoiBoIsEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where maNoiBo equals to DEFAULT_MA_NOI_BO
        defaultThuThuatPhauThuatShouldBeFound("maNoiBo.equals=" + DEFAULT_MA_NOI_BO);

        // Get all the thuThuatPhauThuatList where maNoiBo equals to UPDATED_MA_NOI_BO
        defaultThuThuatPhauThuatShouldNotBeFound("maNoiBo.equals=" + UPDATED_MA_NOI_BO);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByMaNoiBoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where maNoiBo not equals to DEFAULT_MA_NOI_BO
        defaultThuThuatPhauThuatShouldNotBeFound("maNoiBo.notEquals=" + DEFAULT_MA_NOI_BO);

        // Get all the thuThuatPhauThuatList where maNoiBo not equals to UPDATED_MA_NOI_BO
        defaultThuThuatPhauThuatShouldBeFound("maNoiBo.notEquals=" + UPDATED_MA_NOI_BO);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByMaNoiBoIsInShouldWork() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where maNoiBo in DEFAULT_MA_NOI_BO or UPDATED_MA_NOI_BO
        defaultThuThuatPhauThuatShouldBeFound("maNoiBo.in=" + DEFAULT_MA_NOI_BO + "," + UPDATED_MA_NOI_BO);

        // Get all the thuThuatPhauThuatList where maNoiBo equals to UPDATED_MA_NOI_BO
        defaultThuThuatPhauThuatShouldNotBeFound("maNoiBo.in=" + UPDATED_MA_NOI_BO);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByMaNoiBoIsNullOrNotNull() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where maNoiBo is not null
        defaultThuThuatPhauThuatShouldBeFound("maNoiBo.specified=true");

        // Get all the thuThuatPhauThuatList where maNoiBo is null
        defaultThuThuatPhauThuatShouldNotBeFound("maNoiBo.specified=false");
    }
                @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByMaNoiBoContainsSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where maNoiBo contains DEFAULT_MA_NOI_BO
        defaultThuThuatPhauThuatShouldBeFound("maNoiBo.contains=" + DEFAULT_MA_NOI_BO);

        // Get all the thuThuatPhauThuatList where maNoiBo contains UPDATED_MA_NOI_BO
        defaultThuThuatPhauThuatShouldNotBeFound("maNoiBo.contains=" + UPDATED_MA_NOI_BO);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByMaNoiBoNotContainsSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where maNoiBo does not contain DEFAULT_MA_NOI_BO
        defaultThuThuatPhauThuatShouldNotBeFound("maNoiBo.doesNotContain=" + DEFAULT_MA_NOI_BO);

        // Get all the thuThuatPhauThuatList where maNoiBo does not contain UPDATED_MA_NOI_BO
        defaultThuThuatPhauThuatShouldBeFound("maNoiBo.doesNotContain=" + UPDATED_MA_NOI_BO);
    }


    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByMaDungChungIsEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where maDungChung equals to DEFAULT_MA_DUNG_CHUNG
        defaultThuThuatPhauThuatShouldBeFound("maDungChung.equals=" + DEFAULT_MA_DUNG_CHUNG);

        // Get all the thuThuatPhauThuatList where maDungChung equals to UPDATED_MA_DUNG_CHUNG
        defaultThuThuatPhauThuatShouldNotBeFound("maDungChung.equals=" + UPDATED_MA_DUNG_CHUNG);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByMaDungChungIsNotEqualToSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where maDungChung not equals to DEFAULT_MA_DUNG_CHUNG
        defaultThuThuatPhauThuatShouldNotBeFound("maDungChung.notEquals=" + DEFAULT_MA_DUNG_CHUNG);

        // Get all the thuThuatPhauThuatList where maDungChung not equals to UPDATED_MA_DUNG_CHUNG
        defaultThuThuatPhauThuatShouldBeFound("maDungChung.notEquals=" + UPDATED_MA_DUNG_CHUNG);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByMaDungChungIsInShouldWork() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where maDungChung in DEFAULT_MA_DUNG_CHUNG or UPDATED_MA_DUNG_CHUNG
        defaultThuThuatPhauThuatShouldBeFound("maDungChung.in=" + DEFAULT_MA_DUNG_CHUNG + "," + UPDATED_MA_DUNG_CHUNG);

        // Get all the thuThuatPhauThuatList where maDungChung equals to UPDATED_MA_DUNG_CHUNG
        defaultThuThuatPhauThuatShouldNotBeFound("maDungChung.in=" + UPDATED_MA_DUNG_CHUNG);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByMaDungChungIsNullOrNotNull() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where maDungChung is not null
        defaultThuThuatPhauThuatShouldBeFound("maDungChung.specified=true");

        // Get all the thuThuatPhauThuatList where maDungChung is null
        defaultThuThuatPhauThuatShouldNotBeFound("maDungChung.specified=false");
    }
                @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByMaDungChungContainsSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where maDungChung contains DEFAULT_MA_DUNG_CHUNG
        defaultThuThuatPhauThuatShouldBeFound("maDungChung.contains=" + DEFAULT_MA_DUNG_CHUNG);

        // Get all the thuThuatPhauThuatList where maDungChung contains UPDATED_MA_DUNG_CHUNG
        defaultThuThuatPhauThuatShouldNotBeFound("maDungChung.contains=" + UPDATED_MA_DUNG_CHUNG);
    }

    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByMaDungChungNotContainsSomething() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        // Get all the thuThuatPhauThuatList where maDungChung does not contain DEFAULT_MA_DUNG_CHUNG
        defaultThuThuatPhauThuatShouldNotBeFound("maDungChung.doesNotContain=" + DEFAULT_MA_DUNG_CHUNG);

        // Get all the thuThuatPhauThuatList where maDungChung does not contain UPDATED_MA_DUNG_CHUNG
        defaultThuThuatPhauThuatShouldBeFound("maDungChung.doesNotContain=" + UPDATED_MA_DUNG_CHUNG);
    }


    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByDonViIsEqualToSomething() throws Exception {
        // Get already existing entity
        DonVi donVi = thuThuatPhauThuat.getDonVi();
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);
        Long donViId = donVi.getId();

        // Get all the thuThuatPhauThuatList where donVi equals to donViId
        defaultThuThuatPhauThuatShouldBeFound("donViId.equals=" + donViId);

        // Get all the thuThuatPhauThuatList where donVi equals to donViId + 1
        defaultThuThuatPhauThuatShouldNotBeFound("donViId.equals=" + (donViId + 1));
    }


    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByDotMaIsEqualToSomething() throws Exception {
        // Get already existing entity
        DotThayDoiMaDichVu dotMa = thuThuatPhauThuat.getDotMa();
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);
        Long dotMaId = dotMa.getId();

        // Get all the thuThuatPhauThuatList where dotMa equals to dotMaId
        defaultThuThuatPhauThuatShouldBeFound("dotMaId.equals=" + dotMaId);

        // Get all the thuThuatPhauThuatList where dotMa equals to dotMaId + 1
        defaultThuThuatPhauThuatShouldNotBeFound("dotMaId.equals=" + (dotMaId + 1));
    }


    @Test
    @Transactional
    public void getAllThuThuatPhauThuatsByLoaittptIsEqualToSomething() throws Exception {
        // Get already existing entity
        LoaiThuThuatPhauThuat loaittpt = thuThuatPhauThuat.getLoaittpt();
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);
        Long loaittptId = loaittpt.getId();

        // Get all the thuThuatPhauThuatList where loaittpt equals to loaittptId
        defaultThuThuatPhauThuatShouldBeFound("loaittptId.equals=" + loaittptId);

        // Get all the thuThuatPhauThuatList where loaittpt equals to loaittptId + 1
        defaultThuThuatPhauThuatShouldNotBeFound("loaittptId.equals=" + (loaittptId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultThuThuatPhauThuatShouldBeFound(String filter) throws Exception {
        restThuThuatPhauThuatMockMvc.perform(get("/api/thu-thuat-phau-thuats?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(thuThuatPhauThuat.getId().intValue())))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].dichVuYeuCau").value(hasItem(DEFAULT_DICH_VU_YEU_CAU.booleanValue())))
            .andExpect(jsonPath("$.[*].donGiaBenhVien").value(hasItem(DEFAULT_DON_GIA_BENH_VIEN.intValue())))
            .andExpect(jsonPath("$.[*].enabled").value(hasItem(DEFAULT_ENABLED.booleanValue())))
            .andExpect(jsonPath("$.[*].goiHanChiDinh").value(hasItem(DEFAULT_GOI_HAN_CHI_DINH.intValue())))
            .andExpect(jsonPath("$.[*].phamViChiDinh").value(hasItem(DEFAULT_PHAM_VI_CHI_DINH.booleanValue())))
            .andExpect(jsonPath("$.[*].phanTheoGioiTinh").value(hasItem(DEFAULT_PHAN_THEO_GIOI_TINH)))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].tenHienThi").value(hasItem(DEFAULT_TEN_HIEN_THI)))
            .andExpect(jsonPath("$.[*].maNoiBo").value(hasItem(DEFAULT_MA_NOI_BO)))
            .andExpect(jsonPath("$.[*].maDungChung").value(hasItem(DEFAULT_MA_DUNG_CHUNG)));

        // Check, that the count call also returns 1
        restThuThuatPhauThuatMockMvc.perform(get("/api/thu-thuat-phau-thuats/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultThuThuatPhauThuatShouldNotBeFound(String filter) throws Exception {
        restThuThuatPhauThuatMockMvc.perform(get("/api/thu-thuat-phau-thuats?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restThuThuatPhauThuatMockMvc.perform(get("/api/thu-thuat-phau-thuats/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingThuThuatPhauThuat() throws Exception {
        // Get the thuThuatPhauThuat
        restThuThuatPhauThuatMockMvc.perform(get("/api/thu-thuat-phau-thuats/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateThuThuatPhauThuat() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        int databaseSizeBeforeUpdate = thuThuatPhauThuatRepository.findAll().size();

        // Update the thuThuatPhauThuat
        ThuThuatPhauThuat updatedThuThuatPhauThuat = thuThuatPhauThuatRepository.findById(thuThuatPhauThuat.getId()).get();
        // Disconnect from session so that the updates on updatedThuThuatPhauThuat are not directly saved in db
        em.detach(updatedThuThuatPhauThuat);
        updatedThuThuatPhauThuat
            .deleted(UPDATED_DELETED)
            .dichVuYeuCau(UPDATED_DICH_VU_YEU_CAU)
            .donGiaBenhVien(UPDATED_DON_GIA_BENH_VIEN)
            .enabled(UPDATED_ENABLED)
            .goiHanChiDinh(UPDATED_GOI_HAN_CHI_DINH)
            .phamViChiDinh(UPDATED_PHAM_VI_CHI_DINH)
            .phanTheoGioiTinh(UPDATED_PHAN_THEO_GIOI_TINH)
            .ten(UPDATED_TEN)
            .tenHienThi(UPDATED_TEN_HIEN_THI)
            .maNoiBo(UPDATED_MA_NOI_BO)
            .maDungChung(UPDATED_MA_DUNG_CHUNG);
        ThuThuatPhauThuatDTO thuThuatPhauThuatDTO = thuThuatPhauThuatMapper.toDto(updatedThuThuatPhauThuat);

        restThuThuatPhauThuatMockMvc.perform(put("/api/thu-thuat-phau-thuats").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(thuThuatPhauThuatDTO)))
            .andExpect(status().isOk());

        // Validate the ThuThuatPhauThuat in the database
        List<ThuThuatPhauThuat> thuThuatPhauThuatList = thuThuatPhauThuatRepository.findAll();
        assertThat(thuThuatPhauThuatList).hasSize(databaseSizeBeforeUpdate);
        ThuThuatPhauThuat testThuThuatPhauThuat = thuThuatPhauThuatList.get(thuThuatPhauThuatList.size() - 1);
        assertThat(testThuThuatPhauThuat.isDeleted()).isEqualTo(UPDATED_DELETED);
        assertThat(testThuThuatPhauThuat.isDichVuYeuCau()).isEqualTo(UPDATED_DICH_VU_YEU_CAU);
        assertThat(testThuThuatPhauThuat.getDonGiaBenhVien()).isEqualTo(UPDATED_DON_GIA_BENH_VIEN);
        assertThat(testThuThuatPhauThuat.isEnabled()).isEqualTo(UPDATED_ENABLED);
        assertThat(testThuThuatPhauThuat.getGoiHanChiDinh()).isEqualTo(UPDATED_GOI_HAN_CHI_DINH);
        assertThat(testThuThuatPhauThuat.isPhamViChiDinh()).isEqualTo(UPDATED_PHAM_VI_CHI_DINH);
        assertThat(testThuThuatPhauThuat.getPhanTheoGioiTinh()).isEqualTo(UPDATED_PHAN_THEO_GIOI_TINH);
        assertThat(testThuThuatPhauThuat.getTen()).isEqualTo(UPDATED_TEN);
        assertThat(testThuThuatPhauThuat.getTenHienThi()).isEqualTo(UPDATED_TEN_HIEN_THI);
        assertThat(testThuThuatPhauThuat.getMaNoiBo()).isEqualTo(UPDATED_MA_NOI_BO);
        assertThat(testThuThuatPhauThuat.getMaDungChung()).isEqualTo(UPDATED_MA_DUNG_CHUNG);
    }

    @Test
    @Transactional
    public void updateNonExistingThuThuatPhauThuat() throws Exception {
        int databaseSizeBeforeUpdate = thuThuatPhauThuatRepository.findAll().size();

        // Create the ThuThuatPhauThuat
        ThuThuatPhauThuatDTO thuThuatPhauThuatDTO = thuThuatPhauThuatMapper.toDto(thuThuatPhauThuat);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restThuThuatPhauThuatMockMvc.perform(put("/api/thu-thuat-phau-thuats").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(thuThuatPhauThuatDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ThuThuatPhauThuat in the database
        List<ThuThuatPhauThuat> thuThuatPhauThuatList = thuThuatPhauThuatRepository.findAll();
        assertThat(thuThuatPhauThuatList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteThuThuatPhauThuat() throws Exception {
        // Initialize the database
        thuThuatPhauThuatRepository.saveAndFlush(thuThuatPhauThuat);

        int databaseSizeBeforeDelete = thuThuatPhauThuatRepository.findAll().size();

        // Delete the thuThuatPhauThuat
        restThuThuatPhauThuatMockMvc.perform(delete("/api/thu-thuat-phau-thuats/{id}", thuThuatPhauThuat.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ThuThuatPhauThuat> thuThuatPhauThuatList = thuThuatPhauThuatRepository.findAll();
        assertThat(thuThuatPhauThuatList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

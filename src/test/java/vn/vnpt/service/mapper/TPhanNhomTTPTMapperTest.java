package vn.vnpt.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class TPhanNhomTTPTMapperTest {

    private TPhanNhomTTPTMapper tPhanNhomTTPTMapper;

    @BeforeEach
    public void setUp() {
        tPhanNhomTTPTMapper = new TPhanNhomTTPTMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(tPhanNhomTTPTMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(tPhanNhomTTPTMapper.fromId(null)).isNull();
    }
}

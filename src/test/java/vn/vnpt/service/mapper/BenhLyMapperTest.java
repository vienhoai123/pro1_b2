package vn.vnpt.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class BenhLyMapperTest {

    private BenhLyMapper benhLyMapper;

    @BeforeEach
    public void setUp() {
        benhLyMapper = new BenhLyMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(benhLyMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(benhLyMapper.fromId(null)).isNull();
    }
}

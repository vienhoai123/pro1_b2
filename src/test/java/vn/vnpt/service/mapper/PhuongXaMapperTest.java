package vn.vnpt.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class PhuongXaMapperTest {

    private PhuongXaMapper phuongXaMapper;

    @BeforeEach
    public void setUp() {
        phuongXaMapper = new PhuongXaMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(phuongXaMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(phuongXaMapper.fromId(null)).isNull();
    }
}

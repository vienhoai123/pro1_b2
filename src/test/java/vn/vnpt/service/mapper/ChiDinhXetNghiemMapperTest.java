package vn.vnpt.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ChiDinhXetNghiemMapperTest {

    private ChiDinhXetNghiemMapper chiDinhXetNghiemMapper;

    @BeforeEach
    public void setUp() {
        chiDinhXetNghiemMapper = new ChiDinhXetNghiemMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(chiDinhXetNghiemMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(chiDinhXetNghiemMapper.fromId(null)).isNull();
    }
}

package vn.vnpt.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class NhomThuThuatPhauThuatMapperTest {

    private NhomThuThuatPhauThuatMapper nhomThuThuatPhauThuatMapper;

    @BeforeEach
    public void setUp() {
        nhomThuThuatPhauThuatMapper = new NhomThuThuatPhauThuatMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(nhomThuThuatPhauThuatMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(nhomThuThuatPhauThuatMapper.fromId(null)).isNull();
    }
}

package vn.vnpt.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class LoaiChanDoanHinhAnhMapperTest {

    private LoaiChanDoanHinhAnhMapper loaiChanDoanHinhAnhMapper;

    @BeforeEach
    public void setUp() {
        loaiChanDoanHinhAnhMapper = new LoaiChanDoanHinhAnhMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(loaiChanDoanHinhAnhMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(loaiChanDoanHinhAnhMapper.fromId(null)).isNull();
    }
}

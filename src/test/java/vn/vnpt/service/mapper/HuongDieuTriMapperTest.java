package vn.vnpt.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class HuongDieuTriMapperTest {

    private HuongDieuTriMapper huongDieuTriMapper;

    @BeforeEach
    public void setUp() {
        huongDieuTriMapper = new HuongDieuTriMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(huongDieuTriMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(huongDieuTriMapper.fromId(null)).isNull();
    }
}

package vn.vnpt.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class NhomXetNghiemMapperTest {

    private NhomXetNghiemMapper nhomXetNghiemMapper;

    @BeforeEach
    public void setUp() {
        nhomXetNghiemMapper = new NhomXetNghiemMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(nhomXetNghiemMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(nhomXetNghiemMapper.fromId(null)).isNull();
    }
}

package vn.vnpt.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class TPhanNhomCDHAMapperTest {

    private TPhanNhomCDHAMapper tPhanNhomCDHAMapper;

    @BeforeEach
    public void setUp() {
        tPhanNhomCDHAMapper = new TPhanNhomCDHAMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(tPhanNhomCDHAMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(tPhanNhomCDHAMapper.fromId(null)).isNull();
    }
}

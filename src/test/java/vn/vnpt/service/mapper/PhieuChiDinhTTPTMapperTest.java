package vn.vnpt.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class PhieuChiDinhTTPTMapperTest {

    private PhieuChiDinhTTPTMapper phieuChiDinhTTPTMapper;

    @BeforeEach
    public void setUp() {
        phieuChiDinhTTPTMapper = new PhieuChiDinhTTPTMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(phieuChiDinhTTPTMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(phieuChiDinhTTPTMapper.fromId(null)).isNull();
    }
}

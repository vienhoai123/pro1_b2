package vn.vnpt.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class PhieuChiDinhXetNghiemMapperTest {

    private PhieuChiDinhXetNghiemMapper phieuChiDinhXetNghiemMapper;

    @BeforeEach
    public void setUp() {
        phieuChiDinhXetNghiemMapper = new PhieuChiDinhXetNghiemMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(phieuChiDinhXetNghiemMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(phieuChiDinhXetNghiemMapper.fromId(null)).isNull();
    }
}

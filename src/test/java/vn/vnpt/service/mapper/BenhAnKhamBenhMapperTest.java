package vn.vnpt.service.mapper;

import vn.vnpt.domain.BenhAnKhamBenhId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class BenhAnKhamBenhMapperTest {

    private BenhAnKhamBenhMapper benhAnKhamBenhMapper;

    @BeforeEach
    public void setUp() {
        benhAnKhamBenhMapper = new BenhAnKhamBenhMapperImpl();
    }

    @Test
        public void testEntityFromId() {
        assertThat(benhAnKhamBenhMapper.fromId(new BenhAnKhamBenhId(2L, 2L)).getId()).isEqualTo(new BenhAnKhamBenhId(2L, 2L));
        assertThat(benhAnKhamBenhMapper.fromId(null)).isNull();
    }
}

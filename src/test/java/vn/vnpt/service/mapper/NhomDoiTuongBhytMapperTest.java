package vn.vnpt.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class NhomDoiTuongBhytMapperTest {

    private NhomDoiTuongBhytMapper nhomDoiTuongBhytMapper;

    @BeforeEach
    public void setUp() {
        nhomDoiTuongBhytMapper = new NhomDoiTuongBhytMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(nhomDoiTuongBhytMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(nhomDoiTuongBhytMapper.fromId(null)).isNull();
    }
}

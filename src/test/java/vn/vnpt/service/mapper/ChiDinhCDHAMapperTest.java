package vn.vnpt.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ChiDinhCDHAMapperTest {

    private ChiDinhCDHAMapper chiDinhCDHAMapper;

    @BeforeEach
    public void setUp() {
        chiDinhCDHAMapper = new ChiDinhCDHAMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(chiDinhCDHAMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(chiDinhCDHAMapper.fromId(null)).isNull();
    }
}

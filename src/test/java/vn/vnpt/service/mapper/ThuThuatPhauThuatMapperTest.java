package vn.vnpt.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ThuThuatPhauThuatMapperTest {

    private ThuThuatPhauThuatMapper thuThuatPhauThuatMapper;

    @BeforeEach
    public void setUp() {
        thuThuatPhauThuatMapper = new ThuThuatPhauThuatMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(thuThuatPhauThuatMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(thuThuatPhauThuatMapper.fromId(null)).isNull();
    }
}

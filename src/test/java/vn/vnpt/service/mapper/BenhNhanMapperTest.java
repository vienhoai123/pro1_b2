package vn.vnpt.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class BenhNhanMapperTest {

    private BenhNhanMapper benhNhanMapper;

    @BeforeEach
    public void setUp() {
        benhNhanMapper = new BenhNhanMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(benhNhanMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(benhNhanMapper.fromId(null)).isNull();
    }
}

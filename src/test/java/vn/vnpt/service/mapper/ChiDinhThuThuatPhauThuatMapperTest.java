package vn.vnpt.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ChiDinhThuThuatPhauThuatMapperTest {

    private ChiDinhThuThuatPhauThuatMapper chiDinhThuThuatPhauThuatMapper;

    @BeforeEach
    public void setUp() {
        chiDinhThuThuatPhauThuatMapper = new ChiDinhThuThuatPhauThuatMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(chiDinhThuThuatPhauThuatMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(chiDinhThuThuatPhauThuatMapper.fromId(null)).isNull();
    }
}

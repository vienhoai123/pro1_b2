package vn.vnpt.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ThongTinBhxhMapperTest {

    private ThongTinBhxhMapper thongTinBhxhMapper;

    @BeforeEach
    public void setUp() {
        thongTinBhxhMapper = new ThongTinBhxhMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(thongTinBhxhMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(thongTinBhxhMapper.fromId(null)).isNull();
    }
}

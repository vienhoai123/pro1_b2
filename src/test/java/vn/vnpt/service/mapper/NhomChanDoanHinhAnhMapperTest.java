package vn.vnpt.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class NhomChanDoanHinhAnhMapperTest {

    private NhomChanDoanHinhAnhMapper nhomChanDoanHinhAnhMapper;

    @BeforeEach
    public void setUp() {
        nhomChanDoanHinhAnhMapper = new NhomChanDoanHinhAnhMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(nhomChanDoanHinhAnhMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(nhomChanDoanHinhAnhMapper.fromId(null)).isNull();
    }
}

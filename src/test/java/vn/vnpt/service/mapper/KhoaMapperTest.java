package vn.vnpt.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class KhoaMapperTest {

    private KhoaMapper khoaMapper;

    @BeforeEach
    public void setUp() {
        khoaMapper = new KhoaMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(khoaMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(khoaMapper.fromId(null)).isNull();
    }
}

package vn.vnpt.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class TBenhLyKhamBenhMapperTest {

    private TBenhLyKhamBenhMapper tBenhLyKhamBenhMapper;

    @BeforeEach
    public void setUp() {
        tBenhLyKhamBenhMapper = new TBenhLyKhamBenhMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(tBenhLyKhamBenhMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(tBenhLyKhamBenhMapper.fromId(null)).isNull();
    }
}

package vn.vnpt.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class DichVuKhamMapperTest {

    private DichVuKhamMapper dichVuKhamMapper;

    @BeforeEach
    public void setUp() {
        dichVuKhamMapper = new DichVuKhamMapperImpl();
    }

    @Test
        public void testEntityFromId() {
        assertThat(dichVuKhamMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(dichVuKhamMapper.fromId(null)).isNull();
    }
}

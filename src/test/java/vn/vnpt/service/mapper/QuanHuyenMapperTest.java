package vn.vnpt.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class QuanHuyenMapperTest {

    private QuanHuyenMapper quanHuyenMapper;

    @BeforeEach
    public void setUp() {
        quanHuyenMapper = new QuanHuyenMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(quanHuyenMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(quanHuyenMapper.fromId(null)).isNull();
    }
}

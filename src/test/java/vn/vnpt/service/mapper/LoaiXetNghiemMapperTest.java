package vn.vnpt.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class LoaiXetNghiemMapperTest {

    private LoaiXetNghiemMapper loaiXetNghiemMapper;

    @BeforeEach
    public void setUp() {
        loaiXetNghiemMapper = new LoaiXetNghiemMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(loaiXetNghiemMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(loaiXetNghiemMapper.fromId(null)).isNull();
    }
}

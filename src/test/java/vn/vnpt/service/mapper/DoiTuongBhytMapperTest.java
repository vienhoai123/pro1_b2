package vn.vnpt.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class DoiTuongBhytMapperTest {

    private DoiTuongBhytMapper doiTuongBhytMapper;

    @BeforeEach
    public void setUp() {
        doiTuongBhytMapper = new DoiTuongBhytMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(doiTuongBhytMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(doiTuongBhytMapper.fromId(null)).isNull();
    }
}

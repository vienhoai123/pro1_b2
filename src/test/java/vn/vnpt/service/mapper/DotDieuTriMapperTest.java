package vn.vnpt.service.mapper;

import vn.vnpt.domain.DotDieuTriId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class DotDieuTriMapperTest {

    private DotDieuTriMapper dotDieuTriMapper;

    @BeforeEach
    public void setUp() {
        dotDieuTriMapper = new DotDieuTriMapperImpl();
    }

    @Test
        public void testEntityFromId() {
        assertThat(dotDieuTriMapper.fromId(new DotDieuTriId(2L, 2L)).getId()).isEqualTo(new DotDieuTriId(2L, 2L));
        assertThat(dotDieuTriMapper.fromId(null)).isNull();
    }
}

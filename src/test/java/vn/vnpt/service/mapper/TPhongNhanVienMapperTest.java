package vn.vnpt.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class TPhongNhanVienMapperTest {

    private TPhongNhanVienMapper tPhongNhanVienMapper;

    @BeforeEach
    public void setUp() {
        tPhongNhanVienMapper = new TPhongNhanVienMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(tPhongNhanVienMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(tPhongNhanVienMapper.fromId(null)).isNull();
    }
}

package vn.vnpt.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class DotGiaDichVuBhxhMapperTest {

    private DotGiaDichVuBhxhMapper dotGiaDichVuBhxhMapper;

    @BeforeEach
    public void setUp() {
        dotGiaDichVuBhxhMapper = new DotGiaDichVuBhxhMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(dotGiaDichVuBhxhMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(dotGiaDichVuBhxhMapper.fromId(null)).isNull();
    }
}

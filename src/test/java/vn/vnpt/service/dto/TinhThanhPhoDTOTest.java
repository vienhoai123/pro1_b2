package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class TinhThanhPhoDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TinhThanhPhoDTO.class);
        TinhThanhPhoDTO tinhThanhPhoDTO1 = new TinhThanhPhoDTO();
        tinhThanhPhoDTO1.setId(1L);
        TinhThanhPhoDTO tinhThanhPhoDTO2 = new TinhThanhPhoDTO();
        assertThat(tinhThanhPhoDTO1).isNotEqualTo(tinhThanhPhoDTO2);
        tinhThanhPhoDTO2.setId(tinhThanhPhoDTO1.getId());
        assertThat(tinhThanhPhoDTO1).isEqualTo(tinhThanhPhoDTO2);
        tinhThanhPhoDTO2.setId(2L);
        assertThat(tinhThanhPhoDTO1).isNotEqualTo(tinhThanhPhoDTO2);
        tinhThanhPhoDTO1.setId(null);
        assertThat(tinhThanhPhoDTO1).isNotEqualTo(tinhThanhPhoDTO2);
    }
}

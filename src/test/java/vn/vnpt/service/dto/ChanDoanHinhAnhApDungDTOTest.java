package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class ChanDoanHinhAnhApDungDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ChanDoanHinhAnhApDungDTO.class);
        ChanDoanHinhAnhApDungDTO chanDoanHinhAnhApDungDTO1 = new ChanDoanHinhAnhApDungDTO();
        chanDoanHinhAnhApDungDTO1.setId(1L);
        ChanDoanHinhAnhApDungDTO chanDoanHinhAnhApDungDTO2 = new ChanDoanHinhAnhApDungDTO();
        assertThat(chanDoanHinhAnhApDungDTO1).isNotEqualTo(chanDoanHinhAnhApDungDTO2);
        chanDoanHinhAnhApDungDTO2.setId(chanDoanHinhAnhApDungDTO1.getId());
        assertThat(chanDoanHinhAnhApDungDTO1).isEqualTo(chanDoanHinhAnhApDungDTO2);
        chanDoanHinhAnhApDungDTO2.setId(2L);
        assertThat(chanDoanHinhAnhApDungDTO1).isNotEqualTo(chanDoanHinhAnhApDungDTO2);
        chanDoanHinhAnhApDungDTO1.setId(null);
        assertThat(chanDoanHinhAnhApDungDTO1).isNotEqualTo(chanDoanHinhAnhApDungDTO2);
    }
}

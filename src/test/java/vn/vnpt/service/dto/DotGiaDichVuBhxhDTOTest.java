package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class DotGiaDichVuBhxhDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DotGiaDichVuBhxhDTO.class);
        DotGiaDichVuBhxhDTO dotGiaDichVuBhxhDTO1 = new DotGiaDichVuBhxhDTO();
        dotGiaDichVuBhxhDTO1.setId(1L);
        DotGiaDichVuBhxhDTO dotGiaDichVuBhxhDTO2 = new DotGiaDichVuBhxhDTO();
        assertThat(dotGiaDichVuBhxhDTO1).isNotEqualTo(dotGiaDichVuBhxhDTO2);
        dotGiaDichVuBhxhDTO2.setId(dotGiaDichVuBhxhDTO1.getId());
        assertThat(dotGiaDichVuBhxhDTO1).isEqualTo(dotGiaDichVuBhxhDTO2);
        dotGiaDichVuBhxhDTO2.setId(2L);
        assertThat(dotGiaDichVuBhxhDTO1).isNotEqualTo(dotGiaDichVuBhxhDTO2);
        dotGiaDichVuBhxhDTO1.setId(null);
        assertThat(dotGiaDichVuBhxhDTO1).isNotEqualTo(dotGiaDichVuBhxhDTO2);
    }
}

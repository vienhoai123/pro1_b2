package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class NhomThuThuatPhauThuatDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(NhomThuThuatPhauThuatDTO.class);
        NhomThuThuatPhauThuatDTO nhomThuThuatPhauThuatDTO1 = new NhomThuThuatPhauThuatDTO();
        nhomThuThuatPhauThuatDTO1.setId(1L);
        NhomThuThuatPhauThuatDTO nhomThuThuatPhauThuatDTO2 = new NhomThuThuatPhauThuatDTO();
        assertThat(nhomThuThuatPhauThuatDTO1).isNotEqualTo(nhomThuThuatPhauThuatDTO2);
        nhomThuThuatPhauThuatDTO2.setId(nhomThuThuatPhauThuatDTO1.getId());
        assertThat(nhomThuThuatPhauThuatDTO1).isEqualTo(nhomThuThuatPhauThuatDTO2);
        nhomThuThuatPhauThuatDTO2.setId(2L);
        assertThat(nhomThuThuatPhauThuatDTO1).isNotEqualTo(nhomThuThuatPhauThuatDTO2);
        nhomThuThuatPhauThuatDTO1.setId(null);
        assertThat(nhomThuThuatPhauThuatDTO1).isNotEqualTo(nhomThuThuatPhauThuatDTO2);
    }
}

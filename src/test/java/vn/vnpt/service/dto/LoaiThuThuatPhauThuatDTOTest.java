package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class LoaiThuThuatPhauThuatDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(LoaiThuThuatPhauThuatDTO.class);
        LoaiThuThuatPhauThuatDTO loaiThuThuatPhauThuatDTO1 = new LoaiThuThuatPhauThuatDTO();
        loaiThuThuatPhauThuatDTO1.setId(1L);
        LoaiThuThuatPhauThuatDTO loaiThuThuatPhauThuatDTO2 = new LoaiThuThuatPhauThuatDTO();
        assertThat(loaiThuThuatPhauThuatDTO1).isNotEqualTo(loaiThuThuatPhauThuatDTO2);
        loaiThuThuatPhauThuatDTO2.setId(loaiThuThuatPhauThuatDTO1.getId());
        assertThat(loaiThuThuatPhauThuatDTO1).isEqualTo(loaiThuThuatPhauThuatDTO2);
        loaiThuThuatPhauThuatDTO2.setId(2L);
        assertThat(loaiThuThuatPhauThuatDTO1).isNotEqualTo(loaiThuThuatPhauThuatDTO2);
        loaiThuThuatPhauThuatDTO1.setId(null);
        assertThat(loaiThuThuatPhauThuatDTO1).isNotEqualTo(loaiThuThuatPhauThuatDTO2);
    }
}

package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class ChiDinhThuThuatPhauThuatDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ChiDinhThuThuatPhauThuatDTO.class);
        ChiDinhThuThuatPhauThuatDTO chiDinhThuThuatPhauThuatDTO1 = new ChiDinhThuThuatPhauThuatDTO();
        chiDinhThuThuatPhauThuatDTO1.setId(1L);
        ChiDinhThuThuatPhauThuatDTO chiDinhThuThuatPhauThuatDTO2 = new ChiDinhThuThuatPhauThuatDTO();
        assertThat(chiDinhThuThuatPhauThuatDTO1).isNotEqualTo(chiDinhThuThuatPhauThuatDTO2);
        chiDinhThuThuatPhauThuatDTO2.setId(chiDinhThuThuatPhauThuatDTO1.getId());
        assertThat(chiDinhThuThuatPhauThuatDTO1).isEqualTo(chiDinhThuThuatPhauThuatDTO2);
        chiDinhThuThuatPhauThuatDTO2.setId(2L);
        assertThat(chiDinhThuThuatPhauThuatDTO1).isNotEqualTo(chiDinhThuThuatPhauThuatDTO2);
        chiDinhThuThuatPhauThuatDTO1.setId(null);
        assertThat(chiDinhThuThuatPhauThuatDTO1).isNotEqualTo(chiDinhThuThuatPhauThuatDTO2);
    }
}

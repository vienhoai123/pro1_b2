package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class ChucDanhDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ChucDanhDTO.class);
        ChucDanhDTO chucDanhDTO1 = new ChucDanhDTO();
        chucDanhDTO1.setId(1L);
        ChucDanhDTO chucDanhDTO2 = new ChucDanhDTO();
        assertThat(chucDanhDTO1).isNotEqualTo(chucDanhDTO2);
        chucDanhDTO2.setId(chucDanhDTO1.getId());
        assertThat(chucDanhDTO1).isEqualTo(chucDanhDTO2);
        chucDanhDTO2.setId(2L);
        assertThat(chucDanhDTO1).isNotEqualTo(chucDanhDTO2);
        chucDanhDTO1.setId(null);
        assertThat(chucDanhDTO1).isNotEqualTo(chucDanhDTO2);
    }
}

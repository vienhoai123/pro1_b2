package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import vn.vnpt.web.rest.TestUtil;

import static org.assertj.core.api.Assertions.assertThat;

public class DotThayDoiMaDichVuDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DotThayDoiMaDichVuDTO.class);
        DotThayDoiMaDichVuDTO dotThayDoiMaDichVuDTO1 = new DotThayDoiMaDichVuDTO();
        dotThayDoiMaDichVuDTO1.setId(1L);
        DotThayDoiMaDichVuDTO dotThayDoiMaDichVuDTO2 = new DotThayDoiMaDichVuDTO();
        assertThat(dotThayDoiMaDichVuDTO1).isNotEqualTo(dotThayDoiMaDichVuDTO2);
        dotThayDoiMaDichVuDTO2.setId(dotThayDoiMaDichVuDTO1.getId());
        assertThat(dotThayDoiMaDichVuDTO1).isEqualTo(dotThayDoiMaDichVuDTO2);
        dotThayDoiMaDichVuDTO2.setId(2L);
        assertThat(dotThayDoiMaDichVuDTO1).isNotEqualTo(dotThayDoiMaDichVuDTO2);
        dotThayDoiMaDichVuDTO1.setId(null);
        assertThat(dotThayDoiMaDichVuDTO1).isNotEqualTo(dotThayDoiMaDichVuDTO2);
    }
}

package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class KhoaDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(KhoaDTO.class);
        KhoaDTO khoaDTO1 = new KhoaDTO();
        khoaDTO1.setId(1L);
        KhoaDTO khoaDTO2 = new KhoaDTO();
        assertThat(khoaDTO1).isNotEqualTo(khoaDTO2);
        khoaDTO2.setId(khoaDTO1.getId());
        assertThat(khoaDTO1).isEqualTo(khoaDTO2);
        khoaDTO2.setId(2L);
        assertThat(khoaDTO1).isNotEqualTo(khoaDTO2);
        khoaDTO1.setId(null);
        assertThat(khoaDTO1).isNotEqualTo(khoaDTO2);
    }
}

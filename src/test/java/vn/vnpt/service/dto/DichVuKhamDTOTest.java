package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class DichVuKhamDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DichVuKhamDTO.class);
        DichVuKhamDTO dichVuKhamDTO1 = new DichVuKhamDTO();
        dichVuKhamDTO1.setId(1L);
        DichVuKhamDTO dichVuKhamDTO2 = new DichVuKhamDTO();
        assertThat(dichVuKhamDTO1).isNotEqualTo(dichVuKhamDTO2);
        dichVuKhamDTO2.setId(dichVuKhamDTO1.getId());
        assertThat(dichVuKhamDTO1).isEqualTo(dichVuKhamDTO2);
        dichVuKhamDTO2.setId(2L);
        assertThat(dichVuKhamDTO1).isNotEqualTo(dichVuKhamDTO2);
        dichVuKhamDTO1.setId(null);
        assertThat(dichVuKhamDTO1).isNotEqualTo(dichVuKhamDTO2);
    }
}

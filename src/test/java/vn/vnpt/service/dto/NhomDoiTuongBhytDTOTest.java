package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class NhomDoiTuongBhytDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(NhomDoiTuongBhytDTO.class);
        NhomDoiTuongBhytDTO nhomDoiTuongBhytDTO1 = new NhomDoiTuongBhytDTO();
        nhomDoiTuongBhytDTO1.setId(1L);
        NhomDoiTuongBhytDTO nhomDoiTuongBhytDTO2 = new NhomDoiTuongBhytDTO();
        assertThat(nhomDoiTuongBhytDTO1).isNotEqualTo(nhomDoiTuongBhytDTO2);
        nhomDoiTuongBhytDTO2.setId(nhomDoiTuongBhytDTO1.getId());
        assertThat(nhomDoiTuongBhytDTO1).isEqualTo(nhomDoiTuongBhytDTO2);
        nhomDoiTuongBhytDTO2.setId(2L);
        assertThat(nhomDoiTuongBhytDTO1).isNotEqualTo(nhomDoiTuongBhytDTO2);
        nhomDoiTuongBhytDTO1.setId(null);
        assertThat(nhomDoiTuongBhytDTO1).isNotEqualTo(nhomDoiTuongBhytDTO2);
    }
}

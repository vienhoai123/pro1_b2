package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class TPhanNhomTTPTDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TPhanNhomTTPTDTO.class);
        TPhanNhomTTPTDTO tPhanNhomTTPTDTO1 = new TPhanNhomTTPTDTO();
        tPhanNhomTTPTDTO1.setId(1L);
        TPhanNhomTTPTDTO tPhanNhomTTPTDTO2 = new TPhanNhomTTPTDTO();
        assertThat(tPhanNhomTTPTDTO1).isNotEqualTo(tPhanNhomTTPTDTO2);
        tPhanNhomTTPTDTO2.setId(tPhanNhomTTPTDTO1.getId());
        assertThat(tPhanNhomTTPTDTO1).isEqualTo(tPhanNhomTTPTDTO2);
        tPhanNhomTTPTDTO2.setId(2L);
        assertThat(tPhanNhomTTPTDTO1).isNotEqualTo(tPhanNhomTTPTDTO2);
        tPhanNhomTTPTDTO1.setId(null);
        assertThat(tPhanNhomTTPTDTO1).isNotEqualTo(tPhanNhomTTPTDTO2);
    }
}

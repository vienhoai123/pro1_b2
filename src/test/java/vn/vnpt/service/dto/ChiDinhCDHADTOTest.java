package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class ChiDinhCDHADTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ChiDinhCDHADTO.class);
        ChiDinhCDHADTO chiDinhCDHADTO1 = new ChiDinhCDHADTO();
        chiDinhCDHADTO1.setId(1L);
        ChiDinhCDHADTO chiDinhCDHADTO2 = new ChiDinhCDHADTO();
        assertThat(chiDinhCDHADTO1).isNotEqualTo(chiDinhCDHADTO2);
        chiDinhCDHADTO2.setId(chiDinhCDHADTO1.getId());
        assertThat(chiDinhCDHADTO1).isEqualTo(chiDinhCDHADTO2);
        chiDinhCDHADTO2.setId(2L);
        assertThat(chiDinhCDHADTO1).isNotEqualTo(chiDinhCDHADTO2);
        chiDinhCDHADTO1.setId(null);
        assertThat(chiDinhCDHADTO1).isNotEqualTo(chiDinhCDHADTO2);
    }
}

package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class NhomChanDoanHinhAnhDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(NhomChanDoanHinhAnhDTO.class);
        NhomChanDoanHinhAnhDTO nhomChanDoanHinhAnhDTO1 = new NhomChanDoanHinhAnhDTO();
        nhomChanDoanHinhAnhDTO1.setId(1L);
        NhomChanDoanHinhAnhDTO nhomChanDoanHinhAnhDTO2 = new NhomChanDoanHinhAnhDTO();
        assertThat(nhomChanDoanHinhAnhDTO1).isNotEqualTo(nhomChanDoanHinhAnhDTO2);
        nhomChanDoanHinhAnhDTO2.setId(nhomChanDoanHinhAnhDTO1.getId());
        assertThat(nhomChanDoanHinhAnhDTO1).isEqualTo(nhomChanDoanHinhAnhDTO2);
        nhomChanDoanHinhAnhDTO2.setId(2L);
        assertThat(nhomChanDoanHinhAnhDTO1).isNotEqualTo(nhomChanDoanHinhAnhDTO2);
        nhomChanDoanHinhAnhDTO1.setId(null);
        assertThat(nhomChanDoanHinhAnhDTO1).isNotEqualTo(nhomChanDoanHinhAnhDTO2);
    }
}

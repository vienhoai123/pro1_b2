package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class DonViDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DonViDTO.class);
        DonViDTO donViDTO1 = new DonViDTO();
        donViDTO1.setId(1L);
        DonViDTO donViDTO2 = new DonViDTO();
        assertThat(donViDTO1).isNotEqualTo(donViDTO2);
        donViDTO2.setId(donViDTO1.getId());
        assertThat(donViDTO1).isEqualTo(donViDTO2);
        donViDTO2.setId(2L);
        assertThat(donViDTO1).isNotEqualTo(donViDTO2);
        donViDTO1.setId(null);
        assertThat(donViDTO1).isNotEqualTo(donViDTO2);
    }
}

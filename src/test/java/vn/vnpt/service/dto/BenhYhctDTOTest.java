package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class BenhYhctDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(BenhYhctDTO.class);
        BenhYhctDTO benhYhctDTO1 = new BenhYhctDTO();
        benhYhctDTO1.setId(1L);
        BenhYhctDTO benhYhctDTO2 = new BenhYhctDTO();
        assertThat(benhYhctDTO1).isNotEqualTo(benhYhctDTO2);
        benhYhctDTO2.setId(benhYhctDTO1.getId());
        assertThat(benhYhctDTO1).isEqualTo(benhYhctDTO2);
        benhYhctDTO2.setId(2L);
        assertThat(benhYhctDTO1).isNotEqualTo(benhYhctDTO2);
        benhYhctDTO1.setId(null);
        assertThat(benhYhctDTO1).isNotEqualTo(benhYhctDTO2);
    }
}

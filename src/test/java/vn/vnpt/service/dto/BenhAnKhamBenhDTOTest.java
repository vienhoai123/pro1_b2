package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class BenhAnKhamBenhDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(BenhAnKhamBenhDTO.class);
        BenhAnKhamBenhDTO benhAnKhamBenhDTO1 = new BenhAnKhamBenhDTO();
        benhAnKhamBenhDTO1.setBenhNhanId(1L);
        benhAnKhamBenhDTO1.setDonViId(1L);
        BenhAnKhamBenhDTO benhAnKhamBenhDTO2 = new BenhAnKhamBenhDTO();
        assertThat(benhAnKhamBenhDTO1).isNotEqualTo(benhAnKhamBenhDTO2);
        benhAnKhamBenhDTO2.setBenhNhanId(benhAnKhamBenhDTO1.getBenhNhanId());
        benhAnKhamBenhDTO2.setDonViId(benhAnKhamBenhDTO1.getDonViId());
        assertThat(benhAnKhamBenhDTO1).isEqualTo(benhAnKhamBenhDTO2);
        benhAnKhamBenhDTO2.setBenhNhanId(2L);
        benhAnKhamBenhDTO2.setDonViId(2L);
        assertThat(benhAnKhamBenhDTO1).isNotEqualTo(benhAnKhamBenhDTO2);
        benhAnKhamBenhDTO1.setDonViId(null);
        assertThat(benhAnKhamBenhDTO1).isNotEqualTo(benhAnKhamBenhDTO2);
    }
}

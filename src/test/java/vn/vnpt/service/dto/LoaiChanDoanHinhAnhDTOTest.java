package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class LoaiChanDoanHinhAnhDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(LoaiChanDoanHinhAnhDTO.class);
        LoaiChanDoanHinhAnhDTO loaiChanDoanHinhAnhDTO1 = new LoaiChanDoanHinhAnhDTO();
        loaiChanDoanHinhAnhDTO1.setId(1L);
        LoaiChanDoanHinhAnhDTO loaiChanDoanHinhAnhDTO2 = new LoaiChanDoanHinhAnhDTO();
        assertThat(loaiChanDoanHinhAnhDTO1).isNotEqualTo(loaiChanDoanHinhAnhDTO2);
        loaiChanDoanHinhAnhDTO2.setId(loaiChanDoanHinhAnhDTO1.getId());
        assertThat(loaiChanDoanHinhAnhDTO1).isEqualTo(loaiChanDoanHinhAnhDTO2);
        loaiChanDoanHinhAnhDTO2.setId(2L);
        assertThat(loaiChanDoanHinhAnhDTO1).isNotEqualTo(loaiChanDoanHinhAnhDTO2);
        loaiChanDoanHinhAnhDTO1.setId(null);
        assertThat(loaiChanDoanHinhAnhDTO1).isNotEqualTo(loaiChanDoanHinhAnhDTO2);
    }
}

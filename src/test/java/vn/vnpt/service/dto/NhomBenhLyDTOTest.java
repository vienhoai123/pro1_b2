package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class NhomBenhLyDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(NhomBenhLyDTO.class);
        NhomBenhLyDTO nhomBenhLyDTO1 = new NhomBenhLyDTO();
        nhomBenhLyDTO1.setId(1L);
        NhomBenhLyDTO nhomBenhLyDTO2 = new NhomBenhLyDTO();
        assertThat(nhomBenhLyDTO1).isNotEqualTo(nhomBenhLyDTO2);
        nhomBenhLyDTO2.setId(nhomBenhLyDTO1.getId());
        assertThat(nhomBenhLyDTO1).isEqualTo(nhomBenhLyDTO2);
        nhomBenhLyDTO2.setId(2L);
        assertThat(nhomBenhLyDTO1).isNotEqualTo(nhomBenhLyDTO2);
        nhomBenhLyDTO1.setId(null);
        assertThat(nhomBenhLyDTO1).isNotEqualTo(nhomBenhLyDTO2);
    }
}

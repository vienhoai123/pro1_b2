package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class TPhanNhomCDHADTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TPhanNhomCDHADTO.class);
        TPhanNhomCDHADTO tPhanNhomCDHADTO1 = new TPhanNhomCDHADTO();
        tPhanNhomCDHADTO1.setId(1L);
        TPhanNhomCDHADTO tPhanNhomCDHADTO2 = new TPhanNhomCDHADTO();
        assertThat(tPhanNhomCDHADTO1).isNotEqualTo(tPhanNhomCDHADTO2);
        tPhanNhomCDHADTO2.setId(tPhanNhomCDHADTO1.getId());
        assertThat(tPhanNhomCDHADTO1).isEqualTo(tPhanNhomCDHADTO2);
        tPhanNhomCDHADTO2.setId(2L);
        assertThat(tPhanNhomCDHADTO1).isNotEqualTo(tPhanNhomCDHADTO2);
        tPhanNhomCDHADTO1.setId(null);
        assertThat(tPhanNhomCDHADTO1).isNotEqualTo(tPhanNhomCDHADTO2);
    }
}

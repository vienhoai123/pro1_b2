package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class XetNghiemApDungDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(XetNghiemApDungDTO.class);
        XetNghiemApDungDTO xetNghiemApDungDTO1 = new XetNghiemApDungDTO();
        xetNghiemApDungDTO1.setId(1L);
        XetNghiemApDungDTO xetNghiemApDungDTO2 = new XetNghiemApDungDTO();
        assertThat(xetNghiemApDungDTO1).isNotEqualTo(xetNghiemApDungDTO2);
        xetNghiemApDungDTO2.setId(xetNghiemApDungDTO1.getId());
        assertThat(xetNghiemApDungDTO1).isEqualTo(xetNghiemApDungDTO2);
        xetNghiemApDungDTO2.setId(2L);
        assertThat(xetNghiemApDungDTO1).isNotEqualTo(xetNghiemApDungDTO2);
        xetNghiemApDungDTO1.setId(null);
        assertThat(xetNghiemApDungDTO1).isNotEqualTo(xetNghiemApDungDTO2);
    }
}

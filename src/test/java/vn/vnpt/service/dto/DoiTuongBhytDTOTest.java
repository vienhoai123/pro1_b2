package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class DoiTuongBhytDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DoiTuongBhytDTO.class);
        DoiTuongBhytDTO doiTuongBhytDTO1 = new DoiTuongBhytDTO();
        doiTuongBhytDTO1.setId(1L);
        DoiTuongBhytDTO doiTuongBhytDTO2 = new DoiTuongBhytDTO();
        assertThat(doiTuongBhytDTO1).isNotEqualTo(doiTuongBhytDTO2);
        doiTuongBhytDTO2.setId(doiTuongBhytDTO1.getId());
        assertThat(doiTuongBhytDTO1).isEqualTo(doiTuongBhytDTO2);
        doiTuongBhytDTO2.setId(2L);
        assertThat(doiTuongBhytDTO1).isNotEqualTo(doiTuongBhytDTO2);
        doiTuongBhytDTO1.setId(null);
        assertThat(doiTuongBhytDTO1).isNotEqualTo(doiTuongBhytDTO2);
    }
}

package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class NhomXetNghiemDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(NhomXetNghiemDTO.class);
        NhomXetNghiemDTO nhomXetNghiemDTO1 = new NhomXetNghiemDTO();
        nhomXetNghiemDTO1.setId(1L);
        NhomXetNghiemDTO nhomXetNghiemDTO2 = new NhomXetNghiemDTO();
        assertThat(nhomXetNghiemDTO1).isNotEqualTo(nhomXetNghiemDTO2);
        nhomXetNghiemDTO2.setId(nhomXetNghiemDTO1.getId());
        assertThat(nhomXetNghiemDTO1).isEqualTo(nhomXetNghiemDTO2);
        nhomXetNghiemDTO2.setId(2L);
        assertThat(nhomXetNghiemDTO1).isNotEqualTo(nhomXetNghiemDTO2);
        nhomXetNghiemDTO1.setId(null);
        assertThat(nhomXetNghiemDTO1).isNotEqualTo(nhomXetNghiemDTO2);
    }
}

package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class ThongTinBhxhDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ThongTinBhxhDTO.class);
        ThongTinBhxhDTO thongTinBhxhDTO1 = new ThongTinBhxhDTO();
        thongTinBhxhDTO1.setId(1L);
        ThongTinBhxhDTO thongTinBhxhDTO2 = new ThongTinBhxhDTO();
        assertThat(thongTinBhxhDTO1).isNotEqualTo(thongTinBhxhDTO2);
        thongTinBhxhDTO2.setId(thongTinBhxhDTO1.getId());
        assertThat(thongTinBhxhDTO1).isEqualTo(thongTinBhxhDTO2);
        thongTinBhxhDTO2.setId(2L);
        assertThat(thongTinBhxhDTO1).isNotEqualTo(thongTinBhxhDTO2);
        thongTinBhxhDTO1.setId(null);
        assertThat(thongTinBhxhDTO1).isNotEqualTo(thongTinBhxhDTO2);
    }
}

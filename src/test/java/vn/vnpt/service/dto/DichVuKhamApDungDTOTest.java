package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class DichVuKhamApDungDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DichVuKhamApDungDTO.class);
        DichVuKhamApDungDTO dichVuKhamApDungDTO1 = new DichVuKhamApDungDTO();
        dichVuKhamApDungDTO1.setId(1L);
        DichVuKhamApDungDTO dichVuKhamApDungDTO2 = new DichVuKhamApDungDTO();
        assertThat(dichVuKhamApDungDTO1).isNotEqualTo(dichVuKhamApDungDTO2);
        dichVuKhamApDungDTO2.setId(dichVuKhamApDungDTO1.getId());
        assertThat(dichVuKhamApDungDTO1).isEqualTo(dichVuKhamApDungDTO2);
        dichVuKhamApDungDTO2.setId(2L);
        assertThat(dichVuKhamApDungDTO1).isNotEqualTo(dichVuKhamApDungDTO2);
        dichVuKhamApDungDTO1.setId(null);
        assertThat(dichVuKhamApDungDTO1).isNotEqualTo(dichVuKhamApDungDTO2);
    }
}

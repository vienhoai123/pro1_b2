package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class TheBhytDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TheBhytDTO.class);
        TheBhytDTO theBhytDTO1 = new TheBhytDTO();
        theBhytDTO1.setId(1L);
        TheBhytDTO theBhytDTO2 = new TheBhytDTO();
        assertThat(theBhytDTO1).isNotEqualTo(theBhytDTO2);
        theBhytDTO2.setId(theBhytDTO1.getId());
        assertThat(theBhytDTO1).isEqualTo(theBhytDTO2);
        theBhytDTO2.setId(2L);
        assertThat(theBhytDTO1).isNotEqualTo(theBhytDTO2);
        theBhytDTO1.setId(null);
        assertThat(theBhytDTO1).isNotEqualTo(theBhytDTO2);
    }
}

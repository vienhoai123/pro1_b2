package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class HuongDieuTriDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(HuongDieuTriDTO.class);
        HuongDieuTriDTO huongDieuTriDTO1 = new HuongDieuTriDTO();
        huongDieuTriDTO1.setId(1L);
        HuongDieuTriDTO huongDieuTriDTO2 = new HuongDieuTriDTO();
        assertThat(huongDieuTriDTO1).isNotEqualTo(huongDieuTriDTO2);
        huongDieuTriDTO2.setId(huongDieuTriDTO1.getId());
        assertThat(huongDieuTriDTO1).isEqualTo(huongDieuTriDTO2);
        huongDieuTriDTO2.setId(2L);
        assertThat(huongDieuTriDTO1).isNotEqualTo(huongDieuTriDTO2);
        huongDieuTriDTO1.setId(null);
        assertThat(huongDieuTriDTO1).isNotEqualTo(huongDieuTriDTO2);
    }
}

package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class QuanHuyenDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(QuanHuyenDTO.class);
        QuanHuyenDTO quanHuyenDTO1 = new QuanHuyenDTO();
        quanHuyenDTO1.setId(1L);
        QuanHuyenDTO quanHuyenDTO2 = new QuanHuyenDTO();
        assertThat(quanHuyenDTO1).isNotEqualTo(quanHuyenDTO2);
        quanHuyenDTO2.setId(quanHuyenDTO1.getId());
        assertThat(quanHuyenDTO1).isEqualTo(quanHuyenDTO2);
        quanHuyenDTO2.setId(2L);
        assertThat(quanHuyenDTO1).isNotEqualTo(quanHuyenDTO2);
        quanHuyenDTO1.setId(null);
        assertThat(quanHuyenDTO1).isNotEqualTo(quanHuyenDTO2);
    }
}

package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class PhuongXaDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PhuongXaDTO.class);
        PhuongXaDTO phuongXaDTO1 = new PhuongXaDTO();
        phuongXaDTO1.setId(1L);
        PhuongXaDTO phuongXaDTO2 = new PhuongXaDTO();
        assertThat(phuongXaDTO1).isNotEqualTo(phuongXaDTO2);
        phuongXaDTO2.setId(phuongXaDTO1.getId());
        assertThat(phuongXaDTO1).isEqualTo(phuongXaDTO2);
        phuongXaDTO2.setId(2L);
        assertThat(phuongXaDTO1).isNotEqualTo(phuongXaDTO2);
        phuongXaDTO1.setId(null);
        assertThat(phuongXaDTO1).isNotEqualTo(phuongXaDTO2);
    }
}

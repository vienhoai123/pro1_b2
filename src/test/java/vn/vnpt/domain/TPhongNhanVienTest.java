package vn.vnpt.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class TPhongNhanVienTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TPhongNhanVien.class);
        TPhongNhanVien tPhongNhanVien1 = new TPhongNhanVien();
        tPhongNhanVien1.setId(1L);
        TPhongNhanVien tPhongNhanVien2 = new TPhongNhanVien();
        tPhongNhanVien2.setId(tPhongNhanVien1.getId());
        assertThat(tPhongNhanVien1).isEqualTo(tPhongNhanVien2);
        tPhongNhanVien2.setId(2L);
        assertThat(tPhongNhanVien1).isNotEqualTo(tPhongNhanVien2);
        tPhongNhanVien1.setId(null);
        assertThat(tPhongNhanVien1).isNotEqualTo(tPhongNhanVien2);
    }
}

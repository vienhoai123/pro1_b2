package vn.vnpt.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class LoaiXetNghiemTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(LoaiXetNghiem.class);
        LoaiXetNghiem loaiXetNghiem1 = new LoaiXetNghiem();
        loaiXetNghiem1.setId(1L);
        LoaiXetNghiem loaiXetNghiem2 = new LoaiXetNghiem();
        loaiXetNghiem2.setId(loaiXetNghiem1.getId());
        assertThat(loaiXetNghiem1).isEqualTo(loaiXetNghiem2);
        loaiXetNghiem2.setId(2L);
        assertThat(loaiXetNghiem1).isNotEqualTo(loaiXetNghiem2);
        loaiXetNghiem1.setId(null);
        assertThat(loaiXetNghiem1).isNotEqualTo(loaiXetNghiem2);
    }
}

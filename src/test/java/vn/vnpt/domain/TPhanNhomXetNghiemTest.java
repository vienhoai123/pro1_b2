package vn.vnpt.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class TPhanNhomXetNghiemTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TPhanNhomXetNghiem.class);
        TPhanNhomXetNghiem tPhanNhomXetNghiem1 = new TPhanNhomXetNghiem();
        tPhanNhomXetNghiem1.setId(1L);
        TPhanNhomXetNghiem tPhanNhomXetNghiem2 = new TPhanNhomXetNghiem();
        tPhanNhomXetNghiem2.setId(tPhanNhomXetNghiem1.getId());
        assertThat(tPhanNhomXetNghiem1).isEqualTo(tPhanNhomXetNghiem2);
        tPhanNhomXetNghiem2.setId(2L);
        assertThat(tPhanNhomXetNghiem1).isNotEqualTo(tPhanNhomXetNghiem2);
        tPhanNhomXetNghiem1.setId(null);
        assertThat(tPhanNhomXetNghiem1).isNotEqualTo(tPhanNhomXetNghiem2);
    }
}

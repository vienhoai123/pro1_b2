package vn.vnpt.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class LoaiBenhLyTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(LoaiBenhLy.class);
        LoaiBenhLy loaiBenhLy1 = new LoaiBenhLy();
        loaiBenhLy1.setId(1L);
        LoaiBenhLy loaiBenhLy2 = new LoaiBenhLy();
        loaiBenhLy2.setId(loaiBenhLy1.getId());
        assertThat(loaiBenhLy1).isEqualTo(loaiBenhLy2);
        loaiBenhLy2.setId(2L);
        assertThat(loaiBenhLy1).isNotEqualTo(loaiBenhLy2);
        loaiBenhLy1.setId(null);
        assertThat(loaiBenhLy1).isNotEqualTo(loaiBenhLy2);
    }
}

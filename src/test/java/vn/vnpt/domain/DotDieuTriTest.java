package vn.vnpt.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class DotDieuTriTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DotDieuTri.class);
        DotDieuTri dotDieuTri1 = new DotDieuTri();
        dotDieuTri1.setId(new DotDieuTriId(1L, 1L));
        DotDieuTri dotDieuTri2 = new DotDieuTri();
        dotDieuTri2.setId(new DotDieuTriId(1L, 1L));
        assertThat(dotDieuTri1).isEqualTo(dotDieuTri2);
        dotDieuTri2.setId(new DotDieuTriId(2L, 2L));
        assertThat(dotDieuTri1).isNotEqualTo(dotDieuTri2);
        dotDieuTri1.setId(null);
        assertThat(dotDieuTri1).isNotEqualTo(dotDieuTri2);
    }
}

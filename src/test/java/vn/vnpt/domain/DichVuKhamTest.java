package vn.vnpt.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class DichVuKhamTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DichVuKham.class);
        DichVuKham dichVuKham1 = new DichVuKham();
        dichVuKham1.setId(1L);
        DichVuKham dichVuKham2 = new DichVuKham();
        dichVuKham2.setId(1L);
        assertThat(dichVuKham1).isEqualTo(dichVuKham2);
        dichVuKham2.setId(2L);
        assertThat(dichVuKham1).isNotEqualTo(dichVuKham2);
        dichVuKham1.setId(null);
        assertThat(dichVuKham1).isNotEqualTo(dichVuKham2);
    }
}

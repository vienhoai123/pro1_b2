package vn.vnpt.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class DotGiaDichVuBhxhTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DotGiaDichVuBhxh.class);
        DotGiaDichVuBhxh dotGiaDichVuBhxh1 = new DotGiaDichVuBhxh();
        dotGiaDichVuBhxh1.setId(1L);
        DotGiaDichVuBhxh dotGiaDichVuBhxh2 = new DotGiaDichVuBhxh();
        dotGiaDichVuBhxh2.setId(dotGiaDichVuBhxh1.getId());
        assertThat(dotGiaDichVuBhxh1).isEqualTo(dotGiaDichVuBhxh2);
        dotGiaDichVuBhxh2.setId(2L);
        assertThat(dotGiaDichVuBhxh1).isNotEqualTo(dotGiaDichVuBhxh2);
        dotGiaDichVuBhxh1.setId(null);
        assertThat(dotGiaDichVuBhxh1).isNotEqualTo(dotGiaDichVuBhxh2);
    }
}

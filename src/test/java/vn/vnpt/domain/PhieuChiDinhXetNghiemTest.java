package vn.vnpt.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class PhieuChiDinhXetNghiemTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PhieuChiDinhXetNghiem.class);
        PhieuChiDinhXetNghiem phieuChiDinhXetNghiem1 = new PhieuChiDinhXetNghiem();
        phieuChiDinhXetNghiem1.setId(1L);
        PhieuChiDinhXetNghiem phieuChiDinhXetNghiem2 = new PhieuChiDinhXetNghiem();
        phieuChiDinhXetNghiem2.setId(phieuChiDinhXetNghiem1.getId());
        assertThat(phieuChiDinhXetNghiem1).isEqualTo(phieuChiDinhXetNghiem2);
        phieuChiDinhXetNghiem2.setId(2L);
        assertThat(phieuChiDinhXetNghiem1).isNotEqualTo(phieuChiDinhXetNghiem2);
        phieuChiDinhXetNghiem1.setId(null);
        assertThat(phieuChiDinhXetNghiem1).isNotEqualTo(phieuChiDinhXetNghiem2);
    }
}

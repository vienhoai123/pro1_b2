package vn.vnpt.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class LoaiChanDoanHinhAnhTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(LoaiChanDoanHinhAnh.class);
        LoaiChanDoanHinhAnh loaiChanDoanHinhAnh1 = new LoaiChanDoanHinhAnh();
        loaiChanDoanHinhAnh1.setId(1L);
        LoaiChanDoanHinhAnh loaiChanDoanHinhAnh2 = new LoaiChanDoanHinhAnh();
        loaiChanDoanHinhAnh2.setId(loaiChanDoanHinhAnh1.getId());
        assertThat(loaiChanDoanHinhAnh1).isEqualTo(loaiChanDoanHinhAnh2);
        loaiChanDoanHinhAnh2.setId(2L);
        assertThat(loaiChanDoanHinhAnh1).isNotEqualTo(loaiChanDoanHinhAnh2);
        loaiChanDoanHinhAnh1.setId(null);
        assertThat(loaiChanDoanHinhAnh1).isNotEqualTo(loaiChanDoanHinhAnh2);
    }
}

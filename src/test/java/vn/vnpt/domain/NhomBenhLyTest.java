package vn.vnpt.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class NhomBenhLyTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(NhomBenhLy.class);
        NhomBenhLy nhomBenhLy1 = new NhomBenhLy();
        nhomBenhLy1.setId(1L);
        NhomBenhLy nhomBenhLy2 = new NhomBenhLy();
        nhomBenhLy2.setId(nhomBenhLy1.getId());
        assertThat(nhomBenhLy1).isEqualTo(nhomBenhLy2);
        nhomBenhLy2.setId(2L);
        assertThat(nhomBenhLy1).isNotEqualTo(nhomBenhLy2);
        nhomBenhLy1.setId(null);
        assertThat(nhomBenhLy1).isNotEqualTo(nhomBenhLy2);
    }
}

package vn.vnpt.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class QuanHuyenTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(QuanHuyen.class);
        QuanHuyen quanHuyen1 = new QuanHuyen();
        quanHuyen1.setId(1L);
        QuanHuyen quanHuyen2 = new QuanHuyen();
        quanHuyen2.setId(quanHuyen1.getId());
        assertThat(quanHuyen1).isEqualTo(quanHuyen2);
        quanHuyen2.setId(2L);
        assertThat(quanHuyen1).isNotEqualTo(quanHuyen2);
        quanHuyen1.setId(null);
        assertThat(quanHuyen1).isNotEqualTo(quanHuyen2);
    }
}

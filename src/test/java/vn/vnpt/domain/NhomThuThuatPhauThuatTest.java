package vn.vnpt.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class NhomThuThuatPhauThuatTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(NhomThuThuatPhauThuat.class);
        NhomThuThuatPhauThuat nhomThuThuatPhauThuat1 = new NhomThuThuatPhauThuat();
        nhomThuThuatPhauThuat1.setId(1L);
        NhomThuThuatPhauThuat nhomThuThuatPhauThuat2 = new NhomThuThuatPhauThuat();
        nhomThuThuatPhauThuat2.setId(nhomThuThuatPhauThuat1.getId());
        assertThat(nhomThuThuatPhauThuat1).isEqualTo(nhomThuThuatPhauThuat2);
        nhomThuThuatPhauThuat2.setId(2L);
        assertThat(nhomThuThuatPhauThuat1).isNotEqualTo(nhomThuThuatPhauThuat2);
        nhomThuThuatPhauThuat1.setId(null);
        assertThat(nhomThuThuatPhauThuat1).isNotEqualTo(nhomThuThuatPhauThuat2);
    }
}

package vn.vnpt.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class ThuThuatPhauThuatApDungTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ThuThuatPhauThuatApDung.class);
        ThuThuatPhauThuatApDung thuThuatPhauThuatApDung1 = new ThuThuatPhauThuatApDung();
        thuThuatPhauThuatApDung1.setId(1L);
        ThuThuatPhauThuatApDung thuThuatPhauThuatApDung2 = new ThuThuatPhauThuatApDung();
        thuThuatPhauThuatApDung2.setId(thuThuatPhauThuatApDung1.getId());
        assertThat(thuThuatPhauThuatApDung1).isEqualTo(thuThuatPhauThuatApDung2);
        thuThuatPhauThuatApDung2.setId(2L);
        assertThat(thuThuatPhauThuatApDung1).isNotEqualTo(thuThuatPhauThuatApDung2);
        thuThuatPhauThuatApDung1.setId(null);
        assertThat(thuThuatPhauThuatApDung1).isNotEqualTo(thuThuatPhauThuatApDung2);
    }
}

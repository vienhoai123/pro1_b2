package vn.vnpt.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class ChiDinhXetNghiemTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ChiDinhXetNghiem.class);
        ChiDinhXetNghiem chiDinhXetNghiem1 = new ChiDinhXetNghiem();
        chiDinhXetNghiem1.setId(1L);
        ChiDinhXetNghiem chiDinhXetNghiem2 = new ChiDinhXetNghiem();
        chiDinhXetNghiem2.setId(chiDinhXetNghiem1.getId());
        assertThat(chiDinhXetNghiem1).isEqualTo(chiDinhXetNghiem2);
        chiDinhXetNghiem2.setId(2L);
        assertThat(chiDinhXetNghiem1).isNotEqualTo(chiDinhXetNghiem2);
        chiDinhXetNghiem1.setId(null);
        assertThat(chiDinhXetNghiem1).isNotEqualTo(chiDinhXetNghiem2);
    }
}

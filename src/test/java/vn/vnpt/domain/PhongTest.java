package vn.vnpt.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class PhongTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Phong.class);
        Phong phong1 = new Phong();
        phong1.setId(1L);
        Phong phong2 = new Phong();
        phong2.setId(phong1.getId());
        assertThat(phong1).isEqualTo(phong2);
        phong2.setId(2L);
        assertThat(phong1).isNotEqualTo(phong2);
        phong1.setId(null);
        assertThat(phong1).isNotEqualTo(phong2);
    }
}

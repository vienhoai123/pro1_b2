package vn.vnpt.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class TheBhytTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TheBhyt.class);
        TheBhyt theBhyt1 = new TheBhyt();
        theBhyt1.setId(1L);
        TheBhyt theBhyt2 = new TheBhyt();
        theBhyt2.setId(theBhyt1.getId());
        assertThat(theBhyt1).isEqualTo(theBhyt2);
        theBhyt2.setId(2L);
        assertThat(theBhyt1).isNotEqualTo(theBhyt2);
        theBhyt1.setId(null);
        assertThat(theBhyt1).isNotEqualTo(theBhyt2);
    }
}

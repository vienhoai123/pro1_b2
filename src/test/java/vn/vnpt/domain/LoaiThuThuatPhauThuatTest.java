package vn.vnpt.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class LoaiThuThuatPhauThuatTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(LoaiThuThuatPhauThuat.class);
        LoaiThuThuatPhauThuat loaiThuThuatPhauThuat1 = new LoaiThuThuatPhauThuat();
        loaiThuThuatPhauThuat1.setId(1L);
        LoaiThuThuatPhauThuat loaiThuThuatPhauThuat2 = new LoaiThuThuatPhauThuat();
        loaiThuThuatPhauThuat2.setId(loaiThuThuatPhauThuat1.getId());
        assertThat(loaiThuThuatPhauThuat1).isEqualTo(loaiThuThuatPhauThuat2);
        loaiThuThuatPhauThuat2.setId(2L);
        assertThat(loaiThuThuatPhauThuat1).isNotEqualTo(loaiThuThuatPhauThuat2);
        loaiThuThuatPhauThuat1.setId(null);
        assertThat(loaiThuThuatPhauThuat1).isNotEqualTo(loaiThuThuatPhauThuat2);
    }
}

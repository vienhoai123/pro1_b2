package vn.vnpt.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class HuongDieuTriTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(HuongDieuTri.class);
        HuongDieuTri huongDieuTri1 = new HuongDieuTri();
        huongDieuTri1.setId(1L);
        HuongDieuTri huongDieuTri2 = new HuongDieuTri();
        huongDieuTri2.setId(huongDieuTri1.getId());
        assertThat(huongDieuTri1).isEqualTo(huongDieuTri2);
        huongDieuTri2.setId(2L);
        assertThat(huongDieuTri1).isNotEqualTo(huongDieuTri2);
        huongDieuTri1.setId(null);
        assertThat(huongDieuTri1).isNotEqualTo(huongDieuTri2);
    }
}

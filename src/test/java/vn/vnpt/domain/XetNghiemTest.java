package vn.vnpt.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class XetNghiemTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(XetNghiem.class);
        XetNghiem xetNghiem1 = new XetNghiem();
        xetNghiem1.setId(1L);
        XetNghiem xetNghiem2 = new XetNghiem();
        xetNghiem2.setId(xetNghiem1.getId());
        assertThat(xetNghiem1).isEqualTo(xetNghiem2);
        xetNghiem2.setId(2L);
        assertThat(xetNghiem1).isNotEqualTo(xetNghiem2);
        xetNghiem1.setId(null);
        assertThat(xetNghiem1).isNotEqualTo(xetNghiem2);
    }
}

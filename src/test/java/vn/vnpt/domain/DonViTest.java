package vn.vnpt.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class DonViTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DonVi.class);
        DonVi donVi1 = new DonVi();
        donVi1.setId(1L);
        DonVi donVi2 = new DonVi();
        donVi2.setId(1L);
        assertThat(donVi1).isEqualTo(donVi2);
        donVi2.setId(2L);
        assertThat(donVi1).isNotEqualTo(donVi2);
        donVi1.setId(null);
        assertThat(donVi1).isNotEqualTo(donVi2);
    }
}

package vn.vnpt.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class TPhanNhomCDHATest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TPhanNhomCDHA.class);
        TPhanNhomCDHA tPhanNhomCDHA1 = new TPhanNhomCDHA();
        tPhanNhomCDHA1.setId(1L);
        TPhanNhomCDHA tPhanNhomCDHA2 = new TPhanNhomCDHA();
        tPhanNhomCDHA2.setId(tPhanNhomCDHA1.getId());
        assertThat(tPhanNhomCDHA1).isEqualTo(tPhanNhomCDHA2);
        tPhanNhomCDHA2.setId(2L);
        assertThat(tPhanNhomCDHA1).isNotEqualTo(tPhanNhomCDHA2);
        tPhanNhomCDHA1.setId(null);
        assertThat(tPhanNhomCDHA1).isNotEqualTo(tPhanNhomCDHA2);
    }
}

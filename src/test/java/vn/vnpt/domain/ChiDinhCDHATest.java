package vn.vnpt.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class ChiDinhCDHATest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ChiDinhCDHA.class);
        ChiDinhCDHA chiDinhCDHA1 = new ChiDinhCDHA();
        chiDinhCDHA1.setId(1L);
        ChiDinhCDHA chiDinhCDHA2 = new ChiDinhCDHA();
        chiDinhCDHA2.setId(chiDinhCDHA1.getId());
        assertThat(chiDinhCDHA1).isEqualTo(chiDinhCDHA2);
        chiDinhCDHA2.setId(2L);
        assertThat(chiDinhCDHA1).isNotEqualTo(chiDinhCDHA2);
        chiDinhCDHA1.setId(null);
        assertThat(chiDinhCDHA1).isNotEqualTo(chiDinhCDHA2);
    }
}

package vn.vnpt.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class ChiDinhDichVuKhamTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ChiDinhDichVuKham.class);
        ChiDinhDichVuKham chiDinhDichVuKham1 = new ChiDinhDichVuKham();
        chiDinhDichVuKham1.setId(1L);
        ChiDinhDichVuKham chiDinhDichVuKham2 = new ChiDinhDichVuKham();
        chiDinhDichVuKham2.setId(chiDinhDichVuKham1.getId());
        assertThat(chiDinhDichVuKham1).isEqualTo(chiDinhDichVuKham2);
        chiDinhDichVuKham2.setId(2L);
        assertThat(chiDinhDichVuKham1).isNotEqualTo(chiDinhDichVuKham2);
        chiDinhDichVuKham1.setId(null);
        assertThat(chiDinhDichVuKham1).isNotEqualTo(chiDinhDichVuKham2);
    }
}

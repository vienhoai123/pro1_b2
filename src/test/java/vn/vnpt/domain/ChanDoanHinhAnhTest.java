package vn.vnpt.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class ChanDoanHinhAnhTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ChanDoanHinhAnh.class);
        ChanDoanHinhAnh chanDoanHinhAnh1 = new ChanDoanHinhAnh();
        chanDoanHinhAnh1.setId(1L);
        ChanDoanHinhAnh chanDoanHinhAnh2 = new ChanDoanHinhAnh();
        chanDoanHinhAnh2.setId(chanDoanHinhAnh1.getId());
        assertThat(chanDoanHinhAnh1).isEqualTo(chanDoanHinhAnh2);
        chanDoanHinhAnh2.setId(2L);
        assertThat(chanDoanHinhAnh1).isNotEqualTo(chanDoanHinhAnh2);
        chanDoanHinhAnh1.setId(null);
        assertThat(chanDoanHinhAnh1).isNotEqualTo(chanDoanHinhAnh2);
    }
}

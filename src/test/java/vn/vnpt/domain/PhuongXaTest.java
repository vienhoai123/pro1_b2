package vn.vnpt.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class PhuongXaTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PhuongXa.class);
        PhuongXa phuongXa1 = new PhuongXa();
        phuongXa1.setId(1L);
        PhuongXa phuongXa2 = new PhuongXa();
        phuongXa2.setId(phuongXa1.getId());
        assertThat(phuongXa1).isEqualTo(phuongXa2);
        phuongXa2.setId(2L);
        assertThat(phuongXa1).isNotEqualTo(phuongXa2);
        phuongXa1.setId(null);
        assertThat(phuongXa1).isNotEqualTo(phuongXa2);
    }
}

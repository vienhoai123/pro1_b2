package vn.vnpt.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class TBenhLyKhamBenhTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TBenhLyKhamBenh.class);
        TBenhLyKhamBenh tBenhLyKhamBenh1 = new TBenhLyKhamBenh();
        tBenhLyKhamBenh1.setId(1L);
        TBenhLyKhamBenh tBenhLyKhamBenh2 = new TBenhLyKhamBenh();
        tBenhLyKhamBenh2.setId(tBenhLyKhamBenh1.getId());
        assertThat(tBenhLyKhamBenh1).isEqualTo(tBenhLyKhamBenh2);
        tBenhLyKhamBenh2.setId(2L);
        assertThat(tBenhLyKhamBenh1).isNotEqualTo(tBenhLyKhamBenh2);
        tBenhLyKhamBenh1.setId(null);
        assertThat(tBenhLyKhamBenh1).isNotEqualTo(tBenhLyKhamBenh2);
    }
}

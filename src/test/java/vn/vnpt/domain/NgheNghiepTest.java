package vn.vnpt.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class NgheNghiepTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(NgheNghiep.class);
        NgheNghiep ngheNghiep1 = new NgheNghiep();
        ngheNghiep1.setId(1L);
        NgheNghiep ngheNghiep2 = new NgheNghiep();
        ngheNghiep2.setId(ngheNghiep1.getId());
        assertThat(ngheNghiep1).isEqualTo(ngheNghiep2);
        ngheNghiep2.setId(2L);
        assertThat(ngheNghiep1).isNotEqualTo(ngheNghiep2);
        ngheNghiep1.setId(null);
        assertThat(ngheNghiep1).isNotEqualTo(ngheNghiep2);
    }
}

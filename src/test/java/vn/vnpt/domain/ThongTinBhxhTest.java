package vn.vnpt.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class ThongTinBhxhTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ThongTinBhxh.class);
        ThongTinBhxh thongTinBhxh1 = new ThongTinBhxh();
        thongTinBhxh1.setId(1L);
        ThongTinBhxh thongTinBhxh2 = new ThongTinBhxh();
        thongTinBhxh2.setId(thongTinBhxh1.getId());
        assertThat(thongTinBhxh1).isEqualTo(thongTinBhxh2);
        thongTinBhxh2.setId(2L);
        assertThat(thongTinBhxh1).isNotEqualTo(thongTinBhxh2);
        thongTinBhxh1.setId(null);
        assertThat(thongTinBhxh1).isNotEqualTo(thongTinBhxh2);
    }
}

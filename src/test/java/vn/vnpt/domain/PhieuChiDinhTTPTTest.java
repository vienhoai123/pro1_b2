package vn.vnpt.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class PhieuChiDinhTTPTTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PhieuChiDinhTTPT.class);
        PhieuChiDinhTTPT phieuChiDinhTTPT1 = new PhieuChiDinhTTPT();
        phieuChiDinhTTPT1.setId(1L);
        PhieuChiDinhTTPT phieuChiDinhTTPT2 = new PhieuChiDinhTTPT();
        phieuChiDinhTTPT2.setId(phieuChiDinhTTPT1.getId());
        assertThat(phieuChiDinhTTPT1).isEqualTo(phieuChiDinhTTPT2);
        phieuChiDinhTTPT2.setId(2L);
        assertThat(phieuChiDinhTTPT1).isNotEqualTo(phieuChiDinhTTPT2);
        phieuChiDinhTTPT1.setId(null);
        assertThat(phieuChiDinhTTPT1).isNotEqualTo(phieuChiDinhTTPT2);
    }
}

package vn.vnpt.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class NhomChanDoanHinhAnhTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(NhomChanDoanHinhAnh.class);
        NhomChanDoanHinhAnh nhomChanDoanHinhAnh1 = new NhomChanDoanHinhAnh();
        nhomChanDoanHinhAnh1.setId(1L);
        NhomChanDoanHinhAnh nhomChanDoanHinhAnh2 = new NhomChanDoanHinhAnh();
        nhomChanDoanHinhAnh2.setId(nhomChanDoanHinhAnh1.getId());
        assertThat(nhomChanDoanHinhAnh1).isEqualTo(nhomChanDoanHinhAnh2);
        nhomChanDoanHinhAnh2.setId(2L);
        assertThat(nhomChanDoanHinhAnh1).isNotEqualTo(nhomChanDoanHinhAnh2);
        nhomChanDoanHinhAnh1.setId(null);
        assertThat(nhomChanDoanHinhAnh1).isNotEqualTo(nhomChanDoanHinhAnh2);
    }
}

package vn.vnpt.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class KhoaTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Khoa.class);
        Khoa khoa1 = new Khoa();
        khoa1.setId(1L);
        Khoa khoa2 = new Khoa();
        khoa2.setId(khoa1.getId());
        assertThat(khoa1).isEqualTo(khoa2);
        khoa2.setId(2L);
        assertThat(khoa1).isNotEqualTo(khoa2);
        khoa1.setId(null);
        assertThat(khoa1).isNotEqualTo(khoa2);
    }
}

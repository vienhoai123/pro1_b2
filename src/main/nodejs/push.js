const fs = require('fs');

let rawdata = fs.readFileSync('./src/main/nodejs/postman-api-key.json');
let fileJson = JSON.parse(rawdata);

var https = require('follow-redirects').https;

var response;

var options = {
  'method': 'GET',
  'hostname': 'api.getpostman.com',
  'path': '/collections',
  'headers': {
    'X-Api-Key': fileJson.postmanApiKey
  },
  'maxRedirects': 20
};

var req = https.request(options, function (res) {
  var chunks = [];

  res.on("data", function (chunk) {
    chunks.push(chunk);
  });

  res.on("end", function (chunk) {
    var body = Buffer.concat(chunks);
    response = JSON.parse(body.toString());
    // console.log(response.collections);

    response.collections.forEach(function(collection) {
      if (collection.name === "BSGDKhamChuaBenh") {
        console.log(collection.id);
        fileJson.BSGDKhamChuaBenh.collectionId = collection.id;
      }
    });

    let data = JSON.stringify(fileJson);
    fs.writeFileSync('./src/main/nodejs/postman-api-key.json', data);
  });

  res.on("error", function (error) {
    console.error(error);
  });
});

req.end();
console.log(fileJson.BSGDKhamChuaBenh.collectionId);
var optionsSingleCollection = {
  'method': 'GET',
  'hostname': 'api.getpostman.com',
  'path': '/collections/'+fileJson.BSGDKhamChuaBenh.collectionId,
  'headers': {
    'X-Api-Key': fileJson.postmanApiKey
  },
  'maxRedirects': 20
};

var reqSingle = https.request(optionsSingleCollection, function (res) {
  var chunks = [];

  res.on("data", function (chunk) {
    chunks.push(chunk);
  });

  res.on("end", function (chunk) {
    var body = Buffer.concat(chunks);
    response = JSON.parse(body.toString());

    // delete _postman_id in info

    delete response.collection.info._postman_id;

    let data = JSON.stringify(response);
    fs.writeFileSync('./src/main/nodejs/BSGDKhamChuaBenh.json', data);
    // console.log(body.toString());
  });

  res.on("error", function (error) {
    console.error(error);
  });
});

reqSingle.end();

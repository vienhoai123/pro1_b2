package vn.vnpt.domain;
import javax.persistence.*;
import java.util.Objects;

@Embeddable
public class BenhAnKhamBenhId implements java.io.Serializable {

    private Long id;
    private Long benhNhanId;
    private Long donViId;

    public BenhAnKhamBenhId(){}

    public BenhAnKhamBenhId(Long id, Long benhNhanId, Long donViId){
        this.id = id;
        this.benhNhanId = benhNhanId;
        this.donViId = donViId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBenhNhanId() {
        return benhNhanId;
    }

    public void setBenhNhanId(Long benhNhanId) {
        this.benhNhanId = benhNhanId;
    }

    public Long getDonViId() {
        return donViId;
    }

    public void setDonViId(Long donViId) {
        this.donViId = donViId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BenhAnKhamBenhId)) return false;
        BenhAnKhamBenhId that = (BenhAnKhamBenhId) o;
        return Objects.equals(getId(), that.getId()) &&
            Objects.equals(getBenhNhanId(), that.getBenhNhanId()) &&
            Objects.equals(getDonViId(), that.getDonViId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getBenhNhanId(), getDonViId());
    }
}

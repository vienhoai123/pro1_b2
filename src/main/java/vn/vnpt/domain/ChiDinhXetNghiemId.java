package vn.vnpt.domain;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class ChiDinhXetNghiemId implements Serializable {
    private Long id;
    private Long benhNhanId;
    private Long donViId;
    private Long bakbId;
    private Long phieuCDId;

    public ChiDinhXetNghiemId() {
    }

    public ChiDinhXetNghiemId(Long id, Long benhNhanId, Long donViId, Long bakbId, Long phieuCDId) {
        this.id = id;
        this.benhNhanId = benhNhanId;
        this.donViId = donViId;
        this.bakbId=bakbId;
        this.phieuCDId=phieuCDId;
    }
    public ChiDinhXetNghiemId(Long benhNhanId, Long donViId, Long bakbId, Long phieuCDId) {
        this.benhNhanId = benhNhanId;
        this.donViId = donViId;
        this.bakbId=bakbId;
        this.phieuCDId=phieuCDId;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBenhNhanId() {
        return benhNhanId;
    }

    public void setBenhNhanId(Long benhNhanId) {
        this.benhNhanId = benhNhanId;
    }

    public Long getDonViId() {
        return donViId;
    }

    public void setDonViId(Long donViId) {
        this.donViId = donViId;
    }

    public Long getBakbId() {
        return bakbId;
    }

    public void setBakbId(Long bakbId) {
        this.bakbId = bakbId;
    }

    public Long getPhieuId() {
        return phieuCDId;
    }

    public void setPhieuId(Long phieuCDId) {
        this.phieuCDId = phieuCDId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ChiDinhXetNghiemId)) return false;
        ChiDinhXetNghiemId that = (ChiDinhXetNghiemId) o;
        return Objects.equals(getId(), that.getId()) &&
            Objects.equals(getBenhNhanId(), that.getBenhNhanId()) &&
            Objects.equals(getDonViId(), that.getDonViId()) &&
            Objects.equals(getBakbId(), that.getBakbId()) &&
            Objects.equals(getPhieuId(), that.getPhieuId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getBenhNhanId(), getDonViId(), getBakbId(), getPhieuId());
    }

    @Override
    public String toString() {
        return "ChiDinhXetNghiemId{" +
            "id=" + id +
            ", benhNhanId=" + benhNhanId +
            ", donViId=" + donViId +
            ", bakbId=" + bakbId +
            ", phieuId=" + phieuCDId +
            '}';
    }
}

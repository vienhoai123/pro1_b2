package vn.vnpt.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Menu.
 */
@Entity
@Table(name = "menu")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Menu implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * Trạng thái có hiệu lực của dịch vụ:\n1: Còn hiệu lực.\n0: Đã ẩn.
     */
    @NotNull
    @Column(name = "enabled", nullable = false)
    private Boolean enabled;

    /**
     * Cấp của menu
     */
    @NotNull
    @Column(name = "jhi_level", nullable = false)
    private Integer level;

    /**
     * Tên hiển thị của menu
     */
    @NotNull
    @Size(max = 255)
    @Column(name = "name", length = 255, nullable = false)
    private String name;

    /**
     * Đường dẫn của menu
     */
    @Size(max = 255)
    @Column(name = "uri", length = 255)
    private String uri;

    /**
     * Mã menu cha
     */
    @Column(name = "parent")
    private Integer parent;

    /**
     * Mã đơn vị
     */
    @NotNull
    @Column(name = "don_vi_id", nullable = false)
    private Long donViId;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @NotNull
    @JoinTable(name = "menu_user",
               joinColumns = @JoinColumn(name = "menu_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"))
    private Set<UserExtra> users = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public Menu enabled(Boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Integer getLevel() {
        return level;
    }

    public Menu level(Integer level) {
        this.level = level;
        return this;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public Menu name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUri() {
        return uri;
    }

    public Menu uri(String uri) {
        this.uri = uri;
        return this;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public Integer getParent() {
        return parent;
    }

    public Menu parent(Integer parent) {
        this.parent = parent;
        return this;
    }

    public void setParent(Integer parent) {
        this.parent = parent;
    }

    public Long getDonViId() {
        return donViId;
    }

    public Menu donViId(Long donViId) {
        this.donViId = donViId;
        return this;
    }

    public void setDonViId(Long donViId) {
        this.donViId = donViId;
    }

    public Set<UserExtra> getUsers() {
        return users;
    }

    public Menu users(Set<UserExtra> userExtras) {
        this.users = userExtras;
        return this;
    }

    public Menu addUser(UserExtra userExtra) {
        this.users.add(userExtra);
        userExtra.getMenus().add(this);
        return this;
    }

    public Menu removeUser(UserExtra userExtra) {
        this.users.remove(userExtra);
        userExtra.getMenus().remove(this);
        return this;
    }

    public void setUsers(Set<UserExtra> userExtras) {
        this.users = userExtras;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Menu)) {
            return false;
        }
        return id != null && id.equals(((Menu) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Menu{" +
            "id=" + getId() +
            ", enabled='" + isEnabled() + "'" +
            ", level=" + getLevel() +
            ", name='" + getName() + "'" +
            ", uri='" + getUri() + "'" +
            ", parent=" + getParent() +
            ", donViId=" + getDonViId() +
            "}";
    }
}

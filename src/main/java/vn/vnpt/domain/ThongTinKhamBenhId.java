package vn.vnpt.domain;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class ThongTinKhamBenhId implements Serializable {
    private Long id;
    private Long thongTinKhoaId;
    private Long dotDieuTriId;
    private Long bakbId;
    private Long benhNhanId;
    private Long donViId;

    public ThongTinKhamBenhId() {
    }

    public ThongTinKhamBenhId(Long id, ThongTinKhoaId cId) {
        this.id = id;
        this.thongTinKhoaId = cId.getId();
        this.dotDieuTriId = cId.getDotDieuTriId();
        this.bakbId = cId.getBakbId();
        this.benhNhanId = cId.getBenhNhanId();
        this.donViId = cId.getDonViId();
    }

    public ThongTinKhamBenhId(Long id, Long thongTinKhoaId, Long dotDieuTriId, Long bakbId, Long benhNhanId, Long donViId) {
        this.id = id;
        this.thongTinKhoaId = thongTinKhoaId;
        this.dotDieuTriId = dotDieuTriId;
        this.bakbId = bakbId;
        this.benhNhanId = benhNhanId;
        this.donViId = donViId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getThongTinKhoaId() {
        return thongTinKhoaId;
    }

    public void setThongTinKhoaId(Long thongTinKhoaId) {
        this.thongTinKhoaId = thongTinKhoaId;
    }

    public Long getDotDieuTriId() {
        return dotDieuTriId;
    }

    public void setDotDieuTriId(Long dotDieuTriId) {
        this.dotDieuTriId = dotDieuTriId;
    }

    public Long getBakbId() {
        return bakbId;
    }

    public void setBakbId(Long bakbId) {
        this.bakbId = bakbId;
    }

    public Long getBenhNhanId() {
        return benhNhanId;
    }

    public void setBenhNhanId(Long benhNhanId) {
        this.benhNhanId = benhNhanId;
    }

    public Long getDonViId() {
        return donViId;
    }

    public void setDonViId(Long donViId) {
        this.donViId = donViId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ThongTinKhamBenhId)) return false;
        ThongTinKhamBenhId that = (ThongTinKhamBenhId) o;
        return Objects.equals(getId(), that.getId()) &&
            Objects.equals(getThongTinKhoaId(), that.getThongTinKhoaId()) &&
            Objects.equals(getDotDieuTriId(), that.getDotDieuTriId()) &&
            Objects.equals(getBakbId(), that.getBakbId()) &&
            Objects.equals(getBenhNhanId(), that.getBenhNhanId()) &&
            Objects.equals(getDonViId(), that.getDonViId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getThongTinKhoaId(), getDotDieuTriId(), getBakbId(), getBenhNhanId(), getDonViId());
    }
}

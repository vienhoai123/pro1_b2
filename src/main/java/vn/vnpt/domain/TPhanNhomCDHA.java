package vn.vnpt.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A TPhanNhomCDHA.
 */
@Entity
@Table(name = "tpn_cdha")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class TPhanNhomCDHA implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("tPhanNhomCDHAS")
    private NhomChanDoanHinhAnh nhom_cdha;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("tPhanNhomCDHAS")
    private ChanDoanHinhAnh cdha;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public NhomChanDoanHinhAnh getNhom_cdha() {
        return nhom_cdha;
    }

    public TPhanNhomCDHA nhom_cdha(NhomChanDoanHinhAnh nhomChanDoanHinhAnh) {
        this.nhom_cdha = nhomChanDoanHinhAnh;
        return this;
    }

    public void setNhom_cdha(NhomChanDoanHinhAnh nhomChanDoanHinhAnh) {
        this.nhom_cdha = nhomChanDoanHinhAnh;
    }

    public ChanDoanHinhAnh getCdha() {
        return cdha;
    }

    public TPhanNhomCDHA cdha(ChanDoanHinhAnh chanDoanHinhAnh) {
        this.cdha = chanDoanHinhAnh;
        return this;
    }

    public void setCdha(ChanDoanHinhAnh chanDoanHinhAnh) {
        this.cdha = chanDoanHinhAnh;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TPhanNhomCDHA)) {
            return false;
        }
        return id != null && id.equals(((TPhanNhomCDHA) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "TPhanNhomCDHA{" +
            "id=" + getId() +
            "}";
    }
}

package vn.vnpt.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;
import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * The ThongTinKhoa entity.
 */
@Entity
@Table(name = "thong_tin_khoa")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@IdClass(ThongTinKhoaId.class)
public class ThongTinKhoa implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * Thời gian Khoa nhận bệnh
     */
    @Column(name = "thoi_gian_nhan_benh")
    private LocalDate thoiGianNhanBenh;

    /**
     * Thời gian chuyển bệnh nhân chuyển sang Khoa khác
     */
    @Column(name = "thoi_gian_chuyen_khoa")
    private LocalDate thoiGianChuyenKhoa;

    /**
     * Mã Khoa phòng hiện tại
     */
    @Column(name = "khoa_id")
    private Integer khoaId;

    /**
     * khoaChuyenDen Mã khoa chuyển bệnh nhân đến
     */
    @Column(name = "khoa_chuyen_den")
    private Integer khoaChuyenDen;

    /**
     * khoaChuyenDi Mã khoa chuyển đi
     */
    @Column(name = "khoa_chuyen_di")
    private Integer khoaChuyenDi;

    /**
     * nhanVienNhanBenh nhân viên nhận bệnh
     */
    @Column(name = "nhan_vien_nhan_benh")
    private Integer nhanVienNhanBenh;

    /**
     * nhanVienChuyenKhoa
     */
    @Column(name = "nhan_vien_chuyen_khoa")
    private Integer nhanVienChuyenKhoa;

    /**
     * soNgay: số ngày nằm viện trong khoa
     */
    @Column(name = "so_ngay", precision = 21, scale = 2)
    private BigDecimal soNgay;

    /**
     * trangThai:\n0: mới vào khoa, chờ khám;\n1: điều trị, khám bệnh có giường;\n2: điều trị, khám bệnh không giường;\n3: xuất viện, hoàn tất khám;\n4: Chuyển khoa phòng khám\n5: Chuyển viện, chuyển tuyến;\n6: Nhập viện,\n7: Tử vong\n8: Kết thúc đợt ĐT, thêm đợt ĐT mới;
     */
    @Column(name = "trang_thai")
    private Integer trangThai;

    @NotNull
    @Column(name = "nam", nullable = false)
    private Integer nam;
//
//    @ManyToOne(optional = false)
//    @NotNull
//    @JsonIgnoreProperties("thongTinKhoas")
//    private DotDieuTri bakb;
//
//    @ManyToOne(optional = false)
//    @NotNull
//    @JsonIgnoreProperties("thongTinKhoas")
//    private DotDieuTri donVi;
//
//    @ManyToOne(optional = false)
//    @NotNull
//    @JsonIgnoreProperties("thongTinKhoas")
//    private DotDieuTri benhNhan;

    @ManyToOne(optional = false)
    @JsonIgnoreProperties("thongTinKhoas")
    @JoinColumns({
        @JoinColumn(name = "dot_dieu_tri_id",referencedColumnName = "id",insertable = false, updatable = false),
        @JoinColumn(name = "bakb_Id",referencedColumnName = "bakb_id",insertable = false, updatable = false),
        @JoinColumn(name = "benh_nhan_id",referencedColumnName = "benh_nhan_id",insertable = false, updatable = false),
        @JoinColumn(name = "don_vi_id",referencedColumnName = "don_vi_id",insertable = false, updatable = false)
    })
    private DotDieuTri dotDieuTri;

    @Id
    @Column(name = "bakb_Id")
    private Long bakbId;
    @Id
    @Column(name = "dot_dieu_tri_id")
    private Long dotDieuTriId;
    @Id
    @Column(name = "benh_nhan_id")
    private Long benhNhanId;
    @Id
    @Column(name = "don_vi_id")
    private Long donViId;
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getThoiGianNhanBenh() {
        return thoiGianNhanBenh;
    }

    public ThongTinKhoa thoiGianNhanBenh(LocalDate thoiGianNhanBenh) {
        this.thoiGianNhanBenh = thoiGianNhanBenh;
        return this;
    }

    public void setThoiGianNhanBenh(LocalDate thoiGianNhanBenh) {
        this.thoiGianNhanBenh = thoiGianNhanBenh;
    }

    public LocalDate getThoiGianChuyenKhoa() {
        return thoiGianChuyenKhoa;
    }

    public ThongTinKhoa thoiGianChuyenKhoa(LocalDate thoiGianChuyenKhoa) {
        this.thoiGianChuyenKhoa = thoiGianChuyenKhoa;
        return this;
    }

    public void setThoiGianChuyenKhoa(LocalDate thoiGianChuyenKhoa) {
        this.thoiGianChuyenKhoa = thoiGianChuyenKhoa;
    }

    public Integer getKhoaId() {
        return khoaId;
    }

    public ThongTinKhoa khoaId(Integer khoaId) {
        this.khoaId = khoaId;
        return this;
    }

    public void setKhoaId(Integer khoaId) {
        this.khoaId = khoaId;
    }

    public Integer getKhoaChuyenDen() {
        return khoaChuyenDen;
    }

    public ThongTinKhoa khoaChuyenDen(Integer khoaChuyenDen) {
        this.khoaChuyenDen = khoaChuyenDen;
        return this;
    }

    public void setKhoaChuyenDen(Integer khoaChuyenDen) {
        this.khoaChuyenDen = khoaChuyenDen;
    }

    public Integer getKhoaChuyenDi() {
        return khoaChuyenDi;
    }

    public ThongTinKhoa khoaChuyenDi(Integer khoaChuyenDi) {
        this.khoaChuyenDi = khoaChuyenDi;
        return this;
    }

    public void setKhoaChuyenDi(Integer khoaChuyenDi) {
        this.khoaChuyenDi = khoaChuyenDi;
    }

    public Integer getNhanVienNhanBenh() {
        return nhanVienNhanBenh;
    }

    public ThongTinKhoa nhanVienNhanBenh(Integer nhanVienNhanBenh) {
        this.nhanVienNhanBenh = nhanVienNhanBenh;
        return this;
    }

    public void setNhanVienNhanBenh(Integer nhanVienNhanBenh) {
        this.nhanVienNhanBenh = nhanVienNhanBenh;
    }

    public Integer getNhanVienChuyenKhoa() {
        return nhanVienChuyenKhoa;
    }

    public ThongTinKhoa nhanVienChuyenKhoa(Integer nhanVienChuyenKhoa) {
        this.nhanVienChuyenKhoa = nhanVienChuyenKhoa;
        return this;
    }

    public void setNhanVienChuyenKhoa(Integer nhanVienChuyenKhoa) {
        this.nhanVienChuyenKhoa = nhanVienChuyenKhoa;
    }

    public BigDecimal getSoNgay() {
        return soNgay;
    }

    public ThongTinKhoa soNgay(BigDecimal soNgay) {
        this.soNgay = soNgay;
        return this;
    }

    public void setSoNgay(BigDecimal soNgay) {
        this.soNgay = soNgay;
    }

    public Integer getTrangThai() {
        return trangThai;
    }

    public ThongTinKhoa trangThai(Integer trangThai) {
        this.trangThai = trangThai;
        return this;
    }

    public void setTrangThai(Integer trangThai) {
        this.trangThai = trangThai;
    }

    public Integer getNam() {
        return nam;
    }

    public ThongTinKhoa nam(Integer nam) {
        this.nam = nam;
        return this;
    }

    public void setNam(Integer nam) {
        this.nam = nam;
    }

    public DotDieuTri getDotDieuTri() {
        return dotDieuTri;
    }

    public void setDotDieuTri(DotDieuTri dotDieuTri) {
        this.dotDieuTri = dotDieuTri;
    }
    //    public DotDieuTri getBakb() {
//        return bakb;
//    }
//
//    public ThongTinKhoa bakb(DotDieuTri dotDieuTri) {
//        this.bakb = dotDieuTri;
//        return this;
//    }

//    public void setBakb(DotDieuTri dotDieuTri) {
//        this.bakb = dotDieuTri;
//    }
//
//    public DotDieuTri getDonVi() {
//        return donVi;
//    }
//
//    public ThongTinKhoa donVi(DotDieuTri dotDieuTri) {
//        this.donVi = dotDieuTri;
//        return this;
//    }

//    public void setDonVi(DotDieuTri dotDieuTri) {
//        this.donVi = dotDieuTri;
//    }
//
//    public DotDieuTri getBenhNhan() {
//        return benhNhan;
//    }
//
//    public ThongTinKhoa benhNhan(DotDieuTri dotDieuTri) {
//        this.benhNhan = dotDieuTri;
//        return this;
//    }
//
//    public void setBenhNhan(DotDieuTri dotDieuTri) {
//        this.benhNhan = dotDieuTri;
//    }

//    public DotDieuTri getDotDieuTri() {
//        return dotDieuTri;
//    }
//
//    public ThongTinKhoa dotDieuTri(DotDieuTri dotDieuTri) {
//        this.dotDieuTri = dotDieuTri;
//        return this;
//    }
//
//    public void setDotDieuTri(DotDieuTri dotDieuTri) {
//        this.dotDieuTri = dotDieuTri;
//    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove


    public Long getBakbId() {
        return bakbId;
    }

    public void setBakbId(Long bakbId) {
        this.bakbId = bakbId;
    }

    public Long getDotDieuTriId() {
        return dotDieuTriId;
    }

    public void setDotDieuTriId(Long dotDieuTriId) {
        this.dotDieuTriId = dotDieuTriId;
    }

    public Long getBenhNhanId() {
        return benhNhanId;
    }

    public void setBenhNhanId(Long benhNhanId) {
        this.benhNhanId = benhNhanId;
    }

    public Long getDonViId() {
        return donViId;
    }

    public void setDonViId(Long donViId) {
        this.donViId = donViId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ThongTinKhoa)) {
            return false;
        }
        return id != null && id.equals(((ThongTinKhoa) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ThongTinKhoa{" +
            "id=" + getId() +
            ", thoiGianNhanBenh='" + getThoiGianNhanBenh() + "'" +
            ", thoiGianChuyenKhoa='" + getThoiGianChuyenKhoa() + "'" +
            ", khoaId=" + getKhoaId() +
            ", khoaChuyenDen=" + getKhoaChuyenDen() +
            ", khoaChuyenDi=" + getKhoaChuyenDi() +
            ", nhanVienNhanBenh=" + getNhanVienNhanBenh() +
            ", nhanVienChuyenKhoa=" + getNhanVienChuyenKhoa() +
            ", soNgay=" + getSoNgay() +
            ", trangThai=" + getTrangThai() +
            ", nam=" + getNam() +
            "}";
    }
}

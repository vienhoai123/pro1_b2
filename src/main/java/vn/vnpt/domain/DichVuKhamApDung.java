package vn.vnpt.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;
import java.math.BigDecimal;

/**
 * Danh mục dịch vụ khám đã được map với đợt thay đổi mã và thay đổi giá
 */
@Entity
@Table(name = "dvkham_ap_dung")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class DichVuKhamApDung implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Trạng thái có hiệu lực: 1: Có. 0: Hết hiệu lực
     */
    @Column(name = "enabled")
    private Integer enabled;

    @NotNull
    @Column(name = "gia_bhyt", precision = 21, scale = 2, nullable = false)
    private BigDecimal giaBhyt;

    @NotNull
    @Column(name = "gia_khong_bhyt", precision = 21, scale = 2, nullable = false)
    private BigDecimal giaKhongBhyt;

    @Size(max = 255)
    @Column(name = "ma_bao_cao_bhyt", length = 255)
    private String maBaoCaoBhyt;

    @Size(max = 255)
    @Column(name = "ma_bao_cao_byt", length = 255)
    private String maBaoCaoByt;

    @Size(max = 255)
    @Column(name = "so_cong_van_bhxh", length = 255)
    private String soCongVanBhxh;

    @Size(max = 255)
    @Column(name = "so_quyet_dinh", length = 255)
    private String soQuyetDinh;

    @Size(max = 500)
    @Column(name = "ten_bao_cao_bhxh", length = 500)
    private String tenBaoCaoBhxh;

    @Size(max = 500)
    @Column(name = "ten_dich_vu_khong_bao_hiem", length = 500)
    private String tenDichVuKhongBaoHiem;

    @NotNull
    @Column(name = "tien_benh_nhan_chi", precision = 21, scale = 2, nullable = false)
    private BigDecimal tienBenhNhanChi;

    @NotNull
    @Column(name = "tien_bhxh_chi", precision = 21, scale = 2, nullable = false)
    private BigDecimal tienBhxhChi;

    @NotNull
    @Column(name = "tien_ngoai_bhyt", precision = 21, scale = 2, nullable = false)
    private BigDecimal tienNgoaiBhyt;

    @NotNull
    @Column(name = "tong_tien_thanh_toan", precision = 21, scale = 2, nullable = false)
    private BigDecimal tongTienThanhToan;

    @Column(name = "ty_le_bhxh_thanh_toan")
    private Integer tyLeBhxhThanhToan;

    /**
     * Đối tượng đặc biệt có bảng giá khác. 0: Bình thường. 1: được quy định
     */
    @Column(name = "doi_tuong_dac_biet")
    private Integer doiTuongDacBiet;

    /**
     * Nguồn chi bảo hiểm cho bệnh nhân: 1: BHYT. 2: Nhà nước. 3: Khác
     */
    @Column(name = "nguon_chi")
    private Integer nguonChi;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("dichVuKhamApDungs")
    @JoinColumn(name = "dot_gia_dv_bhxh_id")
    private DotGiaDichVuBhxh dotGiaDichVuBhxh;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("dichVuKhamApDungs")
    private DichVuKham dichVuKham;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove

    public Long getId() {
        return id;
    }

    public DichVuKhamApDung id(Long id) {
        this.id = id;
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getEnabled() {
        return enabled;
    }

    public DichVuKhamApDung enabled(Integer enabled) {
        this.enabled = enabled;
        return this;
    }

    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }

    public BigDecimal getGiaBhyt() {
        return giaBhyt;
    }

    public DichVuKhamApDung giaBhyt(BigDecimal giaBhyt) {
        this.giaBhyt = giaBhyt;
        return this;
    }

    public void setGiaBhyt(BigDecimal giaBhyt) {
        this.giaBhyt = giaBhyt;
    }

    public BigDecimal getGiaKhongBhyt() {
        return giaKhongBhyt;
    }

    public DichVuKhamApDung giaKhongBhyt(BigDecimal giaKhongBhyt) {
        this.giaKhongBhyt = giaKhongBhyt;
        return this;
    }

    public void setGiaKhongBhyt(BigDecimal giaKhongBhyt) {
        this.giaKhongBhyt = giaKhongBhyt;
    }

    public String getMaBaoCaoBhyt() {
        return maBaoCaoBhyt;
    }

    public DichVuKhamApDung maBaoCaoBhyt(String maBaoCaoBhyt) {
        this.maBaoCaoBhyt = maBaoCaoBhyt;
        return this;
    }

    public void setMaBaoCaoBhyt(String maBaoCaoBhyt) {
        this.maBaoCaoBhyt = maBaoCaoBhyt;
    }

    public String getMaBaoCaoByt() {
        return maBaoCaoByt;
    }

    public DichVuKhamApDung maBaoCaoByt(String maBaoCaoByt) {
        this.maBaoCaoByt = maBaoCaoByt;
        return this;
    }

    public void setMaBaoCaoByt(String maBaoCaoByt) {
        this.maBaoCaoByt = maBaoCaoByt;
    }

    public String getSoCongVanBhxh() {
        return soCongVanBhxh;
    }

    public DichVuKhamApDung soCongVanBhxh(String soCongVanBhxh) {
        this.soCongVanBhxh = soCongVanBhxh;
        return this;
    }

    public void setSoCongVanBhxh(String soCongVanBhxh) {
        this.soCongVanBhxh = soCongVanBhxh;
    }

    public String getSoQuyetDinh() {
        return soQuyetDinh;
    }

    public DichVuKhamApDung soQuyetDinh(String soQuyetDinh) {
        this.soQuyetDinh = soQuyetDinh;
        return this;
    }

    public void setSoQuyetDinh(String soQuyetDinh) {
        this.soQuyetDinh = soQuyetDinh;
    }

    public String getTenBaoCaoBhxh() {
        return tenBaoCaoBhxh;
    }

    public DichVuKhamApDung tenBaoCaoBhxh(String tenBaoCaoBhxh) {
        this.tenBaoCaoBhxh = tenBaoCaoBhxh;
        return this;
    }

    public void setTenBaoCaoBhxh(String tenBaoCaoBhxh) {
        this.tenBaoCaoBhxh = tenBaoCaoBhxh;
    }

    public String getTenDichVuKhongBaoHiem() {
        return tenDichVuKhongBaoHiem;
    }

    public DichVuKhamApDung tenDichVuKhongBaoHiem(String tenDichVuKhongBaoHiem) {
        this.tenDichVuKhongBaoHiem = tenDichVuKhongBaoHiem;
        return this;
    }

    public void setTenDichVuKhongBaoHiem(String tenDichVuKhongBaoHiem) {
        this.tenDichVuKhongBaoHiem = tenDichVuKhongBaoHiem;
    }

    public BigDecimal getTienBenhNhanChi() {
        return tienBenhNhanChi;
    }

    public DichVuKhamApDung tienBenhNhanChi(BigDecimal tienBenhNhanChi) {
        this.tienBenhNhanChi = tienBenhNhanChi;
        return this;
    }

    public void setTienBenhNhanChi(BigDecimal tienBenhNhanChi) {
        this.tienBenhNhanChi = tienBenhNhanChi;
    }

    public BigDecimal getTienBhxhChi() {
        return tienBhxhChi;
    }

    public DichVuKhamApDung tienBhxhChi(BigDecimal tienBhxhChi) {
        this.tienBhxhChi = tienBhxhChi;
        return this;
    }

    public void setTienBhxhChi(BigDecimal tienBhxhChi) {
        this.tienBhxhChi = tienBhxhChi;
    }

    public BigDecimal getTienNgoaiBhyt() {
        return tienNgoaiBhyt;
    }

    public DichVuKhamApDung tienNgoaiBhyt(BigDecimal tienNgoaiBhyt) {
        this.tienNgoaiBhyt = tienNgoaiBhyt;
        return this;
    }

    public void setTienNgoaiBhyt(BigDecimal tienNgoaiBhyt) {
        this.tienNgoaiBhyt = tienNgoaiBhyt;
    }

    public BigDecimal getTongTienThanhToan() {
        return tongTienThanhToan;
    }

    public DichVuKhamApDung tongTienThanhToan(BigDecimal tongTienThanhToan) {
        this.tongTienThanhToan = tongTienThanhToan;
        return this;
    }

    public void setTongTienThanhToan(BigDecimal tongTienThanhToan) {
        this.tongTienThanhToan = tongTienThanhToan;
    }

    public Integer getTyLeBhxhThanhToan() {
        return tyLeBhxhThanhToan;
    }

    public DichVuKhamApDung tyLeBhxhThanhToan(Integer tyLeBhxhThanhToan) {
        this.tyLeBhxhThanhToan = tyLeBhxhThanhToan;
        return this;
    }

    public void setTyLeBhxhThanhToan(Integer tyLeBhxhThanhToan) {
        this.tyLeBhxhThanhToan = tyLeBhxhThanhToan;
    }

    public Integer getDoiTuongDacBiet() {
        return doiTuongDacBiet;
    }

    public DichVuKhamApDung doiTuongDacBiet(Integer doiTuongDacBiet) {
        this.doiTuongDacBiet = doiTuongDacBiet;
        return this;
    }

    public void setDoiTuongDacBiet(Integer doiTuongDacBiet) {
        this.doiTuongDacBiet = doiTuongDacBiet;
    }

    public Integer getNguonChi() {
        return nguonChi;
    }

    public DichVuKhamApDung nguonChi(Integer nguonChi) {
        this.nguonChi = nguonChi;
        return this;
    }

    public void setNguonChi(Integer nguonChi) {
        this.nguonChi = nguonChi;
    }

    public DotGiaDichVuBhxh getDotGiaDichVuBhxh() {
        return dotGiaDichVuBhxh;
    }

    public DichVuKhamApDung dotGiaDichVuBhxh(DotGiaDichVuBhxh dotGiaDichVuBhxh) {
        this.dotGiaDichVuBhxh = dotGiaDichVuBhxh;
        return this;
    }

    public void setDotGiaDichVuBhxh(DotGiaDichVuBhxh dotGiaDichVuBhxh) {
        this.dotGiaDichVuBhxh = dotGiaDichVuBhxh;
    }

    public DichVuKham getDichVuKham() {
        return dichVuKham;
    }

    public DichVuKhamApDung dichVuKham(DichVuKham dichVuKham) {
        this.dichVuKham = dichVuKham;
        return this;
    }

    public void setDichVuKham(DichVuKham dichVuKham) {
        this.dichVuKham = dichVuKham;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DichVuKhamApDung)) {
            return false;
        }
        return id != null && id.equals(((DichVuKhamApDung) o).id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "DichVuKhamApDung{" +
            ", id=" + getId() +
            ", enabled=" + getEnabled() +
            ", giaBhyt=" + getGiaBhyt() +
            ", giaKhongBhyt=" + getGiaKhongBhyt() +
            ", maBaoCaoBhyt='" + getMaBaoCaoBhyt() + "'" +
            ", maBaoCaoByt='" + getMaBaoCaoByt() + "'" +
            ", soCongVanBhxh='" + getSoCongVanBhxh() + "'" +
            ", soQuyetDinh='" + getSoQuyetDinh() + "'" +
            ", tenBaoCaoBhxh='" + getTenBaoCaoBhxh() + "'" +
            ", tenDichVuKhongBaoHiem='" + getTenDichVuKhongBaoHiem() + "'" +
            ", tienBenhNhanChi=" + getTienBenhNhanChi() +
            ", tienBhxhChi=" + getTienBhxhChi() +
            ", tienNgoaiBhyt=" + getTienNgoaiBhyt() +
            ", tongTienThanhToan=" + getTongTienThanhToan() +
            ", tyLeBhxhThanhToan=" + getTyLeBhxhThanhToan() +
            ", doiTuongDacBiet=" + getDoiTuongDacBiet() +
            ", nguonChi=" + getNguonChi() +
            "}";
    }
}

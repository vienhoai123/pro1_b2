package vn.vnpt.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * A TheBhyt.
 */
@Entity
@Table(name = "the_bhyt")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class TheBhyt implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * Trạng thái có hiệu lực của dịch vụ:\n1: Còn hiệu lực.\n0: Đã ẩn.
     */
    @NotNull
    @Column(name = "enabled", nullable = false)
    private Boolean enabled;

    /**
     * Mã khu vực
     */
    @Size(max = 500)
    @Column(name = "ma_khu_vuc", length = 500)
    private String maKhuVuc;

    /**
     * Mã số BHXH
     */
    @Size(max = 30)
    @Column(name = "ma_so_bhxh", length = 30)
    private String maSoBhxh;

    /**
     * Mã thẻ cũ
     */
    @Size(max = 30)
    @Column(name = "ma_the_cu", length = 30)
    private String maTheCu;

    /**
     * Ngày bắt đầu có hiệu lực của thẻ
     */
    @NotNull
    @Column(name = "ngay_bat_dau", nullable = false)
    private LocalDate ngayBatDau;

    /**
     * Ngày hưởng quyền lợi đủ 5 năm
     */
    @NotNull
    @Column(name = "ngay_du_nam_nam", nullable = false)
    private LocalDate ngayDuNamNam;

    /**
     * Ngày thẻ hết hạn
     */
    @NotNull
    @Column(name = "ngay_het_han", nullable = false)
    private LocalDate ngayHetHan;

    /**
     * Mã QR
     */
    @Size(max = 1000)
    @Column(name = "qr", length = 1000)
    private String qr;

    /**
     * Mã số thẻ
     */
    @NotNull
    @Size(max = 30)
    @Column(name = "so_the", length = 30, nullable = false)
    private String soThe;

    /**
     * Ngày miễn cũng chi trả
     */
    @Column(name = "ngay_mien_cung_chi_tra")
    private LocalDate ngayMienCungChiTra;

    /**
     * Mã bệnh nhân
     */
    @ManyToOne
    @JsonIgnoreProperties("theBhyts")
    private BenhNhan benhNhan;

    /**
     * Mã đối tượng BHYT
     */
    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("theBhyts")
    private DoiTuongBhyt doiTuongBhyt;

    /**
     * Mã thông tin BHXH
     */
    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("theBhyts")
    private ThongTinBhxh thongTinBhxh;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public TheBhyt enabled(Boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getMaKhuVuc() {
        return maKhuVuc;
    }

    public TheBhyt maKhuVuc(String maKhuVuc) {
        this.maKhuVuc = maKhuVuc;
        return this;
    }

    public void setMaKhuVuc(String maKhuVuc) {
        this.maKhuVuc = maKhuVuc;
    }

    public String getMaSoBhxh() {
        return maSoBhxh;
    }

    public TheBhyt maSoBhxh(String maSoBhxh) {
        this.maSoBhxh = maSoBhxh;
        return this;
    }

    public void setMaSoBhxh(String maSoBhxh) {
        this.maSoBhxh = maSoBhxh;
    }

    public String getMaTheCu() {
        return maTheCu;
    }

    public TheBhyt maTheCu(String maTheCu) {
        this.maTheCu = maTheCu;
        return this;
    }

    public void setMaTheCu(String maTheCu) {
        this.maTheCu = maTheCu;
    }

    public LocalDate getNgayBatDau() {
        return ngayBatDau;
    }

    public TheBhyt ngayBatDau(LocalDate ngayBatDau) {
        this.ngayBatDau = ngayBatDau;
        return this;
    }

    public void setNgayBatDau(LocalDate ngayBatDau) {
        this.ngayBatDau = ngayBatDau;
    }

    public LocalDate getNgayDuNamNam() {
        return ngayDuNamNam;
    }

    public TheBhyt ngayDuNamNam(LocalDate ngayDuNamNam) {
        this.ngayDuNamNam = ngayDuNamNam;
        return this;
    }

    public void setNgayDuNamNam(LocalDate ngayDuNamNam) {
        this.ngayDuNamNam = ngayDuNamNam;
    }

    public LocalDate getNgayHetHan() {
        return ngayHetHan;
    }

    public TheBhyt ngayHetHan(LocalDate ngayHetHan) {
        this.ngayHetHan = ngayHetHan;
        return this;
    }

    public void setNgayHetHan(LocalDate ngayHetHan) {
        this.ngayHetHan = ngayHetHan;
    }

    public String getQr() {
        return qr;
    }

    public TheBhyt qr(String qr) {
        this.qr = qr;
        return this;
    }

    public void setQr(String qr) {
        this.qr = qr;
    }

    public String getSoThe() {
        return soThe;
    }

    public TheBhyt soThe(String soThe) {
        this.soThe = soThe;
        return this;
    }

    public void setSoThe(String soThe) {
        this.soThe = soThe;
    }

    public LocalDate getNgayMienCungChiTra() {
        return ngayMienCungChiTra;
    }

    public TheBhyt ngayMienCungChiTra(LocalDate ngayMienCungChiTra) {
        this.ngayMienCungChiTra = ngayMienCungChiTra;
        return this;
    }

    public void setNgayMienCungChiTra(LocalDate ngayMienCungChiTra) {
        this.ngayMienCungChiTra = ngayMienCungChiTra;
    }

    public BenhNhan getBenhNhan() {
        return benhNhan;
    }

    public TheBhyt benhNhan(BenhNhan benhNhan) {
        this.benhNhan = benhNhan;
        return this;
    }

    public void setBenhNhan(BenhNhan benhNhan) {
        this.benhNhan = benhNhan;
    }

    public DoiTuongBhyt getDoiTuongBhyt() {
        return doiTuongBhyt;
    }

    public TheBhyt doiTuongBhyt(DoiTuongBhyt doiTuongBhyt) {
        this.doiTuongBhyt = doiTuongBhyt;
        return this;
    }

    public void setDoiTuongBhyt(DoiTuongBhyt doiTuongBhyt) {
        this.doiTuongBhyt = doiTuongBhyt;
    }

    public ThongTinBhxh getThongTinBhxh() {
        return thongTinBhxh;
    }

    public TheBhyt thongTinBhxh(ThongTinBhxh thongTinBhxh) {
        this.thongTinBhxh = thongTinBhxh;
        return this;
    }

    public void setThongTinBhxh(ThongTinBhxh thongTinBhxh) {
        this.thongTinBhxh = thongTinBhxh;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TheBhyt)) {
            return false;
        }
        return id != null && id.equals(((TheBhyt) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "TheBhyt{" +
            "id=" + getId() +
            ", enabled='" + isEnabled() + "'" +
            ", maKhuVuc='" + getMaKhuVuc() + "'" +
            ", maSoBhxh='" + getMaSoBhxh() + "'" +
            ", maTheCu='" + getMaTheCu() + "'" +
            ", ngayBatDau='" + getNgayBatDau() + "'" +
            ", ngayDuNamNam='" + getNgayDuNamNam() + "'" +
            ", ngayHetHan='" + getNgayHetHan() + "'" +
            ", qr='" + getQr() + "'" +
            ", soThe='" + getSoThe() + "'" +
            ", ngayMienCungChiTra='" + getNgayMienCungChiTra() + "'" +
            "}";
    }
}

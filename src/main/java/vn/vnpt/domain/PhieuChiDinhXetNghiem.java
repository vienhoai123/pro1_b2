package vn.vnpt.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A PhieuChiDinhXetNghiem.
 */
@Entity
@Table(name = "pcd_xn")
@IdClass(PhieuChiDinhXetNghiemId.class)
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PhieuChiDinhXetNghiem implements Serializable {

//    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name="id")
    private Long id;

    @Id
    @Column(name = "bakb_id")
    private Long bakbId;

    @Id
    @Column(name = "benh_nhan_id")
    private Long benhNhanId;

    @Id
    @Column(name = "don_vi_id")
    private Long donViId;


    /**
     * Chẩn đoán tổng quát
     */
    @Size(max = 255)
    @Column(name = "chan_doan_tong_quat", length = 255)
    private String chanDoanTongQuat;

    /**
     * Ghi chú
     */
    @Size(max = 255)
    @Column(name = "ghi_chu", length = 255)
    private String ghiChu;

    /**
     * Kết quả tổng quát
     */
    @Size(max = 255)
    @Column(name = "ket_qua_tong_quat", length = 255)
    private String ketQuaTongQuat;

    @NotNull
    @Column(name = "nam", nullable = false)
    private Integer nam;

//    @ManyToOne(optional = false)
//    @JoinColumn(insertable = false, updatable = false)
//    @JsonIgnoreProperties("phieuCDXNS")
//    private BenhAnKhamBenh benhNhan;
//
//    @ManyToOne(optional = false)
//    @JoinColumn(insertable = false, updatable = false)
//    @JsonIgnoreProperties("phieuCDXNS")
//    private BenhAnKhamBenh donVi;

    @ManyToOne(optional = false)
    @JoinColumns(
        {
            @JoinColumn(name = "bakb_id", referencedColumnName = "id",insertable = false, updatable = false),
            @JoinColumn(name = "benh_nhan_id", referencedColumnName = "benh_nhan_id", insertable = false, updatable = false),
            @JoinColumn(name = "don_vi_id", referencedColumnName = "don_vi_id", insertable = false,updatable = false)
        })
    @JsonIgnoreProperties("phieuCDXNS")
    private BenhAnKhamBenh bakb;

    public BenhAnKhamBenh getBakb() {
        return bakb;
    }

    public void setBakb(BenhAnKhamBenh bakb) {
        this.bakb = bakb;
    }

    public Long getBakbId() {
        return bakbId;
    }

    public void setBakbId(Long bakbId) {
        this.bakbId = bakbId;
    }

    public Long getBenhNhanId() {
        return benhNhanId;
    }

    public void setBenhNhanId(Long benhNhanId) {
        this.benhNhanId = benhNhanId;
    }

    public Long getDonViId() {
        return donViId;
    }

    public void setDonViId(Long donViId) {
        this.donViId = donViId;
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getChanDoanTongQuat() {
        return chanDoanTongQuat;
    }

    public PhieuChiDinhXetNghiem chanDoanTongQuat(String chanDoanTongQuat) {
        this.chanDoanTongQuat = chanDoanTongQuat;
        return this;
    }

    public void setChanDoanTongQuat(String chanDoanTongQuat) {
        this.chanDoanTongQuat = chanDoanTongQuat;
    }

    public String getGhiChu() {
        return ghiChu;
    }

    public PhieuChiDinhXetNghiem ghiChu(String ghiChu) {
        this.ghiChu = ghiChu;
        return this;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    public String getKetQuaTongQuat() {
        return ketQuaTongQuat;
    }

    public PhieuChiDinhXetNghiem ketQuaTongQuat(String ketQuaTongQuat) {
        this.ketQuaTongQuat = ketQuaTongQuat;
        return this;
    }

    public void setKetQuaTongQuat(String ketQuaTongQuat) {
        this.ketQuaTongQuat = ketQuaTongQuat;
    }

    public Integer getNam() {
        return nam;
    }

    public PhieuChiDinhXetNghiem nam(Integer nam) {
        this.nam = nam;
        return this;
    }

    public void setNam(Integer nam) {
        this.nam = nam;
    }

//    public BenhAnKhamBenh getBenhNhan() {
//        return benhNhan;
//    }
//
//    public PhieuChiDinhXetNghiem benhNhan(BenhAnKhamBenh benhAnKhamBenh) {
//        this.benhNhan = benhAnKhamBenh;
//        return this;
//    }
//
//    public void setBenhNhan(BenhAnKhamBenh benhAnKhamBenh) {
//        this.benhNhan = benhAnKhamBenh;
//    }
//
//    public BenhAnKhamBenh getDonVi() {
//        return donVi;
//    }
//
//    public PhieuChiDinhXetNghiem donVi(BenhAnKhamBenh benhAnKhamBenh) {
//        this.donVi = benhAnKhamBenh;
//        return this;
//    }
//
//    public void setDonVi(BenhAnKhamBenh benhAnKhamBenh) {
//        this.donVi = benhAnKhamBenh;
//    }

//    public BenhAnKhamBenh getBakb() {
//        return bakb;
//    }
//
//    public PhieuChiDinhXetNghiem bakb(BenhAnKhamBenh benhAnKhamBenh) {
//        this.bakb = benhAnKhamBenh;
//        return this;
//    }
//
//    public void setBakb(BenhAnKhamBenh benhAnKhamBenh) {
//        this.bakb = benhAnKhamBenh;
//    }
//    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PhieuChiDinhXetNghiem)) return false;
        PhieuChiDinhXetNghiem that = (PhieuChiDinhXetNghiem) o;
        return Objects.equals(getId(), that.getId()) &&
            Objects.equals(getBakbId(), that.getBakbId()) &&
            Objects.equals(getBenhNhanId(), that.getBenhNhanId()) &&
            Objects.equals(getDonViId(), that.getDonViId()) &&
            Objects.equals(getChanDoanTongQuat(), that.getChanDoanTongQuat()) &&
            Objects.equals(getGhiChu(), that.getGhiChu()) &&
            Objects.equals(getKetQuaTongQuat(), that.getKetQuaTongQuat()) &&
            Objects.equals(getNam(), that.getNam());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getBakbId(), getBenhNhanId(), getDonViId(), getChanDoanTongQuat(), getGhiChu(), getKetQuaTongQuat(), getNam());
    }

    @Override
    public String toString() {
        return "PhieuChiDinhXetNghiem{" +
            "id=" + id +
            ", bakbId=" + bakbId +
            ", benhNhanId=" + benhNhanId +
            ", donViId=" + donViId +
            ", chanDoanTongQuat='" + chanDoanTongQuat + '\'' +
            ", ghiChu='" + ghiChu + '\'' +
            ", ketQuaTongQuat='" + ketQuaTongQuat + '\'' +
            ", nam=" + nam +
            '}';
    }
}

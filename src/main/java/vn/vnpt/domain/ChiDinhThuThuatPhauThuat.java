package vn.vnpt.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;
import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * A ChiDinhThuThuatPhauThuat.
 */
@Entity
@Table(name = "chi_dinh_thu_thuat_phau_thuat")
@IdClass(ChiDinhTTPTId.class)
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ChiDinhThuThuatPhauThuat implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;


    @Id
    @Column(name = "bakb_id")
    private Long bakbId;

    @Id
    @Column(name = "benh_nhan_id")
    private Long benhNhanId;

    @Id
    @Column(name = "don_vi_id")
    private Long donViId;

    @Id
    @NotNull
    @Column(name = "phieu_cd_id")
    private Long phieuCDId;
    /**
     * Trạng thái được thanh toán bảo hiểm của chỉ định: \r\n1: Có bảo hiểm. \r\n0: Không bảo hiểm. \r\n
     */
    @NotNull
    @Column(name = "co_bao_hiem", nullable = false)
    private Boolean coBaoHiem;

    /**
     * Trạng thái có kết quả của chỉ định: \r\n1: Có kết quả. \r\n0: Chưa có kết quả. \r\n
     */
    @NotNull
    @Column(name = "co_ket_qua", nullable = false)
    private Boolean coKetQua;

    /**
     * Trạng thái đã thanh toán của chỉ định: \r\n0: Chưa thanh toán. \r\n1: Đã thanh toán. \r\n-1: Đã hủy thanh toán
     */
    @NotNull
    @Column(name = "da_thanh_toan", nullable = false)
    private Integer daThanhToan;

    /**
     * Trạng thái đã thanh toán chênh lệch của chỉ định: \r\n0: Chưa thanh toán. \r\n1: Đã thanh toán. \r\n-1: Đã hủy thanh toán
     */
    @NotNull
    @Column(name = "da_thanh_toan_chenh_lech", nullable = false)
    private Integer daThanhToanChenhLech;

    /**
     * Trạng thái đã thực hiện của chỉ định: \r\n0: Chưa thực hiện. \r\n1: Đã thực hiện. \r\n-1: Đã hủy thực hiện
     */
    @NotNull
    @Column(name = "da_thuc_hien", nullable = false)
    private Integer daThucHien;

    /**
     * Đơn giá
     */
    @NotNull
    @Column(name = "don_gia", precision = 21, scale = 2, nullable = false)
    private BigDecimal donGia;

    /**
     * Đơn giá Bảo hiểm y tế
     */
    @NotNull
    @Column(name = "don_gia_bhyt", precision = 21, scale = 2, nullable = false)
    private BigDecimal donGiaBhyt;

    /**
     * Đơn giá không Bảo hiểm y tế
     */
    @Column(name = "don_gia_khong_bhyt", precision = 21, scale = 2)
    private BigDecimal donGiaKhongBhyt;

    /**
     * Ghi chú chỉ định
     */
    @Size(max = 2000)
    @Column(name = "ghi_chu_chi_dinh", length = 2000)
    private String ghiChuChiDinh;

    /**
     * Mô tả chỉ định
     */
    @Size(max = 2000)
    @Column(name = "mo_ta", length = 2000)
    private String moTa;

    /**
     * Mã nhân viên chỉ định
     */
    @Column(name = "nguoi_chi_dinh_id")
    private Long nguoiChiDinhId;

    /**
     * Số lượng
     */
    @Column(name = "so_luong")
    private Integer soLuong;

    /**
     * Tên chỉ định
     */
    @Size(max = 500)
    @Column(name = "ten", length = 500)
    private String ten;

    /**
     * Thành tiền
     */
    @NotNull
    @Column(name = "thanh_tien", precision = 21, scale = 2, nullable = false)
    private BigDecimal thanhTien;

    /**
     * Thành tiền BHYT
     */
    @Column(name = "thanh_tien_bhyt", precision = 21, scale = 2)
    private BigDecimal thanhTienBhyt;

    /**
     * Thành tiền không BHYT
     */
    @Column(name = "thanh_tien_khong_bhyt", precision = 21, scale = 2)
    private BigDecimal thanhTienKhongBHYT;

    /**
     * Thời gian chỉ định
     */
    @Column(name = "thoi_gian_chi_dinh")
    private LocalDate thoiGianChiDinh;

    /**
     * Thời gian tạo
     */
    @NotNull
    @Column(name = "thoi_gian_tao", nullable = false)
    private LocalDate thoiGianTao;

    /**
     * Tiền ngoài BHYT
     */
    @NotNull
    @Column(name = "tien_ngoai_bhyt", precision = 21, scale = 2, nullable = false)
    private BigDecimal tienNgoaiBHYT;

    /**
     * Trạng thái thanh toán chênh lệch: \r\n0: Không có thanh toán chênh lệch. \r\n1: Có thanh toán chênh lệch
     */
    @NotNull
    @Column(name = "thanh_toan_chenh_lech", nullable = false)
    private Boolean thanhToanChenhLech;

    /**
     * Tỷ lệ thanh toán
     */
    @Column(name = "ty_le_thanh_toan")
    private Integer tyLeThanhToan;

    /**
     * Mã dùng chung cho dịch vụ
     */
    @Size(max = 255)
    @Column(name = "ma_dung_chung", length = 255)
    private String maDungChung;

    /**
     * Trạng thái theo yêu cầu của dịch vụ: 0: Bình thường. 1: Theo yêu cầu
     */
    @Column(name = "dich_vu_yeu_cau")
    private Boolean dichVuYeuCau;

    @NotNull
    @Column(name = "nam", nullable = false)
    private Integer nam;

//    @ManyToOne(optional = false)
//    @NotNull
//    @JsonIgnoreProperties("chiDinhTTPTS")
//    private PhieuChiDinhTTPT donVi;
//
//    @ManyToOne(optional = false)
//    @NotNull
//    @JsonIgnoreProperties("chiDinhTTPTS")
//    private PhieuChiDinhTTPT benhNhan;
//
//    @ManyToOne(optional = false)
//    @NotNull
//    @JsonIgnoreProperties("chiDinhTTPTS")
//    private PhieuChiDinhTTPT bakb;

    @ManyToOne(optional = false)
    @NotNull
    @JoinColumns(
        {
            @JoinColumn(name = "phieu_cd_id", referencedColumnName = "id",insertable = false, updatable = false),
            @JoinColumn(name = "bakb_id", referencedColumnName = "bakb_id",insertable = false, updatable = false),
            @JoinColumn(name = "benh_nhan_id", referencedColumnName = "benh_nhan_id", insertable = false, updatable = false),
            @JoinColumn(name = "don_vi_id", referencedColumnName = "don_vi_id", insertable = false,updatable = false)
        })
    @JsonIgnoreProperties("chiDinhTTPTS")
    private PhieuChiDinhTTPT phieuCD;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("chiDinhTTPTS")
    private Phong phong;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("chiDinhTTPTS")
    private ThuThuatPhauThuat ttpt;

    public Long getBakbId() {
        return bakbId;
    }

    public void setBakbId(Long bakbId) {
        this.bakbId = bakbId;
    }

    public Long getBenhNhanId() {
        return benhNhanId;
    }

    public void setBenhNhanId(Long benhNhanId) {
        this.benhNhanId = benhNhanId;
    }

    public Long getDonViId() {
        return donViId;
    }

    public void setDonViId(Long donViId) {
        this.donViId = donViId;
    }

    public Long getPhieuCDId() {
        return phieuCDId;
    }

    public void setPhieuCDId(Long phieuCDId) {
        this.phieuCDId = phieuCDId;
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isCoBaoHiem() {
        return coBaoHiem;
    }

    public ChiDinhThuThuatPhauThuat coBaoHiem(Boolean coBaoHiem) {
        this.coBaoHiem = coBaoHiem;
        return this;
    }

    public void setCoBaoHiem(Boolean coBaoHiem) {
        this.coBaoHiem = coBaoHiem;
    }

    public Boolean isCoKetQua() {
        return coKetQua;
    }

    public ChiDinhThuThuatPhauThuat coKetQua(Boolean coKetQua) {
        this.coKetQua = coKetQua;
        return this;
    }

    public void setCoKetQua(Boolean coKetQua) {
        this.coKetQua = coKetQua;
    }

    public Integer getDaThanhToan() {
        return daThanhToan;
    }

    public ChiDinhThuThuatPhauThuat daThanhToan(Integer daThanhToan) {
        this.daThanhToan = daThanhToan;
        return this;
    }

    public void setDaThanhToan(Integer daThanhToan) {
        this.daThanhToan = daThanhToan;
    }

    public Integer getDaThanhToanChenhLech() {
        return daThanhToanChenhLech;
    }

    public ChiDinhThuThuatPhauThuat daThanhToanChenhLech(Integer daThanhToanChenhLech) {
        this.daThanhToanChenhLech = daThanhToanChenhLech;
        return this;
    }

    public void setDaThanhToanChenhLech(Integer daThanhToanChenhLech) {
        this.daThanhToanChenhLech = daThanhToanChenhLech;
    }

    public Integer getDaThucHien() {
        return daThucHien;
    }

    public ChiDinhThuThuatPhauThuat daThucHien(Integer daThucHien) {
        this.daThucHien = daThucHien;
        return this;
    }

    public void setDaThucHien(Integer daThucHien) {
        this.daThucHien = daThucHien;
    }

    public BigDecimal getDonGia() {
        return donGia;
    }

    public ChiDinhThuThuatPhauThuat donGia(BigDecimal donGia) {
        this.donGia = donGia;
        return this;
    }

    public void setDonGia(BigDecimal donGia) {
        this.donGia = donGia;
    }

    public BigDecimal getDonGiaBhyt() {
        return donGiaBhyt;
    }

    public ChiDinhThuThuatPhauThuat donGiaBhyt(BigDecimal donGiaBhyt) {
        this.donGiaBhyt = donGiaBhyt;
        return this;
    }

    public void setDonGiaBhyt(BigDecimal donGiaBhyt) {
        this.donGiaBhyt = donGiaBhyt;
    }

    public BigDecimal getDonGiaKhongBhyt() {
        return donGiaKhongBhyt;
    }

    public ChiDinhThuThuatPhauThuat donGiaKhongBhyt(BigDecimal donGiaKhongBhyt) {
        this.donGiaKhongBhyt = donGiaKhongBhyt;
        return this;
    }

    public void setDonGiaKhongBhyt(BigDecimal donGiaKhongBhyt) {
        this.donGiaKhongBhyt = donGiaKhongBhyt;
    }

    public String getGhiChuChiDinh() {
        return ghiChuChiDinh;
    }

    public ChiDinhThuThuatPhauThuat ghiChuChiDinh(String ghiChuChiDinh) {
        this.ghiChuChiDinh = ghiChuChiDinh;
        return this;
    }

    public void setGhiChuChiDinh(String ghiChuChiDinh) {
        this.ghiChuChiDinh = ghiChuChiDinh;
    }

    public String getMoTa() {
        return moTa;
    }

    public ChiDinhThuThuatPhauThuat moTa(String moTa) {
        this.moTa = moTa;
        return this;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public Long getNguoiChiDinhId() {
        return nguoiChiDinhId;
    }

    public ChiDinhThuThuatPhauThuat nguoiChiDinhId(Long nguoiChiDinhId) {
        this.nguoiChiDinhId = nguoiChiDinhId;
        return this;
    }

    public void setNguoiChiDinhId(Long nguoiChiDinhId) {
        this.nguoiChiDinhId = nguoiChiDinhId;
    }

    public Integer getSoLuong() {
        return soLuong;
    }

    public ChiDinhThuThuatPhauThuat soLuong(Integer soLuong) {
        this.soLuong = soLuong;
        return this;
    }

    public void setSoLuong(Integer soLuong) {
        this.soLuong = soLuong;
    }

    public String getTen() {
        return ten;
    }

    public ChiDinhThuThuatPhauThuat ten(String ten) {
        this.ten = ten;
        return this;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public BigDecimal getThanhTien() {
        return thanhTien;
    }

    public ChiDinhThuThuatPhauThuat thanhTien(BigDecimal thanhTien) {
        this.thanhTien = thanhTien;
        return this;
    }

    public void setThanhTien(BigDecimal thanhTien) {
        this.thanhTien = thanhTien;
    }

    public BigDecimal getThanhTienBhyt() {
        return thanhTienBhyt;
    }

    public ChiDinhThuThuatPhauThuat thanhTienBhyt(BigDecimal thanhTienBhyt) {
        this.thanhTienBhyt = thanhTienBhyt;
        return this;
    }

    public void setThanhTienBhyt(BigDecimal thanhTienBhyt) {
        this.thanhTienBhyt = thanhTienBhyt;
    }

    public BigDecimal getThanhTienKhongBHYT() {
        return thanhTienKhongBHYT;
    }

    public ChiDinhThuThuatPhauThuat thanhTienKhongBHYT(BigDecimal thanhTienKhongBHYT) {
        this.thanhTienKhongBHYT = thanhTienKhongBHYT;
        return this;
    }

    public void setThanhTienKhongBHYT(BigDecimal thanhTienKhongBHYT) {
        this.thanhTienKhongBHYT = thanhTienKhongBHYT;
    }

    public LocalDate getThoiGianChiDinh() {
        return thoiGianChiDinh;
    }

    public ChiDinhThuThuatPhauThuat thoiGianChiDinh(LocalDate thoiGianChiDinh) {
        this.thoiGianChiDinh = thoiGianChiDinh;
        return this;
    }

    public void setThoiGianChiDinh(LocalDate thoiGianChiDinh) {
        this.thoiGianChiDinh = thoiGianChiDinh;
    }

    public LocalDate getThoiGianTao() {
        return thoiGianTao;
    }

    public ChiDinhThuThuatPhauThuat thoiGianTao(LocalDate thoiGianTao) {
        this.thoiGianTao = thoiGianTao;
        return this;
    }

    public void setThoiGianTao(LocalDate thoiGianTao) {
        this.thoiGianTao = thoiGianTao;
    }

    public BigDecimal getTienNgoaiBHYT() {
        return tienNgoaiBHYT;
    }

    public ChiDinhThuThuatPhauThuat tienNgoaiBHYT(BigDecimal tienNgoaiBHYT) {
        this.tienNgoaiBHYT = tienNgoaiBHYT;
        return this;
    }

    public void setTienNgoaiBHYT(BigDecimal tienNgoaiBHYT) {
        this.tienNgoaiBHYT = tienNgoaiBHYT;
    }

    public Boolean isThanhToanChenhLech() {
        return thanhToanChenhLech;
    }

    public ChiDinhThuThuatPhauThuat thanhToanChenhLech(Boolean thanhToanChenhLech) {
        this.thanhToanChenhLech = thanhToanChenhLech;
        return this;
    }

    public void setThanhToanChenhLech(Boolean thanhToanChenhLech) {
        this.thanhToanChenhLech = thanhToanChenhLech;
    }

    public Integer getTyLeThanhToan() {
        return tyLeThanhToan;
    }

    public ChiDinhThuThuatPhauThuat tyLeThanhToan(Integer tyLeThanhToan) {
        this.tyLeThanhToan = tyLeThanhToan;
        return this;
    }

    public void setTyLeThanhToan(Integer tyLeThanhToan) {
        this.tyLeThanhToan = tyLeThanhToan;
    }

    public String getMaDungChung() {
        return maDungChung;
    }

    public ChiDinhThuThuatPhauThuat maDungChung(String maDungChung) {
        this.maDungChung = maDungChung;
        return this;
    }

    public void setMaDungChung(String maDungChung) {
        this.maDungChung = maDungChung;
    }

    public Boolean isDichVuYeuCau() {
        return dichVuYeuCau;
    }

    public ChiDinhThuThuatPhauThuat dichVuYeuCau(Boolean dichVuYeuCau) {
        this.dichVuYeuCau = dichVuYeuCau;
        return this;
    }

    public void setDichVuYeuCau(Boolean dichVuYeuCau) {
        this.dichVuYeuCau = dichVuYeuCau;
    }

    public Integer getNam() {
        return nam;
    }

    public ChiDinhThuThuatPhauThuat nam(Integer nam) {
        this.nam = nam;
        return this;
    }

    public void setNam(Integer nam) {
        this.nam = nam;
    }

//    public PhieuChiDinhTTPT getDonVi() {
//        return donVi;
//    }
//
//    public ChiDinhThuThuatPhauThuat donVi(PhieuChiDinhTTPT phieuChiDinhTTPT) {
//        this.donVi = phieuChiDinhTTPT;
//        return this;
//    }
//
//    public void setDonVi(PhieuChiDinhTTPT phieuChiDinhTTPT) {
//        this.donVi = phieuChiDinhTTPT;
//    }
//
//    public PhieuChiDinhTTPT getBenhNhan() {
//        return benhNhan;
//    }
//
//    public ChiDinhThuThuatPhauThuat benhNhan(PhieuChiDinhTTPT phieuChiDinhTTPT) {
//        this.benhNhan = phieuChiDinhTTPT;
//        return this;
//    }
//
//    public void setBenhNhan(PhieuChiDinhTTPT phieuChiDinhTTPT) {
//        this.benhNhan = phieuChiDinhTTPT;
//    }
//
//    public PhieuChiDinhTTPT getBakb() {
//        return bakb;
//    }
//
//    public ChiDinhThuThuatPhauThuat bakb(PhieuChiDinhTTPT phieuChiDinhTTPT) {
//        this.bakb = phieuChiDinhTTPT;
//        return this;
//    }
//
//    public void setBakb(PhieuChiDinhTTPT phieuChiDinhTTPT) {
//        this.bakb = phieuChiDinhTTPT;
//    }

    public PhieuChiDinhTTPT getPhieuCD() {
        return phieuCD;
    }

    public ChiDinhThuThuatPhauThuat phieuCD(PhieuChiDinhTTPT phieuChiDinhTTPT) {
        this.phieuCD = phieuChiDinhTTPT;
        return this;
    }

    public void setPhieuCD(PhieuChiDinhTTPT phieuChiDinhTTPT) {
        this.phieuCD = phieuChiDinhTTPT;
    }

    public Phong getPhong() {
        return phong;
    }

    public ChiDinhThuThuatPhauThuat phong(Phong phong) {
        this.phong = phong;
        return this;
    }

    public void setPhong(Phong phong) {
        this.phong = phong;
    }

    public ThuThuatPhauThuat getTtpt() {
        return ttpt;
    }

    public ChiDinhThuThuatPhauThuat ttpt(ThuThuatPhauThuat thuThuatPhauThuat) {
        this.ttpt = thuThuatPhauThuat;
        return this;
    }

    public void setTtpt(ThuThuatPhauThuat thuThuatPhauThuat) {
        this.ttpt = thuThuatPhauThuat;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ChiDinhThuThuatPhauThuat)) return false;
        ChiDinhThuThuatPhauThuat that = (ChiDinhThuThuatPhauThuat) o;
        return Objects.equals(getId(), that.getId()) &&
            Objects.equals(getBakbId(), that.getBakbId()) &&
            Objects.equals(getBenhNhanId(), that.getBenhNhanId()) &&
            Objects.equals(getDonViId(), that.getDonViId()) &&
            Objects.equals(getPhieuCDId(), that.getPhieuCDId()) &&
            Objects.equals(coBaoHiem, that.coBaoHiem) &&
            Objects.equals(coKetQua, that.coKetQua) &&
            Objects.equals(getDaThanhToan(), that.getDaThanhToan()) &&
            Objects.equals(getDaThanhToanChenhLech(), that.getDaThanhToanChenhLech()) &&
            Objects.equals(getDaThucHien(), that.getDaThucHien()) &&
            Objects.equals(getDonGia(), that.getDonGia()) &&
            Objects.equals(getDonGiaBhyt(), that.getDonGiaBhyt()) &&
            Objects.equals(getDonGiaKhongBhyt(), that.getDonGiaKhongBhyt()) &&
            Objects.equals(getGhiChuChiDinh(), that.getGhiChuChiDinh()) &&
            Objects.equals(getMoTa(), that.getMoTa()) &&
            Objects.equals(getNguoiChiDinhId(), that.getNguoiChiDinhId()) &&
            Objects.equals(getSoLuong(), that.getSoLuong()) &&
            Objects.equals(getTen(), that.getTen()) &&
            Objects.equals(getThanhTien(), that.getThanhTien()) &&
            Objects.equals(getThanhTienBhyt(), that.getThanhTienBhyt()) &&
            Objects.equals(getThanhTienKhongBHYT(), that.getThanhTienKhongBHYT()) &&
            Objects.equals(getThoiGianChiDinh(), that.getThoiGianChiDinh()) &&
            Objects.equals(getThoiGianTao(), that.getThoiGianTao()) &&
            Objects.equals(getTienNgoaiBHYT(), that.getTienNgoaiBHYT()) &&
            Objects.equals(thanhToanChenhLech, that.thanhToanChenhLech) &&
            Objects.equals(getTyLeThanhToan(), that.getTyLeThanhToan()) &&
            Objects.equals(getMaDungChung(), that.getMaDungChung()) &&
            Objects.equals(dichVuYeuCau, that.dichVuYeuCau) &&
            Objects.equals(getNam(), that.getNam()) &&
            Objects.equals(getPhieuCD(), that.getPhieuCD()) &&
            Objects.equals(getPhong(), that.getPhong()) &&
            Objects.equals(getTtpt(), that.getTtpt());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getBakbId(), getBenhNhanId(), getDonViId(), getPhieuCDId(), coBaoHiem, coKetQua, getDaThanhToan(), getDaThanhToanChenhLech(), getDaThucHien(), getDonGia(), getDonGiaBhyt(), getDonGiaKhongBhyt(), getGhiChuChiDinh(), getMoTa(), getNguoiChiDinhId(), getSoLuong(), getTen(), getThanhTien(), getThanhTienBhyt(), getThanhTienKhongBHYT(), getThoiGianChiDinh(), getThoiGianTao(), getTienNgoaiBHYT(), thanhToanChenhLech, getTyLeThanhToan(), getMaDungChung(), dichVuYeuCau, getNam(), getPhieuCD(), getPhong(), getTtpt());
    }

    @Override
    public String toString() {
        return "ChiDinhThuThuatPhauThuat{" +
            "id=" + id +
            ", bakbId=" + bakbId +
            ", benhNhanId=" + benhNhanId +
            ", donViId=" + donViId +
            ", phieuCDId=" + phieuCDId +
            ", coBaoHiem=" + coBaoHiem +
            ", coKetQua=" + coKetQua +
            ", daThanhToan=" + daThanhToan +
            ", daThanhToanChenhLech=" + daThanhToanChenhLech +
            ", daThucHien=" + daThucHien +
            ", donGia=" + donGia +
            ", donGiaBhyt=" + donGiaBhyt +
            ", donGiaKhongBhyt=" + donGiaKhongBhyt +
            ", ghiChuChiDinh='" + ghiChuChiDinh + '\'' +
            ", moTa='" + moTa + '\'' +
            ", nguoiChiDinhId=" + nguoiChiDinhId +
            ", soLuong=" + soLuong +
            ", ten='" + ten + '\'' +
            ", thanhTien=" + thanhTien +
            ", thanhTienBhyt=" + thanhTienBhyt +
            ", thanhTienKhongBHYT=" + thanhTienKhongBHYT +
            ", thoiGianChiDinh=" + thoiGianChiDinh +
            ", thoiGianTao=" + thoiGianTao +
            ", tienNgoaiBHYT=" + tienNgoaiBHYT +
            ", thanhToanChenhLech=" + thanhToanChenhLech +
            ", tyLeThanhToan=" + tyLeThanhToan +
            ", maDungChung='" + maDungChung + '\'' +
            ", dichVuYeuCau=" + dichVuYeuCau +
            ", nam=" + nam +
            ", phieuCD=" + phieuCD +
            ", phong=" + phong +
            ", ttpt=" + ttpt +
            '}';
    }
}

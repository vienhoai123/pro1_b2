package vn.vnpt.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * NDB_TABLE=READ_BACKUP=1 Thông tin theo tên dân tộc
 */
@Entity
@Table(name = "dan_toc")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class DanToc implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * Mã dân tộc
     */
    @NotNull
    @Column(name = "ma_4069_byt", nullable = false)
    private Integer ma4069Byt;

    /**
     * Mã dân tộc theo cục thống kê
     */
    @NotNull
    @Column(name = "ma_cuc_thong_ke", nullable = false)
    private Integer maCucThongKe;

    /**
     * Tên dân tộc
     */
    @NotNull
    @Size(max = 500)
    @Column(name = "ten_4069_byt", length = 500, nullable = false)
    private String ten4069Byt;

    /**
     * Tên dân tộc theo cục thống kê
     */
    @NotNull
    @Size(max = 500)
    @Column(name = "ten_cuc_thong_ke", length = 500, nullable = false)
    private String tenCucThongKe;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getMa4069Byt() {
        return ma4069Byt;
    }

    public DanToc ma4069Byt(Integer ma4069Byt) {
        this.ma4069Byt = ma4069Byt;
        return this;
    }

    public void setMa4069Byt(Integer ma4069Byt) {
        this.ma4069Byt = ma4069Byt;
    }

    public Integer getMaCucThongKe() {
        return maCucThongKe;
    }

    public DanToc maCucThongKe(Integer maCucThongKe) {
        this.maCucThongKe = maCucThongKe;
        return this;
    }

    public void setMaCucThongKe(Integer maCucThongKe) {
        this.maCucThongKe = maCucThongKe;
    }

    public String getTen4069Byt() {
        return ten4069Byt;
    }

    public DanToc ten4069Byt(String ten4069Byt) {
        this.ten4069Byt = ten4069Byt;
        return this;
    }

    public void setTen4069Byt(String ten4069Byt) {
        this.ten4069Byt = ten4069Byt;
    }

    public String getTenCucThongKe() {
        return tenCucThongKe;
    }

    public DanToc tenCucThongKe(String tenCucThongKe) {
        this.tenCucThongKe = tenCucThongKe;
        return this;
    }

    public void setTenCucThongKe(String tenCucThongKe) {
        this.tenCucThongKe = tenCucThongKe;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DanToc)) {
            return false;
        }
        return id != null && id.equals(((DanToc) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "DanToc{" +
            "id=" + getId() +
            ", ma4069Byt=" + getMa4069Byt() +
            ", maCucThongKe=" + getMaCucThongKe() +
            ", ten4069Byt='" + getTen4069Byt() + "'" +
            ", tenCucThongKe='" + getTenCucThongKe() + "'" +
            "}";
    }
}

package vn.vnpt.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;
import java.math.BigDecimal;

/**
 * A ThuThuatPhauThuat.
 */
@Entity
@Table(name = "thu_thuat_phau_thuat")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ThuThuatPhauThuat implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * Trạng thái xóa bỏ của dòng dữ liệu. \r\n1: Đã xóa. \r\n0: Còn hiệu lực.
     */
    @NotNull
    @Column(name = "deleted", nullable = false)
    private Boolean deleted;

    /**
     * Trạng thái dịch vụ theo yêu cầu: \r\n1: Dịch vụ theo yêu cầu. \r\n0: Dịch vụ bình thường.
     */
    @NotNull
    @Column(name = "dich_vu_yeu_cau", nullable = false)
    private Boolean dichVuYeuCau;

    /**
     * Đơn giá bệnh viện
     */
    @NotNull
    @Column(name = "don_gia_benh_vien", precision = 21, scale = 2, nullable = false)
    private BigDecimal donGiaBenhVien;

    /**
     * Trạng thái có hiệu lực của dịch vụ: \r\n1: Còn hiệu lực. \r\n0: Đã ẩn.
     */
    @NotNull
    @Column(name = "enabled", nullable = false)
    private Boolean enabled;

    /**
     * Số lượng giới hạn của một chỉ định
     */
    @Column(name = "goi_han_chi_dinh", precision = 21, scale = 2)
    private BigDecimal goiHanChiDinh;

    /**
     * Trạng thái phạm vi của chỉ định:\r\n0: Chỉ có thu phí. \r\n1: Có cả BHYT và thu phí
     */
    @NotNull
    @Column(name = "pham_vi_chi_dinh", nullable = false)
    private Boolean phamViChiDinh;

    /**
     * Phân biệt theo giới tính: \r\n0: Nữ. \r\n1: Nam. \r\n2: Không phân biệt giới tính. \r\n
     */
    @NotNull
    @Column(name = "phan_theo_gioi_tinh", nullable = false)
    private Integer phanTheoGioiTinh;

    /**
     * Tên của chỉ định
     */
    @Size(max = 500)
    @Column(name = "ten", length = 500)
    private String ten;

    /**
     * Tên hiển thị của chỉ định
     */
    @Size(max = 500)
    @Column(name = "ten_hien_thi", length = 500)
    private String tenHienThi;

    /**
     * Mã nội bộ
     */
    @Size(max = 255)
    @Column(name = "ma_noi_bo", length = 255)
    private String maNoiBo;

    /**
     * Mã dùng chung
     */
    @Size(max = 45)
    @Column(name = "ma_dung_chung", length = 45)
    private String maDungChung;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("ttpts")
    private DonVi donVi;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("ttpts")
    private DotThayDoiMaDichVu dotMa;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("ttpts")
    private LoaiThuThuatPhauThuat loaittpt;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public ThuThuatPhauThuat deleted(Boolean deleted) {
        this.deleted = deleted;
        return this;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Boolean isDichVuYeuCau() {
        return dichVuYeuCau;
    }

    public ThuThuatPhauThuat dichVuYeuCau(Boolean dichVuYeuCau) {
        this.dichVuYeuCau = dichVuYeuCau;
        return this;
    }

    public void setDichVuYeuCau(Boolean dichVuYeuCau) {
        this.dichVuYeuCau = dichVuYeuCau;
    }

    public BigDecimal getDonGiaBenhVien() {
        return donGiaBenhVien;
    }

    public ThuThuatPhauThuat donGiaBenhVien(BigDecimal donGiaBenhVien) {
        this.donGiaBenhVien = donGiaBenhVien;
        return this;
    }

    public void setDonGiaBenhVien(BigDecimal donGiaBenhVien) {
        this.donGiaBenhVien = donGiaBenhVien;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public ThuThuatPhauThuat enabled(Boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public BigDecimal getGoiHanChiDinh() {
        return goiHanChiDinh;
    }

    public ThuThuatPhauThuat goiHanChiDinh(BigDecimal goiHanChiDinh) {
        this.goiHanChiDinh = goiHanChiDinh;
        return this;
    }

    public void setGoiHanChiDinh(BigDecimal goiHanChiDinh) {
        this.goiHanChiDinh = goiHanChiDinh;
    }

    public Boolean isPhamViChiDinh() {
        return phamViChiDinh;
    }

    public ThuThuatPhauThuat phamViChiDinh(Boolean phamViChiDinh) {
        this.phamViChiDinh = phamViChiDinh;
        return this;
    }

    public void setPhamViChiDinh(Boolean phamViChiDinh) {
        this.phamViChiDinh = phamViChiDinh;
    }

    public Integer getPhanTheoGioiTinh() {
        return phanTheoGioiTinh;
    }

    public ThuThuatPhauThuat phanTheoGioiTinh(Integer phanTheoGioiTinh) {
        this.phanTheoGioiTinh = phanTheoGioiTinh;
        return this;
    }

    public void setPhanTheoGioiTinh(Integer phanTheoGioiTinh) {
        this.phanTheoGioiTinh = phanTheoGioiTinh;
    }

    public String getTen() {
        return ten;
    }

    public ThuThuatPhauThuat ten(String ten) {
        this.ten = ten;
        return this;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getTenHienThi() {
        return tenHienThi;
    }

    public ThuThuatPhauThuat tenHienThi(String tenHienThi) {
        this.tenHienThi = tenHienThi;
        return this;
    }

    public void setTenHienThi(String tenHienThi) {
        this.tenHienThi = tenHienThi;
    }

    public String getMaNoiBo() {
        return maNoiBo;
    }

    public ThuThuatPhauThuat maNoiBo(String maNoiBo) {
        this.maNoiBo = maNoiBo;
        return this;
    }

    public void setMaNoiBo(String maNoiBo) {
        this.maNoiBo = maNoiBo;
    }

    public String getMaDungChung() {
        return maDungChung;
    }

    public ThuThuatPhauThuat maDungChung(String maDungChung) {
        this.maDungChung = maDungChung;
        return this;
    }

    public void setMaDungChung(String maDungChung) {
        this.maDungChung = maDungChung;
    }

    public DonVi getDonVi() {
        return donVi;
    }

    public ThuThuatPhauThuat donVi(DonVi donVi) {
        this.donVi = donVi;
        return this;
    }

    public void setDonVi(DonVi donVi) {
        this.donVi = donVi;
    }

    public DotThayDoiMaDichVu getDotMa() {
        return dotMa;
    }

    public ThuThuatPhauThuat dotMa(DotThayDoiMaDichVu dotThayDoiMaDichVu) {
        this.dotMa = dotThayDoiMaDichVu;
        return this;
    }

    public void setDotMa(DotThayDoiMaDichVu dotThayDoiMaDichVu) {
        this.dotMa = dotThayDoiMaDichVu;
    }

    public LoaiThuThuatPhauThuat getLoaittpt() {
        return loaittpt;
    }

    public ThuThuatPhauThuat loaittpt(LoaiThuThuatPhauThuat loaiThuThuatPhauThuat) {
        this.loaittpt = loaiThuThuatPhauThuat;
        return this;
    }

    public void setLoaittpt(LoaiThuThuatPhauThuat loaiThuThuatPhauThuat) {
        this.loaittpt = loaiThuThuatPhauThuat;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ThuThuatPhauThuat)) {
            return false;
        }
        return id != null && id.equals(((ThuThuatPhauThuat) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ThuThuatPhauThuat{" +
            "id=" + getId() +
            ", deleted='" + isDeleted() + "'" +
            ", dichVuYeuCau='" + isDichVuYeuCau() + "'" +
            ", donGiaBenhVien=" + getDonGiaBenhVien() +
            ", enabled='" + isEnabled() + "'" +
            ", goiHanChiDinh=" + getGoiHanChiDinh() +
            ", phamViChiDinh='" + isPhamViChiDinh() + "'" +
            ", phanTheoGioiTinh=" + getPhanTheoGioiTinh() +
            ", ten='" + getTen() + "'" +
            ", tenHienThi='" + getTenHienThi() + "'" +
            ", maNoiBo='" + getMaNoiBo() + "'" +
            ", maDungChung='" + getMaDungChung() + "'" +
            "}";
    }
}

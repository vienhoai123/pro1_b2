package vn.vnpt.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A NhomBenhLy.
 */
@Entity
@Table(name = "nhom_benh_ly")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class NhomBenhLy implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * Mô tả nhóm bệnh lý
     */
    @Size(max = 500)
    @Column(name = "mo_ta", length = 500)
    private String moTa;

    /**
     * Tên nhóm bệnh lý
     */
    @NotNull
    @Size(max = 500)
    @Column(name = "ten", length = 500, nullable = false)
    private String ten;

    /**
     * Mã loại bệnh lý
     */
    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("nhomBenhLies")
    private LoaiBenhLy loaiBenhLy;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMoTa() {
        return moTa;
    }

    public NhomBenhLy moTa(String moTa) {
        this.moTa = moTa;
        return this;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public String getTen() {
        return ten;
    }

    public NhomBenhLy ten(String ten) {
        this.ten = ten;
        return this;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public LoaiBenhLy getLoaiBenhLy() {
        return loaiBenhLy;
    }

    public NhomBenhLy loaiBenhLy(LoaiBenhLy loaiBenhLy) {
        this.loaiBenhLy = loaiBenhLy;
        return this;
    }

    public void setLoaiBenhLy(LoaiBenhLy loaiBenhLy) {
        this.loaiBenhLy = loaiBenhLy;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof NhomBenhLy)) {
            return false;
        }
        return id != null && id.equals(((NhomBenhLy) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "NhomBenhLy{" +
            "id=" + getId() +
            ", moTa='" + getMoTa() + "'" +
            ", ten='" + getTen() + "'" +
            "}";
    }
}

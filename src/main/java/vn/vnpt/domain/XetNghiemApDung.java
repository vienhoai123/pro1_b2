package vn.vnpt.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * A XetNghiemApDung.
 */
@Entity
@Table(name = "xet_nghiem_ap_dung")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class XetNghiemApDung implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * mã báo cáo Bảo Hiểm Xã hội
     */
    @Size(max = 200)
    @Column(name = "ma_bao_cao_bhxh", length = 200)
    private String maBaoCaoBhxh;

    /**
     * mã báo cáo Bảo Hiểm Y tế
     */
    @Size(max = 30)
    @Column(name = "ma_bao_cao_bhyt", length = 30)
    private String maBaoCaoBhyt;

    /**
     * Ngày Áp dụng
     */
    @NotNull
    @Column(name = "ngay_ap_dung", nullable = false)
    private LocalDate ngayApDung;

    /**
     * Số công văn Bảo hiểm xã hội
     */
    @Size(max = 200)
    @Column(name = "so_cong_van_bhxh", length = 200)
    private String soCongVanBhxh;

    /**
     * Số quyết định
     */
    @Size(max = 100)
    @Column(name = "so_quyet_dinh", length = 100)
    private String soQuyetDinh;

    /**
     * Tên báo cáo Bảo hiểm xã hội
     */
    @Size(max = 500)
    @Column(name = "ten_bao_cao_bhxh", length = 500)
    private String tenBaoCaoBhxh;

    /**
     * Tên dịch vụ không Bảo hiểm y tế
     */
    @Size(max = 500)
    @Column(name = "ten_dich_vu_khong_bhyt", length = 500)
    private String tenDichVuKhongBhyt;

    /**
     * Tiền bệnh nhân chi
     */
    @Column(name = "tien_benh_nhan_chi", precision = 21, scale = 2)
    private BigDecimal tienBenhNhanChi;

    /**
     * Tiền bảo hiểm xã hội chi
     */
    @Column(name = "tien_bhxh_chi", precision = 21, scale = 2)
    private BigDecimal tienBhxhChi;

    /**
     * Tiền ngoài bhyt
     */
    @Column(name = "tien_ngoai_bhyt", precision = 21, scale = 2)
    private BigDecimal tienNgoaiBhyt;

    /**
     * Tổng tiền thanh toán
     */
    @Column(name = "tong_tien_thanh_toan", precision = 21, scale = 2)
    private BigDecimal tongTienThanhToan;

    /**
     * Tỷ lệ Bảo hiểm xã hội thanh toán
     */
    @Column(name = "ty_le_bhxh_thanh_toan")
    private Integer tyLeBhxhThanhToan;

    /**
     * Giá Bảo hiểm y tế
     */
    @NotNull
    @Column(name = "gia_bhyt", precision = 21, scale = 2, nullable = false)
    private BigDecimal giaBhyt;

    /**
     * Giá không bảo hiểm y tế
     */
    @NotNull
    @Column(name = "gia_khong_bhyt", precision = 21, scale = 2, nullable = false)
    private BigDecimal giaKhongBhyt;

    /**
     * Đối tượng đặc biệt có bảng giá khác. 0: Bình thường .1 được quy định
     */
    @Column(name = "doi_tuong_dac_biet")
    private Boolean doiTuongDacBiet;

    /**
     * Nguồn chi bảo hiểm cho bệnh nhân: 1: BHYT .2: Nhà nước 3. Khác
     */
    @Column(name = "nguon_chi")
    private Integer nguonChi;

    /**
     * Trạng thái có hiệu lực: 1: Có hiệu lực. 0: không có hiệu lực
     */
    @Column(name = "jhi_enable")
    private Boolean enable;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("xnApDungs")
    private DotGiaDichVuBhxh dotGia;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("xnApDungs")
    private XetNghiem xN;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMaBaoCaoBhxh() {
        return maBaoCaoBhxh;
    }

    public XetNghiemApDung maBaoCaoBhxh(String maBaoCaoBhxh) {
        this.maBaoCaoBhxh = maBaoCaoBhxh;
        return this;
    }

    public void setMaBaoCaoBhxh(String maBaoCaoBhxh) {
        this.maBaoCaoBhxh = maBaoCaoBhxh;
    }

    public String getMaBaoCaoBhyt() {
        return maBaoCaoBhyt;
    }

    public XetNghiemApDung maBaoCaoBhyt(String maBaoCaoBhyt) {
        this.maBaoCaoBhyt = maBaoCaoBhyt;
        return this;
    }

    public void setMaBaoCaoBhyt(String maBaoCaoBhyt) {
        this.maBaoCaoBhyt = maBaoCaoBhyt;
    }

    public LocalDate getNgayApDung() {
        return ngayApDung;
    }

    public XetNghiemApDung ngayApDung(LocalDate ngayApDung) {
        this.ngayApDung = ngayApDung;
        return this;
    }

    public void setNgayApDung(LocalDate ngayApDung) {
        this.ngayApDung = ngayApDung;
    }

    public String getSoCongVanBhxh() {
        return soCongVanBhxh;
    }

    public XetNghiemApDung soCongVanBhxh(String soCongVanBhxh) {
        this.soCongVanBhxh = soCongVanBhxh;
        return this;
    }

    public void setSoCongVanBhxh(String soCongVanBhxh) {
        this.soCongVanBhxh = soCongVanBhxh;
    }

    public String getSoQuyetDinh() {
        return soQuyetDinh;
    }

    public XetNghiemApDung soQuyetDinh(String soQuyetDinh) {
        this.soQuyetDinh = soQuyetDinh;
        return this;
    }

    public void setSoQuyetDinh(String soQuyetDinh) {
        this.soQuyetDinh = soQuyetDinh;
    }

    public String getTenBaoCaoBhxh() {
        return tenBaoCaoBhxh;
    }

    public XetNghiemApDung tenBaoCaoBhxh(String tenBaoCaoBhxh) {
        this.tenBaoCaoBhxh = tenBaoCaoBhxh;
        return this;
    }

    public void setTenBaoCaoBhxh(String tenBaoCaoBhxh) {
        this.tenBaoCaoBhxh = tenBaoCaoBhxh;
    }

    public String getTenDichVuKhongBhyt() {
        return tenDichVuKhongBhyt;
    }

    public XetNghiemApDung tenDichVuKhongBhyt(String tenDichVuKhongBhyt) {
        this.tenDichVuKhongBhyt = tenDichVuKhongBhyt;
        return this;
    }

    public void setTenDichVuKhongBhyt(String tenDichVuKhongBhyt) {
        this.tenDichVuKhongBhyt = tenDichVuKhongBhyt;
    }

    public BigDecimal getTienBenhNhanChi() {
        return tienBenhNhanChi;
    }

    public XetNghiemApDung tienBenhNhanChi(BigDecimal tienBenhNhanChi) {
        this.tienBenhNhanChi = tienBenhNhanChi;
        return this;
    }

    public void setTienBenhNhanChi(BigDecimal tienBenhNhanChi) {
        this.tienBenhNhanChi = tienBenhNhanChi;
    }

    public BigDecimal getTienBhxhChi() {
        return tienBhxhChi;
    }

    public XetNghiemApDung tienBhxhChi(BigDecimal tienBhxhChi) {
        this.tienBhxhChi = tienBhxhChi;
        return this;
    }

    public void setTienBhxhChi(BigDecimal tienBhxhChi) {
        this.tienBhxhChi = tienBhxhChi;
    }

    public BigDecimal getTienNgoaiBhyt() {
        return tienNgoaiBhyt;
    }

    public XetNghiemApDung tienNgoaiBhyt(BigDecimal tienNgoaiBhyt) {
        this.tienNgoaiBhyt = tienNgoaiBhyt;
        return this;
    }

    public void setTienNgoaiBhyt(BigDecimal tienNgoaiBhyt) {
        this.tienNgoaiBhyt = tienNgoaiBhyt;
    }

    public BigDecimal getTongTienThanhToan() {
        return tongTienThanhToan;
    }

    public XetNghiemApDung tongTienThanhToan(BigDecimal tongTienThanhToan) {
        this.tongTienThanhToan = tongTienThanhToan;
        return this;
    }

    public void setTongTienThanhToan(BigDecimal tongTienThanhToan) {
        this.tongTienThanhToan = tongTienThanhToan;
    }

    public Integer getTyLeBhxhThanhToan() {
        return tyLeBhxhThanhToan;
    }

    public XetNghiemApDung tyLeBhxhThanhToan(Integer tyLeBhxhThanhToan) {
        this.tyLeBhxhThanhToan = tyLeBhxhThanhToan;
        return this;
    }

    public void setTyLeBhxhThanhToan(Integer tyLeBhxhThanhToan) {
        this.tyLeBhxhThanhToan = tyLeBhxhThanhToan;
    }

    public BigDecimal getGiaBhyt() {
        return giaBhyt;
    }

    public XetNghiemApDung giaBhyt(BigDecimal giaBhyt) {
        this.giaBhyt = giaBhyt;
        return this;
    }

    public void setGiaBhyt(BigDecimal giaBhyt) {
        this.giaBhyt = giaBhyt;
    }

    public BigDecimal getGiaKhongBhyt() {
        return giaKhongBhyt;
    }

    public XetNghiemApDung giaKhongBhyt(BigDecimal giaKhongBhyt) {
        this.giaKhongBhyt = giaKhongBhyt;
        return this;
    }

    public void setGiaKhongBhyt(BigDecimal giaKhongBhyt) {
        this.giaKhongBhyt = giaKhongBhyt;
    }

    public Boolean isDoiTuongDacBiet() {
        return doiTuongDacBiet;
    }

    public XetNghiemApDung doiTuongDacBiet(Boolean doiTuongDacBiet) {
        this.doiTuongDacBiet = doiTuongDacBiet;
        return this;
    }

    public void setDoiTuongDacBiet(Boolean doiTuongDacBiet) {
        this.doiTuongDacBiet = doiTuongDacBiet;
    }

    public Integer getNguonChi() {
        return nguonChi;
    }

    public XetNghiemApDung nguonChi(Integer nguonChi) {
        this.nguonChi = nguonChi;
        return this;
    }

    public void setNguonChi(Integer nguonChi) {
        this.nguonChi = nguonChi;
    }

    public Boolean isEnable() {
        return enable;
    }

    public XetNghiemApDung enable(Boolean enable) {
        this.enable = enable;
        return this;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public DotGiaDichVuBhxh getDotGia() {
        return dotGia;
    }

    public XetNghiemApDung dotGia(DotGiaDichVuBhxh dotGiaDichVuBhxh) {
        this.dotGia = dotGiaDichVuBhxh;
        return this;
    }

    public void setDotGia(DotGiaDichVuBhxh dotGiaDichVuBhxh) {
        this.dotGia = dotGiaDichVuBhxh;
    }

    public XetNghiem getXN() {
        return xN;
    }

    public XetNghiemApDung xN(XetNghiem xetNghiem) {
        this.xN = xetNghiem;
        return this;
    }

    public void setXN(XetNghiem xetNghiem) {
        this.xN = xetNghiem;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof XetNghiemApDung)) {
            return false;
        }
        return id != null && id.equals(((XetNghiemApDung) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "XetNghiemApDung{" +
            "id=" + getId() +
            ", maBaoCaoBhxh='" + getMaBaoCaoBhxh() + "'" +
            ", maBaoCaoBhyt='" + getMaBaoCaoBhyt() + "'" +
            ", ngayApDung='" + getNgayApDung() + "'" +
            ", soCongVanBhxh='" + getSoCongVanBhxh() + "'" +
            ", soQuyetDinh='" + getSoQuyetDinh() + "'" +
            ", tenBaoCaoBhxh='" + getTenBaoCaoBhxh() + "'" +
            ", tenDichVuKhongBhyt='" + getTenDichVuKhongBhyt() + "'" +
            ", tienBenhNhanChi=" + getTienBenhNhanChi() +
            ", tienBhxhChi=" + getTienBhxhChi() +
            ", tienNgoaiBhyt=" + getTienNgoaiBhyt() +
            ", tongTienThanhToan=" + getTongTienThanhToan() +
            ", tyLeBhxhThanhToan=" + getTyLeBhxhThanhToan() +
            ", giaBhyt=" + getGiaBhyt() +
            ", giaKhongBhyt=" + getGiaKhongBhyt() +
            ", doiTuongDacBiet='" + isDoiTuongDacBiet() + "'" +
            ", nguonChi=" + getNguonChi() +
            ", enable='" + isEnable() + "'" +
            "}";
    }
}

package vn.vnpt.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A TPhanNhomTTPT.
 */
@Entity
@Table(name = "tpn_ttpt")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class TPhanNhomTTPT implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("tPhanNhomTTPTS")
    private NhomThuThuatPhauThuat nhomttpt;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("tPhanNhomTTPTS")
    private ThuThuatPhauThuat ttpt;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public NhomThuThuatPhauThuat getNhomttpt() {
        return nhomttpt;
    }

    public TPhanNhomTTPT nhomttpt(NhomThuThuatPhauThuat nhomThuThuatPhauThuat) {
        this.nhomttpt = nhomThuThuatPhauThuat;
        return this;
    }

    public void setNhomttpt(NhomThuThuatPhauThuat nhomThuThuatPhauThuat) {
        this.nhomttpt = nhomThuThuatPhauThuat;
    }

    public ThuThuatPhauThuat getTtpt() {
        return ttpt;
    }

    public TPhanNhomTTPT ttpt(ThuThuatPhauThuat thuThuatPhauThuat) {
        this.ttpt = thuThuatPhauThuat;
        return this;
    }

    public void setTtpt(ThuThuatPhauThuat thuThuatPhauThuat) {
        this.ttpt = thuThuatPhauThuat;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TPhanNhomTTPT)) {
            return false;
        }
        return id != null && id.equals(((TPhanNhomTTPT) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "TPhanNhomTTPT{" +
            "id=" + getId() +
            "}";
    }
}

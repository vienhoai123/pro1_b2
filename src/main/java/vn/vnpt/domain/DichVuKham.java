package vn.vnpt.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;
import java.math.BigDecimal;

/**
 * Thông tinc ông văn quy định mã của các loại dịch vụ mã bệnh viện sử dụng
 */
@Entity
@Table(name = "dich_vu_kham")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class DichVuKham implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Trạng thái xóa: 1: đã xóa. 0 không xóa
     */
    @Column(name = "deleted")
    private Integer deleted;

    /**
     * Trạng thái có hiệu lực: 1: Có. 0: Hết hiệu lực
     */
    @Column(name = "enabled")
    private Integer enabled;

    /**
     * Giới hạn chỉ định
     */
    @Column(name = "gioi_han_chi_dinh")
    private Integer gioiHanChiDinh;

    @Column(name = "phan_theo_gio_tinh")
    private Integer phanTheoGioTinh;

    @Size(max = 1000)
    @Column(name = "ten_hien_thi", length = 1000)
    private String tenHienThi;

    @Size(max = 255)
    @Column(name = "ma_dung_chung", length = 255)
    private String maDungChung;

    @Size(max = 255)
    @Column(name = "ma_noi_bo", length = 255)
    private String maNoiBo;

    /**
     * 0: Không bảo hiểm. 1: Có bảo hiểm. 2: Cả 2
     */
    @Column(name = "pham_vi_chi_dinh")
    private Integer phamViChiDinh;

    @Size(max = 255)
    @Column(name = "don_vi_tinh", length = 255)
    private String donViTinh;

    @NotNull
    @Column(name = "don_gia_benh_vien", precision = 21, scale = 2, nullable = false)
    private BigDecimal donGiaBenhVien;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("dichVuKhams")
    private DotThayDoiMaDichVu dotThayDoiMaDichVu;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove

    public Long getId() {
        return id;
    }

    public DichVuKham id(Long id) {
        this.id = id;
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public DichVuKham deleted(Integer deleted) {
        this.deleted = deleted;
        return this;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    public Integer getEnabled() {
        return enabled;
    }

    public DichVuKham enabled(Integer enabled) {
        this.enabled = enabled;
        return this;
    }

    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }

    public Integer getGioiHanChiDinh() {
        return gioiHanChiDinh;
    }

    public DichVuKham gioiHanChiDinh(Integer gioiHanChiDinh) {
        this.gioiHanChiDinh = gioiHanChiDinh;
        return this;
    }

    public void setGioiHanChiDinh(Integer gioiHanChiDinh) {
        this.gioiHanChiDinh = gioiHanChiDinh;
    }

    public Integer getPhanTheoGioTinh() {
        return phanTheoGioTinh;
    }

    public DichVuKham phanTheoGioTinh(Integer phanTheoGioTinh) {
        this.phanTheoGioTinh = phanTheoGioTinh;
        return this;
    }

    public void setPhanTheoGioTinh(Integer phanTheoGioTinh) {
        this.phanTheoGioTinh = phanTheoGioTinh;
    }

    public String getTenHienThi() {
        return tenHienThi;
    }

    public DichVuKham tenHienThi(String tenHienThi) {
        this.tenHienThi = tenHienThi;
        return this;
    }

    public void setTenHienThi(String tenHienThi) {
        this.tenHienThi = tenHienThi;
    }

    public String getMaDungChung() {
        return maDungChung;
    }

    public DichVuKham maDungChung(String maDungChung) {
        this.maDungChung = maDungChung;
        return this;
    }

    public void setMaDungChung(String maDungChung) {
        this.maDungChung = maDungChung;
    }

    public String getMaNoiBo() {
        return maNoiBo;
    }

    public DichVuKham maNoiBo(String maNoiBo) {
        this.maNoiBo = maNoiBo;
        return this;
    }

    public void setMaNoiBo(String maNoiBo) {
        this.maNoiBo = maNoiBo;
    }

    public Integer getPhamViChiDinh() {
        return phamViChiDinh;
    }

    public DichVuKham phamViChiDinh(Integer phamViChiDinh) {
        this.phamViChiDinh = phamViChiDinh;
        return this;
    }

    public void setPhamViChiDinh(Integer phamViChiDinh) {
        this.phamViChiDinh = phamViChiDinh;
    }

    public String getDonViTinh() {
        return donViTinh;
    }

    public DichVuKham donViTinh(String donViTinh) {
        this.donViTinh = donViTinh;
        return this;
    }

    public void setDonViTinh(String donViTinh) {
        this.donViTinh = donViTinh;
    }

    public BigDecimal getDonGiaBenhVien() {
        return donGiaBenhVien;
    }

    public DichVuKham donGiaBenhVien(BigDecimal donGiaBenhVien) {
        this.donGiaBenhVien = donGiaBenhVien;
        return this;
    }

    public void setDonGiaBenhVien(BigDecimal donGiaBenhVien) {
        this.donGiaBenhVien = donGiaBenhVien;
    }

    public DotThayDoiMaDichVu getDotThayDoiMaDichVu() {
        return dotThayDoiMaDichVu;
    }

    public DichVuKham dotThayDoiMaDichVu(DotThayDoiMaDichVu dotThayDoiMaDichVu) {
        this.dotThayDoiMaDichVu = dotThayDoiMaDichVu;
        return this;
    }

    public void setDotThayDoiMaDichVu(DotThayDoiMaDichVu dotThayDoiMaDichVu) {
        this.dotThayDoiMaDichVu = dotThayDoiMaDichVu;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DichVuKham)) {
            return false;
        }
        return id != null && id.equals(((DichVuKham) o).id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "DichVuKham{" +
            ", id=" + getId() +
            ", deleted=" + getDeleted() +
            ", enabled=" + getEnabled() +
            ", gioiHanChiDinh=" + getGioiHanChiDinh() +
            ", phanTheoGioTinh=" + getPhanTheoGioTinh() +
            ", tenHienThi='" + getTenHienThi() + "'" +
            ", maDungChung='" + getMaDungChung() + "'" +
            ", maNoiBo='" + getMaNoiBo() + "'" +
            ", phamViChiDinh=" + getPhamViChiDinh() +
            ", donViTinh='" + getDonViTinh() + "'" +
            ", donGiaBenhVien=" + getDonGiaBenhVien() +
            "}";
    }
}

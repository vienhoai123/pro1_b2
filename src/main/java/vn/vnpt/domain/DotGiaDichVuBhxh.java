package vn.vnpt.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * A DotGiaDichVuBhxh.
 */
@Entity
@Table(name = "dot_gia_dv_bhxh")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class DotGiaDichVuBhxh implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "doi_tuong_ap_dung", precision = 21, scale = 2, nullable = false)
    private BigDecimal doiTuongApDung;

    @Size(max = 1000)
    @Column(name = "ghi_chu", length = 1000)
    private String ghiChu;

    @NotNull
    @Column(name = "ngay_ap_dung", nullable = false)
    private LocalDate ngayApDung;

    @NotNull
    @Size(max = 500)
    @Column(name = "ten", length = 500, nullable = false)
    private String ten;

    @ManyToOne(optional = false)
    @NotNull
    private DonVi donVi;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getDoiTuongApDung() {
        return doiTuongApDung;
    }

    public DotGiaDichVuBhxh doiTuongApDung(BigDecimal doiTuongApDung) {
        this.doiTuongApDung = doiTuongApDung;
        return this;
    }

    public void setDoiTuongApDung(BigDecimal doiTuongApDung) {
        this.doiTuongApDung = doiTuongApDung;
    }

    public String getGhiChu() {
        return ghiChu;
    }

    public DotGiaDichVuBhxh ghiChu(String ghiChu) {
        this.ghiChu = ghiChu;
        return this;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    public LocalDate getNgayApDung() {
        return ngayApDung;
    }

    public DotGiaDichVuBhxh ngayApDung(LocalDate ngayApDung) {
        this.ngayApDung = ngayApDung;
        return this;
    }

    public void setNgayApDung(LocalDate ngayApDung) {
        this.ngayApDung = ngayApDung;
    }

    public String getTen() {
        return ten;
    }

    public DotGiaDichVuBhxh ten(String ten) {
        this.ten = ten;
        return this;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public DonVi getDonVi() {
        return donVi;
    }

    public void setDonVi(DonVi donVi) {
        this.donVi = donVi;
    }

    public DotGiaDichVuBhxh donVi(DonVi donVi) {
        this.donVi = donVi;
        return this;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DotGiaDichVuBhxh)) {
            return false;
        }
        return id != null && id.equals(((DotGiaDichVuBhxh) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "DotGiaDichVuBhxh{" +
            "id=" + getId() +
            ", doiTuongApDung=" + getDoiTuongApDung() +
            ", ghiChu='" + getGhiChu() + "'" +
            ", ngayApDung='" + getNgayApDung() + "'" +
            ", ten='" + getTen() + "'" +
            "}";
    }
}

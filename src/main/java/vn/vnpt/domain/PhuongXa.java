package vn.vnpt.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * NDB_TABLE=READ_BACKUP=1 Thông tin quận huyện
 */
@Entity
@Table(name = "phuong_xa")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PhuongXa implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * Cấp
     */
    @Size(max = 255)
    @Column(name = "cap", length = 255)
    private String cap;

    /**
     * Đoạn ký tự gợi ý
     */
    @Size(max = 255)
    @Column(name = "guess_phrase", length = 255)
    private String guessPhrase;

    /**
     * Tên xã
     */
    @NotNull
    @Size(max = 255)
    @Column(name = "ten", length = 255, nullable = false)
    private String ten;

    /**
     * Tên không dấu
     */
    @NotNull
    @Size(max = 255)
    @Column(name = "ten_khong_dau", length = 255, nullable = false)
    private String tenKhongDau;

    /**
     * Mã quận huyện
     */
    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("phuongXas")
    private QuanHuyen quanHuyen;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCap() {
        return cap;
    }

    public PhuongXa cap(String cap) {
        this.cap = cap;
        return this;
    }

    public void setCap(String cap) {
        this.cap = cap;
    }

    public String getGuessPhrase() {
        return guessPhrase;
    }

    public PhuongXa guessPhrase(String guessPhrase) {
        this.guessPhrase = guessPhrase;
        return this;
    }

    public void setGuessPhrase(String guessPhrase) {
        this.guessPhrase = guessPhrase;
    }

    public String getTen() {
        return ten;
    }

    public PhuongXa ten(String ten) {
        this.ten = ten;
        return this;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getTenKhongDau() {
        return tenKhongDau;
    }

    public PhuongXa tenKhongDau(String tenKhongDau) {
        this.tenKhongDau = tenKhongDau;
        return this;
    }

    public void setTenKhongDau(String tenKhongDau) {
        this.tenKhongDau = tenKhongDau;
    }

    public QuanHuyen getQuanHuyen() {
        return quanHuyen;
    }

    public PhuongXa quanHuyen(QuanHuyen quanHuyen) {
        this.quanHuyen = quanHuyen;
        return this;
    }

    public void setQuanHuyen(QuanHuyen quanHuyen) {
        this.quanHuyen = quanHuyen;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PhuongXa)) {
            return false;
        }
        return id != null && id.equals(((PhuongXa) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "PhuongXa{" +
            "id=" + getId() +
            ", cap='" + getCap() + "'" +
            ", guessPhrase='" + getGuessPhrase() + "'" +
            ", ten='" + getTen() + "'" +
            ", tenKhongDau='" + getTenKhongDau() + "'" +
            "}";
    }
}

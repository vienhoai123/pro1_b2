package vn.vnpt.domain;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class ThongTinKhoaId implements Serializable {
    private Long id;
    private Long dotDieuTriId;
    private Long bakbId;
    private Long benhNhanId;
    private Long donViId;

    public ThongTinKhoaId() {
    }

    public ThongTinKhoaId(Long id, DotDieuTriId cId) {
        this.id = id;
        this.dotDieuTriId = cId.getId();
        this.bakbId = cId.getBakbId();
        this.benhNhanId = cId.getBenhNhanId();
        this.donViId = cId.getDonViId();
    }

    public ThongTinKhoaId(Long id, Long dotDieuTriId, Long bakbId, Long benhNhanId, Long donViId) {
        this.id = id;
        this.dotDieuTriId = dotDieuTriId;
        this.bakbId = bakbId;
        this.benhNhanId = benhNhanId;
        this.donViId = donViId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDotDieuTriId() {
        return dotDieuTriId;
    }

    public void setDotDieuTriId(Long dotDieuTriId) {
        this.dotDieuTriId = dotDieuTriId;
    }

    public Long getBakbId() {
        return bakbId;
    }

    public void setBakbId(Long bakbId) {
        this.bakbId = bakbId;
    }

    public Long getBenhNhanId() {
        return benhNhanId;
    }

    public void setBenhNhanId(Long benhNhanId) {
        this.benhNhanId = benhNhanId;
    }

    public Long getDonViId() {
        return donViId;
    }

    public void setDonViId(Long donViId) {
        this.donViId = donViId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ThongTinKhoaId)) return false;
        ThongTinKhoaId that = (ThongTinKhoaId) o;
        return Objects.equals(getId(), that.getId()) &&
            Objects.equals(getDotDieuTriId(), that.getDotDieuTriId()) &&
            Objects.equals(getBakbId(), that.getBakbId()) &&
            Objects.equals(getBenhNhanId(), that.getBenhNhanId()) &&
            Objects.equals(getDonViId(), that.getDonViId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getDotDieuTriId(), getBakbId(), getBenhNhanId(), getDonViId());
    }
}

package vn.vnpt.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;
import java.time.LocalDate;

/**
 * A ThongTinBhxh.
 */
@Entity
@Table(name = "thong_tin_bhxh")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ThongTinBhxh implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * Mã đơn vị trực thuộc
     */
    @NotNull
    @Size(max = 10)
    @Column(name = "dvtt", length = 10, nullable = false)
    private String dvtt;

    /**
     * Trạng thái có hiệu lực: 1: Có hiệu lực: 0: Không có hiệu lực
     */
    @NotNull
    @Column(name = "enabled", nullable = false)
    private Boolean enabled;

    /**
     * Ngày áp dụng BHXH
     */
    @NotNull
    @Column(name = "ngay_ap_dung_bhyt", nullable = false)
    private LocalDate ngayApDungBhyt;

    /**
     * Tên đơn vị theo BHXH
     */
    @NotNull
    @Size(max = 500)
    @Column(name = "ten", length = 500, nullable = false)
    private String ten;

    /**
     * Mã đơn vị
     */
    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("thongTinBhxhs")
    private DonVi donVi;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDvtt() {
        return dvtt;
    }

    public ThongTinBhxh dvtt(String dvtt) {
        this.dvtt = dvtt;
        return this;
    }

    public void setDvtt(String dvtt) {
        this.dvtt = dvtt;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public ThongTinBhxh enabled(Boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public LocalDate getNgayApDungBhyt() {
        return ngayApDungBhyt;
    }

    public ThongTinBhxh ngayApDungBhyt(LocalDate ngayApDungBhyt) {
        this.ngayApDungBhyt = ngayApDungBhyt;
        return this;
    }

    public void setNgayApDungBhyt(LocalDate ngayApDungBhyt) {
        this.ngayApDungBhyt = ngayApDungBhyt;
    }

    public String getTen() {
        return ten;
    }

    public ThongTinBhxh ten(String ten) {
        this.ten = ten;
        return this;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public DonVi getDonVi() {
        return donVi;
    }

    public ThongTinBhxh donVi(DonVi donVi) {
        this.donVi = donVi;
        return this;
    }

    public void setDonVi(DonVi donVi) {
        this.donVi = donVi;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ThongTinBhxh)) {
            return false;
        }
        return id != null && id.equals(((ThongTinBhxh) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ThongTinBhxh{" +
            "id=" + getId() +
            ", dvtt='" + getDvtt() + "'" +
            ", enabled='" + isEnabled() + "'" +
            ", ngayApDungBhyt='" + getNgayApDungBhyt() + "'" +
            ", ten='" + getTen() + "'" +
            "}";
    }
}

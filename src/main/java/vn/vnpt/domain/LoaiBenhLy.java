package vn.vnpt.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A LoaiBenhLy.
 */
@Entity
@Table(name = "loai_benh_ly")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class LoaiBenhLy implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * Ký hiệu bệnh lý
     */
    @Size(max = 50)
    @Column(name = "ky_hieu", length = 50)
    private String kyHieu;

    /**
     * Mô tả bệnh lý
     */
    @NotNull
    @Size(max = 500)
    @Column(name = "mo_ta", length = 500, nullable = false)
    private String moTa;

    /**
     * Tên bệnh lý
     */
    @Size(max = 500)
    @Column(name = "ten", length = 500)
    private String ten;

    /**
     * Tên tiếng anh của bệnh lý
     */
    @NotNull
    @Size(max = 500)
    @Column(name = "ten_tieng_anh", length = 500, nullable = false)
    private String tenTiengAnh;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKyHieu() {
        return kyHieu;
    }

    public LoaiBenhLy kyHieu(String kyHieu) {
        this.kyHieu = kyHieu;
        return this;
    }

    public void setKyHieu(String kyHieu) {
        this.kyHieu = kyHieu;
    }

    public String getMoTa() {
        return moTa;
    }

    public LoaiBenhLy moTa(String moTa) {
        this.moTa = moTa;
        return this;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public String getTen() {
        return ten;
    }

    public LoaiBenhLy ten(String ten) {
        this.ten = ten;
        return this;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getTenTiengAnh() {
        return tenTiengAnh;
    }

    public LoaiBenhLy tenTiengAnh(String tenTiengAnh) {
        this.tenTiengAnh = tenTiengAnh;
        return this;
    }

    public void setTenTiengAnh(String tenTiengAnh) {
        this.tenTiengAnh = tenTiengAnh;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LoaiBenhLy)) {
            return false;
        }
        return id != null && id.equals(((LoaiBenhLy) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "LoaiBenhLy{" +
            "id=" + getId() +
            ", kyHieu='" + getKyHieu() + "'" +
            ", moTa='" + getMoTa() + "'" +
            ", ten='" + getTen() + "'" +
            ", tenTiengAnh='" + getTenTiengAnh() + "'" +
            "}";
    }
}

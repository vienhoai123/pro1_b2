package vn.vnpt.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * NDB_TABLE=READ_BACKUP=1 Thông tin nghề nghiệp theo công văn 4069
 */
@Entity
@Table(name = "nghe_nghiep")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class NgheNghiep implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * Mã nghề nghiệp 4069
     */
    @NotNull
    @Column(name = "ma_4069", nullable = false)
    private Integer ma4069;

    /**
     * Tên nghề nghiệp 4069
     */
    @NotNull
    @Size(max = 500)
    @Column(name = "ten_4069", length = 500, nullable = false)
    private String ten4069;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getMa4069() {
        return ma4069;
    }

    public NgheNghiep ma4069(Integer ma4069) {
        this.ma4069 = ma4069;
        return this;
    }

    public void setMa4069(Integer ma4069) {
        this.ma4069 = ma4069;
    }

    public String getTen4069() {
        return ten4069;
    }

    public NgheNghiep ten4069(String ten4069) {
        this.ten4069 = ten4069;
        return this;
    }

    public void setTen4069(String ten4069) {
        this.ten4069 = ten4069;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof NgheNghiep)) {
            return false;
        }
        return id != null && id.equals(((NgheNghiep) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "NgheNghiep{" +
            "id=" + getId() +
            ", ma4069=" + getMa4069() +
            ", ten4069='" + getTen4069() + "'" +
            "}";
    }
}

package vn.vnpt.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * A NhomThuThuatPhauThuat.
 */
@Entity
@Table(name = "nhom_thu_thuat_phau_thuat")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class NhomThuThuatPhauThuat implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * Trạng thái có hiệu lực của dịch vụ: \r\n1: Còn hiệu lực. \r\n0: Đã ẩn.
     */
    @NotNull
    @Column(name = "jhi_enable", nullable = false)
    private Boolean enable;

    /**
     * Cấp của nhóm thủ thuật phẫu thuật
     */
    @NotNull
    @Column(name = "jhi_level", nullable = false)
    private Integer level;

    /**
     * Mã nhóm cha
     */
    @NotNull
    @Column(name = "parent_id", precision = 21, scale = 2, nullable = false)
    private BigDecimal parentId;

    /**
     * Tên nhóm
     */
    @Size(max = 500)
    @Column(name = "ten", length = 500)
    private String ten;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("nhomttpts")
    private DonVi donVi;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isEnable() {
        return enable;
    }

    public NhomThuThuatPhauThuat enable(Boolean enable) {
        this.enable = enable;
        return this;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public Integer getLevel() {
        return level;
    }

    public NhomThuThuatPhauThuat level(Integer level) {
        this.level = level;
        return this;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public BigDecimal getParentId() {
        return parentId;
    }

    public NhomThuThuatPhauThuat parentId(BigDecimal parentId) {
        this.parentId = parentId;
        return this;
    }

    public void setParentId(BigDecimal parentId) {
        this.parentId = parentId;
    }

    public String getTen() {
        return ten;
    }

    public NhomThuThuatPhauThuat ten(String ten) {
        this.ten = ten;
        return this;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public DonVi getDonVi() {
        return donVi;
    }

    public NhomThuThuatPhauThuat donVi(DonVi donVi) {
        this.donVi = donVi;
        return this;
    }

    public void setDonVi(DonVi donVi) {
        this.donVi = donVi;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof NhomThuThuatPhauThuat)) {
            return false;
        }
        return id != null && id.equals(((NhomThuThuatPhauThuat) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "NhomThuThuatPhauThuat{" +
            "id=" + getId() +
            ", enable='" + isEnable() + "'" +
            ", level=" + getLevel() +
            ", parentId=" + getParentId() +
            ", ten='" + getTen() + "'" +
            "}";
    }
}

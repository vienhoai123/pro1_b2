package vn.vnpt.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A NhanVien.
 */
@Entity
@Table(name = "nhan_vien")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class NhanVien implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * Tên nhân vien
     */
    @NotNull
    @Size(max = 300)
    @Column(name = "ten", length = 300, nullable = false)
    private String ten;

    /**
     * Chứng chỉ hành nghề
     */
    @NotNull
    @Size(max = 255)
    @Column(name = "chung_chi_hanh_nghe", length = 255, nullable = false)
    private String chungChiHanhNghe;

    /**
     * CMND của nhân viên
     */
    @NotNull
    @Size(max = 30)
    @Column(name = "cmnd", length = 30, nullable = false)
    private String cmnd;

    /**
     * 0: Nữ. 1 Nam
     */
    @NotNull
    @Column(name = "gioi_tinh", nullable = false)
    private Integer gioiTinh;

    /**
     * Ngày sinh
     */
    @NotNull
    @Column(name = "ngay_sinh", nullable = false)
    private LocalDate ngaySinh;

    /**
     * Mã chức danh
     */
    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = "nhanViens", allowSetters = true)
    private ChucDanh chucDanh;

    /**
     * Mã người dùng
     */
    @ManyToOne
    @JsonIgnoreProperties(value = "nhanViens", allowSetters = true)
    private UserExtra userExtra;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @NotNull
    @JoinTable(name = "nhan_vien_chuc_vu",
               joinColumns = @JoinColumn(name = "nhan_vien_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "chuc_vu_id", referencedColumnName = "id"))
    private Set<ChucVu> chucVus = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTen() {
        return ten;
    }

    public NhanVien ten(String ten) {
        this.ten = ten;
        return this;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getChungChiHanhNghe() {
        return chungChiHanhNghe;
    }

    public NhanVien chungChiHanhNghe(String chungChiHanhNghe) {
        this.chungChiHanhNghe = chungChiHanhNghe;
        return this;
    }

    public void setChungChiHanhNghe(String chungChiHanhNghe) {
        this.chungChiHanhNghe = chungChiHanhNghe;
    }

    public String getCmnd() {
        return cmnd;
    }

    public NhanVien cmnd(String cmnd) {
        this.cmnd = cmnd;
        return this;
    }

    public void setCmnd(String cmnd) {
        this.cmnd = cmnd;
    }

    public Integer getGioiTinh() {
        return gioiTinh;
    }

    public NhanVien gioiTinh(Integer gioiTinh) {
        this.gioiTinh = gioiTinh;
        return this;
    }

    public void setGioiTinh(Integer gioiTinh) {
        this.gioiTinh = gioiTinh;
    }

    public LocalDate getNgaySinh() {
        return ngaySinh;
    }

    public NhanVien ngaySinh(LocalDate ngaySinh) {
        this.ngaySinh = ngaySinh;
        return this;
    }

    public void setNgaySinh(LocalDate ngaySinh) {
        this.ngaySinh = ngaySinh;
    }

    public ChucDanh getChucDanh() {
        return chucDanh;
    }

    public NhanVien chucDanh(ChucDanh chucDanh) {
        this.chucDanh = chucDanh;
        return this;
    }

    public void setChucDanh(ChucDanh chucDanh) {
        this.chucDanh = chucDanh;
    }

    public UserExtra getUserExtra() {
        return userExtra;
    }

    public NhanVien userExtra(UserExtra userExtra) {
        this.userExtra = userExtra;
        return this;
    }

    public void setUserExtra(UserExtra userExtra) {
        this.userExtra = userExtra;
    }

    public Set<ChucVu> getChucVus() {
        return chucVus;
    }

    public NhanVien chucVus(Set<ChucVu> chucVus) {
        this.chucVus = chucVus;
        return this;
    }

    public NhanVien addChucVu(ChucVu chucVu) {
        this.chucVus.add(chucVu);
        chucVu.getNhanViens().add(this);
        return this;
    }

    public NhanVien removeChucVu(ChucVu chucVu) {
        this.chucVus.remove(chucVu);
        chucVu.getNhanViens().remove(this);
        return this;
    }

    public void setChucVus(Set<ChucVu> chucVus) {
        this.chucVus = chucVus;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof NhanVien)) {
            return false;
        }
        return id != null && id.equals(((NhanVien) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "NhanVien{" +
            "id=" + getId() +
            ", ten='" + getTen() + "'" +
            ", chungChiHanhNghe='" + getChungChiHanhNghe() + "'" +
            ", cmnd='" + getCmnd() + "'" +
            ", gioiTinh=" + getGioiTinh() +
            ", ngaySinh='" + getNgaySinh() + "'" +
            "}";
    }
}

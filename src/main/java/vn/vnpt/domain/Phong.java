package vn.vnpt.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;
import java.math.BigDecimal;

/**
 * A Phong.
 */
@Entity
@Table(name = "phong")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Phong implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * Trạng thái: 1: Còn hiệu lực. 0 Hết hiệu lực
     */
    @NotNull
    @Column(name = "enabled", nullable = false)
    private Boolean enabled;

    /**
     * Ký hiệu phòng
     */
    @NotNull
    @Size(max = 500)
    @Column(name = "ky_hieu", length = 500, nullable = false)
    private String kyHieu;

    /**
     * Loại phòng
     */
    @NotNull
    @Column(name = "loai", precision = 21, scale = 2, nullable = false)
    private BigDecimal loai;

    /**
     * Số điện thoại của phòng
     */
    @Size(max = 15)
    @Column(name = "phone", length = 15)
    private String phone;

    /**
     * Tên phòng
     */
    @NotNull
    @Size(max = 500)
    @Column(name = "ten", length = 500, nullable = false)
    private String ten;

    /**
     * Vị trí phòng
     */
    @Size(max = 500)
    @Column(name = "vi_tri", length = 500)
    private String viTri;

    /**
     * Mã khoa
     */
    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("phongs")
    private Khoa khoa;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public Phong enabled(Boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getKyHieu() {
        return kyHieu;
    }

    public Phong kyHieu(String kyHieu) {
        this.kyHieu = kyHieu;
        return this;
    }

    public void setKyHieu(String kyHieu) {
        this.kyHieu = kyHieu;
    }

    public BigDecimal getLoai() {
        return loai;
    }

    public Phong loai(BigDecimal loai) {
        this.loai = loai;
        return this;
    }

    public void setLoai(BigDecimal loai) {
        this.loai = loai;
    }

    public String getPhone() {
        return phone;
    }

    public Phong phone(String phone) {
        this.phone = phone;
        return this;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getTen() {
        return ten;
    }

    public Phong ten(String ten) {
        this.ten = ten;
        return this;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getViTri() {
        return viTri;
    }

    public Phong viTri(String viTri) {
        this.viTri = viTri;
        return this;
    }

    public void setViTri(String viTri) {
        this.viTri = viTri;
    }

    public Khoa getKhoa() {
        return khoa;
    }

    public Phong khoa(Khoa khoa) {
        this.khoa = khoa;
        return this;
    }

    public void setKhoa(Khoa khoa) {
        this.khoa = khoa;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Phong)) {
            return false;
        }
        return id != null && id.equals(((Phong) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Phong{" +
            "id=" + getId() +
            ", enabled='" + isEnabled() + "'" +
            ", kyHieu='" + getKyHieu() + "'" +
            ", loai=" + getLoai() +
            ", phone='" + getPhone() + "'" +
            ", ten='" + getTen() + "'" +
            ", viTri='" + getViTri() + "'" +
            "}";
    }
}

package vn.vnpt.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A LoaiThuThuatPhauThuat.
 */
@Entity
@Table(name = "loai_thu_thuat_phau_thuat")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class LoaiThuThuatPhauThuat implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * Tên thủ thuật phẫu thuật
     */
    @Size(max = 500)
    @Column(name = "ten", length = 500)
    private String ten;

    /**
     * Mô tả loại thủ thuật phẫu thuật
     */
    @Size(max = 500)
    @Column(name = "mo_ta", length = 500)
    private String moTa;

    /**
     * Trạng thái có hiệu lực: 1: Có hiệu lực. 0: không có hiệu lực
     */
    @Column(name = "jhi_enable")
    private Boolean enable;

    /**
     * Số càng nhỏ thì độ ưu tiên lớn
     */
    @NotNull
    @Column(name = "uu_tien", nullable = false)
    private Integer uuTien;

    /**
     * Tên loại nhỏ hơn chẩn đoán hình ảnh
     */
    @Size(max = 500)
    @Column(name = "ma_phan_loai", length = 500)
    private String maPhanLoai;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("loaittpts")
    private DonVi donVi;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTen() {
        return ten;
    }

    public LoaiThuThuatPhauThuat ten(String ten) {
        this.ten = ten;
        return this;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getMoTa() {
        return moTa;
    }

    public LoaiThuThuatPhauThuat moTa(String moTa) {
        this.moTa = moTa;
        return this;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public Boolean isEnable() {
        return enable;
    }

    public LoaiThuThuatPhauThuat enable(Boolean enable) {
        this.enable = enable;
        return this;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public Integer getUuTien() {
        return uuTien;
    }

    public LoaiThuThuatPhauThuat uuTien(Integer uuTien) {
        this.uuTien = uuTien;
        return this;
    }

    public void setUuTien(Integer uuTien) {
        this.uuTien = uuTien;
    }

    public String getMaPhanLoai() {
        return maPhanLoai;
    }

    public LoaiThuThuatPhauThuat maPhanLoai(String maPhanLoai) {
        this.maPhanLoai = maPhanLoai;
        return this;
    }

    public void setMaPhanLoai(String maPhanLoai) {
        this.maPhanLoai = maPhanLoai;
    }

    public DonVi getDonVi() {
        return donVi;
    }

    public LoaiThuThuatPhauThuat donVi(DonVi donVi) {
        this.donVi = donVi;
        return this;
    }

    public void setDonVi(DonVi donVi) {
        this.donVi = donVi;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LoaiThuThuatPhauThuat)) {
            return false;
        }
        return id != null && id.equals(((LoaiThuThuatPhauThuat) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "LoaiThuThuatPhauThuat{" +
            "id=" + getId() +
            ", ten='" + getTen() + "'" +
            ", moTa='" + getMoTa() + "'" +
            ", enable='" + isEnable() + "'" +
            ", uuTien=" + getUuTien() +
            ", maPhanLoai='" + getMaPhanLoai() + "'" +
            "}";
    }
}

package vn.vnpt.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Khoa.
 */
@Entity
@Table(name = "khoa")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Khoa implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * Cấp khoa
     */
    @NotNull
    @Column(name = "cap", nullable = false)
    private Integer cap;

    /**
     * Trạng thái: 1: Còn hiệu lực. 0 Hết hiệu lực
     */
    @NotNull
    @Column(name = "enabled", nullable = false)
    private Boolean enabled;

    /**
     * Ký hiệu khoa
     */
    @NotNull
    @Size(max = 50)
    @Column(name = "ky_hieu", length = 50, nullable = false)
    private String kyHieu;

    /**
     * Số điện thoại khoa
     */
    @Size(max = 15)
    @Column(name = "phone", length = 15)
    private String phone;

    /**
     * Tên khoa
     */
    @NotNull
    @Size(max = 200)
    @Column(name = "ten", length = 200, nullable = false)
    private String ten;

    /**
     * Mã đơn vị
     */
    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("khoas")
    private DonVi donVi;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCap() {
        return cap;
    }

    public Khoa cap(Integer cap) {
        this.cap = cap;
        return this;
    }

    public void setCap(Integer cap) {
        this.cap = cap;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public Khoa enabled(Boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getKyHieu() {
        return kyHieu;
    }

    public Khoa kyHieu(String kyHieu) {
        this.kyHieu = kyHieu;
        return this;
    }

    public void setKyHieu(String kyHieu) {
        this.kyHieu = kyHieu;
    }

    public String getPhone() {
        return phone;
    }

    public Khoa phone(String phone) {
        this.phone = phone;
        return this;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getTen() {
        return ten;
    }

    public Khoa ten(String ten) {
        this.ten = ten;
        return this;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public DonVi getDonVi() {
        return donVi;
    }

    public Khoa donVi(DonVi donVi) {
        this.donVi = donVi;
        return this;
    }

    public void setDonVi(DonVi donVi) {
        this.donVi = donVi;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Khoa)) {
            return false;
        }
        return id != null && id.equals(((Khoa) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Khoa{" +
            "id=" + getId() +
            ", cap=" + getCap() +
            ", enabled='" + isEnabled() + "'" +
            ", kyHieu='" + getKyHieu() + "'" +
            ", phone='" + getPhone() + "'" +
            ", ten='" + getTen() + "'" +
            "}";
    }
}

package vn.vnpt.constant;

public class Constant {
    private enum TrangThai{
        CHO_KHAM(1),
        DANG_KHAM(2),
        DA_KHAM(3),
        CHUYEN_PHONG(4),
        CHUYEN_TUYEN(5),
        NHAP_VIEN(6),
        TU_VONG(7),
        TRON_VIEN(8);
        private int value;
        private TrangThai(int value){
            this.value = value;
        }
    }

    public static int getTrangThai(String trangThai){
        return TrangThai.valueOf(trangThai).value;
    }
}

package vn.vnpt.repository;

import vn.vnpt.domain.DichVuKhamApDung;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the DichVuKhamApDung entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DichVuKhamApDungRepository extends JpaRepository<DichVuKhamApDung, Long>, JpaSpecificationExecutor<DichVuKhamApDung> {
}

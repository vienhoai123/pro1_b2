package vn.vnpt.repository;

import vn.vnpt.domain.PhieuChiDinhCDHA;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import vn.vnpt.domain.PhieuChiDinhCDHAId;

/**
 * Spring Data  repository for the PhieuChiDinhCDHA entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PhieuChiDinhCDHARepository extends JpaRepository<PhieuChiDinhCDHA, PhieuChiDinhCDHAId>, JpaSpecificationExecutor<PhieuChiDinhCDHA> {
}

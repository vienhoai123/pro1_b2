package vn.vnpt.repository;

import vn.vnpt.domain.NhomThuThuatPhauThuat;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the NhomThuThuatPhauThuat entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NhomThuThuatPhauThuatRepository extends JpaRepository<NhomThuThuatPhauThuat, Long>, JpaSpecificationExecutor<NhomThuThuatPhauThuat> {
}

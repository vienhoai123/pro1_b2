package vn.vnpt.repository;

import vn.vnpt.domain.NhomBenhLy;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the NhomBenhLy entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NhomBenhLyRepository extends JpaRepository<NhomBenhLy, Long>, JpaSpecificationExecutor<NhomBenhLy> {
}

package vn.vnpt.repository;

import vn.vnpt.domain.XetNghiemApDung;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the XetNghiemApDung entity.
 */
@SuppressWarnings("unused")
@Repository
public interface XetNghiemApDungRepository extends JpaRepository<XetNghiemApDung, Long>, JpaSpecificationExecutor<XetNghiemApDung> {
}

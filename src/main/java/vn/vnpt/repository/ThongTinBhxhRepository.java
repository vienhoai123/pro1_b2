package vn.vnpt.repository;

import vn.vnpt.domain.ThongTinBhxh;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ThongTinBhxh entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ThongTinBhxhRepository extends JpaRepository<ThongTinBhxh, Long>, JpaSpecificationExecutor<ThongTinBhxh> {
}

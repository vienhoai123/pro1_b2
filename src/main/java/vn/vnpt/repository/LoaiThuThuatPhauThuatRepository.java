package vn.vnpt.repository;

import vn.vnpt.domain.LoaiThuThuatPhauThuat;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the LoaiThuThuatPhauThuat entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LoaiThuThuatPhauThuatRepository extends JpaRepository<LoaiThuThuatPhauThuat, Long>, JpaSpecificationExecutor<LoaiThuThuatPhauThuat> {
}

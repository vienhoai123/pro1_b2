package vn.vnpt.repository;

import vn.vnpt.domain.TPhongNhanVien;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the TPhongNhanVien entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TPhongNhanVienRepository extends JpaRepository<TPhongNhanVien, Long>, JpaSpecificationExecutor<TPhongNhanVien> {
}

package vn.vnpt.repository;

import vn.vnpt.domain.PhieuChiDinhXetNghiem;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import vn.vnpt.domain.PhieuChiDinhXetNghiemId;

/**
 * Spring Data  repository for the PhieuChiDinhXetNghiem entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PhieuChiDinhXetNghiemRepository extends JpaRepository<PhieuChiDinhXetNghiem, PhieuChiDinhXetNghiemId>, JpaSpecificationExecutor<PhieuChiDinhXetNghiem> {
}

package vn.vnpt.repository;

import vn.vnpt.domain.LoaiBenhLy;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the LoaiBenhLy entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LoaiBenhLyRepository extends JpaRepository<LoaiBenhLy, Long>, JpaSpecificationExecutor<LoaiBenhLy> {
}

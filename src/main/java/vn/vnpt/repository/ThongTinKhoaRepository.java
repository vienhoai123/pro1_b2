package vn.vnpt.repository;

import vn.vnpt.domain.ThongTinKhoa;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import vn.vnpt.domain.ThongTinKhoaId;

/**
 * Spring Data  repository for the ThongTinKhoa entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ThongTinKhoaRepository extends JpaRepository<ThongTinKhoa, ThongTinKhoaId>, JpaSpecificationExecutor<ThongTinKhoa> {
}

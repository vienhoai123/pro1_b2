package vn.vnpt.repository;

import vn.vnpt.domain.Khoa;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Khoa entity.
 */
@SuppressWarnings("unused")
@Repository
public interface KhoaRepository extends JpaRepository<Khoa, Long>, JpaSpecificationExecutor<Khoa> {
}

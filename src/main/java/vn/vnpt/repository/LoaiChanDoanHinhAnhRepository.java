package vn.vnpt.repository;

import vn.vnpt.domain.LoaiChanDoanHinhAnh;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the LoaiChanDoanHinhAnh entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LoaiChanDoanHinhAnhRepository extends JpaRepository<LoaiChanDoanHinhAnh, Long>, JpaSpecificationExecutor<LoaiChanDoanHinhAnh> {
}

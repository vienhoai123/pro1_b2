package vn.vnpt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.vnpt.domain.UserExtra;

import java.util.List;

/**
 * Spring Data  repository for the UserExtra entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserExtraRepository extends JpaRepository<UserExtra, Long>, JpaSpecificationExecutor<UserExtra> {

    @Query("select userExtra from UserExtra userExtra where userExtra.user.login = ?#{principal.preferredUsername}")
    List<UserExtra> findByUserIsCurrentUser();
}

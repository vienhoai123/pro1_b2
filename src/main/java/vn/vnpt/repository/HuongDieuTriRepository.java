package vn.vnpt.repository;

import vn.vnpt.domain.HuongDieuTri;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the HuongDieuTri entity.
 */
@SuppressWarnings("unused")
@Repository
public interface HuongDieuTriRepository extends JpaRepository<HuongDieuTri, Long>, JpaSpecificationExecutor<HuongDieuTri> {
}

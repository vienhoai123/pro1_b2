package vn.vnpt.repository;

import vn.vnpt.domain.BenhYhct;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the BenhYhct entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BenhYhctRepository extends JpaRepository<BenhYhct, Long>, JpaSpecificationExecutor<BenhYhct> {
}

package vn.vnpt.repository;

import vn.vnpt.domain.NgheNghiep;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the NgheNghiep entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NgheNghiepRepository extends JpaRepository<NgheNghiep, Long>, JpaSpecificationExecutor<NgheNghiep> {
}

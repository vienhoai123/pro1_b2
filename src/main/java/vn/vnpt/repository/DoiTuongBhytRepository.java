package vn.vnpt.repository;

import vn.vnpt.domain.DoiTuongBhyt;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the DoiTuongBhyt entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DoiTuongBhytRepository extends JpaRepository<DoiTuongBhyt, Long>, JpaSpecificationExecutor<DoiTuongBhyt> {
}

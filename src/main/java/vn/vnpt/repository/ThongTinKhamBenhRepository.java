package vn.vnpt.repository;

import vn.vnpt.domain.ThongTinKhamBenh;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import vn.vnpt.domain.ThongTinKhamBenhId;

/**
 * Spring Data  repository for the ThongTinKhamBenh entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ThongTinKhamBenhRepository extends JpaRepository<ThongTinKhamBenh, ThongTinKhamBenhId>, JpaSpecificationExecutor<ThongTinKhamBenh> {
}

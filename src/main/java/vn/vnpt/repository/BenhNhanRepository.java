package vn.vnpt.repository;

import vn.vnpt.domain.BenhNhan;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the BenhNhan entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BenhNhanRepository extends JpaRepository<BenhNhan, Long>, JpaSpecificationExecutor<BenhNhan> {
}

package vn.vnpt.repository;

import vn.vnpt.domain.PhieuChiDinhTTPT;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import vn.vnpt.domain.PhieuChiDinhTTPTId;

/**
 * Spring Data  repository for the PhieuChiDinhTTPT entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PhieuChiDinhTTPTRepository extends JpaRepository<PhieuChiDinhTTPT, PhieuChiDinhTTPTId>, JpaSpecificationExecutor<PhieuChiDinhTTPT> {
}

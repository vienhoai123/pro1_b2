package vn.vnpt.repository;

import vn.vnpt.domain.TheBhyt;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Spring Data  repository for the TheBhyt entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TheBhytRepository extends JpaRepository<TheBhyt, Long>, JpaSpecificationExecutor<TheBhyt> {
    Optional<TheBhyt> findByBenhNhanId(Long benhNhanId);
}

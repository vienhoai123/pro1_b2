package vn.vnpt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import vn.vnpt.domain.DotThayDoiMaDichVu;

/**
 * Spring Data  repository for the DotThayDoiMaDichVu entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DotThayDoiMaDichVuRepository extends JpaRepository<DotThayDoiMaDichVu, Long>, JpaSpecificationExecutor<DotThayDoiMaDichVu> {
}

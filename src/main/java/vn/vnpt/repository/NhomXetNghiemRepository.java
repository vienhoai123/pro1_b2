package vn.vnpt.repository;

import vn.vnpt.domain.NhomXetNghiem;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the NhomXetNghiem entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NhomXetNghiemRepository extends JpaRepository<NhomXetNghiem, Long>, JpaSpecificationExecutor<NhomXetNghiem> {
}

package vn.vnpt.repository;

import vn.vnpt.domain.LoaiXetNghiem;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the LoaiXetNghiem entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LoaiXetNghiemRepository extends JpaRepository<LoaiXetNghiem, Long>, JpaSpecificationExecutor<LoaiXetNghiem> {
}

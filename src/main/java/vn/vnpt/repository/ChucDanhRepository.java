package vn.vnpt.repository;

import vn.vnpt.domain.ChucDanh;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ChucDanh entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ChucDanhRepository extends JpaRepository<ChucDanh, Long>, JpaSpecificationExecutor<ChucDanh> {
}

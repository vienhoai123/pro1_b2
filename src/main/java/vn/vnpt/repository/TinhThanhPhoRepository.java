package vn.vnpt.repository;

import vn.vnpt.domain.TinhThanhPho;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the TinhThanhPho entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TinhThanhPhoRepository extends JpaRepository<TinhThanhPho, Long>, JpaSpecificationExecutor<TinhThanhPho> {
}

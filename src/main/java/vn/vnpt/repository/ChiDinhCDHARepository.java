package vn.vnpt.repository;

import vn.vnpt.domain.ChiDinhCDHA;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import vn.vnpt.domain.ChiDinhCDHAId;

/**
 * Spring Data  repository for the ChiDinhCDHA entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ChiDinhCDHARepository extends JpaRepository<ChiDinhCDHA, ChiDinhCDHAId>, JpaSpecificationExecutor<ChiDinhCDHA> {
}

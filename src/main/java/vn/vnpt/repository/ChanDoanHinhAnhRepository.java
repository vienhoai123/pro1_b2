package vn.vnpt.repository;

import vn.vnpt.domain.ChanDoanHinhAnh;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ChanDoanHinhAnh entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ChanDoanHinhAnhRepository extends JpaRepository<ChanDoanHinhAnh, Long>, JpaSpecificationExecutor<ChanDoanHinhAnh> {
}

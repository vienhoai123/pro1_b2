package vn.vnpt.service.impl;

import vn.vnpt.service.TPhongNhanVienService;
import vn.vnpt.domain.TPhongNhanVien;
import vn.vnpt.repository.TPhongNhanVienRepository;
import vn.vnpt.service.dto.TPhongNhanVienDTO;
import vn.vnpt.service.mapper.TPhongNhanVienMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link TPhongNhanVien}.
 */
@Service
@Transactional
public class TPhongNhanVienServiceImpl implements TPhongNhanVienService {

    private final Logger log = LoggerFactory.getLogger(TPhongNhanVienServiceImpl.class);

    private final TPhongNhanVienRepository tPhongNhanVienRepository;

    private final TPhongNhanVienMapper tPhongNhanVienMapper;

    public TPhongNhanVienServiceImpl(TPhongNhanVienRepository tPhongNhanVienRepository, TPhongNhanVienMapper tPhongNhanVienMapper) {
        this.tPhongNhanVienRepository = tPhongNhanVienRepository;
        this.tPhongNhanVienMapper = tPhongNhanVienMapper;
    }

    /**
     * Save a tPhongNhanVien.
     *
     * @param tPhongNhanVienDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public TPhongNhanVienDTO save(TPhongNhanVienDTO tPhongNhanVienDTO) {
        log.debug("Request to save TPhongNhanVien : {}", tPhongNhanVienDTO);
        TPhongNhanVien tPhongNhanVien = tPhongNhanVienMapper.toEntity(tPhongNhanVienDTO);
        tPhongNhanVien = tPhongNhanVienRepository.save(tPhongNhanVien);
        return tPhongNhanVienMapper.toDto(tPhongNhanVien);
    }

    /**
     * Get all the tPhongNhanViens.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<TPhongNhanVienDTO> findAll(Pageable pageable) {
        log.debug("Request to get all TPhongNhanViens");
        return tPhongNhanVienRepository.findAll(pageable)
            .map(tPhongNhanVienMapper::toDto);
    }

    /**
     * Get one tPhongNhanVien by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<TPhongNhanVienDTO> findOne(Long id) {
        log.debug("Request to get TPhongNhanVien : {}", id);
        return tPhongNhanVienRepository.findById(id)
            .map(tPhongNhanVienMapper::toDto);
    }

    /**
     * Delete the tPhongNhanVien by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete TPhongNhanVien : {}", id);
        tPhongNhanVienRepository.deleteById(id);
    }
}

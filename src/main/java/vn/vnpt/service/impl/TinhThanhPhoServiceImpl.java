package vn.vnpt.service.impl;

import vn.vnpt.service.TinhThanhPhoService;
import vn.vnpt.domain.TinhThanhPho;
import vn.vnpt.repository.TinhThanhPhoRepository;
import vn.vnpt.service.dto.TinhThanhPhoDTO;
import vn.vnpt.service.mapper.TinhThanhPhoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link TinhThanhPho}.
 */
@Service
@Transactional
public class TinhThanhPhoServiceImpl implements TinhThanhPhoService {

    private final Logger log = LoggerFactory.getLogger(TinhThanhPhoServiceImpl.class);

    private final TinhThanhPhoRepository tinhThanhPhoRepository;

    private final TinhThanhPhoMapper tinhThanhPhoMapper;

    public TinhThanhPhoServiceImpl(TinhThanhPhoRepository tinhThanhPhoRepository, TinhThanhPhoMapper tinhThanhPhoMapper) {
        this.tinhThanhPhoRepository = tinhThanhPhoRepository;
        this.tinhThanhPhoMapper = tinhThanhPhoMapper;
    }

    /**
     * Save a tinhThanhPho.
     *
     * @param tinhThanhPhoDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public TinhThanhPhoDTO save(TinhThanhPhoDTO tinhThanhPhoDTO) {
        log.debug("Request to save TinhThanhPho : {}", tinhThanhPhoDTO);
        TinhThanhPho tinhThanhPho = tinhThanhPhoMapper.toEntity(tinhThanhPhoDTO);
        tinhThanhPho = tinhThanhPhoRepository.save(tinhThanhPho);
        return tinhThanhPhoMapper.toDto(tinhThanhPho);
    }

    /**
     * Get all the tinhThanhPhos.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<TinhThanhPhoDTO> findAll() {
        log.debug("Request to get all TinhThanhPhos");
        return tinhThanhPhoRepository.findAll().stream()
            .map(tinhThanhPhoMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one tinhThanhPho by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<TinhThanhPhoDTO> findOne(Long id) {
        log.debug("Request to get TinhThanhPho : {}", id);
        return tinhThanhPhoRepository.findById(id)
            .map(tinhThanhPhoMapper::toDto);
    }

    /**
     * Delete the tinhThanhPho by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete TinhThanhPho : {}", id);
        tinhThanhPhoRepository.deleteById(id);
    }
}

package vn.vnpt.service.impl;

import vn.vnpt.service.ThuThuatPhauThuatApDungService;
import vn.vnpt.domain.ThuThuatPhauThuatApDung;
import vn.vnpt.repository.ThuThuatPhauThuatApDungRepository;
import vn.vnpt.service.dto.ThuThuatPhauThuatApDungDTO;
import vn.vnpt.service.mapper.ThuThuatPhauThuatApDungMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ThuThuatPhauThuatApDung}.
 */
@Service
@Transactional
public class ThuThuatPhauThuatApDungServiceImpl implements ThuThuatPhauThuatApDungService {

    private final Logger log = LoggerFactory.getLogger(ThuThuatPhauThuatApDungServiceImpl.class);

    private final ThuThuatPhauThuatApDungRepository thuThuatPhauThuatApDungRepository;

    private final ThuThuatPhauThuatApDungMapper thuThuatPhauThuatApDungMapper;

    public ThuThuatPhauThuatApDungServiceImpl(ThuThuatPhauThuatApDungRepository thuThuatPhauThuatApDungRepository, ThuThuatPhauThuatApDungMapper thuThuatPhauThuatApDungMapper) {
        this.thuThuatPhauThuatApDungRepository = thuThuatPhauThuatApDungRepository;
        this.thuThuatPhauThuatApDungMapper = thuThuatPhauThuatApDungMapper;
    }

    /**
     * Save a thuThuatPhauThuatApDung.
     *
     * @param thuThuatPhauThuatApDungDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ThuThuatPhauThuatApDungDTO save(ThuThuatPhauThuatApDungDTO thuThuatPhauThuatApDungDTO) {
        log.debug("Request to save ThuThuatPhauThuatApDung : {}", thuThuatPhauThuatApDungDTO);
        ThuThuatPhauThuatApDung thuThuatPhauThuatApDung = thuThuatPhauThuatApDungMapper.toEntity(thuThuatPhauThuatApDungDTO);
        thuThuatPhauThuatApDung = thuThuatPhauThuatApDungRepository.save(thuThuatPhauThuatApDung);
        return thuThuatPhauThuatApDungMapper.toDto(thuThuatPhauThuatApDung);
    }

    /**
     * Get all the thuThuatPhauThuatApDungs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ThuThuatPhauThuatApDungDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ThuThuatPhauThuatApDungs");
        return thuThuatPhauThuatApDungRepository.findAll(pageable)
            .map(thuThuatPhauThuatApDungMapper::toDto);
    }

    /**
     * Get one thuThuatPhauThuatApDung by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ThuThuatPhauThuatApDungDTO> findOne(Long id) {
        log.debug("Request to get ThuThuatPhauThuatApDung : {}", id);
        return thuThuatPhauThuatApDungRepository.findById(id)
            .map(thuThuatPhauThuatApDungMapper::toDto);
    }

    /**
     * Delete the thuThuatPhauThuatApDung by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ThuThuatPhauThuatApDung : {}", id);
        thuThuatPhauThuatApDungRepository.deleteById(id);
    }
}

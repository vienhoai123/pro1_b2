package vn.vnpt.service.impl;

import vn.vnpt.service.PhuongXaService;
import vn.vnpt.domain.PhuongXa;
import vn.vnpt.repository.PhuongXaRepository;
import vn.vnpt.service.dto.PhuongXaDTO;
import vn.vnpt.service.mapper.PhuongXaMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link PhuongXa}.
 */
@Service
@Transactional
public class PhuongXaServiceImpl implements PhuongXaService {

    private final Logger log = LoggerFactory.getLogger(PhuongXaServiceImpl.class);

    private final PhuongXaRepository phuongXaRepository;

    private final PhuongXaMapper phuongXaMapper;

    public PhuongXaServiceImpl(PhuongXaRepository phuongXaRepository, PhuongXaMapper phuongXaMapper) {
        this.phuongXaRepository = phuongXaRepository;
        this.phuongXaMapper = phuongXaMapper;
    }

    /**
     * Save a phuongXa.
     *
     * @param phuongXaDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public PhuongXaDTO save(PhuongXaDTO phuongXaDTO) {
        log.debug("Request to save PhuongXa : {}", phuongXaDTO);
        PhuongXa phuongXa = phuongXaMapper.toEntity(phuongXaDTO);
        phuongXa = phuongXaRepository.save(phuongXa);
        return phuongXaMapper.toDto(phuongXa);
    }

    /**
     * Get all the phuongXas.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<PhuongXaDTO> findAll() {
        log.debug("Request to get all PhuongXas");
        return phuongXaRepository.findAll().stream()
            .map(phuongXaMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one phuongXa by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<PhuongXaDTO> findOne(Long id) {
        log.debug("Request to get PhuongXa : {}", id);
        return phuongXaRepository.findById(id)
            .map(phuongXaMapper::toDto);
    }

    /**
     * Delete the phuongXa by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete PhuongXa : {}", id);
        phuongXaRepository.deleteById(id);
    }
}

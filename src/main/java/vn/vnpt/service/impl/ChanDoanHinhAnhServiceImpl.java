package vn.vnpt.service.impl;

import vn.vnpt.service.ChanDoanHinhAnhService;
import vn.vnpt.domain.ChanDoanHinhAnh;
import vn.vnpt.repository.ChanDoanHinhAnhRepository;
import vn.vnpt.service.dto.ChanDoanHinhAnhDTO;
import vn.vnpt.service.mapper.ChanDoanHinhAnhMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ChanDoanHinhAnh}.
 */
@Service
@Transactional
public class ChanDoanHinhAnhServiceImpl implements ChanDoanHinhAnhService {

    private final Logger log = LoggerFactory.getLogger(ChanDoanHinhAnhServiceImpl.class);

    private final ChanDoanHinhAnhRepository chanDoanHinhAnhRepository;

    private final ChanDoanHinhAnhMapper chanDoanHinhAnhMapper;

    public ChanDoanHinhAnhServiceImpl(ChanDoanHinhAnhRepository chanDoanHinhAnhRepository, ChanDoanHinhAnhMapper chanDoanHinhAnhMapper) {
        this.chanDoanHinhAnhRepository = chanDoanHinhAnhRepository;
        this.chanDoanHinhAnhMapper = chanDoanHinhAnhMapper;
    }

    /**
     * Save a chanDoanHinhAnh.
     *
     * @param chanDoanHinhAnhDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ChanDoanHinhAnhDTO save(ChanDoanHinhAnhDTO chanDoanHinhAnhDTO) {
        log.debug("Request to save ChanDoanHinhAnh : {}", chanDoanHinhAnhDTO);
        ChanDoanHinhAnh chanDoanHinhAnh = chanDoanHinhAnhMapper.toEntity(chanDoanHinhAnhDTO);
        chanDoanHinhAnh = chanDoanHinhAnhRepository.save(chanDoanHinhAnh);
        return chanDoanHinhAnhMapper.toDto(chanDoanHinhAnh);
    }

    /**
     * Get all the chanDoanHinhAnhs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ChanDoanHinhAnhDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ChanDoanHinhAnhs");
        return chanDoanHinhAnhRepository.findAll(pageable)
            .map(chanDoanHinhAnhMapper::toDto);
    }

    /**
     * Get one chanDoanHinhAnh by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ChanDoanHinhAnhDTO> findOne(Long id) {
        log.debug("Request to get ChanDoanHinhAnh : {}", id);
        return chanDoanHinhAnhRepository.findById(id)
            .map(chanDoanHinhAnhMapper::toDto);
    }

    /**
     * Delete the chanDoanHinhAnh by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ChanDoanHinhAnh : {}", id);
        chanDoanHinhAnhRepository.deleteById(id);
    }
}

package vn.vnpt.service.impl;

import vn.vnpt.service.LoaiChanDoanHinhAnhService;
import vn.vnpt.domain.LoaiChanDoanHinhAnh;
import vn.vnpt.repository.LoaiChanDoanHinhAnhRepository;
import vn.vnpt.service.dto.LoaiChanDoanHinhAnhDTO;
import vn.vnpt.service.mapper.LoaiChanDoanHinhAnhMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link LoaiChanDoanHinhAnh}.
 */
@Service
@Transactional
public class LoaiChanDoanHinhAnhServiceImpl implements LoaiChanDoanHinhAnhService {

    private final Logger log = LoggerFactory.getLogger(LoaiChanDoanHinhAnhServiceImpl.class);

    private final LoaiChanDoanHinhAnhRepository loaiChanDoanHinhAnhRepository;

    private final LoaiChanDoanHinhAnhMapper loaiChanDoanHinhAnhMapper;

    public LoaiChanDoanHinhAnhServiceImpl(LoaiChanDoanHinhAnhRepository loaiChanDoanHinhAnhRepository, LoaiChanDoanHinhAnhMapper loaiChanDoanHinhAnhMapper) {
        this.loaiChanDoanHinhAnhRepository = loaiChanDoanHinhAnhRepository;
        this.loaiChanDoanHinhAnhMapper = loaiChanDoanHinhAnhMapper;
    }

    /**
     * Save a loaiChanDoanHinhAnh.
     *
     * @param loaiChanDoanHinhAnhDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public LoaiChanDoanHinhAnhDTO save(LoaiChanDoanHinhAnhDTO loaiChanDoanHinhAnhDTO) {
        log.debug("Request to save LoaiChanDoanHinhAnh : {}", loaiChanDoanHinhAnhDTO);
        LoaiChanDoanHinhAnh loaiChanDoanHinhAnh = loaiChanDoanHinhAnhMapper.toEntity(loaiChanDoanHinhAnhDTO);
        loaiChanDoanHinhAnh = loaiChanDoanHinhAnhRepository.save(loaiChanDoanHinhAnh);
        return loaiChanDoanHinhAnhMapper.toDto(loaiChanDoanHinhAnh);
    }

    /**
     * Get all the loaiChanDoanHinhAnhs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<LoaiChanDoanHinhAnhDTO> findAll(Pageable pageable) {
        log.debug("Request to get all LoaiChanDoanHinhAnhs");
        return loaiChanDoanHinhAnhRepository.findAll(pageable)
            .map(loaiChanDoanHinhAnhMapper::toDto);
    }

    /**
     * Get one loaiChanDoanHinhAnh by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<LoaiChanDoanHinhAnhDTO> findOne(Long id) {
        log.debug("Request to get LoaiChanDoanHinhAnh : {}", id);
        return loaiChanDoanHinhAnhRepository.findById(id)
            .map(loaiChanDoanHinhAnhMapper::toDto);
    }

    /**
     * Delete the loaiChanDoanHinhAnh by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete LoaiChanDoanHinhAnh : {}", id);
        loaiChanDoanHinhAnhRepository.deleteById(id);
    }
}

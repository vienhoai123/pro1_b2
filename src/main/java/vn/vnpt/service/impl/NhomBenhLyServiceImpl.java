package vn.vnpt.service.impl;

import vn.vnpt.service.NhomBenhLyService;
import vn.vnpt.domain.NhomBenhLy;
import vn.vnpt.repository.NhomBenhLyRepository;
import vn.vnpt.service.dto.NhomBenhLyDTO;
import vn.vnpt.service.mapper.NhomBenhLyMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link NhomBenhLy}.
 */
@Service
@Transactional
public class NhomBenhLyServiceImpl implements NhomBenhLyService {

    private final Logger log = LoggerFactory.getLogger(NhomBenhLyServiceImpl.class);

    private final NhomBenhLyRepository nhomBenhLyRepository;

    private final NhomBenhLyMapper nhomBenhLyMapper;

    public NhomBenhLyServiceImpl(NhomBenhLyRepository nhomBenhLyRepository, NhomBenhLyMapper nhomBenhLyMapper) {
        this.nhomBenhLyRepository = nhomBenhLyRepository;
        this.nhomBenhLyMapper = nhomBenhLyMapper;
    }

    /**
     * Save a nhomBenhLy.
     *
     * @param nhomBenhLyDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public NhomBenhLyDTO save(NhomBenhLyDTO nhomBenhLyDTO) {
        log.debug("Request to save NhomBenhLy : {}", nhomBenhLyDTO);
        NhomBenhLy nhomBenhLy = nhomBenhLyMapper.toEntity(nhomBenhLyDTO);
        nhomBenhLy = nhomBenhLyRepository.save(nhomBenhLy);
        return nhomBenhLyMapper.toDto(nhomBenhLy);
    }

    /**
     * Get all the nhomBenhLies.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<NhomBenhLyDTO> findAll(Pageable pageable) {
        log.debug("Request to get all NhomBenhLies");
        return nhomBenhLyRepository.findAll(pageable)
            .map(nhomBenhLyMapper::toDto);
    }

    /**
     * Get one nhomBenhLy by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<NhomBenhLyDTO> findOne(Long id) {
        log.debug("Request to get NhomBenhLy : {}", id);
        return nhomBenhLyRepository.findById(id)
            .map(nhomBenhLyMapper::toDto);
    }

    /**
     * Delete the nhomBenhLy by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete NhomBenhLy : {}", id);
        nhomBenhLyRepository.deleteById(id);
    }
}

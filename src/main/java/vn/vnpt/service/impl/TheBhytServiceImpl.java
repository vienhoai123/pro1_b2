package vn.vnpt.service.impl;

import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import org.springframework.data.jpa.domain.Specification;
import vn.vnpt.domain.*;
import vn.vnpt.service.TheBhytQueryService;
import vn.vnpt.service.TheBhytService;
import vn.vnpt.repository.TheBhytRepository;
import vn.vnpt.service.dto.TheBhytCriteria;
import vn.vnpt.service.dto.TheBhytDTO;
import vn.vnpt.service.mapper.TheBhytMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Join;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

/**
 * Service Implementation for managing {@link TheBhyt}.
 */
@Service
@Transactional
public class TheBhytServiceImpl implements TheBhytService {

    private final Logger log = LoggerFactory.getLogger(TheBhytServiceImpl.class);

    private final TheBhytRepository theBhytRepository;

    private final TheBhytMapper theBhytMapper;

    private final TheBhytQueryService theBhytQueryService;

    public TheBhytServiceImpl(TheBhytRepository theBhytRepository, TheBhytMapper theBhytMapper,TheBhytQueryService theBhytQueryService) {
        this.theBhytRepository = theBhytRepository;
        this.theBhytMapper = theBhytMapper;
        this.theBhytQueryService = theBhytQueryService;
    }

    /**
     * Save a theBhyt.
     *
     * @param theBhytDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public TheBhytDTO save(TheBhytDTO theBhytDTO) {
        log.debug("Request to save TheBhyt : {}", theBhytDTO);
        TheBhyt theBhyt = theBhytMapper.toEntity(theBhytDTO);
        theBhyt = theBhytRepository.save(theBhyt);
        return theBhytMapper.toDto(theBhyt);
    }

    /**
     * Get all the theBhyts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<TheBhytDTO> findAll(Pageable pageable) {
        log.debug("Request to get all TheBhyts");
        return theBhytRepository.findAll(pageable)
            .map(theBhytMapper::toDto);
    }

    /**
     * Get one theBhyt by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<TheBhytDTO> findOne(Long id) {
        log.debug("Request to get TheBhyt : {}", id);
        return theBhytRepository.findById(id)
            .map(theBhytMapper::toDto);
    }

    @Override
    public Optional<TheBhytDTO> findOneBySoThe(String soThe){
        StringFilter filter = new StringFilter();
        filter.setEquals(soThe);
        TheBhytCriteria criteria = new TheBhytCriteria();
        criteria.setSoThe(filter);
        TheBhytDTO theBhytDTO = theBhytQueryService.findByCriteria(criteria).get(0);
        return Optional.of(theBhytDTO);
    }
    @Override
    public List<TheBhytDTO> findAllByBenhNhanId(Long benhNhanId) {
        log.debug("Request to get TheBhyt : {}", benhNhanId);
        LongFilter benhNhanfilter = new LongFilter();
        benhNhanfilter.setEquals(benhNhanId);
        BooleanFilter enabled = new BooleanFilter();
        enabled.setEquals(true);
        TheBhytCriteria criteria = new TheBhytCriteria();
        criteria.setBenhNhanId(benhNhanfilter);
        criteria.setEnabled(enabled);
        List<TheBhytDTO> theBhytDTOList = theBhytQueryService.findByCriteria(criteria);
        return theBhytDTOList;
    }

    /**
     * Delete the theBhyt by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete TheBhyt : {}", id);
        theBhytRepository.deleteById(id);
    }

//    /**
//     * Lấy 1 thẻ BHYT duy nhất có tỷ lệ miễn giảm cao nhất theo bệnh nhân
//     * @param benhNhan
//     * @return Optional<TheBHYTDTO>
//     */
//    @Override
//    public Optional<TheBhytDTO> getOnlyOneBhytByBenhNhan(BenhNhan benhNhan){
//
//        Specification<TheBhyt> condition = (root, criteriaQuery, criteriaBuilder) -> {
//            return criteriaBuilder.and( criteriaBuilder.equal(root.get(TheBhyt_.BENH_NHAN),benhNhan),
//                                        criteriaBuilder.equal(root.get(TheBhyt_.ENABLED),true));
//        };
//        List<TheBhyt> theBhytList = theBhytRepository.findAll(condition);
//        if (theBhytList.isEmpty())
//            return Optional.empty();
//        return Optional.of(theBhytList.get(0)).map(theBhytMapper::toDto);
//    }
}

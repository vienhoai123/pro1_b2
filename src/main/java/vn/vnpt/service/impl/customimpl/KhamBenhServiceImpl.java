package vn.vnpt.service.impl.customimpl;

import io.github.jhipster.service.filter.LongFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.constant.Constant;
import vn.vnpt.domain.*;
import vn.vnpt.service.*;
import vn.vnpt.service.customservice.KhamBenhService;
import vn.vnpt.service.customservice.TiepNhanService;
import vn.vnpt.service.dto.*;
import vn.vnpt.service.dto.customdto.KhamBenhDTO;
import vn.vnpt.service.dto.customdto.TiepNhanDTO;
import vn.vnpt.service.mapper.custommapper.TiepNhanMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class KhamBenhServiceImpl implements KhamBenhService {
    @Autowired
    private TiepNhanService tiepNhanService;
    @Autowired
    private ThongTinKhamBenhService thongTinKhamBenhService;
    @Autowired
    private ThongTinKhamBenhQueryService thongTinKhamBenhQueryService;
    @Autowired
    private ThongTinKhoaService thongTinKhoaService;
    @Autowired
    private ThongTinKhoaQueryService thongTinKhoaQueryService;
    @Autowired
    private BenhAnKhamBenhQueryService benhAnKhamBenhQueryService;
    @Autowired
    private TiepNhanMapper tiepNhanMapper;
    @Override
    public Optional<KhamBenhDTO> save(KhamBenhDTO khamBenhDTO) {
        TiepNhanDTO tiepNhanDTOResult = tiepNhanService.save(khamBenhDTO.getTiepNhanDTO());

        ThongTinKhoaDTO thongTinKhoaDTO = khamBenhDTO.getThongTinKhoaDTO();
        ThongTinKhoaDTO thongTinKhoaDTOResult = thongTinKhoaService.save(thongTinKhoaDTO);

        ThongTinKhamBenhDTO thongTinKhamBenhDTO = khamBenhDTO.getThongTinKhamBenhDTO();
        Long thongTinKhoaId = thongTinKhoaDTOResult.getId();

        thongTinKhamBenhDTO.setBakbId(thongTinKhoaId);
        thongTinKhamBenhDTO.setBenhNhanId(thongTinKhoaId);
        thongTinKhamBenhDTO.setDonViId(thongTinKhoaId);
        thongTinKhamBenhDTO.setDotDieuTriId(thongTinKhoaId);
        thongTinKhamBenhDTO.setThongTinKhoaId(thongTinKhoaId);
        ThongTinKhamBenhDTO thongTinKhamBenhDTOResult = thongTinKhamBenhService.save(thongTinKhamBenhDTO);

        return Optional.of(new KhamBenhDTO(tiepNhanDTOResult,thongTinKhamBenhDTOResult,thongTinKhoaDTOResult));
    }

    @Override
    public Optional<KhamBenhDTO> initKhamBenh(TiepNhanDTO tiepNhanDTO) {
        Long nhanVienId = tiepNhanDTO.getNhanVienTiepNhanId();
        Integer nam = tiepNhanDTO.getNam();

        ThongTinKhoaDTO thongTinKhoaDTO = new ThongTinKhoaDTO();
        thongTinKhoaDTO.setCompositeId(new ThongTinKhoaId(null,tiepNhanMapper.toDotDieuTriDto(tiepNhanDTO).getCompositeId()));
        thongTinKhoaDTO.setNhanVienNhanBenh(nhanVienId.intValue());
        thongTinKhoaDTO.setNam(nam);
        ThongTinKhoaDTO thongTinKhoaDTOresult = thongTinKhoaService.save(thongTinKhoaDTO);

        ThongTinKhamBenhDTO thongTinKhamBenhDTO = new ThongTinKhamBenhDTO();
        thongTinKhamBenhDTO.setCompositeId(new ThongTinKhamBenhId(null, thongTinKhoaDTOresult.getCompositeId()));
        thongTinKhamBenhDTO.setNam(thongTinKhoaDTOresult.getNam());
        thongTinKhamBenhDTO.setTrangThai(1);
        thongTinKhamBenhDTO.setTrangThaiXetNghiem(false);
        thongTinKhamBenhDTO.setTrangThaiCdha(false);
        thongTinKhamBenhDTO.setTrangThaiTtpt(false);
        thongTinKhamBenhDTO.setPhongId(tiepNhanDTO.getPhongTiepNhanId());
        thongTinKhamBenhDTO.setNhapVien(false);
        ThongTinKhamBenhDTO thongTinKhamBenhDTOresult = thongTinKhamBenhService.save(thongTinKhamBenhDTO);

        KhamBenhDTO result  = new KhamBenhDTO(tiepNhanDTO,thongTinKhamBenhDTOresult,thongTinKhoaDTOresult);
        return Optional.of(result);
    }

    @Override
    public Optional<KhamBenhDTO> findOne(BenhAnKhamBenhId bakbCId) {
        TiepNhanDTO tiepNhanDTO = tiepNhanService.findOneNgoaiTru(bakbCId).get();
        LongFilter filter = new LongFilter();
        filter.setEquals(tiepNhanDTO.getDotDieuTriId());
        ThongTinKhoaCriteria thongTinKhoaCriteria = new ThongTinKhoaCriteria();
        thongTinKhoaCriteria.setDotDieuTriId(filter);
        ThongTinKhoaDTO thongTinKhoaDTO = thongTinKhoaQueryService.findByCriteria(thongTinKhoaCriteria).get(0);
        LongFilter filter2 = new LongFilter();
        filter2.setEquals(thongTinKhoaDTO.getId());
        ThongTinKhamBenhCriteria thongTinKhamBenhCriteria = new ThongTinKhamBenhCriteria();
        thongTinKhamBenhCriteria.setThongTinKhoaId(filter2);
        ThongTinKhamBenhDTO thongTinKhamBenhDTO = thongTinKhamBenhQueryService.findByCriteria(thongTinKhamBenhCriteria).get(0);
        return Optional.of(new KhamBenhDTO(tiepNhanDTO,thongTinKhamBenhDTO,thongTinKhoaDTO));
    }

    @Override
    public Page<KhamBenhDTO> findByCriteria(BenhAnKhamBenhCriteria criteria, Pageable pageable) {
        List<BenhAnKhamBenhDTO> benhAnKhamBenhDTOList = benhAnKhamBenhQueryService.findByCriteria(criteria,pageable).getContent();
        List<KhamBenhDTO> khamBenhDTOList = new ArrayList<>();
        for (BenhAnKhamBenhDTO benhAnKhamBenhDTO : benhAnKhamBenhDTOList) {
            if (benhAnKhamBenhDTO.getTrangThai() >= Constant.getTrangThai("DANG_KHAM"))
                khamBenhDTOList.add(findOne(benhAnKhamBenhDTO.getCompositeId()).get());
        }
        return new PageImpl<KhamBenhDTO>(khamBenhDTOList);
    }

    @Override
    public void delete(BenhAnKhamBenhId id) {

    }

    @Override
    public void delete(ThongTinKhamBenhId id) {
        ThongTinKhoaId thongTinKhoaId = new ThongTinKhoaId(id.getThongTinKhoaId(),id.getDotDieuTriId(),id.getBakbId(),id.getBenhNhanId(),id.getDonViId());
        thongTinKhamBenhService.delete(id);
        thongTinKhoaService.delete(thongTinKhoaId);
    }



    @Override
    public ThongTinKhamBenhId getThongTinKhamBenhIdByTiepNhanNgoaiTru(TiepNhanDTO tiepNhanDTO) {
        //tìm thông tin khoa
        DotDieuTriId dotDieuTriId = tiepNhanMapper.toDotDieuTriDto(tiepNhanDTO).getCompositeId();
        LongFilter bakbFilter = new LongFilter();
        LongFilter benhNhanFilter = new LongFilter();
        LongFilter donViFilter = new LongFilter();
        LongFilter dotDieuTriFilter = new LongFilter();
        bakbFilter.setEquals(dotDieuTriId.getBakbId());
        benhNhanFilter.setEquals(dotDieuTriId.getBenhNhanId());
        donViFilter.setEquals(dotDieuTriId.getDonViId());
        dotDieuTriFilter.setEquals(dotDieuTriId.getId());
        ThongTinKhoaCriteria thongTinKhoaCriteria = new ThongTinKhoaCriteria();
        thongTinKhoaCriteria.setDotDieuTriId(dotDieuTriFilter);
        thongTinKhoaCriteria.setBakbId(bakbFilter);
        thongTinKhoaCriteria.setBenhNhanId(benhNhanFilter);
        thongTinKhoaCriteria.setDonViId(donViFilter);
        ThongTinKhoaDTO thongTinKhoaDTO = thongTinKhoaQueryService.findByCriteria(thongTinKhoaCriteria).get(0);

        //tìm thông tin khám bệnh
        LongFilter thongTinKhoaFilter = new LongFilter();
        thongTinKhoaFilter.setEquals(thongTinKhoaDTO.getId());
        ThongTinKhamBenhCriteria thongTinKhamBenhCriteria = new ThongTinKhamBenhCriteria();
        thongTinKhamBenhCriteria.setThongTinKhoaId(thongTinKhoaFilter);
        thongTinKhamBenhCriteria.setBakbId(bakbFilter);
        thongTinKhamBenhCriteria.setDotDieuTriId(dotDieuTriFilter);
        thongTinKhamBenhCriteria.setBenhNhanId(benhNhanFilter);
        thongTinKhamBenhCriteria.setDonViId(donViFilter);
        ThongTinKhamBenhDTO thongTinKhamBenhDTO = thongTinKhamBenhQueryService.findByCriteria(thongTinKhamBenhCriteria).get(0);

        return thongTinKhamBenhDTO.getCompositeId();
    }
}




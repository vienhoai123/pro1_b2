package vn.vnpt.service.impl.customimpl;

import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.domain.*;
import vn.vnpt.service.*;
import vn.vnpt.service.customservice.KhamBenhService;
import vn.vnpt.service.customservice.TiepNhanService;
import vn.vnpt.service.dto.*;
import vn.vnpt.service.dto.customdto.*;
import vn.vnpt.service.dto.BenhAnKhamBenhDTO;
import vn.vnpt.service.mapper.BenhAnKhamBenhMapper;
import vn.vnpt.service.mapper.BenhNhanMapper;
import vn.vnpt.service.mapper.DonViMapper;
import vn.vnpt.service.mapper.custommapper.TiepNhanMapper;
import vn.vnpt.constant.Constant;
import java.util.*;
import java.util.stream.Collectors;


@Service
@Transactional
public class TiepNhanServiceImpl implements TiepNhanService {

    @Autowired
    private BenhAnKhamBenhService benhAnKhamBenhService;
    @Autowired
    private  BenhAnKhamBenhQueryService benhAnKhamBenhQueryService;
    @Autowired
    private BenhNhanService benhNhanService;
    @Autowired
    private TheBhytQueryService theBhytQueryService;
    @Autowired
    private DotDieuTriService dotDieuTriService;
    @Autowired
    private TheBhytService theBhytService;
    @Autowired
    private final TiepNhanMapper tiepNhanMapper;
    @Autowired
    private DotDieuTriQueryService dotDieuTriQueryService;
    @Autowired
    private DoiTuongBhytService doiTuongBhytService;
    @Autowired
    private PhongService phongService;
    @Autowired
    private NhanVienService nhanVienService;
    @Autowired
    private BenhAnKhamBenhMapper benhAnKhamBenhMapper;
    @Autowired
    private DonViService donViService;
    @Autowired
    private DonViMapper donViMapper;
    @Autowired
    private ChiDinhDichVuKhamService chiDinhDichVuKhamService;
    @Autowired
    private KhamBenhService khamBenhService;
    @Autowired
    private DichVuKhamQueryService dichVuKhamQueryService;
    public TiepNhanServiceImpl(TiepNhanMapper tiepNhanMapper) {
        this.tiepNhanMapper = tiepNhanMapper;
    }


    /**
     * Lưu thông tin tiếp nhận, bao gồn lưu thông tin bệnh nhân, thông tin bhyt, thông tin bệnh án, thông tin đợt điều trị
     * Các lớp Service đều cung cấp hàm save()
     * Tùy thuộc vào Id của từng Entity trong TiepNhanDTO có bằng null hay không mà lệnh save của Service sẽ có ý nghĩa khác nhau
     * Nếu Id == null, lệnh save có ý nghĩa là tạo mới Entity, lưu lại vào DB và sẽ trả về 1 DTO hoàn chỉnh có Id
     * Nếu Id == {id}, lệnh save có ý nghĩa là cập nhật thông tin Entity có Id = {id} và trả về thông tin  sau khi cập nhật
     * @param tiepNhanDTO
     * @return TiepNhanDTO
     */
    @Override
    public TiepNhanDTO save(TiepNhanDTO tiepNhanDTO) {
        BenhNhanDTO benhNhanDTO = tiepNhanMapper.toBenhNhanDto(tiepNhanDTO);
        BenhNhanDTO benhNhanDTOResult = benhNhanService.save(benhNhanDTO);
        TheBhytDTO theBhytDTO = tiepNhanMapper.toTheBhytDto(tiepNhanDTO);
        // Gán Id bệnh nhân vào TheBhytDTO, sở dĩ phải làm việc này là nếu lúc gửi TiepNhanDTO xuong co the gửi benhNhanId == null
        theBhytDTO.setBenhNhanId(benhNhanDTOResult.getId());
        TheBhytDTO theBhytDTOResult = new TheBhytDTO();
        if (tiepNhanDTO.getCoBaoHiem())
            theBhytDTOResult = theBhytService.save(theBhytDTO);
        BenhAnKhamBenhDTO benhAnKhamBenhDTO = tiepNhanMapper.toBenhAnKhamBenhDto(tiepNhanDTO);
        benhAnKhamBenhDTO.setBenhNhanId(benhNhanDTOResult.getId());
        BenhAnKhamBenhDTO benhAnKhamBenhDTOResult = benhAnKhamBenhService.save(benhAnKhamBenhDTO);
        DotDieuTriDTO dotDieuTriDTO = tiepNhanMapper.toDotDieuTriDto(tiepNhanDTO);
        //Long bakbId = benhAnKhamBenhDTOResult.getId();
        //DotDieuTriId dotDieuTriId = new DotDieuTriId(benhAnKhamBenhDTO.getId(),benhAnKhamBenhDTO.getBenhNhanId(),benhAnKhamBenhDTO.getDonViId());
        dotDieuTriDTO.setCompositeId(new DotDieuTriId(null,benhAnKhamBenhDTOResult.getCompositeId()));
//        if (!tiepNhanDTO.getCoBaoHiem())
//            dotDieuTriDTO = dotDieuTriService.skipThongTinBHYT(dotDieuTriDTO).get();
        DotDieuTriDTO dotDieuTriDTOResult = dotDieuTriService.save(dotDieuTriDTO);
        // Mapping kết quả của hàm save ờ từng lớp Service vào TiepNhanDTO và trả về
        return tiepNhanMapper.toTiepNhanDto(benhNhanDTOResult,benhAnKhamBenhDTOResult,dotDieuTriDTOResult,theBhytDTOResult);
//         return new TiepNhanDTO();
    }

    /**
     * Tìm 1 thông tin tiếp nhận theo bệnh án
     * Các bước:
     *  1. Tìm thông tin bệnh án theo Id bệnh án
     *  2. Tìm thông tin bệnh nhân lưu trong bệnh án qua benhNhanId
     *  3. Tìm thông tin đợt điều trị qua ID bệnh án
     *  4. Tìm thông tin thẻ BHYT theo bệnh nhân
     *  5. Mapping thành TiepNhanDTO rồi trả về
     * @param bakbId
     * @return
     */
    @Override
    public Optional<TiepNhanDTO> findOne(Long bakbId) {
        LongFilter filter = new LongFilter();
        filter.setEquals(bakbId);
        BenhAnKhamBenhCriteria benhAnKhamBenhCriteria = new BenhAnKhamBenhCriteria();
        benhAnKhamBenhCriteria.setId(filter);
        BenhAnKhamBenhDTO benhAnKhamBenhDTO = new BenhAnKhamBenhDTO();
        if(benhAnKhamBenhQueryService.countByCriteria(benhAnKhamBenhCriteria) != 0)
           benhAnKhamBenhDTO =  benhAnKhamBenhQueryService.findByCriteria(benhAnKhamBenhCriteria).get(0);
        else
            return Optional.empty();
        BenhNhanDTO benhNhanDTO = benhNhanService.findOne(benhAnKhamBenhDTO.getBenhNhanId()).get();
        DotDieuTriCriteria dotDieuTriCriteria = new DotDieuTriCriteria();
        dotDieuTriCriteria.setBakbId(filter);
        DotDieuTriDTO dotDieuTriDTO = new DotDieuTriDTO();
        if(dotDieuTriQueryService.countByCriteria(dotDieuTriCriteria) == 1)
            dotDieuTriDTO = dotDieuTriQueryService.findByCriteria(dotDieuTriCriteria).get(0);
        TheBhytDTO theBhytDTO = new TheBhytDTO();
        if(benhAnKhamBenhDTO.isCoBaoHiem())
            theBhytDTO = theBhytService.findOneBySoThe(dotDieuTriDTO.getBhytSoThe()).get();
        return Optional.of(tiepNhanMapper.toTiepNhanDto(benhNhanDTO,benhAnKhamBenhDTO,dotDieuTriDTO,theBhytDTO));
//       return Optional.of(new TiepNhanDTO());
    }

    /**
     * Tìm thông tin tiếp nhận theo criteria
     * Sẽ có 2 trường hợp sử dụng hàm này
     *  1. Khi init thông tin tiếp nhận, lúc này chỉ có thông tin bệnh nhân mà chưa có bệnh án hay đợt điều trị nào cả
     *     Lúc này TiepNhanDTO sẽ chỉ bao gồm thông tin bệnh nhân, thông tin BHYT, thông tin bệnh án và đợt điều trị với các thông số mặc định
     *  2. Khi đã tiếp nhận và lưu thông tin tiếp nhận vào DB rồi
     *     Lúc này TiepNhanDTO là thông tin tiêp nhận được lọc theo điều kiện của bệnh án
     * @param criteria
     * @param page
     * @return
     */
    @Transactional(readOnly = true)
    public Page<TiepNhanDTO> findByCriteria(BenhAnKhamBenhCriteria criteria, Pageable page) {
        //Trường hợp 1
        if(criteria.getBenhNhanId() != null && criteria.getId() == null){
            BenhNhanDTO benhNhanDTO = benhNhanService.findOne(criteria.getBenhNhanId().getEquals()).get();
            TheBhytDTO theBhytDTO = new TheBhytDTO();
            Boolean coBaoHiem = false;
            Map thongTin = new HashMap();
            if (benhNhanService.hasBHYT(benhNhanDTO)){
                theBhytDTO = theBhytService.findAllByBenhNhanId(benhNhanDTO.getId()).get(0);
                //Nếu bệnh nhân có BHYT thì lấy các thông tin có liên quan từ đối tượng BHYT của bệnh nhân đưa vào DotDieuTriDTO
                coBaoHiem = true;
                DoiTuongBhytDTO doiTuongBhytDTO = doiTuongBhytService.findOne(theBhytDTO.getDoiTuongBhytId()).get();
                thongTin.put("tyLeMienGiam",doiTuongBhytDTO.getTyLeMienGiam());
                thongTin.put("tyLeMienGiamKtc", doiTuongBhytDTO.getTyLeMienGiamKtc());
                thongTin.put("canTren", doiTuongBhytDTO.getCanTren());
                thongTin.put("canTrenKtc", doiTuongBhytDTO.getCanTrenKtc());
                thongTin.put("doiTuongBhyt", doiTuongBhytDTO.getTen());
            }
            //Tạo bakb với thông số mặc định
            BenhAnKhamBenhDTO benhAnKhamBenhDTO = benhAnKhamBenhService.makeBakbDefault(benhNhanDTO,coBaoHiem).get();
            //Tạo đợt điều trị với thông số mặc định
            DotDieuTriDTO dotDieuTriDTO = dotDieuTriService.makeDotDieuTriDefault(coBaoHiem,thongTin).get();
            //Đóng TiepNhanDTO thành Page và trả về
            List<TiepNhanDTO> tiepNhanDTOList = new ArrayList<>();
            tiepNhanDTOList.add(tiepNhanMapper.toTiepNhanDto(benhNhanDTO,benhAnKhamBenhDTO,dotDieuTriDTO,theBhytDTO));
            return new PageImpl<TiepNhanDTO>(tiepNhanDTOList);
        }
        //Trường hợp 2
        else{
            //Tìm các bệnh án thỏa criteria
            Page<BenhAnKhamBenhDTO> benhAnKhamBenhDTOPage = benhAnKhamBenhQueryService.findByCriteria(criteria,page);
            List<TiepNhanDTO> tiepNhanDTOList = new ArrayList<TiepNhanDTO>();
            for (BenhAnKhamBenhDTO benhAnKhamBenhDTO : benhAnKhamBenhDTOPage.getContent()) {
                //Với mỗi bệnh án tìm được, tìm thông tin bệnh nhân, thông tin đợt điều trị và thông tin BHYT tương ứng, map thành TiepNhanDTO rồi thêm vào List
//                Long bakbId = benhAnKhamBenhDTO.getId();
                BenhNhanDTO benhNhanDTO = benhNhanService.findOne(benhAnKhamBenhDTO.getBenhNhanId()).get();
                LongFilter benhAnFilter = new LongFilter();
                LongFilter benhNhanFilter = new LongFilter();
                LongFilter donViFilter = new LongFilter();
                benhAnFilter.setEquals(benhAnKhamBenhDTO.getId());
                benhNhanFilter.setEquals(benhAnKhamBenhDTO.getBenhNhanId());
                donViFilter.setEquals(benhAnKhamBenhDTO.getDonViId());
                DotDieuTriCriteria dotDieuTriCriteria = new DotDieuTriCriteria();
                dotDieuTriCriteria.setBakbId(benhAnFilter);
                dotDieuTriCriteria.setBenhNhanId(benhNhanFilter);
                dotDieuTriCriteria.setDonViId(donViFilter);
                DotDieuTriDTO dotDieuTriDTO = dotDieuTriQueryService.findByCriteria(dotDieuTriCriteria).get(0);
                TheBhytDTO theBhytDTO = new TheBhytDTO();
                if (benhAnKhamBenhDTO.isCoBaoHiem())
                    theBhytDTO = theBhytService.findOneBySoThe(dotDieuTriDTO.getBhytSoThe()).get();
                tiepNhanDTOList.add(tiepNhanMapper.toTiepNhanDto(benhNhanDTO,benhAnKhamBenhDTO,dotDieuTriDTO,theBhytDTO));
            }
            // Đóng Page kết quả list và trả về
            return new PageImpl<TiepNhanDTO>(tiepNhanDTOList);
        }

        //return new PageImpl<TiepNhanDTO>(new ArrayList<>());
    }

    @Override
    public void delete(Long bakbId) {
        TiepNhanDTO tiepNhanDTO = findOne(bakbId).get();
        if (tiepNhanDTO.getTrangThai()!= Constant.getTrangThai("CHO_KHAM"))
            return;
        BenhAnKhamBenhDTO benhAnKhamBenhDTO = tiepNhanMapper.toBenhAnKhamBenhDto(tiepNhanDTO);
        DotDieuTriDTO dotDieuTriDTO = tiepNhanMapper.toDotDieuTriDto(tiepNhanDTO);
        dotDieuTriService.delete(new DotDieuTriId(dotDieuTriDTO.getId(),benhAnKhamBenhDTO.getCompositeId()));
        benhAnKhamBenhService.delete(new BenhAnKhamBenhId(benhAnKhamBenhDTO.getId(),benhAnKhamBenhDTO.getBenhNhanId(),benhAnKhamBenhDTO.getDonViId()));
        if (!tiepNhanDTO.getBenhNhanCu()){
            BenhNhanDTO benhNhanDTO = tiepNhanMapper.toBenhNhanDto(tiepNhanDTO);
            if (tiepNhanDTO.getCoBaoHiem()){
                TheBhytDTO theBhytDTO = tiepNhanMapper.toTheBhytDto(tiepNhanDTO);
                theBhytService.delete(theBhytDTO.getId());


            }
            benhNhanService.delete(benhNhanDTO.getId());
        }
    }

    public List<SoLuongBenhNhanDTO> countingBN(BenhAnKhamBenhCriteria criteria){
        List<BenhAnKhamBenhDTO> benhAnKhamBenhDTOList = benhAnKhamBenhQueryService.findByCriteria(criteria);
        List<SoLuongBenhNhanDTO> soLuongBenhNhanDTOListTemp = new ArrayList<SoLuongBenhNhanDTO>();
        List<SoLuongBenhNhanDTO> result = new ArrayList<SoLuongBenhNhanDTO>();
        for (BenhAnKhamBenhDTO benhAnKhamBenhDTO : benhAnKhamBenhDTOList) {
            SoLuongBenhNhanDTO soLuongBenhNhanDTO = new SoLuongBenhNhanDTO();
            soLuongBenhNhanDTO.setPhongID(benhAnKhamBenhDTO.getPhongTiepNhanId());
            soLuongBenhNhanDTO.setSoLuongBN(1);
            soLuongBenhNhanDTO.setSoLuongBNCoBhyt(0);
            if (benhAnKhamBenhDTO.isCoBaoHiem())
                soLuongBenhNhanDTO.setSoLuongBNCoBhyt(1);
            soLuongBenhNhanDTOListTemp.add(soLuongBenhNhanDTO);
        }
        Map<Long, Integer> soLuongBN = soLuongBenhNhanDTOListTemp.stream().collect(Collectors.groupingBy(SoLuongBenhNhanDTO::getPhongID,Collectors.summingInt(SoLuongBenhNhanDTO::getSoLuongBN)));
        Map<Long, Integer> soLuongBNCoBh = soLuongBenhNhanDTOListTemp.stream().collect(Collectors.groupingBy(SoLuongBenhNhanDTO::getPhongID,Collectors.summingInt(SoLuongBenhNhanDTO::getSoLuongBNCoBhyt)));
        Set<Long> keySet = soLuongBN.keySet();
        System.out.println(soLuongBN);
        System.out.println(soLuongBNCoBh);
        for (Long key : keySet) {
            SoLuongBenhNhanDTO soLuongBenhNhanDTO = new SoLuongBenhNhanDTO();
            soLuongBenhNhanDTO.setPhongID(key);
            soLuongBenhNhanDTO.setTenPhong(phongService.findOne(key).get().getTen());
            soLuongBenhNhanDTO.setSoLuongBN(soLuongBN.get(key));
            soLuongBenhNhanDTO.setSoLuongBNCoBhyt(soLuongBNCoBh.get(key));
            soLuongBenhNhanDTO.setTrangThai(criteria.getTrangThai()== null ? 0 : criteria.getTrangThai().getEquals());
            result.add(soLuongBenhNhanDTO);
        }
        return result;

    }

    @Override
    public List<DanhSachTiepNhanDTO> getDanhSachTiepNhan(BenhAnKhamBenhCriteria criteria,Pageable page) {
        List<TiepNhanDTO> tiepNhanDTOList = findByCriteria(criteria,page).getContent();
        List<DanhSachTiepNhanDTO> result = new ArrayList<>();
        for (TiepNhanDTO tiepNhanDTO : tiepNhanDTOList) {
            NhanVienDTO nhanVienDTO = nhanVienService.findOne(tiepNhanDTO.getNhanVienTiepNhanId()).get();
            PhongDTO phongDTO = phongService.findOne(tiepNhanDTO.getPhongTiepNhanId()).get();
            result.add(new DanhSachTiepNhanDTO(tiepNhanDTO,nhanVienDTO,phongDTO));
        }
        return result;
    }



    /**
     * Custom specifically cho  tiếp nhận ngoại trú
     */
    @Override
    public TiepNhanDTO saveNgoaiTru(TiepNhanDTO tiepNhanDTO) {
        tiepNhanDTO.setLoai(1);
        BenhNhanDTO benhNhanDTO = tiepNhanMapper.toBenhNhanDto(tiepNhanDTO);
        BenhNhanDTO benhNhanDTOAfterSave = benhNhanService.save(benhNhanDTO);

        TheBhytDTO theBhytDTOAfterSave = new TheBhytDTO();
        if (tiepNhanDTO.getCoBaoHiem()){
            TheBhytDTO theBhytDTO = tiepNhanMapper.toTheBhytDto(tiepNhanDTO);
            theBhytDTO.setBenhNhanId(benhNhanDTOAfterSave.getId());
            theBhytDTOAfterSave = theBhytService.save(theBhytDTO);
        }

        BenhAnKhamBenhDTO benhAnKhamBenhDTO = tiepNhanMapper.toBenhAnKhamBenhDto(tiepNhanDTO);
        if (benhAnKhamBenhDTO.getId() == null){
            BenhAnKhamBenhId benhAnKhamBenhId = new BenhAnKhamBenhId(null,benhNhanDTOAfterSave.getId(),benhAnKhamBenhDTO.getDonViId());
            benhAnKhamBenhDTO.setCompositeId(benhAnKhamBenhId);
        }
        BenhAnKhamBenhDTO benhAnKhamBenhDTOAfterSave = benhAnKhamBenhService.save(benhAnKhamBenhDTO);

        DotDieuTriDTO dotDieuTriDTO = tiepNhanMapper.toDotDieuTriDto(tiepNhanDTO);
        if (!dotDieuTriDTO.isCoBaoHiem())
            dotDieuTriDTO = dotDieuTriService.skipThongTinBhyt(dotDieuTriDTO);
        if(dotDieuTriDTO.getId() == null){
            DotDieuTriId dotDieuTriId = new DotDieuTriId(null,benhAnKhamBenhDTOAfterSave.getCompositeId());
            dotDieuTriDTO.setCompositeId(dotDieuTriId);
        }
        DotDieuTriDTO dotDieuTriDTOAfterSave = dotDieuTriService.save(dotDieuTriDTO);

        return tiepNhanMapper.toTiepNhanDto(benhNhanDTOAfterSave,benhAnKhamBenhDTOAfterSave,dotDieuTriDTOAfterSave,theBhytDTOAfterSave);
    }

    @Override
    public Optional<TiepNhanDTO> findOneNgoaiTru(BenhAnKhamBenhId id) {
        BenhAnKhamBenhDTO benhAnKhamBenhDTO = benhAnKhamBenhService.findOne(id).get();
        if (benhAnKhamBenhDTO == null)
            return Optional.empty();
        BenhNhanDTO benhNhanDTO = benhNhanService.findOne(id.getBenhNhanId()).get();

        LongFilter bakbFilter = new LongFilter();
        LongFilter benhNhanFilter = new LongFilter();
        LongFilter donViFilter = new LongFilter();
        bakbFilter.setEquals(id.getId());
        benhNhanFilter.setEquals(id.getBenhNhanId());
        donViFilter.setEquals(id.getDonViId());
        DotDieuTriCriteria dotDieuTriCriteria = new DotDieuTriCriteria();
        dotDieuTriCriteria.setBakbId(bakbFilter);
        dotDieuTriCriteria.setBenhNhanId(benhNhanFilter);
        dotDieuTriCriteria.setDonViId(donViFilter);
        DotDieuTriDTO dotDieuTriDTO = dotDieuTriQueryService.findByCriteria(dotDieuTriCriteria).get(0);

        if (benhAnKhamBenhDTO.isCoBaoHiem()){
            StringFilter soTheFilter = new StringFilter();
            soTheFilter.setEquals(dotDieuTriDTO.getBhytSoThe());
            TheBhytCriteria theBhytCriteria = new TheBhytCriteria();
            theBhytCriteria.setSoThe(soTheFilter);
            TheBhytDTO theBhytDTO = theBhytQueryService.findByCriteria(theBhytCriteria).get(0);
            return Optional.of(tiepNhanMapper.toTiepNhanDto(benhNhanDTO,benhAnKhamBenhDTO,dotDieuTriDTO,theBhytDTO));
        }

        return Optional.of(tiepNhanMapper.toTiepNhanDto(benhNhanDTO,benhAnKhamBenhDTO,dotDieuTriDTO,null));
    }

    @Override
    public List<TiepNhanDTO> findNgoaiTruByCriteria(BenhAnKhamBenhCriteria criteria, Pageable pageable) {
        List<BenhAnKhamBenhDTO> benhAnKhamBenhDTOList = benhAnKhamBenhQueryService.findByCriteria(criteria,pageable).getContent();
        List<TiepNhanDTO> result = new ArrayList<>();
        for (BenhAnKhamBenhDTO benhAnKhamBenhDTO : benhAnKhamBenhDTOList) {
            result.add(findOneNgoaiTru(benhAnKhamBenhDTO.getCompositeId()).get());
        }
        return result;
    }

    @Override
    public TiepNhanResponseDTO tiepNhanNgoaiTru(TiepNhanRequestDTO tiepNhanRequestDTO) {
        TiepNhanDTO tiepNhanDTO = saveNgoaiTru(tiepNhanRequestDTO.getTiepNhanDTO());
        ChiDinhDichVuKhamDTO chiDinhDichVuKhamDTO = chiDinhDichVuKhamService.chiDinhDichVuKham(tiepNhanDTO,tiepNhanRequestDTO.getDichVuKhamId());
        return new TiepNhanResponseDTO(tiepNhanDTO,chiDinhDichVuKhamDTO);
    }

    @Override
    public TiepNhanResponseDTO capNhatTiepNhanNgoaiTru(TiepNhanRequestDTO tiepNhanRequestDTO) {
        TiepNhanDTO tiepNhanDTO = tiepNhanRequestDTO.getTiepNhanDTO();
        ThongTinKhamBenhId thongTinKhamBenhId = khamBenhService.getThongTinKhamBenhIdByTiepNhanNgoaiTru(tiepNhanDTO);
        ChiDinhDichVuKhamDTO chiDinhDichVuKhamDTO = chiDinhDichVuKhamService.findOneByThongTinKhamBenhId(thongTinKhamBenhId);
        if (chiDinhDichVuKhamDTO.getDichVuKhamId() != tiepNhanRequestDTO.getDichVuKhamId())
            chiDinhDichVuKhamDTO = chiDinhDichVuKhamService.capNhatChiDinhDv(chiDinhDichVuKhamDTO,tiepNhanRequestDTO.getDichVuKhamId());
        return new TiepNhanResponseDTO(saveNgoaiTru(tiepNhanDTO),chiDinhDichVuKhamDTO);
    }

    @Override
    public List<TiepNhanResponseDTO> getDanhSachTiepNhanNgoaiTru(BenhAnKhamBenhCriteria criteria, Pageable pageable) {
        List<TiepNhanDTO> tiepNhanDTOList = findNgoaiTruByCriteria(criteria,pageable);
        List<TiepNhanResponseDTO> result = new ArrayList<>();

        for (TiepNhanDTO tiepNhanDTO : tiepNhanDTOList){
            ThongTinKhamBenhId thongTinKhamBenhId = khamBenhService.getThongTinKhamBenhIdByTiepNhanNgoaiTru(tiepNhanDTO);
            ChiDinhDichVuKhamDTO   chiDinhDichVuKhamDTO = chiDinhDichVuKhamService.findOneByThongTinKhamBenhId(thongTinKhamBenhId);
            result.add(new TiepNhanResponseDTO(tiepNhanDTO,chiDinhDichVuKhamDTO));
        }
        return result;
    }

    @Override
    public Optional<TiepNhanResponseDTO> getOneTiepNhanNgoaiTru(BenhAnKhamBenhId benhAnKhamBenhId) {
        TiepNhanDTO tiepNhanDTO = findOneNgoaiTru(benhAnKhamBenhId).get();
        ThongTinKhamBenhId thongTinKhamBenhId = khamBenhService.getThongTinKhamBenhIdByTiepNhanNgoaiTru(tiepNhanDTO);
        ChiDinhDichVuKhamDTO   chiDinhDichVuKhamDTO = chiDinhDichVuKhamService.findOneByThongTinKhamBenhId(thongTinKhamBenhId);
        return Optional.of(new TiepNhanResponseDTO(tiepNhanDTO,chiDinhDichVuKhamDTO));
    }

    @Override
    public void deleteNgoaiTru(BenhAnKhamBenhId id) {
      TiepNhanResponseDTO tiepNhanResponseDTO = getOneTiepNhanNgoaiTru(id).get();
      TiepNhanDTO tiepNhanDTO = tiepNhanResponseDTO.getTiepNhanDTO();
      ThongTinKhamBenhId thongTinKhamBenhId = tiepNhanResponseDTO.getChiDinhDichVuKhamDTO().getTtkbCompositeId();
      DotDieuTriId dotDieuTriId = new DotDieuTriId(thongTinKhamBenhId.getDotDieuTriId(),id);

      chiDinhDichVuKhamService.delete(tiepNhanResponseDTO.getChiDinhDichVuKhamDTO().getId());
      khamBenhService.delete(thongTinKhamBenhId);
      dotDieuTriService.delete(dotDieuTriId);
      benhAnKhamBenhService.delete(id);
      if (!tiepNhanDTO.getBenhNhanCu()){
          benhNhanService.delete(id.getBenhNhanId());
          if (tiepNhanDTO.getCoBaoHiem())
              theBhytService.delete(tiepNhanDTO.getBhytId());
      }
    }
}



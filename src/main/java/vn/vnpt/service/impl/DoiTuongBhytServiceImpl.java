package vn.vnpt.service.impl;

import vn.vnpt.service.DoiTuongBhytService;
import vn.vnpt.domain.DoiTuongBhyt;
import vn.vnpt.repository.DoiTuongBhytRepository;
import vn.vnpt.service.dto.DoiTuongBhytDTO;
import vn.vnpt.service.mapper.DoiTuongBhytMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link DoiTuongBhyt}.
 */
@Service
@Transactional
public class DoiTuongBhytServiceImpl implements DoiTuongBhytService {

    private final Logger log = LoggerFactory.getLogger(DoiTuongBhytServiceImpl.class);

    private final DoiTuongBhytRepository doiTuongBhytRepository;

    private final DoiTuongBhytMapper doiTuongBhytMapper;

    public DoiTuongBhytServiceImpl(DoiTuongBhytRepository doiTuongBhytRepository, DoiTuongBhytMapper doiTuongBhytMapper) {
        this.doiTuongBhytRepository = doiTuongBhytRepository;
        this.doiTuongBhytMapper = doiTuongBhytMapper;
    }

    /**
     * Save a doiTuongBhyt.
     *
     * @param doiTuongBhytDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public DoiTuongBhytDTO save(DoiTuongBhytDTO doiTuongBhytDTO) {
        log.debug("Request to save DoiTuongBhyt : {}", doiTuongBhytDTO);
        DoiTuongBhyt doiTuongBhyt = doiTuongBhytMapper.toEntity(doiTuongBhytDTO);
        doiTuongBhyt = doiTuongBhytRepository.save(doiTuongBhyt);
        return doiTuongBhytMapper.toDto(doiTuongBhyt);
    }

    /**
     * Get all the doiTuongBhyts.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<DoiTuongBhytDTO> findAll() {
        log.debug("Request to get all DoiTuongBhyts");
        return doiTuongBhytRepository.findAll().stream()
            .map(doiTuongBhytMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one doiTuongBhyt by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<DoiTuongBhytDTO> findOne(Long id) {
        log.debug("Request to get DoiTuongBhyt : {}", id);
        return doiTuongBhytRepository.findById(id)
            .map(doiTuongBhytMapper::toDto);
    }

    /**
     * Delete the doiTuongBhyt by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete DoiTuongBhyt : {}", id);
        doiTuongBhytRepository.deleteById(id);
    }
}

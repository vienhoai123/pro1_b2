package vn.vnpt.service.impl;

import vn.vnpt.service.BenhAnKhamBenhService;
import vn.vnpt.domain.BenhAnKhamBenh;
import vn.vnpt.repository.BenhAnKhamBenhRepository;
import vn.vnpt.service.dto.BenhAnKhamBenhDTO;
import vn.vnpt.service.dto.BenhNhanDTO;
import vn.vnpt.service.mapper.BenhAnKhamBenhMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import vn.vnpt.domain.BenhAnKhamBenhId;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 * Service Implementation for managing {@link BenhAnKhamBenh}.
 */
@Service
@Transactional
public class BenhAnKhamBenhServiceImpl implements BenhAnKhamBenhService {

    private final Logger log = LoggerFactory.getLogger(BenhAnKhamBenhServiceImpl.class);

    private final BenhAnKhamBenhRepository benhAnKhamBenhRepository;

    private final BenhAnKhamBenhMapper benhAnKhamBenhMapper;

    public BenhAnKhamBenhServiceImpl(BenhAnKhamBenhRepository benhAnKhamBenhRepository, BenhAnKhamBenhMapper benhAnKhamBenhMapper) {
        this.benhAnKhamBenhRepository = benhAnKhamBenhRepository;
        this.benhAnKhamBenhMapper = benhAnKhamBenhMapper;
    }

    /**
     * Save a benhAnKhamBenh.
     *
     * @param benhAnKhamBenhDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public BenhAnKhamBenhDTO save(BenhAnKhamBenhDTO benhAnKhamBenhDTO) {
        log.debug("Request to save BenhAnKhamBenh : {}", benhAnKhamBenhDTO);
        BenhAnKhamBenh benhAnKhamBenh = benhAnKhamBenhMapper.toEntity(benhAnKhamBenhDTO);
        log.debug("BenhAnKhamBenh after map: {}", benhAnKhamBenh);
        benhAnKhamBenh = benhAnKhamBenhRepository.save(benhAnKhamBenh);
        log.debug("BenhAnKhamBenh after save: {}", benhAnKhamBenh);
        return benhAnKhamBenhMapper.toDto(benhAnKhamBenh);
    }

    /**
     * Get all the benhAnKhamBenhs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<BenhAnKhamBenhDTO> findAll(Pageable pageable) {
        log.debug("Request to get all BenhAnKhamBenhs");
        return benhAnKhamBenhRepository.findAll(pageable)
            .map(benhAnKhamBenhMapper::toDto);
    }

    /**
     * Get one benhAnKhamBenh by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<BenhAnKhamBenhDTO> findOne(BenhAnKhamBenhId id) {
        log.debug("Request to get BenhAnKhamBenh : {}", id);
        return benhAnKhamBenhRepository.findById(id)
            .map(benhAnKhamBenhMapper::toDto);
    }

    /**
     * Delete the benhAnKhamBenh by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(BenhAnKhamBenhId id) {
        log.debug("Request to delete BenhAnKhamBenh : {}", id);
        benhAnKhamBenhRepository.deleteById(id);
    }

    @Override
    public Optional<BenhAnKhamBenhDTO> makeBakbDefault(BenhNhanDTO benhNhanDTO, Boolean coBaoHiem) {
        BenhAnKhamBenhDTO benhAnKhamBenhDTO = new BenhAnKhamBenhDTO();
        benhAnKhamBenhDTO.setId(null);
        benhAnKhamBenhDTO.setBenhNhanCu(true);
        benhAnKhamBenhDTO.setCanhBao(false);
        benhAnKhamBenhDTO.setCapCuu(false);
        benhAnKhamBenhDTO.setCoBaoHiem(coBaoHiem);
        benhAnKhamBenhDTO.setLoaiGiayToTreEm(1);
        benhAnKhamBenhDTO.setTrangThai(1);
        benhAnKhamBenhDTO.setNam(java.time.LocalDate.now().getYear());
        benhAnKhamBenhDTO.setThoiGianTiepNhan(java.time.LocalDate.now());
        return Optional.of(benhAnKhamBenhDTO);
    }
}

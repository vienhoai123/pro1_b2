package vn.vnpt.service.impl;

import vn.vnpt.domain.PhieuChiDinhXetNghiemId;
import vn.vnpt.service.PhieuChiDinhXetNghiemService;
import vn.vnpt.domain.PhieuChiDinhXetNghiem;
import vn.vnpt.repository.PhieuChiDinhXetNghiemRepository;
import vn.vnpt.service.dto.PhieuChiDinhXetNghiemDTO;
import vn.vnpt.service.mapper.PhieuChiDinhXetNghiemMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link PhieuChiDinhXetNghiem}.
 */
@Service
@Transactional
public class PhieuChiDinhXetNghiemServiceImpl implements PhieuChiDinhXetNghiemService {

    private final Logger log = LoggerFactory.getLogger(PhieuChiDinhXetNghiemServiceImpl.class);

    private final PhieuChiDinhXetNghiemRepository phieuChiDinhXetNghiemRepository;

    private final PhieuChiDinhXetNghiemMapper phieuChiDinhXetNghiemMapper;

    public PhieuChiDinhXetNghiemServiceImpl(PhieuChiDinhXetNghiemRepository phieuChiDinhXetNghiemRepository, PhieuChiDinhXetNghiemMapper phieuChiDinhXetNghiemMapper) {
        this.phieuChiDinhXetNghiemRepository = phieuChiDinhXetNghiemRepository;
        this.phieuChiDinhXetNghiemMapper = phieuChiDinhXetNghiemMapper;
    }

    /**
     * Save a phieuChiDinhXetNghiem.
     *
     * @param phieuChiDinhXetNghiemDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public PhieuChiDinhXetNghiemDTO save(PhieuChiDinhXetNghiemDTO phieuChiDinhXetNghiemDTO) {
        log.debug("Request to save PhieuChiDinhXetNghiem : {}", phieuChiDinhXetNghiemDTO);
        PhieuChiDinhXetNghiem phieuChiDinhXetNghiem = phieuChiDinhXetNghiemMapper.toEntity(phieuChiDinhXetNghiemDTO);
        phieuChiDinhXetNghiem = phieuChiDinhXetNghiemRepository.save(phieuChiDinhXetNghiem);
        return phieuChiDinhXetNghiemMapper.toDto(phieuChiDinhXetNghiem);
    }

    /**
     * Get all the phieuChiDinhXetNghiems.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PhieuChiDinhXetNghiemDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PhieuChiDinhXetNghiems");
        return phieuChiDinhXetNghiemRepository.findAll(pageable)
            .map(phieuChiDinhXetNghiemMapper::toDto);
    }

    /**
     * Get one phieuChiDinhXetNghiem by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<PhieuChiDinhXetNghiemDTO> findOne(PhieuChiDinhXetNghiemId id) {
        log.debug("Request to get PhieuChiDinhXetNghiem : {}", id);
        return phieuChiDinhXetNghiemRepository.findById(id)
            .map(phieuChiDinhXetNghiemMapper::toDto);
    }

    /**
     * Delete the phieuChiDinhXetNghiem by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(PhieuChiDinhXetNghiemId id) {
        log.debug("Request to delete PhieuChiDinhXetNghiem : {}", id);
        phieuChiDinhXetNghiemRepository.deleteById(id);
    }
}

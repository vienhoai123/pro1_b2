package vn.vnpt.service.impl;

import vn.vnpt.service.UserTypeService;
import vn.vnpt.domain.UserType;
import vn.vnpt.repository.UserTypeRepository;
import vn.vnpt.service.dto.UserTypeDTO;
import vn.vnpt.service.mapper.UserTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link UserType}.
 */
@Service
@Transactional
public class UserTypeServiceImpl implements UserTypeService {

    private final Logger log = LoggerFactory.getLogger(UserTypeServiceImpl.class);

    private final UserTypeRepository userTypeRepository;

    private final UserTypeMapper userTypeMapper;

    public UserTypeServiceImpl(UserTypeRepository userTypeRepository, UserTypeMapper userTypeMapper) {
        this.userTypeRepository = userTypeRepository;
        this.userTypeMapper = userTypeMapper;
    }

    /**
     * Save a userType.
     *
     * @param userTypeDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public UserTypeDTO save(UserTypeDTO userTypeDTO) {
        log.debug("Request to save UserType : {}", userTypeDTO);
        UserType userType = userTypeMapper.toEntity(userTypeDTO);
        userType = userTypeRepository.save(userType);
        return userTypeMapper.toDto(userType);
    }

    /**
     * Get all the userTypes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<UserTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UserTypes");
        return userTypeRepository.findAll(pageable)
            .map(userTypeMapper::toDto);
    }

    /**
     * Get one userType by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<UserTypeDTO> findOne(Long id) {
        log.debug("Request to get UserType : {}", id);
        return userTypeRepository.findById(id)
            .map(userTypeMapper::toDto);
    }

    /**
     * Delete the userType by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete UserType : {}", id);
        userTypeRepository.deleteById(id);
    }
}

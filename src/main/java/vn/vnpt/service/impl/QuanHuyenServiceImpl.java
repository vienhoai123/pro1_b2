package vn.vnpt.service.impl;

import vn.vnpt.service.QuanHuyenService;
import vn.vnpt.domain.QuanHuyen;
import vn.vnpt.repository.QuanHuyenRepository;
import vn.vnpt.service.dto.QuanHuyenDTO;
import vn.vnpt.service.mapper.QuanHuyenMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link QuanHuyen}.
 */
@Service
@Transactional
public class QuanHuyenServiceImpl implements QuanHuyenService {

    private final Logger log = LoggerFactory.getLogger(QuanHuyenServiceImpl.class);

    private final QuanHuyenRepository quanHuyenRepository;

    private final QuanHuyenMapper quanHuyenMapper;

    public QuanHuyenServiceImpl(QuanHuyenRepository quanHuyenRepository, QuanHuyenMapper quanHuyenMapper) {
        this.quanHuyenRepository = quanHuyenRepository;
        this.quanHuyenMapper = quanHuyenMapper;
    }

    /**
     * Save a quanHuyen.
     *
     * @param quanHuyenDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public QuanHuyenDTO save(QuanHuyenDTO quanHuyenDTO) {
        log.debug("Request to save QuanHuyen : {}", quanHuyenDTO);
        QuanHuyen quanHuyen = quanHuyenMapper.toEntity(quanHuyenDTO);
        quanHuyen = quanHuyenRepository.save(quanHuyen);
        return quanHuyenMapper.toDto(quanHuyen);
    }

    /**
     * Get all the quanHuyens.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<QuanHuyenDTO> findAll() {
        log.debug("Request to get all QuanHuyens");
        return quanHuyenRepository.findAll().stream()
            .map(quanHuyenMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one quanHuyen by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<QuanHuyenDTO> findOne(Long id) {
        log.debug("Request to get QuanHuyen : {}", id);
        return quanHuyenRepository.findById(id)
            .map(quanHuyenMapper::toDto);
    }

    /**
     * Delete the quanHuyen by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete QuanHuyen : {}", id);
        quanHuyenRepository.deleteById(id);
    }
}

package vn.vnpt.service.impl;

import vn.vnpt.domain.PhieuChiDinhTTPTId;
import vn.vnpt.service.PhieuChiDinhTTPTService;
import vn.vnpt.domain.PhieuChiDinhTTPT;
import vn.vnpt.repository.PhieuChiDinhTTPTRepository;
import vn.vnpt.service.dto.PhieuChiDinhTTPTDTO;
import vn.vnpt.service.mapper.PhieuChiDinhTTPTMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link PhieuChiDinhTTPT}.
 */
@Service
@Transactional
public class PhieuChiDinhTTPTServiceImpl implements PhieuChiDinhTTPTService {

    private final Logger log = LoggerFactory.getLogger(PhieuChiDinhTTPTServiceImpl.class);

    private final PhieuChiDinhTTPTRepository phieuChiDinhTTPTRepository;

    private final PhieuChiDinhTTPTMapper phieuChiDinhTTPTMapper;

    public PhieuChiDinhTTPTServiceImpl(PhieuChiDinhTTPTRepository phieuChiDinhTTPTRepository, PhieuChiDinhTTPTMapper phieuChiDinhTTPTMapper) {
        this.phieuChiDinhTTPTRepository = phieuChiDinhTTPTRepository;
        this.phieuChiDinhTTPTMapper = phieuChiDinhTTPTMapper;
    }

    /**
     * Save a phieuChiDinhTTPT.
     *
     * @param phieuChiDinhTTPTDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public PhieuChiDinhTTPTDTO save(PhieuChiDinhTTPTDTO phieuChiDinhTTPTDTO) {
        log.debug("Request to save PhieuChiDinhTTPT : {}", phieuChiDinhTTPTDTO);
        PhieuChiDinhTTPT phieuChiDinhTTPT = phieuChiDinhTTPTMapper.toEntity(phieuChiDinhTTPTDTO);
        phieuChiDinhTTPT = phieuChiDinhTTPTRepository.save(phieuChiDinhTTPT);
        return phieuChiDinhTTPTMapper.toDto(phieuChiDinhTTPT);
    }

    /**
     * Get all the phieuChiDinhTTPTS.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PhieuChiDinhTTPTDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PhieuChiDinhTTPTS");
        return phieuChiDinhTTPTRepository.findAll(pageable)
            .map(phieuChiDinhTTPTMapper::toDto);
    }

    /**
     * Get one phieuChiDinhTTPT by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<PhieuChiDinhTTPTDTO> findOne(PhieuChiDinhTTPTId id) {
        log.debug("Request to get PhieuChiDinhTTPT : {}", id);
        return phieuChiDinhTTPTRepository.findById(id)
            .map(phieuChiDinhTTPTMapper::toDto);
    }

    /**
     * Delete the phieuChiDinhTTPT by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(PhieuChiDinhTTPTId id) {
        log.debug("Request to delete PhieuChiDinhTTPT : {}", id);
        phieuChiDinhTTPTRepository.deleteById(id);
    }
}

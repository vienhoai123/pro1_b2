package vn.vnpt.service.impl;

import vn.vnpt.service.KhoaService;
import vn.vnpt.domain.Khoa;
import vn.vnpt.repository.KhoaRepository;
import vn.vnpt.service.dto.KhoaDTO;
import vn.vnpt.service.mapper.KhoaMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Khoa}.
 */
@Service
@Transactional
public class KhoaServiceImpl implements KhoaService {

    private final Logger log = LoggerFactory.getLogger(KhoaServiceImpl.class);

    private final KhoaRepository khoaRepository;

    private final KhoaMapper khoaMapper;

    public KhoaServiceImpl(KhoaRepository khoaRepository, KhoaMapper khoaMapper) {
        this.khoaRepository = khoaRepository;
        this.khoaMapper = khoaMapper;
    }

    /**
     * Save a khoa.
     *
     * @param khoaDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public KhoaDTO save(KhoaDTO khoaDTO) {
        log.debug("Request to save Khoa : {}", khoaDTO);
        Khoa khoa = khoaMapper.toEntity(khoaDTO);
        khoa = khoaRepository.save(khoa);
        return khoaMapper.toDto(khoa);
    }

    /**
     * Get all the khoas.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<KhoaDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Khoas");
        return khoaRepository.findAll(pageable)
            .map(khoaMapper::toDto);
    }

    /**
     * Get one khoa by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<KhoaDTO> findOne(Long id) {
        log.debug("Request to get Khoa : {}", id);
        return khoaRepository.findById(id)
            .map(khoaMapper::toDto);
    }

    /**
     * Delete the khoa by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Khoa : {}", id);
        khoaRepository.deleteById(id);
    }
}

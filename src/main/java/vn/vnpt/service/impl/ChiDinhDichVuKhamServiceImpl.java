package vn.vnpt.service.impl;

import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import org.springframework.beans.factory.annotation.Autowired;
import vn.vnpt.domain.*;
import vn.vnpt.service.ChiDinhDichVuKhamQueryService;
import vn.vnpt.service.ChiDinhDichVuKhamService;
import vn.vnpt.repository.ChiDinhDichVuKhamRepository;
import vn.vnpt.service.DichVuKhamApDungQueryService;
import vn.vnpt.service.DichVuKhamService;
import vn.vnpt.service.customservice.KhamBenhService;
import vn.vnpt.service.dto.*;
import vn.vnpt.service.dto.customdto.KhamBenhDTO;
import vn.vnpt.service.dto.customdto.TiepNhanDTO;
import vn.vnpt.service.mapper.ChiDinhDichVuKhamMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.service.mapper.custommapper.TiepNhanMapper;

import java.math.BigDecimal;
import java.util.Optional;

/**
 * Service Implementation for managing {@link ChiDinhDichVuKham}.
 */
@Service
@Transactional
public class ChiDinhDichVuKhamServiceImpl implements ChiDinhDichVuKhamService {

    private final Logger log = LoggerFactory.getLogger(ChiDinhDichVuKhamServiceImpl.class);

    private final ChiDinhDichVuKhamRepository chiDinhDichVuKhamRepository;

    private final ChiDinhDichVuKhamMapper chiDinhDichVuKhamMapper;

    @Autowired
    private DichVuKhamService dichVuKhamService;
    @Autowired
    private DichVuKhamApDungQueryService dichVuKhamApDungQueryService;
    @Autowired
    private TiepNhanMapper tiepNhanMapper;
    @Autowired
    private KhamBenhService khamBenhService;
    @Autowired
    private ChiDinhDichVuKhamQueryService chiDinhDichVuKhamQueryService;
    public ChiDinhDichVuKhamServiceImpl(ChiDinhDichVuKhamRepository chiDinhDichVuKhamRepository, ChiDinhDichVuKhamMapper chiDinhDichVuKhamMapper) {
        this.chiDinhDichVuKhamRepository = chiDinhDichVuKhamRepository;
        this.chiDinhDichVuKhamMapper = chiDinhDichVuKhamMapper;
    }

    /**
     * Save a chiDinhDichVuKham.
     *
     * @param chiDinhDichVuKhamDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ChiDinhDichVuKhamDTO save(ChiDinhDichVuKhamDTO chiDinhDichVuKhamDTO) {
        log.debug("Request to save ChiDinhDichVuKham : {}", chiDinhDichVuKhamDTO);
        ChiDinhDichVuKham chiDinhDichVuKham = chiDinhDichVuKhamMapper.toEntity(chiDinhDichVuKhamDTO);
        chiDinhDichVuKham = chiDinhDichVuKhamRepository.save(chiDinhDichVuKham);
        return chiDinhDichVuKhamMapper.toDto(chiDinhDichVuKham);
    }

    /**
     * Get all the chiDinhDichVuKhams.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ChiDinhDichVuKhamDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ChiDinhDichVuKhams");
        return chiDinhDichVuKhamRepository.findAll(pageable)
            .map(chiDinhDichVuKhamMapper::toDto);
    }

    /**
     * Get one chiDinhDichVuKham by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ChiDinhDichVuKhamDTO> findOne(Long id) {
        log.debug("Request to get ChiDinhDichVuKham : {}", id);
        return chiDinhDichVuKhamRepository.findById(id)
            .map(chiDinhDichVuKhamMapper::toDto);
    }

    /**
     * Delete the chiDinhDichVuKham by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ChiDinhDichVuKham : {}", id);
        chiDinhDichVuKhamRepository.deleteById(id);
    }

    @Override
    public ChiDinhDichVuKhamDTO chiDinhDichVuKham(TiepNhanDTO tiepNhanDTO, Long dichVuKhamId) {
        DichVuKhamDTO dichVuKhamDTO = dichVuKhamService.findOne(dichVuKhamId).get();

        LongFilter dichVuKhamFilter = new LongFilter();
        dichVuKhamFilter.setEquals(dichVuKhamId);
        IntegerFilter enabledFilter = new IntegerFilter();
        enabledFilter.setEquals(1);
        DichVuKhamApDungCriteria dichVuKhamApDungCriteria = new DichVuKhamApDungCriteria();
        dichVuKhamApDungCriteria.setDichVuKhamId(dichVuKhamFilter);
        dichVuKhamApDungCriteria.setEnabled(enabledFilter);
        DichVuKhamApDungDTO dichVuKhamApDungDTO = new DichVuKhamApDungDTO();
        if (dichVuKhamApDungQueryService.countByCriteria(dichVuKhamApDungCriteria) != 0){
            dichVuKhamApDungDTO = dichVuKhamApDungQueryService.findByCriteria(dichVuKhamApDungCriteria).get(0);
        }

        KhamBenhDTO khamBenhDTO = khamBenhService.initKhamBenh(tiepNhanDTO).get();
        ThongTinKhamBenhId thongTinKhamBenhId = khamBenhDTO.getThongTinKhamBenhDTO().getCompositeId();
        //Mapping
        ChiDinhDichVuKhamDTO chiDinhDichVuKhamDTO = new ChiDinhDichVuKhamDTO();
        chiDinhDichVuKhamDTO.setCongKhamBanDau(true);
        chiDinhDichVuKhamDTO.setDaThanhToan(false);
        chiDinhDichVuKhamDTO.setDonGia(dichVuKhamDTO.getDonGiaBenhVien());
        chiDinhDichVuKhamDTO.setTen(dichVuKhamDTO.getTenHienThi());
        chiDinhDichVuKhamDTO.setThoiGianChiDinh(tiepNhanDTO.getThoiGianTiepNhan());
        chiDinhDichVuKhamDTO.setTyLeThanhToan(BigDecimal.valueOf(dichVuKhamApDungDTO.getTyLeBhxhThanhToan()));
        chiDinhDichVuKhamDTO.setCoBaoHiem(tiepNhanDTO.getCoBaoHiem());
        chiDinhDichVuKhamDTO.setGiaBhyt(dichVuKhamApDungDTO.getGiaBhyt() == null ? null : dichVuKhamApDungDTO.getGiaBhyt());
        chiDinhDichVuKhamDTO.setGiaKhongBhyt(dichVuKhamApDungDTO.getGiaKhongBhyt() == null ? null : dichVuKhamApDungDTO.getGiaKhongBhyt());
        chiDinhDichVuKhamDTO.setMaDungChung(dichVuKhamDTO.getMaDungChung());
        chiDinhDichVuKhamDTO.setDichVuYeuCau(false);
        chiDinhDichVuKhamDTO.setNam(khamBenhDTO.getThongTinKhamBenhDTO().getNam());
        chiDinhDichVuKhamDTO.setTtkbCompositeId(thongTinKhamBenhId);
        chiDinhDichVuKhamDTO.setDichVuKhamId(dichVuKhamId);

        ChiDinhDichVuKhamDTO result = save(chiDinhDichVuKhamDTO);
        return result;
    }

    @Override
    public ChiDinhDichVuKhamDTO findOneByThongTinKhamBenhId(ThongTinKhamBenhId thongTinKhamBenhId) {
//        LongFilter bakbFilter = new LongFilter();
//        LongFilter benhNhanFilter = new LongFilter();
//        LongFilter donViFilter = new LongFilter();
//        LongFilter dotDieuTriFilter = new LongFilter();
//        LongFilter thongTinKhoaFilter = new LongFilter();
        LongFilter thongTinKhamBenhFilter = new LongFilter();

//        bakbFilter.setEquals(thongTinKhamBenhId.getBakbId());
//        benhNhanFilter.setEquals(thongTinKhamBenhId.getBenhNhanId());
//        donViFilter.setEquals(thongTinKhamBenhId.getDonViId());
//        dotDieuTriFilter.setEquals(thongTinKhamBenhId.getDotDieuTriId());
//        thongTinKhoaFilter.setEquals(thongTinKhamBenhId.getThongTinKhoaId());
        thongTinKhamBenhFilter.setEquals(thongTinKhamBenhId.getId());

        ChiDinhDichVuKhamCriteria criteria = new ChiDinhDichVuKhamCriteria();
        criteria.setTtkbId(thongTinKhamBenhFilter);

        return chiDinhDichVuKhamQueryService.findByCriteria(criteria).get(0);
    }

    @Override
    public ChiDinhDichVuKhamDTO capNhatChiDinhDv(ChiDinhDichVuKhamDTO chiDinhDichVuKhamDTO, Long dichVuKhamId) {
        DichVuKhamDTO dichVuKhamDTO = dichVuKhamService.findOne(dichVuKhamId).get();

        LongFilter dichVuKhamFilter = new LongFilter();
        dichVuKhamFilter.setEquals(dichVuKhamId);
        IntegerFilter enabledFilter = new IntegerFilter();
        enabledFilter.setEquals(1);
        DichVuKhamApDungCriteria dichVuKhamApDungCriteria = new DichVuKhamApDungCriteria();
        dichVuKhamApDungCriteria.setDichVuKhamId(dichVuKhamFilter);
        dichVuKhamApDungCriteria.setEnabled(enabledFilter);
        DichVuKhamApDungDTO dichVuKhamApDungDTO = new DichVuKhamApDungDTO();
        if(dichVuKhamApDungQueryService.countByCriteria(dichVuKhamApDungCriteria) != 0)
            dichVuKhamApDungDTO = dichVuKhamApDungQueryService.findByCriteria(dichVuKhamApDungCriteria).get(0);
        chiDinhDichVuKhamDTO.setDonGia(dichVuKhamDTO.getDonGiaBenhVien());
        chiDinhDichVuKhamDTO.setTen(dichVuKhamDTO.getTenHienThi());
        chiDinhDichVuKhamDTO.setTyLeThanhToan(BigDecimal.valueOf(dichVuKhamApDungDTO.getTyLeBhxhThanhToan()));
        chiDinhDichVuKhamDTO.setGiaBhyt(dichVuKhamApDungDTO.getGiaBhyt() == null ? null : dichVuKhamApDungDTO.getGiaBhyt());
        chiDinhDichVuKhamDTO.setGiaKhongBhyt(dichVuKhamApDungDTO.getGiaKhongBhyt() == null ? null : dichVuKhamApDungDTO.getGiaKhongBhyt());
        chiDinhDichVuKhamDTO.setMaDungChung(dichVuKhamDTO.getMaDungChung());
        chiDinhDichVuKhamDTO.setDichVuKhamId(dichVuKhamId);

        return save(chiDinhDichVuKhamDTO);
    }
}

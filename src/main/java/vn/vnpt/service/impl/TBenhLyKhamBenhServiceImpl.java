package vn.vnpt.service.impl;

import vn.vnpt.service.TBenhLyKhamBenhService;
import vn.vnpt.domain.TBenhLyKhamBenh;
import vn.vnpt.repository.TBenhLyKhamBenhRepository;
import vn.vnpt.service.dto.TBenhLyKhamBenhDTO;
import vn.vnpt.service.mapper.TBenhLyKhamBenhMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link TBenhLyKhamBenh}.
 */
@Service
@Transactional
public class TBenhLyKhamBenhServiceImpl implements TBenhLyKhamBenhService {

    private final Logger log = LoggerFactory.getLogger(TBenhLyKhamBenhServiceImpl.class);

    private final TBenhLyKhamBenhRepository tBenhLyKhamBenhRepository;

    private final TBenhLyKhamBenhMapper tBenhLyKhamBenhMapper;

    public TBenhLyKhamBenhServiceImpl(TBenhLyKhamBenhRepository tBenhLyKhamBenhRepository, TBenhLyKhamBenhMapper tBenhLyKhamBenhMapper) {
        this.tBenhLyKhamBenhRepository = tBenhLyKhamBenhRepository;
        this.tBenhLyKhamBenhMapper = tBenhLyKhamBenhMapper;
    }

    /**
     * Save a tBenhLyKhamBenh.
     *
     * @param tBenhLyKhamBenhDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public TBenhLyKhamBenhDTO save(TBenhLyKhamBenhDTO tBenhLyKhamBenhDTO) {
        log.debug("Request to save TBenhLyKhamBenh : {}", tBenhLyKhamBenhDTO);
        TBenhLyKhamBenh tBenhLyKhamBenh = tBenhLyKhamBenhMapper.toEntity(tBenhLyKhamBenhDTO);
        tBenhLyKhamBenh = tBenhLyKhamBenhRepository.save(tBenhLyKhamBenh);
        return tBenhLyKhamBenhMapper.toDto(tBenhLyKhamBenh);
    }

    /**
     * Get all the tBenhLyKhamBenhs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<TBenhLyKhamBenhDTO> findAll(Pageable pageable) {
        log.debug("Request to get all TBenhLyKhamBenhs");
        return tBenhLyKhamBenhRepository.findAll(pageable)
            .map(tBenhLyKhamBenhMapper::toDto);
    }


    /**
     * Get one tBenhLyKhamBenh by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<TBenhLyKhamBenhDTO> findOne(Long id) {
        log.debug("Request to get TBenhLyKhamBenh : {}", id);
        return tBenhLyKhamBenhRepository.findById(id)
            .map(tBenhLyKhamBenhMapper::toDto);
    }

    /**
     * Delete the tBenhLyKhamBenh by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete TBenhLyKhamBenh : {}", id);
        tBenhLyKhamBenhRepository.deleteById(id);
    }
}

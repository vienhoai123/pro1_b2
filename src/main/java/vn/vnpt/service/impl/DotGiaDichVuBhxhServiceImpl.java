package vn.vnpt.service.impl;

import vn.vnpt.service.DotGiaDichVuBhxhService;
import vn.vnpt.domain.DotGiaDichVuBhxh;
import vn.vnpt.repository.DotGiaDichVuBhxhRepository;
import vn.vnpt.service.dto.DotGiaDichVuBhxhDTO;
import vn.vnpt.service.mapper.DotGiaDichVuBhxhMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link DotGiaDichVuBhxh}.
 */
@Service
@Transactional
public class DotGiaDichVuBhxhServiceImpl implements DotGiaDichVuBhxhService {

    private final Logger log = LoggerFactory.getLogger(DotGiaDichVuBhxhServiceImpl.class);

    private final DotGiaDichVuBhxhRepository dotGiaDichVuBhxhRepository;

    private final DotGiaDichVuBhxhMapper dotGiaDichVuBhxhMapper;

    public DotGiaDichVuBhxhServiceImpl(DotGiaDichVuBhxhRepository dotGiaDichVuBhxhRepository, DotGiaDichVuBhxhMapper dotGiaDichVuBhxhMapper) {
        this.dotGiaDichVuBhxhRepository = dotGiaDichVuBhxhRepository;
        this.dotGiaDichVuBhxhMapper = dotGiaDichVuBhxhMapper;
    }

    /**
     * Save a dotGiaDichVuBhxh.
     *
     * @param dotGiaDichVuBhxhDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public DotGiaDichVuBhxhDTO save(DotGiaDichVuBhxhDTO dotGiaDichVuBhxhDTO) {
        log.debug("Request to save DotGiaDichVuBhxh : {}", dotGiaDichVuBhxhDTO);
        DotGiaDichVuBhxh dotGiaDichVuBhxh = dotGiaDichVuBhxhMapper.toEntity(dotGiaDichVuBhxhDTO);
        dotGiaDichVuBhxh = dotGiaDichVuBhxhRepository.save(dotGiaDichVuBhxh);
        return dotGiaDichVuBhxhMapper.toDto(dotGiaDichVuBhxh);
    }

    /**
     * Get all the dotGiaDichVuBhxhs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<DotGiaDichVuBhxhDTO> findAll(Pageable pageable) {
        log.debug("Request to get all DotGiaDichVuBhxhs");
        return dotGiaDichVuBhxhRepository.findAll(pageable)
            .map(dotGiaDichVuBhxhMapper::toDto);
    }

    /**
     * Get one dotGiaDichVuBhxh by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<DotGiaDichVuBhxhDTO> findOne(Long id) {
        log.debug("Request to get DotGiaDichVuBhxh : {}", id);
        return dotGiaDichVuBhxhRepository.findById(id)
            .map(dotGiaDichVuBhxhMapper::toDto);
    }

    /**
     * Delete the dotGiaDichVuBhxh by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete DotGiaDichVuBhxh : {}", id);
        dotGiaDichVuBhxhRepository.deleteById(id);
    }
}

package vn.vnpt.service.impl;

import vn.vnpt.service.LoaiBenhLyService;
import vn.vnpt.domain.LoaiBenhLy;
import vn.vnpt.repository.LoaiBenhLyRepository;
import vn.vnpt.service.dto.LoaiBenhLyDTO;
import vn.vnpt.service.mapper.LoaiBenhLyMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link LoaiBenhLy}.
 */
@Service
@Transactional
public class LoaiBenhLyServiceImpl implements LoaiBenhLyService {

    private final Logger log = LoggerFactory.getLogger(LoaiBenhLyServiceImpl.class);

    private final LoaiBenhLyRepository loaiBenhLyRepository;

    private final LoaiBenhLyMapper loaiBenhLyMapper;

    public LoaiBenhLyServiceImpl(LoaiBenhLyRepository loaiBenhLyRepository, LoaiBenhLyMapper loaiBenhLyMapper) {
        this.loaiBenhLyRepository = loaiBenhLyRepository;
        this.loaiBenhLyMapper = loaiBenhLyMapper;
    }

    /**
     * Save a loaiBenhLy.
     *
     * @param loaiBenhLyDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public LoaiBenhLyDTO save(LoaiBenhLyDTO loaiBenhLyDTO) {
        log.debug("Request to save LoaiBenhLy : {}", loaiBenhLyDTO);
        LoaiBenhLy loaiBenhLy = loaiBenhLyMapper.toEntity(loaiBenhLyDTO);
        loaiBenhLy = loaiBenhLyRepository.save(loaiBenhLy);
        return loaiBenhLyMapper.toDto(loaiBenhLy);
    }

    /**
     * Get all the loaiBenhLies.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<LoaiBenhLyDTO> findAll(Pageable pageable) {
        log.debug("Request to get all LoaiBenhLies");
        return loaiBenhLyRepository.findAll(pageable)
            .map(loaiBenhLyMapper::toDto);
    }

    /**
     * Get one loaiBenhLy by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<LoaiBenhLyDTO> findOne(Long id) {
        log.debug("Request to get LoaiBenhLy : {}", id);
        return loaiBenhLyRepository.findById(id)
            .map(loaiBenhLyMapper::toDto);
    }

    /**
     * Delete the loaiBenhLy by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete LoaiBenhLy : {}", id);
        loaiBenhLyRepository.deleteById(id);
    }
}

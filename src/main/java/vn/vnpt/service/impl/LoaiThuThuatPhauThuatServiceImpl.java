package vn.vnpt.service.impl;

import vn.vnpt.service.LoaiThuThuatPhauThuatService;
import vn.vnpt.domain.LoaiThuThuatPhauThuat;
import vn.vnpt.repository.LoaiThuThuatPhauThuatRepository;
import vn.vnpt.service.dto.LoaiThuThuatPhauThuatDTO;
import vn.vnpt.service.mapper.LoaiThuThuatPhauThuatMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link LoaiThuThuatPhauThuat}.
 */
@Service
@Transactional
public class LoaiThuThuatPhauThuatServiceImpl implements LoaiThuThuatPhauThuatService {

    private final Logger log = LoggerFactory.getLogger(LoaiThuThuatPhauThuatServiceImpl.class);

    private final LoaiThuThuatPhauThuatRepository loaiThuThuatPhauThuatRepository;

    private final LoaiThuThuatPhauThuatMapper loaiThuThuatPhauThuatMapper;

    public LoaiThuThuatPhauThuatServiceImpl(LoaiThuThuatPhauThuatRepository loaiThuThuatPhauThuatRepository, LoaiThuThuatPhauThuatMapper loaiThuThuatPhauThuatMapper) {
        this.loaiThuThuatPhauThuatRepository = loaiThuThuatPhauThuatRepository;
        this.loaiThuThuatPhauThuatMapper = loaiThuThuatPhauThuatMapper;
    }

    /**
     * Save a loaiThuThuatPhauThuat.
     *
     * @param loaiThuThuatPhauThuatDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public LoaiThuThuatPhauThuatDTO save(LoaiThuThuatPhauThuatDTO loaiThuThuatPhauThuatDTO) {
        log.debug("Request to save LoaiThuThuatPhauThuat : {}", loaiThuThuatPhauThuatDTO);
        LoaiThuThuatPhauThuat loaiThuThuatPhauThuat = loaiThuThuatPhauThuatMapper.toEntity(loaiThuThuatPhauThuatDTO);
        loaiThuThuatPhauThuat = loaiThuThuatPhauThuatRepository.save(loaiThuThuatPhauThuat);
        return loaiThuThuatPhauThuatMapper.toDto(loaiThuThuatPhauThuat);
    }

    /**
     * Get all the loaiThuThuatPhauThuats.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<LoaiThuThuatPhauThuatDTO> findAll(Pageable pageable) {
        log.debug("Request to get all LoaiThuThuatPhauThuats");
        return loaiThuThuatPhauThuatRepository.findAll(pageable)
            .map(loaiThuThuatPhauThuatMapper::toDto);
    }

    /**
     * Get one loaiThuThuatPhauThuat by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<LoaiThuThuatPhauThuatDTO> findOne(Long id) {
        log.debug("Request to get LoaiThuThuatPhauThuat : {}", id);
        return loaiThuThuatPhauThuatRepository.findById(id)
            .map(loaiThuThuatPhauThuatMapper::toDto);
    }

    /**
     * Delete the loaiThuThuatPhauThuat by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete LoaiThuThuatPhauThuat : {}", id);
        loaiThuThuatPhauThuatRepository.deleteById(id);
    }
}

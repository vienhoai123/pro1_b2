package vn.vnpt.service.impl;

import vn.vnpt.service.NhomChanDoanHinhAnhService;
import vn.vnpt.domain.NhomChanDoanHinhAnh;
import vn.vnpt.repository.NhomChanDoanHinhAnhRepository;
import vn.vnpt.service.dto.NhomChanDoanHinhAnhDTO;
import vn.vnpt.service.mapper.NhomChanDoanHinhAnhMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link NhomChanDoanHinhAnh}.
 */
@Service
@Transactional
public class NhomChanDoanHinhAnhServiceImpl implements NhomChanDoanHinhAnhService {

    private final Logger log = LoggerFactory.getLogger(NhomChanDoanHinhAnhServiceImpl.class);

    private final NhomChanDoanHinhAnhRepository nhomChanDoanHinhAnhRepository;

    private final NhomChanDoanHinhAnhMapper nhomChanDoanHinhAnhMapper;

    public NhomChanDoanHinhAnhServiceImpl(NhomChanDoanHinhAnhRepository nhomChanDoanHinhAnhRepository, NhomChanDoanHinhAnhMapper nhomChanDoanHinhAnhMapper) {
        this.nhomChanDoanHinhAnhRepository = nhomChanDoanHinhAnhRepository;
        this.nhomChanDoanHinhAnhMapper = nhomChanDoanHinhAnhMapper;
    }

    /**
     * Save a nhomChanDoanHinhAnh.
     *
     * @param nhomChanDoanHinhAnhDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public NhomChanDoanHinhAnhDTO save(NhomChanDoanHinhAnhDTO nhomChanDoanHinhAnhDTO) {
        log.debug("Request to save NhomChanDoanHinhAnh : {}", nhomChanDoanHinhAnhDTO);
        NhomChanDoanHinhAnh nhomChanDoanHinhAnh = nhomChanDoanHinhAnhMapper.toEntity(nhomChanDoanHinhAnhDTO);
        nhomChanDoanHinhAnh = nhomChanDoanHinhAnhRepository.save(nhomChanDoanHinhAnh);
        return nhomChanDoanHinhAnhMapper.toDto(nhomChanDoanHinhAnh);
    }

    /**
     * Get all the nhomChanDoanHinhAnhs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<NhomChanDoanHinhAnhDTO> findAll(Pageable pageable) {
        log.debug("Request to get all NhomChanDoanHinhAnhs");
        return nhomChanDoanHinhAnhRepository.findAll(pageable)
            .map(nhomChanDoanHinhAnhMapper::toDto);
    }

    /**
     * Get one nhomChanDoanHinhAnh by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<NhomChanDoanHinhAnhDTO> findOne(Long id) {
        log.debug("Request to get NhomChanDoanHinhAnh : {}", id);
        return nhomChanDoanHinhAnhRepository.findById(id)
            .map(nhomChanDoanHinhAnhMapper::toDto);
    }

    /**
     * Delete the nhomChanDoanHinhAnh by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete NhomChanDoanHinhAnh : {}", id);
        nhomChanDoanHinhAnhRepository.deleteById(id);
    }
}

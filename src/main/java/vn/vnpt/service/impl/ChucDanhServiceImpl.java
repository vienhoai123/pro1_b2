package vn.vnpt.service.impl;

import vn.vnpt.service.ChucDanhService;
import vn.vnpt.domain.ChucDanh;
import vn.vnpt.repository.ChucDanhRepository;
import vn.vnpt.service.dto.ChucDanhDTO;
import vn.vnpt.service.mapper.ChucDanhMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ChucDanh}.
 */
@Service
@Transactional
public class ChucDanhServiceImpl implements ChucDanhService {

    private final Logger log = LoggerFactory.getLogger(ChucDanhServiceImpl.class);

    private final ChucDanhRepository chucDanhRepository;

    private final ChucDanhMapper chucDanhMapper;

    public ChucDanhServiceImpl(ChucDanhRepository chucDanhRepository, ChucDanhMapper chucDanhMapper) {
        this.chucDanhRepository = chucDanhRepository;
        this.chucDanhMapper = chucDanhMapper;
    }

    /**
     * Save a chucDanh.
     *
     * @param chucDanhDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ChucDanhDTO save(ChucDanhDTO chucDanhDTO) {
        log.debug("Request to save ChucDanh : {}", chucDanhDTO);
        ChucDanh chucDanh = chucDanhMapper.toEntity(chucDanhDTO);
        chucDanh = chucDanhRepository.save(chucDanh);
        return chucDanhMapper.toDto(chucDanh);
    }

    /**
     * Get all the chucDanhs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ChucDanhDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ChucDanhs");
        return chucDanhRepository.findAll(pageable)
            .map(chucDanhMapper::toDto);
    }

    /**
     * Get one chucDanh by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ChucDanhDTO> findOne(Long id) {
        log.debug("Request to get ChucDanh : {}", id);
        return chucDanhRepository.findById(id)
            .map(chucDanhMapper::toDto);
    }

    /**
     * Delete the chucDanh by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ChucDanh : {}", id);
        chucDanhRepository.deleteById(id);
    }
}

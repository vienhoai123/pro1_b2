package vn.vnpt.service.impl;

import vn.vnpt.service.TPhanNhomTTPTService;
import vn.vnpt.domain.TPhanNhomTTPT;
import vn.vnpt.repository.TPhanNhomTTPTRepository;
import vn.vnpt.service.dto.TPhanNhomTTPTDTO;
import vn.vnpt.service.mapper.TPhanNhomTTPTMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link TPhanNhomTTPT}.
 */
@Service
@Transactional
public class TPhanNhomTTPTServiceImpl implements TPhanNhomTTPTService {

    private final Logger log = LoggerFactory.getLogger(TPhanNhomTTPTServiceImpl.class);

    private final TPhanNhomTTPTRepository tPhanNhomTTPTRepository;

    private final TPhanNhomTTPTMapper tPhanNhomTTPTMapper;

    public TPhanNhomTTPTServiceImpl(TPhanNhomTTPTRepository tPhanNhomTTPTRepository, TPhanNhomTTPTMapper tPhanNhomTTPTMapper) {
        this.tPhanNhomTTPTRepository = tPhanNhomTTPTRepository;
        this.tPhanNhomTTPTMapper = tPhanNhomTTPTMapper;
    }

    /**
     * Save a tPhanNhomTTPT.
     *
     * @param tPhanNhomTTPTDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public TPhanNhomTTPTDTO save(TPhanNhomTTPTDTO tPhanNhomTTPTDTO) {
        log.debug("Request to save TPhanNhomTTPT : {}", tPhanNhomTTPTDTO);
        TPhanNhomTTPT tPhanNhomTTPT = tPhanNhomTTPTMapper.toEntity(tPhanNhomTTPTDTO);
        tPhanNhomTTPT = tPhanNhomTTPTRepository.save(tPhanNhomTTPT);
        return tPhanNhomTTPTMapper.toDto(tPhanNhomTTPT);
    }

    /**
     * Get all the tPhanNhomTTPTS.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<TPhanNhomTTPTDTO> findAll(Pageable pageable) {
        log.debug("Request to get all TPhanNhomTTPTS");
        return tPhanNhomTTPTRepository.findAll(pageable)
            .map(tPhanNhomTTPTMapper::toDto);
    }

    /**
     * Get one tPhanNhomTTPT by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<TPhanNhomTTPTDTO> findOne(Long id) {
        log.debug("Request to get TPhanNhomTTPT : {}", id);
        return tPhanNhomTTPTRepository.findById(id)
            .map(tPhanNhomTTPTMapper::toDto);
    }

    /**
     * Delete the tPhanNhomTTPT by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete TPhanNhomTTPT : {}", id);
        tPhanNhomTTPTRepository.deleteById(id);
    }
}

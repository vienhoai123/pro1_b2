package vn.vnpt.service.impl;

import vn.vnpt.service.TPhanNhomCDHAService;
import vn.vnpt.domain.TPhanNhomCDHA;
import vn.vnpt.repository.TPhanNhomCDHARepository;
import vn.vnpt.service.dto.TPhanNhomCDHADTO;
import vn.vnpt.service.mapper.TPhanNhomCDHAMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link TPhanNhomCDHA}.
 */
@Service
@Transactional
public class TPhanNhomCDHAServiceImpl implements TPhanNhomCDHAService {

    private final Logger log = LoggerFactory.getLogger(TPhanNhomCDHAServiceImpl.class);

    private final TPhanNhomCDHARepository tPhanNhomCDHARepository;

    private final TPhanNhomCDHAMapper tPhanNhomCDHAMapper;

    public TPhanNhomCDHAServiceImpl(TPhanNhomCDHARepository tPhanNhomCDHARepository, TPhanNhomCDHAMapper tPhanNhomCDHAMapper) {
        this.tPhanNhomCDHARepository = tPhanNhomCDHARepository;
        this.tPhanNhomCDHAMapper = tPhanNhomCDHAMapper;
    }

    /**
     * Save a tPhanNhomCDHA.
     *
     * @param tPhanNhomCDHADTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public TPhanNhomCDHADTO save(TPhanNhomCDHADTO tPhanNhomCDHADTO) {
        log.debug("Request to save TPhanNhomCDHA : {}", tPhanNhomCDHADTO);
        TPhanNhomCDHA tPhanNhomCDHA = tPhanNhomCDHAMapper.toEntity(tPhanNhomCDHADTO);
        tPhanNhomCDHA = tPhanNhomCDHARepository.save(tPhanNhomCDHA);
        return tPhanNhomCDHAMapper.toDto(tPhanNhomCDHA);
    }

    /**
     * Get all the tPhanNhomCDHAS.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<TPhanNhomCDHADTO> findAll(Pageable pageable) {
        log.debug("Request to get all TPhanNhomCDHAS");
        return tPhanNhomCDHARepository.findAll(pageable)
            .map(tPhanNhomCDHAMapper::toDto);
    }

    /**
     * Get one tPhanNhomCDHA by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<TPhanNhomCDHADTO> findOne(Long id) {
        log.debug("Request to get TPhanNhomCDHA : {}", id);
        return tPhanNhomCDHARepository.findById(id)
            .map(tPhanNhomCDHAMapper::toDto);
    }

    /**
     * Delete the tPhanNhomCDHA by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete TPhanNhomCDHA : {}", id);
        tPhanNhomCDHARepository.deleteById(id);
    }
}

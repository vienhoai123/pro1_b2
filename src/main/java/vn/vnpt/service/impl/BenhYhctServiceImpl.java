package vn.vnpt.service.impl;

import vn.vnpt.service.BenhYhctService;
import vn.vnpt.domain.BenhYhct;
import vn.vnpt.repository.BenhYhctRepository;
import vn.vnpt.service.dto.BenhYhctDTO;
import vn.vnpt.service.mapper.BenhYhctMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link BenhYhct}.
 */
@Service
@Transactional
public class BenhYhctServiceImpl implements BenhYhctService {

    private final Logger log = LoggerFactory.getLogger(BenhYhctServiceImpl.class);

    private final BenhYhctRepository benhYhctRepository;

    private final BenhYhctMapper benhYhctMapper;

    public BenhYhctServiceImpl(BenhYhctRepository benhYhctRepository, BenhYhctMapper benhYhctMapper) {
        this.benhYhctRepository = benhYhctRepository;
        this.benhYhctMapper = benhYhctMapper;
    }

    /**
     * Save a benhYhct.
     *
     * @param benhYhctDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public BenhYhctDTO save(BenhYhctDTO benhYhctDTO) {
        log.debug("Request to save BenhYhct : {}", benhYhctDTO);
        BenhYhct benhYhct = benhYhctMapper.toEntity(benhYhctDTO);
        benhYhct = benhYhctRepository.save(benhYhct);
        return benhYhctMapper.toDto(benhYhct);
    }

    /**
     * Get all the benhYhcts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<BenhYhctDTO> findAll(Pageable pageable) {
        log.debug("Request to get all BenhYhcts");
        return benhYhctRepository.findAll(pageable)
            .map(benhYhctMapper::toDto);
    }

    /**
     * Get one benhYhct by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<BenhYhctDTO> findOne(Long id) {
        log.debug("Request to get BenhYhct : {}", id);
        return benhYhctRepository.findById(id)
            .map(benhYhctMapper::toDto);
    }

    /**
     * Delete the benhYhct by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete BenhYhct : {}", id);
        benhYhctRepository.deleteById(id);
    }
}

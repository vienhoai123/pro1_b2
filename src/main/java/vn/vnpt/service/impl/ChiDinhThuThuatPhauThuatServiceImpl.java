package vn.vnpt.service.impl;

import vn.vnpt.domain.ChiDinhTTPTId;
import vn.vnpt.service.ChiDinhThuThuatPhauThuatService;
import vn.vnpt.domain.ChiDinhThuThuatPhauThuat;
import vn.vnpt.repository.ChiDinhThuThuatPhauThuatRepository;
import vn.vnpt.service.dto.ChiDinhThuThuatPhauThuatDTO;
import vn.vnpt.service.mapper.ChiDinhThuThuatPhauThuatMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ChiDinhThuThuatPhauThuat}.
 */
@Service
@Transactional
public class ChiDinhThuThuatPhauThuatServiceImpl implements ChiDinhThuThuatPhauThuatService {

    private final Logger log = LoggerFactory.getLogger(ChiDinhThuThuatPhauThuatServiceImpl.class);

    private final ChiDinhThuThuatPhauThuatRepository chiDinhThuThuatPhauThuatRepository;

    private final ChiDinhThuThuatPhauThuatMapper chiDinhThuThuatPhauThuatMapper;

    public ChiDinhThuThuatPhauThuatServiceImpl(ChiDinhThuThuatPhauThuatRepository chiDinhThuThuatPhauThuatRepository, ChiDinhThuThuatPhauThuatMapper chiDinhThuThuatPhauThuatMapper) {
        this.chiDinhThuThuatPhauThuatRepository = chiDinhThuThuatPhauThuatRepository;
        this.chiDinhThuThuatPhauThuatMapper = chiDinhThuThuatPhauThuatMapper;
    }

    /**
     * Save a chiDinhThuThuatPhauThuat.
     *
     * @param chiDinhThuThuatPhauThuatDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ChiDinhThuThuatPhauThuatDTO save(ChiDinhThuThuatPhauThuatDTO chiDinhThuThuatPhauThuatDTO) {
        log.debug("Request to save ChiDinhThuThuatPhauThuat : {}", chiDinhThuThuatPhauThuatDTO);
        ChiDinhThuThuatPhauThuat chiDinhThuThuatPhauThuat = chiDinhThuThuatPhauThuatMapper.toEntity(chiDinhThuThuatPhauThuatDTO);
        chiDinhThuThuatPhauThuat = chiDinhThuThuatPhauThuatRepository.save(chiDinhThuThuatPhauThuat);
        return chiDinhThuThuatPhauThuatMapper.toDto(chiDinhThuThuatPhauThuat);
    }

    /**
     * Get all the chiDinhThuThuatPhauThuats.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ChiDinhThuThuatPhauThuatDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ChiDinhThuThuatPhauThuats");
        return chiDinhThuThuatPhauThuatRepository.findAll(pageable)
            .map(chiDinhThuThuatPhauThuatMapper::toDto);
    }

    /**
     * Get one chiDinhThuThuatPhauThuat by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ChiDinhThuThuatPhauThuatDTO> findOne(ChiDinhTTPTId id) {
        log.debug("Request to get ChiDinhThuThuatPhauThuat : {}", id);
        return chiDinhThuThuatPhauThuatRepository.findById(id)
            .map(chiDinhThuThuatPhauThuatMapper::toDto);
    }

    /**
     * Delete the chiDinhThuThuatPhauThuat by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(ChiDinhTTPTId id) {
        log.debug("Request to delete ChiDinhThuThuatPhauThuat : {}", id);
        chiDinhThuThuatPhauThuatRepository.deleteById(id);
    }
}

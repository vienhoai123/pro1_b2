package vn.vnpt.service.impl;

import vn.vnpt.service.NhanVienService;
import vn.vnpt.domain.NhanVien;
import vn.vnpt.repository.NhanVienRepository;
import vn.vnpt.service.dto.NhanVienDTO;
import vn.vnpt.service.mapper.NhanVienMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link NhanVien}.
 */
@Service
@Transactional
public class NhanVienServiceImpl implements NhanVienService {

    private final Logger log = LoggerFactory.getLogger(NhanVienServiceImpl.class);

    private final NhanVienRepository nhanVienRepository;

    private final NhanVienMapper nhanVienMapper;

    public NhanVienServiceImpl(NhanVienRepository nhanVienRepository, NhanVienMapper nhanVienMapper) {
        this.nhanVienRepository = nhanVienRepository;
        this.nhanVienMapper = nhanVienMapper;
    }

    /**
     * Save a nhanVien.
     *
     * @param nhanVienDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public NhanVienDTO save(NhanVienDTO nhanVienDTO) {
        log.debug("Request to save NhanVien : {}", nhanVienDTO);
        NhanVien nhanVien = nhanVienMapper.toEntity(nhanVienDTO);
        nhanVien = nhanVienRepository.save(nhanVien);
        return nhanVienMapper.toDto(nhanVien);
    }

    /**
     * Get all the nhanViens.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<NhanVienDTO> findAll(Pageable pageable) {
        log.debug("Request to get all NhanViens");
        return nhanVienRepository.findAll(pageable)
            .map(nhanVienMapper::toDto);
    }


    /**
     * Get all the nhanViens with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<NhanVienDTO> findAllWithEagerRelationships(Pageable pageable) {
        return nhanVienRepository.findAllWithEagerRelationships(pageable).map(nhanVienMapper::toDto);
    }

    /**
     * Get one nhanVien by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<NhanVienDTO> findOne(Long id) {
        log.debug("Request to get NhanVien : {}", id);
        return nhanVienRepository.findOneWithEagerRelationships(id)
            .map(nhanVienMapper::toDto);
    }

    /**
     * Delete the nhanVien by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete NhanVien : {}", id);
        nhanVienRepository.deleteById(id);
    }
}

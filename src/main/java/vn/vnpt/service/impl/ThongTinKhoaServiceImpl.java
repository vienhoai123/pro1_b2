package vn.vnpt.service.impl;

import vn.vnpt.domain.ThongTinKhoaId;
import vn.vnpt.service.ThongTinKhoaService;
import vn.vnpt.domain.ThongTinKhoa;
import vn.vnpt.repository.ThongTinKhoaRepository;
import vn.vnpt.service.dto.ThongTinKhoaDTO;
import vn.vnpt.service.mapper.ThongTinKhoaMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ThongTinKhoa}.
 */
@Service
@Transactional
public class ThongTinKhoaServiceImpl implements ThongTinKhoaService {

    private final Logger log = LoggerFactory.getLogger(ThongTinKhoaServiceImpl.class);

    private final ThongTinKhoaRepository thongTinKhoaRepository;

    private final ThongTinKhoaMapper thongTinKhoaMapper;

    public ThongTinKhoaServiceImpl(ThongTinKhoaRepository thongTinKhoaRepository, ThongTinKhoaMapper thongTinKhoaMapper) {
        this.thongTinKhoaRepository = thongTinKhoaRepository;
        this.thongTinKhoaMapper = thongTinKhoaMapper;
    }

    /**
     * Save a thongTinKhoa.
     *
     * @param thongTinKhoaDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ThongTinKhoaDTO save(ThongTinKhoaDTO thongTinKhoaDTO) {
        log.debug("Request to save ThongTinKhoa : {}", thongTinKhoaDTO);
        ThongTinKhoa thongTinKhoa = thongTinKhoaMapper.toEntity(thongTinKhoaDTO);
        thongTinKhoa = thongTinKhoaRepository.save(thongTinKhoa);
        return thongTinKhoaMapper.toDto(thongTinKhoa);
    }

    /**
     * Get all the thongTinKhoas.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ThongTinKhoaDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ThongTinKhoas");
        return thongTinKhoaRepository.findAll(pageable)
            .map(thongTinKhoaMapper::toDto);
    }

    /**
     * Get one thongTinKhoa by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ThongTinKhoaDTO> findOne(ThongTinKhoaId id) {
        log.debug("Request to get ThongTinKhoa : {}", id);
        return thongTinKhoaRepository.findById(id)
            .map(thongTinKhoaMapper::toDto);
    }

    /**
     * Delete the thongTinKhoa by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(ThongTinKhoaId id) {
        log.debug("Request to delete ThongTinKhoa : {}", id);
        thongTinKhoaRepository.deleteById(id);
    }
}

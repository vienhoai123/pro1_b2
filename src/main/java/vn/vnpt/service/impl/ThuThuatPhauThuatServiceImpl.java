package vn.vnpt.service.impl;

import vn.vnpt.service.ThuThuatPhauThuatService;
import vn.vnpt.domain.ThuThuatPhauThuat;
import vn.vnpt.repository.ThuThuatPhauThuatRepository;
import vn.vnpt.service.dto.ThuThuatPhauThuatDTO;
import vn.vnpt.service.mapper.ThuThuatPhauThuatMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ThuThuatPhauThuat}.
 */
@Service
@Transactional
public class ThuThuatPhauThuatServiceImpl implements ThuThuatPhauThuatService {

    private final Logger log = LoggerFactory.getLogger(ThuThuatPhauThuatServiceImpl.class);

    private final ThuThuatPhauThuatRepository thuThuatPhauThuatRepository;

    private final ThuThuatPhauThuatMapper thuThuatPhauThuatMapper;

    public ThuThuatPhauThuatServiceImpl(ThuThuatPhauThuatRepository thuThuatPhauThuatRepository, ThuThuatPhauThuatMapper thuThuatPhauThuatMapper) {
        this.thuThuatPhauThuatRepository = thuThuatPhauThuatRepository;
        this.thuThuatPhauThuatMapper = thuThuatPhauThuatMapper;
    }

    /**
     * Save a thuThuatPhauThuat.
     *
     * @param thuThuatPhauThuatDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ThuThuatPhauThuatDTO save(ThuThuatPhauThuatDTO thuThuatPhauThuatDTO) {
        log.debug("Request to save ThuThuatPhauThuat : {}", thuThuatPhauThuatDTO);
        ThuThuatPhauThuat thuThuatPhauThuat = thuThuatPhauThuatMapper.toEntity(thuThuatPhauThuatDTO);
        thuThuatPhauThuat = thuThuatPhauThuatRepository.save(thuThuatPhauThuat);
        return thuThuatPhauThuatMapper.toDto(thuThuatPhauThuat);
    }

    /**
     * Get all the thuThuatPhauThuats.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ThuThuatPhauThuatDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ThuThuatPhauThuats");
        return thuThuatPhauThuatRepository.findAll(pageable)
            .map(thuThuatPhauThuatMapper::toDto);
    }

    /**
     * Get one thuThuatPhauThuat by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ThuThuatPhauThuatDTO> findOne(Long id) {
        log.debug("Request to get ThuThuatPhauThuat : {}", id);
        return thuThuatPhauThuatRepository.findById(id)
            .map(thuThuatPhauThuatMapper::toDto);
    }

    /**
     * Delete the thuThuatPhauThuat by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ThuThuatPhauThuat : {}", id);
        thuThuatPhauThuatRepository.deleteById(id);
    }
}

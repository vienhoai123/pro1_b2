package vn.vnpt.service.impl;

import vn.vnpt.service.DichVuKhamApDungService;
import vn.vnpt.domain.DichVuKhamApDung;
import vn.vnpt.repository.DichVuKhamApDungRepository;
import vn.vnpt.service.dto.DichVuKhamApDungDTO;
import vn.vnpt.service.mapper.DichVuKhamApDungMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link DichVuKhamApDung}.
 */
@Service
@Transactional
public class DichVuKhamApDungServiceImpl implements DichVuKhamApDungService {

    private final Logger log = LoggerFactory.getLogger(DichVuKhamApDungServiceImpl.class);

    private final DichVuKhamApDungRepository dichVuKhamApDungRepository;

    private final DichVuKhamApDungMapper dichVuKhamApDungMapper;

    public DichVuKhamApDungServiceImpl(DichVuKhamApDungRepository dichVuKhamApDungRepository, DichVuKhamApDungMapper dichVuKhamApDungMapper) {
        this.dichVuKhamApDungRepository = dichVuKhamApDungRepository;
        this.dichVuKhamApDungMapper = dichVuKhamApDungMapper;
    }

    /**
     * Save a dichVuKhamApDung.
     *
     * @param dichVuKhamApDungDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public DichVuKhamApDungDTO save(DichVuKhamApDungDTO dichVuKhamApDungDTO) {
        log.debug("Request to save DichVuKhamApDung : {}", dichVuKhamApDungDTO);
        DichVuKhamApDung dichVuKhamApDung = dichVuKhamApDungMapper.toEntity(dichVuKhamApDungDTO);
        dichVuKhamApDung = dichVuKhamApDungRepository.save(dichVuKhamApDung);
        return dichVuKhamApDungMapper.toDto(dichVuKhamApDung);
    }

    /**
     * Get all the dichVuKhamApDungs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<DichVuKhamApDungDTO> findAll(Pageable pageable) {
        log.debug("Request to get all DichVuKhamApDungs");
        return dichVuKhamApDungRepository.findAll(pageable)
            .map(dichVuKhamApDungMapper::toDto);
    }

    /**
     * Get one dichVuKhamApDung by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<DichVuKhamApDungDTO> findOne(Long id) {
        log.debug("Request to get DichVuKhamApDung : {}", id);
        return dichVuKhamApDungRepository.findById(id)
            .map(dichVuKhamApDungMapper::toDto);
    }

    /**
     * Delete the dichVuKhamApDung by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete DichVuKhamApDung : {}", id);
        dichVuKhamApDungRepository.deleteById(id);
    }
}

package vn.vnpt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import vn.vnpt.domain.DoiTuongBhyt;
import vn.vnpt.domain.*; // for static metamodels
import vn.vnpt.repository.DoiTuongBhytRepository;
import vn.vnpt.service.dto.DoiTuongBhytCriteria;
import vn.vnpt.service.dto.DoiTuongBhytDTO;
import vn.vnpt.service.mapper.DoiTuongBhytMapper;

/**
 * Service for executing complex queries for {@link DoiTuongBhyt} entities in the database.
 * The main input is a {@link DoiTuongBhytCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link DoiTuongBhytDTO} or a {@link Page} of {@link DoiTuongBhytDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class DoiTuongBhytQueryService extends QueryService<DoiTuongBhyt> {

    private final Logger log = LoggerFactory.getLogger(DoiTuongBhytQueryService.class);

    private final DoiTuongBhytRepository doiTuongBhytRepository;

    private final DoiTuongBhytMapper doiTuongBhytMapper;

    public DoiTuongBhytQueryService(DoiTuongBhytRepository doiTuongBhytRepository, DoiTuongBhytMapper doiTuongBhytMapper) {
        this.doiTuongBhytRepository = doiTuongBhytRepository;
        this.doiTuongBhytMapper = doiTuongBhytMapper;
    }

    /**
     * Return a {@link List} of {@link DoiTuongBhytDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<DoiTuongBhytDTO> findByCriteria(DoiTuongBhytCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<DoiTuongBhyt> specification = createSpecification(criteria);
        return doiTuongBhytMapper.toDto(doiTuongBhytRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link DoiTuongBhytDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<DoiTuongBhytDTO> findByCriteria(DoiTuongBhytCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<DoiTuongBhyt> specification = createSpecification(criteria);
        return doiTuongBhytRepository.findAll(specification, page)
            .map(doiTuongBhytMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(DoiTuongBhytCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<DoiTuongBhyt> specification = createSpecification(criteria);
        return doiTuongBhytRepository.count(specification);
    }

    /**
     * Function to convert {@link DoiTuongBhytCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<DoiTuongBhyt> createSpecification(DoiTuongBhytCriteria criteria) {
        Specification<DoiTuongBhyt> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), DoiTuongBhyt_.id));
            }
            if (criteria.getCanTren() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCanTren(), DoiTuongBhyt_.canTren));
            }
            if (criteria.getCanTrenKtc() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCanTrenKtc(), DoiTuongBhyt_.canTrenKtc));
            }
            if (criteria.getMa() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMa(), DoiTuongBhyt_.ma));
            }
            if (criteria.getTen() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTen(), DoiTuongBhyt_.ten));
            }
            if (criteria.getTyLeMienGiam() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTyLeMienGiam(), DoiTuongBhyt_.tyLeMienGiam));
            }
            if (criteria.getTyLeMienGiamKtc() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTyLeMienGiamKtc(), DoiTuongBhyt_.tyLeMienGiamKtc));
            }
            if (criteria.getNhomDoiTuongBhytId() != null) {
                specification = specification.and(buildSpecification(criteria.getNhomDoiTuongBhytId(),
                    root -> root.join(DoiTuongBhyt_.nhomDoiTuongBhyt, JoinType.LEFT).get(NhomDoiTuongBhyt_.id)));
            }
        }
        return specification;
    }
}

package vn.vnpt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import vn.vnpt.domain.DichVuKhamApDung;
import vn.vnpt.domain.*; // for static metamodels
import vn.vnpt.repository.DichVuKhamApDungRepository;
import vn.vnpt.service.dto.DichVuKhamApDungCriteria;
import vn.vnpt.service.dto.DichVuKhamApDungDTO;
import vn.vnpt.service.mapper.DichVuKhamApDungMapper;

/**
 * Service for executing complex queries for {@link DichVuKhamApDung} entities in the database.
 * The main input is a {@link DichVuKhamApDungCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link DichVuKhamApDungDTO} or a {@link Page} of {@link DichVuKhamApDungDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class DichVuKhamApDungQueryService extends QueryService<DichVuKhamApDung> {

    private final Logger log = LoggerFactory.getLogger(DichVuKhamApDungQueryService.class);

    private final DichVuKhamApDungRepository dichVuKhamApDungRepository;

    private final DichVuKhamApDungMapper dichVuKhamApDungMapper;

    public DichVuKhamApDungQueryService(DichVuKhamApDungRepository dichVuKhamApDungRepository, DichVuKhamApDungMapper dichVuKhamApDungMapper) {
        this.dichVuKhamApDungRepository = dichVuKhamApDungRepository;
        this.dichVuKhamApDungMapper = dichVuKhamApDungMapper;
    }

    /**
     * Return a {@link List} of {@link DichVuKhamApDungDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<DichVuKhamApDungDTO> findByCriteria(DichVuKhamApDungCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<DichVuKhamApDung> specification = createSpecification(criteria);
        return dichVuKhamApDungMapper.toDto(dichVuKhamApDungRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link DichVuKhamApDungDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<DichVuKhamApDungDTO> findByCriteria(DichVuKhamApDungCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<DichVuKhamApDung> specification = createSpecification(criteria);
        return dichVuKhamApDungRepository.findAll(specification, page)
            .map(dichVuKhamApDungMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(DichVuKhamApDungCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<DichVuKhamApDung> specification = createSpecification(criteria);
        return dichVuKhamApDungRepository.count(specification);
    }

    /**
     * Function to convert {@link DichVuKhamApDungCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<DichVuKhamApDung> createSpecification(DichVuKhamApDungCriteria criteria) {
        Specification<DichVuKhamApDung> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), DichVuKhamApDung_.id));
            }
            if (criteria.getEnabled() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getEnabled(), DichVuKhamApDung_.enabled));
            }
            if (criteria.getGiaBhyt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getGiaBhyt(), DichVuKhamApDung_.giaBhyt));
            }
            if (criteria.getGiaKhongBhyt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getGiaKhongBhyt(), DichVuKhamApDung_.giaKhongBhyt));
            }
            if (criteria.getMaBaoCaoBhyt() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMaBaoCaoBhyt(), DichVuKhamApDung_.maBaoCaoBhyt));
            }
            if (criteria.getMaBaoCaoByt() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMaBaoCaoByt(), DichVuKhamApDung_.maBaoCaoByt));
            }
            if (criteria.getSoCongVanBhxh() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSoCongVanBhxh(), DichVuKhamApDung_.soCongVanBhxh));
            }
            if (criteria.getSoQuyetDinh() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSoQuyetDinh(), DichVuKhamApDung_.soQuyetDinh));
            }
            if (criteria.getTenBaoCaoBhxh() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTenBaoCaoBhxh(), DichVuKhamApDung_.tenBaoCaoBhxh));
            }
            if (criteria.getTenDichVuKhongBaoHiem() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTenDichVuKhongBaoHiem(), DichVuKhamApDung_.tenDichVuKhongBaoHiem));
            }
            if (criteria.getTienBenhNhanChi() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTienBenhNhanChi(), DichVuKhamApDung_.tienBenhNhanChi));
            }
            if (criteria.getTienBhxhChi() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTienBhxhChi(), DichVuKhamApDung_.tienBhxhChi));
            }
            if (criteria.getTienNgoaiBhyt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTienNgoaiBhyt(), DichVuKhamApDung_.tienNgoaiBhyt));
            }
            if (criteria.getTongTienThanhToan() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTongTienThanhToan(), DichVuKhamApDung_.tongTienThanhToan));
            }
            if (criteria.getTyLeBhxhThanhToan() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTyLeBhxhThanhToan(), DichVuKhamApDung_.tyLeBhxhThanhToan));
            }
            if (criteria.getDoiTuongDacBiet() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDoiTuongDacBiet(), DichVuKhamApDung_.doiTuongDacBiet));
            }
            if (criteria.getNguonChi() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNguonChi(), DichVuKhamApDung_.nguonChi));
            }
            if (criteria.getDotGiaDichVuBhxhId() != null) {
                specification = specification.and(buildSpecification(criteria.getDotGiaDichVuBhxhId(),
                    root -> root.join(DichVuKhamApDung_.dotGiaDichVuBhxh, JoinType.LEFT).get(DotGiaDichVuBhxh_.id)));
            }
            if (criteria.getDichVuKhamId() != null) {
                specification = specification.and(buildSpecification(criteria.getDichVuKhamId(),
                    root -> root.join(DichVuKhamApDung_.dichVuKham, JoinType.LEFT).get(DichVuKham_.id)));
            }
        }
        return specification;
    }
}

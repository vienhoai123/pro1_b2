package vn.vnpt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import vn.vnpt.domain.XetNghiem;
import vn.vnpt.domain.*; // for static metamodels
import vn.vnpt.repository.XetNghiemRepository;
import vn.vnpt.service.dto.XetNghiemCriteria;
import vn.vnpt.service.dto.XetNghiemDTO;
import vn.vnpt.service.mapper.XetNghiemMapper;

/**
 * Service for executing complex queries for {@link XetNghiem} entities in the database.
 * The main input is a {@link XetNghiemCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link XetNghiemDTO} or a {@link Page} of {@link XetNghiemDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class XetNghiemQueryService extends QueryService<XetNghiem> {

    private final Logger log = LoggerFactory.getLogger(XetNghiemQueryService.class);

    private final XetNghiemRepository xetNghiemRepository;

    private final XetNghiemMapper xetNghiemMapper;

    public XetNghiemQueryService(XetNghiemRepository xetNghiemRepository, XetNghiemMapper xetNghiemMapper) {
        this.xetNghiemRepository = xetNghiemRepository;
        this.xetNghiemMapper = xetNghiemMapper;
    }

    /**
     * Return a {@link List} of {@link XetNghiemDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<XetNghiemDTO> findByCriteria(XetNghiemCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<XetNghiem> specification = createSpecification(criteria);
        return xetNghiemMapper.toDto(xetNghiemRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link XetNghiemDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<XetNghiemDTO> findByCriteria(XetNghiemCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<XetNghiem> specification = createSpecification(criteria);
        return xetNghiemRepository.findAll(specification, page)
            .map(xetNghiemMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(XetNghiemCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<XetNghiem> specification = createSpecification(criteria);
        return xetNghiemRepository.count(specification);
    }

    /**
     * Function to convert {@link XetNghiemCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<XetNghiem> createSpecification(XetNghiemCriteria criteria) {
        Specification<XetNghiem> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), XetNghiem_.id));
            }
            if (criteria.getCanDuoiNam() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCanDuoiNam(), XetNghiem_.canDuoiNam));
            }
            if (criteria.getCanDuoiNu() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCanDuoiNu(), XetNghiem_.canDuoiNu));
            }
            if (criteria.getCanTrenNam() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCanTrenNam(), XetNghiem_.canTrenNam));
            }
            if (criteria.getCanTrenNu() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCanTrenNu(), XetNghiem_.canTrenNu));
            }
            if (criteria.getChiSoBinhThuongNam() != null) {
                specification = specification.and(buildStringSpecification(criteria.getChiSoBinhThuongNam(), XetNghiem_.chiSoBinhThuongNam));
            }
            if (criteria.getChiSoMax() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getChiSoMax(), XetNghiem_.chiSoMax));
            }
            if (criteria.getChiSoMin() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getChiSoMin(), XetNghiem_.chiSoMin));
            }
            if (criteria.getCongThuc() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCongThuc(), XetNghiem_.congThuc));
            }
            if (criteria.getDeleted() != null) {
                specification = specification.and(buildSpecification(criteria.getDeleted(), XetNghiem_.deleted));
            }
            if (criteria.getDoPhaLoang() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDoPhaLoang(), XetNghiem_.doPhaLoang));
            }
            if (criteria.getDonGiaBenhVien() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDonGiaBenhVien(), XetNghiem_.donGiaBenhVien));
            }
            if (criteria.getDonViTinh() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDonViTinh(), XetNghiem_.donViTinh));
            }
            if (criteria.getEnabled() != null) {
                specification = specification.and(buildSpecification(criteria.getEnabled(), XetNghiem_.enabled));
            }
            if (criteria.getGioiHanChiDinh() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getGioiHanChiDinh(), XetNghiem_.gioiHanChiDinh));
            }
            if (criteria.getKetQuaBatThuong() != null) {
                specification = specification.and(buildStringSpecification(criteria.getKetQuaBatThuong(), XetNghiem_.ketQuaBatThuong));
            }
            if (criteria.getMaDungChung() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMaDungChung(), XetNghiem_.maDungChung));
            }
            if (criteria.getKetQuaMacDinh() != null) {
                specification = specification.and(buildStringSpecification(criteria.getKetQuaMacDinh(), XetNghiem_.ketQuaMacDinh));
            }
            if (criteria.getPhamViChiDinh() != null) {
                specification = specification.and(buildSpecification(criteria.getPhamViChiDinh(), XetNghiem_.phamViChiDinh));
            }
            if (criteria.getPhanTheoGioiTInh() != null) {
                specification = specification.and(buildSpecification(criteria.getPhanTheoGioiTInh(), XetNghiem_.phanTheoGioiTInh));
            }
            if (criteria.getSoLeLamTron() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSoLeLamTron(), XetNghiem_.soLeLamTron));
            }
            if (criteria.getSoLuongThucHien() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSoLuongThucHien(), XetNghiem_.soLuongThucHien));
            }
            if (criteria.getTen() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTen(), XetNghiem_.ten));
            }
            if (criteria.getTenHienThi() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTenHienThi(), XetNghiem_.tenHienThi));
            }
            if (criteria.getMaNoiBo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMaNoiBo(), XetNghiem_.maNoiBo));
            }
            if (criteria.getDonViId() != null) {
                specification = specification.and(buildSpecification(criteria.getDonViId(),
                    root -> root.join(XetNghiem_.donVi, JoinType.LEFT).get(DonVi_.id)));
            }
            if (criteria.getDotMaId() != null) {
                specification = specification.and(buildSpecification(criteria.getDotMaId(),
                    root -> root.join(XetNghiem_.dotMa, JoinType.LEFT).get(DotThayDoiMaDichVu_.id)));
            }
            if (criteria.getLoaiXNId() != null) {
                specification = specification.and(buildSpecification(criteria.getLoaiXNId(),
                    root -> root.join(XetNghiem_.loaiXN, JoinType.LEFT).get(LoaiXetNghiem_.id)));
            }
        }
        return specification;
    }
}

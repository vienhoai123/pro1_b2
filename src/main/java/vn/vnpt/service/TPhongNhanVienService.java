package vn.vnpt.service;

import vn.vnpt.service.dto.TPhongNhanVienDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.TPhongNhanVien}.
 */
public interface TPhongNhanVienService {

    /**
     * Save a tPhongNhanVien.
     *
     * @param tPhongNhanVienDTO the entity to save.
     * @return the persisted entity.
     */
    TPhongNhanVienDTO save(TPhongNhanVienDTO tPhongNhanVienDTO);

    /**
     * Get all the tPhongNhanViens.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<TPhongNhanVienDTO> findAll(Pageable pageable);

    /**
     * Get the "id" tPhongNhanVien.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<TPhongNhanVienDTO> findOne(Long id);

    /**
     * Delete the "id" tPhongNhanVien.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}

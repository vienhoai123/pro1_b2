package vn.vnpt.service;

import io.github.jhipster.service.QueryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.domain.*;
import vn.vnpt.repository.DotThayDoiMaDichVuRepository;
import vn.vnpt.service.dto.DotThayDoiMaDichVuCriteria;
import vn.vnpt.service.dto.DotThayDoiMaDichVuDTO;
import vn.vnpt.service.mapper.DotThayDoiMaDichVuMapper;

import javax.persistence.criteria.JoinType;
import java.util.List;

/**
 * Service for executing complex queries for {@link DotThayDoiMaDichVu} entities in the database.
 * The main input is a {@link DotThayDoiMaDichVuCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link DotThayDoiMaDichVuDTO} or a {@link Page} of {@link DotThayDoiMaDichVuDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class DotThayDoiMaDichVuQueryService extends QueryService<DotThayDoiMaDichVu> {

    private final Logger log = LoggerFactory.getLogger(DotThayDoiMaDichVuQueryService.class);

    private final DotThayDoiMaDichVuRepository dotThayDoiMaDichVuRepository;

    private final DotThayDoiMaDichVuMapper dotThayDoiMaDichVuMapper;

    public DotThayDoiMaDichVuQueryService(DotThayDoiMaDichVuRepository dotThayDoiMaDichVuRepository, DotThayDoiMaDichVuMapper dotThayDoiMaDichVuMapper) {
        this.dotThayDoiMaDichVuRepository = dotThayDoiMaDichVuRepository;
        this.dotThayDoiMaDichVuMapper = dotThayDoiMaDichVuMapper;
    }

    /**
     * Return a {@link List} of {@link DotThayDoiMaDichVuDTO} which matches the criteria from the database.
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<DotThayDoiMaDichVuDTO> findByCriteria(DotThayDoiMaDichVuCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<DotThayDoiMaDichVu> specification = createSpecification(criteria);
        return dotThayDoiMaDichVuMapper.toDto(dotThayDoiMaDichVuRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link DotThayDoiMaDichVuDTO} which matches the criteria from the database.
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page     The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<DotThayDoiMaDichVuDTO> findByCriteria(DotThayDoiMaDichVuCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<DotThayDoiMaDichVu> specification = createSpecification(criteria);
        return dotThayDoiMaDichVuRepository.findAll(specification, page)
            .map(dotThayDoiMaDichVuMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(DotThayDoiMaDichVuCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<DotThayDoiMaDichVu> specification = createSpecification(criteria);
        return dotThayDoiMaDichVuRepository.count(specification);
    }

    /**
     * Function to convert {@link DotThayDoiMaDichVuCriteria} to a {@link Specification}
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<DotThayDoiMaDichVu> createSpecification(DotThayDoiMaDichVuCriteria criteria) {
        Specification<DotThayDoiMaDichVu> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), DotThayDoiMaDichVu_.id));
            }
            if (criteria.getDotTuongApDung() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDotTuongApDung(), DotThayDoiMaDichVu_.dotTuongApDung));
            }
            if (criteria.getGhiChu() != null) {
                specification = specification.and(buildStringSpecification(criteria.getGhiChu(), DotThayDoiMaDichVu_.ghiChu));
            }
            if (criteria.getNgayApDung() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNgayApDung(), DotThayDoiMaDichVu_.ngayApDung));
            }
            if (criteria.getTen() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTen(), DotThayDoiMaDichVu_.ten));
            }
            if (criteria.getDonViId() != null) {
                specification = specification.and(buildSpecification(criteria.getDonViId(),
                    root -> root.join(DotThayDoiMaDichVu_.donVi, JoinType.LEFT).get(DonVi_.id)));
            }
        }
        return specification;
    }
}

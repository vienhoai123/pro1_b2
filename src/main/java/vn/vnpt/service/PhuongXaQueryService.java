package vn.vnpt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import vn.vnpt.domain.PhuongXa;
import vn.vnpt.domain.*; // for static metamodels
import vn.vnpt.repository.PhuongXaRepository;
import vn.vnpt.service.dto.PhuongXaCriteria;
import vn.vnpt.service.dto.PhuongXaDTO;
import vn.vnpt.service.mapper.PhuongXaMapper;

/**
 * Service for executing complex queries for {@link PhuongXa} entities in the database.
 * The main input is a {@link PhuongXaCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link PhuongXaDTO} or a {@link Page} of {@link PhuongXaDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class PhuongXaQueryService extends QueryService<PhuongXa> {

    private final Logger log = LoggerFactory.getLogger(PhuongXaQueryService.class);

    private final PhuongXaRepository phuongXaRepository;

    private final PhuongXaMapper phuongXaMapper;

    public PhuongXaQueryService(PhuongXaRepository phuongXaRepository, PhuongXaMapper phuongXaMapper) {
        this.phuongXaRepository = phuongXaRepository;
        this.phuongXaMapper = phuongXaMapper;
    }

    /**
     * Return a {@link List} of {@link PhuongXaDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<PhuongXaDTO> findByCriteria(PhuongXaCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<PhuongXa> specification = createSpecification(criteria);
        return phuongXaMapper.toDto(phuongXaRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link PhuongXaDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<PhuongXaDTO> findByCriteria(PhuongXaCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<PhuongXa> specification = createSpecification(criteria);
        return phuongXaRepository.findAll(specification, page)
            .map(phuongXaMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(PhuongXaCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<PhuongXa> specification = createSpecification(criteria);
        return phuongXaRepository.count(specification);
    }

    /**
     * Function to convert {@link PhuongXaCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<PhuongXa> createSpecification(PhuongXaCriteria criteria) {
        Specification<PhuongXa> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), PhuongXa_.id));
            }
            if (criteria.getCap() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCap(), PhuongXa_.cap));
            }
            if (criteria.getGuessPhrase() != null) {
                specification = specification.and(buildStringSpecification(criteria.getGuessPhrase(), PhuongXa_.guessPhrase));
            }
            if (criteria.getTen() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTen(), PhuongXa_.ten));
            }
            if (criteria.getTenKhongDau() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTenKhongDau(), PhuongXa_.tenKhongDau));
            }
            if (criteria.getQuanHuyenId() != null) {
                specification = specification.and(buildSpecification(criteria.getQuanHuyenId(),
                    root -> root.join(PhuongXa_.quanHuyen, JoinType.LEFT).get(QuanHuyen_.id)));
            }
        }
        return specification;
    }
}

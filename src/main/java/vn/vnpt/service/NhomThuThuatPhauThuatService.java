package vn.vnpt.service;

import vn.vnpt.service.dto.NhomThuThuatPhauThuatDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.NhomThuThuatPhauThuat}.
 */
public interface NhomThuThuatPhauThuatService {

    /**
     * Save a nhomThuThuatPhauThuat.
     *
     * @param nhomThuThuatPhauThuatDTO the entity to save.
     * @return the persisted entity.
     */
    NhomThuThuatPhauThuatDTO save(NhomThuThuatPhauThuatDTO nhomThuThuatPhauThuatDTO);

    /**
     * Get all the nhomThuThuatPhauThuats.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<NhomThuThuatPhauThuatDTO> findAll(Pageable pageable);

    /**
     * Get the "id" nhomThuThuatPhauThuat.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<NhomThuThuatPhauThuatDTO> findOne(Long id);

    /**
     * Delete the "id" nhomThuThuatPhauThuat.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}

package vn.vnpt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import vn.vnpt.domain.ChanDoanHinhAnhApDung;
import vn.vnpt.domain.*; // for static metamodels
import vn.vnpt.repository.ChanDoanHinhAnhApDungRepository;
import vn.vnpt.service.dto.ChanDoanHinhAnhApDungCriteria;
import vn.vnpt.service.dto.ChanDoanHinhAnhApDungDTO;
import vn.vnpt.service.mapper.ChanDoanHinhAnhApDungMapper;

/**
 * Service for executing complex queries for {@link ChanDoanHinhAnhApDung} entities in the database.
 * The main input is a {@link ChanDoanHinhAnhApDungCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ChanDoanHinhAnhApDungDTO} or a {@link Page} of {@link ChanDoanHinhAnhApDungDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ChanDoanHinhAnhApDungQueryService extends QueryService<ChanDoanHinhAnhApDung> {

    private final Logger log = LoggerFactory.getLogger(ChanDoanHinhAnhApDungQueryService.class);

    private final ChanDoanHinhAnhApDungRepository chanDoanHinhAnhApDungRepository;

    private final ChanDoanHinhAnhApDungMapper chanDoanHinhAnhApDungMapper;

    public ChanDoanHinhAnhApDungQueryService(ChanDoanHinhAnhApDungRepository chanDoanHinhAnhApDungRepository, ChanDoanHinhAnhApDungMapper chanDoanHinhAnhApDungMapper) {
        this.chanDoanHinhAnhApDungRepository = chanDoanHinhAnhApDungRepository;
        this.chanDoanHinhAnhApDungMapper = chanDoanHinhAnhApDungMapper;
    }

    /**
     * Return a {@link List} of {@link ChanDoanHinhAnhApDungDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ChanDoanHinhAnhApDungDTO> findByCriteria(ChanDoanHinhAnhApDungCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ChanDoanHinhAnhApDung> specification = createSpecification(criteria);
        return chanDoanHinhAnhApDungMapper.toDto(chanDoanHinhAnhApDungRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ChanDoanHinhAnhApDungDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ChanDoanHinhAnhApDungDTO> findByCriteria(ChanDoanHinhAnhApDungCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ChanDoanHinhAnhApDung> specification = createSpecification(criteria);
        return chanDoanHinhAnhApDungRepository.findAll(specification, page)
            .map(chanDoanHinhAnhApDungMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ChanDoanHinhAnhApDungCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ChanDoanHinhAnhApDung> specification = createSpecification(criteria);
        return chanDoanHinhAnhApDungRepository.count(specification);
    }

    /**
     * Function to convert {@link ChanDoanHinhAnhApDungCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ChanDoanHinhAnhApDung> createSpecification(ChanDoanHinhAnhApDungCriteria criteria) {
        Specification<ChanDoanHinhAnhApDung> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ChanDoanHinhAnhApDung_.id));
            }
            if (criteria.getMaBaoCaoBhxh() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMaBaoCaoBhxh(), ChanDoanHinhAnhApDung_.maBaoCaoBhxh));
            }
            if (criteria.getMaBaoCaoBhyt() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMaBaoCaoBhyt(), ChanDoanHinhAnhApDung_.maBaoCaoBhyt));
            }
            if (criteria.getNgayApDung() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNgayApDung(), ChanDoanHinhAnhApDung_.ngayApDung));
            }
            if (criteria.getSoCongVanBhxh() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSoCongVanBhxh(), ChanDoanHinhAnhApDung_.soCongVanBhxh));
            }
            if (criteria.getSoQuyetDinh() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSoQuyetDinh(), ChanDoanHinhAnhApDung_.soQuyetDinh));
            }
            if (criteria.getTenBaoCaoBhxh() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTenBaoCaoBhxh(), ChanDoanHinhAnhApDung_.tenBaoCaoBhxh));
            }
            if (criteria.getTenDichVuKhongBhyt() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTenDichVuKhongBhyt(), ChanDoanHinhAnhApDung_.tenDichVuKhongBhyt));
            }
            if (criteria.getTienBenhNhanChi() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTienBenhNhanChi(), ChanDoanHinhAnhApDung_.tienBenhNhanChi));
            }
            if (criteria.getTienBhxhChi() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTienBhxhChi(), ChanDoanHinhAnhApDung_.tienBhxhChi));
            }
            if (criteria.getTienNgoaiBhyt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTienNgoaiBhyt(), ChanDoanHinhAnhApDung_.tienNgoaiBhyt));
            }
            if (criteria.getTongTienThanhToan() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTongTienThanhToan(), ChanDoanHinhAnhApDung_.tongTienThanhToan));
            }
            if (criteria.getTyLeBhxhThanhToan() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTyLeBhxhThanhToan(), ChanDoanHinhAnhApDung_.tyLeBhxhThanhToan));
            }
            if (criteria.getGiaBhyt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getGiaBhyt(), ChanDoanHinhAnhApDung_.giaBhyt));
            }
            if (criteria.getGiaKhongBhyt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getGiaKhongBhyt(), ChanDoanHinhAnhApDung_.giaKhongBhyt));
            }
            if (criteria.getDoiTuongDacBiet() != null) {
                specification = specification.and(buildSpecification(criteria.getDoiTuongDacBiet(), ChanDoanHinhAnhApDung_.doiTuongDacBiet));
            }
            if (criteria.getNguonChi() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNguonChi(), ChanDoanHinhAnhApDung_.nguonChi));
            }
            if (criteria.getEnable() != null) {
                specification = specification.and(buildSpecification(criteria.getEnable(), ChanDoanHinhAnhApDung_.enable));
            }
            if (criteria.getDotGiaId() != null) {
                specification = specification.and(buildSpecification(criteria.getDotGiaId(),
                    root -> root.join(ChanDoanHinhAnhApDung_.dotGia, JoinType.LEFT).get(DotGiaDichVuBhxh_.id)));
            }
            if (criteria.getCdhaId() != null) {
                specification = specification.and(buildSpecification(criteria.getCdhaId(),
                    root -> root.join(ChanDoanHinhAnhApDung_.cdha, JoinType.LEFT).get(ChanDoanHinhAnh_.id)));
            }
        }
        return specification;
    }
}

package vn.vnpt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import vn.vnpt.domain.NhomBenhLy;
import vn.vnpt.domain.*; // for static metamodels
import vn.vnpt.repository.NhomBenhLyRepository;
import vn.vnpt.service.dto.NhomBenhLyCriteria;
import vn.vnpt.service.dto.NhomBenhLyDTO;
import vn.vnpt.service.mapper.NhomBenhLyMapper;

/**
 * Service for executing complex queries for {@link NhomBenhLy} entities in the database.
 * The main input is a {@link NhomBenhLyCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link NhomBenhLyDTO} or a {@link Page} of {@link NhomBenhLyDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class NhomBenhLyQueryService extends QueryService<NhomBenhLy> {

    private final Logger log = LoggerFactory.getLogger(NhomBenhLyQueryService.class);

    private final NhomBenhLyRepository nhomBenhLyRepository;

    private final NhomBenhLyMapper nhomBenhLyMapper;

    public NhomBenhLyQueryService(NhomBenhLyRepository nhomBenhLyRepository, NhomBenhLyMapper nhomBenhLyMapper) {
        this.nhomBenhLyRepository = nhomBenhLyRepository;
        this.nhomBenhLyMapper = nhomBenhLyMapper;
    }

    /**
     * Return a {@link List} of {@link NhomBenhLyDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<NhomBenhLyDTO> findByCriteria(NhomBenhLyCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<NhomBenhLy> specification = createSpecification(criteria);
        return nhomBenhLyMapper.toDto(nhomBenhLyRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link NhomBenhLyDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<NhomBenhLyDTO> findByCriteria(NhomBenhLyCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<NhomBenhLy> specification = createSpecification(criteria);
        return nhomBenhLyRepository.findAll(specification, page)
            .map(nhomBenhLyMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(NhomBenhLyCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<NhomBenhLy> specification = createSpecification(criteria);
        return nhomBenhLyRepository.count(specification);
    }

    /**
     * Function to convert {@link NhomBenhLyCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<NhomBenhLy> createSpecification(NhomBenhLyCriteria criteria) {
        Specification<NhomBenhLy> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), NhomBenhLy_.id));
            }
            if (criteria.getMoTa() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMoTa(), NhomBenhLy_.moTa));
            }
            if (criteria.getTen() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTen(), NhomBenhLy_.ten));
            }
            if (criteria.getLoaiBenhLyId() != null) {
                specification = specification.and(buildSpecification(criteria.getLoaiBenhLyId(),
                    root -> root.join(NhomBenhLy_.loaiBenhLy, JoinType.LEFT).get(LoaiBenhLy_.id)));
            }
        }
        return specification;
    }
}

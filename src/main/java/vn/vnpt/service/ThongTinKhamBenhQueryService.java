package vn.vnpt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import vn.vnpt.domain.ThongTinKhamBenh;
import vn.vnpt.domain.*; // for static metamodels
import vn.vnpt.repository.ThongTinKhamBenhRepository;
import vn.vnpt.service.dto.ThongTinKhamBenhCriteria;
import vn.vnpt.service.dto.ThongTinKhamBenhDTO;
import vn.vnpt.service.mapper.ThongTinKhamBenhMapper;

/**
 * Service for executing complex queries for {@link ThongTinKhamBenh} entities in the database.
 * The main input is a {@link ThongTinKhamBenhCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ThongTinKhamBenhDTO} or a {@link Page} of {@link ThongTinKhamBenhDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ThongTinKhamBenhQueryService extends QueryService<ThongTinKhamBenh> {

    private final Logger log = LoggerFactory.getLogger(ThongTinKhamBenhQueryService.class);

    private final ThongTinKhamBenhRepository thongTinKhamBenhRepository;

    private final ThongTinKhamBenhMapper thongTinKhamBenhMapper;

    public ThongTinKhamBenhQueryService(ThongTinKhamBenhRepository thongTinKhamBenhRepository, ThongTinKhamBenhMapper thongTinKhamBenhMapper) {
        this.thongTinKhamBenhRepository = thongTinKhamBenhRepository;
        this.thongTinKhamBenhMapper = thongTinKhamBenhMapper;
    }

    /**
     * Return a {@link List} of {@link ThongTinKhamBenhDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ThongTinKhamBenhDTO> findByCriteria(ThongTinKhamBenhCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ThongTinKhamBenh> specification = createSpecification(criteria);
        return thongTinKhamBenhMapper.toDto(thongTinKhamBenhRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ThongTinKhamBenhDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ThongTinKhamBenhDTO> findByCriteria(ThongTinKhamBenhCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ThongTinKhamBenh> specification = createSpecification(criteria);
        return thongTinKhamBenhRepository.findAll(specification, page)
            .map(thongTinKhamBenhMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ThongTinKhamBenhCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ThongTinKhamBenh> specification = createSpecification(criteria);
        return thongTinKhamBenhRepository.count(specification);
    }

    /**
     * Function to convert {@link ThongTinKhamBenhCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ThongTinKhamBenh> createSpecification(ThongTinKhamBenhCriteria criteria) {
        Specification<ThongTinKhamBenh> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ThongTinKhamBenh_.id));
            }
            if (criteria.getBmi() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getBmi(), ThongTinKhamBenh_.bmi));
            }
            if (criteria.getCanNang() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCanNang(), ThongTinKhamBenh_.canNang));
            }
            if (criteria.getChieuCao() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getChieuCao(), ThongTinKhamBenh_.chieuCao));
            }
            if (criteria.getCreatinin() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatinin(), ThongTinKhamBenh_.creatinin));
            }
            if (criteria.getDauHieuLamSang() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDauHieuLamSang(), ThongTinKhamBenh_.dauHieuLamSang));
            }
            if (criteria.getDoThanhThai() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDoThanhThai(), ThongTinKhamBenh_.doThanhThai));
            }
            if (criteria.getHuyetApCao() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getHuyetApCao(), ThongTinKhamBenh_.huyetApCao));
            }
            if (criteria.getHuyetApThap() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getHuyetApThap(), ThongTinKhamBenh_.huyetApThap));
            }
            if (criteria.getIcd() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIcd(), ThongTinKhamBenh_.icd));
            }
            if (criteria.getLanHen() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLanHen(), ThongTinKhamBenh_.lanHen));
            }
            if (criteria.getLoiDan() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLoiDan(), ThongTinKhamBenh_.loiDan));
            }
            if (criteria.getLyDoChuyen() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLyDoChuyen(), ThongTinKhamBenh_.lyDoChuyen));
            }
            if (criteria.getMaBenhYhct() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMaBenhYhct(), ThongTinKhamBenh_.maBenhYhct));
            }
            if (criteria.getMach() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getMach(), ThongTinKhamBenh_.mach));
            }
            if (criteria.getMaxNgayRaToa() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getMaxNgayRaToa(), ThongTinKhamBenh_.maxNgayRaToa));
            }
            if (criteria.getNgayHen() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNgayHen(), ThongTinKhamBenh_.ngayHen));
            }
            if (criteria.getNhanDinhBmi() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNhanDinhBmi(), ThongTinKhamBenh_.nhanDinhBmi));
            }
            if (criteria.getNhanDinhDoThanhThai() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNhanDinhDoThanhThai(), ThongTinKhamBenh_.nhanDinhDoThanhThai));
            }
            if (criteria.getNhapVien() != null) {
                specification = specification.and(buildSpecification(criteria.getNhapVien(), ThongTinKhamBenh_.nhapVien));
            }
            if (criteria.getNhietDo() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNhietDo(), ThongTinKhamBenh_.nhietDo));
            }
            if (criteria.getNhipTho() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNhipTho(), ThongTinKhamBenh_.nhipTho));
            }
            if (criteria.getPpDieuTriYtct() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPpDieuTriYtct(), ThongTinKhamBenh_.ppDieuTriYtct));
            }
            if (criteria.getSoLanInBangKe() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSoLanInBangKe(), ThongTinKhamBenh_.soLanInBangKe));
            }
            if (criteria.getSoLanInToaThuoc() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSoLanInToaThuoc(), ThongTinKhamBenh_.soLanInToaThuoc));
            }
            if (criteria.getTenBenhIcd() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTenBenhIcd(), ThongTinKhamBenh_.tenBenhIcd));
            }
            if (criteria.getTenBenhTheoBacSi() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTenBenhTheoBacSi(), ThongTinKhamBenh_.tenBenhTheoBacSi));
            }
            if (criteria.getTenBenhYhct() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTenBenhYhct(), ThongTinKhamBenh_.tenBenhYhct));
            }
            if (criteria.getTrangThaiCdha() != null) {
                specification = specification.and(buildSpecification(criteria.getTrangThaiCdha(), ThongTinKhamBenh_.trangThaiCdha));
            }
            if (criteria.getTrangThaiTtpt() != null) {
                specification = specification.and(buildSpecification(criteria.getTrangThaiTtpt(), ThongTinKhamBenh_.trangThaiTtpt));
            }
            if (criteria.getTrangThaiXetNghiem() != null) {
                specification = specification.and(buildSpecification(criteria.getTrangThaiXetNghiem(), ThongTinKhamBenh_.trangThaiXetNghiem));
            }
            if (criteria.getTrieuChungLamSang() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTrieuChungLamSang(), ThongTinKhamBenh_.trieuChungLamSang));
            }
            if (criteria.getVongBung() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getVongBung(), ThongTinKhamBenh_.vongBung));
            }
            if (criteria.getTrangThai() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTrangThai(), ThongTinKhamBenh_.trangThai));
            }
            if (criteria.getThoiGianKetThucKham() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getThoiGianKetThucKham(), ThongTinKhamBenh_.thoiGianKetThucKham));
            }
            if (criteria.getPhongIdChuyenTu() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPhongIdChuyenTu(), ThongTinKhamBenh_.phongIdChuyenTu));
            }
            if (criteria.getThoiGianKhamBenh() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getThoiGianKhamBenh(), ThongTinKhamBenh_.thoiGianKhamBenh));
            }
            if (criteria.getyLenh() != null) {
                specification = specification.and(buildStringSpecification(criteria.getyLenh(), ThongTinKhamBenh_.yLenh));
            }
            if (criteria.getLoaiYLenh() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLoaiYLenh(), ThongTinKhamBenh_.loaiYLenh));
            }
            if (criteria.getChanDoan() != null) {
                specification = specification.and(buildStringSpecification(criteria.getChanDoan(), ThongTinKhamBenh_.chanDoan));
            }
            if (criteria.getLoaiChanDoan() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLoaiChanDoan(), ThongTinKhamBenh_.loaiChanDoan));
            }
            if (criteria.getNam() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNam(), ThongTinKhamBenh_.nam));
            }
            if (criteria.getBakbId() != null) {
                specification = specification.and(buildSpecification(criteria.getBakbId(), ThongTinKhamBenh_.bakbId));
            }
            if (criteria.getDonViId() != null) {
                specification = specification.and(buildSpecification(criteria.getDonViId(), ThongTinKhamBenh_.donViId));
            }
            if (criteria.getBenhNhanId() != null) {
                specification = specification.and(buildSpecification(criteria.getBenhNhanId(), ThongTinKhamBenh_.benhNhanId));
            }
            if (criteria.getDotDieuTriId() != null) {
                specification = specification.and(buildSpecification(criteria.getDotDieuTriId(), ThongTinKhamBenh_.dotDieuTriId));
            }
            if (criteria.getThongTinKhoaId() != null) {
                specification = specification.and(buildSpecification(criteria.getThongTinKhoaId(),
                    root -> root.join(ThongTinKhamBenh_.thongTinKhoa, JoinType.LEFT).get(ThongTinKhoa_.id)));
            }
            if (criteria.getNhanVienId() != null) {
                specification = specification.and(buildSpecification(criteria.getNhanVienId(),
                    root -> root.join(ThongTinKhamBenh_.nhanVien, JoinType.LEFT).get(NhanVien_.id)));
            }
            if (criteria.getHuongDieuTriId() != null) {
                specification = specification.and(buildSpecification(criteria.getHuongDieuTriId(),
                    root -> root.join(ThongTinKhamBenh_.huongDieuTri, JoinType.LEFT).get(HuongDieuTri_.id)));
            }
            if (criteria.getPhongId() != null) {
                specification = specification.and(buildSpecification(criteria.getPhongId(),
                    root -> root.join(ThongTinKhamBenh_.phong, JoinType.LEFT).get(Phong_.id)));
            }
        }
        return specification;
    }
}

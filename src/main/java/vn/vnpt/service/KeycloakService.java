package vn.vnpt.service;

import org.keycloak.representations.idm.RoleRepresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.client.KeycloakUtils;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

@Service
@Transactional
public class KeycloakService {

    private final Logger log = LoggerFactory.getLogger(KeycloakService.class);

    private KeycloakUtils keycloakUtils;

    public KeycloakService(KeycloakUtils keycloakUtils) {
        this.keycloakUtils = keycloakUtils;
    }

    public boolean createRole(String roleName) {
        RoleRepresentation role = new RoleRepresentation();
        role.setName(roleName);
        AtomicBoolean status = new AtomicBoolean(true);
        try {
            log.debug("REST request to create role Keycloak : {}", role);
            keycloakUtils.getRealmResource().roles().create(role);

        } catch (Exception e) {
            log.debug("REST request to create role Keycloak : {0}", e);
            status.set(false);
        }
        return status.get();
    }
    public boolean deleteRole(String roleName) {
        AtomicBoolean status = new AtomicBoolean(true);
        try {
            keycloakUtils.getRealmResource().roles().deleteRole(roleName);

        } catch (Exception e) {
            log.debug("REST request to delete role Keycloak : {0}", e);
            status.set(false);
        }
        return status.get();
    }
    public List<RoleRepresentation> getRoles() {
        List<RoleRepresentation> roles;
        roles = null;
        try {
            roles = keycloakUtils.getRealmResource().roles().list();

        } catch (Exception e) {
            log.debug("REST request to get roles Keycloak : {0}", e);
        }
        return roles;
    }
}

package vn.vnpt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import vn.vnpt.domain.XetNghiemApDung;
import vn.vnpt.domain.*; // for static metamodels
import vn.vnpt.repository.XetNghiemApDungRepository;
import vn.vnpt.service.dto.XetNghiemApDungCriteria;
import vn.vnpt.service.dto.XetNghiemApDungDTO;
import vn.vnpt.service.mapper.XetNghiemApDungMapper;

/**
 * Service for executing complex queries for {@link XetNghiemApDung} entities in the database.
 * The main input is a {@link XetNghiemApDungCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link XetNghiemApDungDTO} or a {@link Page} of {@link XetNghiemApDungDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class XetNghiemApDungQueryService extends QueryService<XetNghiemApDung> {

    private final Logger log = LoggerFactory.getLogger(XetNghiemApDungQueryService.class);

    private final XetNghiemApDungRepository xetNghiemApDungRepository;

    private final XetNghiemApDungMapper xetNghiemApDungMapper;

    public XetNghiemApDungQueryService(XetNghiemApDungRepository xetNghiemApDungRepository, XetNghiemApDungMapper xetNghiemApDungMapper) {
        this.xetNghiemApDungRepository = xetNghiemApDungRepository;
        this.xetNghiemApDungMapper = xetNghiemApDungMapper;
    }

    /**
     * Return a {@link List} of {@link XetNghiemApDungDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<XetNghiemApDungDTO> findByCriteria(XetNghiemApDungCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<XetNghiemApDung> specification = createSpecification(criteria);
        return xetNghiemApDungMapper.toDto(xetNghiemApDungRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link XetNghiemApDungDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<XetNghiemApDungDTO> findByCriteria(XetNghiemApDungCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<XetNghiemApDung> specification = createSpecification(criteria);
        return xetNghiemApDungRepository.findAll(specification, page)
            .map(xetNghiemApDungMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(XetNghiemApDungCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<XetNghiemApDung> specification = createSpecification(criteria);
        return xetNghiemApDungRepository.count(specification);
    }

    /**
     * Function to convert {@link XetNghiemApDungCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<XetNghiemApDung> createSpecification(XetNghiemApDungCriteria criteria) {
        Specification<XetNghiemApDung> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), XetNghiemApDung_.id));
            }
            if (criteria.getMaBaoCaoBhxh() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMaBaoCaoBhxh(), XetNghiemApDung_.maBaoCaoBhxh));
            }
            if (criteria.getMaBaoCaoBhyt() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMaBaoCaoBhyt(), XetNghiemApDung_.maBaoCaoBhyt));
            }
            if (criteria.getNgayApDung() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNgayApDung(), XetNghiemApDung_.ngayApDung));
            }
            if (criteria.getSoCongVanBhxh() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSoCongVanBhxh(), XetNghiemApDung_.soCongVanBhxh));
            }
            if (criteria.getSoQuyetDinh() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSoQuyetDinh(), XetNghiemApDung_.soQuyetDinh));
            }
            if (criteria.getTenBaoCaoBhxh() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTenBaoCaoBhxh(), XetNghiemApDung_.tenBaoCaoBhxh));
            }
            if (criteria.getTenDichVuKhongBhyt() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTenDichVuKhongBhyt(), XetNghiemApDung_.tenDichVuKhongBhyt));
            }
            if (criteria.getTienBenhNhanChi() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTienBenhNhanChi(), XetNghiemApDung_.tienBenhNhanChi));
            }
            if (criteria.getTienBhxhChi() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTienBhxhChi(), XetNghiemApDung_.tienBhxhChi));
            }
            if (criteria.getTienNgoaiBhyt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTienNgoaiBhyt(), XetNghiemApDung_.tienNgoaiBhyt));
            }
            if (criteria.getTongTienThanhToan() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTongTienThanhToan(), XetNghiemApDung_.tongTienThanhToan));
            }
            if (criteria.getTyLeBhxhThanhToan() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTyLeBhxhThanhToan(), XetNghiemApDung_.tyLeBhxhThanhToan));
            }
            if (criteria.getGiaBhyt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getGiaBhyt(), XetNghiemApDung_.giaBhyt));
            }
            if (criteria.getGiaKhongBhyt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getGiaKhongBhyt(), XetNghiemApDung_.giaKhongBhyt));
            }
            if (criteria.getDoiTuongDacBiet() != null) {
                specification = specification.and(buildSpecification(criteria.getDoiTuongDacBiet(), XetNghiemApDung_.doiTuongDacBiet));
            }
            if (criteria.getNguonChi() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNguonChi(), XetNghiemApDung_.nguonChi));
            }
            if (criteria.getEnable() != null) {
                specification = specification.and(buildSpecification(criteria.getEnable(), XetNghiemApDung_.enable));
            }
            if (criteria.getDotGiaId() != null) {
                specification = specification.and(buildSpecification(criteria.getDotGiaId(),
                    root -> root.join(XetNghiemApDung_.dotGia, JoinType.LEFT).get(DotGiaDichVuBhxh_.id)));
            }
            if (criteria.getXNId() != null) {
                specification = specification.and(buildSpecification(criteria.getXNId(),
                    root -> root.join(XetNghiemApDung_.xN, JoinType.LEFT).get(XetNghiem_.id)));
            }
        }
        return specification;
    }
}

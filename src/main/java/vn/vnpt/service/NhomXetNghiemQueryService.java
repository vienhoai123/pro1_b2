package vn.vnpt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import vn.vnpt.domain.NhomXetNghiem;
import vn.vnpt.domain.*; // for static metamodels
import vn.vnpt.repository.NhomXetNghiemRepository;
import vn.vnpt.service.dto.NhomXetNghiemCriteria;
import vn.vnpt.service.dto.NhomXetNghiemDTO;
import vn.vnpt.service.mapper.NhomXetNghiemMapper;

/**
 * Service for executing complex queries for {@link NhomXetNghiem} entities in the database.
 * The main input is a {@link NhomXetNghiemCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link NhomXetNghiemDTO} or a {@link Page} of {@link NhomXetNghiemDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class NhomXetNghiemQueryService extends QueryService<NhomXetNghiem> {

    private final Logger log = LoggerFactory.getLogger(NhomXetNghiemQueryService.class);

    private final NhomXetNghiemRepository nhomXetNghiemRepository;

    private final NhomXetNghiemMapper nhomXetNghiemMapper;

    public NhomXetNghiemQueryService(NhomXetNghiemRepository nhomXetNghiemRepository, NhomXetNghiemMapper nhomXetNghiemMapper) {
        this.nhomXetNghiemRepository = nhomXetNghiemRepository;
        this.nhomXetNghiemMapper = nhomXetNghiemMapper;
    }

    /**
     * Return a {@link List} of {@link NhomXetNghiemDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<NhomXetNghiemDTO> findByCriteria(NhomXetNghiemCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<NhomXetNghiem> specification = createSpecification(criteria);
        return nhomXetNghiemMapper.toDto(nhomXetNghiemRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link NhomXetNghiemDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<NhomXetNghiemDTO> findByCriteria(NhomXetNghiemCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<NhomXetNghiem> specification = createSpecification(criteria);
        return nhomXetNghiemRepository.findAll(specification, page)
            .map(nhomXetNghiemMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(NhomXetNghiemCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<NhomXetNghiem> specification = createSpecification(criteria);
        return nhomXetNghiemRepository.count(specification);
    }

    /**
     * Function to convert {@link NhomXetNghiemCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<NhomXetNghiem> createSpecification(NhomXetNghiemCriteria criteria) {
        Specification<NhomXetNghiem> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), NhomXetNghiem_.id));
            }
            if (criteria.getEnable() != null) {
                specification = specification.and(buildSpecification(criteria.getEnable(), NhomXetNghiem_.enable));
            }
            if (criteria.getLevel() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLevel(), NhomXetNghiem_.level));
            }
            if (criteria.getParentId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getParentId(), NhomXetNghiem_.parentId));
            }
            if (criteria.getTen() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTen(), NhomXetNghiem_.ten));
            }
            if (criteria.getDonViId() != null) {
                specification = specification.and(buildSpecification(criteria.getDonViId(),
                    root -> root.join(NhomXetNghiem_.donVi, JoinType.LEFT).get(DonVi_.id)));
            }
        }
        return specification;
    }
}

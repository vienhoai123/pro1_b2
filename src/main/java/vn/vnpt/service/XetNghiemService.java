package vn.vnpt.service;

import vn.vnpt.service.dto.XetNghiemDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.XetNghiem}.
 */
public interface XetNghiemService {

    /**
     * Save a xetNghiem.
     *
     * @param xetNghiemDTO the entity to save.
     * @return the persisted entity.
     */
    XetNghiemDTO save(XetNghiemDTO xetNghiemDTO);

    /**
     * Get all the xetNghiems.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<XetNghiemDTO> findAll(Pageable pageable);

    /**
     * Get the "id" xetNghiem.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<XetNghiemDTO> findOne(Long id);

    /**
     * Delete the "id" xetNghiem.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}

package vn.vnpt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import vn.vnpt.domain.ChucVu;
import vn.vnpt.domain.*; // for static metamodels
import vn.vnpt.repository.ChucVuRepository;
import vn.vnpt.service.dto.ChucVuCriteria;
import vn.vnpt.service.dto.ChucVuDTO;
import vn.vnpt.service.mapper.ChucVuMapper;

/**
 * Service for executing complex queries for {@link ChucVu} entities in the database.
 * The main input is a {@link ChucVuCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ChucVuDTO} or a {@link Page} of {@link ChucVuDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ChucVuQueryService extends QueryService<ChucVu> {

    private final Logger log = LoggerFactory.getLogger(ChucVuQueryService.class);

    private final ChucVuRepository chucVuRepository;

    private final ChucVuMapper chucVuMapper;

    public ChucVuQueryService(ChucVuRepository chucVuRepository, ChucVuMapper chucVuMapper) {
        this.chucVuRepository = chucVuRepository;
        this.chucVuMapper = chucVuMapper;
    }

    /**
     * Return a {@link List} of {@link ChucVuDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ChucVuDTO> findByCriteria(ChucVuCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ChucVu> specification = createSpecification(criteria);
        return chucVuMapper.toDto(chucVuRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ChucVuDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ChucVuDTO> findByCriteria(ChucVuCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ChucVu> specification = createSpecification(criteria);
        return chucVuRepository.findAll(specification, page)
            .map(chucVuMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ChucVuCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ChucVu> specification = createSpecification(criteria);
        return chucVuRepository.count(specification);
    }

    /**
     * Function to convert {@link ChucVuCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ChucVu> createSpecification(ChucVuCriteria criteria) {
        Specification<ChucVu> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ChucVu_.id));
            }
            if (criteria.getMoTa() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMoTa(), ChucVu_.moTa));
            }
            if (criteria.getTen() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTen(), ChucVu_.ten));
            }
            if (criteria.getNhanVienId() != null) {
                specification = specification.and(buildSpecification(criteria.getNhanVienId(),
                    root -> root.join(ChucVu_.nhanViens, JoinType.LEFT).get(NhanVien_.id)));
            }
        }
        return specification;
    }
}

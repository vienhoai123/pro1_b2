package vn.vnpt.service;

import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.JoinType;

import io.github.jhipster.service.filter.LongFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import vn.vnpt.domain.DotDieuTri;
import vn.vnpt.domain.*; // for static metamodels
import vn.vnpt.repository.DotDieuTriRepository;
import vn.vnpt.service.dto.DotDieuTriCriteria;
import vn.vnpt.service.dto.DotDieuTriDTO;
import vn.vnpt.service.mapper.DotDieuTriMapper;

/**
 * Service for executing complex queries for {@link DotDieuTri} entities in the database.
 * The main input is a {@link DotDieuTriCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link DotDieuTriDTO} or a {@link Page} of {@link DotDieuTriDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class DotDieuTriQueryService extends QueryService<DotDieuTri> {

    private final Logger log = LoggerFactory.getLogger(DotDieuTriQueryService.class);

    private final DotDieuTriRepository dotDieuTriRepository;

    private final DotDieuTriMapper dotDieuTriMapper;

    public DotDieuTriQueryService(DotDieuTriRepository dotDieuTriRepository, DotDieuTriMapper dotDieuTriMapper) {
        this.dotDieuTriRepository = dotDieuTriRepository;
        this.dotDieuTriMapper = dotDieuTriMapper;
    }

    /**
     * Return a {@link List} of {@link DotDieuTriDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<DotDieuTriDTO> findByCriteria(DotDieuTriCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<DotDieuTri> specification = createSpecification(criteria);
        return dotDieuTriMapper.toDto(dotDieuTriRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link DotDieuTriDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<DotDieuTriDTO> findByCriteria(DotDieuTriCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<DotDieuTri> specification = createSpecification(criteria);
        return dotDieuTriRepository.findAll(specification, page)
            .map(dotDieuTriMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(DotDieuTriCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<DotDieuTri> specification = createSpecification(criteria);
        return dotDieuTriRepository.count(specification);
    }

    public Optional<DotDieuTriDTO> findByBakbId(Long bakbId) {
        LongFilter filter = new LongFilter();
        filter.setEquals(bakbId);
        DotDieuTriCriteria criteria = new DotDieuTriCriteria();
        criteria.setBakbId(filter);
        DotDieuTriDTO result = findByCriteria(criteria).get(0);
        return Optional.of(result);
    }
    /**
     * Function to convert {@link DotDieuTriCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<DotDieuTri> createSpecification(DotDieuTriCriteria criteria) {
        Specification<DotDieuTri> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), DotDieuTri_.id));
            }
            if (criteria.getBakbId() != null) {
                specification = specification.and(buildSpecification(criteria.getBakbId(),
                    root -> root.join(DotDieuTri_.bakb, JoinType.LEFT).get(BenhAnKhamBenh_.id)));
            }
            if (criteria.getBenhNhanId() != null) {
                specification = specification.and(buildSpecification(criteria.getBenhNhanId(),
                    root -> root.join(DotDieuTri_.bakb, JoinType.LEFT).join(BenhAnKhamBenh_.benhNhan,JoinType.LEFT).get(BenhNhan_.id)));
            }
            if (criteria.getDonViId() != null) {
                specification = specification.and(buildSpecification(criteria.getDonViId(),
                    root -> root.join(DotDieuTri_.bakb, JoinType.LEFT).join(BenhAnKhamBenh_.donVi,JoinType.LEFT).get(DonVi_.id)));
            }
            if (criteria.getSoThuTu() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSoThuTu(), DotDieuTri_.soThuTu));
            }
            if (criteria.getBhytCanTren() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getBhytCanTren(), DotDieuTri_.bhytCanTren));
            }
            if (criteria.getBhytCanTrenKtc() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getBhytCanTrenKtc(), DotDieuTri_.bhytCanTrenKtc));
            }
            if (criteria.getBhytMaKhuVuc() != null) {
                specification = specification.and(buildStringSpecification(criteria.getBhytMaKhuVuc(), DotDieuTri_.bhytMaKhuVuc));
            }
            if (criteria.getBhytNgayBatDau() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getBhytNgayBatDau(), DotDieuTri_.bhytNgayBatDau));
            }
            if (criteria.getBhytNgayDuNamNam() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getBhytNgayDuNamNam(), DotDieuTri_.bhytNgayDuNamNam));
            }
            if (criteria.getBhytNgayHetHan() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getBhytNgayHetHan(), DotDieuTri_.bhytNgayHetHan));
            }
            if (criteria.getBhytNoiTinh() != null) {
                specification = specification.and(buildSpecification(criteria.getBhytNoiTinh(), DotDieuTri_.bhytNoiTinh));
            }
            if (criteria.getBhytSoThe() != null) {
                specification = specification.and(buildStringSpecification(criteria.getBhytSoThe(), DotDieuTri_.bhytSoThe));
            }
            if (criteria.getBhytTyLeMienGiam() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getBhytTyLeMienGiam(), DotDieuTri_.bhytTyLeMienGiam));
            }
            if (criteria.getBhytTyLeMienGiamKtc() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getBhytTyLeMienGiamKtc(), DotDieuTri_.bhytTyLeMienGiamKtc));
            }
            if (criteria.getCoBaoHiem() != null) {
                specification = specification.and(buildSpecification(criteria.getCoBaoHiem(), DotDieuTri_.coBaoHiem));
            }
            if (criteria.getBhytDiaChi() != null) {
                specification = specification.and(buildStringSpecification(criteria.getBhytDiaChi(), DotDieuTri_.bhytDiaChi));
            }
            if (criteria.getDoiTuongBhytTen() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDoiTuongBhytTen(), DotDieuTri_.doiTuongBhytTen));
            }
            if (criteria.getDungTuyen() != null) {
                specification = specification.and(buildSpecification(criteria.getDungTuyen(), DotDieuTri_.dungTuyen));
            }
            if (criteria.getGiayToTreEm() != null) {
                specification = specification.and(buildStringSpecification(criteria.getGiayToTreEm(), DotDieuTri_.giayToTreEm));
            }
            if (criteria.getLoaiGiayToTreEm() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLoaiGiayToTreEm(), DotDieuTri_.loaiGiayToTreEm));
            }
            if (criteria.getNgayMienCungChiTra() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNgayMienCungChiTra(), DotDieuTri_.ngayMienCungChiTra));
            }
            if (criteria.getTheTreEm() != null) {
                specification = specification.and(buildSpecification(criteria.getTheTreEm(), DotDieuTri_.theTreEm));
            }
            if (criteria.getThongTuyenBhxhXml4210() != null) {
                specification = specification.and(buildSpecification(criteria.getThongTuyenBhxhXml4210(), DotDieuTri_.thongTuyenBhxhXml4210));
            }
            if (criteria.getTrangThai() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTrangThai(), DotDieuTri_.trangThai));
            }
            if (criteria.getLoai() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLoai(), DotDieuTri_.loai));
            }
            if (criteria.getBhytNoiDkKcbbd() != null) {
                specification = specification.and(buildStringSpecification(criteria.getBhytNoiDkKcbbd(), DotDieuTri_.bhytNoiDkKcbbd));
            }
            if (criteria.getNam() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNam(), DotDieuTri_.nam));
            }
        }
        return specification;
    }
}

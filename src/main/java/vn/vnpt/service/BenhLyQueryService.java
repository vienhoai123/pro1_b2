package vn.vnpt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import vn.vnpt.domain.BenhLy;
import vn.vnpt.domain.*; // for static metamodels
import vn.vnpt.repository.BenhLyRepository;
import vn.vnpt.service.dto.BenhLyCriteria;
import vn.vnpt.service.dto.BenhLyDTO;
import vn.vnpt.service.mapper.BenhLyMapper;

/**
 * Service for executing complex queries for {@link BenhLy} entities in the database.
 * The main input is a {@link BenhLyCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link BenhLyDTO} or a {@link Page} of {@link BenhLyDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class BenhLyQueryService extends QueryService<BenhLy> {

    private final Logger log = LoggerFactory.getLogger(BenhLyQueryService.class);

    private final BenhLyRepository benhLyRepository;

    private final BenhLyMapper benhLyMapper;

    public BenhLyQueryService(BenhLyRepository benhLyRepository, BenhLyMapper benhLyMapper) {
        this.benhLyRepository = benhLyRepository;
        this.benhLyMapper = benhLyMapper;
    }

    /**
     * Return a {@link List} of {@link BenhLyDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<BenhLyDTO> findByCriteria(BenhLyCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<BenhLy> specification = createSpecification(criteria);
        return benhLyMapper.toDto(benhLyRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link BenhLyDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<BenhLyDTO> findByCriteria(BenhLyCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<BenhLy> specification = createSpecification(criteria);
        return benhLyRepository.findAll(specification, page)
            .map(benhLyMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(BenhLyCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<BenhLy> specification = createSpecification(criteria);
        return benhLyRepository.count(specification);
    }

    /**
     * Function to convert {@link BenhLyCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<BenhLy> createSpecification(BenhLyCriteria criteria) {
        Specification<BenhLy> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), BenhLy_.id));
            }
            if (criteria.getEnabled() != null) {
                specification = specification.and(buildSpecification(criteria.getEnabled(), BenhLy_.enabled));
            }
            if (criteria.getIcd() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIcd(), BenhLy_.icd));
            }
            if (criteria.getMoTa() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMoTa(), BenhLy_.moTa));
            }
            if (criteria.getMoTaTiengAnh() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMoTaTiengAnh(), BenhLy_.moTaTiengAnh));
            }
            if (criteria.getTenKhongDau() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTenKhongDau(), BenhLy_.tenKhongDau));
            }
            if (criteria.getVietTat() != null) {
                specification = specification.and(buildStringSpecification(criteria.getVietTat(), BenhLy_.vietTat));
            }
            if (criteria.getVncode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getVncode(), BenhLy_.vncode));
            }
            if (criteria.getNhomBenhLyId() != null) {
                specification = specification.and(buildSpecification(criteria.getNhomBenhLyId(),
                    root -> root.join(BenhLy_.nhomBenhLy, JoinType.LEFT).get(NhomBenhLy_.id)));
            }
        }
        return specification;
    }
}

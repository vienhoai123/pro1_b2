package vn.vnpt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import vn.vnpt.domain.BenhNhan;
import vn.vnpt.domain.*; // for static metamodels
import vn.vnpt.repository.BenhNhanRepository;
import vn.vnpt.service.dto.BenhNhanCriteria;
import vn.vnpt.service.dto.BenhNhanDTO;
import vn.vnpt.service.mapper.BenhNhanMapper;

/**
 * Service for executing complex queries for {@link BenhNhan} entities in the database.
 * The main input is a {@link BenhNhanCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link BenhNhanDTO} or a {@link Page} of {@link BenhNhanDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class BenhNhanQueryService extends QueryService<BenhNhan> {

    private final Logger log = LoggerFactory.getLogger(BenhNhanQueryService.class);

    private final BenhNhanRepository benhNhanRepository;

    private final BenhNhanMapper benhNhanMapper;

    public BenhNhanQueryService(BenhNhanRepository benhNhanRepository, BenhNhanMapper benhNhanMapper) {
        this.benhNhanRepository = benhNhanRepository;
        this.benhNhanMapper = benhNhanMapper;
    }

    /**
     * Return a {@link List} of {@link BenhNhanDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<BenhNhanDTO> findByCriteria(BenhNhanCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<BenhNhan> specification = createSpecification(criteria);
        return benhNhanMapper.toDto(benhNhanRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link BenhNhanDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<BenhNhanDTO> findByCriteria(BenhNhanCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<BenhNhan> specification = createSpecification(criteria);
        return benhNhanRepository.findAll(specification, page)
            .map(benhNhanMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(BenhNhanCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<BenhNhan> specification = createSpecification(criteria);
        return benhNhanRepository.count(specification);
    }

    /**
     * Function to convert {@link BenhNhanCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<BenhNhan> createSpecification(BenhNhanCriteria criteria) {
        Specification<BenhNhan> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), BenhNhan_.id));
            }
            if (criteria.getChiCoNamSinh() != null) {
                specification = specification.and(buildSpecification(criteria.getChiCoNamSinh(), BenhNhan_.chiCoNamSinh));
            }
            if (criteria.getCmnd() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCmnd(), BenhNhan_.cmnd));
            }
            if (criteria.getEmail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEmail(), BenhNhan_.email));
            }
            if (criteria.getEnabled() != null) {
                specification = specification.and(buildSpecification(criteria.getEnabled(), BenhNhan_.enabled));
            }
            if (criteria.getGioiTinh() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getGioiTinh(), BenhNhan_.gioiTinh));
            }
            if (criteria.getKhangThe() != null) {
                specification = specification.and(buildStringSpecification(criteria.getKhangThe(), BenhNhan_.khangThe));
            }
            if (criteria.getMaSoThue() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMaSoThue(), BenhNhan_.maSoThue));
            }
            if (criteria.getNgayCapCmnd() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNgayCapCmnd(), BenhNhan_.ngayCapCmnd));
            }
            if (criteria.getNgaySinh() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNgaySinh(), BenhNhan_.ngaySinh));
            }
            if (criteria.getNhomMau() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNhomMau(), BenhNhan_.nhomMau));
            }
            if (criteria.getNoiCapCmnd() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNoiCapCmnd(), BenhNhan_.noiCapCmnd));
            }
            if (criteria.getNoiLamViec() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNoiLamViec(), BenhNhan_.noiLamViec));
            }
            if (criteria.getPhone() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPhone(), BenhNhan_.phone));
            }
            if (criteria.getTen() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTen(), BenhNhan_.ten));
            }
            if (criteria.getSoNhaXom() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSoNhaXom(), BenhNhan_.soNhaXom));
            }
            if (criteria.getApThon() != null) {
                specification = specification.and(buildStringSpecification(criteria.getApThon(), BenhNhan_.apThon));
            }
            if (criteria.getDiaPhuongId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDiaPhuongId(), BenhNhan_.diaPhuongId));
            }
            if (criteria.getDiaChiThuongTru() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDiaChiThuongTru(), BenhNhan_.diaChiThuongTru));
            }
            if (criteria.getDanTocId() != null) {
                specification = specification.and(buildSpecification(criteria.getDanTocId(),
                    root -> root.join(BenhNhan_.danToc, JoinType.LEFT).get(DanToc_.id)));
            }
            if (criteria.getNgheNghiepId() != null) {
                specification = specification.and(buildSpecification(criteria.getNgheNghiepId(),
                    root -> root.join(BenhNhan_.ngheNghiep, JoinType.LEFT).get(NgheNghiep_.id)));
            }
            if (criteria.getQuocTichId() != null) {
                specification = specification.and(buildSpecification(criteria.getQuocTichId(),
                    root -> root.join(BenhNhan_.quocTich, JoinType.LEFT).get(Country_.id)));
            }
            if (criteria.getPhuongXaId() != null) {
                specification = specification.and(buildSpecification(criteria.getPhuongXaId(),
                    root -> root.join(BenhNhan_.phuongXa, JoinType.LEFT).get(PhuongXa_.id)));
            }
            if (criteria.getQuanHuyenId() != null) {
                specification = specification.and(buildSpecification(criteria.getQuanHuyenId(),
                    root -> root.join(BenhNhan_.quanHuyen, JoinType.LEFT).get(QuanHuyen_.id)));
            }
            if (criteria.getTinhThanhPhoId() != null) {
                specification = specification.and(buildSpecification(criteria.getTinhThanhPhoId(),
                    root -> root.join(BenhNhan_.tinhThanhPho, JoinType.LEFT).get(TinhThanhPho_.id)));
            }
            if (criteria.getUserExtraId() != null) {
                specification = specification.and(buildSpecification(criteria.getUserExtraId(),
                    root -> root.join(BenhNhan_.userExtra, JoinType.LEFT).get(UserExtra_.id)));
            }
        }
        return specification;
    }
}

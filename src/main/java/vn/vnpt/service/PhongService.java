package vn.vnpt.service;

import vn.vnpt.service.dto.PhongDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.Phong}.
 */
public interface PhongService {

    /**
     * Save a phong.
     *
     * @param phongDTO the entity to save.
     * @return the persisted entity.
     */
    PhongDTO save(PhongDTO phongDTO);

    /**
     * Get all the phongs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PhongDTO> findAll(Pageable pageable);

    /**
     * Get the "id" phong.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PhongDTO> findOne(Long id);

    /**
     * Delete the "id" phong.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}

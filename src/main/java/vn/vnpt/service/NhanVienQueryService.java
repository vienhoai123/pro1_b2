package vn.vnpt.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.criteria.JoinType;

import io.github.jhipster.service.filter.LongFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import vn.vnpt.domain.NhanVien;
import vn.vnpt.domain.*; // for static metamodels
import vn.vnpt.repository.NhanVienRepository;
import vn.vnpt.service.dto.*;
import vn.vnpt.service.mapper.NhanVienMapper;

/**
 * Service for executing complex queries for {@link NhanVien} entities in the database.
 * The main input is a {@link NhanVienCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link NhanVienDTO} or a {@link Page} of {@link NhanVienDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class NhanVienQueryService extends QueryService<NhanVien> {

    private final Logger log = LoggerFactory.getLogger(NhanVienQueryService.class);

    private final NhanVienRepository nhanVienRepository;

    private final NhanVienMapper nhanVienMapper;

    @Autowired
    private TPhongNhanVienQueryService tPhongNhanVienQueryService;
    @Autowired
    private PhongQueryService phongQueryService;

    public NhanVienQueryService(NhanVienRepository nhanVienRepository, NhanVienMapper nhanVienMapper) {
        this.nhanVienRepository = nhanVienRepository;
        this.nhanVienMapper = nhanVienMapper;
    }

    /**
     * Return a {@link List} of {@link NhanVienDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<NhanVienDTO> findByCriteria(NhanVienCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<NhanVien> specification = createSpecification(criteria);
        return nhanVienMapper.toDto(nhanVienRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link NhanVienDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<NhanVienDTO> findByCriteria(NhanVienCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<NhanVien> specification = createSpecification(criteria);
        return nhanVienRepository.findAll(specification, page)
            .map(nhanVienMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(NhanVienCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<NhanVien> specification = createSpecification(criteria);
        return nhanVienRepository.count(specification);
    }

    @Transactional(readOnly = true)
    public List<NhanVienDTO> findByKhoaId(Long khoaId){
        LongFilter filter = new LongFilter();
        filter.setEquals(khoaId);
        PhongCriteria phongCriteria = new PhongCriteria();
        phongCriteria.setKhoaId(filter);
        List<PhongDTO> phongDTOList = phongQueryService.findByCriteria(phongCriteria);
        List<Long> nhanVienIdList = new ArrayList<>();
        List<NhanVienDTO> result = new ArrayList<>();
        for (PhongDTO phongDTO : phongDTOList) {
            filter.setEquals(phongDTO.getId());
            TPhongNhanVienCriteria tPhongNhanVienCriteria = new TPhongNhanVienCriteria();
            tPhongNhanVienCriteria.setPhongId(filter);
            List<TPhongNhanVienDTO> tPhongNhanVienDTOList = tPhongNhanVienQueryService.findByCriteria(tPhongNhanVienCriteria);
            for (TPhongNhanVienDTO tPhongNhanVienDTO : tPhongNhanVienDTOList) {
                result.add(nhanVienMapper.toDto(nhanVienRepository.getOne(tPhongNhanVienDTO.getId())));
            }
        }
        return result;
    }

    /**
     * Function to convert {@link NhanVienCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<NhanVien> createSpecification(NhanVienCriteria criteria) {
        Specification<NhanVien> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), NhanVien_.id));
            }
            if (criteria.getTen() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTen(), NhanVien_.ten));
            }
            if (criteria.getChungChiHanhNghe() != null) {
                specification = specification.and(buildStringSpecification(criteria.getChungChiHanhNghe(), NhanVien_.chungChiHanhNghe));
            }
            if (criteria.getCmnd() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCmnd(), NhanVien_.cmnd));
            }
            if (criteria.getGioiTinh() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getGioiTinh(), NhanVien_.gioiTinh));
            }
            if (criteria.getNgaySinh() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNgaySinh(), NhanVien_.ngaySinh));
            }
            if (criteria.getChucDanhId() != null) {
                specification = specification.and(buildSpecification(criteria.getChucDanhId(),
                    root -> root.join(NhanVien_.chucDanh, JoinType.LEFT).get(ChucDanh_.id)));
            }
            if (criteria.getUserExtraId() != null) {
                specification = specification.and(buildSpecification(criteria.getUserExtraId(),
                    root -> root.join(NhanVien_.userExtra, JoinType.LEFT).get(UserExtra_.id)));
            }
            if (criteria.getChucVuId() != null) {
                specification = specification.and(buildSpecification(criteria.getChucVuId(),
                    root -> root.join(NhanVien_.chucVus, JoinType.LEFT).get(ChucVu_.id)));
            }
        }
        return specification;
    }
}

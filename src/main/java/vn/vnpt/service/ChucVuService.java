package vn.vnpt.service;

import vn.vnpt.service.dto.ChucVuDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.ChucVu}.
 */
public interface ChucVuService {

    /**
     * Save a chucVu.
     *
     * @param chucVuDTO the entity to save.
     * @return the persisted entity.
     */
    ChucVuDTO save(ChucVuDTO chucVuDTO);

    /**
     * Get all the chucVus.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ChucVuDTO> findAll(Pageable pageable);

    /**
     * Get the "id" chucVu.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ChucVuDTO> findOne(Long id);

    /**
     * Delete the "id" chucVu.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}

package vn.vnpt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import vn.vnpt.domain.DichVuKham;
import vn.vnpt.domain.*; // for static metamodels
import vn.vnpt.repository.DichVuKhamRepository;
import vn.vnpt.service.dto.DichVuKhamCriteria;
import vn.vnpt.service.dto.DichVuKhamDTO;
import vn.vnpt.service.mapper.DichVuKhamMapper;

/**
 * Service for executing complex queries for {@link DichVuKham} entities in the database.
 * The main input is a {@link DichVuKhamCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link DichVuKhamDTO} or a {@link Page} of {@link DichVuKhamDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class DichVuKhamQueryService extends QueryService<DichVuKham> {

    private final Logger log = LoggerFactory.getLogger(DichVuKhamQueryService.class);

    private final DichVuKhamRepository dichVuKhamRepository;

    private final DichVuKhamMapper dichVuKhamMapper;

    public DichVuKhamQueryService(DichVuKhamRepository dichVuKhamRepository, DichVuKhamMapper dichVuKhamMapper) {
        this.dichVuKhamRepository = dichVuKhamRepository;
        this.dichVuKhamMapper = dichVuKhamMapper;
    }

    /**
     * Return a {@link List} of {@link DichVuKhamDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<DichVuKhamDTO> findByCriteria(DichVuKhamCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<DichVuKham> specification = createSpecification(criteria);
        return dichVuKhamMapper.toDto(dichVuKhamRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link DichVuKhamDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<DichVuKhamDTO> findByCriteria(DichVuKhamCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<DichVuKham> specification = createSpecification(criteria);
        return dichVuKhamRepository.findAll(specification, page)
            .map(dichVuKhamMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(DichVuKhamCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<DichVuKham> specification = createSpecification(criteria);
        return dichVuKhamRepository.count(specification);
    }

    /**
     * Function to convert {@link DichVuKhamCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<DichVuKham> createSpecification(DichVuKhamCriteria criteria) {
        Specification<DichVuKham> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), DichVuKham_.id));
            }
            if (criteria.getDeleted() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDeleted(), DichVuKham_.deleted));
            }
            if (criteria.getEnabled() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getEnabled(), DichVuKham_.enabled));
            }
            if (criteria.getGioiHanChiDinh() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getGioiHanChiDinh(), DichVuKham_.gioiHanChiDinh));
            }
            if (criteria.getPhanTheoGioTinh() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPhanTheoGioTinh(), DichVuKham_.phanTheoGioTinh));
            }
            if (criteria.getTenHienThi() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTenHienThi(), DichVuKham_.tenHienThi));
            }
            if (criteria.getMaDungChung() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMaDungChung(), DichVuKham_.maDungChung));
            }
            if (criteria.getMaNoiBo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMaNoiBo(), DichVuKham_.maNoiBo));
            }
            if (criteria.getPhamViChiDinh() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPhamViChiDinh(), DichVuKham_.phamViChiDinh));
            }
            if (criteria.getDonViTinh() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDonViTinh(), DichVuKham_.donViTinh));
            }
            if (criteria.getDonGiaBenhVien() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDonGiaBenhVien(), DichVuKham_.donGiaBenhVien));
            }
            if (criteria.getDotThayDoiMaDichVuId() != null) {
                specification = specification.and(buildSpecification(criteria.getDotThayDoiMaDichVuId(),
                    root -> root.join(DichVuKham_.dotThayDoiMaDichVu, JoinType.LEFT).get(DotThayDoiMaDichVu_.id)));
            }
        }
        return specification;
    }
}

package vn.vnpt.service;

import vn.vnpt.service.dto.LoaiXetNghiemDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.LoaiXetNghiem}.
 */
public interface LoaiXetNghiemService {

    /**
     * Save a loaiXetNghiem.
     *
     * @param loaiXetNghiemDTO the entity to save.
     * @return the persisted entity.
     */
    LoaiXetNghiemDTO save(LoaiXetNghiemDTO loaiXetNghiemDTO);

    /**
     * Get all the loaiXetNghiems.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<LoaiXetNghiemDTO> findAll(Pageable pageable);

    /**
     * Get the "id" loaiXetNghiem.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<LoaiXetNghiemDTO> findOne(Long id);

    /**
     * Delete the "id" loaiXetNghiem.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}

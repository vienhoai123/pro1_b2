package vn.vnpt.service;

import vn.vnpt.service.dto.DanTocDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.DanToc}.
 */
public interface DanTocService {

    /**
     * Save a danToc.
     *
     * @param danTocDTO the entity to save.
     * @return the persisted entity.
     */
    DanTocDTO save(DanTocDTO danTocDTO);

    /**
     * Get all the danTocs.
     *
     * @return the list of entities.
     */
    List<DanTocDTO> findAll();

    /**
     * Get the "id" danToc.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<DanTocDTO> findOne(Long id);

    /**
     * Delete the "id" danToc.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}

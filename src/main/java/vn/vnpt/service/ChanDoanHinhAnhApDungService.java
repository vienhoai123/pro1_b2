package vn.vnpt.service;

import vn.vnpt.service.dto.ChanDoanHinhAnhApDungDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.ChanDoanHinhAnhApDung}.
 */
public interface ChanDoanHinhAnhApDungService {

    /**
     * Save a chanDoanHinhAnhApDung.
     *
     * @param chanDoanHinhAnhApDungDTO the entity to save.
     * @return the persisted entity.
     */
    ChanDoanHinhAnhApDungDTO save(ChanDoanHinhAnhApDungDTO chanDoanHinhAnhApDungDTO);

    /**
     * Get all the chanDoanHinhAnhApDungs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ChanDoanHinhAnhApDungDTO> findAll(Pageable pageable);

    /**
     * Get the "id" chanDoanHinhAnhApDung.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ChanDoanHinhAnhApDungDTO> findOne(Long id);

    /**
     * Delete the "id" chanDoanHinhAnhApDung.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}

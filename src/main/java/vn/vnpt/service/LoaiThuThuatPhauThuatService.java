package vn.vnpt.service;

import vn.vnpt.service.dto.LoaiThuThuatPhauThuatDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.LoaiThuThuatPhauThuat}.
 */
public interface LoaiThuThuatPhauThuatService {

    /**
     * Save a loaiThuThuatPhauThuat.
     *
     * @param loaiThuThuatPhauThuatDTO the entity to save.
     * @return the persisted entity.
     */
    LoaiThuThuatPhauThuatDTO save(LoaiThuThuatPhauThuatDTO loaiThuThuatPhauThuatDTO);

    /**
     * Get all the loaiThuThuatPhauThuats.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<LoaiThuThuatPhauThuatDTO> findAll(Pageable pageable);

    /**
     * Get the "id" loaiThuThuatPhauThuat.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<LoaiThuThuatPhauThuatDTO> findOne(Long id);

    /**
     * Delete the "id" loaiThuThuatPhauThuat.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}

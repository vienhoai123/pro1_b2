package vn.vnpt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import vn.vnpt.domain.TPhanNhomXetNghiem;
import vn.vnpt.domain.*; // for static metamodels
import vn.vnpt.repository.TPhanNhomXetNghiemRepository;
import vn.vnpt.service.dto.TPhanNhomXetNghiemCriteria;
import vn.vnpt.service.dto.TPhanNhomXetNghiemDTO;
import vn.vnpt.service.mapper.TPhanNhomXetNghiemMapper;

/**
 * Service for executing complex queries for {@link TPhanNhomXetNghiem} entities in the database.
 * The main input is a {@link TPhanNhomXetNghiemCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link TPhanNhomXetNghiemDTO} or a {@link Page} of {@link TPhanNhomXetNghiemDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TPhanNhomXetNghiemQueryService extends QueryService<TPhanNhomXetNghiem> {

    private final Logger log = LoggerFactory.getLogger(TPhanNhomXetNghiemQueryService.class);

    private final TPhanNhomXetNghiemRepository tPhanNhomXetNghiemRepository;

    private final TPhanNhomXetNghiemMapper tPhanNhomXetNghiemMapper;

    public TPhanNhomXetNghiemQueryService(TPhanNhomXetNghiemRepository tPhanNhomXetNghiemRepository, TPhanNhomXetNghiemMapper tPhanNhomXetNghiemMapper) {
        this.tPhanNhomXetNghiemRepository = tPhanNhomXetNghiemRepository;
        this.tPhanNhomXetNghiemMapper = tPhanNhomXetNghiemMapper;
    }

    /**
     * Return a {@link List} of {@link TPhanNhomXetNghiemDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<TPhanNhomXetNghiemDTO> findByCriteria(TPhanNhomXetNghiemCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<TPhanNhomXetNghiem> specification = createSpecification(criteria);
        return tPhanNhomXetNghiemMapper.toDto(tPhanNhomXetNghiemRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link TPhanNhomXetNghiemDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<TPhanNhomXetNghiemDTO> findByCriteria(TPhanNhomXetNghiemCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<TPhanNhomXetNghiem> specification = createSpecification(criteria);
        return tPhanNhomXetNghiemRepository.findAll(specification, page)
            .map(tPhanNhomXetNghiemMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TPhanNhomXetNghiemCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<TPhanNhomXetNghiem> specification = createSpecification(criteria);
        return tPhanNhomXetNghiemRepository.count(specification);
    }

    /**
     * Function to convert {@link TPhanNhomXetNghiemCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<TPhanNhomXetNghiem> createSpecification(TPhanNhomXetNghiemCriteria criteria) {
        Specification<TPhanNhomXetNghiem> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), TPhanNhomXetNghiem_.id));
            }
            if (criteria.getNhomXNId() != null) {
                specification = specification.and(buildSpecification(criteria.getNhomXNId(),
                    root -> root.join(TPhanNhomXetNghiem_.nhomXN, JoinType.LEFT).get(NhomXetNghiem_.id)));
            }
            if (criteria.getXNId() != null) {
                specification = specification.and(buildSpecification(criteria.getXNId(),
                    root -> root.join(TPhanNhomXetNghiem_.xN, JoinType.LEFT).get(XetNghiem_.id)));
            }
        }
        return specification;
    }
}

package vn.vnpt.service;

import vn.vnpt.service.dto.ThuThuatPhauThuatDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.ThuThuatPhauThuat}.
 */
public interface ThuThuatPhauThuatService {

    /**
     * Save a thuThuatPhauThuat.
     *
     * @param thuThuatPhauThuatDTO the entity to save.
     * @return the persisted entity.
     */
    ThuThuatPhauThuatDTO save(ThuThuatPhauThuatDTO thuThuatPhauThuatDTO);

    /**
     * Get all the thuThuatPhauThuats.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ThuThuatPhauThuatDTO> findAll(Pageable pageable);

    /**
     * Get the "id" thuThuatPhauThuat.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ThuThuatPhauThuatDTO> findOne(Long id);

    /**
     * Delete the "id" thuThuatPhauThuat.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}

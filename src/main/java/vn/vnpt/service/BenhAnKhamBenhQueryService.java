package vn.vnpt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import vn.vnpt.domain.BenhAnKhamBenh;
import vn.vnpt.domain.*; // for static metamodels
import vn.vnpt.repository.BenhAnKhamBenhRepository;
import vn.vnpt.service.dto.BenhAnKhamBenhCriteria;
import vn.vnpt.service.dto.BenhAnKhamBenhDTO;
import vn.vnpt.service.mapper.BenhAnKhamBenhMapper;

/**
 * Service for executing complex queries for {@link BenhAnKhamBenh} entities in the database.
 * The main input is a {@link BenhAnKhamBenhCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link BenhAnKhamBenhDTO} or a {@link Page} of {@link BenhAnKhamBenhDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class BenhAnKhamBenhQueryService extends QueryService<BenhAnKhamBenh> {

    private final Logger log = LoggerFactory.getLogger(BenhAnKhamBenhQueryService.class);

    private final BenhAnKhamBenhRepository benhAnKhamBenhRepository;

    private final BenhAnKhamBenhMapper benhAnKhamBenhMapper;

    public BenhAnKhamBenhQueryService(BenhAnKhamBenhRepository benhAnKhamBenhRepository, BenhAnKhamBenhMapper benhAnKhamBenhMapper) {
        this.benhAnKhamBenhRepository = benhAnKhamBenhRepository;
        this.benhAnKhamBenhMapper = benhAnKhamBenhMapper;
    }

    /**
     * Return a {@link List} of {@link BenhAnKhamBenhDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<BenhAnKhamBenhDTO> findByCriteria(BenhAnKhamBenhCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<BenhAnKhamBenh> specification = createSpecification(criteria);
        return benhAnKhamBenhMapper.toDto(benhAnKhamBenhRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link BenhAnKhamBenhDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<BenhAnKhamBenhDTO> findByCriteria(BenhAnKhamBenhCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<BenhAnKhamBenh> specification = createSpecification(criteria);
        return benhAnKhamBenhRepository.findAll(specification, page)
            .map(benhAnKhamBenhMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(BenhAnKhamBenhCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<BenhAnKhamBenh> specification = createSpecification(criteria);
        return benhAnKhamBenhRepository.count(specification);
    }

    /**
     * Function to convert {@link BenhAnKhamBenhCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<BenhAnKhamBenh> createSpecification(BenhAnKhamBenhCriteria criteria) {
        Specification<BenhAnKhamBenh> specification = Specification.where(null);
        if (criteria != null) {

            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(),BenhAnKhamBenh_.id));
            }
            if (criteria.getBenhNhanId() != null) {
                specification = specification.and(buildSpecification(criteria.getBenhNhanId(),
                    root -> root.join(BenhAnKhamBenh_.benhNhan, JoinType.LEFT).get(BenhNhan_.id)));
            }
            if (criteria.getDonViId() != null) {
                specification = specification.and(buildSpecification(criteria.getDonViId(),
                    root -> root.join(BenhAnKhamBenh_.donVi, JoinType.LEFT).get(DonVi_.id)));
            }
            if (criteria.getBenhNhanCu() != null) {
                specification = specification.and(buildSpecification(criteria.getBenhNhanCu(), BenhAnKhamBenh_.benhNhanCu));
            }
            if (criteria.getCanhBao() != null) {
                specification = specification.and(buildSpecification(criteria.getCanhBao(), BenhAnKhamBenh_.canhBao));
            }
            if (criteria.getCapCuu() != null) {
                specification = specification.and(buildSpecification(criteria.getCapCuu(), BenhAnKhamBenh_.capCuu));
            }
            if (criteria.getChiCoNamSinh() != null) {
                specification = specification.and(buildSpecification(criteria.getChiCoNamSinh(), BenhAnKhamBenh_.chiCoNamSinh));
            }
            if (criteria.getCmndBenhNhan() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCmndBenhNhan(), BenhAnKhamBenh_.cmndBenhNhan));
            }
            if (criteria.getCmndNguoiNha() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCmndNguoiNha(), BenhAnKhamBenh_.cmndNguoiNha));
            }
            if (criteria.getCoBaoHiem() != null) {
                specification = specification.and(buildSpecification(criteria.getCoBaoHiem(), BenhAnKhamBenh_.coBaoHiem));
            }
            if (criteria.getDiaChi() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDiaChi(), BenhAnKhamBenh_.diaChi));
            }
            if (criteria.getEmail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEmail(), BenhAnKhamBenh_.email));
            }
            if (criteria.getGioiTinh() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getGioiTinh(), BenhAnKhamBenh_.gioiTinh));
            }
            if (criteria.getKhangThe() != null) {
                specification = specification.and(buildStringSpecification(criteria.getKhangThe(), BenhAnKhamBenh_.khangThe));
            }
            if (criteria.getLanKhamTrongNgay() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLanKhamTrongNgay(), BenhAnKhamBenh_.lanKhamTrongNgay));
            }
            if (criteria.getLoaiGiayToTreEm() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLoaiGiayToTreEm(), BenhAnKhamBenh_.loaiGiayToTreEm));
            }
            if (criteria.getNgayCapCmnd() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNgayCapCmnd(), BenhAnKhamBenh_.ngayCapCmnd));
            }
            if (criteria.getNgayMienCungChiTra() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNgayMienCungChiTra(), BenhAnKhamBenh_.ngayMienCungChiTra));
            }
            if (criteria.getNgaySinh() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNgaySinh(), BenhAnKhamBenh_.ngaySinh));
            }
            if (criteria.getNhomMau() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNhomMau(), BenhAnKhamBenh_.nhomMau));
            }
            if (criteria.getNoiCapCmnd() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNoiCapCmnd(), BenhAnKhamBenh_.noiCapCmnd));
            }
            if (criteria.getNoiLamViec() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNoiLamViec(), BenhAnKhamBenh_.noiLamViec));
            }
            if (criteria.getPhone() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPhone(), BenhAnKhamBenh_.phone));
            }
            if (criteria.getPhoneNguoiNha() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPhoneNguoiNha(), BenhAnKhamBenh_.phoneNguoiNha));
            }
            if (criteria.getQuocTich() != null) {
                specification = specification.and(buildStringSpecification(criteria.getQuocTich(), BenhAnKhamBenh_.quocTich));
            }
            if (criteria.getTenBenhNhan() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTenBenhNhan(), BenhAnKhamBenh_.tenBenhNhan));
            }
            if (criteria.getTenNguoiNha() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTenNguoiNha(), BenhAnKhamBenh_.tenNguoiNha));
            }
            if (criteria.getThangTuoi() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getThangTuoi(), BenhAnKhamBenh_.thangTuoi));
            }
            if (criteria.getThoiGianTiepNhan() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getThoiGianTiepNhan(), BenhAnKhamBenh_.thoiGianTiepNhan));
            }
            if (criteria.getTuoi() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTuoi(), BenhAnKhamBenh_.tuoi));
            }
            if (criteria.getUuTien() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUuTien(), BenhAnKhamBenh_.uuTien));
            }
            if (criteria.getUuid() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUuid(), BenhAnKhamBenh_.uuid));
            }
            if (criteria.getTrangThai() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTrangThai(), BenhAnKhamBenh_.trangThai));
            }
            if (criteria.getMaKhoaHoanTatKham() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getMaKhoaHoanTatKham(), BenhAnKhamBenh_.maKhoaHoanTatKham));
            }
            if (criteria.getMaPhongHoanTatKham() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getMaPhongHoanTatKham(), BenhAnKhamBenh_.maPhongHoanTatKham));
            }
            if (criteria.getTenKhoaHoanTatKham() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTenKhoaHoanTatKham(), BenhAnKhamBenh_.tenKhoaHoanTatKham));
            }
            if (criteria.getTenPhongHoanTatKham() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTenPhongHoanTatKham(), BenhAnKhamBenh_.tenPhongHoanTatKham));
            }
            if (criteria.getThoiGianHoanTatKham() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getThoiGianHoanTatKham(), BenhAnKhamBenh_.thoiGianHoanTatKham));
            }
            if (criteria.getSoBenhAn() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSoBenhAn(), BenhAnKhamBenh_.soBenhAn));
            }
            if (criteria.getSoBenhAnKhoa() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSoBenhAnKhoa(), BenhAnKhamBenh_.soBenhAnKhoa));
            }
            if (criteria.getNam() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNam(), BenhAnKhamBenh_.nam));
            }
            if (criteria.getNhanVienTiepNhanId() != null) {
                specification = specification.and(buildSpecification(criteria.getNhanVienTiepNhanId(),
                    root -> root.join(BenhAnKhamBenh_.nhanVienTiepNhan, JoinType.LEFT).get(NhanVien_.id)));
            }
            if (criteria.getPhongTiepNhanId() != null) {
                specification = specification.and(buildSpecification(criteria.getPhongTiepNhanId(),
                    root -> root.join(BenhAnKhamBenh_.phongTiepNhan, JoinType.LEFT).get(Phong_.id)));
            }
        }
        return specification;
    }
}

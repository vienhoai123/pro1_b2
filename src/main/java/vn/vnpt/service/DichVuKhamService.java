package vn.vnpt.service;

import vn.vnpt.service.dto.DichVuKhamDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.vnpt.service.dto.customdto.ThongTinDichVuKhamDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.DichVuKham}.
 */
public interface DichVuKhamService {

    /**
     * Save a dichVuKham.
     *
     * @param dichVuKhamDTO the entity to save.
     * @return the persisted entity.
     */
    DichVuKhamDTO save(DichVuKhamDTO dichVuKhamDTO);

    /**
     * Get all the dichVuKhams.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<DichVuKhamDTO> findAll(Pageable pageable);


    /**
     * Get the "id" dichVuKham.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<DichVuKhamDTO> findOne(Long id);

    /**
     * Delete the "id" dichVuKham.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    List<ThongTinDichVuKhamDTO> getDanhSachDichVuKham();

}

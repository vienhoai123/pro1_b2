package vn.vnpt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import vn.vnpt.domain.BenhYhct;
import vn.vnpt.domain.*; // for static metamodels
import vn.vnpt.repository.BenhYhctRepository;
import vn.vnpt.service.dto.BenhYhctCriteria;
import vn.vnpt.service.dto.BenhYhctDTO;
import vn.vnpt.service.mapper.BenhYhctMapper;

/**
 * Service for executing complex queries for {@link BenhYhct} entities in the database.
 * The main input is a {@link BenhYhctCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link BenhYhctDTO} or a {@link Page} of {@link BenhYhctDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class BenhYhctQueryService extends QueryService<BenhYhct> {

    private final Logger log = LoggerFactory.getLogger(BenhYhctQueryService.class);

    private final BenhYhctRepository benhYhctRepository;

    private final BenhYhctMapper benhYhctMapper;

    public BenhYhctQueryService(BenhYhctRepository benhYhctRepository, BenhYhctMapper benhYhctMapper) {
        this.benhYhctRepository = benhYhctRepository;
        this.benhYhctMapper = benhYhctMapper;
    }

    /**
     * Return a {@link List} of {@link BenhYhctDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<BenhYhctDTO> findByCriteria(BenhYhctCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<BenhYhct> specification = createSpecification(criteria);
        return benhYhctMapper.toDto(benhYhctRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link BenhYhctDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<BenhYhctDTO> findByCriteria(BenhYhctCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<BenhYhct> specification = createSpecification(criteria);
        return benhYhctRepository.findAll(specification, page)
            .map(benhYhctMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(BenhYhctCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<BenhYhct> specification = createSpecification(criteria);
        return benhYhctRepository.count(specification);
    }

    /**
     * Function to convert {@link BenhYhctCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<BenhYhct> createSpecification(BenhYhctCriteria criteria) {
        Specification<BenhYhct> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), BenhYhct_.id));
            }
            if (criteria.getMa() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMa(), BenhYhct_.ma));
            }
            if (criteria.getTen() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTen(), BenhYhct_.ten));
            }
            if (criteria.getBenhLyId() != null) {
                specification = specification.and(buildSpecification(criteria.getBenhLyId(),
                    root -> root.join(BenhYhct_.benhLy, JoinType.LEFT).get(BenhLy_.id)));
            }
        }
        return specification;
    }
}

package vn.vnpt.service;

import vn.vnpt.service.dto.ChanDoanHinhAnhDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.ChanDoanHinhAnh}.
 */
public interface ChanDoanHinhAnhService {

    /**
     * Save a chanDoanHinhAnh.
     *
     * @param chanDoanHinhAnhDTO the entity to save.
     * @return the persisted entity.
     */
    ChanDoanHinhAnhDTO save(ChanDoanHinhAnhDTO chanDoanHinhAnhDTO);

    /**
     * Get all the chanDoanHinhAnhs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ChanDoanHinhAnhDTO> findAll(Pageable pageable);

    /**
     * Get the "id" chanDoanHinhAnh.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ChanDoanHinhAnhDTO> findOne(Long id);

    /**
     * Delete the "id" chanDoanHinhAnh.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}

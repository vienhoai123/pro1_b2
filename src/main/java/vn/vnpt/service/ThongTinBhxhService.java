package vn.vnpt.service;

import vn.vnpt.service.dto.ThongTinBhxhDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.ThongTinBhxh}.
 */
public interface ThongTinBhxhService {

    /**
     * Save a thongTinBhxh.
     *
     * @param thongTinBhxhDTO the entity to save.
     * @return the persisted entity.
     */
    ThongTinBhxhDTO save(ThongTinBhxhDTO thongTinBhxhDTO);

    /**
     * Get all the thongTinBhxhs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ThongTinBhxhDTO> findAll(Pageable pageable);

    /**
     * Get the "id" thongTinBhxh.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ThongTinBhxhDTO> findOne(Long id);

    /**
     * Delete the "id" thongTinBhxh.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}

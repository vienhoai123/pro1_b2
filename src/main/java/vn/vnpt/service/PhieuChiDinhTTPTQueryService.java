package vn.vnpt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import vn.vnpt.domain.PhieuChiDinhTTPT;
import vn.vnpt.domain.*; // for static metamodels
import vn.vnpt.repository.PhieuChiDinhTTPTRepository;
import vn.vnpt.service.dto.PhieuChiDinhTTPTCriteria;
import vn.vnpt.service.dto.PhieuChiDinhTTPTDTO;
import vn.vnpt.service.mapper.PhieuChiDinhTTPTMapper;

/**
 * Service for executing complex queries for {@link PhieuChiDinhTTPT} entities in the database.
 * The main input is a {@link PhieuChiDinhTTPTCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link PhieuChiDinhTTPTDTO} or a {@link Page} of {@link PhieuChiDinhTTPTDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class PhieuChiDinhTTPTQueryService extends QueryService<PhieuChiDinhTTPT> {

    private final Logger log = LoggerFactory.getLogger(PhieuChiDinhTTPTQueryService.class);

    private final PhieuChiDinhTTPTRepository phieuChiDinhTTPTRepository;

    private final PhieuChiDinhTTPTMapper phieuChiDinhTTPTMapper;

    public PhieuChiDinhTTPTQueryService(PhieuChiDinhTTPTRepository phieuChiDinhTTPTRepository, PhieuChiDinhTTPTMapper phieuChiDinhTTPTMapper) {
        this.phieuChiDinhTTPTRepository = phieuChiDinhTTPTRepository;
        this.phieuChiDinhTTPTMapper = phieuChiDinhTTPTMapper;
    }

    /**
     * Return a {@link List} of {@link PhieuChiDinhTTPTDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<PhieuChiDinhTTPTDTO> findByCriteria(PhieuChiDinhTTPTCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<PhieuChiDinhTTPT> specification = createSpecification(criteria);
        return phieuChiDinhTTPTMapper.toDto(phieuChiDinhTTPTRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link PhieuChiDinhTTPTDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<PhieuChiDinhTTPTDTO> findByCriteria(PhieuChiDinhTTPTCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<PhieuChiDinhTTPT> specification = createSpecification(criteria);
        return phieuChiDinhTTPTRepository.findAll(specification, page)
            .map(phieuChiDinhTTPTMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(PhieuChiDinhTTPTCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<PhieuChiDinhTTPT> specification = createSpecification(criteria);
        return phieuChiDinhTTPTRepository.count(specification);
    }

    /**
     * Function to convert {@link PhieuChiDinhTTPTCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<PhieuChiDinhTTPT> createSpecification(PhieuChiDinhTTPTCriteria criteria) {
        Specification<PhieuChiDinhTTPT> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), PhieuChiDinhTTPT_.id));
            }
            if (criteria.getChanDoanTongQuat() != null) {
                specification = specification.and(buildStringSpecification(criteria.getChanDoanTongQuat(), PhieuChiDinhTTPT_.chanDoanTongQuat));
            }
            if (criteria.getGhiChu() != null) {
                specification = specification.and(buildStringSpecification(criteria.getGhiChu(), PhieuChiDinhTTPT_.ghiChu));
            }
            if (criteria.getKetQuaTongQuat() != null) {
                specification = specification.and(buildStringSpecification(criteria.getKetQuaTongQuat(), PhieuChiDinhTTPT_.ketQuaTongQuat));
            }
            if (criteria.getNam() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNam(), PhieuChiDinhTTPT_.nam));
            }
//            if (criteria.getDonViId() != null) {
//                specification = specification.and(buildSpecification(criteria.getDonViId(),
//                    root -> root.join(PhieuChiDinhTTPT_.donViId, JoinType.LEFT).get(BenhAnKhamBenh_.DON_VI_ID)));
//            }
//            if (criteria.getBenhNhanId() != null) {
//                specification = specification.and(buildSpecification(criteria.getBenhNhanId(),
//                    root -> root.join(PhieuChiDinhTTPT_.benhNhanId, JoinType.LEFT).get(BenhAnKhamBenh_.BENH_NHAN_ID)));
//            }
            if (criteria.getBakbId() != null) {
                specification = specification.and(buildSpecification(criteria.getBakbId(),
                    root -> root.join(PhieuChiDinhTTPT_.bakbId, JoinType.LEFT).get(BenhAnKhamBenh_.ID)));
            }
        }
        return specification;
    }
}

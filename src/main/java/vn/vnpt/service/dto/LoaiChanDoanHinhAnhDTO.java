package vn.vnpt.service.dto;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link vn.vnpt.domain.LoaiChanDoanHinhAnh} entity.
 */
public class LoaiChanDoanHinhAnhDTO implements Serializable {
    
    private Long id;

    /**
     * Tên loại chẩn đoán hình ảnh
     */
    @Size(max = 500)
    @ApiModelProperty(value = "Tên loại chẩn đoán hình ảnh")
    private String ten;

    /**
     * Mô tả loại chẩn đoán hình ảnh
     */
    @Size(max = 500)
    @ApiModelProperty(value = "Mô tả loại chẩn đoán hình ảnh")
    private String moTa;

    /**
     * Trạng thái có hiệu lực: 1: Có hiệu lực. 0: không có hiệu lực
     */
    @ApiModelProperty(value = "Trạng thái có hiệu lực: 1: Có hiệu lực. 0: không có hiệu lực")
    private Boolean enable;

    /**
     * Số càng nhỏ thì độ ưu tiên lớn
     */
    @NotNull
    @ApiModelProperty(value = "Số càng nhỏ thì độ ưu tiên lớn", required = true)
    private Integer uuTien;

    /**
     * Tên loại nhỏ hơn chẩn đoán hình ảnh
     */
    @Size(max = 500)
    @ApiModelProperty(value = "Tên loại nhỏ hơn chẩn đoán hình ảnh")
    private String maPhanLoai;


    private Long donViId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public Boolean isEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public Integer getUuTien() {
        return uuTien;
    }

    public void setUuTien(Integer uuTien) {
        this.uuTien = uuTien;
    }

    public String getMaPhanLoai() {
        return maPhanLoai;
    }

    public void setMaPhanLoai(String maPhanLoai) {
        this.maPhanLoai = maPhanLoai;
    }

    public Long getDonViId() {
        return donViId;
    }

    public void setDonViId(Long donViId) {
        this.donViId = donViId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        LoaiChanDoanHinhAnhDTO loaiChanDoanHinhAnhDTO = (LoaiChanDoanHinhAnhDTO) o;
        if (loaiChanDoanHinhAnhDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), loaiChanDoanHinhAnhDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "LoaiChanDoanHinhAnhDTO{" +
            "id=" + getId() +
            ", ten='" + getTen() + "'" +
            ", moTa='" + getMoTa() + "'" +
            ", enable='" + isEnable() + "'" +
            ", uuTien=" + getUuTien() +
            ", maPhanLoai='" + getMaPhanLoai() + "'" +
            ", donViId=" + getDonViId() +
            "}";
    }
}

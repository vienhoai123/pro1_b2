package vn.vnpt.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.BigDecimalFilter;

/**
 * Criteria class for the {@link vn.vnpt.domain.ThuThuatPhauThuat} entity. This class is used
 * in {@link vn.vnpt.web.rest.ThuThuatPhauThuatResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /thu-thuat-phau-thuats?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ThuThuatPhauThuatCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private BooleanFilter deleted;

    private BooleanFilter dichVuYeuCau;

    private BigDecimalFilter donGiaBenhVien;

    private BooleanFilter enabled;

    private BigDecimalFilter goiHanChiDinh;

    private BooleanFilter phamViChiDinh;

    private IntegerFilter phanTheoGioiTinh;

    private StringFilter ten;

    private StringFilter tenHienThi;

    private StringFilter maNoiBo;

    private StringFilter maDungChung;

    private LongFilter donViId;

    private LongFilter dotMaId;

    private LongFilter loaittptId;

    public ThuThuatPhauThuatCriteria() {
    }

    public ThuThuatPhauThuatCriteria(ThuThuatPhauThuatCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.deleted = other.deleted == null ? null : other.deleted.copy();
        this.dichVuYeuCau = other.dichVuYeuCau == null ? null : other.dichVuYeuCau.copy();
        this.donGiaBenhVien = other.donGiaBenhVien == null ? null : other.donGiaBenhVien.copy();
        this.enabled = other.enabled == null ? null : other.enabled.copy();
        this.goiHanChiDinh = other.goiHanChiDinh == null ? null : other.goiHanChiDinh.copy();
        this.phamViChiDinh = other.phamViChiDinh == null ? null : other.phamViChiDinh.copy();
        this.phanTheoGioiTinh = other.phanTheoGioiTinh == null ? null : other.phanTheoGioiTinh.copy();
        this.ten = other.ten == null ? null : other.ten.copy();
        this.tenHienThi = other.tenHienThi == null ? null : other.tenHienThi.copy();
        this.maNoiBo = other.maNoiBo == null ? null : other.maNoiBo.copy();
        this.maDungChung = other.maDungChung == null ? null : other.maDungChung.copy();
        this.donViId = other.donViId == null ? null : other.donViId.copy();
        this.dotMaId = other.dotMaId == null ? null : other.dotMaId.copy();
        this.loaittptId = other.loaittptId == null ? null : other.loaittptId.copy();
    }

    @Override
    public ThuThuatPhauThuatCriteria copy() {
        return new ThuThuatPhauThuatCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public BooleanFilter getDeleted() {
        return deleted;
    }

    public void setDeleted(BooleanFilter deleted) {
        this.deleted = deleted;
    }

    public BooleanFilter getDichVuYeuCau() {
        return dichVuYeuCau;
    }

    public void setDichVuYeuCau(BooleanFilter dichVuYeuCau) {
        this.dichVuYeuCau = dichVuYeuCau;
    }

    public BigDecimalFilter getDonGiaBenhVien() {
        return donGiaBenhVien;
    }

    public void setDonGiaBenhVien(BigDecimalFilter donGiaBenhVien) {
        this.donGiaBenhVien = donGiaBenhVien;
    }

    public BooleanFilter getEnabled() {
        return enabled;
    }

    public void setEnabled(BooleanFilter enabled) {
        this.enabled = enabled;
    }

    public BigDecimalFilter getGoiHanChiDinh() {
        return goiHanChiDinh;
    }

    public void setGoiHanChiDinh(BigDecimalFilter goiHanChiDinh) {
        this.goiHanChiDinh = goiHanChiDinh;
    }

    public BooleanFilter getPhamViChiDinh() {
        return phamViChiDinh;
    }

    public void setPhamViChiDinh(BooleanFilter phamViChiDinh) {
        this.phamViChiDinh = phamViChiDinh;
    }

    public IntegerFilter getPhanTheoGioiTinh() {
        return phanTheoGioiTinh;
    }

    public void setPhanTheoGioiTinh(IntegerFilter phanTheoGioiTinh) {
        this.phanTheoGioiTinh = phanTheoGioiTinh;
    }

    public StringFilter getTen() {
        return ten;
    }

    public void setTen(StringFilter ten) {
        this.ten = ten;
    }

    public StringFilter getTenHienThi() {
        return tenHienThi;
    }

    public void setTenHienThi(StringFilter tenHienThi) {
        this.tenHienThi = tenHienThi;
    }

    public StringFilter getMaNoiBo() {
        return maNoiBo;
    }

    public void setMaNoiBo(StringFilter maNoiBo) {
        this.maNoiBo = maNoiBo;
    }

    public StringFilter getMaDungChung() {
        return maDungChung;
    }

    public void setMaDungChung(StringFilter maDungChung) {
        this.maDungChung = maDungChung;
    }

    public LongFilter getDonViId() {
        return donViId;
    }

    public void setDonViId(LongFilter donViId) {
        this.donViId = donViId;
    }

    public LongFilter getDotMaId() {
        return dotMaId;
    }

    public void setDotMaId(LongFilter dotMaId) {
        this.dotMaId = dotMaId;
    }

    public LongFilter getLoaittptId() {
        return loaittptId;
    }

    public void setLoaittptId(LongFilter loaittptId) {
        this.loaittptId = loaittptId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ThuThuatPhauThuatCriteria that = (ThuThuatPhauThuatCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(deleted, that.deleted) &&
            Objects.equals(dichVuYeuCau, that.dichVuYeuCau) &&
            Objects.equals(donGiaBenhVien, that.donGiaBenhVien) &&
            Objects.equals(enabled, that.enabled) &&
            Objects.equals(goiHanChiDinh, that.goiHanChiDinh) &&
            Objects.equals(phamViChiDinh, that.phamViChiDinh) &&
            Objects.equals(phanTheoGioiTinh, that.phanTheoGioiTinh) &&
            Objects.equals(ten, that.ten) &&
            Objects.equals(tenHienThi, that.tenHienThi) &&
            Objects.equals(maNoiBo, that.maNoiBo) &&
            Objects.equals(maDungChung, that.maDungChung) &&
            Objects.equals(donViId, that.donViId) &&
            Objects.equals(dotMaId, that.dotMaId) &&
            Objects.equals(loaittptId, that.loaittptId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        deleted,
        dichVuYeuCau,
        donGiaBenhVien,
        enabled,
        goiHanChiDinh,
        phamViChiDinh,
        phanTheoGioiTinh,
        ten,
        tenHienThi,
        maNoiBo,
        maDungChung,
        donViId,
        dotMaId,
        loaittptId
        );
    }

    @Override
    public String toString() {
        return "ThuThuatPhauThuatCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (deleted != null ? "deleted=" + deleted + ", " : "") +
                (dichVuYeuCau != null ? "dichVuYeuCau=" + dichVuYeuCau + ", " : "") +
                (donGiaBenhVien != null ? "donGiaBenhVien=" + donGiaBenhVien + ", " : "") +
                (enabled != null ? "enabled=" + enabled + ", " : "") +
                (goiHanChiDinh != null ? "goiHanChiDinh=" + goiHanChiDinh + ", " : "") +
                (phamViChiDinh != null ? "phamViChiDinh=" + phamViChiDinh + ", " : "") +
                (phanTheoGioiTinh != null ? "phanTheoGioiTinh=" + phanTheoGioiTinh + ", " : "") +
                (ten != null ? "ten=" + ten + ", " : "") +
                (tenHienThi != null ? "tenHienThi=" + tenHienThi + ", " : "") +
                (maNoiBo != null ? "maNoiBo=" + maNoiBo + ", " : "") +
                (maDungChung != null ? "maDungChung=" + maDungChung + ", " : "") +
                (donViId != null ? "donViId=" + donViId + ", " : "") +
                (dotMaId != null ? "dotMaId=" + dotMaId + ", " : "") +
                (loaittptId != null ? "loaittptId=" + loaittptId + ", " : "") +
            "}";
    }

}

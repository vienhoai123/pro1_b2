package vn.vnpt.service.dto;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link vn.vnpt.domain.NhomBenhLy} entity.
 */
public class NhomBenhLyDTO implements Serializable {
    
    private Long id;

    /**
     * Mô tả nhóm bệnh lý
     */
    @Size(max = 500)
    @ApiModelProperty(value = "Mô tả nhóm bệnh lý")
    private String moTa;

    /**
     * Tên nhóm bệnh lý
     */
    @NotNull
    @Size(max = 500)
    @ApiModelProperty(value = "Tên nhóm bệnh lý", required = true)
    private String ten;

    /**
     * Mã loại bệnh lý
     */
    @ApiModelProperty(value = "Mã loại bệnh lý")

    private Long loaiBenhLyId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public Long getLoaiBenhLyId() {
        return loaiBenhLyId;
    }

    public void setLoaiBenhLyId(Long loaiBenhLyId) {
        this.loaiBenhLyId = loaiBenhLyId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        NhomBenhLyDTO nhomBenhLyDTO = (NhomBenhLyDTO) o;
        if (nhomBenhLyDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), nhomBenhLyDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "NhomBenhLyDTO{" +
            "id=" + getId() +
            ", moTa='" + getMoTa() + "'" +
            ", ten='" + getTen() + "'" +
            ", loaiBenhLyId=" + getLoaiBenhLyId() +
            "}";
    }
}

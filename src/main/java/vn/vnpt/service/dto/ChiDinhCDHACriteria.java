package vn.vnpt.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.BigDecimalFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link vn.vnpt.domain.ChiDinhCDHA} entity. This class is used
 * in {@link vn.vnpt.web.rest.ChiDinhCDHAResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /chi-dinh-cdhas?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ChiDinhCDHACriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private BooleanFilter coBaoHiem;

    private BooleanFilter coKetQua;

    private IntegerFilter daThanhToan;

    private IntegerFilter daThanhToanChenhLech;

    private IntegerFilter daThucHien;

    private BigDecimalFilter donGia;

    private BigDecimalFilter donGiaBhyt;

    private BigDecimalFilter donGiaKhongBhyt;

    private StringFilter ghiChuChiDinh;

    private StringFilter moTa;

    private StringFilter moTaXm15;

    private LongFilter nguoiChiDinhId;

    private StringFilter nguoiChiDinh;

    private IntegerFilter soLanChup;

    private BigDecimalFilter soLuong;

    private BigDecimalFilter soLuongCoFilm;

    private BigDecimalFilter soLuongCoFilm1318;

    private BigDecimalFilter soLuongCoFilm1820;

    private BigDecimalFilter soLuongCoFilm2025;

    private BigDecimalFilter soLuongCoFilm2430;

    private BigDecimalFilter soLuongCoFilm2530;

    private BigDecimalFilter soLuongCoFilm3040;

    private BigDecimalFilter soLuongFilm;

    private StringFilter ten;

    private BigDecimalFilter thanhTien;

    private BigDecimalFilter thanhTienBhyt;

    private BigDecimalFilter thanhTienKhongBHYT;

    private LocalDateFilter thoiGianChiDinh;

    private LocalDateFilter thoiGianTao;

    private BigDecimalFilter tienNgoaiBHYT;

    private BooleanFilter thanhToanChenhLech;

    private IntegerFilter tyLeThanhToan;

    private StringFilter maDungChung;

    private BooleanFilter dichVuYeuCau;

    private IntegerFilter nam;

    private LongFilter donViId;

    private LongFilter benhNhanId;

    private LongFilter bakbId;

    private LongFilter phieuCDId;

    private LongFilter phongId;

    private LongFilter cdhaId;

    public ChiDinhCDHACriteria() {
    }

    public ChiDinhCDHACriteria(ChiDinhCDHACriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.coBaoHiem = other.coBaoHiem == null ? null : other.coBaoHiem.copy();
        this.coKetQua = other.coKetQua == null ? null : other.coKetQua.copy();
        this.daThanhToan = other.daThanhToan == null ? null : other.daThanhToan.copy();
        this.daThanhToanChenhLech = other.daThanhToanChenhLech == null ? null : other.daThanhToanChenhLech.copy();
        this.daThucHien = other.daThucHien == null ? null : other.daThucHien.copy();
        this.donGia = other.donGia == null ? null : other.donGia.copy();
        this.donGiaBhyt = other.donGiaBhyt == null ? null : other.donGiaBhyt.copy();
        this.donGiaKhongBhyt = other.donGiaKhongBhyt == null ? null : other.donGiaKhongBhyt.copy();
        this.ghiChuChiDinh = other.ghiChuChiDinh == null ? null : other.ghiChuChiDinh.copy();
        this.moTa = other.moTa == null ? null : other.moTa.copy();
        this.moTaXm15 = other.moTaXm15 == null ? null : other.moTaXm15.copy();
        this.nguoiChiDinhId = other.nguoiChiDinhId == null ? null : other.nguoiChiDinhId.copy();
        this.nguoiChiDinh = other.nguoiChiDinh == null ? null : other.nguoiChiDinh.copy();
        this.soLanChup = other.soLanChup == null ? null : other.soLanChup.copy();
        this.soLuong = other.soLuong == null ? null : other.soLuong.copy();
        this.soLuongCoFilm = other.soLuongCoFilm == null ? null : other.soLuongCoFilm.copy();
        this.soLuongCoFilm1318 = other.soLuongCoFilm1318 == null ? null : other.soLuongCoFilm1318.copy();
        this.soLuongCoFilm1820 = other.soLuongCoFilm1820 == null ? null : other.soLuongCoFilm1820.copy();
        this.soLuongCoFilm2025 = other.soLuongCoFilm2025 == null ? null : other.soLuongCoFilm2025.copy();
        this.soLuongCoFilm2430 = other.soLuongCoFilm2430 == null ? null : other.soLuongCoFilm2430.copy();
        this.soLuongCoFilm2530 = other.soLuongCoFilm2530 == null ? null : other.soLuongCoFilm2530.copy();
        this.soLuongCoFilm3040 = other.soLuongCoFilm3040 == null ? null : other.soLuongCoFilm3040.copy();
        this.soLuongFilm = other.soLuongFilm == null ? null : other.soLuongFilm.copy();
        this.ten = other.ten == null ? null : other.ten.copy();
        this.thanhTien = other.thanhTien == null ? null : other.thanhTien.copy();
        this.thanhTienBhyt = other.thanhTienBhyt == null ? null : other.thanhTienBhyt.copy();
        this.thanhTienKhongBHYT = other.thanhTienKhongBHYT == null ? null : other.thanhTienKhongBHYT.copy();
        this.thoiGianChiDinh = other.thoiGianChiDinh == null ? null : other.thoiGianChiDinh.copy();
        this.thoiGianTao = other.thoiGianTao == null ? null : other.thoiGianTao.copy();
        this.tienNgoaiBHYT = other.tienNgoaiBHYT == null ? null : other.tienNgoaiBHYT.copy();
        this.thanhToanChenhLech = other.thanhToanChenhLech == null ? null : other.thanhToanChenhLech.copy();
        this.tyLeThanhToan = other.tyLeThanhToan == null ? null : other.tyLeThanhToan.copy();
        this.maDungChung = other.maDungChung == null ? null : other.maDungChung.copy();
        this.dichVuYeuCau = other.dichVuYeuCau == null ? null : other.dichVuYeuCau.copy();
        this.nam = other.nam == null ? null : other.nam.copy();
        this.donViId = other.donViId == null ? null : other.donViId.copy();
        this.benhNhanId = other.benhNhanId == null ? null : other.benhNhanId.copy();
        this.bakbId = other.bakbId == null ? null : other.bakbId.copy();
        this.phieuCDId = other.phieuCDId == null ? null : other.phieuCDId.copy();
        this.phongId = other.phongId == null ? null : other.phongId.copy();
        this.cdhaId = other.cdhaId == null ? null : other.cdhaId.copy();
    }

    @Override
    public ChiDinhCDHACriteria copy() {
        return new ChiDinhCDHACriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public BooleanFilter getCoBaoHiem() {
        return coBaoHiem;
    }

    public void setCoBaoHiem(BooleanFilter coBaoHiem) {
        this.coBaoHiem = coBaoHiem;
    }

    public BooleanFilter getCoKetQua() {
        return coKetQua;
    }

    public void setCoKetQua(BooleanFilter coKetQua) {
        this.coKetQua = coKetQua;
    }

    public IntegerFilter getDaThanhToan() {
        return daThanhToan;
    }

    public void setDaThanhToan(IntegerFilter daThanhToan) {
        this.daThanhToan = daThanhToan;
    }

    public IntegerFilter getDaThanhToanChenhLech() {
        return daThanhToanChenhLech;
    }

    public void setDaThanhToanChenhLech(IntegerFilter daThanhToanChenhLech) {
        this.daThanhToanChenhLech = daThanhToanChenhLech;
    }

    public IntegerFilter getDaThucHien() {
        return daThucHien;
    }

    public void setDaThucHien(IntegerFilter daThucHien) {
        this.daThucHien = daThucHien;
    }

    public BigDecimalFilter getDonGia() {
        return donGia;
    }

    public void setDonGia(BigDecimalFilter donGia) {
        this.donGia = donGia;
    }

    public BigDecimalFilter getDonGiaBhyt() {
        return donGiaBhyt;
    }

    public void setDonGiaBhyt(BigDecimalFilter donGiaBhyt) {
        this.donGiaBhyt = donGiaBhyt;
    }

    public BigDecimalFilter getDonGiaKhongBhyt() {
        return donGiaKhongBhyt;
    }

    public void setDonGiaKhongBhyt(BigDecimalFilter donGiaKhongBhyt) {
        this.donGiaKhongBhyt = donGiaKhongBhyt;
    }

    public StringFilter getGhiChuChiDinh() {
        return ghiChuChiDinh;
    }

    public void setGhiChuChiDinh(StringFilter ghiChuChiDinh) {
        this.ghiChuChiDinh = ghiChuChiDinh;
    }

    public StringFilter getMoTa() {
        return moTa;
    }

    public void setMoTa(StringFilter moTa) {
        this.moTa = moTa;
    }

    public StringFilter getMoTaXm15() {
        return moTaXm15;
    }

    public void setMoTaXm15(StringFilter moTaXm15) {
        this.moTaXm15 = moTaXm15;
    }

    public LongFilter getNguoiChiDinhId() {
        return nguoiChiDinhId;
    }

    public void setNguoiChiDinhId(LongFilter nguoiChiDinhId) {
        this.nguoiChiDinhId = nguoiChiDinhId;
    }

    public StringFilter getNguoiChiDinh() {
        return nguoiChiDinh;
    }

    public void setNguoiChiDinh(StringFilter nguoiChiDinh) {
        this.nguoiChiDinh = nguoiChiDinh;
    }

    public IntegerFilter getSoLanChup() {
        return soLanChup;
    }

    public void setSoLanChup(IntegerFilter soLanChup) {
        this.soLanChup = soLanChup;
    }

    public BigDecimalFilter getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(BigDecimalFilter soLuong) {
        this.soLuong = soLuong;
    }

    public BigDecimalFilter getSoLuongCoFilm() {
        return soLuongCoFilm;
    }

    public void setSoLuongCoFilm(BigDecimalFilter soLuongCoFilm) {
        this.soLuongCoFilm = soLuongCoFilm;
    }

    public BigDecimalFilter getSoLuongCoFilm1318() {
        return soLuongCoFilm1318;
    }

    public void setSoLuongCoFilm1318(BigDecimalFilter soLuongCoFilm1318) {
        this.soLuongCoFilm1318 = soLuongCoFilm1318;
    }

    public BigDecimalFilter getSoLuongCoFilm1820() {
        return soLuongCoFilm1820;
    }

    public void setSoLuongCoFilm1820(BigDecimalFilter soLuongCoFilm1820) {
        this.soLuongCoFilm1820 = soLuongCoFilm1820;
    }

    public BigDecimalFilter getSoLuongCoFilm2025() {
        return soLuongCoFilm2025;
    }

    public void setSoLuongCoFilm2025(BigDecimalFilter soLuongCoFilm2025) {
        this.soLuongCoFilm2025 = soLuongCoFilm2025;
    }

    public BigDecimalFilter getSoLuongCoFilm2430() {
        return soLuongCoFilm2430;
    }

    public void setSoLuongCoFilm2430(BigDecimalFilter soLuongCoFilm2430) {
        this.soLuongCoFilm2430 = soLuongCoFilm2430;
    }

    public BigDecimalFilter getSoLuongCoFilm2530() {
        return soLuongCoFilm2530;
    }

    public void setSoLuongCoFilm2530(BigDecimalFilter soLuongCoFilm2530) {
        this.soLuongCoFilm2530 = soLuongCoFilm2530;
    }

    public BigDecimalFilter getSoLuongCoFilm3040() {
        return soLuongCoFilm3040;
    }

    public void setSoLuongCoFilm3040(BigDecimalFilter soLuongCoFilm3040) {
        this.soLuongCoFilm3040 = soLuongCoFilm3040;
    }

    public BigDecimalFilter getSoLuongFilm() {
        return soLuongFilm;
    }

    public void setSoLuongFilm(BigDecimalFilter soLuongFilm) {
        this.soLuongFilm = soLuongFilm;
    }

    public StringFilter getTen() {
        return ten;
    }

    public void setTen(StringFilter ten) {
        this.ten = ten;
    }

    public BigDecimalFilter getThanhTien() {
        return thanhTien;
    }

    public void setThanhTien(BigDecimalFilter thanhTien) {
        this.thanhTien = thanhTien;
    }

    public BigDecimalFilter getThanhTienBhyt() {
        return thanhTienBhyt;
    }

    public void setThanhTienBhyt(BigDecimalFilter thanhTienBhyt) {
        this.thanhTienBhyt = thanhTienBhyt;
    }

    public BigDecimalFilter getThanhTienKhongBHYT() {
        return thanhTienKhongBHYT;
    }

    public void setThanhTienKhongBHYT(BigDecimalFilter thanhTienKhongBHYT) {
        this.thanhTienKhongBHYT = thanhTienKhongBHYT;
    }

    public LocalDateFilter getThoiGianChiDinh() {
        return thoiGianChiDinh;
    }

    public void setThoiGianChiDinh(LocalDateFilter thoiGianChiDinh) {
        this.thoiGianChiDinh = thoiGianChiDinh;
    }

    public LocalDateFilter getThoiGianTao() {
        return thoiGianTao;
    }

    public void setThoiGianTao(LocalDateFilter thoiGianTao) {
        this.thoiGianTao = thoiGianTao;
    }

    public BigDecimalFilter getTienNgoaiBHYT() {
        return tienNgoaiBHYT;
    }

    public void setTienNgoaiBHYT(BigDecimalFilter tienNgoaiBHYT) {
        this.tienNgoaiBHYT = tienNgoaiBHYT;
    }

    public BooleanFilter getThanhToanChenhLech() {
        return thanhToanChenhLech;
    }

    public void setThanhToanChenhLech(BooleanFilter thanhToanChenhLech) {
        this.thanhToanChenhLech = thanhToanChenhLech;
    }

    public IntegerFilter getTyLeThanhToan() {
        return tyLeThanhToan;
    }

    public void setTyLeThanhToan(IntegerFilter tyLeThanhToan) {
        this.tyLeThanhToan = tyLeThanhToan;
    }

    public StringFilter getMaDungChung() {
        return maDungChung;
    }

    public void setMaDungChung(StringFilter maDungChung) {
        this.maDungChung = maDungChung;
    }

    public BooleanFilter getDichVuYeuCau() {
        return dichVuYeuCau;
    }

    public void setDichVuYeuCau(BooleanFilter dichVuYeuCau) {
        this.dichVuYeuCau = dichVuYeuCau;
    }

    public IntegerFilter getNam() {
        return nam;
    }

    public void setNam(IntegerFilter nam) {
        this.nam = nam;
    }

    public LongFilter getDonViId() {
        return donViId;
    }

    public void setDonViId(LongFilter donViId) {
        this.donViId = donViId;
    }

    public LongFilter getBenhNhanId() {
        return benhNhanId;
    }

    public void setBenhNhanId(LongFilter benhNhanId) {
        this.benhNhanId = benhNhanId;
    }

    public LongFilter getBakbId() {
        return bakbId;
    }

    public void setBakbId(LongFilter bakbId) {
        this.bakbId = bakbId;
    }

    public LongFilter getPhieuCDId() {
        return phieuCDId;
    }

    public void setPhieuCDId(LongFilter phieuCDId) {
        this.phieuCDId = phieuCDId;
    }

    public LongFilter getPhongId() {
        return phongId;
    }

    public void setPhongId(LongFilter phongId) {
        this.phongId = phongId;
    }

    public LongFilter getCdhaId() {
        return cdhaId;
    }

    public void setCdhaId(LongFilter cdhaId) {
        this.cdhaId = cdhaId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ChiDinhCDHACriteria that = (ChiDinhCDHACriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(coBaoHiem, that.coBaoHiem) &&
            Objects.equals(coKetQua, that.coKetQua) &&
            Objects.equals(daThanhToan, that.daThanhToan) &&
            Objects.equals(daThanhToanChenhLech, that.daThanhToanChenhLech) &&
            Objects.equals(daThucHien, that.daThucHien) &&
            Objects.equals(donGia, that.donGia) &&
            Objects.equals(donGiaBhyt, that.donGiaBhyt) &&
            Objects.equals(donGiaKhongBhyt, that.donGiaKhongBhyt) &&
            Objects.equals(ghiChuChiDinh, that.ghiChuChiDinh) &&
            Objects.equals(moTa, that.moTa) &&
            Objects.equals(moTaXm15, that.moTaXm15) &&
            Objects.equals(nguoiChiDinhId, that.nguoiChiDinhId) &&
            Objects.equals(nguoiChiDinh, that.nguoiChiDinh) &&
            Objects.equals(soLanChup, that.soLanChup) &&
            Objects.equals(soLuong, that.soLuong) &&
            Objects.equals(soLuongCoFilm, that.soLuongCoFilm) &&
            Objects.equals(soLuongCoFilm1318, that.soLuongCoFilm1318) &&
            Objects.equals(soLuongCoFilm1820, that.soLuongCoFilm1820) &&
            Objects.equals(soLuongCoFilm2025, that.soLuongCoFilm2025) &&
            Objects.equals(soLuongCoFilm2430, that.soLuongCoFilm2430) &&
            Objects.equals(soLuongCoFilm2530, that.soLuongCoFilm2530) &&
            Objects.equals(soLuongCoFilm3040, that.soLuongCoFilm3040) &&
            Objects.equals(soLuongFilm, that.soLuongFilm) &&
            Objects.equals(ten, that.ten) &&
            Objects.equals(thanhTien, that.thanhTien) &&
            Objects.equals(thanhTienBhyt, that.thanhTienBhyt) &&
            Objects.equals(thanhTienKhongBHYT, that.thanhTienKhongBHYT) &&
            Objects.equals(thoiGianChiDinh, that.thoiGianChiDinh) &&
            Objects.equals(thoiGianTao, that.thoiGianTao) &&
            Objects.equals(tienNgoaiBHYT, that.tienNgoaiBHYT) &&
            Objects.equals(thanhToanChenhLech, that.thanhToanChenhLech) &&
            Objects.equals(tyLeThanhToan, that.tyLeThanhToan) &&
            Objects.equals(maDungChung, that.maDungChung) &&
            Objects.equals(dichVuYeuCau, that.dichVuYeuCau) &&
            Objects.equals(nam, that.nam) &&
            Objects.equals(donViId, that.donViId) &&
            Objects.equals(benhNhanId, that.benhNhanId) &&
            Objects.equals(bakbId, that.bakbId) &&
            Objects.equals(phieuCDId, that.phieuCDId) &&
            Objects.equals(phongId, that.phongId) &&
            Objects.equals(cdhaId, that.cdhaId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        coBaoHiem,
        coKetQua,
        daThanhToan,
        daThanhToanChenhLech,
        daThucHien,
        donGia,
        donGiaBhyt,
        donGiaKhongBhyt,
        ghiChuChiDinh,
        moTa,
        moTaXm15,
        nguoiChiDinhId,
        nguoiChiDinh,
        soLanChup,
        soLuong,
        soLuongCoFilm,
        soLuongCoFilm1318,
        soLuongCoFilm1820,
        soLuongCoFilm2025,
        soLuongCoFilm2430,
        soLuongCoFilm2530,
        soLuongCoFilm3040,
        soLuongFilm,
        ten,
        thanhTien,
        thanhTienBhyt,
        thanhTienKhongBHYT,
        thoiGianChiDinh,
        thoiGianTao,
        tienNgoaiBHYT,
        thanhToanChenhLech,
        tyLeThanhToan,
        maDungChung,
        dichVuYeuCau,
        nam,
        donViId,
        benhNhanId,
        bakbId,
        phieuCDId,
        phongId,
        cdhaId
        );
    }

    @Override
    public String toString() {
        return "ChiDinhCDHACriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (coBaoHiem != null ? "coBaoHiem=" + coBaoHiem + ", " : "") +
                (coKetQua != null ? "coKetQua=" + coKetQua + ", " : "") +
                (daThanhToan != null ? "daThanhToan=" + daThanhToan + ", " : "") +
                (daThanhToanChenhLech != null ? "daThanhToanChenhLech=" + daThanhToanChenhLech + ", " : "") +
                (daThucHien != null ? "daThucHien=" + daThucHien + ", " : "") +
                (donGia != null ? "donGia=" + donGia + ", " : "") +
                (donGiaBhyt != null ? "donGiaBhyt=" + donGiaBhyt + ", " : "") +
                (donGiaKhongBhyt != null ? "donGiaKhongBhyt=" + donGiaKhongBhyt + ", " : "") +
                (ghiChuChiDinh != null ? "ghiChuChiDinh=" + ghiChuChiDinh + ", " : "") +
                (moTa != null ? "moTa=" + moTa + ", " : "") +
                (moTaXm15 != null ? "moTaXm15=" + moTaXm15 + ", " : "") +
                (nguoiChiDinhId != null ? "nguoiChiDinhId=" + nguoiChiDinhId + ", " : "") +
                (nguoiChiDinh != null ? "nguoiChiDinh=" + nguoiChiDinh + ", " : "") +
                (soLanChup != null ? "soLanChup=" + soLanChup + ", " : "") +
                (soLuong != null ? "soLuong=" + soLuong + ", " : "") +
                (soLuongCoFilm != null ? "soLuongCoFilm=" + soLuongCoFilm + ", " : "") +
                (soLuongCoFilm1318 != null ? "soLuongCoFilm1318=" + soLuongCoFilm1318 + ", " : "") +
                (soLuongCoFilm1820 != null ? "soLuongCoFilm1820=" + soLuongCoFilm1820 + ", " : "") +
                (soLuongCoFilm2025 != null ? "soLuongCoFilm2025=" + soLuongCoFilm2025 + ", " : "") +
                (soLuongCoFilm2430 != null ? "soLuongCoFilm2430=" + soLuongCoFilm2430 + ", " : "") +
                (soLuongCoFilm2530 != null ? "soLuongCoFilm2530=" + soLuongCoFilm2530 + ", " : "") +
                (soLuongCoFilm3040 != null ? "soLuongCoFilm3040=" + soLuongCoFilm3040 + ", " : "") +
                (soLuongFilm != null ? "soLuongFilm=" + soLuongFilm + ", " : "") +
                (ten != null ? "ten=" + ten + ", " : "") +
                (thanhTien != null ? "thanhTien=" + thanhTien + ", " : "") +
                (thanhTienBhyt != null ? "thanhTienBhyt=" + thanhTienBhyt + ", " : "") +
                (thanhTienKhongBHYT != null ? "thanhTienKhongBHYT=" + thanhTienKhongBHYT + ", " : "") +
                (thoiGianChiDinh != null ? "thoiGianChiDinh=" + thoiGianChiDinh + ", " : "") +
                (thoiGianTao != null ? "thoiGianTao=" + thoiGianTao + ", " : "") +
                (tienNgoaiBHYT != null ? "tienNgoaiBHYT=" + tienNgoaiBHYT + ", " : "") +
                (thanhToanChenhLech != null ? "thanhToanChenhLech=" + thanhToanChenhLech + ", " : "") +
                (tyLeThanhToan != null ? "tyLeThanhToan=" + tyLeThanhToan + ", " : "") +
                (maDungChung != null ? "maDungChung=" + maDungChung + ", " : "") +
                (dichVuYeuCau != null ? "dichVuYeuCau=" + dichVuYeuCau + ", " : "") +
                (nam != null ? "nam=" + nam + ", " : "") +
                (donViId != null ? "donViId=" + donViId + ", " : "") +
                (benhNhanId != null ? "benhNhanId=" + benhNhanId + ", " : "") +
                (bakbId != null ? "bakbId=" + bakbId + ", " : "") +
                (phieuCDId != null ? "phieuCDId=" + phieuCDId + ", " : "") +
                (phongId != null ? "phongId=" + phongId + ", " : "") +
                (cdhaId != null ? "cdhaId=" + cdhaId + ", " : "") +
            "}";
    }

}

package vn.vnpt.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link vn.vnpt.domain.Menu} entity. This class is used
 * in {@link vn.vnpt.web.rest.MenuResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /menus?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class MenuCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private BooleanFilter enabled;

    private IntegerFilter level;

    private StringFilter name;

    private StringFilter uri;

    private IntegerFilter parent;

    private LongFilter donViId;

    private LongFilter userId;

    public MenuCriteria() {
    }

    public MenuCriteria(MenuCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.enabled = other.enabled == null ? null : other.enabled.copy();
        this.level = other.level == null ? null : other.level.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.uri = other.uri == null ? null : other.uri.copy();
        this.parent = other.parent == null ? null : other.parent.copy();
        this.donViId = other.donViId == null ? null : other.donViId.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
    }

    @Override
    public MenuCriteria copy() {
        return new MenuCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public BooleanFilter getEnabled() {
        return enabled;
    }

    public void setEnabled(BooleanFilter enabled) {
        this.enabled = enabled;
    }

    public IntegerFilter getLevel() {
        return level;
    }

    public void setLevel(IntegerFilter level) {
        this.level = level;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getUri() {
        return uri;
    }

    public void setUri(StringFilter uri) {
        this.uri = uri;
    }

    public IntegerFilter getParent() {
        return parent;
    }

    public void setParent(IntegerFilter parent) {
        this.parent = parent;
    }

    public LongFilter getDonViId() {
        return donViId;
    }

    public void setDonViId(LongFilter donViId) {
        this.donViId = donViId;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final MenuCriteria that = (MenuCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(enabled, that.enabled) &&
            Objects.equals(level, that.level) &&
            Objects.equals(name, that.name) &&
            Objects.equals(uri, that.uri) &&
            Objects.equals(parent, that.parent) &&
            Objects.equals(donViId, that.donViId) &&
            Objects.equals(userId, that.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        enabled,
        level,
        name,
        uri,
        parent,
        donViId,
        userId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MenuCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (enabled != null ? "enabled=" + enabled + ", " : "") +
                (level != null ? "level=" + level + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (uri != null ? "uri=" + uri + ", " : "") +
                (parent != null ? "parent=" + parent + ", " : "") +
                (donViId != null ? "donViId=" + donViId + ", " : "") +
                (userId != null ? "userId=" + userId + ", " : "") +
            "}";
    }

}

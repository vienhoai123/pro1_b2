package vn.vnpt.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link vn.vnpt.domain.NhomDoiTuongBhyt} entity. This class is used
 * in {@link vn.vnpt.web.rest.NhomDoiTuongBhytResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /nhom-doi-tuong-bhyts?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class NhomDoiTuongBhytCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter ten;

    public NhomDoiTuongBhytCriteria() {
    }

    public NhomDoiTuongBhytCriteria(NhomDoiTuongBhytCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.ten = other.ten == null ? null : other.ten.copy();
    }

    @Override
    public NhomDoiTuongBhytCriteria copy() {
        return new NhomDoiTuongBhytCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getTen() {
        return ten;
    }

    public void setTen(StringFilter ten) {
        this.ten = ten;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final NhomDoiTuongBhytCriteria that = (NhomDoiTuongBhytCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(ten, that.ten);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        ten
        );
    }

    @Override
    public String toString() {
        return "NhomDoiTuongBhytCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (ten != null ? "ten=" + ten + ", " : "") +
            "}";
    }

}

package vn.vnpt.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link vn.vnpt.domain.Country} entity.
 */
@ApiModel(description = "NDB_TABLE=READ_BACKUP=1 Thông tin các nước trên thế giới")
public class CountryDTO implements Serializable {
    
    private Long id;

    /**
     * Mã 2 ký tự của quốc gia theo ISO
     */
    @Size(max = 255)
    @ApiModelProperty(value = "Mã 2 ký tự của quốc gia theo ISO")
    private String isoCode;

    /**
     * Mã theo số của quốc gia theo ISO
     */
    @Size(max = 255)
    @ApiModelProperty(value = "Mã theo số của quốc gia theo ISO")
    private String isoNumeric;

    /**
     * Tên quốc gia
     */
    @Size(max = 255)
    @ApiModelProperty(value = "Tên quốc gia")
    private String name;

    /**
     * Mã cục thống kê
     */
    @ApiModelProperty(value = "Mã cục thống kê")
    private Integer maCtk;

    /**
     * Tên cục thống kê
     */
    @Size(max = 255)
    @ApiModelProperty(value = "Tên cục thống kê")
    private String tenCtk;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIsoCode() {
        return isoCode;
    }

    public void setIsoCode(String isoCode) {
        this.isoCode = isoCode;
    }

    public String getIsoNumeric() {
        return isoNumeric;
    }

    public void setIsoNumeric(String isoNumeric) {
        this.isoNumeric = isoNumeric;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getMaCtk() {
        return maCtk;
    }

    public void setMaCtk(Integer maCtk) {
        this.maCtk = maCtk;
    }

    public String getTenCtk() {
        return tenCtk;
    }

    public void setTenCtk(String tenCtk) {
        this.tenCtk = tenCtk;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CountryDTO countryDTO = (CountryDTO) o;
        if (countryDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), countryDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CountryDTO{" +
            "id=" + getId() +
            ", isoCode='" + getIsoCode() + "'" +
            ", isoNumeric='" + getIsoNumeric() + "'" +
            ", name='" + getName() + "'" +
            ", maCtk=" + getMaCtk() +
            ", tenCtk='" + getTenCtk() + "'" +
            "}";
    }
}

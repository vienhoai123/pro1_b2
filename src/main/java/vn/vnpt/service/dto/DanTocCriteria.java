package vn.vnpt.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link vn.vnpt.domain.DanToc} entity. This class is used
 * in {@link vn.vnpt.web.rest.DanTocResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /dan-tocs?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class DanTocCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter ma4069Byt;

    private IntegerFilter maCucThongKe;

    private StringFilter ten4069Byt;

    private StringFilter tenCucThongKe;

    public DanTocCriteria() {
    }

    public DanTocCriteria(DanTocCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.ma4069Byt = other.ma4069Byt == null ? null : other.ma4069Byt.copy();
        this.maCucThongKe = other.maCucThongKe == null ? null : other.maCucThongKe.copy();
        this.ten4069Byt = other.ten4069Byt == null ? null : other.ten4069Byt.copy();
        this.tenCucThongKe = other.tenCucThongKe == null ? null : other.tenCucThongKe.copy();
    }

    @Override
    public DanTocCriteria copy() {
        return new DanTocCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getMa4069Byt() {
        return ma4069Byt;
    }

    public void setMa4069Byt(IntegerFilter ma4069Byt) {
        this.ma4069Byt = ma4069Byt;
    }

    public IntegerFilter getMaCucThongKe() {
        return maCucThongKe;
    }

    public void setMaCucThongKe(IntegerFilter maCucThongKe) {
        this.maCucThongKe = maCucThongKe;
    }

    public StringFilter getTen4069Byt() {
        return ten4069Byt;
    }

    public void setTen4069Byt(StringFilter ten4069Byt) {
        this.ten4069Byt = ten4069Byt;
    }

    public StringFilter getTenCucThongKe() {
        return tenCucThongKe;
    }

    public void setTenCucThongKe(StringFilter tenCucThongKe) {
        this.tenCucThongKe = tenCucThongKe;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final DanTocCriteria that = (DanTocCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(ma4069Byt, that.ma4069Byt) &&
            Objects.equals(maCucThongKe, that.maCucThongKe) &&
            Objects.equals(ten4069Byt, that.ten4069Byt) &&
            Objects.equals(tenCucThongKe, that.tenCucThongKe);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        ma4069Byt,
        maCucThongKe,
        ten4069Byt,
        tenCucThongKe
        );
    }

    @Override
    public String toString() {
        return "DanTocCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (ma4069Byt != null ? "ma4069Byt=" + ma4069Byt + ", " : "") +
                (maCucThongKe != null ? "maCucThongKe=" + maCucThongKe + ", " : "") +
                (ten4069Byt != null ? "ten4069Byt=" + ten4069Byt + ", " : "") +
                (tenCucThongKe != null ? "tenCucThongKe=" + tenCucThongKe + ", " : "") +
            "}";
    }

}

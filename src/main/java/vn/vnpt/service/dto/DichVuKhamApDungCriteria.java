package vn.vnpt.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.BigDecimalFilter;

/**
 * Criteria class for the {@link vn.vnpt.domain.DichVuKhamApDung} entity. This class is used
 * in {@link vn.vnpt.web.rest.DichVuKhamApDungResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /dich-vu-kham-ap-dungs?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class DichVuKhamApDungCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter enabled;

    private BigDecimalFilter giaBhyt;

    private BigDecimalFilter giaKhongBhyt;

    private StringFilter maBaoCaoBhyt;

    private StringFilter maBaoCaoByt;

    private StringFilter soCongVanBhxh;

    private StringFilter soQuyetDinh;

    private StringFilter tenBaoCaoBhxh;

    private StringFilter tenDichVuKhongBaoHiem;

    private BigDecimalFilter tienBenhNhanChi;

    private BigDecimalFilter tienBhxhChi;

    private BigDecimalFilter tienNgoaiBhyt;

    private BigDecimalFilter tongTienThanhToan;

    private IntegerFilter tyLeBhxhThanhToan;

    private IntegerFilter doiTuongDacBiet;

    private IntegerFilter nguonChi;

    private LongFilter dotGiaDichVuBhxhId;

    private LongFilter dichVuKhamId;

    public DichVuKhamApDungCriteria() {
    }

    public DichVuKhamApDungCriteria(DichVuKhamApDungCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.enabled = other.enabled == null ? null : other.enabled.copy();
        this.giaBhyt = other.giaBhyt == null ? null : other.giaBhyt.copy();
        this.giaKhongBhyt = other.giaKhongBhyt == null ? null : other.giaKhongBhyt.copy();
        this.maBaoCaoBhyt = other.maBaoCaoBhyt == null ? null : other.maBaoCaoBhyt.copy();
        this.maBaoCaoByt = other.maBaoCaoByt == null ? null : other.maBaoCaoByt.copy();
        this.soCongVanBhxh = other.soCongVanBhxh == null ? null : other.soCongVanBhxh.copy();
        this.soQuyetDinh = other.soQuyetDinh == null ? null : other.soQuyetDinh.copy();
        this.tenBaoCaoBhxh = other.tenBaoCaoBhxh == null ? null : other.tenBaoCaoBhxh.copy();
        this.tenDichVuKhongBaoHiem = other.tenDichVuKhongBaoHiem == null ? null : other.tenDichVuKhongBaoHiem.copy();
        this.tienBenhNhanChi = other.tienBenhNhanChi == null ? null : other.tienBenhNhanChi.copy();
        this.tienBhxhChi = other.tienBhxhChi == null ? null : other.tienBhxhChi.copy();
        this.tienNgoaiBhyt = other.tienNgoaiBhyt == null ? null : other.tienNgoaiBhyt.copy();
        this.tongTienThanhToan = other.tongTienThanhToan == null ? null : other.tongTienThanhToan.copy();
        this.tyLeBhxhThanhToan = other.tyLeBhxhThanhToan == null ? null : other.tyLeBhxhThanhToan.copy();
        this.doiTuongDacBiet = other.doiTuongDacBiet == null ? null : other.doiTuongDacBiet.copy();
        this.nguonChi = other.nguonChi == null ? null : other.nguonChi.copy();
        this.dotGiaDichVuBhxhId = other.dotGiaDichVuBhxhId == null ? null : other.dotGiaDichVuBhxhId.copy();
        this.dichVuKhamId = other.dichVuKhamId == null ? null : other.dichVuKhamId.copy();
    }

    @Override
    public DichVuKhamApDungCriteria copy() {
        return new DichVuKhamApDungCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getEnabled() {
        return enabled;
    }

    public void setEnabled(IntegerFilter enabled) {
        this.enabled = enabled;
    }

    public BigDecimalFilter getGiaBhyt() {
        return giaBhyt;
    }

    public void setGiaBhyt(BigDecimalFilter giaBhyt) {
        this.giaBhyt = giaBhyt;
    }

    public BigDecimalFilter getGiaKhongBhyt() {
        return giaKhongBhyt;
    }

    public void setGiaKhongBhyt(BigDecimalFilter giaKhongBhyt) {
        this.giaKhongBhyt = giaKhongBhyt;
    }

    public StringFilter getMaBaoCaoBhyt() {
        return maBaoCaoBhyt;
    }

    public void setMaBaoCaoBhyt(StringFilter maBaoCaoBhyt) {
        this.maBaoCaoBhyt = maBaoCaoBhyt;
    }

    public StringFilter getMaBaoCaoByt() {
        return maBaoCaoByt;
    }

    public void setMaBaoCaoByt(StringFilter maBaoCaoByt) {
        this.maBaoCaoByt = maBaoCaoByt;
    }

    public StringFilter getSoCongVanBhxh() {
        return soCongVanBhxh;
    }

    public void setSoCongVanBhxh(StringFilter soCongVanBhxh) {
        this.soCongVanBhxh = soCongVanBhxh;
    }

    public StringFilter getSoQuyetDinh() {
        return soQuyetDinh;
    }

    public void setSoQuyetDinh(StringFilter soQuyetDinh) {
        this.soQuyetDinh = soQuyetDinh;
    }

    public StringFilter getTenBaoCaoBhxh() {
        return tenBaoCaoBhxh;
    }

    public void setTenBaoCaoBhxh(StringFilter tenBaoCaoBhxh) {
        this.tenBaoCaoBhxh = tenBaoCaoBhxh;
    }

    public StringFilter getTenDichVuKhongBaoHiem() {
        return tenDichVuKhongBaoHiem;
    }

    public void setTenDichVuKhongBaoHiem(StringFilter tenDichVuKhongBaoHiem) {
        this.tenDichVuKhongBaoHiem = tenDichVuKhongBaoHiem;
    }

    public BigDecimalFilter getTienBenhNhanChi() {
        return tienBenhNhanChi;
    }

    public void setTienBenhNhanChi(BigDecimalFilter tienBenhNhanChi) {
        this.tienBenhNhanChi = tienBenhNhanChi;
    }

    public BigDecimalFilter getTienBhxhChi() {
        return tienBhxhChi;
    }

    public void setTienBhxhChi(BigDecimalFilter tienBhxhChi) {
        this.tienBhxhChi = tienBhxhChi;
    }

    public BigDecimalFilter getTienNgoaiBhyt() {
        return tienNgoaiBhyt;
    }

    public void setTienNgoaiBhyt(BigDecimalFilter tienNgoaiBhyt) {
        this.tienNgoaiBhyt = tienNgoaiBhyt;
    }

    public BigDecimalFilter getTongTienThanhToan() {
        return tongTienThanhToan;
    }

    public void setTongTienThanhToan(BigDecimalFilter tongTienThanhToan) {
        this.tongTienThanhToan = tongTienThanhToan;
    }

    public IntegerFilter getTyLeBhxhThanhToan() {
        return tyLeBhxhThanhToan;
    }

    public void setTyLeBhxhThanhToan(IntegerFilter tyLeBhxhThanhToan) {
        this.tyLeBhxhThanhToan = tyLeBhxhThanhToan;
    }

    public IntegerFilter getDoiTuongDacBiet() {
        return doiTuongDacBiet;
    }

    public void setDoiTuongDacBiet(IntegerFilter doiTuongDacBiet) {
        this.doiTuongDacBiet = doiTuongDacBiet;
    }

    public IntegerFilter getNguonChi() {
        return nguonChi;
    }

    public void setNguonChi(IntegerFilter nguonChi) {
        this.nguonChi = nguonChi;
    }

    public LongFilter getDotGiaDichVuBhxhId() {
        return dotGiaDichVuBhxhId;
    }

    public void setDotGiaDichVuBhxhId(LongFilter dotGiaDichVuBhxhId) {
        this.dotGiaDichVuBhxhId = dotGiaDichVuBhxhId;
    }

    public LongFilter getDichVuKhamId() {
        return dichVuKhamId;
    }

    public void setDichVuKhamId(LongFilter dichVuKhamId) {
        this.dichVuKhamId = dichVuKhamId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final DichVuKhamApDungCriteria that = (DichVuKhamApDungCriteria) o;
        return Objects.equals(id, that.id) &&
            Objects.equals(enabled, that.enabled) &&
            Objects.equals(giaBhyt, that.giaBhyt) &&
            Objects.equals(giaKhongBhyt, that.giaKhongBhyt) &&
            Objects.equals(maBaoCaoBhyt, that.maBaoCaoBhyt) &&
            Objects.equals(maBaoCaoByt, that.maBaoCaoByt) &&
            Objects.equals(soCongVanBhxh, that.soCongVanBhxh) &&
            Objects.equals(soQuyetDinh, that.soQuyetDinh) &&
            Objects.equals(tenBaoCaoBhxh, that.tenBaoCaoBhxh) &&
            Objects.equals(tenDichVuKhongBaoHiem, that.tenDichVuKhongBaoHiem) &&
            Objects.equals(tienBenhNhanChi, that.tienBenhNhanChi) &&
            Objects.equals(tienBhxhChi, that.tienBhxhChi) &&
            Objects.equals(tienNgoaiBhyt, that.tienNgoaiBhyt) &&
            Objects.equals(tongTienThanhToan, that.tongTienThanhToan) &&
            Objects.equals(tyLeBhxhThanhToan, that.tyLeBhxhThanhToan) &&
            Objects.equals(doiTuongDacBiet, that.doiTuongDacBiet) &&
            Objects.equals(nguonChi, that.nguonChi) &&
            Objects.equals(dotGiaDichVuBhxhId, that.dotGiaDichVuBhxhId) &&
            Objects.equals(dichVuKhamId, that.dichVuKhamId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            enabled,
            giaBhyt,
            giaKhongBhyt,
            maBaoCaoBhyt,
            maBaoCaoByt,
            soCongVanBhxh,
            soQuyetDinh,
            tenBaoCaoBhxh,
            tenDichVuKhongBaoHiem,
            tienBenhNhanChi,
            tienBhxhChi,
            tienNgoaiBhyt,
            tongTienThanhToan,
            tyLeBhxhThanhToan,
            doiTuongDacBiet,
            nguonChi,
            dotGiaDichVuBhxhId,
            dichVuKhamId
        );
    }

    @Override
    public String toString() {
        return "DichVuKhamApDungCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (enabled != null ? "enabled=" + enabled + ", " : "") +
            (giaBhyt != null ? "giaBhyt=" + giaBhyt + ", " : "") +
            (giaKhongBhyt != null ? "giaKhongBhyt=" + giaKhongBhyt + ", " : "") +
            (maBaoCaoBhyt != null ? "maBaoCaoBhyt=" + maBaoCaoBhyt + ", " : "") +
            (maBaoCaoByt != null ? "maBaoCaoByt=" + maBaoCaoByt + ", " : "") +
            (soCongVanBhxh != null ? "soCongVanBhxh=" + soCongVanBhxh + ", " : "") +
            (soQuyetDinh != null ? "soQuyetDinh=" + soQuyetDinh + ", " : "") +
            (tenBaoCaoBhxh != null ? "tenBaoCaoBhxh=" + tenBaoCaoBhxh + ", " : "") +
            (tenDichVuKhongBaoHiem != null ? "tenDichVuKhongBaoHiem=" + tenDichVuKhongBaoHiem + ", " : "") +
            (tienBenhNhanChi != null ? "tienBenhNhanChi=" + tienBenhNhanChi + ", " : "") +
            (tienBhxhChi != null ? "tienBhxhChi=" + tienBhxhChi + ", " : "") +
            (tienNgoaiBhyt != null ? "tienNgoaiBhyt=" + tienNgoaiBhyt + ", " : "") +
            (tongTienThanhToan != null ? "tongTienThanhToan=" + tongTienThanhToan + ", " : "") +
            (tyLeBhxhThanhToan != null ? "tyLeBhxhThanhToan=" + tyLeBhxhThanhToan + ", " : "") +
            (doiTuongDacBiet != null ? "doiTuongDacBiet=" + doiTuongDacBiet + ", " : "") +
            (nguonChi != null ? "nguonChi=" + nguonChi + ", " : "") +
            (dotGiaDichVuBhxhId != null ? "dotGiaDichVuBhxhId=" + dotGiaDichVuBhxhId + ", " : "") +
            (dichVuKhamId != null ? "dichVuKhamId=" + dichVuKhamId + ", " : "") +
            "}";
    }

}

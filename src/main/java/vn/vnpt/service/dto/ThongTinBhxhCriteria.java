package vn.vnpt.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link vn.vnpt.domain.ThongTinBhxh} entity. This class is used
 * in {@link vn.vnpt.web.rest.ThongTinBhxhResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /thong-tin-bhxhs?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ThongTinBhxhCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter dvtt;

    private BooleanFilter enabled;

    private LocalDateFilter ngayApDungBhyt;

    private StringFilter ten;

    private LongFilter donViId;

    public ThongTinBhxhCriteria() {
    }

    public ThongTinBhxhCriteria(ThongTinBhxhCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.dvtt = other.dvtt == null ? null : other.dvtt.copy();
        this.enabled = other.enabled == null ? null : other.enabled.copy();
        this.ngayApDungBhyt = other.ngayApDungBhyt == null ? null : other.ngayApDungBhyt.copy();
        this.ten = other.ten == null ? null : other.ten.copy();
        this.donViId = other.donViId == null ? null : other.donViId.copy();
    }

    @Override
    public ThongTinBhxhCriteria copy() {
        return new ThongTinBhxhCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getDvtt() {
        return dvtt;
    }

    public void setDvtt(StringFilter dvtt) {
        this.dvtt = dvtt;
    }

    public BooleanFilter getEnabled() {
        return enabled;
    }

    public void setEnabled(BooleanFilter enabled) {
        this.enabled = enabled;
    }

    public LocalDateFilter getNgayApDungBhyt() {
        return ngayApDungBhyt;
    }

    public void setNgayApDungBhyt(LocalDateFilter ngayApDungBhyt) {
        this.ngayApDungBhyt = ngayApDungBhyt;
    }

    public StringFilter getTen() {
        return ten;
    }

    public void setTen(StringFilter ten) {
        this.ten = ten;
    }

    public LongFilter getDonViId() {
        return donViId;
    }

    public void setDonViId(LongFilter donViId) {
        this.donViId = donViId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ThongTinBhxhCriteria that = (ThongTinBhxhCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(dvtt, that.dvtt) &&
            Objects.equals(enabled, that.enabled) &&
            Objects.equals(ngayApDungBhyt, that.ngayApDungBhyt) &&
            Objects.equals(ten, that.ten) &&
            Objects.equals(donViId, that.donViId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        dvtt,
        enabled,
        ngayApDungBhyt,
        ten,
        donViId
        );
    }

    @Override
    public String toString() {
        return "ThongTinBhxhCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (dvtt != null ? "dvtt=" + dvtt + ", " : "") +
                (enabled != null ? "enabled=" + enabled + ", " : "") +
                (ngayApDungBhyt != null ? "ngayApDungBhyt=" + ngayApDungBhyt + ", " : "") +
                (ten != null ? "ten=" + ten + ", " : "") +
                (donViId != null ? "donViId=" + donViId + ", " : "") +
            "}";
    }

}

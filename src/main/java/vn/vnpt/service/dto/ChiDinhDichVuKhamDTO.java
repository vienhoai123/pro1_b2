package vn.vnpt.service.dto;

import io.swagger.annotations.ApiModelProperty;
import vn.vnpt.domain.ThongTinKhamBenh;
import vn.vnpt.domain.ThongTinKhamBenhId;
import vn.vnpt.domain.ThongTinKhoaId;

import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A DTO for the {@link vn.vnpt.domain.ChiDinhDichVuKham} entity.
 */
public class ChiDinhDichVuKhamDTO implements Serializable {

    private Long id;

    /**
     * Trạng thái công khám ban đầu: 1: Công khám ban đầu. 0: Công khám bình thường
     */
    @NotNull
    @ApiModelProperty(value = "Trạng thái công khám ban đầu: 1: Công khám ban đầu. 0: Công khám bình thường", required = true)
    private Boolean congKhamBanDau;

    /**
     * Trạng thái đã thanh toán: 0: Chưa thanh toán. 1 Đã thanh toán
     */
    @NotNull
    @ApiModelProperty(value = "Trạng thái đã thanh toán: 0: Chưa thanh toán. 1 Đã thanh toán", required = true)
    private Boolean daThanhToan;

    /**
     * Đơn giá
     */
    @NotNull
    @ApiModelProperty(value = "Đơn giá", required = true)
    private BigDecimal donGia;

    /**
     * Tên dịch vụ khám
     */
    @NotNull
    @Size(max = 500)
    @ApiModelProperty(value = "Tên dịch vụ khám", required = true)
    private String ten;

    /**
     * Thời gian chỉ định dịch vụ
     */
    @NotNull
    @ApiModelProperty(value = "Thời gian chỉ định dịch vụ", required = true)
    private LocalDate thoiGianChiDinh;

    /**
     * Tỷ lệ thanh toán
     */
    @ApiModelProperty(value = "Tỷ lệ thanh toán")
    private BigDecimal tyLeThanhToan;

    /**
     * Trạng thái có bảo hiểm: 1: Có bảo hiểm. 0: Không có BHYT
     */
    @NotNull
    @ApiModelProperty(value = "Trạng thái có bảo hiểm: 1: Có bảo hiểm. 0: Không có BHYT", required = true)
    private Boolean coBaoHiem;

    /**
     * Giá BHYT
     */
    @NotNull
    @ApiModelProperty(value = "Giá BHYT", required = true)
    private BigDecimal giaBhyt;

    /**
     * Giá Không có BHYT
     */
    @NotNull
    @ApiModelProperty(value = "Giá Không có BHYT", required = true)
    private BigDecimal giaKhongBhyt;

    /**
     * Mã dùng chung cho chỉ định
     */
    @Size(max = 500)
    @ApiModelProperty(value = "Mã dùng chung cho chỉ định")
    private String maDungChung;

    /**
     * Trạng thái theo yêu cầu của dịch vụ: 0: Bình thường. 1: Theo yêu cầu
     */
    @NotNull
    @ApiModelProperty(value = "Trạng thái theo yêu cầu của dịch vụ: 0: Bình thường. 1: Theo yêu cầu", required = true)
    private Boolean dichVuYeuCau;

    @NotNull
    private Integer nam;


    private Long ttkbId;
    private Long thongTinKhoaId;
    private Long dotDieuTriId;
    private Long bakbId;
    private Long benhNhanId;
    private Long donViId;
    private Long dichVuKhamId;

    public ThongTinKhamBenhId getTtkbCompositeId(){
        ThongTinKhamBenhId result = new ThongTinKhamBenhId();
        result.setId(this.ttkbId);
        result.setThongTinKhoaId(this.thongTinKhoaId);
        result.setDotDieuTriId(this.dotDieuTriId);
        result.setBakbId(this.bakbId);
        result.setBenhNhanId(this.benhNhanId);
        result.setDonViId(this.donViId);
        return result;
    }

    public void setTtkbCompositeId(ThongTinKhamBenhId cId){
        this.ttkbId = cId.getId();
        this.thongTinKhoaId = cId.getThongTinKhoaId();
        this.dotDieuTriId = cId.getDotDieuTriId();
        this.bakbId = cId.getBakbId();
        this.benhNhanId = cId.getBenhNhanId();
        this.donViId = cId.getDonViId();
    }

    public Boolean getCongKhamBanDau() {
        return congKhamBanDau;
    }

    public Boolean getDaThanhToan() {
        return daThanhToan;
    }

    public Boolean getCoBaoHiem() {
        return coBaoHiem;
    }

    public Boolean getDichVuYeuCau() {
        return dichVuYeuCau;
    }

    public Long getThongTinKhoaId() {
        return thongTinKhoaId;
    }

    public void setThongTinKhoaId(Long thongTinKhoaId) {
        this.thongTinKhoaId = thongTinKhoaId;
    }

    public Long getDotDieuTriId() {
        return dotDieuTriId;
    }

    public void setDotDieuTriId(Long dotDieuTriId) {
        this.dotDieuTriId = dotDieuTriId;
    }

    public Long getBakbId() {
        return bakbId;
    }

    public void setBakbId(Long bakbId) {
        this.bakbId = bakbId;
    }

    public Long getBenhNhanId() {
        return benhNhanId;
    }

    public void setBenhNhanId(Long benhNhanId) {
        this.benhNhanId = benhNhanId;
    }

    public Long getDonViId() {
        return donViId;
    }

    public void setDonViId(Long donViId) {
        this.donViId = donViId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isCongKhamBanDau() {
        return congKhamBanDau;
    }

    public void setCongKhamBanDau(Boolean congKhamBanDau) {
        this.congKhamBanDau = congKhamBanDau;
    }

    public Boolean isDaThanhToan() {
        return daThanhToan;
    }

    public void setDaThanhToan(Boolean daThanhToan) {
        this.daThanhToan = daThanhToan;
    }

    public BigDecimal getDonGia() {
        return donGia;
    }

    public void setDonGia(BigDecimal donGia) {
        this.donGia = donGia;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public LocalDate getThoiGianChiDinh() {
        return thoiGianChiDinh;
    }

    public void setThoiGianChiDinh(LocalDate thoiGianChiDinh) {
        this.thoiGianChiDinh = thoiGianChiDinh;
    }

    public BigDecimal getTyLeThanhToan() {
        return tyLeThanhToan;
    }

    public void setTyLeThanhToan(BigDecimal tyLeThanhToan) {
        this.tyLeThanhToan = tyLeThanhToan;
    }

    public Boolean isCoBaoHiem() {
        return coBaoHiem;
    }

    public void setCoBaoHiem(Boolean coBaoHiem) {
        this.coBaoHiem = coBaoHiem;
    }

    public BigDecimal getGiaBhyt() {
        return giaBhyt;
    }

    public void setGiaBhyt(BigDecimal giaBhyt) {
        this.giaBhyt = giaBhyt;
    }

    public BigDecimal getGiaKhongBhyt() {
        return giaKhongBhyt;
    }

    public void setGiaKhongBhyt(BigDecimal giaKhongBhyt) {
        this.giaKhongBhyt = giaKhongBhyt;
    }

    public String getMaDungChung() {
        return maDungChung;
    }

    public void setMaDungChung(String maDungChung) {
        this.maDungChung = maDungChung;
    }

    public Boolean isDichVuYeuCau() {
        return dichVuYeuCau;
    }

    public void setDichVuYeuCau(Boolean dichVuYeuCau) {
        this.dichVuYeuCau = dichVuYeuCau;
    }

    public Integer getNam() {
        return nam;
    }

    public void setNam(Integer nam) {
        this.nam = nam;
    }

    public Long getTtkbId() {
        return ttkbId;
    }

    public void setTtkbId(Long thongTinKhamBenhId) {
        this.ttkbId = thongTinKhamBenhId;
    }

    public Long getDichVuKhamId() {
        return dichVuKhamId;
    }

    public void setDichVuKhamId(Long dichVuKhamId) {
        this.dichVuKhamId = dichVuKhamId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ChiDinhDichVuKhamDTO chiDinhDichVuKhamDTO = (ChiDinhDichVuKhamDTO) o;
        if (chiDinhDichVuKhamDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), chiDinhDichVuKhamDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ChiDinhDichVuKhamDTO{" +
            "id=" + id +
            ", congKhamBanDau=" + congKhamBanDau +
            ", daThanhToan=" + daThanhToan +
            ", donGia=" + donGia +
            ", ten='" + ten + '\'' +
            ", thoiGianChiDinh=" + thoiGianChiDinh +
            ", tyLeThanhToan=" + tyLeThanhToan +
            ", coBaoHiem=" + coBaoHiem +
            ", giaBhyt=" + giaBhyt +
            ", giaKhongBhyt=" + giaKhongBhyt +
            ", maDungChung='" + maDungChung + '\'' +
            ", dichVuYeuCau=" + dichVuYeuCau +
            ", nam=" + nam +
            ", ttkbId=" + ttkbId +
            ", thongTinKhoaId=" + thongTinKhoaId +
            ", dotDieuTriId=" + dotDieuTriId +
            ", bakbId=" + bakbId +
            ", benhNhanId=" + benhNhanId +
            ", donViId=" + donViId +
            ", dichVuKhamId=" + dichVuKhamId +
            '}';
    }
}

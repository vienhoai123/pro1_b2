package vn.vnpt.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.BigDecimalFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link vn.vnpt.domain.ChiDinhThuThuatPhauThuat} entity. This class is used
 * in {@link vn.vnpt.web.rest.ChiDinhThuThuatPhauThuatResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /chi-dinh-thu-thuat-phau-thuats?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ChiDinhThuThuatPhauThuatCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private BooleanFilter coBaoHiem;

    private BooleanFilter coKetQua;

    private IntegerFilter daThanhToan;

    private IntegerFilter daThanhToanChenhLech;

    private IntegerFilter daThucHien;

    private BigDecimalFilter donGia;

    private BigDecimalFilter donGiaBhyt;

    private BigDecimalFilter donGiaKhongBhyt;

    private StringFilter ghiChuChiDinh;

    private StringFilter moTa;

    private LongFilter nguoiChiDinhId;

    private IntegerFilter soLuong;

    private StringFilter ten;

    private BigDecimalFilter thanhTien;

    private BigDecimalFilter thanhTienBhyt;

    private BigDecimalFilter thanhTienKhongBHYT;

    private LocalDateFilter thoiGianChiDinh;

    private LocalDateFilter thoiGianTao;

    private BigDecimalFilter tienNgoaiBHYT;

    private BooleanFilter thanhToanChenhLech;

    private IntegerFilter tyLeThanhToan;

    private StringFilter maDungChung;

    private BooleanFilter dichVuYeuCau;

    private IntegerFilter nam;

    private LongFilter donViId;

    private LongFilter benhNhanId;

    private LongFilter bakbId;

    private LongFilter phieuCDId;

    private LongFilter phongId;

    private LongFilter ttptId;

    public ChiDinhThuThuatPhauThuatCriteria() {
    }

    public ChiDinhThuThuatPhauThuatCriteria(ChiDinhThuThuatPhauThuatCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.coBaoHiem = other.coBaoHiem == null ? null : other.coBaoHiem.copy();
        this.coKetQua = other.coKetQua == null ? null : other.coKetQua.copy();
        this.daThanhToan = other.daThanhToan == null ? null : other.daThanhToan.copy();
        this.daThanhToanChenhLech = other.daThanhToanChenhLech == null ? null : other.daThanhToanChenhLech.copy();
        this.daThucHien = other.daThucHien == null ? null : other.daThucHien.copy();
        this.donGia = other.donGia == null ? null : other.donGia.copy();
        this.donGiaBhyt = other.donGiaBhyt == null ? null : other.donGiaBhyt.copy();
        this.donGiaKhongBhyt = other.donGiaKhongBhyt == null ? null : other.donGiaKhongBhyt.copy();
        this.ghiChuChiDinh = other.ghiChuChiDinh == null ? null : other.ghiChuChiDinh.copy();
        this.moTa = other.moTa == null ? null : other.moTa.copy();
        this.nguoiChiDinhId = other.nguoiChiDinhId == null ? null : other.nguoiChiDinhId.copy();
        this.soLuong = other.soLuong == null ? null : other.soLuong.copy();
        this.ten = other.ten == null ? null : other.ten.copy();
        this.thanhTien = other.thanhTien == null ? null : other.thanhTien.copy();
        this.thanhTienBhyt = other.thanhTienBhyt == null ? null : other.thanhTienBhyt.copy();
        this.thanhTienKhongBHYT = other.thanhTienKhongBHYT == null ? null : other.thanhTienKhongBHYT.copy();
        this.thoiGianChiDinh = other.thoiGianChiDinh == null ? null : other.thoiGianChiDinh.copy();
        this.thoiGianTao = other.thoiGianTao == null ? null : other.thoiGianTao.copy();
        this.tienNgoaiBHYT = other.tienNgoaiBHYT == null ? null : other.tienNgoaiBHYT.copy();
        this.thanhToanChenhLech = other.thanhToanChenhLech == null ? null : other.thanhToanChenhLech.copy();
        this.tyLeThanhToan = other.tyLeThanhToan == null ? null : other.tyLeThanhToan.copy();
        this.maDungChung = other.maDungChung == null ? null : other.maDungChung.copy();
        this.dichVuYeuCau = other.dichVuYeuCau == null ? null : other.dichVuYeuCau.copy();
        this.nam = other.nam == null ? null : other.nam.copy();
        this.donViId = other.donViId == null ? null : other.donViId.copy();
        this.benhNhanId = other.benhNhanId == null ? null : other.benhNhanId.copy();
        this.bakbId = other.bakbId == null ? null : other.bakbId.copy();
        this.phieuCDId = other.phieuCDId == null ? null : other.phieuCDId.copy();
        this.phongId = other.phongId == null ? null : other.phongId.copy();
        this.ttptId = other.ttptId == null ? null : other.ttptId.copy();
    }

    @Override
    public ChiDinhThuThuatPhauThuatCriteria copy() {
        return new ChiDinhThuThuatPhauThuatCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public BooleanFilter getCoBaoHiem() {
        return coBaoHiem;
    }

    public void setCoBaoHiem(BooleanFilter coBaoHiem) {
        this.coBaoHiem = coBaoHiem;
    }

    public BooleanFilter getCoKetQua() {
        return coKetQua;
    }

    public void setCoKetQua(BooleanFilter coKetQua) {
        this.coKetQua = coKetQua;
    }

    public IntegerFilter getDaThanhToan() {
        return daThanhToan;
    }

    public void setDaThanhToan(IntegerFilter daThanhToan) {
        this.daThanhToan = daThanhToan;
    }

    public IntegerFilter getDaThanhToanChenhLech() {
        return daThanhToanChenhLech;
    }

    public void setDaThanhToanChenhLech(IntegerFilter daThanhToanChenhLech) {
        this.daThanhToanChenhLech = daThanhToanChenhLech;
    }

    public IntegerFilter getDaThucHien() {
        return daThucHien;
    }

    public void setDaThucHien(IntegerFilter daThucHien) {
        this.daThucHien = daThucHien;
    }

    public BigDecimalFilter getDonGia() {
        return donGia;
    }

    public void setDonGia(BigDecimalFilter donGia) {
        this.donGia = donGia;
    }

    public BigDecimalFilter getDonGiaBhyt() {
        return donGiaBhyt;
    }

    public void setDonGiaBhyt(BigDecimalFilter donGiaBhyt) {
        this.donGiaBhyt = donGiaBhyt;
    }

    public BigDecimalFilter getDonGiaKhongBhyt() {
        return donGiaKhongBhyt;
    }

    public void setDonGiaKhongBhyt(BigDecimalFilter donGiaKhongBhyt) {
        this.donGiaKhongBhyt = donGiaKhongBhyt;
    }

    public StringFilter getGhiChuChiDinh() {
        return ghiChuChiDinh;
    }

    public void setGhiChuChiDinh(StringFilter ghiChuChiDinh) {
        this.ghiChuChiDinh = ghiChuChiDinh;
    }

    public StringFilter getMoTa() {
        return moTa;
    }

    public void setMoTa(StringFilter moTa) {
        this.moTa = moTa;
    }

    public LongFilter getNguoiChiDinhId() {
        return nguoiChiDinhId;
    }

    public void setNguoiChiDinhId(LongFilter nguoiChiDinhId) {
        this.nguoiChiDinhId = nguoiChiDinhId;
    }

    public IntegerFilter getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(IntegerFilter soLuong) {
        this.soLuong = soLuong;
    }

    public StringFilter getTen() {
        return ten;
    }

    public void setTen(StringFilter ten) {
        this.ten = ten;
    }

    public BigDecimalFilter getThanhTien() {
        return thanhTien;
    }

    public void setThanhTien(BigDecimalFilter thanhTien) {
        this.thanhTien = thanhTien;
    }

    public BigDecimalFilter getThanhTienBhyt() {
        return thanhTienBhyt;
    }

    public void setThanhTienBhyt(BigDecimalFilter thanhTienBhyt) {
        this.thanhTienBhyt = thanhTienBhyt;
    }

    public BigDecimalFilter getThanhTienKhongBHYT() {
        return thanhTienKhongBHYT;
    }

    public void setThanhTienKhongBHYT(BigDecimalFilter thanhTienKhongBHYT) {
        this.thanhTienKhongBHYT = thanhTienKhongBHYT;
    }

    public LocalDateFilter getThoiGianChiDinh() {
        return thoiGianChiDinh;
    }

    public void setThoiGianChiDinh(LocalDateFilter thoiGianChiDinh) {
        this.thoiGianChiDinh = thoiGianChiDinh;
    }

    public LocalDateFilter getThoiGianTao() {
        return thoiGianTao;
    }

    public void setThoiGianTao(LocalDateFilter thoiGianTao) {
        this.thoiGianTao = thoiGianTao;
    }

    public BigDecimalFilter getTienNgoaiBHYT() {
        return tienNgoaiBHYT;
    }

    public void setTienNgoaiBHYT(BigDecimalFilter tienNgoaiBHYT) {
        this.tienNgoaiBHYT = tienNgoaiBHYT;
    }

    public BooleanFilter getThanhToanChenhLech() {
        return thanhToanChenhLech;
    }

    public void setThanhToanChenhLech(BooleanFilter thanhToanChenhLech) {
        this.thanhToanChenhLech = thanhToanChenhLech;
    }

    public IntegerFilter getTyLeThanhToan() {
        return tyLeThanhToan;
    }

    public void setTyLeThanhToan(IntegerFilter tyLeThanhToan) {
        this.tyLeThanhToan = tyLeThanhToan;
    }

    public StringFilter getMaDungChung() {
        return maDungChung;
    }

    public void setMaDungChung(StringFilter maDungChung) {
        this.maDungChung = maDungChung;
    }

    public BooleanFilter getDichVuYeuCau() {
        return dichVuYeuCau;
    }

    public void setDichVuYeuCau(BooleanFilter dichVuYeuCau) {
        this.dichVuYeuCau = dichVuYeuCau;
    }

    public IntegerFilter getNam() {
        return nam;
    }

    public void setNam(IntegerFilter nam) {
        this.nam = nam;
    }

    public LongFilter getDonViId() {
        return donViId;
    }

    public void setDonViId(LongFilter donViId) {
        this.donViId = donViId;
    }

    public LongFilter getBenhNhanId() {
        return benhNhanId;
    }

    public void setBenhNhanId(LongFilter benhNhanId) {
        this.benhNhanId = benhNhanId;
    }

    public LongFilter getBakbId() {
        return bakbId;
    }

    public void setBakbId(LongFilter bakbId) {
        this.bakbId = bakbId;
    }

    public LongFilter getPhieuCDId() {
        return phieuCDId;
    }

    public void setPhieuCDId(LongFilter phieuCDId) {
        this.phieuCDId = phieuCDId;
    }

    public LongFilter getPhongId() {
        return phongId;
    }

    public void setPhongId(LongFilter phongId) {
        this.phongId = phongId;
    }

    public LongFilter getTtptId() {
        return ttptId;
    }

    public void setTtptId(LongFilter ttptId) {
        this.ttptId = ttptId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ChiDinhThuThuatPhauThuatCriteria that = (ChiDinhThuThuatPhauThuatCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(coBaoHiem, that.coBaoHiem) &&
            Objects.equals(coKetQua, that.coKetQua) &&
            Objects.equals(daThanhToan, that.daThanhToan) &&
            Objects.equals(daThanhToanChenhLech, that.daThanhToanChenhLech) &&
            Objects.equals(daThucHien, that.daThucHien) &&
            Objects.equals(donGia, that.donGia) &&
            Objects.equals(donGiaBhyt, that.donGiaBhyt) &&
            Objects.equals(donGiaKhongBhyt, that.donGiaKhongBhyt) &&
            Objects.equals(ghiChuChiDinh, that.ghiChuChiDinh) &&
            Objects.equals(moTa, that.moTa) &&
            Objects.equals(nguoiChiDinhId, that.nguoiChiDinhId) &&
            Objects.equals(soLuong, that.soLuong) &&
            Objects.equals(ten, that.ten) &&
            Objects.equals(thanhTien, that.thanhTien) &&
            Objects.equals(thanhTienBhyt, that.thanhTienBhyt) &&
            Objects.equals(thanhTienKhongBHYT, that.thanhTienKhongBHYT) &&
            Objects.equals(thoiGianChiDinh, that.thoiGianChiDinh) &&
            Objects.equals(thoiGianTao, that.thoiGianTao) &&
            Objects.equals(tienNgoaiBHYT, that.tienNgoaiBHYT) &&
            Objects.equals(thanhToanChenhLech, that.thanhToanChenhLech) &&
            Objects.equals(tyLeThanhToan, that.tyLeThanhToan) &&
            Objects.equals(maDungChung, that.maDungChung) &&
            Objects.equals(dichVuYeuCau, that.dichVuYeuCau) &&
            Objects.equals(nam, that.nam) &&
            Objects.equals(donViId, that.donViId) &&
            Objects.equals(benhNhanId, that.benhNhanId) &&
            Objects.equals(bakbId, that.bakbId) &&
            Objects.equals(phieuCDId, that.phieuCDId) &&
            Objects.equals(phongId, that.phongId) &&
            Objects.equals(ttptId, that.ttptId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        coBaoHiem,
        coKetQua,
        daThanhToan,
        daThanhToanChenhLech,
        daThucHien,
        donGia,
        donGiaBhyt,
        donGiaKhongBhyt,
        ghiChuChiDinh,
        moTa,
        nguoiChiDinhId,
        soLuong,
        ten,
        thanhTien,
        thanhTienBhyt,
        thanhTienKhongBHYT,
        thoiGianChiDinh,
        thoiGianTao,
        tienNgoaiBHYT,
        thanhToanChenhLech,
        tyLeThanhToan,
        maDungChung,
        dichVuYeuCau,
        nam,
        donViId,
        benhNhanId,
        bakbId,
        phieuCDId,
        phongId,
        ttptId
        );
    }

    @Override
    public String toString() {
        return "ChiDinhThuThuatPhauThuatCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (coBaoHiem != null ? "coBaoHiem=" + coBaoHiem + ", " : "") +
                (coKetQua != null ? "coKetQua=" + coKetQua + ", " : "") +
                (daThanhToan != null ? "daThanhToan=" + daThanhToan + ", " : "") +
                (daThanhToanChenhLech != null ? "daThanhToanChenhLech=" + daThanhToanChenhLech + ", " : "") +
                (daThucHien != null ? "daThucHien=" + daThucHien + ", " : "") +
                (donGia != null ? "donGia=" + donGia + ", " : "") +
                (donGiaBhyt != null ? "donGiaBhyt=" + donGiaBhyt + ", " : "") +
                (donGiaKhongBhyt != null ? "donGiaKhongBhyt=" + donGiaKhongBhyt + ", " : "") +
                (ghiChuChiDinh != null ? "ghiChuChiDinh=" + ghiChuChiDinh + ", " : "") +
                (moTa != null ? "moTa=" + moTa + ", " : "") +
                (nguoiChiDinhId != null ? "nguoiChiDinhId=" + nguoiChiDinhId + ", " : "") +
                (soLuong != null ? "soLuong=" + soLuong + ", " : "") +
                (ten != null ? "ten=" + ten + ", " : "") +
                (thanhTien != null ? "thanhTien=" + thanhTien + ", " : "") +
                (thanhTienBhyt != null ? "thanhTienBhyt=" + thanhTienBhyt + ", " : "") +
                (thanhTienKhongBHYT != null ? "thanhTienKhongBHYT=" + thanhTienKhongBHYT + ", " : "") +
                (thoiGianChiDinh != null ? "thoiGianChiDinh=" + thoiGianChiDinh + ", " : "") +
                (thoiGianTao != null ? "thoiGianTao=" + thoiGianTao + ", " : "") +
                (tienNgoaiBHYT != null ? "tienNgoaiBHYT=" + tienNgoaiBHYT + ", " : "") +
                (thanhToanChenhLech != null ? "thanhToanChenhLech=" + thanhToanChenhLech + ", " : "") +
                (tyLeThanhToan != null ? "tyLeThanhToan=" + tyLeThanhToan + ", " : "") +
                (maDungChung != null ? "maDungChung=" + maDungChung + ", " : "") +
                (dichVuYeuCau != null ? "dichVuYeuCau=" + dichVuYeuCau + ", " : "") +
                (nam != null ? "nam=" + nam + ", " : "") +
                (donViId != null ? "donViId=" + donViId + ", " : "") +
                (benhNhanId != null ? "benhNhanId=" + benhNhanId + ", " : "") +
                (bakbId != null ? "bakbId=" + bakbId + ", " : "") +
                (phieuCDId != null ? "phieuCDId=" + phieuCDId + ", " : "") +
                (phongId != null ? "phongId=" + phongId + ", " : "") +
                (ttptId != null ? "ttptId=" + ttptId + ", " : "") +
            "}";
    }

}

package vn.vnpt.service.dto;

public class TestDTO {
        private String HoTen;
        private String Test1;
        private String Test2;
        public TestDTO(String hoTen,String test1, String test2){
            this.HoTen=hoTen;
            this.Test1=test1;
            this.Test2=test2;
        }

        public String getHoTen() {
            return HoTen;
        }

        public void setHoTen(String hoTen) {
            HoTen = hoTen;
        }

        public String getTest1() {
            return Test1;
        }

        public void setTest1(String test1) {
            Test1 = test1;
        }

        public String getTest2() {
            return Test2;
        }

        public void setTest2(String test2) {
            Test2 = test2;
        }
}

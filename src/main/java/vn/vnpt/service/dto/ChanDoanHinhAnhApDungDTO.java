package vn.vnpt.service.dto;

import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A DTO for the {@link vn.vnpt.domain.ChanDoanHinhAnhApDung} entity.
 */
public class ChanDoanHinhAnhApDungDTO implements Serializable {
    
    private Long id;

    /**
     * mã báo cáo Bảo Hiểm Xã hội
     */
    @Size(max = 200)
    @ApiModelProperty(value = "mã báo cáo Bảo Hiểm Xã hội")
    private String maBaoCaoBhxh;

    /**
     * mã báo cáo Bảo Hiểm Y tế
     */
    @Size(max = 30)
    @ApiModelProperty(value = "mã báo cáo Bảo Hiểm Y tế")
    private String maBaoCaoBhyt;

    /**
     * Ngày Áp dụng
     */
    @NotNull
    @ApiModelProperty(value = "Ngày Áp dụng", required = true)
    private LocalDate ngayApDung;

    /**
     * Số công văn Bảo hiểm xã hội
     */
    @Size(max = 200)
    @ApiModelProperty(value = "Số công văn Bảo hiểm xã hội")
    private String soCongVanBhxh;

    /**
     * Số quyết định
     */
    @Size(max = 100)
    @ApiModelProperty(value = "Số quyết định")
    private String soQuyetDinh;

    /**
     * Tên báo cáo Bảo hiểm xã hội
     */
    @Size(max = 500)
    @ApiModelProperty(value = "Tên báo cáo Bảo hiểm xã hội")
    private String tenBaoCaoBhxh;

    /**
     * Tên dịch vụ không Bảo hiểm y tế
     */
    @Size(max = 500)
    @ApiModelProperty(value = "Tên dịch vụ không Bảo hiểm y tế")
    private String tenDichVuKhongBhyt;

    /**
     * Tiền bệnh nhân chi
     */
    @ApiModelProperty(value = "Tiền bệnh nhân chi")
    private BigDecimal tienBenhNhanChi;

    /**
     * Tiền bảo hiểm xã hội chi
     */
    @ApiModelProperty(value = "Tiền bảo hiểm xã hội chi")
    private BigDecimal tienBhxhChi;

    /**
     * Tiền ngoài bhyt
     */
    @ApiModelProperty(value = "Tiền ngoài bhyt")
    private BigDecimal tienNgoaiBhyt;

    /**
     * Tổng tiền thanh toán
     */
    @ApiModelProperty(value = "Tổng tiền thanh toán")
    private BigDecimal tongTienThanhToan;

    /**
     * Tỷ lệ Bảo hiểm xã hội thanh toán
     */
    @ApiModelProperty(value = "Tỷ lệ Bảo hiểm xã hội thanh toán")
    private Integer tyLeBhxhThanhToan;

    /**
     * Giá Bảo hiểm y tế
     */
    @NotNull
    @ApiModelProperty(value = "Giá Bảo hiểm y tế", required = true)
    private BigDecimal giaBhyt;

    /**
     * Giá không bảo hiểm y tế
     */
    @NotNull
    @ApiModelProperty(value = "Giá không bảo hiểm y tế", required = true)
    private BigDecimal giaKhongBhyt;

    /**
     * Đối tượng đặc biệt có bảng giá khác. 0: Bình thường .1 được quy định
     */
    @ApiModelProperty(value = "Đối tượng đặc biệt có bảng giá khác. 0: Bình thường .1 được quy định")
    private Boolean doiTuongDacBiet;

    /**
     * Nguồn chi bảo hiểm cho bệnh nhân: 1: BHYT .2: Nhà nước 3. Khác
     */
    @ApiModelProperty(value = "Nguồn chi bảo hiểm cho bệnh nhân: 1: BHYT .2: Nhà nước 3. Khác")
    private Integer nguonChi;

    /**
     * Trạng thái có hiệu lực: 1: Có hiệu lực. 0: không có hiệu lực
     */
    @ApiModelProperty(value = "Trạng thái có hiệu lực: 1: Có hiệu lực. 0: không có hiệu lực")
    private Boolean enable;


    private Long dotGiaId;

    private Long cdhaId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMaBaoCaoBhxh() {
        return maBaoCaoBhxh;
    }

    public void setMaBaoCaoBhxh(String maBaoCaoBhxh) {
        this.maBaoCaoBhxh = maBaoCaoBhxh;
    }

    public String getMaBaoCaoBhyt() {
        return maBaoCaoBhyt;
    }

    public void setMaBaoCaoBhyt(String maBaoCaoBhyt) {
        this.maBaoCaoBhyt = maBaoCaoBhyt;
    }

    public LocalDate getNgayApDung() {
        return ngayApDung;
    }

    public void setNgayApDung(LocalDate ngayApDung) {
        this.ngayApDung = ngayApDung;
    }

    public String getSoCongVanBhxh() {
        return soCongVanBhxh;
    }

    public void setSoCongVanBhxh(String soCongVanBhxh) {
        this.soCongVanBhxh = soCongVanBhxh;
    }

    public String getSoQuyetDinh() {
        return soQuyetDinh;
    }

    public void setSoQuyetDinh(String soQuyetDinh) {
        this.soQuyetDinh = soQuyetDinh;
    }

    public String getTenBaoCaoBhxh() {
        return tenBaoCaoBhxh;
    }

    public void setTenBaoCaoBhxh(String tenBaoCaoBhxh) {
        this.tenBaoCaoBhxh = tenBaoCaoBhxh;
    }

    public String getTenDichVuKhongBhyt() {
        return tenDichVuKhongBhyt;
    }

    public void setTenDichVuKhongBhyt(String tenDichVuKhongBhyt) {
        this.tenDichVuKhongBhyt = tenDichVuKhongBhyt;
    }

    public BigDecimal getTienBenhNhanChi() {
        return tienBenhNhanChi;
    }

    public void setTienBenhNhanChi(BigDecimal tienBenhNhanChi) {
        this.tienBenhNhanChi = tienBenhNhanChi;
    }

    public BigDecimal getTienBhxhChi() {
        return tienBhxhChi;
    }

    public void setTienBhxhChi(BigDecimal tienBhxhChi) {
        this.tienBhxhChi = tienBhxhChi;
    }

    public BigDecimal getTienNgoaiBhyt() {
        return tienNgoaiBhyt;
    }

    public void setTienNgoaiBhyt(BigDecimal tienNgoaiBhyt) {
        this.tienNgoaiBhyt = tienNgoaiBhyt;
    }

    public BigDecimal getTongTienThanhToan() {
        return tongTienThanhToan;
    }

    public void setTongTienThanhToan(BigDecimal tongTienThanhToan) {
        this.tongTienThanhToan = tongTienThanhToan;
    }

    public Integer getTyLeBhxhThanhToan() {
        return tyLeBhxhThanhToan;
    }

    public void setTyLeBhxhThanhToan(Integer tyLeBhxhThanhToan) {
        this.tyLeBhxhThanhToan = tyLeBhxhThanhToan;
    }

    public BigDecimal getGiaBhyt() {
        return giaBhyt;
    }

    public void setGiaBhyt(BigDecimal giaBhyt) {
        this.giaBhyt = giaBhyt;
    }

    public BigDecimal getGiaKhongBhyt() {
        return giaKhongBhyt;
    }

    public void setGiaKhongBhyt(BigDecimal giaKhongBhyt) {
        this.giaKhongBhyt = giaKhongBhyt;
    }

    public Boolean isDoiTuongDacBiet() {
        return doiTuongDacBiet;
    }

    public void setDoiTuongDacBiet(Boolean doiTuongDacBiet) {
        this.doiTuongDacBiet = doiTuongDacBiet;
    }

    public Integer getNguonChi() {
        return nguonChi;
    }

    public void setNguonChi(Integer nguonChi) {
        this.nguonChi = nguonChi;
    }

    public Boolean isEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public Long getDotGiaId() {
        return dotGiaId;
    }

    public void setDotGiaId(Long dotGiaDichVuBhxhId) {
        this.dotGiaId = dotGiaDichVuBhxhId;
    }

    public Long getCdhaId() {
        return cdhaId;
    }

    public void setCdhaId(Long chanDoanHinhAnhId) {
        this.cdhaId = chanDoanHinhAnhId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ChanDoanHinhAnhApDungDTO chanDoanHinhAnhApDungDTO = (ChanDoanHinhAnhApDungDTO) o;
        if (chanDoanHinhAnhApDungDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), chanDoanHinhAnhApDungDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ChanDoanHinhAnhApDungDTO{" +
            "id=" + getId() +
            ", maBaoCaoBhxh='" + getMaBaoCaoBhxh() + "'" +
            ", maBaoCaoBhyt='" + getMaBaoCaoBhyt() + "'" +
            ", ngayApDung='" + getNgayApDung() + "'" +
            ", soCongVanBhxh='" + getSoCongVanBhxh() + "'" +
            ", soQuyetDinh='" + getSoQuyetDinh() + "'" +
            ", tenBaoCaoBhxh='" + getTenBaoCaoBhxh() + "'" +
            ", tenDichVuKhongBhyt='" + getTenDichVuKhongBhyt() + "'" +
            ", tienBenhNhanChi=" + getTienBenhNhanChi() +
            ", tienBhxhChi=" + getTienBhxhChi() +
            ", tienNgoaiBhyt=" + getTienNgoaiBhyt() +
            ", tongTienThanhToan=" + getTongTienThanhToan() +
            ", tyLeBhxhThanhToan=" + getTyLeBhxhThanhToan() +
            ", giaBhyt=" + getGiaBhyt() +
            ", giaKhongBhyt=" + getGiaKhongBhyt() +
            ", doiTuongDacBiet='" + isDoiTuongDacBiet() + "'" +
            ", nguonChi=" + getNguonChi() +
            ", enable='" + isEnable() + "'" +
            ", dotGiaId=" + getDotGiaId() +
            ", cdhaId=" + getCdhaId() +
            "}";
    }
}

package vn.vnpt.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link vn.vnpt.domain.Khoa} entity. This class is used
 * in {@link vn.vnpt.web.rest.KhoaResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /khoas?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class KhoaCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter cap;

    private BooleanFilter enabled;

    private StringFilter kyHieu;

    private StringFilter phone;

    private StringFilter ten;

    private LongFilter donViId;

    public KhoaCriteria() {
    }

    public KhoaCriteria(KhoaCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.cap = other.cap == null ? null : other.cap.copy();
        this.enabled = other.enabled == null ? null : other.enabled.copy();
        this.kyHieu = other.kyHieu == null ? null : other.kyHieu.copy();
        this.phone = other.phone == null ? null : other.phone.copy();
        this.ten = other.ten == null ? null : other.ten.copy();
        this.donViId = other.donViId == null ? null : other.donViId.copy();
    }

    @Override
    public KhoaCriteria copy() {
        return new KhoaCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getCap() {
        return cap;
    }

    public void setCap(IntegerFilter cap) {
        this.cap = cap;
    }

    public BooleanFilter getEnabled() {
        return enabled;
    }

    public void setEnabled(BooleanFilter enabled) {
        this.enabled = enabled;
    }

    public StringFilter getKyHieu() {
        return kyHieu;
    }

    public void setKyHieu(StringFilter kyHieu) {
        this.kyHieu = kyHieu;
    }

    public StringFilter getPhone() {
        return phone;
    }

    public void setPhone(StringFilter phone) {
        this.phone = phone;
    }

    public StringFilter getTen() {
        return ten;
    }

    public void setTen(StringFilter ten) {
        this.ten = ten;
    }

    public LongFilter getDonViId() {
        return donViId;
    }

    public void setDonViId(LongFilter donViId) {
        this.donViId = donViId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final KhoaCriteria that = (KhoaCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(cap, that.cap) &&
            Objects.equals(enabled, that.enabled) &&
            Objects.equals(kyHieu, that.kyHieu) &&
            Objects.equals(phone, that.phone) &&
            Objects.equals(ten, that.ten) &&
            Objects.equals(donViId, that.donViId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        cap,
        enabled,
        kyHieu,
        phone,
        ten,
        donViId
        );
    }

    @Override
    public String toString() {
        return "KhoaCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (cap != null ? "cap=" + cap + ", " : "") +
                (enabled != null ? "enabled=" + enabled + ", " : "") +
                (kyHieu != null ? "kyHieu=" + kyHieu + ", " : "") +
                (phone != null ? "phone=" + phone + ", " : "") +
                (ten != null ? "ten=" + ten + ", " : "") +
                (donViId != null ? "donViId=" + donViId + ", " : "") +
            "}";
    }

}

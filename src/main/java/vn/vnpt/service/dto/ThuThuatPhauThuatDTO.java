package vn.vnpt.service.dto;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A DTO for the {@link vn.vnpt.domain.ThuThuatPhauThuat} entity.
 */
public class ThuThuatPhauThuatDTO implements Serializable {
    
    private Long id;

    /**
     * Trạng thái xóa bỏ của dòng dữ liệu. \r\n1: Đã xóa. \r\n0: Còn hiệu lực.
     */
    @NotNull
    @ApiModelProperty(value = "Trạng thái xóa bỏ của dòng dữ liệu. \r\n1: Đã xóa. \r\n0: Còn hiệu lực.", required = true)
    private Boolean deleted;

    /**
     * Trạng thái dịch vụ theo yêu cầu: \r\n1: Dịch vụ theo yêu cầu. \r\n0: Dịch vụ bình thường. 
     */
    @NotNull
    @ApiModelProperty(value = "Trạng thái dịch vụ theo yêu cầu: \r\n1: Dịch vụ theo yêu cầu. \r\n0: Dịch vụ bình thường. ", required = true)
    private Boolean dichVuYeuCau;

    /**
     * Đơn giá bệnh viện
     */
    @NotNull
    @ApiModelProperty(value = "Đơn giá bệnh viện", required = true)
    private BigDecimal donGiaBenhVien;

    /**
     * Trạng thái có hiệu lực của dịch vụ: \r\n1: Còn hiệu lực. \r\n0: Đã ẩn.
     */
    @NotNull
    @ApiModelProperty(value = "Trạng thái có hiệu lực của dịch vụ: \r\n1: Còn hiệu lực. \r\n0: Đã ẩn.", required = true)
    private Boolean enabled;

    /**
     * Số lượng giới hạn của một chỉ định
     */
    @ApiModelProperty(value = "Số lượng giới hạn của một chỉ định")
    private BigDecimal goiHanChiDinh;

    /**
     * Trạng thái phạm vi của chỉ định:\r\n0: Chỉ có thu phí. \r\n1: Có cả BHYT và thu phí
     */
    @NotNull
    @ApiModelProperty(value = "Trạng thái phạm vi của chỉ định:\r\n0: Chỉ có thu phí. \r\n1: Có cả BHYT và thu phí", required = true)
    private Boolean phamViChiDinh;

    /**
     * Phân biệt theo giới tính: \r\n0: Nữ. \r\n1: Nam. \r\n2: Không phân biệt giới tính. \r\n
     */
    @NotNull
    @ApiModelProperty(value = "Phân biệt theo giới tính: \r\n0: Nữ. \r\n1: Nam. \r\n2: Không phân biệt giới tính. \r\n", required = true)
    private Integer phanTheoGioiTinh;

    /**
     * Tên của chỉ định
     */
    @Size(max = 500)
    @ApiModelProperty(value = "Tên của chỉ định")
    private String ten;

    /**
     * Tên hiển thị của chỉ định
     */
    @Size(max = 500)
    @ApiModelProperty(value = "Tên hiển thị của chỉ định")
    private String tenHienThi;

    /**
     * Mã nội bộ
     */
    @Size(max = 255)
    @ApiModelProperty(value = "Mã nội bộ")
    private String maNoiBo;

    /**
     * Mã dùng chung
     */
    @Size(max = 45)
    @ApiModelProperty(value = "Mã dùng chung")
    private String maDungChung;


    private Long donViId;

    private Long dotMaId;

    private Long loaittptId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Boolean isDichVuYeuCau() {
        return dichVuYeuCau;
    }

    public void setDichVuYeuCau(Boolean dichVuYeuCau) {
        this.dichVuYeuCau = dichVuYeuCau;
    }

    public BigDecimal getDonGiaBenhVien() {
        return donGiaBenhVien;
    }

    public void setDonGiaBenhVien(BigDecimal donGiaBenhVien) {
        this.donGiaBenhVien = donGiaBenhVien;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public BigDecimal getGoiHanChiDinh() {
        return goiHanChiDinh;
    }

    public void setGoiHanChiDinh(BigDecimal goiHanChiDinh) {
        this.goiHanChiDinh = goiHanChiDinh;
    }

    public Boolean isPhamViChiDinh() {
        return phamViChiDinh;
    }

    public void setPhamViChiDinh(Boolean phamViChiDinh) {
        this.phamViChiDinh = phamViChiDinh;
    }

    public Integer getPhanTheoGioiTinh() {
        return phanTheoGioiTinh;
    }

    public void setPhanTheoGioiTinh(Integer phanTheoGioiTinh) {
        this.phanTheoGioiTinh = phanTheoGioiTinh;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getTenHienThi() {
        return tenHienThi;
    }

    public void setTenHienThi(String tenHienThi) {
        this.tenHienThi = tenHienThi;
    }

    public String getMaNoiBo() {
        return maNoiBo;
    }

    public void setMaNoiBo(String maNoiBo) {
        this.maNoiBo = maNoiBo;
    }

    public String getMaDungChung() {
        return maDungChung;
    }

    public void setMaDungChung(String maDungChung) {
        this.maDungChung = maDungChung;
    }

    public Long getDonViId() {
        return donViId;
    }

    public void setDonViId(Long donViId) {
        this.donViId = donViId;
    }

    public Long getDotMaId() {
        return dotMaId;
    }

    public void setDotMaId(Long dotThayDoiMaDichVuId) {
        this.dotMaId = dotThayDoiMaDichVuId;
    }

    public Long getLoaittptId() {
        return loaittptId;
    }

    public void setLoaittptId(Long loaiThuThuatPhauThuatId) {
        this.loaittptId = loaiThuThuatPhauThuatId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ThuThuatPhauThuatDTO thuThuatPhauThuatDTO = (ThuThuatPhauThuatDTO) o;
        if (thuThuatPhauThuatDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), thuThuatPhauThuatDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ThuThuatPhauThuatDTO{" +
            "id=" + getId() +
            ", deleted='" + isDeleted() + "'" +
            ", dichVuYeuCau='" + isDichVuYeuCau() + "'" +
            ", donGiaBenhVien=" + getDonGiaBenhVien() +
            ", enabled='" + isEnabled() + "'" +
            ", goiHanChiDinh=" + getGoiHanChiDinh() +
            ", phamViChiDinh='" + isPhamViChiDinh() + "'" +
            ", phanTheoGioiTinh=" + getPhanTheoGioiTinh() +
            ", ten='" + getTen() + "'" +
            ", tenHienThi='" + getTenHienThi() + "'" +
            ", maNoiBo='" + getMaNoiBo() + "'" +
            ", maDungChung='" + getMaDungChung() + "'" +
            ", donViId=" + getDonViId() +
            ", dotMaId=" + getDotMaId() +
            ", loaittptId=" + getLoaittptId() +
            "}";
    }
}

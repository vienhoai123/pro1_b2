package vn.vnpt.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.BigDecimalFilter;

/**
 * Criteria class for the {@link vn.vnpt.domain.DichVuKham} entity. This class is used
 * in {@link vn.vnpt.web.rest.DichVuKhamResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /dich-vu-khams?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class DichVuKhamCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter deleted;

    private IntegerFilter enabled;

    private IntegerFilter gioiHanChiDinh;

    private IntegerFilter phanTheoGioTinh;

    private StringFilter tenHienThi;

    private StringFilter maDungChung;

    private StringFilter maNoiBo;

    private IntegerFilter phamViChiDinh;

    private StringFilter donViTinh;

    private BigDecimalFilter donGiaBenhVien;

    private LongFilter dotThayDoiMaDichVuId;

    public DichVuKhamCriteria() {
    }

    public DichVuKhamCriteria(DichVuKhamCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.deleted = other.deleted == null ? null : other.deleted.copy();
        this.enabled = other.enabled == null ? null : other.enabled.copy();
        this.gioiHanChiDinh = other.gioiHanChiDinh == null ? null : other.gioiHanChiDinh.copy();
        this.phanTheoGioTinh = other.phanTheoGioTinh == null ? null : other.phanTheoGioTinh.copy();
        this.tenHienThi = other.tenHienThi == null ? null : other.tenHienThi.copy();
        this.maDungChung = other.maDungChung == null ? null : other.maDungChung.copy();
        this.maNoiBo = other.maNoiBo == null ? null : other.maNoiBo.copy();
        this.phamViChiDinh = other.phamViChiDinh == null ? null : other.phamViChiDinh.copy();
        this.donViTinh = other.donViTinh == null ? null : other.donViTinh.copy();
        this.donGiaBenhVien = other.donGiaBenhVien == null ? null : other.donGiaBenhVien.copy();
        this.dotThayDoiMaDichVuId = other.dotThayDoiMaDichVuId == null ? null : other.dotThayDoiMaDichVuId.copy();
    }

    @Override
    public DichVuKhamCriteria copy() {
        return new DichVuKhamCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getDeleted() {
        return deleted;
    }

    public void setDeleted(IntegerFilter deleted) {
        this.deleted = deleted;
    }

    public IntegerFilter getEnabled() {
        return enabled;
    }

    public void setEnabled(IntegerFilter enabled) {
        this.enabled = enabled;
    }

    public IntegerFilter getGioiHanChiDinh() {
        return gioiHanChiDinh;
    }

    public void setGioiHanChiDinh(IntegerFilter gioiHanChiDinh) {
        this.gioiHanChiDinh = gioiHanChiDinh;
    }

    public IntegerFilter getPhanTheoGioTinh() {
        return phanTheoGioTinh;
    }

    public void setPhanTheoGioTinh(IntegerFilter phanTheoGioTinh) {
        this.phanTheoGioTinh = phanTheoGioTinh;
    }

    public StringFilter getTenHienThi() {
        return tenHienThi;
    }

    public void setTenHienThi(StringFilter tenHienThi) {
        this.tenHienThi = tenHienThi;
    }

    public StringFilter getMaDungChung() {
        return maDungChung;
    }

    public void setMaDungChung(StringFilter maDungChung) {
        this.maDungChung = maDungChung;
    }

    public StringFilter getMaNoiBo() {
        return maNoiBo;
    }

    public void setMaNoiBo(StringFilter maNoiBo) {
        this.maNoiBo = maNoiBo;
    }

    public IntegerFilter getPhamViChiDinh() {
        return phamViChiDinh;
    }

    public void setPhamViChiDinh(IntegerFilter phamViChiDinh) {
        this.phamViChiDinh = phamViChiDinh;
    }

    public StringFilter getDonViTinh() {
        return donViTinh;
    }

    public void setDonViTinh(StringFilter donViTinh) {
        this.donViTinh = donViTinh;
    }

    public BigDecimalFilter getDonGiaBenhVien() {
        return donGiaBenhVien;
    }

    public void setDonGiaBenhVien(BigDecimalFilter donGiaBenhVien) {
        this.donGiaBenhVien = donGiaBenhVien;
    }

    public LongFilter getDotThayDoiMaDichVuId() {
        return dotThayDoiMaDichVuId;
    }

    public void setDotThayDoiMaDichVuId(LongFilter dotThayDoiMaDichVuId) {
        this.dotThayDoiMaDichVuId = dotThayDoiMaDichVuId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final DichVuKhamCriteria that = (DichVuKhamCriteria) o;
        return Objects.equals(id, that.id) &&
            Objects.equals(deleted, that.deleted) &&
            Objects.equals(enabled, that.enabled) &&
            Objects.equals(gioiHanChiDinh, that.gioiHanChiDinh) &&
            Objects.equals(phanTheoGioTinh, that.phanTheoGioTinh) &&
            Objects.equals(tenHienThi, that.tenHienThi) &&
            Objects.equals(maDungChung, that.maDungChung) &&
            Objects.equals(maNoiBo, that.maNoiBo) &&
            Objects.equals(phamViChiDinh, that.phamViChiDinh) &&
            Objects.equals(donViTinh, that.donViTinh) &&
            Objects.equals(donGiaBenhVien, that.donGiaBenhVien) &&
            Objects.equals(dotThayDoiMaDichVuId, that.dotThayDoiMaDichVuId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            deleted,
            enabled,
            gioiHanChiDinh,
            phanTheoGioTinh,
            tenHienThi,
            maDungChung,
            maNoiBo,
            phamViChiDinh,
            donViTinh,
            donGiaBenhVien,
            dotThayDoiMaDichVuId
        );
    }

    @Override
    public String toString() {
        return "DichVuKhamCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (deleted != null ? "deleted=" + deleted + ", " : "") +
            (enabled != null ? "enabled=" + enabled + ", " : "") +
            (gioiHanChiDinh != null ? "gioiHanChiDinh=" + gioiHanChiDinh + ", " : "") +
            (phanTheoGioTinh != null ? "phanTheoGioTinh=" + phanTheoGioTinh + ", " : "") +
            (tenHienThi != null ? "tenHienThi=" + tenHienThi + ", " : "") +
            (maDungChung != null ? "maDungChung=" + maDungChung + ", " : "") +
            (maNoiBo != null ? "maNoiBo=" + maNoiBo + ", " : "") +
            (phamViChiDinh != null ? "phamViChiDinh=" + phamViChiDinh + ", " : "") +
            (donViTinh != null ? "donViTinh=" + donViTinh + ", " : "") +
            (donGiaBenhVien != null ? "donGiaBenhVien=" + donGiaBenhVien + ", " : "") +
            (dotThayDoiMaDichVuId != null ? "dotThayDoiMaDichVuId=" + dotThayDoiMaDichVuId + ", " : "") +
            "}";
    }

}

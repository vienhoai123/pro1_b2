package vn.vnpt.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A DTO for the {@link vn.vnpt.domain.DichVuKhamApDung} entity.
 */
@ApiModel(description = "Danh mục dịch vụ khám đã được map với đợt thay đổi mã và thay đổi giá")
public class DichVuKhamApDungDTO implements Serializable {

    private Long id;

    /**
     * Trạng thái có hiệu lực: 1: Có. 0: Hết hiệu lực
     */
    @ApiModelProperty(value = "Trạng thái có hiệu lực: 1: Có. 0: Hết hiệu lực")
    private Integer enabled;

    @NotNull
    private BigDecimal giaBhyt;

    @NotNull
    private BigDecimal giaKhongBhyt;

    @Size(max = 255)
    private String maBaoCaoBhyt;

    @Size(max = 255)
    private String maBaoCaoByt;

    @Size(max = 255)
    private String soCongVanBhxh;

    @Size(max = 255)
    private String soQuyetDinh;

    @Size(max = 500)
    private String tenBaoCaoBhxh;

    @Size(max = 500)
    private String tenDichVuKhongBaoHiem;

    @NotNull
    private BigDecimal tienBenhNhanChi;

    @NotNull
    private BigDecimal tienBhxhChi;

    @NotNull
    private BigDecimal tienNgoaiBhyt;

    @NotNull
    private BigDecimal tongTienThanhToan;

    private Integer tyLeBhxhThanhToan;

    /**
     * Đối tượng đặc biệt có bảng giá khác. 0: Bình thường. 1: được quy định
     */
    @ApiModelProperty(value = "Đối tượng đặc biệt có bảng giá khác. 0: Bình thường. 1: được quy định")
    private Integer doiTuongDacBiet;

    /**
     * Nguồn chi bảo hiểm cho bệnh nhân: 1: BHYT. 2: Nhà nước. 3: Khác
     */
    @ApiModelProperty(value = "Nguồn chi bảo hiểm cho bệnh nhân: 1: BHYT. 2: Nhà nước. 3: Khác")
    private Integer nguonChi;

    private Long dotGiaDichVuBhxhId;

    private Long dichVuKhamId;

    public Long getId(){
        return this.id;
    }

    public void setId(Long id){
        this.id = id;
    }

    public Integer getEnabled() {
        return enabled;
    }

    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }

    public BigDecimal getGiaBhyt() {
        return giaBhyt;
    }

    public void setGiaBhyt(BigDecimal giaBhyt) {
        this.giaBhyt = giaBhyt;
    }

    public BigDecimal getGiaKhongBhyt() {
        return giaKhongBhyt;
    }

    public void setGiaKhongBhyt(BigDecimal giaKhongBhyt) {
        this.giaKhongBhyt = giaKhongBhyt;
    }

    public String getMaBaoCaoBhyt() {
        return maBaoCaoBhyt;
    }

    public void setMaBaoCaoBhyt(String maBaoCaoBhyt) {
        this.maBaoCaoBhyt = maBaoCaoBhyt;
    }

    public String getMaBaoCaoByt() {
        return maBaoCaoByt;
    }

    public void setMaBaoCaoByt(String maBaoCaoByt) {
        this.maBaoCaoByt = maBaoCaoByt;
    }

    public String getSoCongVanBhxh() {
        return soCongVanBhxh;
    }

    public void setSoCongVanBhxh(String soCongVanBhxh) {
        this.soCongVanBhxh = soCongVanBhxh;
    }

    public String getSoQuyetDinh() {
        return soQuyetDinh;
    }

    public void setSoQuyetDinh(String soQuyetDinh) {
        this.soQuyetDinh = soQuyetDinh;
    }

    public String getTenBaoCaoBhxh() {
        return tenBaoCaoBhxh;
    }

    public void setTenBaoCaoBhxh(String tenBaoCaoBhxh) {
        this.tenBaoCaoBhxh = tenBaoCaoBhxh;
    }

    public String getTenDichVuKhongBaoHiem() {
        return tenDichVuKhongBaoHiem;
    }

    public void setTenDichVuKhongBaoHiem(String tenDichVuKhongBaoHiem) {
        this.tenDichVuKhongBaoHiem = tenDichVuKhongBaoHiem;
    }

    public BigDecimal getTienBenhNhanChi() {
        return tienBenhNhanChi;
    }

    public void setTienBenhNhanChi(BigDecimal tienBenhNhanChi) {
        this.tienBenhNhanChi = tienBenhNhanChi;
    }

    public BigDecimal getTienBhxhChi() {
        return tienBhxhChi;
    }

    public void setTienBhxhChi(BigDecimal tienBhxhChi) {
        this.tienBhxhChi = tienBhxhChi;
    }

    public BigDecimal getTienNgoaiBhyt() {
        return tienNgoaiBhyt;
    }

    public void setTienNgoaiBhyt(BigDecimal tienNgoaiBhyt) {
        this.tienNgoaiBhyt = tienNgoaiBhyt;
    }

    public BigDecimal getTongTienThanhToan() {
        return tongTienThanhToan;
    }

    public void setTongTienThanhToan(BigDecimal tongTienThanhToan) {
        this.tongTienThanhToan = tongTienThanhToan;
    }

    public Integer getTyLeBhxhThanhToan() {
        return tyLeBhxhThanhToan;
    }

    public void setTyLeBhxhThanhToan(Integer tyLeBhxhThanhToan) {
        this.tyLeBhxhThanhToan = tyLeBhxhThanhToan;
    }

    public Integer getDoiTuongDacBiet() {
        return doiTuongDacBiet;
    }

    public void setDoiTuongDacBiet(Integer doiTuongDacBiet) {
        this.doiTuongDacBiet = doiTuongDacBiet;
    }

    public Integer getNguonChi() {
        return nguonChi;
    }

    public void setNguonChi(Integer nguonChi) {
        this.nguonChi = nguonChi;
    }

    public Long getDotGiaDichVuBhxhId() {
        return dotGiaDichVuBhxhId;
    }

    public void setDotGiaDichVuBhxhId(Long dotGiaDichVuBhxhId) {
        this.dotGiaDichVuBhxhId = dotGiaDichVuBhxhId;
    }

    public Long getDichVuKhamId() {
        return dichVuKhamId;
    }

    public void setDichVuKhamId(Long dichVuKhamId) {
        this.dichVuKhamId = dichVuKhamId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DichVuKhamApDungDTO dichVuKhamApDungDTO = (DichVuKhamApDungDTO) o;
        if (dichVuKhamApDungDTO.getId() == null && getId() == null){
            return false;
        }
        return Objects.equals(getId(), dichVuKhamApDungDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "DichVuKhamApDungDTO{" +
            ", id=" + getId() +
            ", enabled=" + getEnabled() +
            ", giaBhyt=" + getGiaBhyt() +
            ", giaKhongBhyt=" + getGiaKhongBhyt() +
            ", maBaoCaoBhyt='" + getMaBaoCaoBhyt() + "'" +
            ", maBaoCaoByt='" + getMaBaoCaoByt() + "'" +
            ", soCongVanBhxh='" + getSoCongVanBhxh() + "'" +
            ", soQuyetDinh='" + getSoQuyetDinh() + "'" +
            ", tenBaoCaoBhxh='" + getTenBaoCaoBhxh() + "'" +
            ", tenDichVuKhongBaoHiem='" + getTenDichVuKhongBaoHiem() + "'" +
            ", tienBenhNhanChi=" + getTienBenhNhanChi() +
            ", tienBhxhChi=" + getTienBhxhChi() +
            ", tienNgoaiBhyt=" + getTienNgoaiBhyt() +
            ", tongTienThanhToan=" + getTongTienThanhToan() +
            ", tyLeBhxhThanhToan=" + getTyLeBhxhThanhToan() +
            ", doiTuongDacBiet=" + getDoiTuongDacBiet() +
            ", nguonChi=" + getNguonChi() +
            ", dotGiaDichVuBhxhId='" + getDotGiaDichVuBhxhId() + "'" +
            ", dichVuKhamId='" + getDichVuKhamId() + "'" +
            "}";
    }
}

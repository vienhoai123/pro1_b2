package vn.vnpt.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link vn.vnpt.domain.DotDieuTri} entity. This class is used
 * in {@link vn.vnpt.web.rest.DotDieuTriResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /dot-dieu-tris?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class DotDieuTriCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter bakbId;

    private LongFilter benhNhanId;

    private LongFilter donViId;

    private StringFilter soThuTu;

    private IntegerFilter bhytCanTren;

    private IntegerFilter bhytCanTrenKtc;

    private StringFilter bhytMaKhuVuc;

    private LocalDateFilter bhytNgayBatDau;

    private LocalDateFilter bhytNgayDuNamNam;

    private LocalDateFilter bhytNgayHetHan;

    private BooleanFilter bhytNoiTinh;

    private StringFilter bhytSoThe;

    private IntegerFilter bhytTyLeMienGiam;

    private IntegerFilter bhytTyLeMienGiamKtc;

    private BooleanFilter coBaoHiem;

    private StringFilter bhytDiaChi;

    private StringFilter doiTuongBhytTen;

    private BooleanFilter dungTuyen;

    private StringFilter giayToTreEm;

    private IntegerFilter loaiGiayToTreEm;

    private LocalDateFilter ngayMienCungChiTra;

    private BooleanFilter theTreEm;

    private BooleanFilter thongTuyenBhxhXml4210;

    private IntegerFilter trangThai;

    private IntegerFilter loai;

    private StringFilter bhytNoiDkKcbbd;

    private IntegerFilter nam;

    public DotDieuTriCriteria() {
    }

    public DotDieuTriCriteria(DotDieuTriCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.bakbId = other.bakbId == null ? null : other.bakbId.copy();
        this.benhNhanId = other.benhNhanId == null ? null : other.benhNhanId.copy();
        this.donViId = other.donViId == null ? null : other.donViId.copy();
        this.soThuTu = other.soThuTu == null ? null : other.soThuTu.copy();
        this.bhytCanTren = other.bhytCanTren == null ? null : other.bhytCanTren.copy();
        this.bhytCanTrenKtc = other.bhytCanTrenKtc == null ? null : other.bhytCanTrenKtc.copy();
        this.bhytMaKhuVuc = other.bhytMaKhuVuc == null ? null : other.bhytMaKhuVuc.copy();
        this.bhytNgayBatDau = other.bhytNgayBatDau == null ? null : other.bhytNgayBatDau.copy();
        this.bhytNgayDuNamNam = other.bhytNgayDuNamNam == null ? null : other.bhytNgayDuNamNam.copy();
        this.bhytNgayHetHan = other.bhytNgayHetHan == null ? null : other.bhytNgayHetHan.copy();
        this.bhytNoiTinh = other.bhytNoiTinh == null ? null : other.bhytNoiTinh.copy();
        this.bhytSoThe = other.bhytSoThe == null ? null : other.bhytSoThe.copy();
        this.bhytTyLeMienGiam = other.bhytTyLeMienGiam == null ? null : other.bhytTyLeMienGiam.copy();
        this.bhytTyLeMienGiamKtc = other.bhytTyLeMienGiamKtc == null ? null : other.bhytTyLeMienGiamKtc.copy();
        this.coBaoHiem = other.coBaoHiem == null ? null : other.coBaoHiem.copy();
        this.bhytDiaChi = other.bhytDiaChi == null ? null : other.bhytDiaChi.copy();
        this.doiTuongBhytTen = other.doiTuongBhytTen == null ? null : other.doiTuongBhytTen.copy();
        this.dungTuyen = other.dungTuyen == null ? null : other.dungTuyen.copy();
        this.giayToTreEm = other.giayToTreEm == null ? null : other.giayToTreEm.copy();
        this.loaiGiayToTreEm = other.loaiGiayToTreEm == null ? null : other.loaiGiayToTreEm.copy();
        this.ngayMienCungChiTra = other.ngayMienCungChiTra == null ? null : other.ngayMienCungChiTra.copy();
        this.theTreEm = other.theTreEm == null ? null : other.theTreEm.copy();
        this.thongTuyenBhxhXml4210 = other.thongTuyenBhxhXml4210 == null ? null : other.thongTuyenBhxhXml4210.copy();
        this.trangThai = other.trangThai == null ? null : other.trangThai.copy();
        this.loai = other.loai == null ? null : other.loai.copy();
        this.bhytNoiDkKcbbd = other.bhytNoiDkKcbbd == null ? null : other.bhytNoiDkKcbbd.copy();
        this.nam = other.nam == null ? null : other.nam.copy();
    }

    @Override
    public DotDieuTriCriteria copy() {
        return new DotDieuTriCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getBakbId() {
        return bakbId;
    }

    public void setBakbId(LongFilter bakbId) {
        this.bakbId = bakbId;
    }

    public LongFilter getBenhNhanId() {
        return benhNhanId;
    }

    public void setBenhNhanId(LongFilter benhNhanId) {
        benhNhanId = benhNhanId;
    }

    public LongFilter getDonViId() {
        return donViId;
    }

    public void setDonViId(LongFilter donViId) {
        donViId = donViId;
    }

    public StringFilter getSoThuTu() {
        return soThuTu;
    }

    public void setSoThuTu(StringFilter soThuTu) {
        this.soThuTu = soThuTu;
    }

    public IntegerFilter getBhytCanTren() {
        return bhytCanTren;
    }

    public void setBhytCanTren(IntegerFilter bhytCanTren) {
        this.bhytCanTren = bhytCanTren;
    }

    public IntegerFilter getBhytCanTrenKtc() {
        return bhytCanTrenKtc;
    }

    public void setBhytCanTrenKtc(IntegerFilter bhytCanTrenKtc) {
        this.bhytCanTrenKtc = bhytCanTrenKtc;
    }

    public StringFilter getBhytMaKhuVuc() {
        return bhytMaKhuVuc;
    }

    public void setBhytMaKhuVuc(StringFilter bhytMaKhuVuc) {
        this.bhytMaKhuVuc = bhytMaKhuVuc;
    }

    public LocalDateFilter getBhytNgayBatDau() {
        return bhytNgayBatDau;
    }

    public void setBhytNgayBatDau(LocalDateFilter bhytNgayBatDau) {
        this.bhytNgayBatDau = bhytNgayBatDau;
    }

    public LocalDateFilter getBhytNgayDuNamNam() {
        return bhytNgayDuNamNam;
    }

    public void setBhytNgayDuNamNam(LocalDateFilter bhytNgayDuNamNam) {
        this.bhytNgayDuNamNam = bhytNgayDuNamNam;
    }

    public LocalDateFilter getBhytNgayHetHan() {
        return bhytNgayHetHan;
    }

    public void setBhytNgayHetHan(LocalDateFilter bhytNgayHetHan) {
        this.bhytNgayHetHan = bhytNgayHetHan;
    }

    public BooleanFilter getBhytNoiTinh() {
        return bhytNoiTinh;
    }

    public void setBhytNoiTinh(BooleanFilter bhytNoiTinh) {
        this.bhytNoiTinh = bhytNoiTinh;
    }

    public StringFilter getBhytSoThe() {
        return bhytSoThe;
    }

    public void setBhytSoThe(StringFilter bhytSoThe) {
        this.bhytSoThe = bhytSoThe;
    }

    public IntegerFilter getBhytTyLeMienGiam() {
        return bhytTyLeMienGiam;
    }

    public void setBhytTyLeMienGiam(IntegerFilter bhytTyLeMienGiam) {
        this.bhytTyLeMienGiam = bhytTyLeMienGiam;
    }

    public IntegerFilter getBhytTyLeMienGiamKtc() {
        return bhytTyLeMienGiamKtc;
    }

    public void setBhytTyLeMienGiamKtc(IntegerFilter bhytTyLeMienGiamKtc) {
        this.bhytTyLeMienGiamKtc = bhytTyLeMienGiamKtc;
    }

    public BooleanFilter getCoBaoHiem() {
        return coBaoHiem;
    }

    public void setCoBaoHiem(BooleanFilter coBaoHiem) {
        this.coBaoHiem = coBaoHiem;
    }

    public StringFilter getBhytDiaChi() {
        return bhytDiaChi;
    }

    public void setBhytDiaChi(StringFilter bhytDiaChi) {
        this.bhytDiaChi = bhytDiaChi;
    }

    public StringFilter getDoiTuongBhytTen() {
        return doiTuongBhytTen;
    }

    public void setDoiTuongBhytTen(StringFilter doiTuongBhytTen) {
        this.doiTuongBhytTen = doiTuongBhytTen;
    }

    public BooleanFilter getDungTuyen() {
        return dungTuyen;
    }

    public void setDungTuyen(BooleanFilter dungTuyen) {
        this.dungTuyen = dungTuyen;
    }

    public StringFilter getGiayToTreEm() {
        return giayToTreEm;
    }

    public void setGiayToTreEm(StringFilter giayToTreEm) {
        this.giayToTreEm = giayToTreEm;
    }

    public IntegerFilter getLoaiGiayToTreEm() {
        return loaiGiayToTreEm;
    }

    public void setLoaiGiayToTreEm(IntegerFilter loaiGiayToTreEm) {
        this.loaiGiayToTreEm = loaiGiayToTreEm;
    }

    public LocalDateFilter getNgayMienCungChiTra() {
        return ngayMienCungChiTra;
    }

    public void setNgayMienCungChiTra(LocalDateFilter ngayMienCungChiTra) {
        this.ngayMienCungChiTra = ngayMienCungChiTra;
    }

    public BooleanFilter getTheTreEm() {
        return theTreEm;
    }

    public void setTheTreEm(BooleanFilter theTreEm) {
        this.theTreEm = theTreEm;
    }

    public BooleanFilter getThongTuyenBhxhXml4210() {
        return thongTuyenBhxhXml4210;
    }

    public void setThongTuyenBhxhXml4210(BooleanFilter thongTuyenBhxhXml4210) {
        this.thongTuyenBhxhXml4210 = thongTuyenBhxhXml4210;
    }

    public IntegerFilter getTrangThai() {
        return trangThai;
    }

    public void setTrangThai(IntegerFilter trangThai) {
        this.trangThai = trangThai;
    }

    public IntegerFilter getLoai() {
        return loai;
    }

    public void setLoai(IntegerFilter loai) {
        this.loai = loai;
    }

    public StringFilter getBhytNoiDkKcbbd() {
        return bhytNoiDkKcbbd;
    }

    public void setBhytNoiDkKcbbd(StringFilter bhytNoiDkKcbbd) {
        this.bhytNoiDkKcbbd = bhytNoiDkKcbbd;
    }

    public IntegerFilter getNam() {
        return nam;
    }

    public void setNam(IntegerFilter nam) {
        this.nam = nam;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final DotDieuTriCriteria that = (DotDieuTriCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(bakbId, that.bakbId) &&
            Objects.equals(benhNhanId, that.benhNhanId) &&
            Objects.equals(donViId, that.donViId) &&
            Objects.equals(soThuTu, that.soThuTu) &&
            Objects.equals(bhytCanTren, that.bhytCanTren) &&
            Objects.equals(bhytCanTrenKtc, that.bhytCanTrenKtc) &&
            Objects.equals(bhytMaKhuVuc, that.bhytMaKhuVuc) &&
            Objects.equals(bhytNgayBatDau, that.bhytNgayBatDau) &&
            Objects.equals(bhytNgayDuNamNam, that.bhytNgayDuNamNam) &&
            Objects.equals(bhytNgayHetHan, that.bhytNgayHetHan) &&
            Objects.equals(bhytNoiTinh, that.bhytNoiTinh) &&
            Objects.equals(bhytSoThe, that.bhytSoThe) &&
            Objects.equals(bhytTyLeMienGiam, that.bhytTyLeMienGiam) &&
            Objects.equals(bhytTyLeMienGiamKtc, that.bhytTyLeMienGiamKtc) &&
            Objects.equals(coBaoHiem, that.coBaoHiem) &&
            Objects.equals(bhytDiaChi, that.bhytDiaChi) &&
            Objects.equals(doiTuongBhytTen, that.doiTuongBhytTen) &&
            Objects.equals(dungTuyen, that.dungTuyen) &&
            Objects.equals(giayToTreEm, that.giayToTreEm) &&
            Objects.equals(loaiGiayToTreEm, that.loaiGiayToTreEm) &&
            Objects.equals(ngayMienCungChiTra, that.ngayMienCungChiTra) &&
            Objects.equals(theTreEm, that.theTreEm) &&
            Objects.equals(thongTuyenBhxhXml4210, that.thongTuyenBhxhXml4210) &&
            Objects.equals(trangThai, that.trangThai) &&
            Objects.equals(loai, that.loai) &&
            Objects.equals(bhytNoiDkKcbbd, that.bhytNoiDkKcbbd) &&
            Objects.equals(nam, that.nam);
    }

    @Override
    public int hashCode() {
        return Objects.hash
            (id,
            bakbId,
            benhNhanId,
            donViId,
            soThuTu,
            bhytCanTren,
            bhytCanTrenKtc,
            bhytMaKhuVuc,
            bhytNgayBatDau,
            bhytNgayDuNamNam,
            bhytNgayHetHan,
            bhytNoiTinh,
            bhytSoThe,
            bhytTyLeMienGiam,
            bhytTyLeMienGiamKtc,
            coBaoHiem,
            bhytDiaChi,
            doiTuongBhytTen,
            dungTuyen,
            giayToTreEm,
            loaiGiayToTreEm,
            ngayMienCungChiTra,
            theTreEm,
            thongTuyenBhxhXml4210,
            trangThai,
            loai,
            bhytNoiDkKcbbd,
            nam
        );
    }

    @Override
    public String toString() {
        return "DotDieuTriCriteria{" +
            "id=" + id +
            ", bakbId=" + bakbId +
            ", bakbBenhNhanId=" + benhNhanId +
            ", bakbDonViId=" + donViId +
            ", soThuTu=" + soThuTu +
            ", bhytCanTren=" + bhytCanTren +
            ", bhytCanTrenKtc=" + bhytCanTrenKtc +
            ", bhytMaKhuVuc=" + bhytMaKhuVuc +
            ", bhytNgayBatDau=" + bhytNgayBatDau +
            ", bhytNgayDuNamNam=" + bhytNgayDuNamNam +
            ", bhytNgayHetHan=" + bhytNgayHetHan +
            ", bhytNoiTinh=" + bhytNoiTinh +
            ", bhytSoThe=" + bhytSoThe +
            ", bhytTyLeMienGiam=" + bhytTyLeMienGiam +
            ", bhytTyLeMienGiamKtc=" + bhytTyLeMienGiamKtc +
            ", coBaoHiem=" + coBaoHiem +
            ", bhytDiaChi=" + bhytDiaChi +
            ", doiTuongBhytTen=" + doiTuongBhytTen +
            ", dungTuyen=" + dungTuyen +
            ", giayToTreEm=" + giayToTreEm +
            ", loaiGiayToTreEm=" + loaiGiayToTreEm +
            ", ngayMienCungChiTra=" + ngayMienCungChiTra +
            ", theTreEm=" + theTreEm +
            ", thongTuyenBhxhXml4210=" + thongTuyenBhxhXml4210 +
            ", trangThai=" + trangThai +
            ", loai=" + loai +
            ", bhytNoiDkKcbbd=" + bhytNoiDkKcbbd +
            ", nam=" + nam +
            '}';
    }


}

package vn.vnpt.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link vn.vnpt.domain.QuanHuyen} entity. This class is used
 * in {@link vn.vnpt.web.rest.QuanHuyenResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /quan-huyens?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class QuanHuyenCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter cap;

    private StringFilter guessPhrase;

    private StringFilter ten;

    private StringFilter tenKhongDau;

    private LongFilter tinhThanhPhoId;

    public QuanHuyenCriteria() {
    }

    public QuanHuyenCriteria(QuanHuyenCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.cap = other.cap == null ? null : other.cap.copy();
        this.guessPhrase = other.guessPhrase == null ? null : other.guessPhrase.copy();
        this.ten = other.ten == null ? null : other.ten.copy();
        this.tenKhongDau = other.tenKhongDau == null ? null : other.tenKhongDau.copy();
        this.tinhThanhPhoId = other.tinhThanhPhoId == null ? null : other.tinhThanhPhoId.copy();
    }

    @Override
    public QuanHuyenCriteria copy() {
        return new QuanHuyenCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCap() {
        return cap;
    }

    public void setCap(StringFilter cap) {
        this.cap = cap;
    }

    public StringFilter getGuessPhrase() {
        return guessPhrase;
    }

    public void setGuessPhrase(StringFilter guessPhrase) {
        this.guessPhrase = guessPhrase;
    }

    public StringFilter getTen() {
        return ten;
    }

    public void setTen(StringFilter ten) {
        this.ten = ten;
    }

    public StringFilter getTenKhongDau() {
        return tenKhongDau;
    }

    public void setTenKhongDau(StringFilter tenKhongDau) {
        this.tenKhongDau = tenKhongDau;
    }

    public LongFilter getTinhThanhPhoId() {
        return tinhThanhPhoId;
    }

    public void setTinhThanhPhoId(LongFilter tinhThanhPhoId) {
        this.tinhThanhPhoId = tinhThanhPhoId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final QuanHuyenCriteria that = (QuanHuyenCriteria) o;
        return
            Objects.equals(id, that.id) &&
                Objects.equals(cap, that.cap) &&
                Objects.equals(guessPhrase, that.guessPhrase) &&
                Objects.equals(ten, that.ten) &&
                Objects.equals(tenKhongDau, that.tenKhongDau) &&
                Objects.equals(tinhThanhPhoId, that.tinhThanhPhoId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        cap,
        guessPhrase,
        ten,
        tenKhongDau,
            tinhThanhPhoId
        );
    }

    @Override
    public String toString() {
        return "QuanHuyenCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (cap != null ? "cap=" + cap + ", " : "") +
                (guessPhrase != null ? "guessPhrase=" + guessPhrase + ", " : "") +
                (ten != null ? "ten=" + ten + ", " : "") +
                (tenKhongDau != null ? "tenKhongDau=" + tenKhongDau + ", " : "") +
                (tinhThanhPhoId != null ? "tinhThanhPhoId=" + tinhThanhPhoId + ", " : "") +
            "}";
    }

}

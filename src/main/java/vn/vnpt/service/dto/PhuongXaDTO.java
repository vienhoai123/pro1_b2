package vn.vnpt.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link vn.vnpt.domain.PhuongXa} entity.
 */
@ApiModel(description = "NDB_TABLE=READ_BACKUP=1 Thông tin quận huyện")
public class PhuongXaDTO implements Serializable {

    private Long id;

    /**
     * Cấp
     */
    @Size(max = 255)
    @ApiModelProperty(value = "Cấp")
    private String cap;

    /**
     * Đoạn ký tự gợi ý
     */
    @Size(max = 255)
    @ApiModelProperty(value = "Đoạn ký tự gợi ý")
    private String guessPhrase;

    /**
     * Tên xã
     */
    @NotNull
    @Size(max = 255)
    @ApiModelProperty(value = "Tên xã", required = true)
    private String ten;

    /**
     * Tên không dấu
     */
    @NotNull
    @Size(max = 255)
    @ApiModelProperty(value = "Tên không dấu", required = true)
    private String tenKhongDau;

    /**
     * Mã quận huyện
     */
    @ApiModelProperty(value = "Mã quận huyện")

    private Long quanHuyenId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCap() {
        return cap;
    }

    public void setCap(String cap) {
        this.cap = cap;
    }

    public String getGuessPhrase() {
        return guessPhrase;
    }

    public void setGuessPhrase(String guessPhrase) {
        this.guessPhrase = guessPhrase;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getTenKhongDau() {
        return tenKhongDau;
    }

    public void setTenKhongDau(String tenKhongDau) {
        this.tenKhongDau = tenKhongDau;
    }

    public Long getQuanHuyenId() {
        return quanHuyenId;
    }

    public void setQuanHuyenId(Long quanHuyenId) {
        this.quanHuyenId = quanHuyenId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PhuongXaDTO phuongXaDTO = (PhuongXaDTO) o;
        if (phuongXaDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), phuongXaDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PhuongXaDTO{" +
            "id=" + getId() +
            ", cap='" + getCap() + "'" +
            ", guessPhrase='" + getGuessPhrase() + "'" +
            ", ten='" + getTen() + "'" +
            ", tenKhongDau='" + getTenKhongDau() + "'" +
            ", quanHuyenId=" + getQuanHuyenId() +
            "}";
    }
}

package vn.vnpt.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link vn.vnpt.domain.BenhAnKhamBenh} entity. This class is used
 * in {@link vn.vnpt.web.rest.BenhAnKhamBenhResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /benh-an-kham-benhs?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class BenhAnKhamBenhCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter benhNhanId;

    private LongFilter donViId;

    private BooleanFilter benhNhanCu;

    private BooleanFilter canhBao;

    private BooleanFilter capCuu;

    private BooleanFilter chiCoNamSinh;

    private StringFilter cmndBenhNhan;

    private StringFilter cmndNguoiNha;

    private BooleanFilter coBaoHiem;

    private StringFilter diaChi;

    private StringFilter email;

    private IntegerFilter gioiTinh;

    private StringFilter khangThe;

    private IntegerFilter lanKhamTrongNgay;

    private IntegerFilter loaiGiayToTreEm;

    private LocalDateFilter ngayCapCmnd;

    private LocalDateFilter ngayMienCungChiTra;

    private LocalDateFilter ngaySinh;

    private StringFilter nhomMau;

    private StringFilter noiCapCmnd;

    private StringFilter noiLamViec;

    private StringFilter phone;

    private StringFilter phoneNguoiNha;

    private StringFilter quocTich;

    private StringFilter tenBenhNhan;

    private StringFilter tenNguoiNha;

    private IntegerFilter thangTuoi;

    private LocalDateFilter thoiGianTiepNhan;

    private IntegerFilter tuoi;

    private IntegerFilter uuTien;

    private StringFilter uuid;

    private IntegerFilter trangThai;

    private IntegerFilter maKhoaHoanTatKham;

    private IntegerFilter maPhongHoanTatKham;

    private StringFilter tenKhoaHoanTatKham;

    private StringFilter tenPhongHoanTatKham;

    private LocalDateFilter thoiGianHoanTatKham;

    private StringFilter soBenhAn;

    private StringFilter soBenhAnKhoa;

    private IntegerFilter nam;

    private LongFilter nhanVienTiepNhanId;

    private LongFilter phongTiepNhanId;

    public BenhAnKhamBenhCriteria() {
    }

    public BenhAnKhamBenhCriteria(BenhAnKhamBenhCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.benhNhanId = other.benhNhanId == null ? null : other.benhNhanId.copy();
        this.donViId = other.donViId == null ? null : other.donViId.copy();
        this.benhNhanCu = other.benhNhanCu == null ? null : other.benhNhanCu.copy();
        this.canhBao = other.canhBao == null ? null : other.canhBao.copy();
        this.capCuu = other.capCuu == null ? null : other.capCuu.copy();
        this.chiCoNamSinh = other.chiCoNamSinh == null ? null : other.chiCoNamSinh.copy();
        this.cmndBenhNhan = other.cmndBenhNhan == null ? null : other.cmndBenhNhan.copy();
        this.cmndNguoiNha = other.cmndNguoiNha == null ? null : other.cmndNguoiNha.copy();
        this.coBaoHiem = other.coBaoHiem == null ? null : other.coBaoHiem.copy();
        this.diaChi = other.diaChi == null ? null : other.diaChi.copy();
        this.email = other.email == null ? null : other.email.copy();
        this.gioiTinh = other.gioiTinh == null ? null : other.gioiTinh.copy();
        this.khangThe = other.khangThe == null ? null : other.khangThe.copy();
        this.lanKhamTrongNgay = other.lanKhamTrongNgay == null ? null : other.lanKhamTrongNgay.copy();
        this.loaiGiayToTreEm = other.loaiGiayToTreEm == null ? null : other.loaiGiayToTreEm.copy();
        this.ngayCapCmnd = other.ngayCapCmnd == null ? null : other.ngayCapCmnd.copy();
        this.ngayMienCungChiTra = other.ngayMienCungChiTra == null ? null : other.ngayMienCungChiTra.copy();
        this.ngaySinh = other.ngaySinh == null ? null : other.ngaySinh.copy();
        this.nhomMau = other.nhomMau == null ? null : other.nhomMau.copy();
        this.noiCapCmnd = other.noiCapCmnd == null ? null : other.noiCapCmnd.copy();
        this.noiLamViec = other.noiLamViec == null ? null : other.noiLamViec.copy();
        this.phone = other.phone == null ? null : other.phone.copy();
        this.phoneNguoiNha = other.phoneNguoiNha == null ? null : other.phoneNguoiNha.copy();
        this.quocTich = other.quocTich == null ? null : other.quocTich.copy();
        this.tenBenhNhan = other.tenBenhNhan == null ? null : other.tenBenhNhan.copy();
        this.tenNguoiNha = other.tenNguoiNha == null ? null : other.tenNguoiNha.copy();
        this.thangTuoi = other.thangTuoi == null ? null : other.thangTuoi.copy();
        this.thoiGianTiepNhan = other.thoiGianTiepNhan == null ? null : other.thoiGianTiepNhan.copy();
        this.tuoi = other.tuoi == null ? null : other.tuoi.copy();
        this.uuTien = other.uuTien == null ? null : other.uuTien.copy();
        this.uuid = other.uuid == null ? null : other.uuid.copy();
        this.trangThai = other.trangThai == null ? null : other.trangThai.copy();
        this.maKhoaHoanTatKham = other.maKhoaHoanTatKham == null ? null : other.maKhoaHoanTatKham.copy();
        this.maPhongHoanTatKham = other.maPhongHoanTatKham == null ? null : other.maPhongHoanTatKham.copy();
        this.tenKhoaHoanTatKham = other.tenKhoaHoanTatKham == null ? null : other.tenKhoaHoanTatKham.copy();
        this.tenPhongHoanTatKham = other.tenPhongHoanTatKham == null ? null : other.tenPhongHoanTatKham.copy();
        this.thoiGianHoanTatKham = other.thoiGianHoanTatKham == null ? null : other.thoiGianHoanTatKham.copy();
        this.soBenhAn = other.soBenhAn == null ? null : other.soBenhAn.copy();
        this.soBenhAnKhoa = other.soBenhAnKhoa == null ? null : other.soBenhAnKhoa.copy();
        this.nam = other.nam == null ? null : other.nam.copy();
        this.nhanVienTiepNhanId = other.nhanVienTiepNhanId == null ? null : other.nhanVienTiepNhanId.copy();
        this.phongTiepNhanId = other.phongTiepNhanId == null ? null : other.phongTiepNhanId.copy();
    }

    @Override
    public BenhAnKhamBenhCriteria copy() {
        return new BenhAnKhamBenhCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getBenhNhanId() {
        return benhNhanId;
    }

    public void setBenhNhanId(LongFilter benhNhanId) {
        this.benhNhanId = benhNhanId;
    }

    public LongFilter getDonViId() {
        return donViId;
    }

    public void setDonViId(LongFilter donViId) {
        this.donViId = donViId;
    }

    public BooleanFilter getBenhNhanCu() {
        return benhNhanCu;
    }

    public void setBenhNhanCu(BooleanFilter benhNhanCu) {
        this.benhNhanCu = benhNhanCu;
    }

    public BooleanFilter getCanhBao() {
        return canhBao;
    }

    public void setCanhBao(BooleanFilter canhBao) {
        this.canhBao = canhBao;
    }

    public BooleanFilter getCapCuu() {
        return capCuu;
    }

    public void setCapCuu(BooleanFilter capCuu) {
        this.capCuu = capCuu;
    }

    public BooleanFilter getChiCoNamSinh() {
        return chiCoNamSinh;
    }

    public void setChiCoNamSinh(BooleanFilter chiCoNamSinh) {
        this.chiCoNamSinh = chiCoNamSinh;
    }

    public StringFilter getCmndBenhNhan() {
        return cmndBenhNhan;
    }

    public void setCmndBenhNhan(StringFilter cmndBenhNhan) {
        this.cmndBenhNhan = cmndBenhNhan;
    }

    public StringFilter getCmndNguoiNha() {
        return cmndNguoiNha;
    }

    public void setCmndNguoiNha(StringFilter cmndNguoiNha) {
        this.cmndNguoiNha = cmndNguoiNha;
    }

    public BooleanFilter getCoBaoHiem() {
        return coBaoHiem;
    }

    public void setCoBaoHiem(BooleanFilter coBaoHiem) {
        this.coBaoHiem = coBaoHiem;
    }

    public StringFilter getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(StringFilter diaChi) {
        this.diaChi = diaChi;
    }

    public StringFilter getEmail() {
        return email;
    }

    public void setEmail(StringFilter email) {
        this.email = email;
    }

    public IntegerFilter getGioiTinh() {
        return gioiTinh;
    }

    public void setGioiTinh(IntegerFilter gioiTinh) {
        this.gioiTinh = gioiTinh;
    }

    public StringFilter getKhangThe() {
        return khangThe;
    }

    public void setKhangThe(StringFilter khangThe) {
        this.khangThe = khangThe;
    }

    public IntegerFilter getLanKhamTrongNgay() {
        return lanKhamTrongNgay;
    }

    public void setLanKhamTrongNgay(IntegerFilter lanKhamTrongNgay) {
        this.lanKhamTrongNgay = lanKhamTrongNgay;
    }

    public IntegerFilter getLoaiGiayToTreEm() {
        return loaiGiayToTreEm;
    }

    public void setLoaiGiayToTreEm(IntegerFilter loaiGiayToTreEm) {
        this.loaiGiayToTreEm = loaiGiayToTreEm;
    }

    public LocalDateFilter getNgayCapCmnd() {
        return ngayCapCmnd;
    }

    public void setNgayCapCmnd(LocalDateFilter ngayCapCmnd) {
        this.ngayCapCmnd = ngayCapCmnd;
    }

    public LocalDateFilter getNgayMienCungChiTra() {
        return ngayMienCungChiTra;
    }

    public void setNgayMienCungChiTra(LocalDateFilter ngayMienCungChiTra) {
        this.ngayMienCungChiTra = ngayMienCungChiTra;
    }

    public LocalDateFilter getNgaySinh() {
        return ngaySinh;
    }

    public void setNgaySinh(LocalDateFilter ngaySinh) {
        this.ngaySinh = ngaySinh;
    }

    public StringFilter getNhomMau() {
        return nhomMau;
    }

    public void setNhomMau(StringFilter nhomMau) {
        this.nhomMau = nhomMau;
    }

    public StringFilter getNoiCapCmnd() {
        return noiCapCmnd;
    }

    public void setNoiCapCmnd(StringFilter noiCapCmnd) {
        this.noiCapCmnd = noiCapCmnd;
    }

    public StringFilter getNoiLamViec() {
        return noiLamViec;
    }

    public void setNoiLamViec(StringFilter noiLamViec) {
        this.noiLamViec = noiLamViec;
    }

    public StringFilter getPhone() {
        return phone;
    }

    public void setPhone(StringFilter phone) {
        this.phone = phone;
    }

    public StringFilter getPhoneNguoiNha() {
        return phoneNguoiNha;
    }

    public void setPhoneNguoiNha(StringFilter phoneNguoiNha) {
        this.phoneNguoiNha = phoneNguoiNha;
    }

    public StringFilter getQuocTich() {
        return quocTich;
    }

    public void setQuocTich(StringFilter quocTich) {
        this.quocTich = quocTich;
    }

    public StringFilter getTenBenhNhan() {
        return tenBenhNhan;
    }

    public void setTenBenhNhan(StringFilter tenBenhNhan) {
        this.tenBenhNhan = tenBenhNhan;
    }

    public StringFilter getTenNguoiNha() {
        return tenNguoiNha;
    }

    public void setTenNguoiNha(StringFilter tenNguoiNha) {
        this.tenNguoiNha = tenNguoiNha;
    }

    public IntegerFilter getThangTuoi() {
        return thangTuoi;
    }

    public void setThangTuoi(IntegerFilter thangTuoi) {
        this.thangTuoi = thangTuoi;
    }

    public LocalDateFilter getThoiGianTiepNhan() {
        return thoiGianTiepNhan;
    }

    public void setThoiGianTiepNhan(LocalDateFilter thoiGianTiepNhan) {
        this.thoiGianTiepNhan = thoiGianTiepNhan;
    }

    public IntegerFilter getTuoi() {
        return tuoi;
    }

    public void setTuoi(IntegerFilter tuoi) {
        this.tuoi = tuoi;
    }

    public IntegerFilter getUuTien() {
        return uuTien;
    }

    public void setUuTien(IntegerFilter uuTien) {
        this.uuTien = uuTien;
    }

    public StringFilter getUuid() {
        return uuid;
    }

    public void setUuid(StringFilter uuid) {
        this.uuid = uuid;
    }

    public IntegerFilter getTrangThai() {
        return trangThai;
    }

    public void setTrangThai(IntegerFilter trangThai) {
        this.trangThai = trangThai;
    }

    public IntegerFilter getMaKhoaHoanTatKham() {
        return maKhoaHoanTatKham;
    }

    public void setMaKhoaHoanTatKham(IntegerFilter maKhoaHoanTatKham) {
        this.maKhoaHoanTatKham = maKhoaHoanTatKham;
    }

    public IntegerFilter getMaPhongHoanTatKham() {
        return maPhongHoanTatKham;
    }

    public void setMaPhongHoanTatKham(IntegerFilter maPhongHoanTatKham) {
        this.maPhongHoanTatKham = maPhongHoanTatKham;
    }

    public StringFilter getTenKhoaHoanTatKham() {
        return tenKhoaHoanTatKham;
    }

    public void setTenKhoaHoanTatKham(StringFilter tenKhoaHoanTatKham) {
        this.tenKhoaHoanTatKham = tenKhoaHoanTatKham;
    }

    public StringFilter getTenPhongHoanTatKham() {
        return tenPhongHoanTatKham;
    }

    public void setTenPhongHoanTatKham(StringFilter tenPhongHoanTatKham) {
        this.tenPhongHoanTatKham = tenPhongHoanTatKham;
    }

    public LocalDateFilter getThoiGianHoanTatKham() {
        return thoiGianHoanTatKham;
    }

    public void setThoiGianHoanTatKham(LocalDateFilter thoiGianHoanTatKham) {
        this.thoiGianHoanTatKham = thoiGianHoanTatKham;
    }

    public StringFilter getSoBenhAn() {
        return soBenhAn;
    }

    public void setSoBenhAn(StringFilter soBenhAn) {
        this.soBenhAn = soBenhAn;
    }

    public StringFilter getSoBenhAnKhoa() {
        return soBenhAnKhoa;
    }

    public void setSoBenhAnKhoa(StringFilter soBenhAnKhoa) {
        this.soBenhAnKhoa = soBenhAnKhoa;
    }

    public IntegerFilter getNam() {
        return nam;
    }

    public void setNam(IntegerFilter nam) {
        this.nam = nam;
    }

    public LongFilter getNhanVienTiepNhanId() {
        return nhanVienTiepNhanId;
    }

    public void setNhanVienTiepNhanId(LongFilter nhanVienTiepNhanId) {
        this.nhanVienTiepNhanId = nhanVienTiepNhanId;
    }

    public LongFilter getPhongTiepNhanId() {
        return phongTiepNhanId;
    }

    public void setPhongTiepNhanId(LongFilter phongTiepNhanId) {
        this.phongTiepNhanId = phongTiepNhanId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final BenhAnKhamBenhCriteria that = (BenhAnKhamBenhCriteria) o;
        return Objects.equals(id, that.id) &&
            Objects.equals(benhNhanId, that.benhNhanId) &&
            Objects.equals(donViId, that.donViId) &&
            Objects.equals(benhNhanCu, that.benhNhanCu) &&
            Objects.equals(canhBao, that.canhBao) &&
            Objects.equals(capCuu, that.capCuu) &&
            Objects.equals(chiCoNamSinh, that.chiCoNamSinh) &&
            Objects.equals(cmndBenhNhan, that.cmndBenhNhan) &&
            Objects.equals(cmndNguoiNha, that.cmndNguoiNha) &&
            Objects.equals(coBaoHiem, that.coBaoHiem) &&
            Objects.equals(diaChi, that.diaChi) &&
            Objects.equals(email, that.email) &&
            Objects.equals(gioiTinh, that.gioiTinh) &&
            Objects.equals(khangThe, that.khangThe) &&
            Objects.equals(lanKhamTrongNgay, that.lanKhamTrongNgay) &&
            Objects.equals(loaiGiayToTreEm, that.loaiGiayToTreEm) &&
            Objects.equals(ngayCapCmnd, that.ngayCapCmnd) &&
            Objects.equals(ngayMienCungChiTra, that.ngayMienCungChiTra) &&
            Objects.equals(ngaySinh, that.ngaySinh) &&
            Objects.equals(nhomMau, that.nhomMau) &&
            Objects.equals(noiCapCmnd, that.noiCapCmnd) &&
            Objects.equals(noiLamViec, that.noiLamViec) &&
            Objects.equals(phone, that.phone) &&
            Objects.equals(phoneNguoiNha, that.phoneNguoiNha) &&
            Objects.equals(quocTich, that.quocTich) &&
            Objects.equals(tenBenhNhan, that.tenBenhNhan) &&
            Objects.equals(tenNguoiNha, that.tenNguoiNha) &&
            Objects.equals(thangTuoi, that.thangTuoi) &&
            Objects.equals(thoiGianTiepNhan, that.thoiGianTiepNhan) &&
            Objects.equals(tuoi, that.tuoi) &&
            Objects.equals(uuTien, that.uuTien) &&
            Objects.equals(uuid, that.uuid) &&
            Objects.equals(trangThai, that.trangThai) &&
            Objects.equals(maKhoaHoanTatKham, that.maKhoaHoanTatKham) &&
            Objects.equals(maPhongHoanTatKham, that.maPhongHoanTatKham) &&
            Objects.equals(tenKhoaHoanTatKham, that.tenKhoaHoanTatKham) &&
            Objects.equals(tenPhongHoanTatKham, that.tenPhongHoanTatKham) &&
            Objects.equals(thoiGianHoanTatKham, that.thoiGianHoanTatKham) &&
            Objects.equals(soBenhAn, that.soBenhAn) &&
            Objects.equals(soBenhAnKhoa, that.soBenhAnKhoa) &&
            Objects.equals(nam, that.nam) &&
            Objects.equals(nhanVienTiepNhanId, that.nhanVienTiepNhanId) &&
            Objects.equals(phongTiepNhanId, that.phongTiepNhanId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            benhNhanId,
            donViId,
            benhNhanCu,
            canhBao,
            capCuu,
            chiCoNamSinh,
            cmndBenhNhan,
            cmndNguoiNha,
            coBaoHiem,
            diaChi,
            email,
            gioiTinh,
            khangThe,
            lanKhamTrongNgay,
            loaiGiayToTreEm,
            ngayCapCmnd,
            ngayMienCungChiTra,
            ngaySinh,
            nhomMau,
            noiCapCmnd,
            noiLamViec,
            phone,
            phoneNguoiNha,
            quocTich,
            tenBenhNhan,
            tenNguoiNha,
            thangTuoi,
            thoiGianTiepNhan,
            tuoi,
            uuTien,
            uuid,
            trangThai,
            maKhoaHoanTatKham,
            maPhongHoanTatKham,
            tenKhoaHoanTatKham,
            tenPhongHoanTatKham,
            thoiGianHoanTatKham,
            soBenhAn,
            soBenhAnKhoa,
            nam,
            nhanVienTiepNhanId,
            phongTiepNhanId
        );
    }

    @Override
    public String toString() {
        return "BenhAnKhamBenhCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (benhNhanId != null ? "benhNhanId=" + benhNhanId + ", " : "") +
            (donViId != null ? "donViId=" + donViId + ", " : "") +
            (benhNhanCu != null ? "benhNhanCu=" + benhNhanCu + ", " : "") +
            (canhBao != null ? "canhBao=" + canhBao + ", " : "") +
            (capCuu != null ? "capCuu=" + capCuu + ", " : "") +
            (chiCoNamSinh != null ? "chiCoNamSinh=" + chiCoNamSinh + ", " : "") +
            (cmndBenhNhan != null ? "cmndBenhNhan=" + cmndBenhNhan + ", " : "") +
            (cmndNguoiNha != null ? "cmndNguoiNha=" + cmndNguoiNha + ", " : "") +
            (coBaoHiem != null ? "coBaoHiem=" + coBaoHiem + ", " : "") +
            (diaChi != null ? "diaChi=" + diaChi + ", " : "") +
            (email != null ? "email=" + email + ", " : "") +
            (gioiTinh != null ? "gioiTinh=" + gioiTinh + ", " : "") +
            (khangThe != null ? "khangThe=" + khangThe + ", " : "") +
            (lanKhamTrongNgay != null ? "lanKhamTrongNgay=" + lanKhamTrongNgay + ", " : "") +
            (loaiGiayToTreEm != null ? "loaiGiayToTreEm=" + loaiGiayToTreEm + ", " : "") +
            (ngayCapCmnd != null ? "ngayCapCmnd=" + ngayCapCmnd + ", " : "") +
            (ngayMienCungChiTra != null ? "ngayMienCungChiTra=" + ngayMienCungChiTra + ", " : "") +
            (ngaySinh != null ? "ngaySinh=" + ngaySinh + ", " : "") +
            (nhomMau != null ? "nhomMau=" + nhomMau + ", " : "") +
            (noiCapCmnd != null ? "noiCapCmnd=" + noiCapCmnd + ", " : "") +
            (noiLamViec != null ? "noiLamViec=" + noiLamViec + ", " : "") +
            (phone != null ? "phone=" + phone + ", " : "") +
            (phoneNguoiNha != null ? "phoneNguoiNha=" + phoneNguoiNha + ", " : "") +
            (quocTich != null ? "quocTich=" + quocTich + ", " : "") +
            (tenBenhNhan != null ? "tenBenhNhan=" + tenBenhNhan + ", " : "") +
            (tenNguoiNha != null ? "tenNguoiNha=" + tenNguoiNha + ", " : "") +
            (thangTuoi != null ? "thangTuoi=" + thangTuoi + ", " : "") +
            (thoiGianTiepNhan != null ? "thoiGianTiepNhan=" + thoiGianTiepNhan + ", " : "") +
            (tuoi != null ? "tuoi=" + tuoi + ", " : "") +
            (uuTien != null ? "uuTien=" + uuTien + ", " : "") +
            (uuid != null ? "uuid=" + uuid + ", " : "") +
            (trangThai != null ? "trangThai=" + trangThai + ", " : "") +
            (maKhoaHoanTatKham != null ? "maKhoaHoanTatKham=" + maKhoaHoanTatKham + ", " : "") +
            (maPhongHoanTatKham != null ? "maPhongHoanTatKham=" + maPhongHoanTatKham + ", " : "") +
            (tenKhoaHoanTatKham != null ? "tenKhoaHoanTatKham=" + tenKhoaHoanTatKham + ", " : "") +
            (tenPhongHoanTatKham != null ? "tenPhongHoanTatKham=" + tenPhongHoanTatKham + ", " : "") +
            (thoiGianHoanTatKham != null ? "thoiGianHoanTatKham=" + thoiGianHoanTatKham + ", " : "") +
            (soBenhAn != null ? "soBenhAn=" + soBenhAn + ", " : "") +
            (soBenhAnKhoa != null ? "soBenhAnKhoa=" + soBenhAnKhoa + ", " : "") +
            (nam != null ? "nam=" + nam + ", " : "") +
            (nhanVienTiepNhanId != null ? "nhanVienTiepNhanId=" + nhanVienTiepNhanId + ", " : "") +
            (phongTiepNhanId != null ? "phongTiepNhanId=" + phongTiepNhanId + ", " : "") +
            "}";
    }

}

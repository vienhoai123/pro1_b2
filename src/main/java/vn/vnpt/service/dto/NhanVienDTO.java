package vn.vnpt.service.dto;

import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A DTO for the {@link vn.vnpt.domain.NhanVien} entity.
 */
public class NhanVienDTO implements Serializable {
    
    private Long id;

    /**
     * Tên nhân vien
     */
    @NotNull
    @Size(max = 300)
    @ApiModelProperty(value = "Tên nhân vien", required = true)
    private String ten;

    /**
     * Chứng chỉ hành nghề
     */
    @NotNull
    @Size(max = 255)
    @ApiModelProperty(value = "Chứng chỉ hành nghề", required = true)
    private String chungChiHanhNghe;

    /**
     * CMND của nhân viên
     */
    @NotNull
    @Size(max = 30)
    @ApiModelProperty(value = "CMND của nhân viên", required = true)
    private String cmnd;

    /**
     * 0: Nữ. 1 Nam
     */
    @NotNull
    @ApiModelProperty(value = "0: Nữ. 1 Nam", required = true)
    private Integer gioiTinh;

    /**
     * Ngày sinh
     */
    @NotNull
    @ApiModelProperty(value = "Ngày sinh", required = true)
    private LocalDate ngaySinh;

    /**
     * Mã chức danh
     */
    @ApiModelProperty(value = "Mã chức danh")

    private Long chucDanhId;
    /**
     * Mã người dùng
     */
    @ApiModelProperty(value = "Mã người dùng")

    private Long userExtraId;
    private Set<ChucVuDTO> chucVus = new HashSet<>();
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getChungChiHanhNghe() {
        return chungChiHanhNghe;
    }

    public void setChungChiHanhNghe(String chungChiHanhNghe) {
        this.chungChiHanhNghe = chungChiHanhNghe;
    }

    public String getCmnd() {
        return cmnd;
    }

    public void setCmnd(String cmnd) {
        this.cmnd = cmnd;
    }

    public Integer getGioiTinh() {
        return gioiTinh;
    }

    public void setGioiTinh(Integer gioiTinh) {
        this.gioiTinh = gioiTinh;
    }

    public LocalDate getNgaySinh() {
        return ngaySinh;
    }

    public void setNgaySinh(LocalDate ngaySinh) {
        this.ngaySinh = ngaySinh;
    }

    public Long getChucDanhId() {
        return chucDanhId;
    }

    public void setChucDanhId(Long chucDanhId) {
        this.chucDanhId = chucDanhId;
    }

    public Long getUserExtraId() {
        return userExtraId;
    }

    public void setUserExtraId(Long userExtraId) {
        this.userExtraId = userExtraId;
    }

    public Set<ChucVuDTO> getChucVus() {
        return chucVus;
    }

    public void setChucVus(Set<ChucVuDTO> chucVus) {
        this.chucVus = chucVus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof NhanVienDTO)) {
            return false;
        }

        return id != null && id.equals(((NhanVienDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "NhanVienDTO{" +
            "id=" + getId() +
            ", ten='" + getTen() + "'" +
            ", chungChiHanhNghe='" + getChungChiHanhNghe() + "'" +
            ", cmnd='" + getCmnd() + "'" +
            ", gioiTinh=" + getGioiTinh() +
            ", ngaySinh='" + getNgaySinh() + "'" +
            ", chucDanhId=" + getChucDanhId() +
            ", userExtraId=" + getUserExtraId() +
            ", chucVus='" + getChucVus() + "'" +
            "}";
    }
}

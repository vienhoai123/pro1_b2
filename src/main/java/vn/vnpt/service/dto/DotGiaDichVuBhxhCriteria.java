package vn.vnpt.service.dto;

import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * Criteria class for the {@link vn.vnpt.domain.DotGiaDichVuBhxh} entity. This class is used
 * in {@link vn.vnpt.web.rest.DotGiaDichVuBhxhResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /dot-gia-dich-vu-bhxhs?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class DotGiaDichVuBhxhCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private BigDecimalFilter doiTuongApDung;

    private StringFilter ghiChu;

    private LocalDateFilter ngayApDung;

    private StringFilter ten;

    private LongFilter donViId;

    public DotGiaDichVuBhxhCriteria() {
    }

    public DotGiaDichVuBhxhCriteria(DotGiaDichVuBhxhCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.doiTuongApDung = other.doiTuongApDung == null ? null : other.doiTuongApDung.copy();
        this.ghiChu = other.ghiChu == null ? null : other.ghiChu.copy();
        this.ngayApDung = other.ngayApDung == null ? null : other.ngayApDung.copy();
        this.ten = other.ten == null ? null : other.ten.copy();
        this.donViId = other.donViId == null ? null : other.donViId.copy();
    }

    @Override
    public DotGiaDichVuBhxhCriteria copy() {
        return new DotGiaDichVuBhxhCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public BigDecimalFilter getDoiTuongApDung() {
        return doiTuongApDung;
    }

    public void setDoiTuongApDung(BigDecimalFilter doiTuongApDung) {
        this.doiTuongApDung = doiTuongApDung;
    }

    public StringFilter getGhiChu() {
        return ghiChu;
    }

    public void setGhiChu(StringFilter ghiChu) {
        this.ghiChu = ghiChu;
    }

    public LocalDateFilter getNgayApDung() {
        return ngayApDung;
    }

    public void setNgayApDung(LocalDateFilter ngayApDung) {
        this.ngayApDung = ngayApDung;
    }

    public StringFilter getTen() {
        return ten;
    }

    public void setTen(StringFilter ten) {
        this.ten = ten;
    }

    public LongFilter getDonViId() {
        return donViId;
    }

    public void setDonViId(LongFilter donViId) {
        this.donViId = donViId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final DotGiaDichVuBhxhCriteria that = (DotGiaDichVuBhxhCriteria) o;
        return
            Objects.equals(id, that.id) &&
                Objects.equals(doiTuongApDung, that.doiTuongApDung) &&
                Objects.equals(ghiChu, that.ghiChu) &&
                Objects.equals(ngayApDung, that.ngayApDung) &&
                Objects.equals(ten, that.ten) &&
                Objects.equals(donViId, that.donViId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            doiTuongApDung,
            ghiChu,
            ngayApDung,
            ten,
            donViId
        );
    }

    @Override
    public String toString() {
        return "DotGiaDichVuBhxhCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (doiTuongApDung != null ? "doiTuongApDung=" + doiTuongApDung + ", " : "") +
            (ghiChu != null ? "ghiChu=" + ghiChu + ", " : "") +
            (ngayApDung != null ? "ngayApDung=" + ngayApDung + ", " : "") +
            (ten != null ? "ten=" + ten + ", " : "") +
            (donViId != null ? "donViId=" + donViId + ", " : "") +
            "}";
    }

}

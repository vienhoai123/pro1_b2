package vn.vnpt.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link vn.vnpt.domain.DoiTuongBhyt} entity. This class is used
 * in {@link vn.vnpt.web.rest.DoiTuongBhytResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /doi-tuong-bhyts?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class DoiTuongBhytCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter canTren;

    private IntegerFilter canTrenKtc;

    private StringFilter ma;

    private StringFilter ten;

    private IntegerFilter tyLeMienGiam;

    private IntegerFilter tyLeMienGiamKtc;

    private LongFilter nhomDoiTuongBhytId;

    public DoiTuongBhytCriteria() {
    }

    public DoiTuongBhytCriteria(DoiTuongBhytCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.canTren = other.canTren == null ? null : other.canTren.copy();
        this.canTrenKtc = other.canTrenKtc == null ? null : other.canTrenKtc.copy();
        this.ma = other.ma == null ? null : other.ma.copy();
        this.ten = other.ten == null ? null : other.ten.copy();
        this.tyLeMienGiam = other.tyLeMienGiam == null ? null : other.tyLeMienGiam.copy();
        this.tyLeMienGiamKtc = other.tyLeMienGiamKtc == null ? null : other.tyLeMienGiamKtc.copy();
        this.nhomDoiTuongBhytId = other.nhomDoiTuongBhytId == null ? null : other.nhomDoiTuongBhytId.copy();
    }

    @Override
    public DoiTuongBhytCriteria copy() {
        return new DoiTuongBhytCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getCanTren() {
        return canTren;
    }

    public void setCanTren(IntegerFilter canTren) {
        this.canTren = canTren;
    }

    public IntegerFilter getCanTrenKtc() {
        return canTrenKtc;
    }

    public void setCanTrenKtc(IntegerFilter canTrenKtc) {
        this.canTrenKtc = canTrenKtc;
    }

    public StringFilter getMa() {
        return ma;
    }

    public void setMa(StringFilter ma) {
        this.ma = ma;
    }

    public StringFilter getTen() {
        return ten;
    }

    public void setTen(StringFilter ten) {
        this.ten = ten;
    }

    public IntegerFilter getTyLeMienGiam() {
        return tyLeMienGiam;
    }

    public void setTyLeMienGiam(IntegerFilter tyLeMienGiam) {
        this.tyLeMienGiam = tyLeMienGiam;
    }

    public IntegerFilter getTyLeMienGiamKtc() {
        return tyLeMienGiamKtc;
    }

    public void setTyLeMienGiamKtc(IntegerFilter tyLeMienGiamKtc) {
        this.tyLeMienGiamKtc = tyLeMienGiamKtc;
    }

    public LongFilter getNhomDoiTuongBhytId() {
        return nhomDoiTuongBhytId;
    }

    public void setNhomDoiTuongBhytId(LongFilter nhomDoiTuongBhytId) {
        this.nhomDoiTuongBhytId = nhomDoiTuongBhytId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final DoiTuongBhytCriteria that = (DoiTuongBhytCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(canTren, that.canTren) &&
            Objects.equals(canTrenKtc, that.canTrenKtc) &&
            Objects.equals(ma, that.ma) &&
            Objects.equals(ten, that.ten) &&
            Objects.equals(tyLeMienGiam, that.tyLeMienGiam) &&
            Objects.equals(tyLeMienGiamKtc, that.tyLeMienGiamKtc) &&
            Objects.equals(nhomDoiTuongBhytId, that.nhomDoiTuongBhytId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        canTren,
        canTrenKtc,
        ma,
        ten,
        tyLeMienGiam,
        tyLeMienGiamKtc,
        nhomDoiTuongBhytId
        );
    }

    @Override
    public String toString() {
        return "DoiTuongBhytCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (canTren != null ? "canTren=" + canTren + ", " : "") +
                (canTrenKtc != null ? "canTrenKtc=" + canTrenKtc + ", " : "") +
                (ma != null ? "ma=" + ma + ", " : "") +
                (ten != null ? "ten=" + ten + ", " : "") +
                (tyLeMienGiam != null ? "tyLeMienGiam=" + tyLeMienGiam + ", " : "") +
                (tyLeMienGiamKtc != null ? "tyLeMienGiamKtc=" + tyLeMienGiamKtc + ", " : "") +
                (nhomDoiTuongBhytId != null ? "nhomDoiTuongBhytId=" + nhomDoiTuongBhytId + ", " : "") +
            "}";
    }

}

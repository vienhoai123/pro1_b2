package vn.vnpt.service.dto;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link vn.vnpt.domain.UserType} entity.
 */
public class UserTypeDTO implements Serializable {
    
    private Long id;

    /**
     * Mô tả loại user
     */
    @Size(max = 255)
    @ApiModelProperty(value = "Mô tả loại user")
    private String moTa;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserTypeDTO userTypeDTO = (UserTypeDTO) o;
        if (userTypeDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), userTypeDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UserTypeDTO{" +
            "id=" + getId() +
            ", moTa='" + getMoTa() + "'" +
            "}";
    }
}

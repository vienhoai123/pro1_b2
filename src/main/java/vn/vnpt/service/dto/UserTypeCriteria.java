package vn.vnpt.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link vn.vnpt.domain.UserType} entity. This class is used
 * in {@link vn.vnpt.web.rest.UserTypeResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /user-types?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class UserTypeCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter moTa;

    public UserTypeCriteria() {
    }

    public UserTypeCriteria(UserTypeCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.moTa = other.moTa == null ? null : other.moTa.copy();
    }

    @Override
    public UserTypeCriteria copy() {
        return new UserTypeCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getMoTa() {
        return moTa;
    }

    public void setMoTa(StringFilter moTa) {
        this.moTa = moTa;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final UserTypeCriteria that = (UserTypeCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(moTa, that.moTa);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        moTa
        );
    }

    @Override
    public String toString() {
        return "UserTypeCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (moTa != null ? "moTa=" + moTa + ", " : "") +
            "}";
    }

}

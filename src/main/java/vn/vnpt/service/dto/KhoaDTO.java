package vn.vnpt.service.dto;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link vn.vnpt.domain.Khoa} entity.
 */
public class KhoaDTO implements Serializable {
    
    private Long id;

    /**
     * Cấp khoa
     */
    @NotNull
    @ApiModelProperty(value = "Cấp khoa", required = true)
    private Integer cap;

    /**
     * Trạng thái: 1: Còn hiệu lực. 0 Hết hiệu lực
     */
    @NotNull
    @ApiModelProperty(value = "Trạng thái: 1: Còn hiệu lực. 0 Hết hiệu lực", required = true)
    private Boolean enabled;

    /**
     * Ký hiệu khoa
     */
    @NotNull
    @Size(max = 50)
    @ApiModelProperty(value = "Ký hiệu khoa", required = true)
    private String kyHieu;

    /**
     * Số điện thoại khoa
     */
    @Size(max = 15)
    @ApiModelProperty(value = "Số điện thoại khoa")
    private String phone;

    /**
     * Tên khoa
     */
    @NotNull
    @Size(max = 200)
    @ApiModelProperty(value = "Tên khoa", required = true)
    private String ten;

    /**
     * Mã đơn vị
     */
    @ApiModelProperty(value = "Mã đơn vị")

    private Long donViId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCap() {
        return cap;
    }

    public void setCap(Integer cap) {
        this.cap = cap;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getKyHieu() {
        return kyHieu;
    }

    public void setKyHieu(String kyHieu) {
        this.kyHieu = kyHieu;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public Long getDonViId() {
        return donViId;
    }

    public void setDonViId(Long donViId) {
        this.donViId = donViId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        KhoaDTO khoaDTO = (KhoaDTO) o;
        if (khoaDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), khoaDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "KhoaDTO{" +
            "id=" + getId() +
            ", cap=" + getCap() +
            ", enabled='" + isEnabled() + "'" +
            ", kyHieu='" + getKyHieu() + "'" +
            ", phone='" + getPhone() + "'" +
            ", ten='" + getTen() + "'" +
            ", donViId=" + getDonViId() +
            "}";
    }
}

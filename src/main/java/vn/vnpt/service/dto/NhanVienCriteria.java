package vn.vnpt.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link vn.vnpt.domain.NhanVien} entity. This class is used
 * in {@link vn.vnpt.web.rest.NhanVienResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /nhan-viens?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class NhanVienCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter ten;

    private StringFilter chungChiHanhNghe;

    private StringFilter cmnd;

    private IntegerFilter gioiTinh;

    private LocalDateFilter ngaySinh;

    private LongFilter chucDanhId;

    private LongFilter userExtraId;

    private LongFilter chucVuId;

    public NhanVienCriteria() {
    }

    public NhanVienCriteria(NhanVienCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.ten = other.ten == null ? null : other.ten.copy();
        this.chungChiHanhNghe = other.chungChiHanhNghe == null ? null : other.chungChiHanhNghe.copy();
        this.cmnd = other.cmnd == null ? null : other.cmnd.copy();
        this.gioiTinh = other.gioiTinh == null ? null : other.gioiTinh.copy();
        this.ngaySinh = other.ngaySinh == null ? null : other.ngaySinh.copy();
        this.chucDanhId = other.chucDanhId == null ? null : other.chucDanhId.copy();
        this.userExtraId = other.userExtraId == null ? null : other.userExtraId.copy();
        this.chucVuId = other.chucVuId == null ? null : other.chucVuId.copy();
    }

    @Override
    public NhanVienCriteria copy() {
        return new NhanVienCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getTen() {
        return ten;
    }

    public void setTen(StringFilter ten) {
        this.ten = ten;
    }

    public StringFilter getChungChiHanhNghe() {
        return chungChiHanhNghe;
    }

    public void setChungChiHanhNghe(StringFilter chungChiHanhNghe) {
        this.chungChiHanhNghe = chungChiHanhNghe;
    }

    public StringFilter getCmnd() {
        return cmnd;
    }

    public void setCmnd(StringFilter cmnd) {
        this.cmnd = cmnd;
    }

    public IntegerFilter getGioiTinh() {
        return gioiTinh;
    }

    public void setGioiTinh(IntegerFilter gioiTinh) {
        this.gioiTinh = gioiTinh;
    }

    public LocalDateFilter getNgaySinh() {
        return ngaySinh;
    }

    public void setNgaySinh(LocalDateFilter ngaySinh) {
        this.ngaySinh = ngaySinh;
    }

    public LongFilter getChucDanhId() {
        return chucDanhId;
    }

    public void setChucDanhId(LongFilter chucDanhId) {
        this.chucDanhId = chucDanhId;
    }

    public LongFilter getUserExtraId() {
        return userExtraId;
    }

    public void setUserExtraId(LongFilter userExtraId) {
        this.userExtraId = userExtraId;
    }

    public LongFilter getChucVuId() {
        return chucVuId;
    }

    public void setChucVuId(LongFilter chucVuId) {
        this.chucVuId = chucVuId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final NhanVienCriteria that = (NhanVienCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(ten, that.ten) &&
            Objects.equals(chungChiHanhNghe, that.chungChiHanhNghe) &&
            Objects.equals(cmnd, that.cmnd) &&
            Objects.equals(gioiTinh, that.gioiTinh) &&
            Objects.equals(ngaySinh, that.ngaySinh) &&
            Objects.equals(chucDanhId, that.chucDanhId) &&
            Objects.equals(userExtraId, that.userExtraId) &&
            Objects.equals(chucVuId, that.chucVuId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        ten,
        chungChiHanhNghe,
        cmnd,
        gioiTinh,
        ngaySinh,
        chucDanhId,
        userExtraId,
        chucVuId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "NhanVienCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (ten != null ? "ten=" + ten + ", " : "") +
                (chungChiHanhNghe != null ? "chungChiHanhNghe=" + chungChiHanhNghe + ", " : "") +
                (cmnd != null ? "cmnd=" + cmnd + ", " : "") +
                (gioiTinh != null ? "gioiTinh=" + gioiTinh + ", " : "") +
                (ngaySinh != null ? "ngaySinh=" + ngaySinh + ", " : "") +
                (chucDanhId != null ? "chucDanhId=" + chucDanhId + ", " : "") +
                (userExtraId != null ? "userExtraId=" + userExtraId + ", " : "") +
                (chucVuId != null ? "chucVuId=" + chucVuId + ", " : "") +
            "}";
    }

}

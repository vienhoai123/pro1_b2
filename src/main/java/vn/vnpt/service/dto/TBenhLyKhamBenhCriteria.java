package vn.vnpt.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link vn.vnpt.domain.TBenhLyKhamBenh} entity. This class is used
 * in {@link vn.vnpt.web.rest.TBenhLyKhamBenhResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /t-benh-ly-kham-benhs?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class TBenhLyKhamBenhCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private BooleanFilter loaiBenh;

    private LongFilter bakbId;

    private LongFilter donViId;

    private LongFilter benhNhanId;

    private LongFilter dotDieuTriId;

    private LongFilter thongTinKhoaId;

    private LongFilter thongTinKhamBenhId;

    private LongFilter benhLyId;

    public TBenhLyKhamBenhCriteria() {
    }

    public TBenhLyKhamBenhCriteria(TBenhLyKhamBenhCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.loaiBenh = other.loaiBenh == null ? null : other.loaiBenh.copy();
        this.bakbId = other.bakbId == null ? null : other.bakbId.copy();
        this.donViId = other.donViId == null ? null : other.donViId.copy();
        this.benhNhanId = other.benhNhanId == null ? null : other.benhNhanId.copy();
        this.dotDieuTriId = other.dotDieuTriId == null ? null : other.dotDieuTriId.copy();
        this.thongTinKhoaId = other.thongTinKhoaId == null ? null : other.thongTinKhoaId.copy();
        this.thongTinKhamBenhId = other.thongTinKhamBenhId == null ? null : other.thongTinKhamBenhId.copy();
        this.benhLyId = other.benhLyId == null ? null : other.benhLyId.copy();
    }

    @Override
    public TBenhLyKhamBenhCriteria copy() {
        return new TBenhLyKhamBenhCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public BooleanFilter getLoaiBenh() {
        return loaiBenh;
    }

    public void setLoaiBenh(BooleanFilter loaiBenh) {
        this.loaiBenh = loaiBenh;
    }

    public LongFilter getBakbId() {
        return bakbId;
    }

    public void setBakbId(LongFilter bakbId) {
        this.bakbId = bakbId;
    }

    public LongFilter getDonViId() {
        return donViId;
    }

    public void setDonViId(LongFilter donViId) {
        this.donViId = donViId;
    }

    public LongFilter getBenhNhanId() {
        return benhNhanId;
    }

    public void setBenhNhanId(LongFilter benhNhanId) {
        this.benhNhanId = benhNhanId;
    }

    public LongFilter getDotDieuTriId() {
        return dotDieuTriId;
    }

    public void setDotDieuTriId(LongFilter dotDieuTriId) {
        this.dotDieuTriId = dotDieuTriId;
    }

    public LongFilter getThongTinKhoaId() {
        return thongTinKhoaId;
    }

    public void setThongTinKhoaId(LongFilter thongTinKhoaId) {
        this.thongTinKhoaId = thongTinKhoaId;
    }

    public LongFilter getThongTinKhamBenhId() {
        return thongTinKhamBenhId;
    }

    public void setThongTinKhamBenhId(LongFilter thongTinKhamBenhId) {
        this.thongTinKhamBenhId = thongTinKhamBenhId;
    }

    public LongFilter getBenhLyId() {
        return benhLyId;
    }

    public void setBenhLyId(LongFilter benhLyId) {
        this.benhLyId = benhLyId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final TBenhLyKhamBenhCriteria that = (TBenhLyKhamBenhCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(loaiBenh, that.loaiBenh) &&
            Objects.equals(bakbId, that.bakbId) &&
            Objects.equals(donViId, that.donViId) &&
            Objects.equals(benhNhanId, that.benhNhanId) &&
            Objects.equals(dotDieuTriId, that.dotDieuTriId) &&
            Objects.equals(thongTinKhoaId, that.thongTinKhoaId) &&
            Objects.equals(thongTinKhamBenhId, that.thongTinKhamBenhId) &&
            Objects.equals(benhLyId, that.benhLyId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        loaiBenh,
        bakbId,
        donViId,
        benhNhanId,
        dotDieuTriId,
        thongTinKhoaId,
        thongTinKhamBenhId,
        benhLyId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TBenhLyKhamBenhCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (loaiBenh != null ? "loaiBenh=" + loaiBenh + ", " : "") +
                (bakbId != null ? "bakbId=" + bakbId + ", " : "") +
                (donViId != null ? "donViId=" + donViId + ", " : "") +
                (benhNhanId != null ? "benhNhanId=" + benhNhanId + ", " : "") +
                (dotDieuTriId != null ? "dotDieuTriId=" + dotDieuTriId + ", " : "") +
                (thongTinKhoaId != null ? "thongTinKhoaId=" + thongTinKhoaId + ", " : "") +
                (thongTinKhamBenhId != null ? "thongTinKhamBenhId=" + thongTinKhamBenhId + ", " : "") +
                (benhLyId != null ? "benhLyId=" + benhLyId + ", " : "") +
            "}";
    }

}

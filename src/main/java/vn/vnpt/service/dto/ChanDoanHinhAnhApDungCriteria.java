package vn.vnpt.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.BigDecimalFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link vn.vnpt.domain.ChanDoanHinhAnhApDung} entity. This class is used
 * in {@link vn.vnpt.web.rest.ChanDoanHinhAnhApDungResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /chan-doan-hinh-anh-ap-dungs?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ChanDoanHinhAnhApDungCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter maBaoCaoBhxh;

    private StringFilter maBaoCaoBhyt;

    private LocalDateFilter ngayApDung;

    private StringFilter soCongVanBhxh;

    private StringFilter soQuyetDinh;

    private StringFilter tenBaoCaoBhxh;

    private StringFilter tenDichVuKhongBhyt;

    private BigDecimalFilter tienBenhNhanChi;

    private BigDecimalFilter tienBhxhChi;

    private BigDecimalFilter tienNgoaiBhyt;

    private BigDecimalFilter tongTienThanhToan;

    private IntegerFilter tyLeBhxhThanhToan;

    private BigDecimalFilter giaBhyt;

    private BigDecimalFilter giaKhongBhyt;

    private BooleanFilter doiTuongDacBiet;

    private IntegerFilter nguonChi;

    private BooleanFilter enable;

    private LongFilter dotGiaId;

    private LongFilter cdhaId;

    public ChanDoanHinhAnhApDungCriteria() {
    }

    public ChanDoanHinhAnhApDungCriteria(ChanDoanHinhAnhApDungCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.maBaoCaoBhxh = other.maBaoCaoBhxh == null ? null : other.maBaoCaoBhxh.copy();
        this.maBaoCaoBhyt = other.maBaoCaoBhyt == null ? null : other.maBaoCaoBhyt.copy();
        this.ngayApDung = other.ngayApDung == null ? null : other.ngayApDung.copy();
        this.soCongVanBhxh = other.soCongVanBhxh == null ? null : other.soCongVanBhxh.copy();
        this.soQuyetDinh = other.soQuyetDinh == null ? null : other.soQuyetDinh.copy();
        this.tenBaoCaoBhxh = other.tenBaoCaoBhxh == null ? null : other.tenBaoCaoBhxh.copy();
        this.tenDichVuKhongBhyt = other.tenDichVuKhongBhyt == null ? null : other.tenDichVuKhongBhyt.copy();
        this.tienBenhNhanChi = other.tienBenhNhanChi == null ? null : other.tienBenhNhanChi.copy();
        this.tienBhxhChi = other.tienBhxhChi == null ? null : other.tienBhxhChi.copy();
        this.tienNgoaiBhyt = other.tienNgoaiBhyt == null ? null : other.tienNgoaiBhyt.copy();
        this.tongTienThanhToan = other.tongTienThanhToan == null ? null : other.tongTienThanhToan.copy();
        this.tyLeBhxhThanhToan = other.tyLeBhxhThanhToan == null ? null : other.tyLeBhxhThanhToan.copy();
        this.giaBhyt = other.giaBhyt == null ? null : other.giaBhyt.copy();
        this.giaKhongBhyt = other.giaKhongBhyt == null ? null : other.giaKhongBhyt.copy();
        this.doiTuongDacBiet = other.doiTuongDacBiet == null ? null : other.doiTuongDacBiet.copy();
        this.nguonChi = other.nguonChi == null ? null : other.nguonChi.copy();
        this.enable = other.enable == null ? null : other.enable.copy();
        this.dotGiaId = other.dotGiaId == null ? null : other.dotGiaId.copy();
        this.cdhaId = other.cdhaId == null ? null : other.cdhaId.copy();
    }

    @Override
    public ChanDoanHinhAnhApDungCriteria copy() {
        return new ChanDoanHinhAnhApDungCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getMaBaoCaoBhxh() {
        return maBaoCaoBhxh;
    }

    public void setMaBaoCaoBhxh(StringFilter maBaoCaoBhxh) {
        this.maBaoCaoBhxh = maBaoCaoBhxh;
    }

    public StringFilter getMaBaoCaoBhyt() {
        return maBaoCaoBhyt;
    }

    public void setMaBaoCaoBhyt(StringFilter maBaoCaoBhyt) {
        this.maBaoCaoBhyt = maBaoCaoBhyt;
    }

    public LocalDateFilter getNgayApDung() {
        return ngayApDung;
    }

    public void setNgayApDung(LocalDateFilter ngayApDung) {
        this.ngayApDung = ngayApDung;
    }

    public StringFilter getSoCongVanBhxh() {
        return soCongVanBhxh;
    }

    public void setSoCongVanBhxh(StringFilter soCongVanBhxh) {
        this.soCongVanBhxh = soCongVanBhxh;
    }

    public StringFilter getSoQuyetDinh() {
        return soQuyetDinh;
    }

    public void setSoQuyetDinh(StringFilter soQuyetDinh) {
        this.soQuyetDinh = soQuyetDinh;
    }

    public StringFilter getTenBaoCaoBhxh() {
        return tenBaoCaoBhxh;
    }

    public void setTenBaoCaoBhxh(StringFilter tenBaoCaoBhxh) {
        this.tenBaoCaoBhxh = tenBaoCaoBhxh;
    }

    public StringFilter getTenDichVuKhongBhyt() {
        return tenDichVuKhongBhyt;
    }

    public void setTenDichVuKhongBhyt(StringFilter tenDichVuKhongBhyt) {
        this.tenDichVuKhongBhyt = tenDichVuKhongBhyt;
    }

    public BigDecimalFilter getTienBenhNhanChi() {
        return tienBenhNhanChi;
    }

    public void setTienBenhNhanChi(BigDecimalFilter tienBenhNhanChi) {
        this.tienBenhNhanChi = tienBenhNhanChi;
    }

    public BigDecimalFilter getTienBhxhChi() {
        return tienBhxhChi;
    }

    public void setTienBhxhChi(BigDecimalFilter tienBhxhChi) {
        this.tienBhxhChi = tienBhxhChi;
    }

    public BigDecimalFilter getTienNgoaiBhyt() {
        return tienNgoaiBhyt;
    }

    public void setTienNgoaiBhyt(BigDecimalFilter tienNgoaiBhyt) {
        this.tienNgoaiBhyt = tienNgoaiBhyt;
    }

    public BigDecimalFilter getTongTienThanhToan() {
        return tongTienThanhToan;
    }

    public void setTongTienThanhToan(BigDecimalFilter tongTienThanhToan) {
        this.tongTienThanhToan = tongTienThanhToan;
    }

    public IntegerFilter getTyLeBhxhThanhToan() {
        return tyLeBhxhThanhToan;
    }

    public void setTyLeBhxhThanhToan(IntegerFilter tyLeBhxhThanhToan) {
        this.tyLeBhxhThanhToan = tyLeBhxhThanhToan;
    }

    public BigDecimalFilter getGiaBhyt() {
        return giaBhyt;
    }

    public void setGiaBhyt(BigDecimalFilter giaBhyt) {
        this.giaBhyt = giaBhyt;
    }

    public BigDecimalFilter getGiaKhongBhyt() {
        return giaKhongBhyt;
    }

    public void setGiaKhongBhyt(BigDecimalFilter giaKhongBhyt) {
        this.giaKhongBhyt = giaKhongBhyt;
    }

    public BooleanFilter getDoiTuongDacBiet() {
        return doiTuongDacBiet;
    }

    public void setDoiTuongDacBiet(BooleanFilter doiTuongDacBiet) {
        this.doiTuongDacBiet = doiTuongDacBiet;
    }

    public IntegerFilter getNguonChi() {
        return nguonChi;
    }

    public void setNguonChi(IntegerFilter nguonChi) {
        this.nguonChi = nguonChi;
    }

    public BooleanFilter getEnable() {
        return enable;
    }

    public void setEnable(BooleanFilter enable) {
        this.enable = enable;
    }

    public LongFilter getDotGiaId() {
        return dotGiaId;
    }

    public void setDotGiaId(LongFilter dotGiaId) {
        this.dotGiaId = dotGiaId;
    }

    public LongFilter getCdhaId() {
        return cdhaId;
    }

    public void setCdhaId(LongFilter cdhaId) {
        this.cdhaId = cdhaId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ChanDoanHinhAnhApDungCriteria that = (ChanDoanHinhAnhApDungCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(maBaoCaoBhxh, that.maBaoCaoBhxh) &&
            Objects.equals(maBaoCaoBhyt, that.maBaoCaoBhyt) &&
            Objects.equals(ngayApDung, that.ngayApDung) &&
            Objects.equals(soCongVanBhxh, that.soCongVanBhxh) &&
            Objects.equals(soQuyetDinh, that.soQuyetDinh) &&
            Objects.equals(tenBaoCaoBhxh, that.tenBaoCaoBhxh) &&
            Objects.equals(tenDichVuKhongBhyt, that.tenDichVuKhongBhyt) &&
            Objects.equals(tienBenhNhanChi, that.tienBenhNhanChi) &&
            Objects.equals(tienBhxhChi, that.tienBhxhChi) &&
            Objects.equals(tienNgoaiBhyt, that.tienNgoaiBhyt) &&
            Objects.equals(tongTienThanhToan, that.tongTienThanhToan) &&
            Objects.equals(tyLeBhxhThanhToan, that.tyLeBhxhThanhToan) &&
            Objects.equals(giaBhyt, that.giaBhyt) &&
            Objects.equals(giaKhongBhyt, that.giaKhongBhyt) &&
            Objects.equals(doiTuongDacBiet, that.doiTuongDacBiet) &&
            Objects.equals(nguonChi, that.nguonChi) &&
            Objects.equals(enable, that.enable) &&
            Objects.equals(dotGiaId, that.dotGiaId) &&
            Objects.equals(cdhaId, that.cdhaId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        maBaoCaoBhxh,
        maBaoCaoBhyt,
        ngayApDung,
        soCongVanBhxh,
        soQuyetDinh,
        tenBaoCaoBhxh,
        tenDichVuKhongBhyt,
        tienBenhNhanChi,
        tienBhxhChi,
        tienNgoaiBhyt,
        tongTienThanhToan,
        tyLeBhxhThanhToan,
        giaBhyt,
        giaKhongBhyt,
        doiTuongDacBiet,
        nguonChi,
        enable,
        dotGiaId,
        cdhaId
        );
    }

    @Override
    public String toString() {
        return "ChanDoanHinhAnhApDungCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (maBaoCaoBhxh != null ? "maBaoCaoBhxh=" + maBaoCaoBhxh + ", " : "") +
                (maBaoCaoBhyt != null ? "maBaoCaoBhyt=" + maBaoCaoBhyt + ", " : "") +
                (ngayApDung != null ? "ngayApDung=" + ngayApDung + ", " : "") +
                (soCongVanBhxh != null ? "soCongVanBhxh=" + soCongVanBhxh + ", " : "") +
                (soQuyetDinh != null ? "soQuyetDinh=" + soQuyetDinh + ", " : "") +
                (tenBaoCaoBhxh != null ? "tenBaoCaoBhxh=" + tenBaoCaoBhxh + ", " : "") +
                (tenDichVuKhongBhyt != null ? "tenDichVuKhongBhyt=" + tenDichVuKhongBhyt + ", " : "") +
                (tienBenhNhanChi != null ? "tienBenhNhanChi=" + tienBenhNhanChi + ", " : "") +
                (tienBhxhChi != null ? "tienBhxhChi=" + tienBhxhChi + ", " : "") +
                (tienNgoaiBhyt != null ? "tienNgoaiBhyt=" + tienNgoaiBhyt + ", " : "") +
                (tongTienThanhToan != null ? "tongTienThanhToan=" + tongTienThanhToan + ", " : "") +
                (tyLeBhxhThanhToan != null ? "tyLeBhxhThanhToan=" + tyLeBhxhThanhToan + ", " : "") +
                (giaBhyt != null ? "giaBhyt=" + giaBhyt + ", " : "") +
                (giaKhongBhyt != null ? "giaKhongBhyt=" + giaKhongBhyt + ", " : "") +
                (doiTuongDacBiet != null ? "doiTuongDacBiet=" + doiTuongDacBiet + ", " : "") +
                (nguonChi != null ? "nguonChi=" + nguonChi + ", " : "") +
                (enable != null ? "enable=" + enable + ", " : "") +
                (dotGiaId != null ? "dotGiaId=" + dotGiaId + ", " : "") +
                (cdhaId != null ? "cdhaId=" + cdhaId + ", " : "") +
            "}";
    }

}

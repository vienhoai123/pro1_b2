package vn.vnpt.service.dto;

import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link vn.vnpt.domain.BenhNhan} entity.
 */
public class BenhNhanDTO implements Serializable {
    
    private Long id;

    /**
     * Trạng thái thông tin bệnh nhân chỉ có năm sinh:\n1: Chỉ có năm sinh.\n0: Đầy đủ ngày tháng năm sinh.
     */
    @NotNull
    @ApiModelProperty(value = "Trạng thái thông tin bệnh nhân chỉ có năm sinh:\n1: Chỉ có năm sinh.\n0: Đầy đủ ngày tháng năm sinh.", required = true)
    private Boolean chiCoNamSinh;

    /**
     * Thông tin chứng minh nhân dân của bệnh nhân
     */
    @Size(max = 20)
    @ApiModelProperty(value = "Thông tin chứng minh nhân dân của bệnh nhân")
    private String cmnd;

    /**
     * Thông tin địa chỉ email của bệnh nhân
     */
    @Size(max = 255)
    @ApiModelProperty(value = "Thông tin địa chỉ email của bệnh nhân")
    private String email;

    /**
     * Trạng thái của 1 dòng thông tin bệnh nhân\n\n1: Có hiệu lực.\n\n0: Không có hiệu lực.
     */
    @NotNull
    @ApiModelProperty(value = "Trạng thái của 1 dòng thông tin bệnh nhân\n\n1: Có hiệu lực.\n\n0: Không có hiệu lực.", required = true)
    private Boolean enabled;

    /**
     * Trạng thái giới tính của bệnh nhân:\n\n1: Nam\n.\n0: Nữ.
     */
    @NotNull
    @ApiModelProperty(value = "Trạng thái giới tính của bệnh nhân:\n\n1: Nam\n.\n0: Nữ.", required = true)
    private Integer gioiTinh;

    /**
     * Thông tin kháng thể của bệnh nhân
     */
    @Size(max = 5)
    @ApiModelProperty(value = "Thông tin kháng thể của bệnh nhân")
    private String khangThe;

    /**
     * Thông tin mã số thuế của bệnh nhân
     */
    @Size(max = 100)
    @ApiModelProperty(value = "Thông tin mã số thuế của bệnh nhân")
    private String maSoThue;

    /**
     * Ngày cấp chứng minh nhân dân của bệnh nhân
     */
    @ApiModelProperty(value = "Ngày cấp chứng minh nhân dân của bệnh nhân")
    private LocalDate ngayCapCmnd;

    /**
     * Thông tin ngày sinh của bệnh nhân
     */
    @NotNull
    @ApiModelProperty(value = "Thông tin ngày sinh của bệnh nhân", required = true)
    private LocalDate ngaySinh;

    /**
     * Thông tin nhóm máu của bệnh nhân
     */
    @Size(max = 5)
    @ApiModelProperty(value = "Thông tin nhóm máu của bệnh nhân")
    private String nhomMau;

    /**
     * Thông tin nơi cấp chứng minh nhân dân
     */
    @Size(max = 500)
    @ApiModelProperty(value = "Thông tin nơi cấp chứng minh nhân dân")
    private String noiCapCmnd;

    /**
     * Thông tin nơi làm việc của bệnh nhân
     */
    @Size(max = 500)
    @ApiModelProperty(value = "Thông tin nơi làm việc của bệnh nhân")
    private String noiLamViec;

    /**
     * Thông tin số điện thoại của bệnh nhân
     */
    @Size(max = 15)
    @ApiModelProperty(value = "Thông tin số điện thoại của bệnh nhân")
    private String phone;

    /**
     * Tên bệnh nhân
     */
    @NotNull
    @Size(max = 200)
    @ApiModelProperty(value = "Tên bệnh nhân", required = true)
    private String ten;

    /**
     * Số nhà của bệnh nhân
     */
    @Size(max = 255)
    @ApiModelProperty(value = "Số nhà của bệnh nhân")
    private String soNhaXom;

    /**
     * Thông tin ấp, thôn, xóm của bệnh nhân
     */
    @Size(max = 255)
    @ApiModelProperty(value = "Thông tin ấp, thôn, xóm của bệnh nhân")
    private String apThon;

    /**
     * Mã danh mục địa phương
     */
    @ApiModelProperty(value = "Mã danh mục địa phương")
    private Long diaPhuongId;

    /**
     * Thông tin địa chỉ thường trú của bệnh nhân
     */
    @NotNull
    @Size(max = 500)
    @ApiModelProperty(value = "Thông tin địa chỉ thường trú của bệnh nhân", required = true)
    private String diaChiThuongTru;

    /**
     * Mã dân tộc
     */
    @ApiModelProperty(value = "Mã dân tộc")

    private Long danTocId;
    /**
     * Mã nghề nghiệp
     */
    @ApiModelProperty(value = "Mã nghề nghiệp")

    private Long ngheNghiepId;
    /**
     * Mã quốc tịch
     */
    @ApiModelProperty(value = "Mã quốc tịch")

    private Long quocTichId;
    /**
     * Mã phường xã
     */
    @ApiModelProperty(value = "Mã phường xã")

    private Long phuongXaId;
    /**
     * Mã quận huyện
     */
    @ApiModelProperty(value = "Mã quận huyện")

    private Long quanHuyenId;
    /**
     * Mã tỉnh thành phố
     */
    @ApiModelProperty(value = "Mã tỉnh thành phố")

    private Long tinhThanhPhoId;

    private Long userExtraId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isChiCoNamSinh() {
        return chiCoNamSinh;
    }

    public void setChiCoNamSinh(Boolean chiCoNamSinh) {
        this.chiCoNamSinh = chiCoNamSinh;
    }

    public String getCmnd() {
        return cmnd;
    }

    public void setCmnd(String cmnd) {
        this.cmnd = cmnd;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Integer getGioiTinh() {
        return gioiTinh;
    }

    public void setGioiTinh(Integer gioiTinh) {
        this.gioiTinh = gioiTinh;
    }

    public String getKhangThe() {
        return khangThe;
    }

    public void setKhangThe(String khangThe) {
        this.khangThe = khangThe;
    }

    public String getMaSoThue() {
        return maSoThue;
    }

    public void setMaSoThue(String maSoThue) {
        this.maSoThue = maSoThue;
    }

    public LocalDate getNgayCapCmnd() {
        return ngayCapCmnd;
    }

    public void setNgayCapCmnd(LocalDate ngayCapCmnd) {
        this.ngayCapCmnd = ngayCapCmnd;
    }

    public LocalDate getNgaySinh() {
        return ngaySinh;
    }

    public void setNgaySinh(LocalDate ngaySinh) {
        this.ngaySinh = ngaySinh;
    }

    public String getNhomMau() {
        return nhomMau;
    }

    public void setNhomMau(String nhomMau) {
        this.nhomMau = nhomMau;
    }

    public String getNoiCapCmnd() {
        return noiCapCmnd;
    }

    public void setNoiCapCmnd(String noiCapCmnd) {
        this.noiCapCmnd = noiCapCmnd;
    }

    public String getNoiLamViec() {
        return noiLamViec;
    }

    public void setNoiLamViec(String noiLamViec) {
        this.noiLamViec = noiLamViec;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getSoNhaXom() {
        return soNhaXom;
    }

    public void setSoNhaXom(String soNhaXom) {
        this.soNhaXom = soNhaXom;
    }

    public String getApThon() {
        return apThon;
    }

    public void setApThon(String apThon) {
        this.apThon = apThon;
    }

    public Long getDiaPhuongId() {
        return diaPhuongId;
    }

    public void setDiaPhuongId(Long diaPhuongId) {
        this.diaPhuongId = diaPhuongId;
    }

    public String getDiaChiThuongTru() {
        return diaChiThuongTru;
    }

    public void setDiaChiThuongTru(String diaChiThuongTru) {
        this.diaChiThuongTru = diaChiThuongTru;
    }

    public Long getDanTocId() {
        return danTocId;
    }

    public void setDanTocId(Long danTocId) {
        this.danTocId = danTocId;
    }

    public Long getNgheNghiepId() {
        return ngheNghiepId;
    }

    public void setNgheNghiepId(Long ngheNghiepId) {
        this.ngheNghiepId = ngheNghiepId;
    }

    public Long getQuocTichId() {
        return quocTichId;
    }

    public void setQuocTichId(Long countryId) {
        this.quocTichId = countryId;
    }

    public Long getPhuongXaId() {
        return phuongXaId;
    }

    public void setPhuongXaId(Long phuongXaId) {
        this.phuongXaId = phuongXaId;
    }

    public Long getQuanHuyenId() {
        return quanHuyenId;
    }

    public void setQuanHuyenId(Long quanHuyenId) {
        this.quanHuyenId = quanHuyenId;
    }

    public Long getTinhThanhPhoId() {
        return tinhThanhPhoId;
    }

    public void setTinhThanhPhoId(Long tinhThanhPhoId) {
        this.tinhThanhPhoId = tinhThanhPhoId;
    }

    public Long getUserExtraId() {
        return userExtraId;
    }

    public void setUserExtraId(Long userExtraId) {
        this.userExtraId = userExtraId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BenhNhanDTO)) {
            return false;
        }

        return id != null && id.equals(((BenhNhanDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "BenhNhanDTO{" +
            "id=" + getId() +
            ", chiCoNamSinh='" + isChiCoNamSinh() + "'" +
            ", cmnd='" + getCmnd() + "'" +
            ", email='" + getEmail() + "'" +
            ", enabled='" + isEnabled() + "'" +
            ", gioiTinh=" + getGioiTinh() +
            ", khangThe='" + getKhangThe() + "'" +
            ", maSoThue='" + getMaSoThue() + "'" +
            ", ngayCapCmnd='" + getNgayCapCmnd() + "'" +
            ", ngaySinh='" + getNgaySinh() + "'" +
            ", nhomMau='" + getNhomMau() + "'" +
            ", noiCapCmnd='" + getNoiCapCmnd() + "'" +
            ", noiLamViec='" + getNoiLamViec() + "'" +
            ", phone='" + getPhone() + "'" +
            ", ten='" + getTen() + "'" +
            ", soNhaXom='" + getSoNhaXom() + "'" +
            ", apThon='" + getApThon() + "'" +
            ", diaPhuongId=" + getDiaPhuongId() +
            ", diaChiThuongTru='" + getDiaChiThuongTru() + "'" +
            ", danTocId=" + getDanTocId() +
            ", ngheNghiepId=" + getNgheNghiepId() +
            ", quocTichId=" + getQuocTichId() +
            ", phuongXaId=" + getPhuongXaId() +
            ", quanHuyenId=" + getQuanHuyenId() +
            ", tinhThanhPhoId=" + getTinhThanhPhoId() +
            ", userExtraId=" + getUserExtraId() +
            "}";
    }
}

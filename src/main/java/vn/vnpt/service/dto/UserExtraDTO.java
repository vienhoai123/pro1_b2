package vn.vnpt.service.dto;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link vn.vnpt.domain.UserExtra} entity.
 */
public class UserExtraDTO implements Serializable {

    private Long id;

    /**
     * Trạng thái: 1: Còn hiệu lực. 0 Hết hiệu lực
     */
    @NotNull
    @ApiModelProperty(value = "Trạng thái: 1: Còn hiệu lực. 0 Hết hiệu lực", required = true)
    private Boolean enabled;

    /**
     * username
     */
    @NotNull
    @Size(max = 100)
    @ApiModelProperty(value = "username", required = true)
    private String username;

    /**
     * user
     */
    @ApiModelProperty(value = "userId")

    private String userId;

    private Long userTypeId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Long getUserTypeId() {
        return userTypeId;
    }

    public void setUserTypeId(Long userTypeId) {
        this.userTypeId = userTypeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserExtraDTO userExtraDTO = (UserExtraDTO) o;
        if (userExtraDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), userExtraDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UserExtraDTO{" +
            "id=" + getId() +
            ", enabled='" + isEnabled() + "'" +
            ", username='" + getUsername() + "'" +
            ", userId='" + getUserId() + "'" +
            ", userTypeId=" + getUserTypeId() +
            "}";
    }
}

package vn.vnpt.service.dto;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link vn.vnpt.domain.LoaiBenhLy} entity.
 */
public class LoaiBenhLyDTO implements Serializable {
    
    private Long id;

    /**
     * Ký hiệu bệnh lý
     */
    @Size(max = 50)
    @ApiModelProperty(value = "Ký hiệu bệnh lý")
    private String kyHieu;

    /**
     * Mô tả bệnh lý
     */
    @NotNull
    @Size(max = 500)
    @ApiModelProperty(value = "Mô tả bệnh lý", required = true)
    private String moTa;

    /**
     * Tên bệnh lý
     */
    @Size(max = 500)
    @ApiModelProperty(value = "Tên bệnh lý")
    private String ten;

    /**
     * Tên tiếng anh của bệnh lý
     */
    @NotNull
    @Size(max = 500)
    @ApiModelProperty(value = "Tên tiếng anh của bệnh lý", required = true)
    private String tenTiengAnh;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKyHieu() {
        return kyHieu;
    }

    public void setKyHieu(String kyHieu) {
        this.kyHieu = kyHieu;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getTenTiengAnh() {
        return tenTiengAnh;
    }

    public void setTenTiengAnh(String tenTiengAnh) {
        this.tenTiengAnh = tenTiengAnh;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        LoaiBenhLyDTO loaiBenhLyDTO = (LoaiBenhLyDTO) o;
        if (loaiBenhLyDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), loaiBenhLyDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "LoaiBenhLyDTO{" +
            "id=" + getId() +
            ", kyHieu='" + getKyHieu() + "'" +
            ", moTa='" + getMoTa() + "'" +
            ", ten='" + getTen() + "'" +
            ", tenTiengAnh='" + getTenTiengAnh() + "'" +
            "}";
    }
}

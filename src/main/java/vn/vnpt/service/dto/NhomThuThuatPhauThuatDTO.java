package vn.vnpt.service.dto;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A DTO for the {@link vn.vnpt.domain.NhomThuThuatPhauThuat} entity.
 */
public class NhomThuThuatPhauThuatDTO implements Serializable {
    
    private Long id;

    /**
     * Trạng thái có hiệu lực của dịch vụ: \r\n1: Còn hiệu lực. \r\n0: Đã ẩn.
     */
    @NotNull
    @ApiModelProperty(value = "Trạng thái có hiệu lực của dịch vụ: \r\n1: Còn hiệu lực. \r\n0: Đã ẩn.", required = true)
    private Boolean enable;

    /**
     * Cấp của nhóm thủ thuật phẫu thuật
     */
    @NotNull
    @ApiModelProperty(value = "Cấp của nhóm thủ thuật phẫu thuật", required = true)
    private Integer level;

    /**
     * Mã nhóm cha
     */
    @NotNull
    @ApiModelProperty(value = "Mã nhóm cha", required = true)
    private BigDecimal parentId;

    /**
     * Tên nhóm
     */
    @Size(max = 500)
    @ApiModelProperty(value = "Tên nhóm")
    private String ten;


    private Long donViId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public BigDecimal getParentId() {
        return parentId;
    }

    public void setParentId(BigDecimal parentId) {
        this.parentId = parentId;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public Long getDonViId() {
        return donViId;
    }

    public void setDonViId(Long donViId) {
        this.donViId = donViId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        NhomThuThuatPhauThuatDTO nhomThuThuatPhauThuatDTO = (NhomThuThuatPhauThuatDTO) o;
        if (nhomThuThuatPhauThuatDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), nhomThuThuatPhauThuatDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "NhomThuThuatPhauThuatDTO{" +
            "id=" + getId() +
            ", enable='" + isEnable() + "'" +
            ", level=" + getLevel() +
            ", parentId=" + getParentId() +
            ", ten='" + getTen() + "'" +
            ", donViId=" + getDonViId() +
            "}";
    }
}

package vn.vnpt.service.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A DTO for the {@link vn.vnpt.domain.DotGiaDichVuBhxh} entity.
 */
public class DotGiaDichVuBhxhDTO implements Serializable {

    private Long id;

    @NotNull
    private BigDecimal doiTuongApDung;

    @Size(max = 1000)
    private String ghiChu;

    @NotNull
    private LocalDate ngayApDung;

    @NotNull
    @Size(max = 500)
    private String ten;


    private Long donViId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getDoiTuongApDung() {
        return doiTuongApDung;
    }

    public void setDoiTuongApDung(BigDecimal doiTuongApDung) {
        this.doiTuongApDung = doiTuongApDung;
    }

    public String getGhiChu() {
        return ghiChu;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    public LocalDate getNgayApDung() {
        return ngayApDung;
    }

    public void setNgayApDung(LocalDate ngayApDung) {
        this.ngayApDung = ngayApDung;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public Long getDonViId() {
        return donViId;
    }

    public void setDonViId(Long donViId) {
        this.donViId = donViId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DotGiaDichVuBhxhDTO dotGiaDichVuBhxhDTO = (DotGiaDichVuBhxhDTO) o;
        if (dotGiaDichVuBhxhDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), dotGiaDichVuBhxhDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DotGiaDichVuBhxhDTO{" +
            "id=" + getId() +
            ", doiTuongApDung=" + getDoiTuongApDung() +
            ", ghiChu='" + getGhiChu() + "'" +
            ", ngayApDung='" + getNgayApDung() + "'" +
            ", ten='" + getTen() + "'" +
            ", donViId=" + getDonViId() +
            "}";
    }
}

package vn.vnpt.service.dto;

import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

import java.io.Serializable;
import java.util.Objects;

/**
 * Criteria class for the {@link vn.vnpt.domain.UserExtra} entity. This class is used
 * in {@link vn.vnpt.web.rest.UserExtraResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /user-extras?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class UserExtraCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private BooleanFilter enabled;

    private StringFilter username;

    private StringFilter userId;

    private LongFilter userTypeId;

    private LongFilter menuId;

    public UserExtraCriteria() {
    }

    public UserExtraCriteria(UserExtraCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.enabled = other.enabled == null ? null : other.enabled.copy();
        this.username = other.username == null ? null : other.username.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
        this.userTypeId = other.userTypeId == null ? null : other.userTypeId.copy();
        this.menuId = other.menuId == null ? null : other.menuId.copy();
    }

    @Override
    public UserExtraCriteria copy() {
        return new UserExtraCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public BooleanFilter getEnabled() {
        return enabled;
    }

    public void setEnabled(BooleanFilter enabled) {
        this.enabled = enabled;
    }

    public StringFilter getUsername() {
        return username;
    }

    public void setUsername(StringFilter username) {
        this.username = username;
    }

    public StringFilter getUserId() {
        return userId;
    }

    public void setUserId(StringFilter userId) {
        this.userId = userId;
    }

    public LongFilter getUserTypeId() {
        return userTypeId;
    }

    public void setUserTypeId(LongFilter userTypeId) {
        this.userTypeId = userTypeId;
    }

    public LongFilter getMenuId() {
        return menuId;
    }

    public void setMenuId(LongFilter menuId) {
        this.menuId = menuId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final UserExtraCriteria that = (UserExtraCriteria) o;
        return
            Objects.equals(id, that.id) &&
                Objects.equals(enabled, that.enabled) &&
                Objects.equals(username, that.username) &&
                Objects.equals(userId, that.userId) &&
                Objects.equals(userTypeId, that.userTypeId) &&
                Objects.equals(menuId, that.menuId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            enabled,
            username,
            userId,
            userTypeId,
            menuId
        );
    }

    @Override
    public String toString() {
        return "UserExtraCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (enabled != null ? "enabled=" + enabled + ", " : "") +
            (username != null ? "username=" + username + ", " : "") +
            (userId != null ? "userId=" + userId + ", " : "") +
            (userTypeId != null ? "userTypeId=" + userTypeId + ", " : "") +
            (menuId != null ? "menuId=" + menuId + ", " : "") +
            "}";
    }

}

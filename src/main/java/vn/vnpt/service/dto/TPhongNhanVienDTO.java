package vn.vnpt.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link vn.vnpt.domain.TPhongNhanVien} entity.
 */
public class TPhongNhanVienDTO implements Serializable {
    
    private Long id;

    @NotNull
    private Boolean enabled;


    private Long phongId;

    private Long nhanVienId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Long getPhongId() {
        return phongId;
    }

    public void setPhongId(Long phongId) {
        this.phongId = phongId;
    }

    public Long getNhanVienId() {
        return nhanVienId;
    }

    public void setNhanVienId(Long nhanVienId) {
        this.nhanVienId = nhanVienId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TPhongNhanVienDTO tPhongNhanVienDTO = (TPhongNhanVienDTO) o;
        if (tPhongNhanVienDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), tPhongNhanVienDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TPhongNhanVienDTO{" +
            "id=" + getId() +
            ", enabled='" + isEnabled() + "'" +
            ", phongId=" + getPhongId() +
            ", nhanVienId=" + getNhanVienId() +
            "}";
    }
}

package vn.vnpt.service.dto;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link vn.vnpt.domain.TinhThanhPho} entity.
 */
public class TinhThanhPhoDTO implements Serializable {
    
    private Long id;

    /**
     * Cấp
     */
    @NotNull
    @Size(max = 255)
    @ApiModelProperty(value = "Cấp", required = true)
    private String cap;

    /**
     * Đoạn gợi ý
     */
    @Size(max = 255)
    @ApiModelProperty(value = "Đoạn gợi ý")
    private String guessPhrase;

    /**
     * Tên tỉnh thành phố
     */
    @NotNull
    @Size(max = 255)
    @ApiModelProperty(value = "Tên tỉnh thành phố", required = true)
    private String ten;

    /**
     * Tên không dấu
     */
    @NotNull
    @Size(max = 255)
    @ApiModelProperty(value = "Tên không dấu", required = true)
    private String tenKhongDau;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCap() {
        return cap;
    }

    public void setCap(String cap) {
        this.cap = cap;
    }

    public String getGuessPhrase() {
        return guessPhrase;
    }

    public void setGuessPhrase(String guessPhrase) {
        this.guessPhrase = guessPhrase;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getTenKhongDau() {
        return tenKhongDau;
    }

    public void setTenKhongDau(String tenKhongDau) {
        this.tenKhongDau = tenKhongDau;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TinhThanhPhoDTO tinhThanhPhoDTO = (TinhThanhPhoDTO) o;
        if (tinhThanhPhoDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), tinhThanhPhoDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TinhThanhPhoDTO{" +
            "id=" + getId() +
            ", cap='" + getCap() + "'" +
            ", guessPhrase='" + getGuessPhrase() + "'" +
            ", ten='" + getTen() + "'" +
            ", tenKhongDau='" + getTenKhongDau() + "'" +
            "}";
    }
}

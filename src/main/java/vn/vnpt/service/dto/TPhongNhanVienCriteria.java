package vn.vnpt.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link vn.vnpt.domain.TPhongNhanVien} entity. This class is used
 * in {@link vn.vnpt.web.rest.TPhongNhanVienResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /t-phong-nhan-viens?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class TPhongNhanVienCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private BooleanFilter enabled;

    private LongFilter phongId;

    private LongFilter nhanVienId;

    private StringFilter tenNV;
    private StringFilter tenPhong;

    public StringFilter getTenPhong() {
        return tenPhong;
    }

    public void setTenPhong(StringFilter tenPhong) {
        this.tenPhong = tenPhong;
    }

    public StringFilter getTenNV() {
        return tenNV;
    }

    public void setTenNV(StringFilter tenNV) {
        this.tenNV = tenNV;
    }

    public TPhongNhanVienCriteria() {
    }

    public TPhongNhanVienCriteria(TPhongNhanVienCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.enabled = other.enabled == null ? null : other.enabled.copy();
        this.phongId = other.phongId == null ? null : other.phongId.copy();
        this.nhanVienId = other.nhanVienId == null ? null : other.nhanVienId.copy();
    }

    @Override
    public TPhongNhanVienCriteria copy() {
        return new TPhongNhanVienCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public BooleanFilter getEnabled() {
        return enabled;
    }

    public void setEnabled(BooleanFilter enabled) {
        this.enabled = enabled;
    }

    public LongFilter getPhongId() {
        return phongId;
    }

    public void setPhongId(LongFilter phongId) {
        this.phongId = phongId;
    }

    public LongFilter getNhanVienId() {
        return nhanVienId;
    }

    public void setNhanVienId(LongFilter nhanVienId) {
        this.nhanVienId = nhanVienId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final TPhongNhanVienCriteria that = (TPhongNhanVienCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(enabled, that.enabled) &&
            Objects.equals(phongId, that.phongId) &&
            Objects.equals(nhanVienId, that.nhanVienId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        enabled,
        phongId,
        nhanVienId
        );
    }

    @Override
    public String toString() {
        return "TPhongNhanVienCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (enabled != null ? "enabled=" + enabled + ", " : "") +
                (phongId != null ? "phongId=" + phongId + ", " : "") +
                (nhanVienId != null ? "nhanVienId=" + nhanVienId + ", " : "") +
            "}";
    }

}

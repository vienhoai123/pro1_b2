package vn.vnpt.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link vn.vnpt.domain.TPhanNhomTTPT} entity.
 */
public class TPhanNhomTTPTDTO implements Serializable {
    
    private Long id;


    private Long nhomttptId;

    private Long ttptId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNhomttptId() {
        return nhomttptId;
    }

    public void setNhomttptId(Long nhomThuThuatPhauThuatId) {
        this.nhomttptId = nhomThuThuatPhauThuatId;
    }

    public Long getTtptId() {
        return ttptId;
    }

    public void setTtptId(Long thuThuatPhauThuatId) {
        this.ttptId = thuThuatPhauThuatId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TPhanNhomTTPTDTO tPhanNhomTTPTDTO = (TPhanNhomTTPTDTO) o;
        if (tPhanNhomTTPTDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), tPhanNhomTTPTDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TPhanNhomTTPTDTO{" +
            "id=" + getId() +
            ", nhomttptId=" + getNhomttptId() +
            ", ttptId=" + getTtptId() +
            "}";
    }
}

package vn.vnpt.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link vn.vnpt.domain.HuongDieuTri} entity. This class is used
 * in {@link vn.vnpt.web.rest.HuongDieuTriResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /huong-dieu-tris?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class HuongDieuTriCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter noiTru;

    private StringFilter ten;

    private IntegerFilter ma;

    public HuongDieuTriCriteria() {
    }

    public HuongDieuTriCriteria(HuongDieuTriCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.noiTru = other.noiTru == null ? null : other.noiTru.copy();
        this.ten = other.ten == null ? null : other.ten.copy();
        this.ma = other.ma == null ? null : other.ma.copy();
    }

    @Override
    public HuongDieuTriCriteria copy() {
        return new HuongDieuTriCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getNoiTru() {
        return noiTru;
    }

    public void setNoiTru(IntegerFilter noiTru) {
        this.noiTru = noiTru;
    }

    public StringFilter getTen() {
        return ten;
    }

    public void setTen(StringFilter ten) {
        this.ten = ten;
    }

    public IntegerFilter getMa() {
        return ma;
    }

    public void setMa(IntegerFilter ma) {
        this.ma = ma;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final HuongDieuTriCriteria that = (HuongDieuTriCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(noiTru, that.noiTru) &&
            Objects.equals(ten, that.ten) &&
            Objects.equals(ma, that.ma);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        noiTru,
        ten,
        ma
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "HuongDieuTriCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (noiTru != null ? "noiTru=" + noiTru + ", " : "") +
                (ten != null ? "ten=" + ten + ", " : "") +
                (ma != null ? "ma=" + ma + ", " : "") +
            "}";
    }

}

package vn.vnpt.service.dto.customdto;

import vn.vnpt.service.dto.DichVuKhamApDungDTO;
import vn.vnpt.service.dto.DichVuKhamDTO;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

public class ThongTinDichVuKhamDTO implements Serializable {
    private Long id;
    private String tenHienThi;
    private String maDungChung;
    private String maNoiBo;
    private Long donGiaBenhVien;
    private BigDecimal giaBhyt;
    private BigDecimal giaKhongBhyt;
    private String tenDichVuKhongBaoHiem;
    private BigDecimal tienBenhNhanChi;
    private BigDecimal tienBhxhChi;
    private BigDecimal tienNgoaiBhyt;
    private BigDecimal tongTienThanhToan;
    private Integer tyLeBhxhThanhToan;
    private String donViTinh;

    public ThongTinDichVuKhamDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTenHienThi() {
        return tenHienThi;
    }

    public void setTenHienThi(String tenHienThi) {
        this.tenHienThi = tenHienThi;
    }

    public String getMaDungChung() {
        return maDungChung;
    }

    public void setMaDungChung(String maDungChung) {
        this.maDungChung = maDungChung;
    }

    public String getMaNoiBo() {
        return maNoiBo;
    }

    public void setMaNoiBo(String maNoiBo) {
        this.maNoiBo = maNoiBo;
    }

    public Long getDonGiaBenhVien() {
        return donGiaBenhVien;
    }

    public void setDonGiaBenhVien(Long donGiaBenhVien) {
        this.donGiaBenhVien = donGiaBenhVien;
    }

    public BigDecimal getGiaBhyt() {
        return giaBhyt;
    }

    public void setGiaBhyt(BigDecimal giaBhyt) {
        this.giaBhyt = giaBhyt;
    }

    public BigDecimal getGiaKhongBhyt() {
        return giaKhongBhyt;
    }

    public void setGiaKhongBhyt(BigDecimal giaKhongBhyt) {
        this.giaKhongBhyt = giaKhongBhyt;
    }

    public String getTenDichVuKhongBaoHiem() {
        return tenDichVuKhongBaoHiem;
    }

    public void setTenDichVuKhongBaoHiem(String tenDichVuKhongBaoHiem) {
        this.tenDichVuKhongBaoHiem = tenDichVuKhongBaoHiem;
    }

    public BigDecimal getTienBenhNhanChi() {
        return tienBenhNhanChi;
    }

    public void setTienBenhNhanChi(BigDecimal tienBenhNhanChi) {
        this.tienBenhNhanChi = tienBenhNhanChi;
    }

    public BigDecimal getTienBhxhChi() {
        return tienBhxhChi;
    }

    public void setTienBhxhChi(BigDecimal tienBhxhChi) {
        this.tienBhxhChi = tienBhxhChi;
    }

    public BigDecimal getTienNgoaiBhyt() {
        return tienNgoaiBhyt;
    }

    public void setTienNgoaiBhyt(BigDecimal tienNgoaiBhyt) {
        this.tienNgoaiBhyt = tienNgoaiBhyt;
    }

    public BigDecimal getTongTienThanhToan() {
        return tongTienThanhToan;
    }

    public void setTongTienThanhToan(BigDecimal tongTienThanhToan) {
        this.tongTienThanhToan = tongTienThanhToan;
    }

    public Integer getTyLeBhxhThanhToan() {
        return tyLeBhxhThanhToan;
    }

    public void setTyLeBhxhThanhToan(Integer tyLeBhxhThanhToan) {
        this.tyLeBhxhThanhToan = tyLeBhxhThanhToan;
    }

    public String getDonViTinh() {
        return donViTinh;
    }

    public void setDonViTinh(String donViTinh) {
        this.donViTinh = donViTinh;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ThongTinDichVuKhamDTO)) return false;
        ThongTinDichVuKhamDTO that = (ThongTinDichVuKhamDTO) o;
        return Objects.equals(getId(), that.getId()) &&
            Objects.equals(getTenHienThi(), that.getTenHienThi()) &&
            Objects.equals(getMaDungChung(), that.getMaDungChung()) &&
            Objects.equals(getMaNoiBo(), that.getMaNoiBo()) &&
            Objects.equals(getDonGiaBenhVien(), that.getDonGiaBenhVien()) &&
            Objects.equals(getGiaBhyt(), that.getGiaBhyt()) &&
            Objects.equals(getGiaKhongBhyt(), that.getGiaKhongBhyt()) &&
            Objects.equals(getTenDichVuKhongBaoHiem(), that.getTenDichVuKhongBaoHiem()) &&
            Objects.equals(getTienBenhNhanChi(), that.getTienBenhNhanChi()) &&
            Objects.equals(getTienBhxhChi(), that.getTienBhxhChi()) &&
            Objects.equals(getTienNgoaiBhyt(), that.getTienNgoaiBhyt()) &&
            Objects.equals(getTongTienThanhToan(), that.getTongTienThanhToan()) &&
            Objects.equals(getTyLeBhxhThanhToan(), that.getTyLeBhxhThanhToan()) &&
            Objects.equals(getDonViTinh(), that.getDonViTinh());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getTenHienThi(), getMaDungChung(), getMaNoiBo(), getDonGiaBenhVien(), getGiaBhyt(), getGiaKhongBhyt(), getTenDichVuKhongBaoHiem(), getTienBenhNhanChi(), getTienBhxhChi(), getTienNgoaiBhyt(), getTongTienThanhToan(), getTyLeBhxhThanhToan(), getDonViTinh());
    }

    @Override
    public String toString() {
        return "ThongTinDichVuKhamDTO{" +
            "id=" + id +
            ", tenHienThi='" + tenHienThi + '\'' +
            ", maDungChung='" + maDungChung + '\'' +
            ", maNoiBo='" + maNoiBo + '\'' +
            ", donGiaBenhVien=" + donGiaBenhVien +
            ", giaBhyt=" + giaBhyt +
            ", giaKhongBhyt=" + giaKhongBhyt +
            ", tenDichVuKhongBaoHiem='" + tenDichVuKhongBaoHiem + '\'' +
            ", tienBenhNhanChi=" + tienBenhNhanChi +
            ", tienBhxhChi=" + tienBhxhChi +
            ", tienNgoaiBhyt=" + tienNgoaiBhyt +
            ", tongTienThanhToan=" + tongTienThanhToan +
            ", tyLeBhxhThanhToan=" + tyLeBhxhThanhToan +
            ", donViTinh='" + donViTinh + '\'' +
            '}';
    }
}

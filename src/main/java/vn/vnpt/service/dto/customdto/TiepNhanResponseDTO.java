package vn.vnpt.service.dto.customdto;

import vn.vnpt.domain.ChiDinhDichVuKham;
import vn.vnpt.service.dto.ChiDinhDichVuKhamDTO;

import java.io.Serializable;
import java.util.Objects;

public class TiepNhanResponseDTO implements Serializable {
    private TiepNhanDTO tiepNhanDTO;
    private ChiDinhDichVuKhamDTO chiDinhDichVuKhamDTO;

    public TiepNhanResponseDTO() {
    }

    public TiepNhanResponseDTO(TiepNhanDTO tiepNhanDTO, ChiDinhDichVuKhamDTO chiDinhDichVuKhamDTO) {
        this.tiepNhanDTO = tiepNhanDTO;
        this.chiDinhDichVuKhamDTO = chiDinhDichVuKhamDTO;
    }

    public TiepNhanDTO getTiepNhanDTO() {
        return tiepNhanDTO;
    }

    public void setTiepNhanDTO(TiepNhanDTO tiepNhanDTO) {
        this.tiepNhanDTO = tiepNhanDTO;
    }

    public ChiDinhDichVuKhamDTO getChiDinhDichVuKhamDTO() {
        return chiDinhDichVuKhamDTO;
    }

    public void setChiDinhDichVuKhamDTO(ChiDinhDichVuKhamDTO chiDinhDichVuKhamDTO) {
        this.chiDinhDichVuKhamDTO = chiDinhDichVuKhamDTO;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TiepNhanResponseDTO)) return false;
        TiepNhanResponseDTO that = (TiepNhanResponseDTO) o;
        return Objects.equals(getTiepNhanDTO(), that.getTiepNhanDTO()) &&
            Objects.equals(getChiDinhDichVuKhamDTO(), that.getChiDinhDichVuKhamDTO());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTiepNhanDTO(), getChiDinhDichVuKhamDTO());
    }
}

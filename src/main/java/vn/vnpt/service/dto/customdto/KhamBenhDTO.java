package vn.vnpt.service.dto.customdto;

import vn.vnpt.service.dto.ThongTinKhamBenhDTO;
import vn.vnpt.service.dto.ThongTinKhoaDTO;

import java.io.Serializable;

public class KhamBenhDTO implements Serializable {

    private TiepNhanDTO tiepNhanDTO;
    private ThongTinKhamBenhDTO thongTinKhamBenhDTO;
    private ThongTinKhoaDTO thongTinKhoaDTO;

    public KhamBenhDTO(TiepNhanDTO tiepNhanDTO, ThongTinKhamBenhDTO thongTinKhamBenhDTO, ThongTinKhoaDTO thongTinKhoaDTO) {
        this.tiepNhanDTO = tiepNhanDTO;
        this.thongTinKhamBenhDTO = thongTinKhamBenhDTO;
        this.thongTinKhoaDTO = thongTinKhoaDTO;
    }

    public TiepNhanDTO getTiepNhanDTO() {
        return tiepNhanDTO;
    }

    public void setTiepNhanDTO(TiepNhanDTO tiepNhanDTO) {
        this.tiepNhanDTO = tiepNhanDTO;
    }

    public ThongTinKhamBenhDTO getThongTinKhamBenhDTO() {
        return thongTinKhamBenhDTO;
    }

    public void setThongTinKhamBenhDTO(ThongTinKhamBenhDTO thongTinKhamBenhDTO) {
        this.thongTinKhamBenhDTO = thongTinKhamBenhDTO;
    }

    public ThongTinKhoaDTO getThongTinKhoaDTO() {
        return thongTinKhoaDTO;
    }

    public void setThongTinKhoaDTO(ThongTinKhoaDTO thongTinKhoaDTO) {
        this.thongTinKhoaDTO = thongTinKhoaDTO;
    }

    @Override
    public String toString() {
        return "KhamBenhDTO{" +
            "tiepNhanDTO=" + tiepNhanDTO +
            ", thongTinKhamBenhDTO=" + thongTinKhamBenhDTO +
            ", thongTinKhoaDTO=" + thongTinKhoaDTO +
            '}';
    }
}

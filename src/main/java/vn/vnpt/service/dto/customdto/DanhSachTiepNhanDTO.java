package vn.vnpt.service.dto.customdto;

import org.springframework.beans.factory.annotation.Autowired;
import vn.vnpt.domain.NhanVien;
import vn.vnpt.service.dto.NhanVienDTO;
import vn.vnpt.service.dto.PhongDTO;
import vn.vnpt.service.mapper.custommapper.TiepNhanMapper;

import java.io.Serializable;
import java.util.Objects;

public class DanhSachTiepNhanDTO implements Serializable {
    @Autowired
    private TiepNhanDTO tiepNhanDTO;
    @Autowired
    private NhanVienDTO nhanVienDTO;
    @Autowired
    private PhongDTO phongDTO;

    public DanhSachTiepNhanDTO() {
    }

    public DanhSachTiepNhanDTO(TiepNhanDTO tiepNhanDTO, NhanVienDTO nhanVienDTO, PhongDTO phongDTO){
        this.tiepNhanDTO = tiepNhanDTO;
        this.nhanVienDTO = nhanVienDTO;
        this.phongDTO = phongDTO;
    }

    public TiepNhanDTO getTiepNhanDTO() {
        return tiepNhanDTO;
    }

    public void setTiepNhanDTO(TiepNhanDTO tiepNhanDTO) {
        this.tiepNhanDTO = tiepNhanDTO;
    }

    public NhanVienDTO getNhanVienDTO() {
        return nhanVienDTO;
    }

    public void setNhanVienDTO(NhanVienDTO nhanVienDTO) {
        this.nhanVienDTO = nhanVienDTO;
    }

    public PhongDTO getPhongDTO() {
        return phongDTO;
    }

    public void setPhongDTO(PhongDTO phongDTO) {
        this.phongDTO = phongDTO;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DanhSachTiepNhanDTO)) return false;
        DanhSachTiepNhanDTO that = (DanhSachTiepNhanDTO) o;
        return Objects.equals(getTiepNhanDTO(), that.getTiepNhanDTO()) &&
            Objects.equals(getNhanVienDTO(), that.getNhanVienDTO()) &&
            Objects.equals(getPhongDTO(), that.getPhongDTO());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTiepNhanDTO(), getNhanVienDTO(), getPhongDTO());
    }

    @Override
    public String toString() {
        return "DanhSachTiepNhanDTO{" +
            "tiepNhanDTO=" + tiepNhanDTO +
            ", nhanVienDTO=" + nhanVienDTO +
            ", phongDTO=" + phongDTO +
            '}';
    }
}

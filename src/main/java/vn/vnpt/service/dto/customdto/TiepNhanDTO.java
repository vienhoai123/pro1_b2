package vn.vnpt.service.dto.customdto;


import io.swagger.models.auth.In;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;


public class TiepNhanDTO implements Serializable {

    /**
     * ID bệnh án
     */
    private Long bakbId;
    /**
     * Id của bệnh nhân
     */
    private Long benhNhanId;
    private Long bhytId;
    /**
     * Id đợt điều trị
     */
    private Long dotDieuTriId;

    /**
     * Id đơn vị tiếp nhận bệnh nhân
     */
    private Long donViId;

    /**
     * Mã nhân viên tiếp nhận
     */
    private Long nhanVienTiepNhanId;

    /**
     * Id phòng hoàn tất khám
     */
    private Long phongTiepNhanId;

    /**
     * Thông tin thời gian tiếp nhận bệnh nhân
     */
    @NotNull
    private LocalDate thoiGianTiepNhan;

    /**
     * Trạng thái của Bệnh án khám bệnh:\n1: Chờ khám, mới vào khoa\n2: Đang khám, đang điều trị\n3: Đã khám, xuất viện\n4: Chuyển khoa phòng khám\n5: Chuyển viện, chuyển tuyến\n6: Nhập viện,\n7: Tử vong;\n8: trốn viện;
     */
    @NotNull
    private Integer trangThai;

    /**
     * Trạng thái bệnh nhân đã tiếp nhận\n1: Bệnh nhân cũ\n0: Bệnh nhân mới
     */
    @NotNull
    private Boolean benhNhanCu;

    /**
     * Cảnh báo còn thuộc khi bệnh nhân khám BHYT
     */
    @NotNull
    private Boolean canhBao;

    /**
     * Trạng thái cấp cứu của bệnh nhân\n1: Cấp cứu\n0: Bình thường
     */
    @NotNull
    private Boolean capCuu;

    /**
     * Trạng thái chỉ có năm sinh của bệnh nhân\n1: Chỉ có năm sinh\n0: Đủ thông tin
     */
    @NotNull
    private Boolean chiCoNamSinh;

    /**
     * Thông tin chứng minh nhân dân của bệnh nhân
     */
    @Size(max = 20)
    private String cmndBenhNhan;

    /**
     * Thông tin chứng minh nhân dân của người nhà bệnh nhân
     */
    @Size(max = 20)
    private String cmndNguoiNha;

    /**
     * Trạng thái có bảo hiểm của
     */
    @NotNull
    private Boolean coBaoHiem;

    /**
     * Thông tin địa chỉ của bệnh nhân
     */
    @NotNull
    @Size(max = 500)
    private String diaChi;

    /**
     * Thông tin email của bệnh nhân
     */
    @Size(max = 255)
    private String email;

    /**
     * Trạng thái giới tính của bệnh nhân:\n\n1: Nam\n.\n0: Nữ.
     */
    @NotNull
    private Integer gioiTinh;

    /**
     * Thông tin kháng thể của bệnh nhân
     */
    @Size(max = 5)
    private String khangThe;

    /**
     * Lần khám trong ngày của bệnh nhân
     */
    private Integer lanKhamTrongNgay;

    /**
     * Mã loại giấy tờ của trẻ em như giấy khai sinh, giấy chứng sinh
     */
    private Integer loaiGiayToTreEm;

    /**
     * Thông tin ngày cấp CMND của bệnh nhân
     */
    private LocalDate ngayCapCmnd;

    /**
     * Thời gian miễn cùng chi trả của thẻ BHYT
     */
    private LocalDate ngayMienCungChiTra;

    /**
     * Thôi tin ngày sinh của bệnh nhân
     */
    @NotNull
    private LocalDate ngaySinh;

    /**
     * Thông tin nhóm máu cảu bệnh nhân
     */
    @Size(max = 5)
    private String nhomMau;

    /**
     * Thông tin nơi cấp CMND của bệnh nhân
     */
    @Size(max = 500)
    private String noiCapCmnd;

    /**
     * Thông tin nơi làm việc của bệnh nhân
     */
    @Size(max = 500)
    private String noiLamViec;

    /**
     * Thông tin số điện thoại của bệnh nhân
     */
    @Size(max = 15)
    private String phone;

    /**
     * Thông tin số điện thoại người nhà
     */
    @Size(max = 15)
    private String phoneNguoiNha;

    /**
     * Thông tin quốc tịch của bệnh nhân
     */
    @Size(max = 200)
    private String quocTich;

    /**
     * id quốc tịch của bệnh nhân
     */
    @Size(max = 200)
    private String quocTichId;

    /**
     * Tên bệnh nhân
     */
    @NotNull
    @Size(max = 200)
    private String tenBenhNhan;

    /**
     * Tên người nhà bệnh nhân
     */
    @Size(max = 200)
    private String tenNguoiNha;

    /**
     * Thông tin tháng tuổi nếu là bệnh nhân nhi
     */
    private Integer thangTuoi;



    /**
     * Thông tin tuổi của bệnh nhân
     */
    private Integer tuoi;

    /**
     * Trạng thái ưu tiên của bệnh nhân:\n1: Ưu tiên\n0: Không ưu tiên
     */
    @NotNull
    private Integer uuTien;

    /**
     * Mã bệnh nhân theo công thức USER
     */
    @Size(max = 255)
    private String uuid;


    /**
     * Số bệnh án
     */
    private String soBenhAn;

    /**
     * Số bệnh án theo khoa
     */
    private String soBenhAnKhoa;

    /**
     * Năm tiếp nhận bệnh án
     */
    @NotNull
    private Integer nam;


    /**
     * STT đợt điều trị
     */
    private String soThuTu;

    /**
     * Giá tiền cận trên BHYT
     */
    private Integer bhytCanTren;

    /**
     * Giá tiền cận trên BHYT quy định cho các Dịch vụ kỹ thuật cao
     */
    private Integer bhytCanTrenKtc;

    /**
     * Thông tin mã khu vực của thẻ BHYT
     */
    private String bhytMaKhuVuc;

    /**
     * Thời gian có hiệu lực của BHYT
     */
    private LocalDate bhytNgayBatDau;

    /**
     * Thời gian được hưởng quyền lợi bảo hiểm 5 năm
     */
    private LocalDate bhytNgayDuNamNam;

    /**
     * Thời gian hết hạn của thẻ BHYT
     */
    private LocalDate bhytNgayHetHan;

    /**
     * Trạng thái nội tỉnh của thẻ BHYT:\n1: Nội tỉnh\n0: Ngoại tỉnh
     */
    @NotNull
    private Boolean bhytNoiTinh;

    /**
     * Mã thẻ BHYT
     */
    @Size(max = 30)
    private String bhytSoThe;

    /**
     * Tỷ lệ miễn giảm của thẻ BHYT
     */
    private Integer bhytTyLeMienGiam;

    /**
     * Tỷ lệ miễn giảm cho các dịch vụ kỹ thuật cao của thẻ BHYT
     */
    private Integer bhytTyLeMienGiamKtc;

    /**
     * Thông tin địa chỉ BHYT của bệnh nhân
     */
    @NotNull
    @Size(max = 500)
    private String bhytDiaChi;

    /**
     * Tên đối tượng của thẻ BHYT
     */
    @Size(max = 200)
    private String doiTuongBhytTen;

    /**
     * Trạng thái đúng tuyến của thẻ BHYT\n1: Đúng tuyến\n0: Không đúng tuyến
     */
    @NotNull
    private Boolean dungTuyen;

    /**
     * Mã giấy chứng sinh, giấy khai sinh....
     */
    @Size(max = 200)
    private String giayToTreEm;

    /**
     * Thông tin thẻ TEKT
     */
    @NotNull
    private Boolean theTreEm;

    /**
     * Trạng thái thông tuyến theo bhxh xml 4210:\n1: Thông tuyến\n0: không thông tuyến
     */
    @NotNull
    private Boolean thongTuyenBhxhXml4210;

    /**
     * Loại của bệnh án 1: ngoại trú. 2: Nội trú, 3: điều trị bệnh án ngoại trú
     */
    private Integer loai;

    /**
     * Nơi đăng ký khám chữa bệnh ban đầu
     */
    @Size(max = 30)
    private String bhytNoiDkKcbbd;

    /**
     * Trạng thái của 1 dòng thông tin bệnh nhân\n\n1: Có hiệu lực.\n\n0: Không có hiệu lực.
     */
    @NotNull
    private Boolean enabled;

    /**
     * Thông tin mã số thuế của bệnh nhân
     */
    @Size(max = 100)
    private String maSoThue;

    /**
     * Số nhà của bệnh nhân
     */
    @Size(max = 255)
    private String soNhaXom;

    /**
     * Thông tin ấp, thôn, xóm của bệnh nhân
     */
    @Size(max = 255)
    private String apThon;

    /**
     * Mã danh mục địa phương
     */
    private Long diaPhuongId;

    /**
     * Thông tin địa chỉ thường trú của bệnh nhân
     */
    @NotNull
    @Size(max = 500)
    private String diaChiThuongTru;

    /**
     * Mã dân tộc
     */
    private Long danTocId;

    /**
     * Mã nghề nghiệp
     */
    private Long ngheNghiepId;

    /**
     * Mã phường xã
     */
    private Long phuongXaId;

    /**
     * Mã quận huyện
     */
    private Long quanHuyenId;

    /**
     * Mã tỉnh thành phố
     */
    private Long tinhThanhPhoId;

    private Long userExtraId;



    /**
     * Trạng thái có hiệu lực của dịch vụ:\n1: Còn hiệu lực.\n0: Đã ẩn.
     */
    @NotNull
    private Boolean bhytEnabled;

    /**
     * Mã số BHXH
     */
    @Size(max = 30)
    private String maSoBhxh;

    /**
     * Mã thẻ cũ
     */
    @Size(max = 30)
    private String maTheCu;

    /**
     * Mã QR
     */
    @Size(max = 1000)
    private String qr;


    /**
     * Mã đối tượng BHYT
     */
    private Long doiTuongBhytId;

    /**
     * Mã thông tin BHXH
     */

    private Long thongTinBhxhId;

    /**
     * Mã khoa hoàn tất khám
     */
    private Integer maKhoaHoanTatKham;

    /**
     * Mã phòng hoàn tất khám
     */
    private Integer maPhongHoanTatKham;

    /**
     * Tên khoa hoàn tất khám
     */
    @Size(max = 200)
    private String tenKhoaHoanTatKham;

    /**
     * Tên phòng hoàn tất khám
     */
    @Size(max = 200)
    private String tenPhongHoanTatKham;

    /**
     * Thời gian hoàn tất khám
     */
    private LocalDate thoiGianHoanTatKham;

    public TiepNhanDTO() {
    }

    public Long getBakbId() {
        return bakbId;
    }

    public void setBakbId(Long bakbId) {
        this.bakbId= bakbId;
    }

    public Boolean getBenhNhanCu() {
        return benhNhanCu;
    }

    public void setBenhNhanCu(Boolean benhNhanCu) {
        this.benhNhanCu = benhNhanCu;
    }

    public Boolean getCanhBao() {
        return canhBao;
    }

    public void setCanhBao(Boolean canhBao) {
        this.canhBao = canhBao;
    }

    public Boolean getCapCuu() {
        return capCuu;
    }

    public void setCapCuu(Boolean capCuu) {
        this.capCuu = capCuu;
    }

    public Boolean getChiCoNamSinh() {
        return chiCoNamSinh;
    }

    public void setChiCoNamSinh(Boolean chiCoNamSinh) {
        this.chiCoNamSinh = chiCoNamSinh;
    }

    public String getCmndBenhNhan() {
        return cmndBenhNhan;
    }

    public void setCmndBenhNhan(String cmndBenhNhan) {
        this.cmndBenhNhan = cmndBenhNhan;
    }

    public String getCmndNguoiNha() {
        return cmndNguoiNha;
    }

    public void setCmndNguoiNha(String cmndNguoiNha) {
        this.cmndNguoiNha = cmndNguoiNha;
    }

    public Boolean getCoBaoHiem() {
        return coBaoHiem;
    }

    public void setCoBaoHiem(Boolean coBaoHiem) {
        this.coBaoHiem = coBaoHiem;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getGioiTinh() {
        return gioiTinh;
    }

    public void setGioiTinh(Integer gioiTinh) {
        this.gioiTinh = gioiTinh;
    }

    public String getKhangThe() {
        return khangThe;
    }

    public void setKhangThe(String khangThe) {
        this.khangThe = khangThe;
    }

    public Integer getLanKhamTrongNgay() {
        return lanKhamTrongNgay;
    }

    public void setLanKhamTrongNgay(Integer lanKhamTrongNgay) {
        this.lanKhamTrongNgay = lanKhamTrongNgay;
    }

    public Integer getLoaiGiayToTreEm() {
        return loaiGiayToTreEm;
    }

    public void setLoaiGiayToTreEm(Integer loaiGiayToTreEm) {
        this.loaiGiayToTreEm = loaiGiayToTreEm;
    }

    public LocalDate getNgayCapCmnd() {
        return ngayCapCmnd;
    }

    public void setNgayCapCmnd(LocalDate ngayCapCmnd) {
        this.ngayCapCmnd = ngayCapCmnd;
    }

    public LocalDate getNgayMienCungChiTra() {
        return ngayMienCungChiTra;
    }

    public void setNgayMienCungChiTra(LocalDate ngayMienCungChiTra) {
        this.ngayMienCungChiTra = ngayMienCungChiTra;
    }

    public LocalDate getNgaySinh() {
        return ngaySinh;
    }

    public void setNgaySinh(LocalDate ngaySinh) {
        this.ngaySinh = ngaySinh;
    }

    public String getNhomMau() {
        return nhomMau;
    }

    public void setNhomMau(String nhomMau) {
        this.nhomMau = nhomMau;
    }

    public String getNoiCapCmnd() {
        return noiCapCmnd;
    }

    public void setNoiCapCmnd(String noiCapCmnd) {
        this.noiCapCmnd = noiCapCmnd;
    }

    public String getNoiLamViec() {
        return noiLamViec;
    }

    public void setNoiLamViec(String noiLamViec) {
        this.noiLamViec = noiLamViec;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhoneNguoiNha() {
        return phoneNguoiNha;
    }

    public void setPhoneNguoiNha(String phoneNguoiNha) {
        this.phoneNguoiNha = phoneNguoiNha;
    }

    public String getQuocTich() {
        return quocTich;
    }

    public void setQuocTich(String quocTich) {
        this.quocTich = quocTich;
    }

    public String getQuocTichId() {
        return quocTichId;
    }

    public void setQuocTichId(String quocTichId) {
        this.quocTichId = quocTichId;
    }

    public String getTenBenhNhan() {
        return tenBenhNhan;
    }

    public void setTenBenhNhan(String tenBenhNhan) {
        this.tenBenhNhan = tenBenhNhan;
    }

    public String getTenNguoiNha() {
        return tenNguoiNha;
    }

    public void setTenNguoiNha(String tenNguoiNha) {
        this.tenNguoiNha = tenNguoiNha;
    }

    public Integer getThangTuoi() {
        return thangTuoi;
    }

    public void setThangTuoi(Integer thangTuoi) {
        this.thangTuoi = thangTuoi;
    }

    public LocalDate getThoiGianTiepNhan() {
        return thoiGianTiepNhan;
    }

    public void setThoiGianTiepNhan(LocalDate thoiGianTiepNhan) {
        this.thoiGianTiepNhan = thoiGianTiepNhan;
    }

    public Integer getTuoi() {
        return tuoi;
    }

    public void setTuoi(Integer tuoi) {
        this.tuoi = tuoi;
    }

    public Integer getUuTien() {
        return uuTien;
    }

    public void setUuTien(Integer uuTien) {
        this.uuTien = uuTien;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Integer getTrangThai() {
        return trangThai;
    }

    public void setTrangThai(Integer trangThai) {
        this.trangThai = trangThai;
    }

    public String getSoBenhAn() {
        return soBenhAn;
    }

    public void setSoBenhAn(String soBenhAn) {
        this.soBenhAn = soBenhAn;
    }

    public String getSoBenhAnKhoa() {
        return soBenhAnKhoa;
    }

    public void setSoBenhAnKhoa(String soBenhAnKhoa) {
        this.soBenhAnKhoa = soBenhAnKhoa;
    }

    public Integer getNam() {
        return nam;
    }

    public void setNam(Integer nam) {
        this.nam = nam;
    }

    public Long getBenhNhanId() {
        return benhNhanId;
    }

    public void setBenhNhanId(Long benhNhanId) {
        this.benhNhanId = benhNhanId;
    }

    public Long getDonViId() {
        return donViId;
    }

    public void setDonViId(Long donViId) {
        this.donViId = donViId;
    }

    public Long getNhanVienTiepNhanId() {
        return nhanVienTiepNhanId;
    }

    public void setNhanVienTiepNhanId(Long nhanVienTiepNhanId) {
        this.nhanVienTiepNhanId = nhanVienTiepNhanId;
    }

    public Long getPhongTiepNhanId() {
        return phongTiepNhanId;
    }

    public void setPhongTiepNhanId(Long phongTiepNhanId) {
        this.phongTiepNhanId = phongTiepNhanId;
    }

    public Long getDotDieuTriId() {
        return dotDieuTriId;
    }

    public void setDotDieuTriId(Long dotDieuTriId) {
        this.dotDieuTriId = dotDieuTriId;
    }

    public String getSoThuTu() {
        return soThuTu;
    }

    public void setSoThuTu(String soThuTu) {
        this.soThuTu = soThuTu;
    }

    public Integer getBhytCanTren() {
        return bhytCanTren;
    }

    public void setBhytCanTren(Integer bhytCanTren) {
        this.bhytCanTren = bhytCanTren;
    }

    public Integer getBhytCanTrenKtc() {
        return bhytCanTrenKtc;
    }

    public void setBhytCanTrenKtc(Integer bhytCanTrenKtc) {
        this.bhytCanTrenKtc = bhytCanTrenKtc;
    }

    public String getBhytMaKhuVuc() {
        return bhytMaKhuVuc;
    }

    public void setBhytMaKhuVuc(String bhytMaKhuVuc) {
        this.bhytMaKhuVuc = bhytMaKhuVuc;
    }

    public LocalDate getBhytNgayBatDau() {
        return bhytNgayBatDau;
    }

    public void setBhytNgayBatDau(LocalDate bhytNgayBatDau) {
        this.bhytNgayBatDau = bhytNgayBatDau;
    }

    public LocalDate getBhytNgayDuNamNam() {
        return bhytNgayDuNamNam;
    }

    public void setBhytNgayDuNamNam(LocalDate bhytNgayDuNamNam) {
        this.bhytNgayDuNamNam = bhytNgayDuNamNam;
    }

    public LocalDate getBhytNgayHetHan() {
        return bhytNgayHetHan;
    }

    public void setBhytNgayHetHan(LocalDate bhytNgayHetHan) {
        this.bhytNgayHetHan = bhytNgayHetHan;
    }

    public Boolean getBhytNoiTinh() {
        return bhytNoiTinh;
    }

    public void setBhytNoiTinh(Boolean bhytNoiTinh) {
        this.bhytNoiTinh = bhytNoiTinh;
    }

    public String getBhytSoThe() {
        return bhytSoThe;
    }

    public void setBhytSoThe(String bhytSoThe) {
        this.bhytSoThe = bhytSoThe;
    }

    public Integer getBhytTyLeMienGiam() {
        return bhytTyLeMienGiam;
    }

    public void setBhytTyLeMienGiam(Integer bhytTyLeMienGiam) {
        this.bhytTyLeMienGiam = bhytTyLeMienGiam;
    }

    public Integer getBhytTyLeMienGiamKtc() {
        return bhytTyLeMienGiamKtc;
    }

    public void setBhytTyLeMienGiamKtc(Integer bhytTyLeMienGiamKtc) {
        this.bhytTyLeMienGiamKtc = bhytTyLeMienGiamKtc;
    }

    public String getBhytDiaChi() {
        return bhytDiaChi;
    }

    public void setBhytDiaChi(String bhytDiaChi) {
        this.bhytDiaChi = bhytDiaChi;
    }

    public String getDoiTuongBhytTen() {
        return doiTuongBhytTen;
    }

    public void setDoiTuongBhytTen(String doiTuongBhytTen) {
        this.doiTuongBhytTen = doiTuongBhytTen;
    }

    public Boolean getDungTuyen() {
        return dungTuyen;
    }

    public void setDungTuyen(Boolean dungTuyen) {
        this.dungTuyen = dungTuyen;
    }

    public String getGiayToTreEm() {
        return giayToTreEm;
    }

    public void setGiayToTreEm(String giayToTreEm) {
        this.giayToTreEm = giayToTreEm;
    }

    public Boolean getTheTreEm() {
        return theTreEm;
    }

    public void setTheTreEm(Boolean theTreEm) {
        this.theTreEm = theTreEm;
    }

    public Boolean getThongTuyenBhxhXml4210() {
        return thongTuyenBhxhXml4210;
    }

    public void setThongTuyenBhxhXml4210(Boolean thongTuyenBhxhXml4210) {
        this.thongTuyenBhxhXml4210 = thongTuyenBhxhXml4210;
    }

    public Integer getLoai() {
        return loai;
    }

    public void setLoai(Integer loai) {
        this.loai = loai;
    }

    public String getBhytNoiDkKcbbd() {
        return bhytNoiDkKcbbd;
    }

    public void setBhytNoiDkKcbbd(String bhytNoiDkKcbbd) {
        this.bhytNoiDkKcbbd = bhytNoiDkKcbbd;
    }


    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getMaSoThue() {
        return maSoThue;
    }

    public void setMaSoThue(String maSoThue) {
        this.maSoThue = maSoThue;
    }

    public String getSoNhaXom() {
        return soNhaXom;
    }

    public void setSoNhaXom(String soNhaXom) {
        this.soNhaXom = soNhaXom;
    }

    public String getApThon() {
        return apThon;
    }

    public void setApThon(String apThon) {
        this.apThon = apThon;
    }

    public Long getDiaPhuongId() {
        return diaPhuongId;
    }

    public void setDiaPhuongId(Long diaPhuongId) {
        this.diaPhuongId = diaPhuongId;
    }

    public String getDiaChiThuongTru() {
        return diaChiThuongTru;
    }

    public void setDiaChiThuongTru(String diaChiThuongTru) {
        this.diaChiThuongTru = diaChiThuongTru;
    }

    public Long getDanTocId() {
        return danTocId;
    }

    public void setDanTocId(Long danTocId) {
        this.danTocId = danTocId;
    }

    public Long getNgheNghiepId() {
        return ngheNghiepId;
    }

    public void setNgheNghiepId(Long ngheNghiepId) {
        this.ngheNghiepId = ngheNghiepId;
    }

    public Long getPhuongXaId() {
        return phuongXaId;
    }

    public void setPhuongXaId(Long phuongXaId) {
        this.phuongXaId = phuongXaId;
    }

    public Long getQuanHuyenId() {
        return quanHuyenId;
    }

    public void setQuanHuyenId(Long quanHuyenId) {
        this.quanHuyenId = quanHuyenId;
    }

    public Long getTinhThanhPhoId() {
        return tinhThanhPhoId;
    }

    public void setTinhThanhPhoId(Long tinhThanhPhoId) {
        this.tinhThanhPhoId = tinhThanhPhoId;
    }

    public Long getUserExtraId() {
        return userExtraId;
    }

    public void setUserExtraId(Long userExtraId) {
        this.userExtraId = userExtraId;
    }

    public Long getBhytId() {
        return bhytId;
    }

    public void setBhytId(Long bhytId) {
        this.bhytId = bhytId;
    }

    public Boolean getBhytEnabled() {
        return bhytEnabled;
    }

    public void setBhytEnabled(Boolean bhytEnabled) {
        this.bhytEnabled = bhytEnabled;
    }

    public String getMaSoBhxh() {
        return maSoBhxh;
    }

    public void setMaSoBhxh(String maSoBhxh) {
        this.maSoBhxh = maSoBhxh;
    }

    public String getMaTheCu() {
        return maTheCu;
    }

    public void setMaTheCu(String maTheCu) {
        this.maTheCu = maTheCu;
    }

    public String getQr() {
        return qr;
    }

    public void setQr(String qr) {
        this.qr = qr;
    }

    public Long getDoiTuongBhytId() {
        return doiTuongBhytId;
    }

    public void setDoiTuongBhytId(Long doiTuongBhytId) {
        this.doiTuongBhytId = doiTuongBhytId;
    }

    public Long getThongTinBhxhId() {
        return thongTinBhxhId;
    }

    public void setThongTinBhxhId(Long thongTinBhxhId) {
        this.thongTinBhxhId = thongTinBhxhId;
    }



    public Integer getMaKhoaHoanTatKham() {
        return maKhoaHoanTatKham;
    }

    public void setMaKhoaHoanTatKham(Integer maKhoaHoanTatKham) {
        this.maKhoaHoanTatKham = maKhoaHoanTatKham;
    }

    public Integer getMaPhongHoanTatKham() {
        return maPhongHoanTatKham;
    }

    public void setMaPhongHoanTatKham(Integer maPhongHoanTatKham) {
        this.maPhongHoanTatKham = maPhongHoanTatKham;
    }

    public String getTenKhoaHoanTatKham() {
        return tenKhoaHoanTatKham;
    }

    public void setTenKhoaHoanTatKham(String tenKhoaHoanTatKham) {
        this.tenKhoaHoanTatKham = tenKhoaHoanTatKham;
    }

    public String getTenPhongHoanTatKham() {
        return tenPhongHoanTatKham;
    }

    public void setTenPhongHoanTatKham(String tenPhongHoanTatKham) {
        this.tenPhongHoanTatKham = tenPhongHoanTatKham;
    }

    @Override
    public String toString() {
        return "TiepNhanDTO{" +
            "bakbId=" + bakbId +
            ", benhNhanId=" + benhNhanId +
            ", bhytId=" + bhytId +
            ", dotDieuTriId=" + dotDieuTriId +
            ", donViId=" + donViId +
            ", nhanVienTiepNhanId=" + nhanVienTiepNhanId +
            ", phongTiepNhanId=" + phongTiepNhanId +
            ", thoiGianTiepNhan=" + thoiGianTiepNhan +
            ", trangThai=" + trangThai +
            ", benhNhanCu=" + benhNhanCu +
            ", canhBao=" + canhBao +
            ", capCuu=" + capCuu +
            ", chiCoNamSinh=" + chiCoNamSinh +
            ", cmndBenhNhan='" + cmndBenhNhan + '\'' +
            ", cmndNguoiNha='" + cmndNguoiNha + '\'' +
            ", coBaoHiem=" + coBaoHiem +
            ", diaChi='" + diaChi + '\'' +
            ", email='" + email + '\'' +
            ", gioiTinh=" + gioiTinh +
            ", khangThe='" + khangThe + '\'' +
            ", lanKhamTrongNgay=" + lanKhamTrongNgay +
            ", loaiGiayToTreEm=" + loaiGiayToTreEm +
            ", ngayCapCmnd=" + ngayCapCmnd +
            ", ngayMienCungChiTra=" + ngayMienCungChiTra +
            ", ngaySinh=" + ngaySinh +
            ", nhomMau='" + nhomMau + '\'' +
            ", noiCapCmnd='" + noiCapCmnd + '\'' +
            ", noiLamViec='" + noiLamViec + '\'' +
            ", phone='" + phone + '\'' +
            ", phoneNguoiNha='" + phoneNguoiNha + '\'' +
            ", quocTich='" + quocTich + '\'' +
            ", quocTichId='" + quocTichId + '\'' +
            ", tenBenhNhan='" + tenBenhNhan + '\'' +
            ", tenNguoiNha='" + tenNguoiNha + '\'' +
            ", thangTuoi=" + thangTuoi +
            ", tuoi=" + tuoi +
            ", uuTien=" + uuTien +
            ", uuid='" + uuid + '\'' +
            ", soBenhAn='" + soBenhAn + '\'' +
            ", soBenhAnKhoa='" + soBenhAnKhoa + '\'' +
            ", nam=" + nam +
            ", soThuTu='" + soThuTu + '\'' +
            ", bhytCanTren=" + bhytCanTren +
            ", bhytCanTrenKtc=" + bhytCanTrenKtc +
            ", bhytMaKhuVuc='" + bhytMaKhuVuc + '\'' +
            ", bhytNgayBatDau=" + bhytNgayBatDau +
            ", bhytNgayDuNamNam=" + bhytNgayDuNamNam +
            ", bhytNgayHetHan=" + bhytNgayHetHan +
            ", bhytNoiTinh=" + bhytNoiTinh +
            ", bhytSoThe='" + bhytSoThe + '\'' +
            ", bhytTyLeMienGiam=" + bhytTyLeMienGiam +
            ", bhytTyLeMienGiamKtc=" + bhytTyLeMienGiamKtc +
            ", bhytDiaChi='" + bhytDiaChi + '\'' +
            ", doiTuongBhytTen='" + doiTuongBhytTen + '\'' +
            ", dungTuyen=" + dungTuyen +
            ", giayToTreEm='" + giayToTreEm + '\'' +
            ", theTreEm=" + theTreEm +
            ", thongTuyenBhxhXml4210=" + thongTuyenBhxhXml4210 +
            ", loai=" + loai +
            ", bhytNoiDkKcbbd='" + bhytNoiDkKcbbd + '\'' +
            ", enabled=" + enabled +
            ", maSoThue='" + maSoThue + '\'' +
            ", soNhaXom='" + soNhaXom + '\'' +
            ", apThon='" + apThon + '\'' +
            ", diaPhuongId=" + diaPhuongId +
            ", diaChiThuongTru='" + diaChiThuongTru + '\'' +
            ", danTocId=" + danTocId +
            ", ngheNghiepId=" + ngheNghiepId +
            ", phuongXaId=" + phuongXaId +
            ", quanHuyenId=" + quanHuyenId +
            ", tinhThanhPhoId=" + tinhThanhPhoId +
            ", userExtraId=" + userExtraId +
            ", bhytEnabled=" + bhytEnabled +
            ", maSoBhxh='" + maSoBhxh + '\'' +
            ", maTheCu='" + maTheCu + '\'' +
            ", qr='" + qr + '\'' +
            ", doiTuongBhytId=" + doiTuongBhytId +
            ", thongTinBhxhId=" + thongTinBhxhId +
            ", maKhoaHoanTatKham=" + maKhoaHoanTatKham +
            ", maPhongHoanTatKham=" + maPhongHoanTatKham +
            ", tenKhoaHoanTatKham='" + tenKhoaHoanTatKham + '\'' +
            ", tenPhongHoanTatKham='" + tenPhongHoanTatKham + '\'' +
            ", thoiGianHoanTatKham=" + thoiGianHoanTatKham +
            '}';
    }

    public LocalDate getThoiGianHoanTatKham() {
        return thoiGianHoanTatKham;
    }

    public void setThoiGianHoanTatKham(LocalDate thoiGianHoanTatKham) {
        this.thoiGianHoanTatKham = thoiGianHoanTatKham;
    }

}

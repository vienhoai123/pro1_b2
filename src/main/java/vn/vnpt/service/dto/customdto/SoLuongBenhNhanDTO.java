package vn.vnpt.service.dto.customdto;

public class SoLuongBenhNhanDTO {
    Long phongID;
    String tenPhong;
    Integer soLuongBN;
    Integer soLuongBNCoBhyt;
    Integer trangThai;

    public SoLuongBenhNhanDTO() {
    }

    public Long getPhongID() {
        return phongID;
    }

    public void setPhongID(Long phongID) {
        this.phongID = phongID;
    }

    public String getTenPhong() {
        return tenPhong;
    }

    public void setTenPhong(String tenPhong) {
        this.tenPhong = tenPhong;
    }

    public Integer getSoLuongBN() {
        return soLuongBN;
    }

    public void setSoLuongBN(Integer soLuongBN) {
        this.soLuongBN = soLuongBN;
    }

    public Integer getSoLuongBNCoBhyt() {
        return soLuongBNCoBhyt;
    }

    public void setSoLuongBNCoBhyt(Integer soLuongBNCoBhyt) {
        this.soLuongBNCoBhyt = soLuongBNCoBhyt;
    }

    public Integer getTrangThai() {
        return trangThai;
    }

    public void setTrangThai(Integer trangThai) {
        this.trangThai = trangThai;
    }

    @Override
    public String toString() {
        return "SoLuongBenhNhanDTO{" +
            "phongID=" + phongID +
            ", tenPhong='" + tenPhong + '\'' +
            ", soLuongBN=" + soLuongBN +
            ", soLuongBNCoBhyt=" + soLuongBNCoBhyt +
            ", trangThai=" + trangThai +
            '}';
    }
}

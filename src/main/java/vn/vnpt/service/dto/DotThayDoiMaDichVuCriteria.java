package vn.vnpt.service.dto;

import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * Criteria class for the {@link vn.vnpt.domain.DotThayDoiMaDichVu} entity. This class is used
 * in {@link vn.vnpt.web.rest.DotThayDoiMaDichVuResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /dot-thay-doi-ma-dich-vus?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class DotThayDoiMaDichVuCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter dotTuongApDung;

    private StringFilter ghiChu;

    private LocalDateFilter ngayApDung;

    private StringFilter ten;

    private LongFilter donViId;

    public DotThayDoiMaDichVuCriteria() {
    }

    public DotThayDoiMaDichVuCriteria(DotThayDoiMaDichVuCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.dotTuongApDung = other.dotTuongApDung == null ? null : other.dotTuongApDung.copy();
        this.ghiChu = other.ghiChu == null ? null : other.ghiChu.copy();
        this.ngayApDung = other.ngayApDung == null ? null : other.ngayApDung.copy();
        this.ten = other.ten == null ? null : other.ten.copy();
        this.donViId = other.donViId == null ? null : other.donViId.copy();
    }

    @Override
    public DotThayDoiMaDichVuCriteria copy() {
        return new DotThayDoiMaDichVuCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getDotTuongApDung() {
        return dotTuongApDung;
    }

    public void setDotTuongApDung(IntegerFilter dotTuongApDung) {
        this.dotTuongApDung = dotTuongApDung;
    }

    public StringFilter getGhiChu() {
        return ghiChu;
    }

    public void setGhiChu(StringFilter ghiChu) {
        this.ghiChu = ghiChu;
    }

    public LocalDateFilter getNgayApDung() {
        return ngayApDung;
    }

    public void setNgayApDung(LocalDateFilter ngayApDung) {
        this.ngayApDung = ngayApDung;
    }

    public StringFilter getTen() {
        return ten;
    }

    public void setTen(StringFilter ten) {
        this.ten = ten;
    }

    public LongFilter getDonViId() {
        return donViId;
    }

    public void setDonViId(LongFilter donViId) {
        this.donViId = donViId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final DotThayDoiMaDichVuCriteria that = (DotThayDoiMaDichVuCriteria) o;
        return
            Objects.equals(id, that.id) &&
                Objects.equals(dotTuongApDung, that.dotTuongApDung) &&
                Objects.equals(ghiChu, that.ghiChu) &&
                Objects.equals(ngayApDung, that.ngayApDung) &&
                Objects.equals(ten, that.ten) &&
                Objects.equals(donViId, that.donViId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            dotTuongApDung,
            ghiChu,
            ngayApDung,
            ten,
            donViId
        );
    }

    @Override
    public String toString() {
        return "DotThayDoiMaDichVuCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (dotTuongApDung != null ? "dotTuongApDung=" + dotTuongApDung + ", " : "") +
            (ghiChu != null ? "ghiChu=" + ghiChu + ", " : "") +
            (ngayApDung != null ? "ngayApDung=" + ngayApDung + ", " : "") +
            (ten != null ? "ten=" + ten + ", " : "") +
            (donViId != null ? "donViId=" + donViId + ", " : "") +
            "}";
    }

}

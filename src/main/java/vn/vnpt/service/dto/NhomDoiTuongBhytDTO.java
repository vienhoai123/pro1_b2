package vn.vnpt.service.dto;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link vn.vnpt.domain.NhomDoiTuongBhyt} entity.
 */
public class NhomDoiTuongBhytDTO implements Serializable {
    
    private Long id;

    /**
     * Tên nhóm đối tượng BHYT
     */
    @NotNull
    @Size(max = 500)
    @ApiModelProperty(value = "Tên nhóm đối tượng BHYT", required = true)
    private String ten;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        NhomDoiTuongBhytDTO nhomDoiTuongBhytDTO = (NhomDoiTuongBhytDTO) o;
        if (nhomDoiTuongBhytDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), nhomDoiTuongBhytDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "NhomDoiTuongBhytDTO{" +
            "id=" + getId() +
            ", ten='" + getTen() + "'" +
            "}";
    }
}

package vn.vnpt.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link vn.vnpt.domain.DanToc} entity.
 */
@ApiModel(description = "NDB_TABLE=READ_BACKUP=1 Thông tin theo tên dân tộc")
public class DanTocDTO implements Serializable {
    
    private Long id;

    /**
     * Mã dân tộc
     */
    @NotNull
    @ApiModelProperty(value = "Mã dân tộc", required = true)
    private Integer ma4069Byt;

    /**
     * Mã dân tộc theo cục thống kê
     */
    @NotNull
    @ApiModelProperty(value = "Mã dân tộc theo cục thống kê", required = true)
    private Integer maCucThongKe;

    /**
     * Tên dân tộc
     */
    @NotNull
    @Size(max = 500)
    @ApiModelProperty(value = "Tên dân tộc", required = true)
    private String ten4069Byt;

    /**
     * Tên dân tộc theo cục thống kê
     */
    @NotNull
    @Size(max = 500)
    @ApiModelProperty(value = "Tên dân tộc theo cục thống kê", required = true)
    private String tenCucThongKe;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getMa4069Byt() {
        return ma4069Byt;
    }

    public void setMa4069Byt(Integer ma4069Byt) {
        this.ma4069Byt = ma4069Byt;
    }

    public Integer getMaCucThongKe() {
        return maCucThongKe;
    }

    public void setMaCucThongKe(Integer maCucThongKe) {
        this.maCucThongKe = maCucThongKe;
    }

    public String getTen4069Byt() {
        return ten4069Byt;
    }

    public void setTen4069Byt(String ten4069Byt) {
        this.ten4069Byt = ten4069Byt;
    }

    public String getTenCucThongKe() {
        return tenCucThongKe;
    }

    public void setTenCucThongKe(String tenCucThongKe) {
        this.tenCucThongKe = tenCucThongKe;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DanTocDTO danTocDTO = (DanTocDTO) o;
        if (danTocDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), danTocDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DanTocDTO{" +
            "id=" + getId() +
            ", ma4069Byt=" + getMa4069Byt() +
            ", maCucThongKe=" + getMaCucThongKe() +
            ", ten4069Byt='" + getTen4069Byt() + "'" +
            ", tenCucThongKe='" + getTenCucThongKe() + "'" +
            "}";
    }
}

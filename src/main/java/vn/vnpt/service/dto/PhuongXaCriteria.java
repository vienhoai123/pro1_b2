package vn.vnpt.service.dto;

import java.io.Serializable;
import java.util.Objects;

import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link vn.vnpt.domain.PhuongXa} entity. This class is used
 * in {@link vn.vnpt.web.rest.PhuongXaResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /phuong-xas?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class PhuongXaCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter cap;

    private StringFilter guessPhrase;

    private StringFilter ten;

    private StringFilter tenKhongDau;

    private LongFilter quanHuyenId;

    public PhuongXaCriteria() {
    }

    public PhuongXaCriteria(PhuongXaCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.cap = other.cap == null ? null : other.cap.copy();
        this.guessPhrase = other.guessPhrase == null ? null : other.guessPhrase.copy();
        this.ten = other.ten == null ? null : other.ten.copy();
        this.tenKhongDau = other.tenKhongDau == null ? null : other.tenKhongDau.copy();
        this.quanHuyenId = other.quanHuyenId == null ? null : other.quanHuyenId.copy();
    }

    @Override
    public PhuongXaCriteria copy() {
        return new PhuongXaCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCap() {
        return cap;
    }

    public void setCap(StringFilter cap) {
        this.cap = cap;
    }

    public StringFilter getGuessPhrase() {
        return guessPhrase;
    }

    public void setGuessPhrase(StringFilter guessPhrase) {
        this.guessPhrase = guessPhrase;
    }

    public StringFilter getTen() {
        return ten;
    }

    public void setTen(StringFilter ten) {
        this.ten = ten;
    }

    public StringFilter getTenKhongDau() {
        return tenKhongDau;
    }

    public void setTenKhongDau(StringFilter tenKhongDau) {
        this.tenKhongDau = tenKhongDau;
    }

    public LongFilter getQuanHuyenId() {
        return quanHuyenId;
    }

    public void setQuanHuyenId(LongFilter quanHuyenId) {
        this.quanHuyenId = quanHuyenId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final PhuongXaCriteria that = (PhuongXaCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(cap, that.cap) &&
            Objects.equals(guessPhrase, that.guessPhrase) &&
            Objects.equals(ten, that.ten) &&
            Objects.equals(tenKhongDau, that.tenKhongDau) &&
            Objects.equals(quanHuyenId, that.quanHuyenId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        cap,
        guessPhrase,
        ten,
        tenKhongDau,
        quanHuyenId
        );
    }

    @Override
    public String toString() {
        return "PhuongXaCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (cap != null ? "cap=" + cap + ", " : "") +
                (guessPhrase != null ? "guessPhrase=" + guessPhrase + ", " : "") +
                (ten != null ? "ten=" + ten + ", " : "") +
                (tenKhongDau != null ? "tenKhongDau=" + tenKhongDau + ", " : "") +
                (quanHuyenId != null ? "quanHuyenId=" + quanHuyenId + ", " : "") +
            "}";
    }

}

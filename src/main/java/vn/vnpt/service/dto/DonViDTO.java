package vn.vnpt.service.dto;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A DTO for the {@link vn.vnpt.domain.DonVi} entity.
 */
public class DonViDTO implements Serializable {
    
    private Long id;

    /**
     * Cấp đơn vị
     */
    @NotNull
    @ApiModelProperty(value = "Cấp đơn vị", required = true)
    private Integer cap;

    /**
     * Thông tin chi nhánh ngân hàng
     */
    @Size(max = 300)
    @ApiModelProperty(value = "Thông tin chi nhánh ngân hàng")
    private String chiNhanhNganHang;

    /**
     * Địa chỉ của đơn vị
     */
    @Size(max = 1000)
    @ApiModelProperty(value = "Địa chỉ của đơn vị")
    private String diaChi;

    /**
     * Mã đơn vị quản lý
     */
    @ApiModelProperty(value = "Mã đơn vị quản lý")
    private BigDecimal donViQuanLyId;

    /**
     * Đơn vị trực thuộc
     */
    @Size(max = 10)
    @ApiModelProperty(value = "Đơn vị trực thuộc")
    private String dvtt;

    /**
     * email
     */
    @Size(max = 50)
    @ApiModelProperty(value = "email")
    private String email;

    /**
     * Trạng thái hiệu lực của dòng dữ liệu: 1: Có hiệu lực. 0: Không có hiệu lực
     */
    @NotNull
    @ApiModelProperty(value = "Trạng thái hiệu lực của dòng dữ liệu: 1: Có hiệu lực. 0: Không có hiệu lực", required = true)
    private Boolean enabled;

    /**
     * Ký hiệu của đơn vị
     */
    @Size(max = 100)
    @ApiModelProperty(value = "Ký hiệu của đơn vị")
    private String kyHieu;

    /**
     * Loại của đơn vị
     */
    @NotNull
    @ApiModelProperty(value = "Loại của đơn vị", required = true)
    private Integer loai;

    /**
     * Mã tỉnh
     */
    @NotNull
    @Size(max = 10)
    @ApiModelProperty(value = "Mã tỉnh", required = true)
    private String maTinh;

    /**
     * Số điện thoại
     */
    @Size(max = 15)
    @ApiModelProperty(value = "Số điện thoại")
    private String phone;

    /**
     * Số tài khoản của đơn vị
     */
    @Size(max = 30)
    @ApiModelProperty(value = "Số tài khoản của đơn vị")
    private String soTaiKhoan;

    /**
     * Tên của đơn vị
     */
    @NotNull
    @Size(max = 300)
    @ApiModelProperty(value = "Tên của đơn vị", required = true)
    private String ten;

    /**
     * Tên thông tin ngân hàng
     */
    @Size(max = 300)
    @ApiModelProperty(value = "Tên thông tin ngân hàng")
    private String tenNganHang;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCap() {
        return cap;
    }

    public void setCap(Integer cap) {
        this.cap = cap;
    }

    public String getChiNhanhNganHang() {
        return chiNhanhNganHang;
    }

    public void setChiNhanhNganHang(String chiNhanhNganHang) {
        this.chiNhanhNganHang = chiNhanhNganHang;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    public BigDecimal getDonViQuanLyId() {
        return donViQuanLyId;
    }

    public void setDonViQuanLyId(BigDecimal donViQuanLyId) {
        this.donViQuanLyId = donViQuanLyId;
    }

    public String getDvtt() {
        return dvtt;
    }

    public void setDvtt(String dvtt) {
        this.dvtt = dvtt;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getKyHieu() {
        return kyHieu;
    }

    public void setKyHieu(String kyHieu) {
        this.kyHieu = kyHieu;
    }

    public Integer getLoai() {
        return loai;
    }

    public void setLoai(Integer loai) {
        this.loai = loai;
    }

    public String getMaTinh() {
        return maTinh;
    }

    public void setMaTinh(String maTinh) {
        this.maTinh = maTinh;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSoTaiKhoan() {
        return soTaiKhoan;
    }

    public void setSoTaiKhoan(String soTaiKhoan) {
        this.soTaiKhoan = soTaiKhoan;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getTenNganHang() {
        return tenNganHang;
    }

    public void setTenNganHang(String tenNganHang) {
        this.tenNganHang = tenNganHang;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DonViDTO donViDTO = (DonViDTO) o;
        if (donViDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), donViDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DonViDTO{" +
            "id=" + getId() +
            ", cap=" + getCap() +
            ", chiNhanhNganHang='" + getChiNhanhNganHang() + "'" +
            ", diaChi='" + getDiaChi() + "'" +
            ", donViQuanLyId=" + getDonViQuanLyId() +
            ", dvtt='" + getDvtt() + "'" +
            ", email='" + getEmail() + "'" +
            ", enabled='" + isEnabled() + "'" +
            ", kyHieu='" + getKyHieu() + "'" +
            ", loai=" + getLoai() +
            ", maTinh='" + getMaTinh() + "'" +
            ", phone='" + getPhone() + "'" +
            ", soTaiKhoan='" + getSoTaiKhoan() + "'" +
            ", ten='" + getTen() + "'" +
            ", tenNganHang='" + getTenNganHang() + "'" +
            "}";
    }
}

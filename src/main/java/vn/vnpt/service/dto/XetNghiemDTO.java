package vn.vnpt.service.dto;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A DTO for the {@link vn.vnpt.domain.XetNghiem} entity.
 */
public class XetNghiemDTO implements Serializable {
    
    private Long id;

    /**
     * Cận dưới nam
     */
    @NotNull
    @ApiModelProperty(value = "Cận dưới nam", required = true)
    private BigDecimal canDuoiNam;

    /**
     * Cận dưới nữ
     */
    @NotNull
    @ApiModelProperty(value = "Cận dưới nữ", required = true)
    private BigDecimal canDuoiNu;

    /**
     * Cận trên nam
     */
    @NotNull
    @ApiModelProperty(value = "Cận trên nam", required = true)
    private BigDecimal canTrenNam;

    /**
     * Cận trên nữ
     */
    @NotNull
    @ApiModelProperty(value = "Cận trên nữ", required = true)
    private BigDecimal canTrenNu;

    /**
     * Chỉ số bình thường nam
     */
    @Size(max = 100)
    @ApiModelProperty(value = "Chỉ số bình thường nam")
    private String chiSoBinhThuongNam;

    /**
     * Chỉ số max
     */
    @NotNull
    @ApiModelProperty(value = "Chỉ số max", required = true)
    private BigDecimal chiSoMax;

    /**
     * Chỉ số min
     */
    @NotNull
    @ApiModelProperty(value = "Chỉ số min", required = true)
    private BigDecimal chiSoMin;

    /**
     * Công thức
     */
    @Size(max = 100)
    @ApiModelProperty(value = "Công thức")
    private String congThuc;

    /**
     * Trạng thái xóa bỏ của dòng dữ liệu. \n1: Đã xóa. \n0: Còn hiệu lực.
     */
    @NotNull
    @ApiModelProperty(value = "Trạng thái xóa bỏ của dòng dữ liệu. \n1: Đã xóa. \n0: Còn hiệu lực.", required = true)
    private Boolean deleted;

    /**
     * Độ pha loãng
     */
    @Size(max = 10)
    @ApiModelProperty(value = "Độ pha loãng")
    private String doPhaLoang;

    /**
     * Đơn giá bệnh viện
     */
    @NotNull
    @ApiModelProperty(value = "Đơn giá bệnh viện", required = true)
    private BigDecimal donGiaBenhVien;

    /**
     * Đơn vị tính
     */
    @Size(max = 30)
    @ApiModelProperty(value = "Đơn vị tính")
    private String donViTinh;

    /**
     * Trạng thái có hiệu lực của dịch vụ: \r\n1: Còn hiệu lực. \r\n0: Đã ẩn.
     */
    @NotNull
    @ApiModelProperty(value = "Trạng thái có hiệu lực của dịch vụ: \r\n1: Còn hiệu lực. \r\n0: Đã ẩn.", required = true)
    private Boolean enabled;

    /**
     * Số lượng giới hạn của 1 chỉ định
     */
    @ApiModelProperty(value = "Số lượng giới hạn của 1 chỉ định")
    private BigDecimal gioiHanChiDinh;

    /**
     * Kết quả bất thường
     */
    @Size(max = 100)
    @ApiModelProperty(value = "Kết quả bất thường")
    private String ketQuaBatThuong;

    /**
     * Mã dùng chung
     */
    @Size(max = 45)
    @ApiModelProperty(value = "Mã dùng chung")
    private String maDungChung;

    /**
     * Kết quả mặc định
     */
    @Size(max = 500)
    @ApiModelProperty(value = "Kết quả mặc định")
    private String ketQuaMacDinh;

    /**
     * Trạng thái phạm vi của chỉ định:\r\n0: Chỉ có thu phí. \r\n1: Có cả BHYT và thu phí
     */
    @NotNull
    @ApiModelProperty(value = "Trạng thái phạm vi của chỉ định:\r\n0: Chỉ có thu phí. \r\n1: Có cả BHYT và thu phí", required = true)
    private Boolean phamViChiDinh;

    /**
     * Trạng thái phân biệt theo giới tính của chỉ định
     */
    @NotNull
    @ApiModelProperty(value = "Trạng thái phân biệt theo giới tính của chỉ định", required = true)
    private Boolean phanTheoGioiTInh;

    /**
     * Làm tròn kết quả đến bao nhiêu chữ số lẻ
     */
    @NotNull
    @ApiModelProperty(value = "Làm tròn kết quả đến bao nhiêu chữ số lẻ", required = true)
    private Integer soLeLamTron;

    /**
     * Số lần thực hiện trong ngày
     */
    @NotNull
    @ApiModelProperty(value = "Số lần thực hiện trong ngày", required = true)
    private BigDecimal soLuongThucHien;

    /**
     * Tên của chỉ định
     */
    @NotNull
    @Size(max = 500)
    @ApiModelProperty(value = "Tên của chỉ định", required = true)
    private String ten;

    /**
     * Tên hiển thị của chỉ định
     */
    @NotNull
    @Size(max = 500)
    @ApiModelProperty(value = "Tên hiển thị của chỉ định", required = true)
    private String tenHienThi;

    /**
     * Mã nội bộ
     */
    @NotNull
    @Size(max = 255)
    @ApiModelProperty(value = "Mã nội bộ", required = true)
    private String maNoiBo;


    private Long donViId;

    private Long dotMaId;

    private Long loaiXNId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getCanDuoiNam() {
        return canDuoiNam;
    }

    public void setCanDuoiNam(BigDecimal canDuoiNam) {
        this.canDuoiNam = canDuoiNam;
    }

    public BigDecimal getCanDuoiNu() {
        return canDuoiNu;
    }

    public void setCanDuoiNu(BigDecimal canDuoiNu) {
        this.canDuoiNu = canDuoiNu;
    }

    public BigDecimal getCanTrenNam() {
        return canTrenNam;
    }

    public void setCanTrenNam(BigDecimal canTrenNam) {
        this.canTrenNam = canTrenNam;
    }

    public BigDecimal getCanTrenNu() {
        return canTrenNu;
    }

    public void setCanTrenNu(BigDecimal canTrenNu) {
        this.canTrenNu = canTrenNu;
    }

    public String getChiSoBinhThuongNam() {
        return chiSoBinhThuongNam;
    }

    public void setChiSoBinhThuongNam(String chiSoBinhThuongNam) {
        this.chiSoBinhThuongNam = chiSoBinhThuongNam;
    }

    public BigDecimal getChiSoMax() {
        return chiSoMax;
    }

    public void setChiSoMax(BigDecimal chiSoMax) {
        this.chiSoMax = chiSoMax;
    }

    public BigDecimal getChiSoMin() {
        return chiSoMin;
    }

    public void setChiSoMin(BigDecimal chiSoMin) {
        this.chiSoMin = chiSoMin;
    }

    public String getCongThuc() {
        return congThuc;
    }

    public void setCongThuc(String congThuc) {
        this.congThuc = congThuc;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public String getDoPhaLoang() {
        return doPhaLoang;
    }

    public void setDoPhaLoang(String doPhaLoang) {
        this.doPhaLoang = doPhaLoang;
    }

    public BigDecimal getDonGiaBenhVien() {
        return donGiaBenhVien;
    }

    public void setDonGiaBenhVien(BigDecimal donGiaBenhVien) {
        this.donGiaBenhVien = donGiaBenhVien;
    }

    public String getDonViTinh() {
        return donViTinh;
    }

    public void setDonViTinh(String donViTinh) {
        this.donViTinh = donViTinh;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public BigDecimal getGioiHanChiDinh() {
        return gioiHanChiDinh;
    }

    public void setGioiHanChiDinh(BigDecimal gioiHanChiDinh) {
        this.gioiHanChiDinh = gioiHanChiDinh;
    }

    public String getKetQuaBatThuong() {
        return ketQuaBatThuong;
    }

    public void setKetQuaBatThuong(String ketQuaBatThuong) {
        this.ketQuaBatThuong = ketQuaBatThuong;
    }

    public String getMaDungChung() {
        return maDungChung;
    }

    public void setMaDungChung(String maDungChung) {
        this.maDungChung = maDungChung;
    }

    public String getKetQuaMacDinh() {
        return ketQuaMacDinh;
    }

    public void setKetQuaMacDinh(String ketQuaMacDinh) {
        this.ketQuaMacDinh = ketQuaMacDinh;
    }

    public Boolean isPhamViChiDinh() {
        return phamViChiDinh;
    }

    public void setPhamViChiDinh(Boolean phamViChiDinh) {
        this.phamViChiDinh = phamViChiDinh;
    }

    public Boolean isPhanTheoGioiTInh() {
        return phanTheoGioiTInh;
    }

    public void setPhanTheoGioiTInh(Boolean phanTheoGioiTInh) {
        this.phanTheoGioiTInh = phanTheoGioiTInh;
    }

    public Integer getSoLeLamTron() {
        return soLeLamTron;
    }

    public void setSoLeLamTron(Integer soLeLamTron) {
        this.soLeLamTron = soLeLamTron;
    }

    public BigDecimal getSoLuongThucHien() {
        return soLuongThucHien;
    }

    public void setSoLuongThucHien(BigDecimal soLuongThucHien) {
        this.soLuongThucHien = soLuongThucHien;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getTenHienThi() {
        return tenHienThi;
    }

    public void setTenHienThi(String tenHienThi) {
        this.tenHienThi = tenHienThi;
    }

    public String getMaNoiBo() {
        return maNoiBo;
    }

    public void setMaNoiBo(String maNoiBo) {
        this.maNoiBo = maNoiBo;
    }

    public Long getDonViId() {
        return donViId;
    }

    public void setDonViId(Long donViId) {
        this.donViId = donViId;
    }

    public Long getDotMaId() {
        return dotMaId;
    }

    public void setDotMaId(Long dotThayDoiMaDichVuId) {
        this.dotMaId = dotThayDoiMaDichVuId;
    }

    public Long getLoaiXNId() {
        return loaiXNId;
    }

    public void setLoaiXNId(Long loaiXetNghiemId) {
        this.loaiXNId = loaiXetNghiemId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        XetNghiemDTO xetNghiemDTO = (XetNghiemDTO) o;
        if (xetNghiemDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), xetNghiemDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "XetNghiemDTO{" +
            "id=" + getId() +
            ", canDuoiNam=" + getCanDuoiNam() +
            ", canDuoiNu=" + getCanDuoiNu() +
            ", canTrenNam=" + getCanTrenNam() +
            ", canTrenNu=" + getCanTrenNu() +
            ", chiSoBinhThuongNam='" + getChiSoBinhThuongNam() + "'" +
            ", chiSoMax=" + getChiSoMax() +
            ", chiSoMin=" + getChiSoMin() +
            ", congThuc='" + getCongThuc() + "'" +
            ", deleted='" + isDeleted() + "'" +
            ", doPhaLoang='" + getDoPhaLoang() + "'" +
            ", donGiaBenhVien=" + getDonGiaBenhVien() +
            ", donViTinh='" + getDonViTinh() + "'" +
            ", enabled='" + isEnabled() + "'" +
            ", gioiHanChiDinh=" + getGioiHanChiDinh() +
            ", ketQuaBatThuong='" + getKetQuaBatThuong() + "'" +
            ", maDungChung='" + getMaDungChung() + "'" +
            ", ketQuaMacDinh='" + getKetQuaMacDinh() + "'" +
            ", phamViChiDinh='" + isPhamViChiDinh() + "'" +
            ", phanTheoGioiTInh='" + isPhanTheoGioiTInh() + "'" +
            ", soLeLamTron=" + getSoLeLamTron() +
            ", soLuongThucHien=" + getSoLuongThucHien() +
            ", ten='" + getTen() + "'" +
            ", tenHienThi='" + getTenHienThi() + "'" +
            ", maNoiBo='" + getMaNoiBo() + "'" +
            ", donViId=" + getDonViId() +
            ", dotMaId=" + getDotMaId() +
            ", loaiXNId=" + getLoaiXNId() +
            "}";
    }
}

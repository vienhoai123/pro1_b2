package vn.vnpt.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link vn.vnpt.domain.ChucVu} entity.
 */
public class ChucVuDTO implements Serializable {
    
    private Long id;

    @Size(max = 500)
    private String moTa;

    @NotNull
    @Size(max = 500)
    private String ten;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ChucVuDTO chucVuDTO = (ChucVuDTO) o;
        if (chucVuDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), chucVuDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ChucVuDTO{" +
            "id=" + getId() +
            ", moTa='" + getMoTa() + "'" +
            ", ten='" + getTen() + "'" +
            "}";
    }
}

package vn.vnpt.service.dto;

import io.swagger.annotations.ApiModelProperty;
import vn.vnpt.domain.ThongTinKhamBenhId;

import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * A DTO for the {@link vn.vnpt.domain.ThongTinKhamBenh} entity.
 */
public class ThongTinKhamBenhDTO implements Serializable {

    private Long id;

    /**
     * Chỉ số BMI
     */
    @ApiModelProperty(value = "Chỉ số BMI")
    private BigDecimal bmi;

    /**
     * Cân nặng
     */
    @ApiModelProperty(value = "Cân nặng")
    private BigDecimal canNang;

    /**
     * Chiều cao (tính bằng cm)
     */
    @ApiModelProperty(value = "Chiều cao (tính bằng cm)")
    private Integer chieuCao;

    /**
     * Creatinin
     */
    @ApiModelProperty(value = "Creatinin")
    private BigDecimal creatinin;

    /**
     * Dấu hiệu lâm sàng
     */
    @Size(max = 500)
    @ApiModelProperty(value = "Dấu hiệu lâm sàng")
    private String dauHieuLamSang;

    /**
     * Độ thành thải Creatinin
     */
    @ApiModelProperty(value = "Độ thành thải Creatinin")
    private BigDecimal doThanhThai;

    /**
     * Huyết áp cao
     */
    @ApiModelProperty(value = "Huyết áp cao")
    private Integer huyetApCao;

    /**
     * Huyết áp thấp
     */
    @ApiModelProperty(value = "Huyết áp thấp")
    private Integer huyetApThap;

    /**
     * ICD
     */
    @Size(max = 50)
    @ApiModelProperty(value = "ICD")
    private String icd;

    /**
     * Lần hẹn
     */
    @ApiModelProperty(value = "Lần hẹn")
    private Integer lanHen;

    /**
     * Lời dặn
     */
    @Size(max = 500)
    @ApiModelProperty(value = "Lời dặn")
    private String loiDan;

    /**
     * Lý do chuyển
     */
    @Size(max = 2000)
    @ApiModelProperty(value = "Lý do chuyển")
    private String lyDoChuyen;

    /**
     * Mã bệnh y học cổ truyền
     */
    @Size(max = 100)
    @ApiModelProperty(value = "Mã bệnh y học cổ truyền")
    private String maBenhYhct;

    /**
     * Mạch
     */
    @ApiModelProperty(value = "Mạch")
    private Integer mach;

    /**
     * Ngày ra toa tối đa
     */
    @ApiModelProperty(value = "Ngày ra toa tối đa")
    private Integer maxNgayRaToa;

    /**
     * Ngày hẹn
     */
    @ApiModelProperty(value = "Ngày hẹn")
    private LocalDate ngayHen;

    /**
     * Nhận định BMI
     */
    @Size(max = 500)
    @ApiModelProperty(value = "Nhận định BMI")
    private String nhanDinhBmi;

    /**
     * Nhận định độ thanh thải
     */
    @Size(max = 500)
    @ApiModelProperty(value = "Nhận định độ thanh thải")
    private String nhanDinhDoThanhThai;

    /**
     * Trận thái nhập viện. 0: Không nhập viện. 1: Nhập viện
     */
    @NotNull
    @ApiModelProperty(value = "Trận thái nhập viện. 0: Không nhập viện. 1: Nhập viện", required = true)
    private Boolean nhapVien;

    /**
     * Nhiệt độ
     */
    @ApiModelProperty(value = "Nhiệt độ")
    private BigDecimal nhietDo;

    /**
     * Nhiệp thở
     */
    @ApiModelProperty(value = "Nhiệp thở")
    private Integer nhipTho;

    /**
     * Phương pháp điều trị y học cổ truyền
     */
    @Size(max = 2000)
    @ApiModelProperty(value = "Phương pháp điều trị y học cổ truyền")
    private String ppDieuTriYtct;

    /**
     * Số lần in bảng kê
     */
    @ApiModelProperty(value = "Số lần in bảng kê")
    private Integer soLanInBangKe;

    /**
     * Số lần in toa thuốc
     */
    @ApiModelProperty(value = "Số lần in toa thuốc")
    private Integer soLanInToaThuoc;

    /**
     * Tên bệnh theo icd
     */
    @Size(max = 500)
    @ApiModelProperty(value = "Tên bệnh theo icd")
    private String tenBenhIcd;

    /**
     * Tên bệnh theo bác sĩ
     */
    @Size(max = 500)
    @ApiModelProperty(value = "Tên bệnh theo bác sĩ")
    private String tenBenhTheoBacSi;

    /**
     * Tên bệnh y học cổ truyền
     */
    @Size(max = 500)
    @ApiModelProperty(value = "Tên bệnh y học cổ truyền")
    private String tenBenhYhct;

    /**
     * Trạng thái đã chẩn đoán hình ảnh: 0: Chưa thực hiện đủ: 1: Đã thực hiện đủ
     */
    @NotNull
    @ApiModelProperty(value = "Trạng thái đã chẩn đoán hình ảnh: 0: Chưa thực hiện đủ: 1: Đã thực hiện đủ", required = true)
    private Boolean trangThaiCdha;

    /**
     * Trạng thái đã thực hiện ttpt: 0: Chưa thực hiện đủ: 1: Đã thực hiện đủ
     */
    @NotNull
    @ApiModelProperty(value = "Trạng thái đã thực hiện ttpt: 0: Chưa thực hiện đủ: 1: Đã thực hiện đủ", required = true)
    private Boolean trangThaiTtpt;

    /**
     * Trạng thái đã xét nghiệm: 0: Chưa thực hiện đủ: 1: Đã thực hiện đủ
     */
    @NotNull
    @ApiModelProperty(value = "Trạng thái đã xét nghiệm: 0: Chưa thực hiện đủ: 1: Đã thực hiện đủ", required = true)
    private Boolean trangThaiXetNghiem;

    /**
     * Triệu chứng lâm sàng
     */
    @Size(max = 255)
    @ApiModelProperty(value = "Triệu chứng lâm sàng")
    private String trieuChungLamSang;

    /**
     * Vòng bụng
     */
    @ApiModelProperty(value = "Vòng bụng")
    private Integer vongBung;

    /**
     * Trạng thái lần khám bệnh; 1 chờ khám; 2 đang khám, 3 đã khám, 4 kết thúc bắt buộc, 5 chuyển phòng khám, 6 chuyển viện, 7 nhập viện nội trú từ ngoại trú
     */
    @ApiModelProperty(value = "Trạng thái lần khám bệnh; 1 chờ khám; 2 đang khám, 3 đã khám, 4 kết thúc bắt buộc, 5 chuyển phòng khám, 6 chuyển viện, 7 nhập viện nội trú từ ngoại trú")
    private Integer trangThai;

    /**
     * Thời gian kết thúc khám
     */
    @ApiModelProperty(value = "Thời gian kết thúc khám")
    private LocalDate thoiGianKetThucKham;

    /**
     * Mã phòng chuyển từ
     */
    @ApiModelProperty(value = "Mã phòng chuyển từ")
    private Long phongIdChuyenTu;

    /**
     * Thời gian khám bệnh
     */
    @ApiModelProperty(value = "Thời gian khám bệnh")
    private LocalDate thoiGianKhamBenh;

    /**
     * Nội dung y lệnh
     */
    @Size(max = 255)
    @ApiModelProperty(value = "Nội dung y lệnh")
    private String yLenh;

    /**
     * Loại y lệnh: 0: Bình thường. 1: Tây y: 2 Đông Y
     */
    @ApiModelProperty(value = "Loại y lệnh: 0: Bình thường. 1: Tây y: 2 Đông Y")
    private Integer loaiYLenh;

    /**
     * Chẩn đoán
     */
    @Size(max = 255)
    @ApiModelProperty(value = "Chẩn đoán")
    private String chanDoan;

    /**
     * Loại chẩn đoán: 0: Bình thường. 1: Tây y: 2 Đông Y
     */
    @ApiModelProperty(value = "Loại chẩn đoán: 0: Bình thường. 1: Tây y: 2 Đông Y")
    private Integer loaiChanDoan;

    @NotNull
    private Integer nam;


    private Long bakbId;

    private Long donViId;

    private Long benhNhanId;

    private Long dotDieuTriId;

    private Long thongTinKhoaId;
    /**
     * Mã nhân viên
     */
    @ApiModelProperty(value = "Mã nhân viên")

    private Long nhanVienId;
    /**
     * Mã hướng điều trị
     */
    @ApiModelProperty(value = "Mã hướng điều trị")

    private Long huongDieuTriId;
    /**
     * Mã phòng
     */
    @ApiModelProperty(value = "Mã phòng")

    private Long phongId;

    public ThongTinKhamBenhId getCompositeId(){
        ThongTinKhamBenhId result = new ThongTinKhamBenhId();
        result.setId(this.id);
        result.setThongTinKhoaId(this.thongTinKhoaId);
        result.setDotDieuTriId(this.dotDieuTriId);
        result.setBakbId(this.bakbId);
        result.setBenhNhanId(this.benhNhanId);
        result.setDonViId(this.donViId);
        return result;
    }

    public void setCompositeId(ThongTinKhamBenhId cId){
        this.id = cId.getId();
        this.thongTinKhoaId = cId.getThongTinKhoaId();
        this.dotDieuTriId = cId.getDotDieuTriId();
        this.bakbId = cId.getBakbId();
        this.benhNhanId = cId.getBenhNhanId();
        this.donViId = cId.getDonViId();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getBmi() {
        return bmi;
    }

    public void setBmi(BigDecimal bmi) {
        this.bmi = bmi;
    }

    public BigDecimal getCanNang() {
        return canNang;
    }

    public void setCanNang(BigDecimal canNang) {
        this.canNang = canNang;
    }

    public Integer getChieuCao() {
        return chieuCao;
    }

    public void setChieuCao(Integer chieuCao) {
        this.chieuCao = chieuCao;
    }

    public BigDecimal getCreatinin() {
        return creatinin;
    }

    public void setCreatinin(BigDecimal creatinin) {
        this.creatinin = creatinin;
    }

    public String getDauHieuLamSang() {
        return dauHieuLamSang;
    }

    public void setDauHieuLamSang(String dauHieuLamSang) {
        this.dauHieuLamSang = dauHieuLamSang;
    }

    public BigDecimal getDoThanhThai() {
        return doThanhThai;
    }

    public void setDoThanhThai(BigDecimal doThanhThai) {
        this.doThanhThai = doThanhThai;
    }

    public Integer getHuyetApCao() {
        return huyetApCao;
    }

    public void setHuyetApCao(Integer huyetApCao) {
        this.huyetApCao = huyetApCao;
    }

    public Integer getHuyetApThap() {
        return huyetApThap;
    }

    public void setHuyetApThap(Integer huyetApThap) {
        this.huyetApThap = huyetApThap;
    }

    public String getIcd() {
        return icd;
    }

    public void setIcd(String icd) {
        this.icd = icd;
    }

    public Integer getLanHen() {
        return lanHen;
    }

    public void setLanHen(Integer lanHen) {
        this.lanHen = lanHen;
    }

    public String getLoiDan() {
        return loiDan;
    }

    public void setLoiDan(String loiDan) {
        this.loiDan = loiDan;
    }

    public String getLyDoChuyen() {
        return lyDoChuyen;
    }

    public void setLyDoChuyen(String lyDoChuyen) {
        this.lyDoChuyen = lyDoChuyen;
    }

    public String getMaBenhYhct() {
        return maBenhYhct;
    }

    public void setMaBenhYhct(String maBenhYhct) {
        this.maBenhYhct = maBenhYhct;
    }

    public Integer getMach() {
        return mach;
    }

    public void setMach(Integer mach) {
        this.mach = mach;
    }

    public Integer getMaxNgayRaToa() {
        return maxNgayRaToa;
    }

    public void setMaxNgayRaToa(Integer maxNgayRaToa) {
        this.maxNgayRaToa = maxNgayRaToa;
    }

    public LocalDate getNgayHen() {
        return ngayHen;
    }

    public void setNgayHen(LocalDate ngayHen) {
        this.ngayHen = ngayHen;
    }

    public String getNhanDinhBmi() {
        return nhanDinhBmi;
    }

    public void setNhanDinhBmi(String nhanDinhBmi) {
        this.nhanDinhBmi = nhanDinhBmi;
    }

    public String getNhanDinhDoThanhThai() {
        return nhanDinhDoThanhThai;
    }

    public void setNhanDinhDoThanhThai(String nhanDinhDoThanhThai) {
        this.nhanDinhDoThanhThai = nhanDinhDoThanhThai;
    }

    public Boolean isNhapVien() {
        return nhapVien;
    }

    public void setNhapVien(Boolean nhapVien) {
        this.nhapVien = nhapVien;
    }

    public BigDecimal getNhietDo() {
        return nhietDo;
    }

    public void setNhietDo(BigDecimal nhietDo) {
        this.nhietDo = nhietDo;
    }

    public Integer getNhipTho() {
        return nhipTho;
    }

    public void setNhipTho(Integer nhipTho) {
        this.nhipTho = nhipTho;
    }

    public String getPpDieuTriYtct() {
        return ppDieuTriYtct;
    }

    public void setPpDieuTriYtct(String ppDieuTriYtct) {
        this.ppDieuTriYtct = ppDieuTriYtct;
    }

    public Integer getSoLanInBangKe() {
        return soLanInBangKe;
    }

    public void setSoLanInBangKe(Integer soLanInBangKe) {
        this.soLanInBangKe = soLanInBangKe;
    }

    public Integer getSoLanInToaThuoc() {
        return soLanInToaThuoc;
    }

    public void setSoLanInToaThuoc(Integer soLanInToaThuoc) {
        this.soLanInToaThuoc = soLanInToaThuoc;
    }

    public String getTenBenhIcd() {
        return tenBenhIcd;
    }

    public void setTenBenhIcd(String tenBenhIcd) {
        this.tenBenhIcd = tenBenhIcd;
    }

    public String getTenBenhTheoBacSi() {
        return tenBenhTheoBacSi;
    }

    public void setTenBenhTheoBacSi(String tenBenhTheoBacSi) {
        this.tenBenhTheoBacSi = tenBenhTheoBacSi;
    }

    public String getTenBenhYhct() {
        return tenBenhYhct;
    }

    public void setTenBenhYhct(String tenBenhYhct) {
        this.tenBenhYhct = tenBenhYhct;
    }

    public Boolean isTrangThaiCdha() {
        return trangThaiCdha;
    }

    public void setTrangThaiCdha(Boolean trangThaiCdha) {
        this.trangThaiCdha = trangThaiCdha;
    }

    public Boolean isTrangThaiTtpt() {
        return trangThaiTtpt;
    }

    public void setTrangThaiTtpt(Boolean trangThaiTtpt) {
        this.trangThaiTtpt = trangThaiTtpt;
    }

    public Boolean isTrangThaiXetNghiem() {
        return trangThaiXetNghiem;
    }

    public void setTrangThaiXetNghiem(Boolean trangThaiXetNghiem) {
        this.trangThaiXetNghiem = trangThaiXetNghiem;
    }

    public String getTrieuChungLamSang() {
        return trieuChungLamSang;
    }

    public void setTrieuChungLamSang(String trieuChungLamSang) {
        this.trieuChungLamSang = trieuChungLamSang;
    }

    public Integer getVongBung() {
        return vongBung;
    }

    public void setVongBung(Integer vongBung) {
        this.vongBung = vongBung;
    }

    public Integer getTrangThai() {
        return trangThai;
    }

    public void setTrangThai(Integer trangThai) {
        this.trangThai = trangThai;
    }

    public LocalDate getThoiGianKetThucKham() {
        return thoiGianKetThucKham;
    }

    public void setThoiGianKetThucKham(LocalDate thoiGianKetThucKham) {
        this.thoiGianKetThucKham = thoiGianKetThucKham;
    }

    public Long getPhongIdChuyenTu() {
        return phongIdChuyenTu;
    }

    public void setPhongIdChuyenTu(Long phongIdChuyenTu) {
        this.phongIdChuyenTu = phongIdChuyenTu;
    }

    public LocalDate getThoiGianKhamBenh() {
        return thoiGianKhamBenh;
    }

    public void setThoiGianKhamBenh(LocalDate thoiGianKhamBenh) {
        this.thoiGianKhamBenh = thoiGianKhamBenh;
    }

    public String getyLenh() {
        return yLenh;
    }

    public void setyLenh(String yLenh) {
        this.yLenh = yLenh;
    }

    public Integer getLoaiYLenh() {
        return loaiYLenh;
    }

    public void setLoaiYLenh(Integer loaiYLenh) {
        this.loaiYLenh = loaiYLenh;
    }

    public String getChanDoan() {
        return chanDoan;
    }

    public void setChanDoan(String chanDoan) {
        this.chanDoan = chanDoan;
    }

    public Integer getLoaiChanDoan() {
        return loaiChanDoan;
    }

    public void setLoaiChanDoan(Integer loaiChanDoan) {
        this.loaiChanDoan = loaiChanDoan;
    }

    public Integer getNam() {
        return nam;
    }

    public void setNam(Integer nam) {
        this.nam = nam;
    }

    public Long getBakbId() {
        return bakbId;
    }

    public void setBakbId(Long thongTinKhoaId) {
        this.bakbId = thongTinKhoaId;
    }

    public Long getDonViId() {
        return donViId;
    }

    public void setDonViId(Long thongTinKhoaId) {
        this.donViId = thongTinKhoaId;
    }

    public Long getBenhNhanId() {
        return benhNhanId;
    }

    public void setBenhNhanId(Long thongTinKhoaId) {
        this.benhNhanId = thongTinKhoaId;
    }

    public Long getDotDieuTriId() {
        return dotDieuTriId;
    }

    public void setDotDieuTriId(Long thongTinKhoaId) {
        this.dotDieuTriId = thongTinKhoaId;
    }

    public Long getThongTinKhoaId() {
        return thongTinKhoaId;
    }

    public void setThongTinKhoaId(Long thongTinKhoaId) {
        this.thongTinKhoaId = thongTinKhoaId;
    }

    public Long getNhanVienId() {
        return nhanVienId;
    }

    public void setNhanVienId(Long nhanVienId) {
        this.nhanVienId = nhanVienId;
    }

    public Long getHuongDieuTriId() {
        return huongDieuTriId;
    }

    public void setHuongDieuTriId(Long huongDieuTriId) {
        this.huongDieuTriId = huongDieuTriId;
    }

    public Long getPhongId() {
        return phongId;
    }

    public void setPhongId(Long phongId) {
        this.phongId = phongId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ThongTinKhamBenhDTO)) {
            return false;
        }

        return id != null && id.equals(((ThongTinKhamBenhDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ThongTinKhamBenhDTO{" +
            "id=" + getId() +
            ", bmi=" + getBmi() +
            ", canNang=" + getCanNang() +
            ", chieuCao=" + getChieuCao() +
            ", creatinin=" + getCreatinin() +
            ", dauHieuLamSang='" + getDauHieuLamSang() + "'" +
            ", doThanhThai=" + getDoThanhThai() +
            ", huyetApCao=" + getHuyetApCao() +
            ", huyetApThap=" + getHuyetApThap() +
            ", icd='" + getIcd() + "'" +
            ", lanHen=" + getLanHen() +
            ", loiDan='" + getLoiDan() + "'" +
            ", lyDoChuyen='" + getLyDoChuyen() + "'" +
            ", maBenhYhct='" + getMaBenhYhct() + "'" +
            ", mach=" + getMach() +
            ", maxNgayRaToa=" + getMaxNgayRaToa() +
            ", ngayHen='" + getNgayHen() + "'" +
            ", nhanDinhBmi='" + getNhanDinhBmi() + "'" +
            ", nhanDinhDoThanhThai='" + getNhanDinhDoThanhThai() + "'" +
            ", nhapVien='" + isNhapVien() + "'" +
            ", nhietDo=" + getNhietDo() +
            ", nhipTho=" + getNhipTho() +
            ", ppDieuTriYtct='" + getPpDieuTriYtct() + "'" +
            ", soLanInBangKe=" + getSoLanInBangKe() +
            ", soLanInToaThuoc=" + getSoLanInToaThuoc() +
            ", tenBenhIcd='" + getTenBenhIcd() + "'" +
            ", tenBenhTheoBacSi='" + getTenBenhTheoBacSi() + "'" +
            ", tenBenhYhct='" + getTenBenhYhct() + "'" +
            ", trangThaiCdha='" + isTrangThaiCdha() + "'" +
            ", trangThaiTtpt='" + isTrangThaiTtpt() + "'" +
            ", trangThaiXetNghiem='" + isTrangThaiXetNghiem() + "'" +
            ", trieuChungLamSang='" + getTrieuChungLamSang() + "'" +
            ", vongBung=" + getVongBung() +
            ", trangThai=" + getTrangThai() +
            ", thoiGianKetThucKham='" + getThoiGianKetThucKham() + "'" +
            ", phongIdChuyenTu=" + getPhongIdChuyenTu() +
            ", thoiGianKhamBenh='" + getThoiGianKhamBenh() + "'" +
            ", yLenh='" + getyLenh() + "'" +
            ", loaiYLenh=" + getLoaiYLenh() +
            ", chanDoan='" + getChanDoan() + "'" +
            ", loaiChanDoan=" + getLoaiChanDoan() +
            ", nam=" + getNam() +
            ", bakbId=" + getBakbId() +
            ", donViId=" + getDonViId() +
            ", benhNhanId=" + getBenhNhanId() +
            ", dotDieuTriId=" + getDotDieuTriId() +
            ", thongTinKhoaId=" + getThongTinKhoaId() +
            ", nhanVienId=" + getNhanVienId() +
            ", huongDieuTriId=" + getHuongDieuTriId() +
            ", phongId=" + getPhongId() +
            "}";
    }
}

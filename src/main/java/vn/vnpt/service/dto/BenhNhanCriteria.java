package vn.vnpt.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link vn.vnpt.domain.BenhNhan} entity. This class is used
 * in {@link vn.vnpt.web.rest.BenhNhanResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /benh-nhans?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class BenhNhanCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private BooleanFilter chiCoNamSinh;

    private StringFilter cmnd;

    private StringFilter email;

    private BooleanFilter enabled;

    private IntegerFilter gioiTinh;

    private StringFilter khangThe;

    private StringFilter maSoThue;

    private LocalDateFilter ngayCapCmnd;

    private LocalDateFilter ngaySinh;

    private StringFilter nhomMau;

    private StringFilter noiCapCmnd;

    private StringFilter noiLamViec;

    private StringFilter phone;

    private StringFilter ten;

    private StringFilter soNhaXom;

    private StringFilter apThon;

    private LongFilter diaPhuongId;

    private StringFilter diaChiThuongTru;

    private LongFilter danTocId;

    private LongFilter ngheNghiepId;

    private LongFilter quocTichId;

    private LongFilter phuongXaId;

    private LongFilter quanHuyenId;

    private LongFilter tinhThanhPhoId;

    private LongFilter userExtraId;

    public BenhNhanCriteria() {
    }

    public BenhNhanCriteria(BenhNhanCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.chiCoNamSinh = other.chiCoNamSinh == null ? null : other.chiCoNamSinh.copy();
        this.cmnd = other.cmnd == null ? null : other.cmnd.copy();
        this.email = other.email == null ? null : other.email.copy();
        this.enabled = other.enabled == null ? null : other.enabled.copy();
        this.gioiTinh = other.gioiTinh == null ? null : other.gioiTinh.copy();
        this.khangThe = other.khangThe == null ? null : other.khangThe.copy();
        this.maSoThue = other.maSoThue == null ? null : other.maSoThue.copy();
        this.ngayCapCmnd = other.ngayCapCmnd == null ? null : other.ngayCapCmnd.copy();
        this.ngaySinh = other.ngaySinh == null ? null : other.ngaySinh.copy();
        this.nhomMau = other.nhomMau == null ? null : other.nhomMau.copy();
        this.noiCapCmnd = other.noiCapCmnd == null ? null : other.noiCapCmnd.copy();
        this.noiLamViec = other.noiLamViec == null ? null : other.noiLamViec.copy();
        this.phone = other.phone == null ? null : other.phone.copy();
        this.ten = other.ten == null ? null : other.ten.copy();
        this.soNhaXom = other.soNhaXom == null ? null : other.soNhaXom.copy();
        this.apThon = other.apThon == null ? null : other.apThon.copy();
        this.diaPhuongId = other.diaPhuongId == null ? null : other.diaPhuongId.copy();
        this.diaChiThuongTru = other.diaChiThuongTru == null ? null : other.diaChiThuongTru.copy();
        this.danTocId = other.danTocId == null ? null : other.danTocId.copy();
        this.ngheNghiepId = other.ngheNghiepId == null ? null : other.ngheNghiepId.copy();
        this.quocTichId = other.quocTichId == null ? null : other.quocTichId.copy();
        this.phuongXaId = other.phuongXaId == null ? null : other.phuongXaId.copy();
        this.quanHuyenId = other.quanHuyenId == null ? null : other.quanHuyenId.copy();
        this.tinhThanhPhoId = other.tinhThanhPhoId == null ? null : other.tinhThanhPhoId.copy();
        this.userExtraId = other.userExtraId == null ? null : other.userExtraId.copy();
    }

    @Override
    public BenhNhanCriteria copy() {
        return new BenhNhanCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public BooleanFilter getChiCoNamSinh() {
        return chiCoNamSinh;
    }

    public void setChiCoNamSinh(BooleanFilter chiCoNamSinh) {
        this.chiCoNamSinh = chiCoNamSinh;
    }

    public StringFilter getCmnd() {
        return cmnd;
    }

    public void setCmnd(StringFilter cmnd) {
        this.cmnd = cmnd;
    }

    public StringFilter getEmail() {
        return email;
    }

    public void setEmail(StringFilter email) {
        this.email = email;
    }

    public BooleanFilter getEnabled() {
        return enabled;
    }

    public void setEnabled(BooleanFilter enabled) {
        this.enabled = enabled;
    }

    public IntegerFilter getGioiTinh() {
        return gioiTinh;
    }

    public void setGioiTinh(IntegerFilter gioiTinh) {
        this.gioiTinh = gioiTinh;
    }

    public StringFilter getKhangThe() {
        return khangThe;
    }

    public void setKhangThe(StringFilter khangThe) {
        this.khangThe = khangThe;
    }

    public StringFilter getMaSoThue() {
        return maSoThue;
    }

    public void setMaSoThue(StringFilter maSoThue) {
        this.maSoThue = maSoThue;
    }

    public LocalDateFilter getNgayCapCmnd() {
        return ngayCapCmnd;
    }

    public void setNgayCapCmnd(LocalDateFilter ngayCapCmnd) {
        this.ngayCapCmnd = ngayCapCmnd;
    }

    public LocalDateFilter getNgaySinh() {
        return ngaySinh;
    }

    public void setNgaySinh(LocalDateFilter ngaySinh) {
        this.ngaySinh = ngaySinh;
    }

    public StringFilter getNhomMau() {
        return nhomMau;
    }

    public void setNhomMau(StringFilter nhomMau) {
        this.nhomMau = nhomMau;
    }

    public StringFilter getNoiCapCmnd() {
        return noiCapCmnd;
    }

    public void setNoiCapCmnd(StringFilter noiCapCmnd) {
        this.noiCapCmnd = noiCapCmnd;
    }

    public StringFilter getNoiLamViec() {
        return noiLamViec;
    }

    public void setNoiLamViec(StringFilter noiLamViec) {
        this.noiLamViec = noiLamViec;
    }

    public StringFilter getPhone() {
        return phone;
    }

    public void setPhone(StringFilter phone) {
        this.phone = phone;
    }

    public StringFilter getTen() {
        return ten;
    }

    public void setTen(StringFilter ten) {
        this.ten = ten;
    }

    public StringFilter getSoNhaXom() {
        return soNhaXom;
    }

    public void setSoNhaXom(StringFilter soNhaXom) {
        this.soNhaXom = soNhaXom;
    }

    public StringFilter getApThon() {
        return apThon;
    }

    public void setApThon(StringFilter apThon) {
        this.apThon = apThon;
    }

    public LongFilter getDiaPhuongId() {
        return diaPhuongId;
    }

    public void setDiaPhuongId(LongFilter diaPhuongId) {
        this.diaPhuongId = diaPhuongId;
    }

    public StringFilter getDiaChiThuongTru() {
        return diaChiThuongTru;
    }

    public void setDiaChiThuongTru(StringFilter diaChiThuongTru) {
        this.diaChiThuongTru = diaChiThuongTru;
    }

    public LongFilter getDanTocId() {
        return danTocId;
    }

    public void setDanTocId(LongFilter danTocId) {
        this.danTocId = danTocId;
    }

    public LongFilter getNgheNghiepId() {
        return ngheNghiepId;
    }

    public void setNgheNghiepId(LongFilter ngheNghiepId) {
        this.ngheNghiepId = ngheNghiepId;
    }

    public LongFilter getQuocTichId() {
        return quocTichId;
    }

    public void setQuocTichId(LongFilter quocTichId) {
        this.quocTichId = quocTichId;
    }

    public LongFilter getPhuongXaId() {
        return phuongXaId;
    }

    public void setPhuongXaId(LongFilter phuongXaId) {
        this.phuongXaId = phuongXaId;
    }

    public LongFilter getQuanHuyenId() {
        return quanHuyenId;
    }

    public void setQuanHuyenId(LongFilter quanHuyenId) {
        this.quanHuyenId = quanHuyenId;
    }

    public LongFilter getTinhThanhPhoId() {
        return tinhThanhPhoId;
    }

    public void setTinhThanhPhoId(LongFilter tinhThanhPhoId) {
        this.tinhThanhPhoId = tinhThanhPhoId;
    }

    public LongFilter getUserExtraId() {
        return userExtraId;
    }

    public void setUserExtraId(LongFilter userExtraId) {
        this.userExtraId = userExtraId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final BenhNhanCriteria that = (BenhNhanCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(chiCoNamSinh, that.chiCoNamSinh) &&
            Objects.equals(cmnd, that.cmnd) &&
            Objects.equals(email, that.email) &&
            Objects.equals(enabled, that.enabled) &&
            Objects.equals(gioiTinh, that.gioiTinh) &&
            Objects.equals(khangThe, that.khangThe) &&
            Objects.equals(maSoThue, that.maSoThue) &&
            Objects.equals(ngayCapCmnd, that.ngayCapCmnd) &&
            Objects.equals(ngaySinh, that.ngaySinh) &&
            Objects.equals(nhomMau, that.nhomMau) &&
            Objects.equals(noiCapCmnd, that.noiCapCmnd) &&
            Objects.equals(noiLamViec, that.noiLamViec) &&
            Objects.equals(phone, that.phone) &&
            Objects.equals(ten, that.ten) &&
            Objects.equals(soNhaXom, that.soNhaXom) &&
            Objects.equals(apThon, that.apThon) &&
            Objects.equals(diaPhuongId, that.diaPhuongId) &&
            Objects.equals(diaChiThuongTru, that.diaChiThuongTru) &&
            Objects.equals(danTocId, that.danTocId) &&
            Objects.equals(ngheNghiepId, that.ngheNghiepId) &&
            Objects.equals(quocTichId, that.quocTichId) &&
            Objects.equals(phuongXaId, that.phuongXaId) &&
            Objects.equals(quanHuyenId, that.quanHuyenId) &&
            Objects.equals(tinhThanhPhoId, that.tinhThanhPhoId) &&
            Objects.equals(userExtraId, that.userExtraId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        chiCoNamSinh,
        cmnd,
        email,
        enabled,
        gioiTinh,
        khangThe,
        maSoThue,
        ngayCapCmnd,
        ngaySinh,
        nhomMau,
        noiCapCmnd,
        noiLamViec,
        phone,
        ten,
        soNhaXom,
        apThon,
        diaPhuongId,
        diaChiThuongTru,
        danTocId,
        ngheNghiepId,
        quocTichId,
        phuongXaId,
        quanHuyenId,
        tinhThanhPhoId,
        userExtraId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "BenhNhanCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (chiCoNamSinh != null ? "chiCoNamSinh=" + chiCoNamSinh + ", " : "") +
                (cmnd != null ? "cmnd=" + cmnd + ", " : "") +
                (email != null ? "email=" + email + ", " : "") +
                (enabled != null ? "enabled=" + enabled + ", " : "") +
                (gioiTinh != null ? "gioiTinh=" + gioiTinh + ", " : "") +
                (khangThe != null ? "khangThe=" + khangThe + ", " : "") +
                (maSoThue != null ? "maSoThue=" + maSoThue + ", " : "") +
                (ngayCapCmnd != null ? "ngayCapCmnd=" + ngayCapCmnd + ", " : "") +
                (ngaySinh != null ? "ngaySinh=" + ngaySinh + ", " : "") +
                (nhomMau != null ? "nhomMau=" + nhomMau + ", " : "") +
                (noiCapCmnd != null ? "noiCapCmnd=" + noiCapCmnd + ", " : "") +
                (noiLamViec != null ? "noiLamViec=" + noiLamViec + ", " : "") +
                (phone != null ? "phone=" + phone + ", " : "") +
                (ten != null ? "ten=" + ten + ", " : "") +
                (soNhaXom != null ? "soNhaXom=" + soNhaXom + ", " : "") +
                (apThon != null ? "apThon=" + apThon + ", " : "") +
                (diaPhuongId != null ? "diaPhuongId=" + diaPhuongId + ", " : "") +
                (diaChiThuongTru != null ? "diaChiThuongTru=" + diaChiThuongTru + ", " : "") +
                (danTocId != null ? "danTocId=" + danTocId + ", " : "") +
                (ngheNghiepId != null ? "ngheNghiepId=" + ngheNghiepId + ", " : "") +
                (quocTichId != null ? "quocTichId=" + quocTichId + ", " : "") +
                (phuongXaId != null ? "phuongXaId=" + phuongXaId + ", " : "") +
                (quanHuyenId != null ? "quanHuyenId=" + quanHuyenId + ", " : "") +
                (tinhThanhPhoId != null ? "tinhThanhPhoId=" + tinhThanhPhoId + ", " : "") +
                (userExtraId != null ? "userExtraId=" + userExtraId + ", " : "") +
            "}";
    }

}

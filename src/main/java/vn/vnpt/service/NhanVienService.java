package vn.vnpt.service;

import vn.vnpt.service.dto.NhanVienDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.NhanVien}.
 */
public interface NhanVienService {

    /**
     * Save a nhanVien.
     *
     * @param nhanVienDTO the entity to save.
     * @return the persisted entity.
     */
    NhanVienDTO save(NhanVienDTO nhanVienDTO);

    /**
     * Get all the nhanViens.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<NhanVienDTO> findAll(Pageable pageable);

    /**
     * Get all the nhanViens with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    Page<NhanVienDTO> findAllWithEagerRelationships(Pageable pageable);


    /**
     * Get the "id" nhanVien.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<NhanVienDTO> findOne(Long id);

    /**
     * Delete the "id" nhanVien.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}

package vn.vnpt.service;

import vn.vnpt.service.dto.TBenhLyKhamBenhDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.TBenhLyKhamBenh}.
 */
public interface TBenhLyKhamBenhService {

    /**
     * Save a tBenhLyKhamBenh.
     *
     * @param tBenhLyKhamBenhDTO the entity to save.
     * @return the persisted entity.
     */
    TBenhLyKhamBenhDTO save(TBenhLyKhamBenhDTO tBenhLyKhamBenhDTO);

    /**
     * Get all the tBenhLyKhamBenhs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<TBenhLyKhamBenhDTO> findAll(Pageable pageable);


    /**
     * Get the "id" tBenhLyKhamBenh.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<TBenhLyKhamBenhDTO> findOne(Long id);

    /**
     * Delete the "id" tBenhLyKhamBenh.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}

package vn.vnpt.service.mapper;


import org.springframework.beans.factory.annotation.Autowired;
import vn.vnpt.domain.*;
import vn.vnpt.service.dto.ThongTinKhamBenhDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ThongTinKhamBenh} and its DTO {@link ThongTinKhamBenhDTO}.
 */
@Mapper(componentModel = "spring", uses = {ThongTinKhoaMapper.class, NhanVienMapper.class, HuongDieuTriMapper.class, PhongMapper.class})
public abstract class ThongTinKhamBenhMapper implements EntityMapper<ThongTinKhamBenhDTO, ThongTinKhamBenh> {

    @Autowired
    ThongTinKhoaMapper thongTinKhoaMapper;
//    @Mapping(source = "bakb.id", target = "bakbId")
//    @Mapping(source = "donVi.id", target = "donViId")
//    @Mapping(source = "benhNhan.id", target = "benhNhanId")
//    @Mapping(source = "dotDieuTri.id", target = "dotDieuTriId")
//    @Mapping(source = "thongTinKhoa.id", target = "thongTinKhoaId")
    @Mapping(source = "nhanVien.id", target = "nhanVienId")
    @Mapping(source = "huongDieuTri.id", target = "huongDieuTriId")
    @Mapping(source = "phong.id", target = "phongId")
    public  abstract ThongTinKhamBenhDTO toDto(ThongTinKhamBenh thongTinKhamBenh);

//    @Mapping(source = "bakbId", target = "bakb")
//    @Mapping(source = "donViId", target = "donVi")
//    @Mapping(source = "benhNhanId", target = "benhNhan")
//    @Mapping(source = "dotDieuTriId", target = "dotDieuTri")
    @Mapping(source = "thongTinKhoaId", target = "thongTinKhoaId")
    @Mapping(source = "dotDieuTriId", target = "dotDieuTriId")
    @Mapping(source = "bakbId", target = "bakbId")
    @Mapping(source = "benhNhanId", target = "benhNhanId")
    @Mapping(source = "donViId", target = "donViId")
    @Mapping(source = "nhanVienId", target = "nhanVien")
    @Mapping(source = "huongDieuTriId", target = "huongDieuTri")
    @Mapping(source = "phongId", target = "phong")
    @Mapping(expression = "java(" +
        "this.thongTinKhoaMapper.fromId(" +
        "new vn.vnpt.domain.ThongTinKhoaId(" +
        "thongTinKhamBenhDTO.getThongTinKhoaId(), " +
        "thongTinKhamBenhDTO.getDotDieuTriId(), " +
        "thongTinKhamBenhDTO.getBakbId(), " +
        "thongTinKhamBenhDTO.getBenhNhanId(), " +
        "thongTinKhamBenhDTO.getDonViId()" +
        ")" +
        ")" +
        ")", target = "thongTinKhoa")
    public abstract ThongTinKhamBenh toEntity(ThongTinKhamBenhDTO thongTinKhamBenhDTO);

    public ThongTinKhamBenh fromId(ThongTinKhamBenhId id) {
        if (id == null) {
            return null;
        }
        ThongTinKhamBenh thongTinKhamBenh = new ThongTinKhamBenh();
        thongTinKhamBenh.setId(id.getId());
        thongTinKhamBenh.setThongTinKhoaId(id.getThongTinKhoaId());
        thongTinKhamBenh.setDotDieuTriId(id.getDotDieuTriId());
        thongTinKhamBenh.setBakbId(id.getBakbId());
        thongTinKhamBenh.setBenhNhanId(id.getBenhNhanId());
        thongTinKhamBenh.setDonViId(id.getDonViId());
        return thongTinKhamBenh;
    }
}

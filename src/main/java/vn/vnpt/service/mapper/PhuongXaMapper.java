package vn.vnpt.service.mapper;


import vn.vnpt.domain.*;
import vn.vnpt.service.dto.PhuongXaDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link PhuongXa} and its DTO {@link PhuongXaDTO}.
 */
@Mapper(componentModel = "spring", uses = {QuanHuyenMapper.class})
public interface PhuongXaMapper extends EntityMapper<PhuongXaDTO, PhuongXa> {

    @Mapping(source = "quanHuyen.id", target = "quanHuyenId")
    PhuongXaDTO toDto(PhuongXa phuongXa);

    @Mapping(source = "quanHuyenId", target = "quanHuyen")
    PhuongXa toEntity(PhuongXaDTO phuongXaDTO);

    default PhuongXa fromId(Long id) {
        if (id == null) {
            return null;
        }
        PhuongXa phuongXa = new PhuongXa();
        phuongXa.setId(id);
        return phuongXa;
    }
}

package vn.vnpt.service.mapper.custommapper;

import com.hazelcast.internal.jmx.ManagedAnnotation;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import vn.vnpt.service.dto.BenhAnKhamBenhDTO;
import vn.vnpt.service.dto.BenhNhanDTO;
import vn.vnpt.service.dto.DotDieuTriDTO;
import vn.vnpt.service.dto.TheBhytDTO;
import vn.vnpt.service.dto.customdto.TiepNhanDTO;


@Mapper(componentModel = "spring")
public interface TiepNhanMapper  {

    @Mapping(source = "cmndBenhNhan", target = "cmnd")
    @Mapping(source = "diaChi", target = "diaChiThuongTru")
    @Mapping(source = "tenBenhNhan", target = "ten")
    @Mapping(source = "benhNhanId", target = "id")
    @Mapping(target = "enabled", defaultValue = "true")
    BenhNhanDTO toBenhNhanDto(TiepNhanDTO tiepNhanDTO);

    @Mapping(source = "bakbId", target = "id")
    BenhAnKhamBenhDTO toBenhAnKhamBenhDto(TiepNhanDTO tiepNhanDTO);

    @Mapping(source = "dotDieuTriId",target = "id")
    @Mapping(source = "bakbId", target = "bakbId")
//    @Mapping(source = "bakbId",target = "dotDieuTriId")
    DotDieuTriDTO toDotDieuTriDto(TiepNhanDTO tiepNhanDTO);

    @Mapping(source = "bhytId", target = "id")
    @Mapping(source = "bhytEnabled", target = "enabled", defaultValue = "true")
    @Mapping(source = "bhytSoThe", target = "soThe")
    @Mapping(source = "bhytMaKhuVuc", target = "maKhuVuc")
    @Mapping(source = "bhytNgayBatDau", target = "ngayBatDau")
    @Mapping(source = "bhytNgayDuNamNam", target = "ngayDuNamNam")
    @Mapping(source = "bhytNgayHetHan",target = "ngayHetHan")
    TheBhytDTO toTheBhytDto(TiepNhanDTO tiepNhanDTO);


    @Mapping(target = "bakbId", source = "benhAnKhamBenhDTO.id")
    @Mapping(target = "benhNhanCu", source = "benhAnKhamBenhDTO.benhNhanCu")
    @Mapping(target = "canhBao", source = "benhAnKhamBenhDTO.canhBao")
    @Mapping(target = "capCuu", source = "benhAnKhamBenhDTO.capCuu")
    @Mapping(target = "chiCoNamSinh", source = "benhNhanDTO.chiCoNamSinh")
    @Mapping(target = "cmndBenhNhan", source = "benhNhanDTO.cmnd")
    @Mapping(target = "cmndNguoiNha",source = "benhAnKhamBenhDTO.cmndNguoiNha")
    @Mapping(target = "coBaoHiem", source = "benhAnKhamBenhDTO.coBaoHiem")
    @Mapping(target = "diaChi", source = "benhNhanDTO.diaChiThuongTru")
    @Mapping(target = "email", source = "benhNhanDTO.email")
    @Mapping(target = "gioiTinh", source = "benhNhanDTO.gioiTinh")
    @Mapping(target = "khangThe", source = "benhNhanDTO.khangThe")
    @Mapping(target = "lanKhamTrongNgay", source = "benhAnKhamBenhDTO.lanKhamTrongNgay")
    @Mapping(target = "loaiGiayToTreEm", source = "benhAnKhamBenhDTO.loaiGiayToTreEm")
    @Mapping(target = "ngayCapCmnd", source = "benhNhanDTO.ngayCapCmnd")
    @Mapping(target = "ngayMienCungChiTra", source = "theBhytDTO.ngayMienCungChiTra")
    @Mapping(target = "ngaySinh", source = "benhNhanDTO.ngaySinh")
    @Mapping(target = "nhomMau", source = "benhNhanDTO.nhomMau")
    @Mapping(target = "noiCapCmnd", source = "benhNhanDTO.noiCapCmnd")
    @Mapping(target = "noiLamViec", source = "benhNhanDTO.noiLamViec")
    @Mapping(target = "phone", source = "benhNhanDTO.phone")
    @Mapping(target = "phoneNguoiNha", source = "benhAnKhamBenhDTO.phoneNguoiNha")
    @Mapping(target = "quocTich", source = "benhAnKhamBenhDTO.quocTich")
    @Mapping(target = "quocTichId", source ="benhNhanDTO.quocTichId")
    @Mapping(target = "tenBenhNhan", source = "benhNhanDTO.ten")
    @Mapping(target = "tenNguoiNha", source = "benhAnKhamBenhDTO.tenNguoiNha")
    @Mapping(target = "thangTuoi", source = "benhAnKhamBenhDTO.thangTuoi")
    @Mapping(target = "thoiGianTiepNhan", source = "benhAnKhamBenhDTO.thoiGianTiepNhan")
    @Mapping(target = "tuoi", source = "benhAnKhamBenhDTO.tuoi")
    @Mapping(target = "uuTien", source = "benhAnKhamBenhDTO.uuTien")
    @Mapping(target = "uuid", source = "benhAnKhamBenhDTO.uuid")
    @Mapping(target = "trangThai", source = "benhAnKhamBenhDTO.trangThai")
    @Mapping(target = "soBenhAn", source = "benhAnKhamBenhDTO.soBenhAn")
    @Mapping(target = "soBenhAnKhoa", source = "benhAnKhamBenhDTO.soBenhAnKhoa")
    @Mapping(target = "nam", source = "benhAnKhamBenhDTO.nam")
    @Mapping(target = "benhNhanId", source = "benhNhanDTO.id")
    @Mapping(target = "donViId", source = "benhAnKhamBenhDTO.donViId")
    @Mapping(target = "nhanVienTiepNhanId", source = "benhAnKhamBenhDTO.nhanVienTiepNhanId")
    @Mapping(target = "phongTiepNhanId", source = "benhAnKhamBenhDTO.phongTiepNhanId")
    @Mapping(target = "dotDieuTriId", source = "dotDieuTriDTO.id")
    @Mapping(target = "soThuTu", source = "dotDieuTriDTO.soThuTu")
    @Mapping(target = "bhytCanTren", source = "dotDieuTriDTO.bhytCanTren")
    @Mapping(target = "bhytCanTrenKtc", source = "dotDieuTriDTO.bhytCanTrenKtc")
    @Mapping(target = "bhytMaKhuVuc", source = "theBhytDTO.maKhuVuc")
    @Mapping(target = "bhytNgayBatDau", source = "theBhytDTO.ngayBatDau")
    @Mapping(target = "bhytNgayDuNamNam", source = "theBhytDTO.ngayDuNamNam")
    @Mapping(target = "bhytNgayHetHan",source = "theBhytDTO.ngayHetHan")
    @Mapping(target = "bhytNoiTinh", source = "dotDieuTriDTO.bhytNoiTinh")
    @Mapping(target = "bhytSoThe", source = "theBhytDTO.soThe")
    @Mapping(target = "bhytTyLeMienGiam", source = "dotDieuTriDTO.bhytTyLeMienGiam")
    @Mapping(target = "bhytTyLeMienGiamKtc", source = "dotDieuTriDTO.bhytTyLeMienGiamKtc")
    @Mapping(target = "bhytDiaChi", source = "dotDieuTriDTO.bhytDiaChi")
    @Mapping(target = "doiTuongBhytTen", source = "dotDieuTriDTO.doiTuongBhytTen")
    @Mapping(target = "dungTuyen", source = "dotDieuTriDTO.dungTuyen")
    @Mapping(target = "giayToTreEm", source = "dotDieuTriDTO.giayToTreEm")
    @Mapping(target = "theTreEm", source = "dotDieuTriDTO.theTreEm")
    @Mapping(target = "thongTuyenBhxhXml4210", source = "dotDieuTriDTO.thongTuyenBhxhXml4210")
    @Mapping(target = "loai", source = "dotDieuTriDTO.loai")
    @Mapping(target = "bhytNoiDkKcbbd", source = "dotDieuTriDTO.bhytNoiDkKcbbd")
    @Mapping(target = "enabled", source = "benhNhanDTO.enabled")
    @Mapping(target = "maSoThue", source = "benhNhanDTO.maSoThue")
    @Mapping(target = "soNhaXom", source = "benhNhanDTO.soNhaXom")
    @Mapping(target = "apThon", source = "benhNhanDTO.apThon")
    @Mapping(target = "diaPhuongId", source = "benhNhanDTO.diaPhuongId")
    @Mapping(target = "diaChiThuongTru", source = "benhNhanDTO.diaChiThuongTru")
    @Mapping(target = "danTocId", source = "benhNhanDTO.danTocId")
    @Mapping(target = "ngheNghiepId", source = "benhNhanDTO.ngheNghiepId")
    @Mapping(target = "phuongXaId", source = "benhNhanDTO.phuongXaId")
    @Mapping(target = "quanHuyenId", source = "benhNhanDTO.quanHuyenId")
    @Mapping(target = "tinhThanhPhoId", source = "benhNhanDTO.tinhThanhPhoId")
    @Mapping(target = "userExtraId", source = "benhNhanDTO.userExtraId")
    @Mapping(target = "bhytId", source = "theBhytDTO.id")
    @Mapping(target = "bhytEnabled", source = "theBhytDTO.enabled")
    @Mapping(target = "maSoBhxh", source = "theBhytDTO.maSoBhxh")
    @Mapping(target = "maTheCu", source = "theBhytDTO.maTheCu")
    @Mapping(target = "qr", source = "theBhytDTO.qr")
    @Mapping(target = "doiTuongBhytId", source = "theBhytDTO.doiTuongBhytId")
    @Mapping(target = "thongTinBhxhId", source = "theBhytDTO.thongTinBhxhId")
    @Mapping(target = "maKhoaHoanTatKham", source = "benhAnKhamBenhDTO.maKhoaHoanTatKham")
    @Mapping(target = "tenKhoaHoanTatKham", source = "benhAnKhamBenhDTO.tenKhoaHoanTatKham")
    @Mapping(target = "maPhongHoanTatKham", source = "benhAnKhamBenhDTO.maPhongHoanTatKham")
    @Mapping(target = "tenPhongHoanTatKham", source = "benhAnKhamBenhDTO.tenPhongHoanTatKham")
    @Mapping(target = "thoiGianHoanTatKham", source = "benhAnKhamBenhDTO.thoiGianHoanTatKham")

    TiepNhanDTO toTiepNhanDto(BenhNhanDTO benhNhanDTO, BenhAnKhamBenhDTO benhAnKhamBenhDTO, DotDieuTriDTO dotDieuTriDTO, TheBhytDTO theBhytDTO);

}







package vn.vnpt.service.mapper;

import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import vn.vnpt.domain.Authority;
import vn.vnpt.domain.User;
import vn.vnpt.domain.UserExtra;
import vn.vnpt.service.dto.UserExtraDTO;
import vn.vnpt.service.dto.UserNhanVienDTO;

import java.time.Instant;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Mapper(componentModel = "spring", uses = {UserMapper.class, UserExtraMapper.class, NhanVienMapper.class})
public interface UserNhanVienMapper {

    User toUser(UserNhanVienDTO userNhanVienDTO);

    @Mapping(target = "password", ignore = true)
    @Mapping(target = "nhanVienDTO", ignore = true)
    @Mapping(target = "userExtraTypeId", ignore = true)
    UserNhanVienDTO toUserNhanVienDto(User user);

    @Mapping(source = "userExtraTypeId", target = "userTypeId")
    @Mapping(source = "activated", target = "enabled")
    @Mapping(source = "login", target = "username")
    @Mapping(source = "id", target = "userId")
    UserExtraDTO toUserExtraDto(UserNhanVienDTO userNhanVienDTO);

    @Mapping(source = "userExtraTypeId", target = "userType.id")
    @Mapping(source = "activated", target = "enabled")
    @Mapping(source = "login", target = "username")
    @Mapping(source = "id", target = "user.id")
    @Mapping(target = "menus", ignore = true)
    @Mapping(target = "removeMenu", ignore = true)
    UserExtra toUserExtra(UserNhanVienDTO userNhanVienDTO);

    @Mapping(target = "self", ignore = true)
    @Mapping(target = "origin", ignore = true)
    @Mapping(target = "createdTimestamp", source = "createdDate")
    @Mapping(target = "totp", ignore = true)
    @Mapping(target = "emailVerified", ignore = true)
    @Mapping(target = "federationLink", ignore = true)
    @Mapping(target = "credentials", source = "password")
    @Mapping(target = "attributes", ignore = true)
    @Mapping(target = "serviceAccountClientId", ignore = true)
    @Mapping(target = "disableableCredentialTypes", ignore = true)
    @Mapping(target = "requiredActions", ignore = true)
    @Mapping(target = "clientRoles", ignore = true)
    @Mapping(target = "federatedIdentities", ignore = true)
    @Mapping(target = "realmRoles", source = "authorities")
    @Mapping(target = "clientConsents", ignore = true)
    @Mapping(target = "notBefore", ignore = true)
    @Mapping(target = "access", ignore = true)
    @Mapping(target = "socialLinks", ignore = true)
    @Mapping(target = "applicationRoles", ignore = true)
    @Mapping(target = "groups", ignore = true)
    @Mapping(target = "username", source = "login")
    @Mapping(target = "enabled", source = "activated")
    UserRepresentation toUserRepresentation(UserNhanVienDTO userNhanVienDTO);

    default Long createdTimestamp(Instant createdDate) {
        return createdDate.toEpochMilli();
    }

    default List<String> clientRoles(Set<Authority> authorities) {
        return authorities.stream()
            .map(Authority::getName)
            .collect(Collectors.toList());
    }

    default List<CredentialRepresentation> credentials(String password) {
        CredentialRepresentation credentialRepresentation = new CredentialRepresentation();
        credentialRepresentation.setTemporary(false);
        credentialRepresentation.setType(CredentialRepresentation.PASSWORD);
        credentialRepresentation.setValue(password);
        return Stream.of(credentialRepresentation)
            .collect(Collectors.toList());
    }
//    default ThongTinKhamBenh fromId(Set<Authority> authorities) {
//        if (id == null) {
//            return null;
//        }
//        ThongTinKhamBenh thongTinKhamBenh = new ThongTinKhamBenh();
//        thongTinKhamBenh.setId(id);
//        return thongTinKhamBenh;
//    }

//    @Mapping(source = "NhanVienDTO", target = ".")
//    NhanVienDTO toNhanVienDto(UserNhanVienDTO userNhanVienDTO);
}

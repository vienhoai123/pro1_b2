package vn.vnpt.service.mapper;


import vn.vnpt.domain.*;
import vn.vnpt.service.dto.ThuThuatPhauThuatDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ThuThuatPhauThuat} and its DTO {@link ThuThuatPhauThuatDTO}.
 */
@Mapper(componentModel = "spring", uses = {DonViMapper.class, DotThayDoiMaDichVuMapper.class, LoaiThuThuatPhauThuatMapper.class})
public interface ThuThuatPhauThuatMapper extends EntityMapper<ThuThuatPhauThuatDTO, ThuThuatPhauThuat> {

    @Mapping(source = "donVi.id", target = "donViId")
    @Mapping(source = "dotMa.id", target = "dotMaId")
    @Mapping(source = "loaittpt.id", target = "loaittptId")
    ThuThuatPhauThuatDTO toDto(ThuThuatPhauThuat thuThuatPhauThuat);

    @Mapping(source = "donViId", target = "donVi")
    @Mapping(source = "dotMaId", target = "dotMa")
    @Mapping(source = "loaittptId", target = "loaittpt")
    ThuThuatPhauThuat toEntity(ThuThuatPhauThuatDTO thuThuatPhauThuatDTO);

    default ThuThuatPhauThuat fromId(Long id) {
        if (id == null) {
            return null;
        }
        ThuThuatPhauThuat thuThuatPhauThuat = new ThuThuatPhauThuat();
        thuThuatPhauThuat.setId(id);
        return thuThuatPhauThuat;
    }
}

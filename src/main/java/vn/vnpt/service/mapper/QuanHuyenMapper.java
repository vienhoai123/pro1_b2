package vn.vnpt.service.mapper;


import vn.vnpt.domain.*;
import vn.vnpt.service.dto.QuanHuyenDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link QuanHuyen} and its DTO {@link QuanHuyenDTO}.
 */
@Mapper(componentModel = "spring", uses = {TinhThanhPhoMapper.class})
public interface QuanHuyenMapper extends EntityMapper<QuanHuyenDTO, QuanHuyen> {

    @Mapping(source = "tinhThanhPho.id", target = "tinhThanhPhoId")
    QuanHuyenDTO toDto(QuanHuyen quanHuyen);

    @Mapping(source = "tinhThanhPhoId", target = "tinhThanhPho")
    QuanHuyen toEntity(QuanHuyenDTO quanHuyenDTO);

    default QuanHuyen fromId(Long id) {
        if (id == null) {
            return null;
        }
        QuanHuyen quanHuyen = new QuanHuyen();
        quanHuyen.setId(id);
        return quanHuyen;
    }
}

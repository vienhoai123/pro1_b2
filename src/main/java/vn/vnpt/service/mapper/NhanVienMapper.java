package vn.vnpt.service.mapper;


import vn.vnpt.domain.*;
import vn.vnpt.service.dto.NhanVienDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link NhanVien} and its DTO {@link NhanVienDTO}.
 */
@Mapper(componentModel = "spring", uses = {ChucDanhMapper.class, UserExtraMapper.class, ChucVuMapper.class})
public interface NhanVienMapper extends EntityMapper<NhanVienDTO, NhanVien> {

    @Mapping(source = "chucDanh.id", target = "chucDanhId")
    @Mapping(source = "userExtra.id", target = "userExtraId")
    NhanVienDTO toDto(NhanVien nhanVien);

    @Mapping(source = "chucDanhId", target = "chucDanh")
    @Mapping(source = "userExtraId", target = "userExtra")
    @Mapping(target = "removeChucVu", ignore = true)
    NhanVien toEntity(NhanVienDTO nhanVienDTO);

    default NhanVien fromId(Long id) {
        if (id == null) {
            return null;
        }
        NhanVien nhanVien = new NhanVien();
        nhanVien.setId(id);
        return nhanVien;
    }
}

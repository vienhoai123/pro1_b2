package vn.vnpt.service.mapper;


import org.springframework.beans.factory.annotation.Autowired;
import vn.vnpt.domain.*;
import vn.vnpt.service.dto.PhieuChiDinhTTPTDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link PhieuChiDinhTTPT} and its DTO {@link PhieuChiDinhTTPTDTO}.
 */
@Mapper(componentModel = "spring", uses = {BenhAnKhamBenhMapper.class})
public abstract class PhieuChiDinhTTPTMapper implements EntityMapper<PhieuChiDinhTTPTDTO, PhieuChiDinhTTPT> {

    @Autowired
    BenhAnKhamBenhMapper benhAnKhamBenhMapper;

//    @Mapping(source = "donVi.id", target = "donViId")
//    @Mapping(source = "benhNhan.id", target = "benhNhanId")
//    @Mapping(source = "bakb.id", target = "bakbId")
    public abstract PhieuChiDinhTTPTDTO toDto(PhieuChiDinhTTPT phieuChiDinhTTPT);

//    @Mapping(source = "donViId", target = "donVi")
//    @Mapping(source = "benhNhanId", target = "benhNhan")
//    @Mapping(source = "bakbId", target = "bakb.id")
    public abstract PhieuChiDinhTTPT toEntity(PhieuChiDinhTTPTDTO phieuChiDinhTTPTDTO);

    public PhieuChiDinhTTPT fromId(PhieuChiDinhTTPTId id) {
        if (id == null) {
            return null;
        }
        PhieuChiDinhTTPT phieuChiDinhTTPT = new PhieuChiDinhTTPT();
        phieuChiDinhTTPT.setId(id.getId());
        phieuChiDinhTTPT.setBakbId(id.getBakbId());
        phieuChiDinhTTPT.setDonViId(id.getDonViId());
        return phieuChiDinhTTPT;
    }
}

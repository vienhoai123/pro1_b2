package vn.vnpt.service.mapper;


import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import vn.vnpt.domain.DotThayDoiMaDichVu;
import vn.vnpt.service.dto.DotThayDoiMaDichVuDTO;

/**
 * Mapper for the entity {@link DotThayDoiMaDichVu} and its DTO {@link DotThayDoiMaDichVuDTO}.
 */
@Mapper(componentModel = "spring", uses = {DonViMapper.class})
public interface DotThayDoiMaDichVuMapper extends EntityMapper<DotThayDoiMaDichVuDTO, DotThayDoiMaDichVu> {

    @Mapping(source = "donVi.id", target = "donViId")
    DotThayDoiMaDichVuDTO toDto(DotThayDoiMaDichVu dotThayDoiMaDichVu);

    @Mapping(source = "donViId", target = "donVi")
    DotThayDoiMaDichVu toEntity(DotThayDoiMaDichVuDTO dotThayDoiMaDichVuDTO);

    default DotThayDoiMaDichVu fromId(Long id) {
        if (id == null) {
            return null;
        }
        DotThayDoiMaDichVu dotThayDoiMaDichVu = new DotThayDoiMaDichVu();
        dotThayDoiMaDichVu.setId(id);
        return dotThayDoiMaDichVu;
    }
}

package vn.vnpt.service.mapper;


import vn.vnpt.domain.*;
import vn.vnpt.service.dto.KhoaDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Khoa} and its DTO {@link KhoaDTO}.
 */
@Mapper(componentModel = "spring", uses = {DonViMapper.class})
public interface KhoaMapper extends EntityMapper<KhoaDTO, Khoa> {

    @Mapping(source = "donVi.id", target = "donViId")
    KhoaDTO toDto(Khoa khoa);

    @Mapping(source = "donViId", target = "donVi")
    Khoa toEntity(KhoaDTO khoaDTO);

    default Khoa fromId(Long id) {
        if (id == null) {
            return null;
        }
        Khoa khoa = new Khoa();
        khoa.setId(id);
        return khoa;
    }
}

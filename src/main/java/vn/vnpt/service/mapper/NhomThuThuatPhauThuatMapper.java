package vn.vnpt.service.mapper;


import vn.vnpt.domain.*;
import vn.vnpt.service.dto.NhomThuThuatPhauThuatDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link NhomThuThuatPhauThuat} and its DTO {@link NhomThuThuatPhauThuatDTO}.
 */
@Mapper(componentModel = "spring", uses = {DonViMapper.class})
public interface NhomThuThuatPhauThuatMapper extends EntityMapper<NhomThuThuatPhauThuatDTO, NhomThuThuatPhauThuat> {

    @Mapping(source = "donVi.id", target = "donViId")
    NhomThuThuatPhauThuatDTO toDto(NhomThuThuatPhauThuat nhomThuThuatPhauThuat);

    @Mapping(source = "donViId", target = "donVi")
    NhomThuThuatPhauThuat toEntity(NhomThuThuatPhauThuatDTO nhomThuThuatPhauThuatDTO);

    default NhomThuThuatPhauThuat fromId(Long id) {
        if (id == null) {
            return null;
        }
        NhomThuThuatPhauThuat nhomThuThuatPhauThuat = new NhomThuThuatPhauThuat();
        nhomThuThuatPhauThuat.setId(id);
        return nhomThuThuatPhauThuat;
    }
}

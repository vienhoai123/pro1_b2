package vn.vnpt.service.mapper;


import vn.vnpt.domain.*;
import vn.vnpt.service.dto.BenhLyDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link BenhLy} and its DTO {@link BenhLyDTO}.
 */
@Mapper(componentModel = "spring", uses = {NhomBenhLyMapper.class})
public interface BenhLyMapper extends EntityMapper<BenhLyDTO, BenhLy> {

    @Mapping(source = "nhomBenhLy.id", target = "nhomBenhLyId")
    BenhLyDTO toDto(BenhLy benhLy);

    @Mapping(source = "nhomBenhLyId", target = "nhomBenhLy")
    BenhLy toEntity(BenhLyDTO benhLyDTO);

    default BenhLy fromId(Long id) {
        if (id == null) {
            return null;
        }
        BenhLy benhLy = new BenhLy();
        benhLy.setId(id);
        return benhLy;
    }
}

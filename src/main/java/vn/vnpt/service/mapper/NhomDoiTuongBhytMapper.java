package vn.vnpt.service.mapper;


import vn.vnpt.domain.*;
import vn.vnpt.service.dto.NhomDoiTuongBhytDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link NhomDoiTuongBhyt} and its DTO {@link NhomDoiTuongBhytDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface NhomDoiTuongBhytMapper extends EntityMapper<NhomDoiTuongBhytDTO, NhomDoiTuongBhyt> {



    default NhomDoiTuongBhyt fromId(Long id) {
        if (id == null) {
            return null;
        }
        NhomDoiTuongBhyt nhomDoiTuongBhyt = new NhomDoiTuongBhyt();
        nhomDoiTuongBhyt.setId(id);
        return nhomDoiTuongBhyt;
    }
}

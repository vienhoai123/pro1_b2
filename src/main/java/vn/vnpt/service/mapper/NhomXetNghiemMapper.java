package vn.vnpt.service.mapper;


import vn.vnpt.domain.*;
import vn.vnpt.service.dto.NhomXetNghiemDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link NhomXetNghiem} and its DTO {@link NhomXetNghiemDTO}.
 */
@Mapper(componentModel = "spring", uses = {DonViMapper.class})
public interface NhomXetNghiemMapper extends EntityMapper<NhomXetNghiemDTO, NhomXetNghiem> {

    @Mapping(source = "donVi.id", target = "donViId")
    NhomXetNghiemDTO toDto(NhomXetNghiem nhomXetNghiem);

    @Mapping(source = "donViId", target = "donVi")
    NhomXetNghiem toEntity(NhomXetNghiemDTO nhomXetNghiemDTO);

    default NhomXetNghiem fromId(Long id) {
        if (id == null) {
            return null;
        }
        NhomXetNghiem nhomXetNghiem = new NhomXetNghiem();
        nhomXetNghiem.setId(id);
        return nhomXetNghiem;
    }
}

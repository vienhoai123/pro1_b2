package vn.vnpt.service.mapper;


import org.springframework.beans.factory.annotation.Autowired;
import vn.vnpt.domain.*;
import vn.vnpt.service.dto.ChiDinhThuThuatPhauThuatDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ChiDinhThuThuatPhauThuat} and its DTO {@link ChiDinhThuThuatPhauThuatDTO}.
 */
@Mapper(componentModel = "spring", uses = {PhieuChiDinhTTPTMapper.class, PhongMapper.class, ThuThuatPhauThuatMapper.class})
public abstract class ChiDinhThuThuatPhauThuatMapper implements EntityMapper<ChiDinhThuThuatPhauThuatDTO, ChiDinhThuThuatPhauThuat> {

    @Autowired
    PhieuChiDinhTTPTMapper phieuChiDinhThuThuatPhauThuatMapper;

//    @Mapping(source = "donVi.id", target = "donViId")
//    @Mapping(source = "benhNhan.id", target = "benhNhanId")
//    @Mapping(source = "bakb.id", target = "bakbId")
//    @Mapping(source = "phieuCD.id", target = "phieuCDId")
    @Mapping(source = "phong.id", target = "phongId")
    @Mapping(source = "ttpt.id", target = "ttptId")
    public abstract ChiDinhThuThuatPhauThuatDTO toDto(ChiDinhThuThuatPhauThuat chiDinhThuThuatPhauThuat);

//    @Mapping(source = "donViId", target = "donVi")
//    @Mapping(source = "benhNhanId", target = "benhNhan")
//    @Mapping(source = "bakbId", target = "bakb")
//    @Mapping(source = "phieuCDId", target = "phieuCD")
    @Mapping(source = "phongId", target = "phong")
    @Mapping(source = "ttptId", target = "ttpt")
    public abstract ChiDinhThuThuatPhauThuat toEntity(ChiDinhThuThuatPhauThuatDTO chiDinhThuThuatPhauThuatDTO);

    public ChiDinhThuThuatPhauThuat fromId(ChiDinhTTPTId id) {
        if (id == null) {
            return null;
        }
        ChiDinhThuThuatPhauThuat chiDinhThuThuatPhauThuat = new ChiDinhThuThuatPhauThuat();
        chiDinhThuThuatPhauThuat.setId(id.getId());
        chiDinhThuThuatPhauThuat.setBakbId(id.getBakbId());
        chiDinhThuThuatPhauThuat.setBenhNhanId(id.getBenhNhanId());
        chiDinhThuThuatPhauThuat.setDonViId(id.getDonViId());
        chiDinhThuThuatPhauThuat.setPhieuCDId(id.getPhieuId());
        return chiDinhThuThuatPhauThuat;
    }
}

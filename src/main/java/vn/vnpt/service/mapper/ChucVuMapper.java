package vn.vnpt.service.mapper;


import vn.vnpt.domain.*;
import vn.vnpt.service.dto.ChucVuDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ChucVu} and its DTO {@link ChucVuDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ChucVuMapper extends EntityMapper<ChucVuDTO, ChucVu> {


    @Mapping(target = "nhanViens", ignore = true)
    @Mapping(target = "removeNhanVien", ignore = true)
    ChucVu toEntity(ChucVuDTO chucVuDTO);

    default ChucVu fromId(Long id) {
        if (id == null) {
            return null;
        }
        ChucVu chucVu = new ChucVu();
        chucVu.setId(id);
        return chucVu;
    }
}

package vn.vnpt.service.mapper;


import vn.vnpt.domain.*;
import vn.vnpt.service.dto.LoaiChanDoanHinhAnhDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link LoaiChanDoanHinhAnh} and its DTO {@link LoaiChanDoanHinhAnhDTO}.
 */
@Mapper(componentModel = "spring", uses = {DonViMapper.class})
public interface LoaiChanDoanHinhAnhMapper extends EntityMapper<LoaiChanDoanHinhAnhDTO, LoaiChanDoanHinhAnh> {

    @Mapping(source = "donVi.id", target = "donViId")
    LoaiChanDoanHinhAnhDTO toDto(LoaiChanDoanHinhAnh loaiChanDoanHinhAnh);

    @Mapping(source = "donViId", target = "donVi")
    LoaiChanDoanHinhAnh toEntity(LoaiChanDoanHinhAnhDTO loaiChanDoanHinhAnhDTO);

    default LoaiChanDoanHinhAnh fromId(Long id) {
        if (id == null) {
            return null;
        }
        LoaiChanDoanHinhAnh loaiChanDoanHinhAnh = new LoaiChanDoanHinhAnh();
        loaiChanDoanHinhAnh.setId(id);
        return loaiChanDoanHinhAnh;
    }
}

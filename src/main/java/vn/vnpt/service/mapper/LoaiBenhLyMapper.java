package vn.vnpt.service.mapper;


import vn.vnpt.domain.*;
import vn.vnpt.service.dto.LoaiBenhLyDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link LoaiBenhLy} and its DTO {@link LoaiBenhLyDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface LoaiBenhLyMapper extends EntityMapper<LoaiBenhLyDTO, LoaiBenhLy> {



    default LoaiBenhLy fromId(Long id) {
        if (id == null) {
            return null;
        }
        LoaiBenhLy loaiBenhLy = new LoaiBenhLy();
        loaiBenhLy.setId(id);
        return loaiBenhLy;
    }
}

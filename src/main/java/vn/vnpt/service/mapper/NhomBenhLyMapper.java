package vn.vnpt.service.mapper;


import vn.vnpt.domain.*;
import vn.vnpt.service.dto.NhomBenhLyDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link NhomBenhLy} and its DTO {@link NhomBenhLyDTO}.
 */
@Mapper(componentModel = "spring", uses = {LoaiBenhLyMapper.class})
public interface NhomBenhLyMapper extends EntityMapper<NhomBenhLyDTO, NhomBenhLy> {

    @Mapping(source = "loaiBenhLy.id", target = "loaiBenhLyId")
    NhomBenhLyDTO toDto(NhomBenhLy nhomBenhLy);

    @Mapping(source = "loaiBenhLyId", target = "loaiBenhLy")
    NhomBenhLy toEntity(NhomBenhLyDTO nhomBenhLyDTO);

    default NhomBenhLy fromId(Long id) {
        if (id == null) {
            return null;
        }
        NhomBenhLy nhomBenhLy = new NhomBenhLy();
        nhomBenhLy.setId(id);
        return nhomBenhLy;
    }
}

package vn.vnpt.service.mapper;


import vn.vnpt.domain.*;
import vn.vnpt.service.dto.PhongDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Phong} and its DTO {@link PhongDTO}.
 */
@Mapper(componentModel = "spring", uses = {KhoaMapper.class})
public interface PhongMapper extends EntityMapper<PhongDTO, Phong> {

    @Mapping(source = "khoa.id", target = "khoaId")
    PhongDTO toDto(Phong phong);

    @Mapping(source = "khoaId", target = "khoa")
    Phong toEntity(PhongDTO phongDTO);

    default Phong fromId(Long id) {
        if (id == null) {
            return null;
        }
        Phong phong = new Phong();
        phong.setId(id);
        return phong;
    }
}

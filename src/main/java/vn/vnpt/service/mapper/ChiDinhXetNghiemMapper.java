package vn.vnpt.service.mapper;


import org.springframework.beans.factory.annotation.Autowired;
import vn.vnpt.domain.*;
import vn.vnpt.service.dto.ChiDinhXetNghiemDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ChiDinhXetNghiem} and its DTO {@link ChiDinhXetNghiemDTO}.
 */
@Mapper(componentModel = "spring", uses = {PhieuChiDinhXetNghiemMapper.class, PhongMapper.class, XetNghiemMapper.class})
public abstract class ChiDinhXetNghiemMapper implements EntityMapper<ChiDinhXetNghiemDTO, ChiDinhXetNghiem> {
    @Autowired
    PhieuChiDinhXetNghiemMapper phieuChiDinhXetNghiemMapper;

//    @Mapping(source = "donVi.id", target = "donViId")
//    @Mapping(source = "benhNhan.id", target = "benhNhanId")
//    @Mapping(source = "bakb.id", target = "bakbId")
//    @Mapping(source = "phieuCDId", target = "phieuCDId")
    @Mapping(source = "phong.id", target = "phongId")
    @Mapping(source = "xetNghiem.id", target = "xetNghiemId")
   public abstract ChiDinhXetNghiemDTO toDto(ChiDinhXetNghiem chiDinhXetNghiem);

//    @Mapping(source = "donViId", target = "donVi")
//    @Mapping(source = "benhNhanId", target = "benhNhan")
//    @Mapping(source = "bakbId", target = "bakb")
//    @Mapping(source = "phieuCDId", target = "phieuCDId")
    @Mapping(source = "phongId", target = "phong")
    @Mapping(source = "xetNghiemId", target = "xetNghiem")
    public abstract ChiDinhXetNghiem toEntity(ChiDinhXetNghiemDTO chiDinhXetNghiemDTO);

    public ChiDinhXetNghiem fromId(ChiDinhXetNghiemId id) {
        if (id == null) {
            return null;
        }
        ChiDinhXetNghiem chiDinhXetNghiem = new ChiDinhXetNghiem();
        chiDinhXetNghiem.setId(id.getId());
        chiDinhXetNghiem.setBakbId(id.getBakbId());
        chiDinhXetNghiem.setBenhNhanId(id.getBenhNhanId());
        chiDinhXetNghiem.setDonViId(id.getDonViId());
        chiDinhXetNghiem.setPhieuCDId(id.getPhieuId());
        return chiDinhXetNghiem;
    }
}

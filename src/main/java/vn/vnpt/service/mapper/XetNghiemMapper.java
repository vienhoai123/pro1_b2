package vn.vnpt.service.mapper;


import vn.vnpt.domain.*;
import vn.vnpt.service.dto.XetNghiemDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link XetNghiem} and its DTO {@link XetNghiemDTO}.
 */
@Mapper(componentModel = "spring", uses = {DonViMapper.class, DotThayDoiMaDichVuMapper.class, LoaiXetNghiemMapper.class})
public interface XetNghiemMapper extends EntityMapper<XetNghiemDTO, XetNghiem> {

    @Mapping(source = "donVi.id", target = "donViId")
    @Mapping(source = "dotMa.id", target = "dotMaId")
    @Mapping(source = "loaiXN.id", target = "loaiXNId")
    XetNghiemDTO toDto(XetNghiem xetNghiem);

    @Mapping(source = "donViId", target = "donVi")
    @Mapping(source = "dotMaId", target = "dotMa")
    @Mapping(source = "loaiXNId", target = "loaiXN")
    XetNghiem toEntity(XetNghiemDTO xetNghiemDTO);

    default XetNghiem fromId(Long id) {
        if (id == null) {
            return null;
        }
        XetNghiem xetNghiem = new XetNghiem();
        xetNghiem.setId(id);
        return xetNghiem;
    }
}

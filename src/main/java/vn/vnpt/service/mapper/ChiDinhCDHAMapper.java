package vn.vnpt.service.mapper;


import org.springframework.beans.factory.annotation.Autowired;
import vn.vnpt.domain.*;
import vn.vnpt.service.dto.ChiDinhCDHADTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ChiDinhCDHA} and its DTO {@link ChiDinhCDHADTO}.
 */
@Mapper(componentModel = "spring", uses = {PhieuChiDinhCDHAMapper.class, PhongMapper.class, ChanDoanHinhAnhMapper.class})
public abstract class ChiDinhCDHAMapper implements EntityMapper<ChiDinhCDHADTO, ChiDinhCDHA> {

    @Autowired
    PhieuChiDinhCDHAMapper phieuChiDinhCDHAMapper;
//    @Mapping(source = "donVi.id", target = "donViId")
//    @Mapping(source = "benhNhan.id", target = "benhNhanId")
//    @Mapping(source = "bakb.id", target = "bakbId")
//    @Mapping(source = "phieuCD.id", target = "phieuCDId")
    @Mapping(source = "phong.id", target = "phongId")
    @Mapping(source = "cdha.id", target = "cdhaId")
    public abstract ChiDinhCDHADTO toDto(ChiDinhCDHA chiDinhCDHA);

//    @Mapping(source = "donViId", target = "donVi")
//    @Mapping(source = "benhNhanId", target = "benhNhan")
//    @Mapping(source = "bakbId", target = "bakb")
//    @Mapping(source = "phieuCDId", target = "phieuCD")
    @Mapping(source = "phongId", target = "phong")
    @Mapping(source = "cdhaId", target = "cdha")
    public abstract ChiDinhCDHA toEntity(ChiDinhCDHADTO chiDinhCDHADTO);

    public ChiDinhCDHA fromId(ChiDinhCDHAId id) {
        if (id == null) {
            return null;
        }
        ChiDinhCDHA chiDinhCDHA = new ChiDinhCDHA();
        chiDinhCDHA.setId(id.getId());
        chiDinhCDHA.setBakbId(id.getBakbId());
        chiDinhCDHA.setBenhNhanId(id.getBenhNhanId());
        chiDinhCDHA.setDonViId(id.getDonViId());
        chiDinhCDHA.setPhieuCDId(id.getPhieuId());
        return chiDinhCDHA;
    }
}

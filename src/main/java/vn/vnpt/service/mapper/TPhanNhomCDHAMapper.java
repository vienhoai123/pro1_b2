package vn.vnpt.service.mapper;


import vn.vnpt.domain.*;
import vn.vnpt.service.dto.TPhanNhomCDHADTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link TPhanNhomCDHA} and its DTO {@link TPhanNhomCDHADTO}.
 */
@Mapper(componentModel = "spring", uses = {NhomChanDoanHinhAnhMapper.class, ChanDoanHinhAnhMapper.class})
public interface TPhanNhomCDHAMapper extends EntityMapper<TPhanNhomCDHADTO, TPhanNhomCDHA> {

    @Mapping(source = "nhom_cdha.id", target = "nhom_cdhaId")
    @Mapping(source = "cdha.id", target = "cdhaId")
    TPhanNhomCDHADTO toDto(TPhanNhomCDHA tPhanNhomCDHA);

    @Mapping(source = "nhom_cdhaId", target = "nhom_cdha")
    @Mapping(source = "cdhaId", target = "cdha")
    TPhanNhomCDHA toEntity(TPhanNhomCDHADTO tPhanNhomCDHADTO);

    default TPhanNhomCDHA fromId(Long id) {
        if (id == null) {
            return null;
        }
        TPhanNhomCDHA tPhanNhomCDHA = new TPhanNhomCDHA();
        tPhanNhomCDHA.setId(id);
        return tPhanNhomCDHA;
    }
}

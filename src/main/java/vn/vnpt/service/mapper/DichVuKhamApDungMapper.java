package vn.vnpt.service.mapper;

import vn.vnpt.domain.*;
import vn.vnpt.service.dto.DichVuKhamApDungDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link DichVuKhamApDung} and its DTO {@link DichVuKhamApDungDTO}.
 */
@Mapper(componentModel = "spring", uses = {DotGiaDichVuBhxhMapper.class, DichVuKhamMapper.class})
public interface DichVuKhamApDungMapper extends EntityMapper<DichVuKhamApDungDTO, DichVuKhamApDung> {

    @Mapping(source = "dotGiaDichVuBhxh.id", target = "dotGiaDichVuBhxhId")
    @Mapping(source = "dichVuKham.id", target = "dichVuKhamId")
    DichVuKhamApDungDTO toDto(DichVuKhamApDung dichVuKhamApDung);

    @Mapping(source = "dotGiaDichVuBhxhId", target = "dotGiaDichVuBhxh")
    @Mapping(source = "dichVuKhamId", target = "dichVuKham")
    DichVuKhamApDung toEntity(DichVuKhamApDungDTO dichVuKhamApDungDTO);

    default DichVuKhamApDung fromId(Long id) {
        if (id == null) {
            return null;
        }
        DichVuKhamApDung dichVuKhamApDung = new DichVuKhamApDung();
        dichVuKhamApDung.setId(id);
        return dichVuKhamApDung;
    }
}

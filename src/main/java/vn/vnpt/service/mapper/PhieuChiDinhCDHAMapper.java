package vn.vnpt.service.mapper;


import org.springframework.beans.factory.annotation.Autowired;
import vn.vnpt.domain.*;
import vn.vnpt.service.dto.PhieuChiDinhCDHADTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link PhieuChiDinhCDHA} and its DTO {@link PhieuChiDinhCDHADTO}.
 */
@Mapper(componentModel = "spring", uses = {BenhAnKhamBenhMapper.class})
public abstract class PhieuChiDinhCDHAMapper implements EntityMapper<PhieuChiDinhCDHADTO, PhieuChiDinhCDHA> {
    @Autowired
    BenhAnKhamBenhMapper benhAnKhamBenhMapper;

//    @Mapping(source = "donVi.id", target = "donViId")
//    @Mapping(source = "benhNhan.id", target = "benhNhanId")
//    @Mapping(source = "bakb.id", target = "bakbId")
    public abstract PhieuChiDinhCDHADTO toDto(PhieuChiDinhCDHA phieuChiDinhCDHA);

//    @Mapping(source = "donViId", target = "donVi")
//    @Mapping(source = "benhNhanId", target = "benhNhan")
//    @Mapping(source = "bakbId", target = "bakb.id")
    public abstract PhieuChiDinhCDHA toEntity(PhieuChiDinhCDHADTO phieuChiDinhCDHADTO);

    public PhieuChiDinhCDHA fromId(PhieuChiDinhCDHA id) {
        if (id == null) {
            return null;
        }
        PhieuChiDinhCDHA phieuChiDinhCDHA = new PhieuChiDinhCDHA();
        phieuChiDinhCDHA.setId(id.getId());
        phieuChiDinhCDHA.setBakbId(id.getBakbId());
        phieuChiDinhCDHA.setBenhNhanId(id.getBenhNhanId());
        phieuChiDinhCDHA.setDonViId(id.getDonViId());
        return phieuChiDinhCDHA;
    }
}

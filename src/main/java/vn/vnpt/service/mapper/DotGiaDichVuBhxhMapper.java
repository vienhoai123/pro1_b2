package vn.vnpt.service.mapper;


import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import vn.vnpt.domain.DotGiaDichVuBhxh;
import vn.vnpt.service.dto.DotGiaDichVuBhxhDTO;

/**
 * Mapper for the entity {@link DotGiaDichVuBhxh} and its DTO {@link DotGiaDichVuBhxhDTO}.
 */
@Mapper(componentModel = "spring", uses = {DonViMapper.class})
public interface DotGiaDichVuBhxhMapper extends EntityMapper<DotGiaDichVuBhxhDTO, DotGiaDichVuBhxh> {

    @Mapping(source = "donVi.id", target = "donViId")
    DotGiaDichVuBhxhDTO toDto(DotGiaDichVuBhxh dotGiaDichVuBhxh);

    @Mapping(source = "donViId", target = "donVi")
    DotGiaDichVuBhxh toEntity(DotGiaDichVuBhxhDTO dotGiaDichVuBhxhDTO);

    default DotGiaDichVuBhxh fromId(Long id) {
        if (id == null) {
            return null;
        }
        DotGiaDichVuBhxh dotGiaDichVuBhxh = new DotGiaDichVuBhxh();
        dotGiaDichVuBhxh.setId(id);
        return dotGiaDichVuBhxh;
    }
}

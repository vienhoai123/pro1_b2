package vn.vnpt.service.mapper;


import vn.vnpt.domain.*;
import vn.vnpt.service.dto.TPhongNhanVienDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link TPhongNhanVien} and its DTO {@link TPhongNhanVienDTO}.
 */
@Mapper(componentModel = "spring", uses = {PhongMapper.class, NhanVienMapper.class})
public interface TPhongNhanVienMapper extends EntityMapper<TPhongNhanVienDTO, TPhongNhanVien> {

    @Mapping(source = "phong.id", target = "phongId")
    @Mapping(source = "nhanVien.id", target = "nhanVienId")
    TPhongNhanVienDTO toDto(TPhongNhanVien tPhongNhanVien);

    @Mapping(source = "phongId", target = "phong")
    @Mapping(source = "nhanVienId", target = "nhanVien")
    TPhongNhanVien toEntity(TPhongNhanVienDTO tPhongNhanVienDTO);

    default TPhongNhanVien fromId(Long id) {
        if (id == null) {
            return null;
        }
        TPhongNhanVien tPhongNhanVien = new TPhongNhanVien();
        tPhongNhanVien.setId(id);
        return tPhongNhanVien;
    }
}

package vn.vnpt.service.mapper;


import vn.vnpt.domain.*;
import vn.vnpt.service.dto.ThuThuatPhauThuatApDungDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ThuThuatPhauThuatApDung} and its DTO {@link ThuThuatPhauThuatApDungDTO}.
 */
@Mapper(componentModel = "spring", uses = {DotGiaDichVuBhxhMapper.class, ThuThuatPhauThuatMapper.class})
public interface ThuThuatPhauThuatApDungMapper extends EntityMapper<ThuThuatPhauThuatApDungDTO, ThuThuatPhauThuatApDung> {

    @Mapping(source = "dotGia.id", target = "dotGiaId")
    @Mapping(source = "ttpt.id", target = "ttptId")
    ThuThuatPhauThuatApDungDTO toDto(ThuThuatPhauThuatApDung thuThuatPhauThuatApDung);

    @Mapping(source = "dotGiaId", target = "dotGia")
    @Mapping(source = "ttptId", target = "ttpt")
    ThuThuatPhauThuatApDung toEntity(ThuThuatPhauThuatApDungDTO thuThuatPhauThuatApDungDTO);

    default ThuThuatPhauThuatApDung fromId(Long id) {
        if (id == null) {
            return null;
        }
        ThuThuatPhauThuatApDung thuThuatPhauThuatApDung = new ThuThuatPhauThuatApDung();
        thuThuatPhauThuatApDung.setId(id);
        return thuThuatPhauThuatApDung;
    }
}

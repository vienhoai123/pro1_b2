package vn.vnpt.service;

import vn.vnpt.service.dto.TPhanNhomXetNghiemDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.TPhanNhomXetNghiem}.
 */
public interface TPhanNhomXetNghiemService {

    /**
     * Save a tPhanNhomXetNghiem.
     *
     * @param tPhanNhomXetNghiemDTO the entity to save.
     * @return the persisted entity.
     */
    TPhanNhomXetNghiemDTO save(TPhanNhomXetNghiemDTO tPhanNhomXetNghiemDTO);

    /**
     * Get all the tPhanNhomXetNghiems.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<TPhanNhomXetNghiemDTO> findAll(Pageable pageable);

    /**
     * Get the "id" tPhanNhomXetNghiem.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<TPhanNhomXetNghiemDTO> findOne(Long id);

    /**
     * Delete the "id" tPhanNhomXetNghiem.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}

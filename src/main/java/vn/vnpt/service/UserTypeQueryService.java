package vn.vnpt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import vn.vnpt.domain.UserType;
import vn.vnpt.domain.*; // for static metamodels
import vn.vnpt.repository.UserTypeRepository;
import vn.vnpt.service.dto.UserTypeCriteria;
import vn.vnpt.service.dto.UserTypeDTO;
import vn.vnpt.service.mapper.UserTypeMapper;

/**
 * Service for executing complex queries for {@link UserType} entities in the database.
 * The main input is a {@link UserTypeCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link UserTypeDTO} or a {@link Page} of {@link UserTypeDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class UserTypeQueryService extends QueryService<UserType> {

    private final Logger log = LoggerFactory.getLogger(UserTypeQueryService.class);

    private final UserTypeRepository userTypeRepository;

    private final UserTypeMapper userTypeMapper;

    public UserTypeQueryService(UserTypeRepository userTypeRepository, UserTypeMapper userTypeMapper) {
        this.userTypeRepository = userTypeRepository;
        this.userTypeMapper = userTypeMapper;
    }

    /**
     * Return a {@link List} of {@link UserTypeDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<UserTypeDTO> findByCriteria(UserTypeCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<UserType> specification = createSpecification(criteria);
        return userTypeMapper.toDto(userTypeRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link UserTypeDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<UserTypeDTO> findByCriteria(UserTypeCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<UserType> specification = createSpecification(criteria);
        return userTypeRepository.findAll(specification, page)
            .map(userTypeMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(UserTypeCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<UserType> specification = createSpecification(criteria);
        return userTypeRepository.count(specification);
    }

    /**
     * Function to convert {@link UserTypeCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<UserType> createSpecification(UserTypeCriteria criteria) {
        Specification<UserType> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), UserType_.id));
            }
            if (criteria.getMoTa() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMoTa(), UserType_.moTa));
            }
        }
        return specification;
    }
}

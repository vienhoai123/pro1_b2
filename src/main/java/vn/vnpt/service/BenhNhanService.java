package vn.vnpt.service;

import vn.vnpt.service.dto.BenhNhanDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.vnpt.service.dto.customdto.TTBenhNhanTongHopDTO;

import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.BenhNhan}.
 */
public interface BenhNhanService {

    /**
     * Save a benhNhan.
     *
     * @param benhNhanDTO the entity to save.
     * @return the persisted entity.
     */
    BenhNhanDTO save(BenhNhanDTO benhNhanDTO);

    /**
     * Get all the benhNhans.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<BenhNhanDTO> findAll(Pageable pageable);


    /**
     * Get the "id" benhNhan.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<BenhNhanDTO> findOne(Long id);

    /**
     * Delete the "id" benhNhan.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
    Boolean hasBHYT(BenhNhanDTO benhNhanDTO);
    BenhNhanDTO getThongTinLienQuanBhyt(BenhNhanDTO benhNhanDTO);
    Optional<TTBenhNhanTongHopDTO> getThongTinBenhNhanTongHopDTO(Long benhNhanId);
}

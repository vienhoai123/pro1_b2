package vn.vnpt.service;

import vn.vnpt.service.dto.KhoaDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.Khoa}.
 */
public interface KhoaService {

    /**
     * Save a khoa.
     *
     * @param khoaDTO the entity to save.
     * @return the persisted entity.
     */
    KhoaDTO save(KhoaDTO khoaDTO);

    /**
     * Get all the khoas.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<KhoaDTO> findAll(Pageable pageable);

    /**
     * Get the "id" khoa.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<KhoaDTO> findOne(Long id);

    /**
     * Delete the "id" khoa.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}

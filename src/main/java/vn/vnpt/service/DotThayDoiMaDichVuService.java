package vn.vnpt.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.vnpt.service.dto.DotThayDoiMaDichVuDTO;

import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.DotThayDoiMaDichVu}.
 */
public interface DotThayDoiMaDichVuService {

    /**
     * Save a dotThayDoiMaDichVu.
     *
     * @param dotThayDoiMaDichVuDTO the entity to save.
     * @return the persisted entity.
     */
    DotThayDoiMaDichVuDTO save(DotThayDoiMaDichVuDTO dotThayDoiMaDichVuDTO);

    /**
     * Get all the dotThayDoiMaDichVus.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<DotThayDoiMaDichVuDTO> findAll(Pageable pageable);

    /**
     * Get the "id" dotThayDoiMaDichVu.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<DotThayDoiMaDichVuDTO> findOne(Long id);

    /**
     * Delete the "id" dotThayDoiMaDichVu.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}

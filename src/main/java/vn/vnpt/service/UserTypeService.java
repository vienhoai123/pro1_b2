package vn.vnpt.service;

import vn.vnpt.service.dto.UserTypeDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.UserType}.
 */
public interface UserTypeService {

    /**
     * Save a userType.
     *
     * @param userTypeDTO the entity to save.
     * @return the persisted entity.
     */
    UserTypeDTO save(UserTypeDTO userTypeDTO);

    /**
     * Get all the userTypes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<UserTypeDTO> findAll(Pageable pageable);

    /**
     * Get the "id" userType.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<UserTypeDTO> findOne(Long id);

    /**
     * Delete the "id" userType.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}

package vn.vnpt.service;

import vn.vnpt.service.dto.TPhanNhomCDHADTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.TPhanNhomCDHA}.
 */
public interface TPhanNhomCDHAService {

    /**
     * Save a tPhanNhomCDHA.
     *
     * @param tPhanNhomCDHADTO the entity to save.
     * @return the persisted entity.
     */
    TPhanNhomCDHADTO save(TPhanNhomCDHADTO tPhanNhomCDHADTO);

    /**
     * Get all the tPhanNhomCDHAS.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<TPhanNhomCDHADTO> findAll(Pageable pageable);

    /**
     * Get the "id" tPhanNhomCDHA.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<TPhanNhomCDHADTO> findOne(Long id);

    /**
     * Delete the "id" tPhanNhomCDHA.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}

package vn.vnpt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import vn.vnpt.domain.TheBhyt;
import vn.vnpt.domain.*; // for static metamodels
import vn.vnpt.repository.TheBhytRepository;
import vn.vnpt.service.dto.TheBhytCriteria;
import vn.vnpt.service.dto.TheBhytDTO;
import vn.vnpt.service.mapper.TheBhytMapper;

/**
 * Service for executing complex queries for {@link TheBhyt} entities in the database.
 * The main input is a {@link TheBhytCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link TheBhytDTO} or a {@link Page} of {@link TheBhytDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TheBhytQueryService extends QueryService<TheBhyt> {

    private final Logger log = LoggerFactory.getLogger(TheBhytQueryService.class);

    private final TheBhytRepository theBhytRepository;

    private final TheBhytMapper theBhytMapper;

    public TheBhytQueryService(TheBhytRepository theBhytRepository, TheBhytMapper theBhytMapper) {
        this.theBhytRepository = theBhytRepository;
        this.theBhytMapper = theBhytMapper;
    }

    /**
     * Return a {@link List} of {@link TheBhytDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<TheBhytDTO> findByCriteria(TheBhytCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<TheBhyt> specification = createSpecification(criteria);
        return theBhytMapper.toDto(theBhytRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link TheBhytDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<TheBhytDTO> findByCriteria(TheBhytCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<TheBhyt> specification = createSpecification(criteria);
        return theBhytRepository.findAll(specification, page)
            .map(theBhytMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TheBhytCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<TheBhyt> specification = createSpecification(criteria);
        return theBhytRepository.count(specification);
    }

    /**
     * Function to convert {@link TheBhytCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<TheBhyt> createSpecification(TheBhytCriteria criteria) {
        Specification<TheBhyt> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), TheBhyt_.id));
            }
            if (criteria.getEnabled() != null) {
                specification = specification.and(buildSpecification(criteria.getEnabled(), TheBhyt_.enabled));
            }
            if (criteria.getMaKhuVuc() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMaKhuVuc(), TheBhyt_.maKhuVuc));
            }
            if (criteria.getMaSoBhxh() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMaSoBhxh(), TheBhyt_.maSoBhxh));
            }
            if (criteria.getMaTheCu() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMaTheCu(), TheBhyt_.maTheCu));
            }
            if (criteria.getNgayBatDau() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNgayBatDau(), TheBhyt_.ngayBatDau));
            }
            if (criteria.getNgayDuNamNam() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNgayDuNamNam(), TheBhyt_.ngayDuNamNam));
            }
            if (criteria.getNgayHetHan() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNgayHetHan(), TheBhyt_.ngayHetHan));
            }
            if (criteria.getQr() != null) {
                specification = specification.and(buildStringSpecification(criteria.getQr(), TheBhyt_.qr));
            }
            if (criteria.getSoThe() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSoThe(), TheBhyt_.soThe));
            }
            if (criteria.getNgayMienCungChiTra() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNgayMienCungChiTra(), TheBhyt_.ngayMienCungChiTra));
            }
            if (criteria.getBenhNhanId() != null) {
                specification = specification.and(buildSpecification(criteria.getBenhNhanId(),
                    root -> root.join(TheBhyt_.benhNhan, JoinType.LEFT).get(BenhNhan_.id)));
            }
            if (criteria.getDoiTuongBhytId() != null) {
                specification = specification.and(buildSpecification(criteria.getDoiTuongBhytId(),
                    root -> root.join(TheBhyt_.doiTuongBhyt, JoinType.LEFT).get(DoiTuongBhyt_.id)));
            }
            if (criteria.getThongTinBhxhId() != null) {
                specification = specification.and(buildSpecification(criteria.getThongTinBhxhId(),
                    root -> root.join(TheBhyt_.thongTinBhxh, JoinType.LEFT).get(ThongTinBhxh_.id)));
            }
        }
        return specification;
    }


}

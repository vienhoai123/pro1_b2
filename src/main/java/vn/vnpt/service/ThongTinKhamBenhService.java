package vn.vnpt.service;

import vn.vnpt.domain.ThongTinKhamBenhId;
import vn.vnpt.domain.ThongTinKhoaId;
import vn.vnpt.service.dto.ThongTinKhamBenhDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.ThongTinKhamBenh}.
 */
public interface ThongTinKhamBenhService {

    /**
     * Save a thongTinKhamBenh.
     *
     * @param thongTinKhamBenhDTO the entity to save.
     * @return the persisted entity.
     */
    ThongTinKhamBenhDTO save(ThongTinKhamBenhDTO thongTinKhamBenhDTO);

    /**
     * Get all the thongTinKhamBenhs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ThongTinKhamBenhDTO> findAll(Pageable pageable);


    /**
     * Get the "id" thongTinKhamBenh.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ThongTinKhamBenhDTO> findOne(ThongTinKhamBenhId id);

    /**
     * Delete the "id" thongTinKhamBenh.
     *
     * @param id the id of the entity.
     */
    void delete(ThongTinKhamBenhId id);
}

package vn.vnpt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import vn.vnpt.domain.QuanHuyen;
import vn.vnpt.domain.*; // for static metamodels
import vn.vnpt.repository.QuanHuyenRepository;
import vn.vnpt.service.dto.QuanHuyenCriteria;
import vn.vnpt.service.dto.QuanHuyenDTO;
import vn.vnpt.service.mapper.QuanHuyenMapper;

/**
 * Service for executing complex queries for {@link QuanHuyen} entities in the database.
 * The main input is a {@link QuanHuyenCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link QuanHuyenDTO} or a {@link Page} of {@link QuanHuyenDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class QuanHuyenQueryService extends QueryService<QuanHuyen> {

    private final Logger log = LoggerFactory.getLogger(QuanHuyenQueryService.class);

    private final QuanHuyenRepository quanHuyenRepository;

    private final QuanHuyenMapper quanHuyenMapper;

    public QuanHuyenQueryService(QuanHuyenRepository quanHuyenRepository, QuanHuyenMapper quanHuyenMapper) {
        this.quanHuyenRepository = quanHuyenRepository;
        this.quanHuyenMapper = quanHuyenMapper;
    }

    /**
     * Return a {@link List} of {@link QuanHuyenDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<QuanHuyenDTO> findByCriteria(QuanHuyenCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<QuanHuyen> specification = createSpecification(criteria);
        return quanHuyenMapper.toDto(quanHuyenRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link QuanHuyenDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<QuanHuyenDTO> findByCriteria(QuanHuyenCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<QuanHuyen> specification = createSpecification(criteria);
        return quanHuyenRepository.findAll(specification, page)
            .map(quanHuyenMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(QuanHuyenCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<QuanHuyen> specification = createSpecification(criteria);
        return quanHuyenRepository.count(specification);
    }

    /**
     * Function to convert {@link QuanHuyenCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<QuanHuyen> createSpecification(QuanHuyenCriteria criteria) {
        Specification<QuanHuyen> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), QuanHuyen_.id));
            }
            if (criteria.getCap() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCap(), QuanHuyen_.cap));
            }
            if (criteria.getGuessPhrase() != null) {
                specification = specification.and(buildStringSpecification(criteria.getGuessPhrase(), QuanHuyen_.guessPhrase));
            }
            if (criteria.getTen() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTen(), QuanHuyen_.ten));
            }
            if (criteria.getTenKhongDau() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTenKhongDau(), QuanHuyen_.tenKhongDau));
            }
            if (criteria.getTinhThanhPhoId() != null) {
                specification = specification.and(buildSpecification(criteria.getTinhThanhPhoId(),
                    root -> root.join(QuanHuyen_.tinhThanhPho, JoinType.LEFT).get(TinhThanhPho_.id)));
            }
        }
        return specification;
    }
}

package vn.vnpt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import vn.vnpt.domain.ChiDinhXetNghiem;
import vn.vnpt.domain.*; // for static metamodels
import vn.vnpt.repository.ChiDinhXetNghiemRepository;
import vn.vnpt.service.dto.ChiDinhXetNghiemCriteria;
import vn.vnpt.service.dto.ChiDinhXetNghiemDTO;
import vn.vnpt.service.mapper.ChiDinhXetNghiemMapper;

/**
 * Service for executing complex queries for {@link ChiDinhXetNghiem} entities in the database.
 * The main input is a {@link ChiDinhXetNghiemCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ChiDinhXetNghiemDTO} or a {@link Page} of {@link ChiDinhXetNghiemDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ChiDinhXetNghiemQueryService extends QueryService<ChiDinhXetNghiem> {

    private final Logger log = LoggerFactory.getLogger(ChiDinhXetNghiemQueryService.class);

    private final ChiDinhXetNghiemRepository chiDinhXetNghiemRepository;

    private final ChiDinhXetNghiemMapper chiDinhXetNghiemMapper;

    public ChiDinhXetNghiemQueryService(ChiDinhXetNghiemRepository chiDinhXetNghiemRepository, ChiDinhXetNghiemMapper chiDinhXetNghiemMapper) {
        this.chiDinhXetNghiemRepository = chiDinhXetNghiemRepository;
        this.chiDinhXetNghiemMapper = chiDinhXetNghiemMapper;
    }

    /**
     * Return a {@link List} of {@link ChiDinhXetNghiemDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ChiDinhXetNghiemDTO> findByCriteria(ChiDinhXetNghiemCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ChiDinhXetNghiem> specification = createSpecification(criteria);
        return chiDinhXetNghiemMapper.toDto(chiDinhXetNghiemRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ChiDinhXetNghiemDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ChiDinhXetNghiemDTO> findByCriteria(ChiDinhXetNghiemCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ChiDinhXetNghiem> specification = createSpecification(criteria);
        return chiDinhXetNghiemRepository.findAll(specification, page)
            .map(chiDinhXetNghiemMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ChiDinhXetNghiemCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ChiDinhXetNghiem> specification = createSpecification(criteria);
        return chiDinhXetNghiemRepository.count(specification);
    }

    /**
     * Function to convert {@link ChiDinhXetNghiemCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ChiDinhXetNghiem> createSpecification(ChiDinhXetNghiemCriteria criteria) {
        Specification<ChiDinhXetNghiem> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ChiDinhXetNghiem_.id));
            }
            if (criteria.getCoBaoHiem() != null) {
                specification = specification.and(buildSpecification(criteria.getCoBaoHiem(), ChiDinhXetNghiem_.coBaoHiem));
            }
            if (criteria.getCoKetQua() != null) {
                specification = specification.and(buildSpecification(criteria.getCoKetQua(), ChiDinhXetNghiem_.coKetQua));
            }
            if (criteria.getDaThanhToan() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDaThanhToan(), ChiDinhXetNghiem_.daThanhToan));
            }
            if (criteria.getDaThanhToanChenhLech() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDaThanhToanChenhLech(), ChiDinhXetNghiem_.daThanhToanChenhLech));
            }
            if (criteria.getDaThucHien() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDaThucHien(), ChiDinhXetNghiem_.daThucHien));
            }
            if (criteria.getDonGia() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDonGia(), ChiDinhXetNghiem_.donGia));
            }
            if (criteria.getDonGiaBhyt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDonGiaBhyt(), ChiDinhXetNghiem_.donGiaBhyt));
            }
            if (criteria.getDonGiaKhongBhyt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDonGiaKhongBhyt(), ChiDinhXetNghiem_.donGiaKhongBhyt));
            }
            if (criteria.getGhiChuChiDinh() != null) {
                specification = specification.and(buildStringSpecification(criteria.getGhiChuChiDinh(), ChiDinhXetNghiem_.ghiChuChiDinh));
            }
            if (criteria.getMoTa() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMoTa(), ChiDinhXetNghiem_.moTa));
            }
            if (criteria.getNguoiChiDinhId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNguoiChiDinhId(), ChiDinhXetNghiem_.nguoiChiDinhId));
            }
            if (criteria.getSoLuong() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSoLuong(), ChiDinhXetNghiem_.soLuong));
            }
            if (criteria.getTen() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTen(), ChiDinhXetNghiem_.ten));
            }
            if (criteria.getThanhTien() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getThanhTien(), ChiDinhXetNghiem_.thanhTien));
            }
            if (criteria.getThanhTienBhyt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getThanhTienBhyt(), ChiDinhXetNghiem_.thanhTienBhyt));
            }
            if (criteria.getThanhTienKhongBHYT() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getThanhTienKhongBHYT(), ChiDinhXetNghiem_.thanhTienKhongBHYT));
            }
            if (criteria.getThoiGianChiDinh() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getThoiGianChiDinh(), ChiDinhXetNghiem_.thoiGianChiDinh));
            }
            if (criteria.getThoiGianTao() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getThoiGianTao(), ChiDinhXetNghiem_.thoiGianTao));
            }
            if (criteria.getTienNgoaiBHYT() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTienNgoaiBHYT(), ChiDinhXetNghiem_.tienNgoaiBHYT));
            }
            if (criteria.getThanhToanChenhLech() != null) {
                specification = specification.and(buildSpecification(criteria.getThanhToanChenhLech(), ChiDinhXetNghiem_.thanhToanChenhLech));
            }
            if (criteria.getTyLeThanhToan() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTyLeThanhToan(), ChiDinhXetNghiem_.tyLeThanhToan));
            }
            if (criteria.getMaDungChung() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMaDungChung(), ChiDinhXetNghiem_.maDungChung));
            }
            if (criteria.getDichVuYeuCau() != null) {
                specification = specification.and(buildSpecification(criteria.getDichVuYeuCau(), ChiDinhXetNghiem_.dichVuYeuCau));
            }
            if (criteria.getNam() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNam(), ChiDinhXetNghiem_.nam));
            }
            if (criteria.getBakbId() != null) {
                specification = specification.and(buildSpecification(criteria.getBakbId(), ChiDinhXetNghiem_.bakbId));
            }
            if (criteria.getBenhNhanId() != null) {
                specification = specification.and(buildSpecification(criteria.getBenhNhanId(), ChiDinhXetNghiem_.benhNhanId));
            }
            if (criteria.getDonViId() != null) {
                specification = specification.and(buildSpecification(criteria.getDonViId(), ChiDinhXetNghiem_.donViId));
            }
            if (criteria.getPhieuCDId() != null) {
                specification = specification.and(buildSpecification(criteria.getPhieuCDId(),
                    root -> root.join(ChiDinhXetNghiem_.phieuCD, JoinType.LEFT).get(PhieuChiDinhXetNghiem_.id)));
            }
            if (criteria.getBakbId() != null) {
                specification = specification.and(buildSpecification(criteria.getBakbId(),
                    root -> root.join(ChiDinhXetNghiem_.phieuCD, JoinType.LEFT).get(PhieuChiDinhXetNghiem_.bakbId)));
            }
            if (criteria.getBenhNhanId() != null) {
                specification = specification.and(buildSpecification(criteria.getBenhNhanId(),
                    root -> root.join(ChiDinhXetNghiem_.phieuCD, JoinType.LEFT).get(PhieuChiDinhXetNghiem_.benhNhanId)));
            }
            if (criteria.getDonViId() != null) {
                specification = specification.and(buildSpecification(criteria.getDonViId(),
                    root -> root.join(ChiDinhXetNghiem_.phieuCD, JoinType.LEFT).get(PhieuChiDinhXetNghiem_.donViId)));
            }
            if (criteria.getPhongId() != null) {
                specification = specification.and(buildSpecification(criteria.getPhongId(),
                    root -> root.join(ChiDinhXetNghiem_.phong, JoinType.LEFT).get(Phong_.id)));
            }
            if (criteria.getXetNghiemId() != null) {
                specification = specification.and(buildSpecification(criteria.getXetNghiemId(),
                    root -> root.join(ChiDinhXetNghiem_.xetNghiem, JoinType.LEFT).get(XetNghiem_.id)));
            }

        }
        return specification;
    }
}

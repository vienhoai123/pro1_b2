package vn.vnpt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import vn.vnpt.domain.HuongDieuTri;
import vn.vnpt.domain.*; // for static metamodels
import vn.vnpt.repository.HuongDieuTriRepository;
import vn.vnpt.service.dto.HuongDieuTriCriteria;
import vn.vnpt.service.dto.HuongDieuTriDTO;
import vn.vnpt.service.mapper.HuongDieuTriMapper;

/**
 * Service for executing complex queries for {@link HuongDieuTri} entities in the database.
 * The main input is a {@link HuongDieuTriCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link HuongDieuTriDTO} or a {@link Page} of {@link HuongDieuTriDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class HuongDieuTriQueryService extends QueryService<HuongDieuTri> {

    private final Logger log = LoggerFactory.getLogger(HuongDieuTriQueryService.class);

    private final HuongDieuTriRepository huongDieuTriRepository;

    private final HuongDieuTriMapper huongDieuTriMapper;

    public HuongDieuTriQueryService(HuongDieuTriRepository huongDieuTriRepository, HuongDieuTriMapper huongDieuTriMapper) {
        this.huongDieuTriRepository = huongDieuTriRepository;
        this.huongDieuTriMapper = huongDieuTriMapper;
    }

    /**
     * Return a {@link List} of {@link HuongDieuTriDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<HuongDieuTriDTO> findByCriteria(HuongDieuTriCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<HuongDieuTri> specification = createSpecification(criteria);
        return huongDieuTriMapper.toDto(huongDieuTriRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link HuongDieuTriDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<HuongDieuTriDTO> findByCriteria(HuongDieuTriCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<HuongDieuTri> specification = createSpecification(criteria);
        return huongDieuTriRepository.findAll(specification, page)
            .map(huongDieuTriMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(HuongDieuTriCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<HuongDieuTri> specification = createSpecification(criteria);
        return huongDieuTriRepository.count(specification);
    }

    /**
     * Function to convert {@link HuongDieuTriCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<HuongDieuTri> createSpecification(HuongDieuTriCriteria criteria) {
        Specification<HuongDieuTri> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), HuongDieuTri_.id));
            }
            if (criteria.getNoiTru() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNoiTru(), HuongDieuTri_.noiTru));
            }
            if (criteria.getTen() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTen(), HuongDieuTri_.ten));
            }
            if (criteria.getMa() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getMa(), HuongDieuTri_.ma));
            }
        }
        return specification;
    }
}

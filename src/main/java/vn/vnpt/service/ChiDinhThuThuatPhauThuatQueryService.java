package vn.vnpt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import vn.vnpt.domain.ChiDinhThuThuatPhauThuat;
import vn.vnpt.domain.*; // for static metamodels
import vn.vnpt.repository.ChiDinhThuThuatPhauThuatRepository;
import vn.vnpt.service.dto.ChiDinhThuThuatPhauThuatCriteria;
import vn.vnpt.service.dto.ChiDinhThuThuatPhauThuatDTO;
import vn.vnpt.service.mapper.ChiDinhThuThuatPhauThuatMapper;

/**
 * Service for executing complex queries for {@link ChiDinhThuThuatPhauThuat} entities in the database.
 * The main input is a {@link ChiDinhThuThuatPhauThuatCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ChiDinhThuThuatPhauThuatDTO} or a {@link Page} of {@link ChiDinhThuThuatPhauThuatDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ChiDinhThuThuatPhauThuatQueryService extends QueryService<ChiDinhThuThuatPhauThuat> {

    private final Logger log = LoggerFactory.getLogger(ChiDinhThuThuatPhauThuatQueryService.class);

    private final ChiDinhThuThuatPhauThuatRepository chiDinhThuThuatPhauThuatRepository;

    private final ChiDinhThuThuatPhauThuatMapper chiDinhThuThuatPhauThuatMapper;

    public ChiDinhThuThuatPhauThuatQueryService(ChiDinhThuThuatPhauThuatRepository chiDinhThuThuatPhauThuatRepository, ChiDinhThuThuatPhauThuatMapper chiDinhThuThuatPhauThuatMapper) {
        this.chiDinhThuThuatPhauThuatRepository = chiDinhThuThuatPhauThuatRepository;
        this.chiDinhThuThuatPhauThuatMapper = chiDinhThuThuatPhauThuatMapper;
    }

    /**
     * Return a {@link List} of {@link ChiDinhThuThuatPhauThuatDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ChiDinhThuThuatPhauThuatDTO> findByCriteria(ChiDinhThuThuatPhauThuatCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ChiDinhThuThuatPhauThuat> specification = createSpecification(criteria);
        return chiDinhThuThuatPhauThuatMapper.toDto(chiDinhThuThuatPhauThuatRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ChiDinhThuThuatPhauThuatDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ChiDinhThuThuatPhauThuatDTO> findByCriteria(ChiDinhThuThuatPhauThuatCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ChiDinhThuThuatPhauThuat> specification = createSpecification(criteria);
        return chiDinhThuThuatPhauThuatRepository.findAll(specification, page)
            .map(chiDinhThuThuatPhauThuatMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ChiDinhThuThuatPhauThuatCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ChiDinhThuThuatPhauThuat> specification = createSpecification(criteria);
        return chiDinhThuThuatPhauThuatRepository.count(specification);
    }

    /**
     * Function to convert {@link ChiDinhThuThuatPhauThuatCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ChiDinhThuThuatPhauThuat> createSpecification(ChiDinhThuThuatPhauThuatCriteria criteria) {
        Specification<ChiDinhThuThuatPhauThuat> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ChiDinhThuThuatPhauThuat_.id));
            }
            if (criteria.getCoBaoHiem() != null) {
                specification = specification.and(buildSpecification(criteria.getCoBaoHiem(), ChiDinhThuThuatPhauThuat_.coBaoHiem));
            }
            if (criteria.getCoKetQua() != null) {
                specification = specification.and(buildSpecification(criteria.getCoKetQua(), ChiDinhThuThuatPhauThuat_.coKetQua));
            }
            if (criteria.getDaThanhToan() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDaThanhToan(), ChiDinhThuThuatPhauThuat_.daThanhToan));
            }
            if (criteria.getDaThanhToanChenhLech() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDaThanhToanChenhLech(), ChiDinhThuThuatPhauThuat_.daThanhToanChenhLech));
            }
            if (criteria.getDaThucHien() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDaThucHien(), ChiDinhThuThuatPhauThuat_.daThucHien));
            }
            if (criteria.getDonGia() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDonGia(), ChiDinhThuThuatPhauThuat_.donGia));
            }
            if (criteria.getDonGiaBhyt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDonGiaBhyt(), ChiDinhThuThuatPhauThuat_.donGiaBhyt));
            }
            if (criteria.getDonGiaKhongBhyt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDonGiaKhongBhyt(), ChiDinhThuThuatPhauThuat_.donGiaKhongBhyt));
            }
            if (criteria.getGhiChuChiDinh() != null) {
                specification = specification.and(buildStringSpecification(criteria.getGhiChuChiDinh(), ChiDinhThuThuatPhauThuat_.ghiChuChiDinh));
            }
            if (criteria.getMoTa() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMoTa(), ChiDinhThuThuatPhauThuat_.moTa));
            }
            if (criteria.getNguoiChiDinhId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNguoiChiDinhId(), ChiDinhThuThuatPhauThuat_.nguoiChiDinhId));
            }
            if (criteria.getSoLuong() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSoLuong(), ChiDinhThuThuatPhauThuat_.soLuong));
            }
            if (criteria.getTen() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTen(), ChiDinhThuThuatPhauThuat_.ten));
            }
            if (criteria.getThanhTien() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getThanhTien(), ChiDinhThuThuatPhauThuat_.thanhTien));
            }
            if (criteria.getThanhTienBhyt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getThanhTienBhyt(), ChiDinhThuThuatPhauThuat_.thanhTienBhyt));
            }
            if (criteria.getThanhTienKhongBHYT() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getThanhTienKhongBHYT(), ChiDinhThuThuatPhauThuat_.thanhTienKhongBHYT));
            }
            if (criteria.getThoiGianChiDinh() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getThoiGianChiDinh(), ChiDinhThuThuatPhauThuat_.thoiGianChiDinh));
            }
            if (criteria.getThoiGianTao() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getThoiGianTao(), ChiDinhThuThuatPhauThuat_.thoiGianTao));
            }
            if (criteria.getTienNgoaiBHYT() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTienNgoaiBHYT(), ChiDinhThuThuatPhauThuat_.tienNgoaiBHYT));
            }
            if (criteria.getThanhToanChenhLech() != null) {
                specification = specification.and(buildSpecification(criteria.getThanhToanChenhLech(), ChiDinhThuThuatPhauThuat_.thanhToanChenhLech));
            }
            if (criteria.getTyLeThanhToan() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTyLeThanhToan(), ChiDinhThuThuatPhauThuat_.tyLeThanhToan));
            }
            if (criteria.getMaDungChung() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMaDungChung(), ChiDinhThuThuatPhauThuat_.maDungChung));
            }
            if (criteria.getDichVuYeuCau() != null) {
                specification = specification.and(buildSpecification(criteria.getDichVuYeuCau(), ChiDinhThuThuatPhauThuat_.dichVuYeuCau));
            }
            if (criteria.getNam() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNam(), ChiDinhThuThuatPhauThuat_.nam));
            }

          if (criteria.getPhieuCDId() != null) {
                specification = specification.and(buildSpecification(criteria.getPhieuCDId(),
                    root -> root.join(ChiDinhThuThuatPhauThuat_.phieuCD, JoinType.LEFT).get(PhieuChiDinhTTPT_.id)));
            }
            if (criteria.getBakbId() != null) {
                specification = specification.and(buildSpecification(criteria.getBakbId(),
                    root -> root.join(ChiDinhThuThuatPhauThuat_.phieuCD, JoinType.LEFT).get(PhieuChiDinhTTPT_.bakbId)));
            }
            if (criteria.getBenhNhanId() != null) {
                specification = specification.and(buildSpecification(criteria.getBenhNhanId(),
                    root -> root.join(ChiDinhThuThuatPhauThuat_.phieuCD, JoinType.LEFT).get(PhieuChiDinhTTPT_.benhNhanId)));
            }
            if (criteria.getDonViId() != null) {
                specification = specification.and(buildSpecification(criteria.getDonViId(),
                    root -> root.join(ChiDinhThuThuatPhauThuat_.phieuCD, JoinType.LEFT).get(PhieuChiDinhTTPT_.donViId)));
            }
            if (criteria.getPhongId() != null) {
                specification = specification.and(buildSpecification(criteria.getPhongId(),
                    root -> root.join(ChiDinhThuThuatPhauThuat_.phong, JoinType.LEFT).get(Phong_.id)));
            }
            if (criteria.getTtptId() != null) {
                specification = specification.and(buildSpecification(criteria.getTtptId(),
                    root -> root.join(ChiDinhThuThuatPhauThuat_.ttpt, JoinType.LEFT).get(ThuThuatPhauThuat_.id)));
            }
        }
        return specification;
    }
}

package vn.vnpt.service;

import io.github.jhipster.service.QueryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.domain.*;
import vn.vnpt.repository.DotGiaDichVuBhxhRepository;
import vn.vnpt.service.dto.DotGiaDichVuBhxhCriteria;
import vn.vnpt.service.dto.DotGiaDichVuBhxhDTO;
import vn.vnpt.service.mapper.DotGiaDichVuBhxhMapper;

import javax.persistence.criteria.JoinType;
import java.util.List;

/**
 * Service for executing complex queries for {@link DotGiaDichVuBhxh} entities in the database.
 * The main input is a {@link DotGiaDichVuBhxhCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link DotGiaDichVuBhxhDTO} or a {@link Page} of {@link DotGiaDichVuBhxhDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class DotGiaDichVuBhxhQueryService extends QueryService<DotGiaDichVuBhxh> {

    private final Logger log = LoggerFactory.getLogger(DotGiaDichVuBhxhQueryService.class);

    private final DotGiaDichVuBhxhRepository dotGiaDichVuBhxhRepository;

    private final DotGiaDichVuBhxhMapper dotGiaDichVuBhxhMapper;

    public DotGiaDichVuBhxhQueryService(DotGiaDichVuBhxhRepository dotGiaDichVuBhxhRepository, DotGiaDichVuBhxhMapper dotGiaDichVuBhxhMapper) {
        this.dotGiaDichVuBhxhRepository = dotGiaDichVuBhxhRepository;
        this.dotGiaDichVuBhxhMapper = dotGiaDichVuBhxhMapper;
    }

    /**
     * Return a {@link List} of {@link DotGiaDichVuBhxhDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<DotGiaDichVuBhxhDTO> findByCriteria(DotGiaDichVuBhxhCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<DotGiaDichVuBhxh> specification = createSpecification(criteria);
        return dotGiaDichVuBhxhMapper.toDto(dotGiaDichVuBhxhRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link DotGiaDichVuBhxhDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<DotGiaDichVuBhxhDTO> findByCriteria(DotGiaDichVuBhxhCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<DotGiaDichVuBhxh> specification = createSpecification(criteria);
        return dotGiaDichVuBhxhRepository.findAll(specification, page)
            .map(dotGiaDichVuBhxhMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(DotGiaDichVuBhxhCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<DotGiaDichVuBhxh> specification = createSpecification(criteria);
        return dotGiaDichVuBhxhRepository.count(specification);
    }

    /**
     * Function to convert {@link DotGiaDichVuBhxhCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<DotGiaDichVuBhxh> createSpecification(DotGiaDichVuBhxhCriteria criteria) {
        Specification<DotGiaDichVuBhxh> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), DotGiaDichVuBhxh_.id));
            }
            if (criteria.getDoiTuongApDung() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDoiTuongApDung(), DotGiaDichVuBhxh_.doiTuongApDung));
            }
            if (criteria.getGhiChu() != null) {
                specification = specification.and(buildStringSpecification(criteria.getGhiChu(), DotGiaDichVuBhxh_.ghiChu));
            }
            if (criteria.getNgayApDung() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNgayApDung(), DotGiaDichVuBhxh_.ngayApDung));
            }
            if (criteria.getTen() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTen(), DotGiaDichVuBhxh_.ten));
            }
            if (criteria.getDonViId() != null) {
                specification = specification.and(buildSpecification(criteria.getDonViId(),
                    root -> root.join(DotGiaDichVuBhxh_.donVi, JoinType.LEFT).get(DonVi_.id)));
            }
        }
        return specification;
    }
}

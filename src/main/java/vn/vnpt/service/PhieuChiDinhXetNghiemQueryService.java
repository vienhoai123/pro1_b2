package vn.vnpt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import vn.vnpt.domain.PhieuChiDinhXetNghiem;
import vn.vnpt.domain.*; // for static metamodels
import vn.vnpt.repository.PhieuChiDinhXetNghiemRepository;
import vn.vnpt.service.dto.PhieuChiDinhXetNghiemCriteria;
import vn.vnpt.service.dto.PhieuChiDinhXetNghiemDTO;
import vn.vnpt.service.mapper.PhieuChiDinhXetNghiemMapper;

/**
 * Service for executing complex queries for {@link PhieuChiDinhXetNghiem} entities in the database.
 * The main input is a {@link PhieuChiDinhXetNghiemCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link PhieuChiDinhXetNghiemDTO} or a {@link Page} of {@link PhieuChiDinhXetNghiemDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class PhieuChiDinhXetNghiemQueryService extends QueryService<PhieuChiDinhXetNghiem> {

    private final Logger log = LoggerFactory.getLogger(PhieuChiDinhXetNghiemQueryService.class);

    private final PhieuChiDinhXetNghiemRepository phieuChiDinhXetNghiemRepository;

    private final PhieuChiDinhXetNghiemMapper phieuChiDinhXetNghiemMapper;

    public PhieuChiDinhXetNghiemQueryService(PhieuChiDinhXetNghiemRepository phieuChiDinhXetNghiemRepository, PhieuChiDinhXetNghiemMapper phieuChiDinhXetNghiemMapper) {
        this.phieuChiDinhXetNghiemRepository = phieuChiDinhXetNghiemRepository;
        this.phieuChiDinhXetNghiemMapper = phieuChiDinhXetNghiemMapper;
    }

    /**
     * Return a {@link List} of {@link PhieuChiDinhXetNghiemDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<PhieuChiDinhXetNghiemDTO> findByCriteria(PhieuChiDinhXetNghiemCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<PhieuChiDinhXetNghiem> specification = createSpecification(criteria);
        return phieuChiDinhXetNghiemMapper.toDto(phieuChiDinhXetNghiemRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link PhieuChiDinhXetNghiemDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<PhieuChiDinhXetNghiemDTO> findByCriteria(PhieuChiDinhXetNghiemCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<PhieuChiDinhXetNghiem> specification = createSpecification(criteria);
        return phieuChiDinhXetNghiemRepository.findAll(specification, page)
            .map(phieuChiDinhXetNghiemMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(PhieuChiDinhXetNghiemCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<PhieuChiDinhXetNghiem> specification = createSpecification(criteria);
        return phieuChiDinhXetNghiemRepository.count(specification);
    }

    /**
     * Function to convert {@link PhieuChiDinhXetNghiemCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<PhieuChiDinhXetNghiem> createSpecification(PhieuChiDinhXetNghiemCriteria criteria) {
        Specification<PhieuChiDinhXetNghiem> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), PhieuChiDinhXetNghiem_.id));
            }
            if (criteria.getChanDoanTongQuat() != null) {
                specification = specification.and(buildStringSpecification(criteria.getChanDoanTongQuat(), PhieuChiDinhXetNghiem_.chanDoanTongQuat));
            }
            if (criteria.getGhiChu() != null) {
                specification = specification.and(buildStringSpecification(criteria.getGhiChu(), PhieuChiDinhXetNghiem_.ghiChu));
            }
            if (criteria.getKetQuaTongQuat() != null) {
                specification = specification.and(buildStringSpecification(criteria.getKetQuaTongQuat(), PhieuChiDinhXetNghiem_.ketQuaTongQuat));
            }
            if (criteria.getNam() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNam(), PhieuChiDinhXetNghiem_.nam));
            }
//            if (criteria.getBenhNhanId() != null) {
//                specification = specification.and(buildSpecification(criteria.getBenhNhanId(),
//                    root -> root.join(PhieuChiDinhXetNghiem_.benhNhanId, JoinType.LEFT).get(BenhAnKhamBenh_.benhNhanId)));
//            }
//            if (criteria.getDonViId() != null) {
//                specification = specification.and(buildSpecification(criteria.getDonViId(),
//                    root -> root.join(PhieuChiDinhXetNghiem_.donViId, JoinType.LEFT).get(BenhAnKhamBenh_.id)));
//            }
//            if (criteria.getBakbId() != null) {
//                specification = specification.and(buildSpecification(criteria.getBakbId(),
//                    root -> root.join(PhieuChiDinhXetNghiem_.bakbId, JoinType.LEFT).get(BenhAnKhamBenh_.id)));
//            }
            if (criteria.getBakbId() != null) {
                specification = specification.and(buildSpecification(criteria.getBakbId(), PhieuChiDinhXetNghiem_.bakbId));
            }
            if (criteria.getBenhNhanId() != null) {
                specification = specification.and(buildSpecification(criteria.getBenhNhanId(), PhieuChiDinhXetNghiem_.benhNhanId));
            }
            if (criteria.getDonViId() != null) {
                specification = specification.and(buildSpecification(criteria.getDonViId(), PhieuChiDinhXetNghiem_.donViId));
            }
        }
        return specification;
    }
}

package vn.vnpt.service;

import vn.vnpt.domain.PhieuChiDinhXetNghiemId;
import vn.vnpt.service.dto.PhieuChiDinhXetNghiemDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.PhieuChiDinhXetNghiem}.
 */
public interface PhieuChiDinhXetNghiemService {

    /**
     * Save a phieuChiDinhXetNghiem.
     *
     * @param phieuChiDinhXetNghiemDTO the entity to save.
     * @return the persisted entity.
     */
    PhieuChiDinhXetNghiemDTO save(PhieuChiDinhXetNghiemDTO phieuChiDinhXetNghiemDTO);

    /**
     * Get all the phieuChiDinhXetNghiems.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PhieuChiDinhXetNghiemDTO> findAll(Pageable pageable);

    /**
     * Get the "id" phieuChiDinhXetNghiem.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PhieuChiDinhXetNghiemDTO> findOne(PhieuChiDinhXetNghiemId id);

    /**
     * Delete the "id" phieuChiDinhXetNghiem.
     *
     * @param id the id of the entity.
     */
    void delete(PhieuChiDinhXetNghiemId id);
}

package vn.vnpt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import vn.vnpt.domain.LoaiChanDoanHinhAnh;
import vn.vnpt.domain.*; // for static metamodels
import vn.vnpt.repository.LoaiChanDoanHinhAnhRepository;
import vn.vnpt.service.dto.LoaiChanDoanHinhAnhCriteria;
import vn.vnpt.service.dto.LoaiChanDoanHinhAnhDTO;
import vn.vnpt.service.mapper.LoaiChanDoanHinhAnhMapper;

/**
 * Service for executing complex queries for {@link LoaiChanDoanHinhAnh} entities in the database.
 * The main input is a {@link LoaiChanDoanHinhAnhCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link LoaiChanDoanHinhAnhDTO} or a {@link Page} of {@link LoaiChanDoanHinhAnhDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class LoaiChanDoanHinhAnhQueryService extends QueryService<LoaiChanDoanHinhAnh> {

    private final Logger log = LoggerFactory.getLogger(LoaiChanDoanHinhAnhQueryService.class);

    private final LoaiChanDoanHinhAnhRepository loaiChanDoanHinhAnhRepository;

    private final LoaiChanDoanHinhAnhMapper loaiChanDoanHinhAnhMapper;

    public LoaiChanDoanHinhAnhQueryService(LoaiChanDoanHinhAnhRepository loaiChanDoanHinhAnhRepository, LoaiChanDoanHinhAnhMapper loaiChanDoanHinhAnhMapper) {
        this.loaiChanDoanHinhAnhRepository = loaiChanDoanHinhAnhRepository;
        this.loaiChanDoanHinhAnhMapper = loaiChanDoanHinhAnhMapper;
    }

    /**
     * Return a {@link List} of {@link LoaiChanDoanHinhAnhDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<LoaiChanDoanHinhAnhDTO> findByCriteria(LoaiChanDoanHinhAnhCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<LoaiChanDoanHinhAnh> specification = createSpecification(criteria);
        return loaiChanDoanHinhAnhMapper.toDto(loaiChanDoanHinhAnhRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link LoaiChanDoanHinhAnhDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<LoaiChanDoanHinhAnhDTO> findByCriteria(LoaiChanDoanHinhAnhCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<LoaiChanDoanHinhAnh> specification = createSpecification(criteria);
        return loaiChanDoanHinhAnhRepository.findAll(specification, page)
            .map(loaiChanDoanHinhAnhMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(LoaiChanDoanHinhAnhCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<LoaiChanDoanHinhAnh> specification = createSpecification(criteria);
        return loaiChanDoanHinhAnhRepository.count(specification);
    }

    /**
     * Function to convert {@link LoaiChanDoanHinhAnhCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<LoaiChanDoanHinhAnh> createSpecification(LoaiChanDoanHinhAnhCriteria criteria) {
        Specification<LoaiChanDoanHinhAnh> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), LoaiChanDoanHinhAnh_.id));
            }
            if (criteria.getTen() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTen(), LoaiChanDoanHinhAnh_.ten));
            }
            if (criteria.getMoTa() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMoTa(), LoaiChanDoanHinhAnh_.moTa));
            }
            if (criteria.getEnable() != null) {
                specification = specification.and(buildSpecification(criteria.getEnable(), LoaiChanDoanHinhAnh_.enable));
            }
            if (criteria.getUuTien() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUuTien(), LoaiChanDoanHinhAnh_.uuTien));
            }
            if (criteria.getMaPhanLoai() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMaPhanLoai(), LoaiChanDoanHinhAnh_.maPhanLoai));
            }
            if (criteria.getDonViId() != null) {
                specification = specification.and(buildSpecification(criteria.getDonViId(),
                    root -> root.join(LoaiChanDoanHinhAnh_.donVi, JoinType.LEFT).get(DonVi_.id)));
            }
        }
        return specification;
    }
}

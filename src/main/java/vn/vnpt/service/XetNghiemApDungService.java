package vn.vnpt.service;

import vn.vnpt.service.dto.XetNghiemApDungDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.XetNghiemApDung}.
 */
public interface XetNghiemApDungService {

    /**
     * Save a xetNghiemApDung.
     *
     * @param xetNghiemApDungDTO the entity to save.
     * @return the persisted entity.
     */
    XetNghiemApDungDTO save(XetNghiemApDungDTO xetNghiemApDungDTO);

    /**
     * Get all the xetNghiemApDungs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<XetNghiemApDungDTO> findAll(Pageable pageable);

    /**
     * Get the "id" xetNghiemApDung.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<XetNghiemApDungDTO> findOne(Long id);

    /**
     * Delete the "id" xetNghiemApDung.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}

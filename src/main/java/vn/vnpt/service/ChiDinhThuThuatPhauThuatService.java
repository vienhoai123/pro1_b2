package vn.vnpt.service;

import vn.vnpt.domain.ChiDinhTTPTId;
import vn.vnpt.service.dto.ChiDinhThuThuatPhauThuatDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.ChiDinhThuThuatPhauThuat}.
 */
public interface ChiDinhThuThuatPhauThuatService {

    /**
     * Save a chiDinhThuThuatPhauThuat.
     *
     * @param chiDinhThuThuatPhauThuatDTO the entity to save.
     * @return the persisted entity.
     */
    ChiDinhThuThuatPhauThuatDTO save(ChiDinhThuThuatPhauThuatDTO chiDinhThuThuatPhauThuatDTO);

    /**
     * Get all the chiDinhThuThuatPhauThuats.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ChiDinhThuThuatPhauThuatDTO> findAll(Pageable pageable);

    /**
     * Get the "id" chiDinhThuThuatPhauThuat.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ChiDinhThuThuatPhauThuatDTO> findOne(ChiDinhTTPTId id);

    /**
     * Delete the "id" chiDinhThuThuatPhauThuat.
     *
     * @param id the id of the entity.
     */
    void delete(ChiDinhTTPTId id);
}

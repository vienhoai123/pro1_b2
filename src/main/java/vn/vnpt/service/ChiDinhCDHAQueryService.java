package vn.vnpt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import vn.vnpt.domain.ChiDinhCDHA;
import vn.vnpt.domain.*; // for static metamodels
import vn.vnpt.repository.ChiDinhCDHARepository;
import vn.vnpt.service.dto.ChiDinhCDHACriteria;
import vn.vnpt.service.dto.ChiDinhCDHADTO;
import vn.vnpt.service.mapper.ChiDinhCDHAMapper;

/**
 * Service for executing complex queries for {@link ChiDinhCDHA} entities in the database.
 * The main input is a {@link ChiDinhCDHACriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ChiDinhCDHADTO} or a {@link Page} of {@link ChiDinhCDHADTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ChiDinhCDHAQueryService extends QueryService<ChiDinhCDHA> {

    private final Logger log = LoggerFactory.getLogger(ChiDinhCDHAQueryService.class);

    private final ChiDinhCDHARepository chiDinhCDHARepository;

    private final ChiDinhCDHAMapper chiDinhCDHAMapper;

    public ChiDinhCDHAQueryService(ChiDinhCDHARepository chiDinhCDHARepository, ChiDinhCDHAMapper chiDinhCDHAMapper) {
        this.chiDinhCDHARepository = chiDinhCDHARepository;
        this.chiDinhCDHAMapper = chiDinhCDHAMapper;
    }

    /**
     * Return a {@link List} of {@link ChiDinhCDHADTO} which matches the criteria from the database.
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ChiDinhCDHADTO> findByCriteria(ChiDinhCDHACriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ChiDinhCDHA> specification = createSpecification(criteria);
        return chiDinhCDHAMapper.toDto(chiDinhCDHARepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ChiDinhCDHADTO} which matches the criteria from the database.
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page     The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ChiDinhCDHADTO> findByCriteria(ChiDinhCDHACriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ChiDinhCDHA> specification = createSpecification(criteria);
        return chiDinhCDHARepository.findAll(specification, page)
            .map(chiDinhCDHAMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ChiDinhCDHACriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ChiDinhCDHA> specification = createSpecification(criteria);
        return chiDinhCDHARepository.count(specification);
    }

    /**
     * Function to convert {@link ChiDinhCDHACriteria} to a {@link Specification}
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ChiDinhCDHA> createSpecification(ChiDinhCDHACriteria criteria) {
        Specification<ChiDinhCDHA> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ChiDinhCDHA_.id));
            }
            if (criteria.getCoBaoHiem() != null) {
                specification = specification.and(buildSpecification(criteria.getCoBaoHiem(), ChiDinhCDHA_.coBaoHiem));
            }
            if (criteria.getCoKetQua() != null) {
                specification = specification.and(buildSpecification(criteria.getCoKetQua(), ChiDinhCDHA_.coKetQua));
            }
            if (criteria.getDaThanhToan() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDaThanhToan(), ChiDinhCDHA_.daThanhToan));
            }
            if (criteria.getDaThanhToanChenhLech() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDaThanhToanChenhLech(), ChiDinhCDHA_.daThanhToanChenhLech));
            }
            if (criteria.getDaThucHien() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDaThucHien(), ChiDinhCDHA_.daThucHien));
            }
            if (criteria.getDonGia() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDonGia(), ChiDinhCDHA_.donGia));
            }
            if (criteria.getDonGiaBhyt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDonGiaBhyt(), ChiDinhCDHA_.donGiaBhyt));
            }
            if (criteria.getDonGiaKhongBhyt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDonGiaKhongBhyt(), ChiDinhCDHA_.donGiaKhongBhyt));
            }
            if (criteria.getGhiChuChiDinh() != null) {
                specification = specification.and(buildStringSpecification(criteria.getGhiChuChiDinh(), ChiDinhCDHA_.ghiChuChiDinh));
            }
            if (criteria.getMoTa() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMoTa(), ChiDinhCDHA_.moTa));
            }
            if (criteria.getMoTaXm15() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMoTaXm15(), ChiDinhCDHA_.moTaXm15));
            }
            if (criteria.getNguoiChiDinhId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNguoiChiDinhId(), ChiDinhCDHA_.nguoiChiDinhId));
            }
            if (criteria.getNguoiChiDinh() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNguoiChiDinh(), ChiDinhCDHA_.nguoiChiDinh));
            }
            if (criteria.getSoLanChup() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSoLanChup(), ChiDinhCDHA_.soLanChup));
            }
            if (criteria.getSoLuong() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSoLuong(), ChiDinhCDHA_.soLuong));
            }
            if (criteria.getSoLuongCoFilm() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSoLuongCoFilm(), ChiDinhCDHA_.soLuongCoFilm));
            }
            if (criteria.getSoLuongCoFilm1318() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSoLuongCoFilm1318(), ChiDinhCDHA_.soLuongCoFilm1318));
            }
            if (criteria.getSoLuongCoFilm1820() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSoLuongCoFilm1820(), ChiDinhCDHA_.soLuongCoFilm1820));
            }
            if (criteria.getSoLuongCoFilm2025() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSoLuongCoFilm2025(), ChiDinhCDHA_.soLuongCoFilm2025));
            }
            if (criteria.getSoLuongCoFilm2430() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSoLuongCoFilm2430(), ChiDinhCDHA_.soLuongCoFilm2430));
            }
            if (criteria.getSoLuongCoFilm2530() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSoLuongCoFilm2530(), ChiDinhCDHA_.soLuongCoFilm2530));
            }
            if (criteria.getSoLuongCoFilm3040() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSoLuongCoFilm3040(), ChiDinhCDHA_.soLuongCoFilm3040));
            }
            if (criteria.getSoLuongFilm() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSoLuongFilm(), ChiDinhCDHA_.soLuongFilm));
            }
            if (criteria.getTen() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTen(), ChiDinhCDHA_.ten));
            }
            if (criteria.getThanhTien() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getThanhTien(), ChiDinhCDHA_.thanhTien));
            }
            if (criteria.getThanhTienBhyt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getThanhTienBhyt(), ChiDinhCDHA_.thanhTienBhyt));
            }
            if (criteria.getThanhTienKhongBHYT() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getThanhTienKhongBHYT(), ChiDinhCDHA_.thanhTienKhongBHYT));
            }
            if (criteria.getThoiGianChiDinh() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getThoiGianChiDinh(), ChiDinhCDHA_.thoiGianChiDinh));
            }
            if (criteria.getThoiGianTao() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getThoiGianTao(), ChiDinhCDHA_.thoiGianTao));
            }
            if (criteria.getTienNgoaiBHYT() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTienNgoaiBHYT(), ChiDinhCDHA_.tienNgoaiBHYT));
            }
            if (criteria.getThanhToanChenhLech() != null) {
                specification = specification.and(buildSpecification(criteria.getThanhToanChenhLech(), ChiDinhCDHA_.thanhToanChenhLech));
            }
            if (criteria.getTyLeThanhToan() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTyLeThanhToan(), ChiDinhCDHA_.tyLeThanhToan));
            }
            if (criteria.getMaDungChung() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMaDungChung(), ChiDinhCDHA_.maDungChung));
            }
            if (criteria.getDichVuYeuCau() != null) {
                specification = specification.and(buildSpecification(criteria.getDichVuYeuCau(), ChiDinhCDHA_.dichVuYeuCau));
            }
            if (criteria.getNam() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNam(), ChiDinhCDHA_.nam));
            }
            if (criteria.getPhieuCDId() != null) {
                specification = specification.and(buildSpecification(criteria.getPhieuCDId(),
                    root -> root.join(ChiDinhCDHA_.phieuCD, JoinType.LEFT).get(PhieuChiDinhCDHA_.id)));
            }
            if (criteria.getBakbId() != null) {
                specification = specification.and(buildSpecification(criteria.getBakbId(),
                    root -> root.join(ChiDinhCDHA_.phieuCD, JoinType.LEFT).get(PhieuChiDinhCDHA_.bakbId)));
            }
            if (criteria.getBenhNhanId() != null) {
                specification = specification.and(buildSpecification(criteria.getBenhNhanId(),
                    root -> root.join(ChiDinhCDHA_.phieuCD, JoinType.LEFT).get(PhieuChiDinhCDHA_.benhNhanId)));
            }
            if (criteria.getDonViId() != null) {
                specification = specification.and(buildSpecification(criteria.getDonViId(),
                    root -> root.join(ChiDinhCDHA_.phieuCD, JoinType.LEFT).get(PhieuChiDinhCDHA_.donViId)));
            }
        }
        if (criteria.getPhongId() != null) {
            specification = specification.and(buildSpecification(criteria.getPhongId(),
                root -> root.join(ChiDinhCDHA_.phong, JoinType.LEFT).get(Phong_.id)));
        }
        if (criteria.getCdhaId() != null) {
            specification = specification.and(buildSpecification(criteria.getCdhaId(),
                root -> root.join(ChiDinhCDHA_.cdha, JoinType.LEFT).get(ChanDoanHinhAnh_.id)));
        }
        return specification;
    }
}

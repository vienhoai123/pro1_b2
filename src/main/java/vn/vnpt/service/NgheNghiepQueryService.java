package vn.vnpt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import vn.vnpt.domain.NgheNghiep;
import vn.vnpt.domain.*; // for static metamodels
import vn.vnpt.repository.NgheNghiepRepository;
import vn.vnpt.service.dto.NgheNghiepCriteria;
import vn.vnpt.service.dto.NgheNghiepDTO;
import vn.vnpt.service.mapper.NgheNghiepMapper;

/**
 * Service for executing complex queries for {@link NgheNghiep} entities in the database.
 * The main input is a {@link NgheNghiepCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link NgheNghiepDTO} or a {@link Page} of {@link NgheNghiepDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class NgheNghiepQueryService extends QueryService<NgheNghiep> {

    private final Logger log = LoggerFactory.getLogger(NgheNghiepQueryService.class);

    private final NgheNghiepRepository ngheNghiepRepository;

    private final NgheNghiepMapper ngheNghiepMapper;

    public NgheNghiepQueryService(NgheNghiepRepository ngheNghiepRepository, NgheNghiepMapper ngheNghiepMapper) {
        this.ngheNghiepRepository = ngheNghiepRepository;
        this.ngheNghiepMapper = ngheNghiepMapper;
    }

    /**
     * Return a {@link List} of {@link NgheNghiepDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<NgheNghiepDTO> findByCriteria(NgheNghiepCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<NgheNghiep> specification = createSpecification(criteria);
        return ngheNghiepMapper.toDto(ngheNghiepRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link NgheNghiepDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<NgheNghiepDTO> findByCriteria(NgheNghiepCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<NgheNghiep> specification = createSpecification(criteria);
        return ngheNghiepRepository.findAll(specification, page)
            .map(ngheNghiepMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(NgheNghiepCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<NgheNghiep> specification = createSpecification(criteria);
        return ngheNghiepRepository.count(specification);
    }

    /**
     * Function to convert {@link NgheNghiepCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<NgheNghiep> createSpecification(NgheNghiepCriteria criteria) {
        Specification<NgheNghiep> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), NgheNghiep_.id));
            }
            if (criteria.getMa4069() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getMa4069(), NgheNghiep_.ma4069));
            }
            if (criteria.getTen4069() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTen4069(), NgheNghiep_.ten4069));
            }
        }
        return specification;
    }
}

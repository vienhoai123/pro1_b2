package vn.vnpt.service;

import vn.vnpt.service.dto.DotGiaDichVuBhxhDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.DotGiaDichVuBhxh}.
 */
public interface DotGiaDichVuBhxhService {

    /**
     * Save a dotGiaDichVuBhxh.
     *
     * @param dotGiaDichVuBhxhDTO the entity to save.
     * @return the persisted entity.
     */
    DotGiaDichVuBhxhDTO save(DotGiaDichVuBhxhDTO dotGiaDichVuBhxhDTO);

    /**
     * Get all the dotGiaDichVuBhxhs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<DotGiaDichVuBhxhDTO> findAll(Pageable pageable);

    /**
     * Get the "id" dotGiaDichVuBhxh.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<DotGiaDichVuBhxhDTO> findOne(Long id);

    /**
     * Delete the "id" dotGiaDichVuBhxh.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}

package vn.vnpt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import vn.vnpt.domain.TPhanNhomCDHA;
import vn.vnpt.domain.*; // for static metamodels
import vn.vnpt.repository.TPhanNhomCDHARepository;
import vn.vnpt.service.dto.TPhanNhomCDHACriteria;
import vn.vnpt.service.dto.TPhanNhomCDHADTO;
import vn.vnpt.service.mapper.TPhanNhomCDHAMapper;

/**
 * Service for executing complex queries for {@link TPhanNhomCDHA} entities in the database.
 * The main input is a {@link TPhanNhomCDHACriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link TPhanNhomCDHADTO} or a {@link Page} of {@link TPhanNhomCDHADTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TPhanNhomCDHAQueryService extends QueryService<TPhanNhomCDHA> {

    private final Logger log = LoggerFactory.getLogger(TPhanNhomCDHAQueryService.class);

    private final TPhanNhomCDHARepository tPhanNhomCDHARepository;

    private final TPhanNhomCDHAMapper tPhanNhomCDHAMapper;

    public TPhanNhomCDHAQueryService(TPhanNhomCDHARepository tPhanNhomCDHARepository, TPhanNhomCDHAMapper tPhanNhomCDHAMapper) {
        this.tPhanNhomCDHARepository = tPhanNhomCDHARepository;
        this.tPhanNhomCDHAMapper = tPhanNhomCDHAMapper;
    }

    /**
     * Return a {@link List} of {@link TPhanNhomCDHADTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<TPhanNhomCDHADTO> findByCriteria(TPhanNhomCDHACriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<TPhanNhomCDHA> specification = createSpecification(criteria);
        return tPhanNhomCDHAMapper.toDto(tPhanNhomCDHARepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link TPhanNhomCDHADTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<TPhanNhomCDHADTO> findByCriteria(TPhanNhomCDHACriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<TPhanNhomCDHA> specification = createSpecification(criteria);
        return tPhanNhomCDHARepository.findAll(specification, page)
            .map(tPhanNhomCDHAMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TPhanNhomCDHACriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<TPhanNhomCDHA> specification = createSpecification(criteria);
        return tPhanNhomCDHARepository.count(specification);
    }

    /**
     * Function to convert {@link TPhanNhomCDHACriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<TPhanNhomCDHA> createSpecification(TPhanNhomCDHACriteria criteria) {
        Specification<TPhanNhomCDHA> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), TPhanNhomCDHA_.id));
            }
            if (criteria.getNhom_cdhaId() != null) {
                specification = specification.and(buildSpecification(criteria.getNhom_cdhaId(),
                    root -> root.join(TPhanNhomCDHA_.nhom_cdha, JoinType.LEFT).get(NhomChanDoanHinhAnh_.id)));
            }
            if (criteria.getCdhaId() != null) {
                specification = specification.and(buildSpecification(criteria.getCdhaId(),
                    root -> root.join(TPhanNhomCDHA_.cdha, JoinType.LEFT).get(ChanDoanHinhAnh_.id)));
            }
        }
        return specification;
    }
}

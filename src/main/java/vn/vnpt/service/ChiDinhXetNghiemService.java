package vn.vnpt.service;

import vn.vnpt.domain.ChiDinhXetNghiemId;
import vn.vnpt.service.dto.ChiDinhXetNghiemDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.ChiDinhXetNghiem}.
 */
public interface ChiDinhXetNghiemService {

    /**
     * Save a chiDinhXetNghiem.
     *
     * @param chiDinhXetNghiemDTO the entity to save.
     * @return the persisted entity.
     */
    ChiDinhXetNghiemDTO save(ChiDinhXetNghiemDTO chiDinhXetNghiemDTO);

    /**
     * Get all the chiDinhXetNghiems.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ChiDinhXetNghiemDTO> findAll(Pageable pageable);

    /**
     * Get the "id" chiDinhXetNghiem.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ChiDinhXetNghiemDTO> findOne(ChiDinhXetNghiemId id);

    /**
     * Delete the "id" chiDinhXetNghiem.
     *
     * @param id the id of the entity.
     */
    void delete(ChiDinhXetNghiemId id);
}

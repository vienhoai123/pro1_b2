package vn.vnpt.service;

import vn.vnpt.service.dto.HuongDieuTriDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.HuongDieuTri}.
 */
public interface HuongDieuTriService {

    /**
     * Save a huongDieuTri.
     *
     * @param huongDieuTriDTO the entity to save.
     * @return the persisted entity.
     */
    HuongDieuTriDTO save(HuongDieuTriDTO huongDieuTriDTO);

    /**
     * Get all the huongDieuTris.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<HuongDieuTriDTO> findAll(Pageable pageable);


    /**
     * Get the "id" huongDieuTri.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<HuongDieuTriDTO> findOne(Long id);

    /**
     * Delete the "id" huongDieuTri.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}

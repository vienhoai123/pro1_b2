package vn.vnpt.client;

import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.ClientsResource;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.RealmsResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.ClientRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import vn.vnpt.web.rest.UserExtraResource;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class KeycloakUtils {

    private final Logger log = LoggerFactory.getLogger(KeycloakUtils.class);

    private RealmResource realmResource;

    public RealmResource getRealmResource() {
        return realmResource;
    }

    public void setRealmResource(RealmResource realmResource) {
        this.realmResource = realmResource;
    }

    public UsersResource getUsersResource() {
        return usersResource;
    }

    public void setUsersResource(UsersResource usersResource) {
        this.usersResource = usersResource;
    }

    private UsersResource usersResource;

    public KeycloakUtils(
        @Value("${spring.security.oauth2.client.provider.oidc.issuer-uri}") String issuerUri,
        @Value("${spring.security.oauth2.client.provider.oidc.client-secret}") String clientSecret,
        @Value("${spring.security.oauth2.client.provider.oidc.client-id}") String clientId,
        @Value("${spring.security.oauth2.client.provider.oidc.username}") String username,
        @Value("${spring.security.oauth2.client.provider.oidc.password}") String password
    ) {
        String serverUrl = issuerUri.replaceAll("(?<=auth)(.+)$", "");
        Pattern pattern = Pattern.compile("(?<=realms/)(.+)$");
        Matcher matcher = pattern.matcher(issuerUri);
        String realm = matcher.find() ? matcher.group(0) : null;
//        Keycloak keycloak = KeycloakBuilder.builder()
//            .serverUrl(serverUrl)
//            .realm(realm)
//            .clientSecret(clientSecret)
//            .username(username)
//            .password(password)
//            .grantType(OAuth2Constants.CLIENT_CREDENTIALS)
//            .clientId(clientId)
//            .resteasyClient(new ResteasyClientBuilder().connectionPoolSize(10).build())
//            .build();
        Keycloak keycloak =  Keycloak.getInstance(
            serverUrl,
            realm,
            username,
            password,
            clientId,
            clientSecret
        );
        this.realmResource = keycloak.realm(realm);
        this.usersResource = realmResource.users();
    }

    public boolean createRole(String roleName) {
        RoleRepresentation role = new RoleRepresentation();
        role.setName(roleName);
        AtomicBoolean status = new AtomicBoolean(true);
        try {
            realmResource.roles().create(role);

        } catch (Exception e) {
            log.debug("REST request to delete role Keycloak : {}", e);
            status.set(false);
        }
        return status.get();
    }
    public boolean deleteRole(String roleName) {
        AtomicBoolean status = new AtomicBoolean(true);
        try {
            realmResource.roles().deleteRole(roleName);

        } catch (Exception e) {
            log.debug("REST request to delete role Keycloak : {}", e);
            status.set(false);
        }
        return status.get();
    }
}

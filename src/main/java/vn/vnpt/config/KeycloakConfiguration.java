package vn.vnpt.config;

import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.RealmResource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Configuration
public class KeycloakConfiguration {
    @Value("${spring.security.oauth2.client.provider.oidc.issuer-uri}")
    private String issuerUri;
    @Bean
    public RealmResource keycloak() {
        String serverUrl = issuerUri.replaceAll("(?<=auth)(.+)$", "");
        Pattern pattern = Pattern.compile("(?<=realms/)(.+)$");
        Matcher matcher = pattern.matcher(issuerUri);
        String realm = matcher.find() ? matcher.group(0) : null;
        String clientId = "admin-cli";
        String clientSecret = "962fc1f3-3319-4b26-ada5-474ea6de332c";
        String userName = "admin";
        String passWord = "admin";
        final Keycloak keycloak = Keycloak.getInstance(
            serverUrl,
            realm,
            userName,
            passWord,
            clientId
            );
        return keycloak.realm(realm);
    }
}

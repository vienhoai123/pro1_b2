package vn.vnpt.web.rest;

import vn.vnpt.service.ChanDoanHinhAnhApDungService;
import vn.vnpt.web.rest.errors.BadRequestAlertException;
import vn.vnpt.service.dto.ChanDoanHinhAnhApDungDTO;
import vn.vnpt.service.dto.ChanDoanHinhAnhApDungCriteria;
import vn.vnpt.service.ChanDoanHinhAnhApDungQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link vn.vnpt.domain.ChanDoanHinhAnhApDung}.
 */
@RestController
@RequestMapping("/api")
public class ChanDoanHinhAnhApDungResource {

    private final Logger log = LoggerFactory.getLogger(ChanDoanHinhAnhApDungResource.class);

    private static final String ENTITY_NAME = "khamchuabenhChanDoanHinhAnhApDung";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ChanDoanHinhAnhApDungService chanDoanHinhAnhApDungService;

    private final ChanDoanHinhAnhApDungQueryService chanDoanHinhAnhApDungQueryService;

    public ChanDoanHinhAnhApDungResource(ChanDoanHinhAnhApDungService chanDoanHinhAnhApDungService, ChanDoanHinhAnhApDungQueryService chanDoanHinhAnhApDungQueryService) {
        this.chanDoanHinhAnhApDungService = chanDoanHinhAnhApDungService;
        this.chanDoanHinhAnhApDungQueryService = chanDoanHinhAnhApDungQueryService;
    }

    /**
     * {@code POST  /chan-doan-hinh-anh-ap-dungs} : Create a new chanDoanHinhAnhApDung.
     *
     * @param chanDoanHinhAnhApDungDTO the chanDoanHinhAnhApDungDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new chanDoanHinhAnhApDungDTO, or with status {@code 400 (Bad Request)} if the chanDoanHinhAnhApDung has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/chan-doan-hinh-anh-ap-dungs")
    public ResponseEntity<ChanDoanHinhAnhApDungDTO> createChanDoanHinhAnhApDung(@Valid @RequestBody ChanDoanHinhAnhApDungDTO chanDoanHinhAnhApDungDTO) throws URISyntaxException {
        log.debug("REST request to save ChanDoanHinhAnhApDung : {}", chanDoanHinhAnhApDungDTO);
        if (chanDoanHinhAnhApDungDTO.getId() != null) {
            throw new BadRequestAlertException("A new chanDoanHinhAnhApDung cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ChanDoanHinhAnhApDungDTO result = chanDoanHinhAnhApDungService.save(chanDoanHinhAnhApDungDTO);
        return ResponseEntity.created(new URI("/api/chan-doan-hinh-anh-ap-dungs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /chan-doan-hinh-anh-ap-dungs} : Updates an existing chanDoanHinhAnhApDung.
     *
     * @param chanDoanHinhAnhApDungDTO the chanDoanHinhAnhApDungDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated chanDoanHinhAnhApDungDTO,
     * or with status {@code 400 (Bad Request)} if the chanDoanHinhAnhApDungDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the chanDoanHinhAnhApDungDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/chan-doan-hinh-anh-ap-dungs")
    public ResponseEntity<ChanDoanHinhAnhApDungDTO> updateChanDoanHinhAnhApDung(@Valid @RequestBody ChanDoanHinhAnhApDungDTO chanDoanHinhAnhApDungDTO) throws URISyntaxException {
        log.debug("REST request to update ChanDoanHinhAnhApDung : {}", chanDoanHinhAnhApDungDTO);
        if (chanDoanHinhAnhApDungDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ChanDoanHinhAnhApDungDTO result = chanDoanHinhAnhApDungService.save(chanDoanHinhAnhApDungDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, chanDoanHinhAnhApDungDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /chan-doan-hinh-anh-ap-dungs} : get all the chanDoanHinhAnhApDungs.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of chanDoanHinhAnhApDungs in body.
     */
    @GetMapping("/chan-doan-hinh-anh-ap-dungs")
    public ResponseEntity<List<ChanDoanHinhAnhApDungDTO>> getAllChanDoanHinhAnhApDungs(ChanDoanHinhAnhApDungCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ChanDoanHinhAnhApDungs by criteria: {}", criteria);
        Page<ChanDoanHinhAnhApDungDTO> page = chanDoanHinhAnhApDungQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /chan-doan-hinh-anh-ap-dungs/count} : count all the chanDoanHinhAnhApDungs.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/chan-doan-hinh-anh-ap-dungs/count")
    public ResponseEntity<Long> countChanDoanHinhAnhApDungs(ChanDoanHinhAnhApDungCriteria criteria) {
        log.debug("REST request to count ChanDoanHinhAnhApDungs by criteria: {}", criteria);
        return ResponseEntity.ok().body(chanDoanHinhAnhApDungQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /chan-doan-hinh-anh-ap-dungs/:id} : get the "id" chanDoanHinhAnhApDung.
     *
     * @param id the id of the chanDoanHinhAnhApDungDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the chanDoanHinhAnhApDungDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/chan-doan-hinh-anh-ap-dungs/{id}")
    public ResponseEntity<ChanDoanHinhAnhApDungDTO> getChanDoanHinhAnhApDung(@PathVariable Long id) {
        log.debug("REST request to get ChanDoanHinhAnhApDung : {}", id);
        Optional<ChanDoanHinhAnhApDungDTO> chanDoanHinhAnhApDungDTO = chanDoanHinhAnhApDungService.findOne(id);
        return ResponseUtil.wrapOrNotFound(chanDoanHinhAnhApDungDTO);
    }

    /**
     * {@code DELETE  /chan-doan-hinh-anh-ap-dungs/:id} : delete the "id" chanDoanHinhAnhApDung.
     *
     * @param id the id of the chanDoanHinhAnhApDungDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/chan-doan-hinh-anh-ap-dungs/{id}")
    public ResponseEntity<Void> deleteChanDoanHinhAnhApDung(@PathVariable Long id) {
        log.debug("REST request to delete ChanDoanHinhAnhApDung : {}", id);
        chanDoanHinhAnhApDungService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

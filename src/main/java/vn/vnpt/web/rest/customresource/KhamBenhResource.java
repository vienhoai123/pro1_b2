package vn.vnpt.web.rest.customresource;

import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.web.util.HeaderUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.vnpt.service.BenhNhanService;
import vn.vnpt.service.TheBhytService;
import vn.vnpt.service.ThongTinKhamBenhService;
import vn.vnpt.service.ThongTinKhoaQueryService;
import vn.vnpt.service.customservice.KhamBenhService;
import vn.vnpt.service.dto.*;
import vn.vnpt.service.dto.customdto.KhamBenhDTO;
import vn.vnpt.service.mapper.custommapper.TiepNhanMapper;
import vn.vnpt.web.rest.errors.BadRequestAlertException;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping("/api")
public class KhamBenhResource {
    @Autowired
    private ThongTinKhamBenhService thongTinKhamBenhService;
    @Autowired
    private KhamBenhService khamBenhService;
    @Autowired
    private TheBhytService theBhytService;

    private TiepNhanMapper tiepNhanMapper;
    @Autowired
    private BenhNhanService benhNhanService;
    @Autowired
    private ThongTinKhoaQueryService thongTinKhoaQueryService;

    public KhamBenhResource(TiepNhanMapper tiepNhanMapper) {
        this.tiepNhanMapper = tiepNhanMapper;
    }

    @PostMapping("/kham-benhs")
    public ResponseEntity<KhamBenhDTO> createKhamBenh(@Valid @RequestBody KhamBenhDTO khamBenhDTO) throws URISyntaxException {
        if (khamBenhDTO.getTiepNhanDTO().getCoBaoHiem()&&khamBenhDTO.getTiepNhanDTO().getTheTreEm()&&khamBenhDTO.getThongTinKhamBenhDTO().getCanNang()==null)
            throw new BadRequestAlertException("Nếu BHYT là Trẻ Em thì phải nhập cân nặng", "ThongTinKhamBenhDTO","Thiếu thông tin");
        ThongTinKhoaDTO thongTinKhoaDTO = khamBenhDTO.getThongTinKhoaDTO();
        if (thongTinKhoaDTO.getDotDieuTriId() == null || thongTinKhoaDTO.getBakbId() == null || thongTinKhoaDTO.getBenhNhanId() == null || thongTinKhoaDTO.getDonViId() == null)
            throw new BadRequestAlertException("Thiếu thông tin khoa", "ThongTinKhoaDTO","Thiếu thông tin");
        Long dotDieutriId = khamBenhDTO.getTiepNhanDTO().getDotDieuTriId();
        if (thongTinKhoaDTO.getDotDieuTriId().intValue() != dotDieutriId  || thongTinKhoaDTO.getBakbId().intValue() != dotDieutriId || thongTinKhoaDTO.getBenhNhanId().intValue() != dotDieutriId || thongTinKhoaDTO.getDonViId().intValue() != dotDieutriId)
            throw new BadRequestAlertException("Thông tin khoa không đồng bộ với thông tin tiếp nhận", "ThongTinKhoaDTO","Thông tin sai");
        LongFilter filter = new LongFilter();
        filter.setEquals(dotDieutriId);
        ThongTinKhoaCriteria criteria = new ThongTinKhoaCriteria();
        criteria.setDotDieuTriId(filter);
        if (thongTinKhoaQueryService.countByCriteria(criteria) != 0)
            throw new BadRequestAlertException("Thông tin khoa đã tồn tại", "ThongTinKhoaDTO","Thông tin tồn tại");
        khamBenhDTO.getTiepNhanDTO().setTrangThai(2);
        System.out.println(khamBenhDTO.getTiepNhanDTO());
        KhamBenhDTO result = khamBenhService.save(khamBenhDTO).get();
        return ResponseEntity.created(new URI("/api/kham-benhs/"+khamBenhDTO.getTiepNhanDTO().getBakbId())).body(result);
    }

    @PutMapping("/kham-benhs")
    public ResponseEntity<KhamBenhDTO> updateKhamBenh(@Valid @RequestBody KhamBenhDTO khamBenhDTO) throws URISyntaxException {
//        if(khamBenhDTO.getTiepNhanDTO().getBakbId() == null || khamBenhDTO.getTiepNhanDTO().getDotDieuTriId() == null || khamBenhDTO.getTiepNhanDTO().getBenhNhanId() == null || khamBenhDTO.getThongTinKhamBenhDTO().getId()==null || khamBenhDTO.getThongTinKhoaDTO().getId()==null)
//            throw new BadRequestAlertException("Thiếu thông tin tiếp nhận, khoa hoặc tt khám bệnh", "KhamBenhDTO","Thiếu thông tin");
//        Long benhnhanId= khamBenhDTO.getTiepNhanDTO().getBenhNhanId();
//        TheBhytDTO theBhytDTO=tiepNhanMapper.toTheBhytDto(khamBenhDTO.getTiepNhanDTO());
//        BenhNhanDTO benhNhanDTO=tiepNhanMapper.toBenhNhanDto(khamBenhDTO.getTiepNhanDTO());
//        ThongTinKhamBenhDTO thongTinKhamBenhDTO= thongTinKhamBenhService.findOne(khamBenhDTO.getThongTinKhamBenhDTO().getId()).get();
//        if(thongTinKhamBenhDTO.getTrangThai() >= 3)
//            throw new BadRequestAlertException("Chỉ được phép thay đổi thông tin trong lúc BN chờ khám và đang khám", "KhamBenhDTO","Không cho phép cập nhật");
//        if(khamBenhDTO.getTiepNhanDTO().getCoBaoHiem()){
//            BenhNhanDTO thongTinBnLienQuanBhytTrongDB = benhNhanService.getThongTinLienQuanBhyt(benhNhanDTO);
//            BenhNhanDTO thongTinLienQuanBhytFeGuiXuong = benhNhanService.getThongTinLienQuanBhyt(benhNhanService.findOne(benhnhanId).get());
//            if (!(theBhytService.findOne(theBhytDTO.getId()).get().equals(theBhytDTO)) || (!(thongTinBnLienQuanBhytTrongDB.equals(thongTinLienQuanBhytFeGuiXuong))))
//                throw new BadRequestAlertException("Không dược thay đổi thông tin liên quan BHYT", "KhamBenhDTO","Không cho phép cập nhật");
//        }
//        KhamBenhDTO result = khamBenhService.save(khamBenhDTO).get();
//        return ResponseEntity.ok().body(result);
    return ResponseEntity.noContent().build();
    }
    @GetMapping("/kham-benhs/{bakbId}")
    public ResponseEntity<KhamBenhDTO> getKhamBenh(@PathVariable Long bakbId){
        //KhamBenhDTO result = khamBenhService.findOne(bakbId).get();
        return ResponseEntity.ok().body(null);
    }

    @GetMapping("/kham-benhs")
    public ResponseEntity<List<KhamBenhDTO>> getKhamBenhByCondition(BenhAnKhamBenhCriteria criteria, Pageable pageable){
        List<KhamBenhDTO> result = khamBenhService.findByCriteria(criteria,pageable).getContent();
        return ResponseEntity.ok().body(result);
    }

//    @PostMapping("kham-benhs/get-init")
//    public ResponseEntity<KhamBenhDTO> getInitKhamBenhDTO(@RequestBody ChiDinhDichVuKhamDTO chiDinhDichVuKhamDTO){
//        KhamBenhDTO result = khamBenhService.getInitKhamBenhDTO(chiDinhDichVuKhamDTO).get();
//        return ResponseEntity.ok().body(result);
//    }

//
//    @DeleteMapping("/kham-benhs/{bakbId}")
//    public ResponseEntity<Void> deleteBenhAnKhamBenh(@PathVariable Long bakbId) {
//        ThongTinKhamBenhDTO thongTinKhamBenhDTO = thongTinKhamBenhService.findOne(bakbId).get();
//        if(thongTinKhamBenhDTO.getTrangThai()!=1)
//        {
//            throw new BadRequestAlertException("Chỉ được xóa bệnh nhân đang chờ khám", "KhamBenhDTO","Sai thông tin");
//        }
//        khamBenhService.delete(bakbId);
//        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert("BSGD", true, "TiepNhan", bakbId.toString())).build();
//    }

}

package vn.vnpt.web.rest.customresource;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.vnpt.constant.Constant;
import vn.vnpt.domain.BenhAnKhamBenhId;
import vn.vnpt.domain.ChiDinhDichVuKham;
import vn.vnpt.service.BenhAnKhamBenhQueryService;
import vn.vnpt.service.BenhAnKhamBenhService;
import vn.vnpt.service.ChiDinhDichVuKhamService;
import vn.vnpt.service.DichVuKhamService;
import vn.vnpt.service.customservice.TiepNhanService;
import vn.vnpt.service.dto.BenhAnKhamBenhCriteria;
import vn.vnpt.service.dto.BenhAnKhamBenhDTO;
import vn.vnpt.service.dto.customdto.*;
import vn.vnpt.service.mapper.BenhNhanMapper;
import vn.vnpt.service.mapper.DonViMapper;
import vn.vnpt.service.mapper.custommapper.TiepNhanMapper;
import vn.vnpt.web.rest.errors.BadRequestAlertException;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class TiepNhanResource {
    @Autowired
    private TiepNhanService tiepNhanService;
    @Autowired
    private BenhAnKhamBenhService benhAnKhamBenhService;
    @Autowired
    private DichVuKhamService dichVuKhamService;
    @PostMapping("/tiep-nhans")
    public ResponseEntity<TiepNhanDTO> createTiepNhan(@Valid @RequestBody TiepNhanDTO tiepNhanDTO) throws URISyntaxException {
        if (tiepNhanDTO.getBakbId() != null)
            throw new BadRequestAlertException("Không cần gửi id", "BenhAnKhamBenh", "ID tự sinh");
        if (tiepNhanDTO.getDotDieuTriId() != null)
            throw new BadRequestAlertException("Không cần gửi id", "DotDieuTri", "ID tự sinh");
        try {
            TiepNhanDTO result = tiepNhanService.save(tiepNhanDTO);
            return ResponseEntity.created(new URI("api/tiep-nhans/"+result.getBakbId())).body(result);
        }
        catch (NullPointerException ne){
            System.out.println("Null trong createTiepNhan" + ne);
            return null;
        }
    }

    @PutMapping("/tiep-nhans")
    public ResponseEntity<TiepNhanDTO> updateTiepNhan(@Valid @RequestBody TiepNhanDTO tiepNhanDTO){
        if (tiepNhanDTO.getBakbId() == null || tiepNhanDTO.getDotDieuTriId() == null || tiepNhanDTO.getBenhNhanId() == null)
            throw new BadRequestAlertException("Thiếu thông tin tiếp nhận (bakbId, dotDieuTriId, benhNhanId)", "TiepNhanDTO","Thiếu thông tin");
        BenhAnKhamBenhId benhAnKhamBenhId = new BenhAnKhamBenhId(tiepNhanDTO.getBakbId(),tiepNhanDTO.getBenhNhanId(),tiepNhanDTO.getDonViId());
        BenhAnKhamBenhDTO benhAnKhamBenhDTO =  benhAnKhamBenhService.findOne(benhAnKhamBenhId).get();
//        BenhAnKhamBenhDTO benhAnKhamBenhDTO = new BenhAnKhamBenhDTO();
        if (benhAnKhamBenhDTO.getTrangThai() >= Constant.getTrangThai("DA_KHAM"))
            throw new BadRequestAlertException("Chỉ được phép chỉnh sửa khi chưa hoàn tất khám", "TiepNhanDTO","Không cho phép chỉnh sửa");
        TiepNhanDTO result = tiepNhanService.save(tiepNhanDTO);
        return ResponseEntity.ok().body(result);
    }

    @GetMapping("/tiep-nhans")
    public ResponseEntity<List<TiepNhanDTO>> getTiepNhanByCondition(BenhAnKhamBenhCriteria criteria, Pageable pageable){
        Page<TiepNhanDTO> result = tiepNhanService.findByCriteria(criteria,pageable);
        return ResponseEntity.ok().body(result.getContent());
    }

    @GetMapping("/tiep-nhans/{id}")
    public ResponseEntity<TiepNhanDTO> getTiepNhan(@PathVariable Long id){
        Optional<TiepNhanDTO> result = tiepNhanService.findOne(id);
        return ResponseEntity.ok().body(result.get());
    }

    @GetMapping("tiep-nhans/count")
    public ResponseEntity<List<SoLuongBenhNhanDTO>> getSoLuongBenhNhan(BenhAnKhamBenhCriteria criteria){
        List<SoLuongBenhNhanDTO> soLuongBenhNhanDTOList = tiepNhanService.countingBN(criteria);
        return ResponseEntity.ok().body(soLuongBenhNhanDTOList);
    }

    @GetMapping("tiep-nhans/danh-sachs")
    public ResponseEntity<List<DanhSachTiepNhanDTO>> getDanhSachTiepNhan(BenhAnKhamBenhCriteria criteria, Pageable pageable){
        List<DanhSachTiepNhanDTO> result = tiepNhanService.getDanhSachTiepNhan(criteria,pageable);
        return ResponseEntity.ok().body(result);
    }

    @DeleteMapping("/tiep-nhans/{bakbId}")
    public ResponseEntity<Void> deleteBenhAnKhamBenh(@PathVariable Long bakbId) {
        tiepNhanService.delete(bakbId);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert("BSGD", true, "TiepNhan", bakbId.toString())).build();
    }

    @GetMapping("/tiep-nhans/danh-sach-dich-vus")
    public ResponseEntity<List<ThongTinDichVuKhamDTO>> getDanhSachDichVuKham(){
        return ResponseEntity.ok(dichVuKhamService.getDanhSachDichVuKham());
    }



    /**
     * Customize theo Tiếp nhận ngoại trú
     */
    @PostMapping("/tiep-nhan-ngoai-trus")
    public ResponseEntity<TiepNhanResponseDTO> tiepNhanNgoaiTru(@RequestBody TiepNhanRequestDTO tiepNhanRequestDTO) throws URISyntaxException {
        TiepNhanDTO tiepNhanDTO = tiepNhanRequestDTO.getTiepNhanDTO();
        if (tiepNhanDTO.getBakbId() != null)
            throw new BadRequestAlertException("Không cần gửi id", "BenhAnKhamBenh", "ID tự sinh");
        if (tiepNhanDTO.getDotDieuTriId() != null)
            throw new BadRequestAlertException("Không cần gửi id", "DotDieuTri", "ID tự sinh");
        if (tiepNhanDTO.getDonViId() == null)
            throw new BadRequestAlertException("Thiếu thông tin đơn vị", "DonVi", "Thiếu thông tin");
        if (tiepNhanRequestDTO.getDichVuKhamId() == null)
            throw new BadRequestAlertException("Thiếu thông tin dịch vụ khám", "DichVuKham", "Thiếu thông tin");
        TiepNhanResponseDTO result = tiepNhanService.tiepNhanNgoaiTru(tiepNhanRequestDTO);
        return ResponseEntity.created(new URI("api/tiep-nhan-ngoai-trus/bakbId="+result.getTiepNhanDTO().getBakbId()+";benhNhanId="+result.getTiepNhanDTO().getBenhNhanId()+";donViId="+result.getTiepNhanDTO().getDonViId())).body(result);
    }

    @PutMapping("/tiep-nhan-ngoai-trus")
    public ResponseEntity<TiepNhanResponseDTO> updateTiepNhanNgoaiTru(@RequestBody TiepNhanRequestDTO tiepNhanRequestDTO){
        TiepNhanDTO tiepNhanDTO = tiepNhanRequestDTO.getTiepNhanDTO();
        if (tiepNhanDTO.getBakbId() == null)
            throw new BadRequestAlertException("Thiếu thông tin bệnh án", "BenhAnKhamBenh", "Thiếu thông tin");
        if (tiepNhanDTO.getBenhNhanId() == null)
            throw new BadRequestAlertException("Thiếu thông tin bệnh nhân ", "BenhNhan", "Thiếu thông tin");
        if (tiepNhanDTO.getDonViId() == null)
            throw new BadRequestAlertException("Thiếu thông tin đơn vị ", "BenhNhan", "Thiếu thông tin");
        BenhAnKhamBenhId benhAnKhamBenhIdRecieve = new BenhAnKhamBenhId(tiepNhanDTO.getBakbId(),tiepNhanDTO.getBenhNhanId(),tiepNhanDTO.getDonViId());
        BenhAnKhamBenhDTO benhAnKhamBenhDTO = benhAnKhamBenhService.findOne(benhAnKhamBenhIdRecieve).get();
        if (benhAnKhamBenhDTO == null)
            throw new BadRequestAlertException("Sai thông tin bệnh án", "BenhAnKhamBenh", "Không tìm thấy");
        if(benhAnKhamBenhDTO.getTrangThai() > Constant.getTrangThai("DA_KHAM"))
            throw new BadRequestAlertException("Không thể cập nhật thông tin sau khi đã khám", "BenhAnKhamBenh", "Không được phép");
        TiepNhanResponseDTO result = tiepNhanService.capNhatTiepNhanNgoaiTru(tiepNhanRequestDTO);
        return ResponseEntity.ok().body(result);
    }

    @GetMapping("/tiep-nhan-ngoai-trus/{id}")
    public ResponseEntity<TiepNhanResponseDTO> getTiepNhanNgoaiTru(@MatrixVariable(pathVar = "id") Map<String,String> idMap){
        ObjectMapper mapper = new ObjectMapper();
        BenhAnKhamBenhId benhAnKhamBenhId = mapper.convertValue(idMap,BenhAnKhamBenhId.class);
       return ResponseUtil.wrapOrNotFound(tiepNhanService.getOneTiepNhanNgoaiTru(benhAnKhamBenhId));
    }

    @GetMapping("/tiep-nhan-ngoai-trus")
    public ResponseEntity<List<TiepNhanResponseDTO>> getDanhSachBenhNhanTiepNhanNgoaiTru(BenhAnKhamBenhCriteria criteria, Pageable pageable){
        List<TiepNhanResponseDTO> redult = tiepNhanService.getDanhSachTiepNhanNgoaiTru(criteria, pageable);
        return ResponseEntity.ok().body(redult);
    }

    @DeleteMapping("/tiep-nhan-ngoai-trus/{id}")
    public ResponseEntity<Void> deleteTiepNhanNgoaiTru(@MatrixVariable(pathVar = "id") Map<String, String> idMap){
        ObjectMapper mapper = new ObjectMapper();
        BenhAnKhamBenhId benhAnKhamBenhId = mapper.convertValue(idMap,BenhAnKhamBenhId.class);
        BenhAnKhamBenhDTO benhAnKhamBenhDTO = benhAnKhamBenhService.findOne(benhAnKhamBenhId).get();
        if (benhAnKhamBenhDTO == null)
            throw new BadRequestAlertException("Bệnh án không tồn tại", "BenhAnKhamBenh", "Không tìm thấy");
        if (benhAnKhamBenhDTO.getTrangThai() >= Constant.getTrangThai("DA_KHAM"))
            throw new BadRequestAlertException("Không thể xóa thông tin sau khi đã khám", "BenhAnKhamBenh", "Không được phép");
        tiepNhanService.deleteNgoaiTru(benhAnKhamBenhId);
        return ResponseEntity.noContent().build();
    }


}

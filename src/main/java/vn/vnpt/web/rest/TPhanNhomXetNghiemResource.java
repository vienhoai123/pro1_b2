package vn.vnpt.web.rest;

import vn.vnpt.service.TPhanNhomXetNghiemService;
import vn.vnpt.web.rest.errors.BadRequestAlertException;
import vn.vnpt.service.dto.TPhanNhomXetNghiemDTO;
import vn.vnpt.service.dto.TPhanNhomXetNghiemCriteria;
import vn.vnpt.service.TPhanNhomXetNghiemQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link vn.vnpt.domain.TPhanNhomXetNghiem}.
 */
@RestController
@RequestMapping("/api")
public class TPhanNhomXetNghiemResource {

    private final Logger log = LoggerFactory.getLogger(TPhanNhomXetNghiemResource.class);

    private static final String ENTITY_NAME = "khamchuabenhTPhanNhomXetNghiem";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TPhanNhomXetNghiemService tPhanNhomXetNghiemService;

    private final TPhanNhomXetNghiemQueryService tPhanNhomXetNghiemQueryService;

    public TPhanNhomXetNghiemResource(TPhanNhomXetNghiemService tPhanNhomXetNghiemService, TPhanNhomXetNghiemQueryService tPhanNhomXetNghiemQueryService) {
        this.tPhanNhomXetNghiemService = tPhanNhomXetNghiemService;
        this.tPhanNhomXetNghiemQueryService = tPhanNhomXetNghiemQueryService;
    }

    /**
     * {@code POST  /t-phan-nhom-xet-nghiems} : Create a new tPhanNhomXetNghiem.
     *
     * @param tPhanNhomXetNghiemDTO the tPhanNhomXetNghiemDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new tPhanNhomXetNghiemDTO, or with status {@code 400 (Bad Request)} if the tPhanNhomXetNghiem has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/t-phan-nhom-xet-nghiems")
    public ResponseEntity<TPhanNhomXetNghiemDTO> createTPhanNhomXetNghiem(@Valid @RequestBody TPhanNhomXetNghiemDTO tPhanNhomXetNghiemDTO) throws URISyntaxException {
        log.debug("REST request to save TPhanNhomXetNghiem : {}", tPhanNhomXetNghiemDTO);
        if (tPhanNhomXetNghiemDTO.getId() != null) {
            throw new BadRequestAlertException("A new tPhanNhomXetNghiem cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TPhanNhomXetNghiemDTO result = tPhanNhomXetNghiemService.save(tPhanNhomXetNghiemDTO);
        return ResponseEntity.created(new URI("/api/t-phan-nhom-xet-nghiems/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /t-phan-nhom-xet-nghiems} : Updates an existing tPhanNhomXetNghiem.
     *
     * @param tPhanNhomXetNghiemDTO the tPhanNhomXetNghiemDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated tPhanNhomXetNghiemDTO,
     * or with status {@code 400 (Bad Request)} if the tPhanNhomXetNghiemDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the tPhanNhomXetNghiemDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/t-phan-nhom-xet-nghiems")
    public ResponseEntity<TPhanNhomXetNghiemDTO> updateTPhanNhomXetNghiem(@Valid @RequestBody TPhanNhomXetNghiemDTO tPhanNhomXetNghiemDTO) throws URISyntaxException {
        log.debug("REST request to update TPhanNhomXetNghiem : {}", tPhanNhomXetNghiemDTO);
        if (tPhanNhomXetNghiemDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TPhanNhomXetNghiemDTO result = tPhanNhomXetNghiemService.save(tPhanNhomXetNghiemDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, tPhanNhomXetNghiemDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /t-phan-nhom-xet-nghiems} : get all the tPhanNhomXetNghiems.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of tPhanNhomXetNghiems in body.
     */
    @GetMapping("/t-phan-nhom-xet-nghiems")
    public ResponseEntity<List<TPhanNhomXetNghiemDTO>> getAllTPhanNhomXetNghiems(TPhanNhomXetNghiemCriteria criteria, Pageable pageable) {
        log.debug("REST request to get TPhanNhomXetNghiems by criteria: {}", criteria);
        Page<TPhanNhomXetNghiemDTO> page = tPhanNhomXetNghiemQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /t-phan-nhom-xet-nghiems/count} : count all the tPhanNhomXetNghiems.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/t-phan-nhom-xet-nghiems/count")
    public ResponseEntity<Long> countTPhanNhomXetNghiems(TPhanNhomXetNghiemCriteria criteria) {
        log.debug("REST request to count TPhanNhomXetNghiems by criteria: {}", criteria);
        return ResponseEntity.ok().body(tPhanNhomXetNghiemQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /t-phan-nhom-xet-nghiems/:id} : get the "id" tPhanNhomXetNghiem.
     *
     * @param id the id of the tPhanNhomXetNghiemDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the tPhanNhomXetNghiemDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/t-phan-nhom-xet-nghiems/{id}")
    public ResponseEntity<TPhanNhomXetNghiemDTO> getTPhanNhomXetNghiem(@PathVariable Long id) {
        log.debug("REST request to get TPhanNhomXetNghiem : {}", id);
        Optional<TPhanNhomXetNghiemDTO> tPhanNhomXetNghiemDTO = tPhanNhomXetNghiemService.findOne(id);
        return ResponseUtil.wrapOrNotFound(tPhanNhomXetNghiemDTO);
    }

    /**
     * {@code DELETE  /t-phan-nhom-xet-nghiems/:id} : delete the "id" tPhanNhomXetNghiem.
     *
     * @param id the id of the tPhanNhomXetNghiemDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/t-phan-nhom-xet-nghiems/{id}")
    public ResponseEntity<Void> deleteTPhanNhomXetNghiem(@PathVariable Long id) {
        log.debug("REST request to delete TPhanNhomXetNghiem : {}", id);
        tPhanNhomXetNghiemService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

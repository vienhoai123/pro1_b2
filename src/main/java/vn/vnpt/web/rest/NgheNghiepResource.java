package vn.vnpt.web.rest;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.vnpt.config.KafkaProperties;
import vn.vnpt.service.NgheNghiepQueryService;
import vn.vnpt.service.NgheNghiepService;
import vn.vnpt.service.dto.NgheNghiepCriteria;
import vn.vnpt.service.dto.NgheNghiepDTO;
import vn.vnpt.web.rest.errors.BadRequestAlertException;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link vn.vnpt.domain.NgheNghiep}.
 */
@RestController
@RequestMapping("/api")
public class NgheNghiepResource {

    private final Logger log = LoggerFactory.getLogger(NgheNghiepResource.class);
    private static final String ENTITY_NAME = "khamchuabenhNgheNghiep";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final NgheNghiepService ngheNghiepService;

    private final NgheNghiepQueryService ngheNghiepQueryService;

    public NgheNghiepResource(NgheNghiepService ngheNghiepService, NgheNghiepQueryService ngheNghiepQueryService,KafkaProperties kafkaProperties) {
        this.ngheNghiepService = ngheNghiepService;
        this.ngheNghiepQueryService = ngheNghiepQueryService;
    }

    /**
     * {@code POST  /nghe-nghieps} : Create a new ngheNghiep.
     *
     * @param ngheNghiepDTO the ngheNghiepDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new ngheNghiepDTO, or with status {@code 400 (Bad Request)} if the ngheNghiep has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/nghe-nghieps")
    public ResponseEntity<NgheNghiepDTO> createNgheNghiep(@Valid @RequestBody NgheNghiepDTO ngheNghiepDTO) throws URISyntaxException {
        log.debug("REST request to save NgheNghiep : {}", ngheNghiepDTO);
        if (ngheNghiepDTO.getId() != null) {
            throw new BadRequestAlertException("A new ngheNghiep cannot already have an ID", ENTITY_NAME, "idexists");
        }
        NgheNghiepDTO result = ngheNghiepService.save(ngheNghiepDTO);
        return ResponseEntity.created(new URI("/api/nghe-nghieps/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /nghe-nghieps} : Updates an existing ngheNghiep.
     *
     * @param ngheNghiepDTO the ngheNghiepDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated ngheNghiepDTO,
     * or with status {@code 400 (Bad Request)} if the ngheNghiepDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the ngheNghiepDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/nghe-nghieps")
    public ResponseEntity<NgheNghiepDTO> updateNgheNghiep(@Valid @RequestBody NgheNghiepDTO ngheNghiepDTO) throws URISyntaxException {
        log.debug("REST request to update NgheNghiep : {}", ngheNghiepDTO);
        if (ngheNghiepDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        NgheNghiepDTO result = ngheNghiepService.save(ngheNghiepDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, ngheNghiepDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /nghe-nghieps} : get all the ngheNghieps.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of ngheNghieps in body.
     */
    @GetMapping("/nghe-nghieps")
    public ResponseEntity<List<NgheNghiepDTO>> getAllNgheNghieps(NgheNghiepCriteria criteria) {
        log.debug("REST request to get NgheNghieps by criteria: {}", criteria);
        List<NgheNghiepDTO> entityList = ngheNghiepQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
     * {@code GET  /nghe-nghieps/count} : count all the ngheNghieps.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/nghe-nghieps/count")
    public ResponseEntity<Long> countNgheNghieps(NgheNghiepCriteria criteria) {
        log.debug("REST request to count NgheNghieps by criteria: {}", criteria);
        return ResponseEntity.ok().body(ngheNghiepQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /nghe-nghieps/:id} : get the "id" ngheNghiep.
     *
     * @param id the id of the ngheNghiepDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the ngheNghiepDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/nghe-nghieps/{id}")
    public ResponseEntity<NgheNghiepDTO> getNgheNghiep(@PathVariable Long id) {
        log.debug("REST request to get NgheNghiep : {}", id);
        Optional<NgheNghiepDTO> ngheNghiepDTO = ngheNghiepService.findOne(id);
        return ResponseUtil.wrapOrNotFound(ngheNghiepDTO);
    }

    /**
     * {@code DELETE  /nghe-nghieps/:id} : delete the "id" ngheNghiep.
     *
     * @param id the id of the ngheNghiepDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/nghe-nghieps/{id}")
    public ResponseEntity<Void> deleteNgheNghiep(@PathVariable Long id) {
        log.debug("REST request to delete NgheNghiep : {}", id);
        ngheNghiepService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

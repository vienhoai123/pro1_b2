package vn.vnpt.web.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import vn.vnpt.domain.ChiDinhCDHAId;
import vn.vnpt.domain.ChiDinhXetNghiemId;
import vn.vnpt.service.ChiDinhCDHAService;
import vn.vnpt.web.rest.errors.BadRequestAlertException;
import vn.vnpt.service.dto.ChiDinhCDHADTO;
import vn.vnpt.service.dto.ChiDinhCDHACriteria;
import vn.vnpt.service.ChiDinhCDHAQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * REST controller for managing {@link vn.vnpt.domain.ChiDinhCDHA}.
 */
@RestController
@RequestMapping("/api")
public class ChiDinhCDHAResource {

    private final Logger log = LoggerFactory.getLogger(ChiDinhCDHAResource.class);

    private static final String ENTITY_NAME = "khamchuabenhChiDinhCdha";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ChiDinhCDHAService chiDinhCDHAService;

    private final ChiDinhCDHAQueryService chiDinhCDHAQueryService;

    public ChiDinhCDHAResource(ChiDinhCDHAService chiDinhCDHAService, ChiDinhCDHAQueryService chiDinhCDHAQueryService) {
        this.chiDinhCDHAService = chiDinhCDHAService;
        this.chiDinhCDHAQueryService = chiDinhCDHAQueryService;
    }

    /**
     * {@code POST  /chi-dinh-cdhas} : Create a new chiDinhCDHA.
     *
     * @param chiDinhCDHADTO the chiDinhCDHADTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new chiDinhCDHADTO, or with status {@code 400 (Bad Request)} if the chiDinhCDHA has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/chi-dinh-cdhas")
    public ResponseEntity<ChiDinhCDHADTO> createChiDinhCDHA(@Valid @RequestBody ChiDinhCDHADTO chiDinhCDHADTO) throws URISyntaxException {
        log.debug("REST request to save ChiDinhCDHA : {}", chiDinhCDHADTO);
        if (chiDinhCDHADTO.getId() != null) {
            throw new BadRequestAlertException("A new chiDinhCDHA cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ChiDinhCDHADTO result = chiDinhCDHAService.save(chiDinhCDHADTO);
        return ResponseEntity.created(new URI("/api/chi-dinh-cdhas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /chi-dinh-cdhas} : Updates an existing chiDinhCDHA.
     *
     * @param chiDinhCDHADTO the chiDinhCDHADTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated chiDinhCDHADTO,
     * or with status {@code 400 (Bad Request)} if the chiDinhCDHADTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the chiDinhCDHADTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/chi-dinh-cdhas")
    public ResponseEntity<ChiDinhCDHADTO> updateChiDinhCDHA(@Valid @RequestBody ChiDinhCDHADTO chiDinhCDHADTO) throws URISyntaxException {
        log.debug("REST request to update ChiDinhCDHA : {}", chiDinhCDHADTO);
        if (chiDinhCDHADTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ChiDinhCDHADTO result = chiDinhCDHAService.save(chiDinhCDHADTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, chiDinhCDHADTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /chi-dinh-cdhas} : get all the chiDinhCDHAS.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of chiDinhCDHAS in body.
     */
    @GetMapping("/chi-dinh-cdhas")
    public ResponseEntity<List<ChiDinhCDHADTO>> getAllChiDinhCDHAS(ChiDinhCDHACriteria criteria, Pageable pageable) {
        log.debug("REST request to get ChiDinhCDHAS by criteria: {}", criteria);
        Page<ChiDinhCDHADTO> page = chiDinhCDHAQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /chi-dinh-cdhas/count} : count all the chiDinhCDHAS.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/chi-dinh-cdhas/count")
    public ResponseEntity<Long> countChiDinhCDHAS(ChiDinhCDHACriteria criteria) {
        log.debug("REST request to count ChiDinhCDHAS by criteria: {}", criteria);
        return ResponseEntity.ok().body(chiDinhCDHAQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /chi-dinh-cdhas/:id} : get the "id" chiDinhCDHA.
     *
//     * @param id the id of the chiDinhCDHADTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the chiDinhCDHADTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/chi-dinh-cdhas/{id}")
    public ResponseEntity<ChiDinhCDHADTO> getChiDinhCDHA(@MatrixVariable(pathVar = "id") Map<String, String> idMap) {
        final ObjectMapper mapper = new ObjectMapper();
        final ChiDinhCDHAId id = mapper.convertValue(idMap, ChiDinhCDHAId.class);
        log.debug("REST request to get ChiDinhCDHA : {}", id);
        Optional<ChiDinhCDHADTO> chiDinhCDHADTO = chiDinhCDHAService.findOne(id);
        return ResponseUtil.wrapOrNotFound(chiDinhCDHADTO);
    }

    /**
     * {@code DELETE  /chi-dinh-cdhas/:id} : delete the "id" chiDinhCDHA.
     *
//     * @param id the id of the chiDinhCDHADTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/chi-dinh-cdhas/{id}")
    public ResponseEntity<Void> deleteChiDinhCDHA(@MatrixVariable(pathVar = "id") Map<String, String> idMap) {
        final ObjectMapper mapper = new ObjectMapper();
        final ChiDinhCDHAId id = mapper.convertValue(idMap, ChiDinhCDHAId.class);
        log.debug("REST request to delete ChiDinhCDHA : {}", id);
        chiDinhCDHAService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

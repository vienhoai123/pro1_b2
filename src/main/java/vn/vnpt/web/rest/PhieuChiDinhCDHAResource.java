package vn.vnpt.web.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import vn.vnpt.domain.PhieuChiDinhCDHAId;
import vn.vnpt.domain.PhieuChiDinhXetNghiemId;
import vn.vnpt.service.PhieuChiDinhCDHAService;
import vn.vnpt.web.rest.errors.BadRequestAlertException;
import vn.vnpt.service.dto.PhieuChiDinhCDHADTO;
import vn.vnpt.service.dto.PhieuChiDinhCDHACriteria;
import vn.vnpt.service.PhieuChiDinhCDHAQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * REST controller for managing {@link vn.vnpt.domain.PhieuChiDinhCDHA}.
 */
@RestController
@RequestMapping("/api")
public class PhieuChiDinhCDHAResource {

    private final Logger log = LoggerFactory.getLogger(PhieuChiDinhCDHAResource.class);

    private static final String ENTITY_NAME = "khamchuabenhPhieuChiDinhCdha";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PhieuChiDinhCDHAService phieuChiDinhCDHAService;

    private final PhieuChiDinhCDHAQueryService phieuChiDinhCDHAQueryService;

    public PhieuChiDinhCDHAResource(PhieuChiDinhCDHAService phieuChiDinhCDHAService, PhieuChiDinhCDHAQueryService phieuChiDinhCDHAQueryService) {
        this.phieuChiDinhCDHAService = phieuChiDinhCDHAService;
        this.phieuChiDinhCDHAQueryService = phieuChiDinhCDHAQueryService;
    }

    /**
     * {@code POST  /phieu-chi-dinh-cdhas} : Create a new phieuChiDinhCDHA.
     *
     * @param phieuChiDinhCDHADTO the phieuChiDinhCDHADTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new phieuChiDinhCDHADTO, or with status {@code 400 (Bad Request)} if the phieuChiDinhCDHA has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/phieu-chi-dinh-cdhas")
    public ResponseEntity<PhieuChiDinhCDHADTO> createPhieuChiDinhCDHA(@Valid @RequestBody PhieuChiDinhCDHADTO phieuChiDinhCDHADTO) throws URISyntaxException {
        log.debug("REST request to save PhieuChiDinhCDHA : {}", phieuChiDinhCDHADTO);
        if (phieuChiDinhCDHADTO.getId() != null) {
            throw new BadRequestAlertException("A new phieuChiDinhCDHA cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PhieuChiDinhCDHADTO result = phieuChiDinhCDHAService.save(phieuChiDinhCDHADTO);
        return ResponseEntity.created(new URI("/api/phieu-chi-dinh-cdhas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /phieu-chi-dinh-cdhas} : Updates an existing phieuChiDinhCDHA.
     *
     * @param phieuChiDinhCDHADTO the phieuChiDinhCDHADTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated phieuChiDinhCDHADTO,
     * or with status {@code 400 (Bad Request)} if the phieuChiDinhCDHADTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the phieuChiDinhCDHADTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/phieu-chi-dinh-cdhas")
    public ResponseEntity<PhieuChiDinhCDHADTO> updatePhieuChiDinhCDHA(@Valid @RequestBody PhieuChiDinhCDHADTO phieuChiDinhCDHADTO) throws URISyntaxException {
        log.debug("REST request to update PhieuChiDinhCDHA : {}", phieuChiDinhCDHADTO);
        if (phieuChiDinhCDHADTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PhieuChiDinhCDHADTO result = phieuChiDinhCDHAService.save(phieuChiDinhCDHADTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, phieuChiDinhCDHADTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /phieu-chi-dinh-cdhas} : get all the phieuChiDinhCDHAS.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of phieuChiDinhCDHAS in body.
     */
    @GetMapping("/phieu-chi-dinh-cdhas")
    public ResponseEntity<List<PhieuChiDinhCDHADTO>> getAllPhieuChiDinhCDHAS(PhieuChiDinhCDHACriteria criteria, Pageable pageable) {
        log.debug("REST request to get PhieuChiDinhCDHAS by criteria: {}", criteria);
        Page<PhieuChiDinhCDHADTO> page = phieuChiDinhCDHAQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /phieu-chi-dinh-cdhas/count} : count all the phieuChiDinhCDHAS.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/phieu-chi-dinh-cdhas/count")
    public ResponseEntity<Long> countPhieuChiDinhCDHAS(PhieuChiDinhCDHACriteria criteria) {
        log.debug("REST request to count PhieuChiDinhCDHAS by criteria: {}", criteria);
        return ResponseEntity.ok().body(phieuChiDinhCDHAQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /phieu-chi-dinh-cdhas/:id} : get the "id" phieuChiDinhCDHA.
     *
     * @param id the id of the phieuChiDinhCDHADTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the phieuChiDinhCDHADTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/phieu-chi-dinh-cdhas/{id}")
    public ResponseEntity<PhieuChiDinhCDHADTO> getPhieuChiDinhCDHA(@MatrixVariable(pathVar = "id") Map<String, String> idMap) {
        final ObjectMapper mapper = new ObjectMapper();
        final PhieuChiDinhCDHAId id = mapper.convertValue(idMap, PhieuChiDinhCDHAId.class);
        log.debug("REST request to get PhieuChiDinhCDHA : {}", id);
        Optional<PhieuChiDinhCDHADTO> phieuChiDinhCDHADTO = phieuChiDinhCDHAService.findOne(id);
        return ResponseUtil.wrapOrNotFound(phieuChiDinhCDHADTO);
    }

    /**
     * {@code DELETE  /phieu-chi-dinh-cdhas/:id} : delete the "id" phieuChiDinhCDHA.
     *
//     * @param id the id of the phieuChiDinhCDHADTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/phieu-chi-dinh-cdhas/{id}")
    public ResponseEntity<Void> deletePhieuChiDinhCDHA(@MatrixVariable(pathVar = "id") Map<String, String> idMap) {
        final ObjectMapper mapper = new ObjectMapper();
        final PhieuChiDinhCDHAId id = mapper.convertValue(idMap, PhieuChiDinhCDHAId.class);
        log.debug("REST request to delete PhieuChiDinhCDHA : {}", id);
        phieuChiDinhCDHAService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

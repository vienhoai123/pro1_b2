package vn.vnpt.web.rest;

import vn.vnpt.service.ChiDinhDichVuKhamService;
import vn.vnpt.web.rest.errors.BadRequestAlertException;
import vn.vnpt.service.dto.ChiDinhDichVuKhamDTO;
import vn.vnpt.service.dto.ChiDinhDichVuKhamCriteria;
import vn.vnpt.service.ChiDinhDichVuKhamQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link vn.vnpt.domain.ChiDinhDichVuKham}.
 */
@RestController
@RequestMapping("/api")
public class ChiDinhDichVuKhamResource {

    private final Logger log = LoggerFactory.getLogger(ChiDinhDichVuKhamResource.class);

    private static final String ENTITY_NAME = "khamchuabenhChiDinhDichVuKham";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ChiDinhDichVuKhamService chiDinhDichVuKhamService;

    private final ChiDinhDichVuKhamQueryService chiDinhDichVuKhamQueryService;

    public ChiDinhDichVuKhamResource(ChiDinhDichVuKhamService chiDinhDichVuKhamService, ChiDinhDichVuKhamQueryService chiDinhDichVuKhamQueryService) {
        this.chiDinhDichVuKhamService = chiDinhDichVuKhamService;
        this.chiDinhDichVuKhamQueryService = chiDinhDichVuKhamQueryService;
    }

    /**
     * {@code POST  /chi-dinh-dich-vu-khams} : Create a new chiDinhDichVuKham.
     *
     * @param chiDinhDichVuKhamDTO the chiDinhDichVuKhamDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new chiDinhDichVuKhamDTO, or with status {@code 400 (Bad Request)} if the chiDinhDichVuKham has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/chi-dinh-dich-vu-khams")
    public ResponseEntity<ChiDinhDichVuKhamDTO> createChiDinhDichVuKham(@Valid @RequestBody ChiDinhDichVuKhamDTO chiDinhDichVuKhamDTO) throws URISyntaxException {
        log.debug("REST request to save ChiDinhDichVuKham : {}", chiDinhDichVuKhamDTO);
        if (chiDinhDichVuKhamDTO.getId() != null) {
            throw new BadRequestAlertException("A new chiDinhDichVuKham cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ChiDinhDichVuKhamDTO result = chiDinhDichVuKhamService.save(chiDinhDichVuKhamDTO);
        return ResponseEntity.created(new URI("/api/chi-dinh-dich-vu-khams/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /chi-dinh-dich-vu-khams} : Updates an existing chiDinhDichVuKham.
     *
     * @param chiDinhDichVuKhamDTO the chiDinhDichVuKhamDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated chiDinhDichVuKhamDTO,
     * or with status {@code 400 (Bad Request)} if the chiDinhDichVuKhamDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the chiDinhDichVuKhamDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/chi-dinh-dich-vu-khams")
    public ResponseEntity<ChiDinhDichVuKhamDTO> updateChiDinhDichVuKham(@Valid @RequestBody ChiDinhDichVuKhamDTO chiDinhDichVuKhamDTO) throws URISyntaxException {
        log.debug("REST request to update ChiDinhDichVuKham : {}", chiDinhDichVuKhamDTO);
        if (chiDinhDichVuKhamDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ChiDinhDichVuKhamDTO result = chiDinhDichVuKhamService.save(chiDinhDichVuKhamDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, chiDinhDichVuKhamDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /chi-dinh-dich-vu-khams} : get all the chiDinhDichVuKhams.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of chiDinhDichVuKhams in body.
     */
    @GetMapping("/chi-dinh-dich-vu-khams")
    public ResponseEntity<List<ChiDinhDichVuKhamDTO>> getAllChiDinhDichVuKhams(ChiDinhDichVuKhamCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ChiDinhDichVuKhams by criteria: {}", criteria);
        Page<ChiDinhDichVuKhamDTO> page = chiDinhDichVuKhamQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /chi-dinh-dich-vu-khams/count} : count all the chiDinhDichVuKhams.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/chi-dinh-dich-vu-khams/count")
    public ResponseEntity<Long> countChiDinhDichVuKhams(ChiDinhDichVuKhamCriteria criteria) {
        log.debug("REST request to count ChiDinhDichVuKhams by criteria: {}", criteria);
        return ResponseEntity.ok().body(chiDinhDichVuKhamQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /chi-dinh-dich-vu-khams/:id} : get the "id" chiDinhDichVuKham.
     *
     * @param id the id of the chiDinhDichVuKhamDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the chiDinhDichVuKhamDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/chi-dinh-dich-vu-khams/{id}")
    public ResponseEntity<ChiDinhDichVuKhamDTO> getChiDinhDichVuKham(@PathVariable Long id) {
        log.debug("REST request to get ChiDinhDichVuKham : {}", id);
        Optional<ChiDinhDichVuKhamDTO> chiDinhDichVuKhamDTO = chiDinhDichVuKhamService.findOne(id);
        return ResponseUtil.wrapOrNotFound(chiDinhDichVuKhamDTO);
    }

    /**
     * {@code DELETE  /chi-dinh-dich-vu-khams/:id} : delete the "id" chiDinhDichVuKham.
     *
     * @param id the id of the chiDinhDichVuKhamDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/chi-dinh-dich-vu-khams/{id}")
    public ResponseEntity<Void> deleteChiDinhDichVuKham(@PathVariable Long id) {
        log.debug("REST request to delete ChiDinhDichVuKham : {}", id);
        chiDinhDichVuKhamService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

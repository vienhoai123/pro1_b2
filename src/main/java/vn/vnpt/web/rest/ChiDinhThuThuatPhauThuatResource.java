package vn.vnpt.web.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import vn.vnpt.domain.ChiDinhTTPTId;
import vn.vnpt.domain.ChiDinhXetNghiemId;
import vn.vnpt.service.ChiDinhThuThuatPhauThuatService;
import vn.vnpt.web.rest.errors.BadRequestAlertException;
import vn.vnpt.service.dto.ChiDinhThuThuatPhauThuatDTO;
import vn.vnpt.service.dto.ChiDinhThuThuatPhauThuatCriteria;
import vn.vnpt.service.ChiDinhThuThuatPhauThuatQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * REST controller for managing {@link vn.vnpt.domain.ChiDinhThuThuatPhauThuat}.
 */
@RestController
@RequestMapping("/api")
public class ChiDinhThuThuatPhauThuatResource {

    private final Logger log = LoggerFactory.getLogger(ChiDinhThuThuatPhauThuatResource.class);

    private static final String ENTITY_NAME = "khamchuabenhChiDinhThuThuatPhauThuat";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ChiDinhThuThuatPhauThuatService chiDinhThuThuatPhauThuatService;

    private final ChiDinhThuThuatPhauThuatQueryService chiDinhThuThuatPhauThuatQueryService;

    public ChiDinhThuThuatPhauThuatResource(ChiDinhThuThuatPhauThuatService chiDinhThuThuatPhauThuatService, ChiDinhThuThuatPhauThuatQueryService chiDinhThuThuatPhauThuatQueryService) {
        this.chiDinhThuThuatPhauThuatService = chiDinhThuThuatPhauThuatService;
        this.chiDinhThuThuatPhauThuatQueryService = chiDinhThuThuatPhauThuatQueryService;
    }

    /**
     * {@code POST  /chi-dinh-thu-thuat-phau-thuats} : Create a new chiDinhThuThuatPhauThuat.
     *
     * @param chiDinhThuThuatPhauThuatDTO the chiDinhThuThuatPhauThuatDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new chiDinhThuThuatPhauThuatDTO, or with status {@code 400 (Bad Request)} if the chiDinhThuThuatPhauThuat has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/chi-dinh-thu-thuat-phau-thuats")
    public ResponseEntity<ChiDinhThuThuatPhauThuatDTO> createChiDinhThuThuatPhauThuat(@Valid @RequestBody ChiDinhThuThuatPhauThuatDTO chiDinhThuThuatPhauThuatDTO) throws URISyntaxException {
        log.debug("REST request to save ChiDinhThuThuatPhauThuat : {}", chiDinhThuThuatPhauThuatDTO);
        if (chiDinhThuThuatPhauThuatDTO.getId() != null) {
            throw new BadRequestAlertException("A new chiDinhThuThuatPhauThuat cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ChiDinhThuThuatPhauThuatDTO result = chiDinhThuThuatPhauThuatService.save(chiDinhThuThuatPhauThuatDTO);
        return ResponseEntity.created(new URI("/api/chi-dinh-thu-thuat-phau-thuats/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /chi-dinh-thu-thuat-phau-thuats} : Updates an existing chiDinhThuThuatPhauThuat.
     *
     * @param chiDinhThuThuatPhauThuatDTO the chiDinhThuThuatPhauThuatDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated chiDinhThuThuatPhauThuatDTO,
     * or with status {@code 400 (Bad Request)} if the chiDinhThuThuatPhauThuatDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the chiDinhThuThuatPhauThuatDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/chi-dinh-thu-thuat-phau-thuats")
    public ResponseEntity<ChiDinhThuThuatPhauThuatDTO> updateChiDinhThuThuatPhauThuat(@Valid @RequestBody ChiDinhThuThuatPhauThuatDTO chiDinhThuThuatPhauThuatDTO) throws URISyntaxException {
        log.debug("REST request to update ChiDinhThuThuatPhauThuat : {}", chiDinhThuThuatPhauThuatDTO);
        if (chiDinhThuThuatPhauThuatDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ChiDinhThuThuatPhauThuatDTO result = chiDinhThuThuatPhauThuatService.save(chiDinhThuThuatPhauThuatDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, chiDinhThuThuatPhauThuatDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /chi-dinh-thu-thuat-phau-thuats} : get all the chiDinhThuThuatPhauThuats.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of chiDinhThuThuatPhauThuats in body.
     */
    @GetMapping("/chi-dinh-thu-thuat-phau-thuats")
    public ResponseEntity<List<ChiDinhThuThuatPhauThuatDTO>> getAllChiDinhThuThuatPhauThuats(ChiDinhThuThuatPhauThuatCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ChiDinhThuThuatPhauThuats by criteria: {}", criteria);
        Page<ChiDinhThuThuatPhauThuatDTO> page = chiDinhThuThuatPhauThuatQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /chi-dinh-thu-thuat-phau-thuats/count} : count all the chiDinhThuThuatPhauThuats.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/chi-dinh-thu-thuat-phau-thuats/count")
    public ResponseEntity<Long> countChiDinhThuThuatPhauThuats(ChiDinhThuThuatPhauThuatCriteria criteria) {
        log.debug("REST request to count ChiDinhThuThuatPhauThuats by criteria: {}", criteria);
        return ResponseEntity.ok().body(chiDinhThuThuatPhauThuatQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /chi-dinh-thu-thuat-phau-thuats/:id} : get the "id" chiDinhThuThuatPhauThuat.
     *
//     * @param id the id of the chiDinhThuThuatPhauThuatDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the chiDinhThuThuatPhauThuatDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/chi-dinh-thu-thuat-phau-thuats/{id}")
    public ResponseEntity<ChiDinhThuThuatPhauThuatDTO> getChiDinhThuThuatPhauThuat(@MatrixVariable(pathVar = "id") Map<String, String> idMap) {
        final ObjectMapper mapper = new ObjectMapper();
        final ChiDinhTTPTId id = mapper.convertValue(idMap, ChiDinhTTPTId.class);
        log.debug("REST request to get ChiDinhThuThuatPhauThuat : {}", id);
        Optional<ChiDinhThuThuatPhauThuatDTO> chiDinhThuThuatPhauThuatDTO = chiDinhThuThuatPhauThuatService.findOne(id);
        return ResponseUtil.wrapOrNotFound(chiDinhThuThuatPhauThuatDTO);
    }

    /**
     * {@code DELETE  /chi-dinh-thu-thuat-phau-thuats/:id} : delete the "id" chiDinhThuThuatPhauThuat.
     *
//     * @param id the id of the chiDinhThuThuatPhauThuatDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/chi-dinh-thu-thuat-phau-thuats/{id}")
    public ResponseEntity<Void> deleteChiDinhThuThuatPhauThuat(@MatrixVariable(pathVar = "id") Map<String, String> idMap) {
        final ObjectMapper mapper = new ObjectMapper();
        final ChiDinhTTPTId id = mapper.convertValue(idMap, ChiDinhTTPTId.class);
        log.debug("REST request to delete ChiDinhThuThuatPhauThuat : {}", id);
        chiDinhThuThuatPhauThuatService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

package vn.vnpt.web.rest;

import vn.vnpt.service.ThuThuatPhauThuatApDungService;
import vn.vnpt.web.rest.errors.BadRequestAlertException;
import vn.vnpt.service.dto.ThuThuatPhauThuatApDungDTO;
import vn.vnpt.service.dto.ThuThuatPhauThuatApDungCriteria;
import vn.vnpt.service.ThuThuatPhauThuatApDungQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link vn.vnpt.domain.ThuThuatPhauThuatApDung}.
 */
@RestController
@RequestMapping("/api")
public class ThuThuatPhauThuatApDungResource {

    private final Logger log = LoggerFactory.getLogger(ThuThuatPhauThuatApDungResource.class);

    private static final String ENTITY_NAME = "khamchuabenhThuThuatPhauThuatApDung";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ThuThuatPhauThuatApDungService thuThuatPhauThuatApDungService;

    private final ThuThuatPhauThuatApDungQueryService thuThuatPhauThuatApDungQueryService;

    public ThuThuatPhauThuatApDungResource(ThuThuatPhauThuatApDungService thuThuatPhauThuatApDungService, ThuThuatPhauThuatApDungQueryService thuThuatPhauThuatApDungQueryService) {
        this.thuThuatPhauThuatApDungService = thuThuatPhauThuatApDungService;
        this.thuThuatPhauThuatApDungQueryService = thuThuatPhauThuatApDungQueryService;
    }

    /**
     * {@code POST  /thu-thuat-phau-thuat-ap-dungs} : Create a new thuThuatPhauThuatApDung.
     *
     * @param thuThuatPhauThuatApDungDTO the thuThuatPhauThuatApDungDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new thuThuatPhauThuatApDungDTO, or with status {@code 400 (Bad Request)} if the thuThuatPhauThuatApDung has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/thu-thuat-phau-thuat-ap-dungs")
    public ResponseEntity<ThuThuatPhauThuatApDungDTO> createThuThuatPhauThuatApDung(@Valid @RequestBody ThuThuatPhauThuatApDungDTO thuThuatPhauThuatApDungDTO) throws URISyntaxException {
        log.debug("REST request to save ThuThuatPhauThuatApDung : {}", thuThuatPhauThuatApDungDTO);
        if (thuThuatPhauThuatApDungDTO.getId() != null) {
            throw new BadRequestAlertException("A new thuThuatPhauThuatApDung cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ThuThuatPhauThuatApDungDTO result = thuThuatPhauThuatApDungService.save(thuThuatPhauThuatApDungDTO);
        return ResponseEntity.created(new URI("/api/thu-thuat-phau-thuat-ap-dungs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /thu-thuat-phau-thuat-ap-dungs} : Updates an existing thuThuatPhauThuatApDung.
     *
     * @param thuThuatPhauThuatApDungDTO the thuThuatPhauThuatApDungDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated thuThuatPhauThuatApDungDTO,
     * or with status {@code 400 (Bad Request)} if the thuThuatPhauThuatApDungDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the thuThuatPhauThuatApDungDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/thu-thuat-phau-thuat-ap-dungs")
    public ResponseEntity<ThuThuatPhauThuatApDungDTO> updateThuThuatPhauThuatApDung(@Valid @RequestBody ThuThuatPhauThuatApDungDTO thuThuatPhauThuatApDungDTO) throws URISyntaxException {
        log.debug("REST request to update ThuThuatPhauThuatApDung : {}", thuThuatPhauThuatApDungDTO);
        if (thuThuatPhauThuatApDungDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ThuThuatPhauThuatApDungDTO result = thuThuatPhauThuatApDungService.save(thuThuatPhauThuatApDungDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, thuThuatPhauThuatApDungDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /thu-thuat-phau-thuat-ap-dungs} : get all the thuThuatPhauThuatApDungs.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of thuThuatPhauThuatApDungs in body.
     */
    @GetMapping("/thu-thuat-phau-thuat-ap-dungs")
    public ResponseEntity<List<ThuThuatPhauThuatApDungDTO>> getAllThuThuatPhauThuatApDungs(ThuThuatPhauThuatApDungCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ThuThuatPhauThuatApDungs by criteria: {}", criteria);
        Page<ThuThuatPhauThuatApDungDTO> page = thuThuatPhauThuatApDungQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /thu-thuat-phau-thuat-ap-dungs/count} : count all the thuThuatPhauThuatApDungs.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/thu-thuat-phau-thuat-ap-dungs/count")
    public ResponseEntity<Long> countThuThuatPhauThuatApDungs(ThuThuatPhauThuatApDungCriteria criteria) {
        log.debug("REST request to count ThuThuatPhauThuatApDungs by criteria: {}", criteria);
        return ResponseEntity.ok().body(thuThuatPhauThuatApDungQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /thu-thuat-phau-thuat-ap-dungs/:id} : get the "id" thuThuatPhauThuatApDung.
     *
     * @param id the id of the thuThuatPhauThuatApDungDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the thuThuatPhauThuatApDungDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/thu-thuat-phau-thuat-ap-dungs/{id}")
    public ResponseEntity<ThuThuatPhauThuatApDungDTO> getThuThuatPhauThuatApDung(@PathVariable Long id) {
        log.debug("REST request to get ThuThuatPhauThuatApDung : {}", id);
        Optional<ThuThuatPhauThuatApDungDTO> thuThuatPhauThuatApDungDTO = thuThuatPhauThuatApDungService.findOne(id);
        return ResponseUtil.wrapOrNotFound(thuThuatPhauThuatApDungDTO);
    }

    /**
     * {@code DELETE  /thu-thuat-phau-thuat-ap-dungs/:id} : delete the "id" thuThuatPhauThuatApDung.
     *
     * @param id the id of the thuThuatPhauThuatApDungDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/thu-thuat-phau-thuat-ap-dungs/{id}")
    public ResponseEntity<Void> deleteThuThuatPhauThuatApDung(@PathVariable Long id) {
        log.debug("REST request to delete ThuThuatPhauThuatApDung : {}", id);
        thuThuatPhauThuatApDungService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

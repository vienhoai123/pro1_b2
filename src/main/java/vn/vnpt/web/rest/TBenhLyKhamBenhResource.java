package vn.vnpt.web.rest;

import vn.vnpt.service.TBenhLyKhamBenhService;
import vn.vnpt.web.rest.errors.BadRequestAlertException;
import vn.vnpt.service.dto.TBenhLyKhamBenhDTO;
import vn.vnpt.service.dto.TBenhLyKhamBenhCriteria;
import vn.vnpt.service.TBenhLyKhamBenhQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link vn.vnpt.domain.TBenhLyKhamBenh}.
 */
@RestController
@RequestMapping("/api")
public class TBenhLyKhamBenhResource {

    private final Logger log = LoggerFactory.getLogger(TBenhLyKhamBenhResource.class);

    private static final String ENTITY_NAME = "khamchuabenhTBenhLyKhamBenh";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TBenhLyKhamBenhService tBenhLyKhamBenhService;

    private final TBenhLyKhamBenhQueryService tBenhLyKhamBenhQueryService;

    public TBenhLyKhamBenhResource(TBenhLyKhamBenhService tBenhLyKhamBenhService, TBenhLyKhamBenhQueryService tBenhLyKhamBenhQueryService) {
        this.tBenhLyKhamBenhService = tBenhLyKhamBenhService;
        this.tBenhLyKhamBenhQueryService = tBenhLyKhamBenhQueryService;
    }

    /**
     * {@code POST  /t-benh-ly-kham-benhs} : Create a new tBenhLyKhamBenh.
     *
     * @param tBenhLyKhamBenhDTO the tBenhLyKhamBenhDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new tBenhLyKhamBenhDTO, or with status {@code 400 (Bad Request)} if the tBenhLyKhamBenh has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/t-benh-ly-kham-benhs")
    public ResponseEntity<TBenhLyKhamBenhDTO> createTBenhLyKhamBenh(@Valid @RequestBody TBenhLyKhamBenhDTO tBenhLyKhamBenhDTO) throws URISyntaxException {
        log.debug("REST request to save TBenhLyKhamBenh : {}", tBenhLyKhamBenhDTO);
        if (tBenhLyKhamBenhDTO.getId() != null) {
            throw new BadRequestAlertException("A new tBenhLyKhamBenh cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TBenhLyKhamBenhDTO result = tBenhLyKhamBenhService.save(tBenhLyKhamBenhDTO);
        return ResponseEntity.created(new URI("/api/t-benh-ly-kham-benhs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /t-benh-ly-kham-benhs} : Updates an existing tBenhLyKhamBenh.
     *
     * @param tBenhLyKhamBenhDTO the tBenhLyKhamBenhDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated tBenhLyKhamBenhDTO,
     * or with status {@code 400 (Bad Request)} if the tBenhLyKhamBenhDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the tBenhLyKhamBenhDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/t-benh-ly-kham-benhs")
    public ResponseEntity<TBenhLyKhamBenhDTO> updateTBenhLyKhamBenh(@Valid @RequestBody TBenhLyKhamBenhDTO tBenhLyKhamBenhDTO) throws URISyntaxException {
        log.debug("REST request to update TBenhLyKhamBenh : {}", tBenhLyKhamBenhDTO);
        if (tBenhLyKhamBenhDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TBenhLyKhamBenhDTO result = tBenhLyKhamBenhService.save(tBenhLyKhamBenhDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, tBenhLyKhamBenhDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /t-benh-ly-kham-benhs} : get all the tBenhLyKhamBenhs.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of tBenhLyKhamBenhs in body.
     */
    @GetMapping("/t-benh-ly-kham-benhs")
    public ResponseEntity<List<TBenhLyKhamBenhDTO>> getAllTBenhLyKhamBenhs(TBenhLyKhamBenhCriteria criteria, Pageable pageable) {
        log.debug("REST request to get TBenhLyKhamBenhs by criteria: {}", criteria);
        Page<TBenhLyKhamBenhDTO> page = tBenhLyKhamBenhQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /t-benh-ly-kham-benhs/count} : count all the tBenhLyKhamBenhs.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/t-benh-ly-kham-benhs/count")
    public ResponseEntity<Long> countTBenhLyKhamBenhs(TBenhLyKhamBenhCriteria criteria) {
        log.debug("REST request to count TBenhLyKhamBenhs by criteria: {}", criteria);
        return ResponseEntity.ok().body(tBenhLyKhamBenhQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /t-benh-ly-kham-benhs/:id} : get the "id" tBenhLyKhamBenh.
     *
     * @param id the id of the tBenhLyKhamBenhDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the tBenhLyKhamBenhDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/t-benh-ly-kham-benhs/{id}")
    public ResponseEntity<TBenhLyKhamBenhDTO> getTBenhLyKhamBenh(@PathVariable Long id) {
        log.debug("REST request to get TBenhLyKhamBenh : {}", id);
        Optional<TBenhLyKhamBenhDTO> tBenhLyKhamBenhDTO = tBenhLyKhamBenhService.findOne(id);
        return ResponseUtil.wrapOrNotFound(tBenhLyKhamBenhDTO);
    }

    /**
     * {@code DELETE  /t-benh-ly-kham-benhs/:id} : delete the "id" tBenhLyKhamBenh.
     *
     * @param id the id of the tBenhLyKhamBenhDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/t-benh-ly-kham-benhs/{id}")
    public ResponseEntity<Void> deleteTBenhLyKhamBenh(@PathVariable Long id) {
        log.debug("REST request to delete TBenhLyKhamBenh : {}", id);
        tBenhLyKhamBenhService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

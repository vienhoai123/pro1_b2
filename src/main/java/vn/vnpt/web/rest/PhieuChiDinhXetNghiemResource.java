package vn.vnpt.web.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import vn.vnpt.domain.ChiDinhXetNghiemId;
import vn.vnpt.domain.PhieuChiDinhXetNghiemId;
import vn.vnpt.service.PhieuChiDinhXetNghiemService;
import vn.vnpt.web.rest.errors.BadRequestAlertException;
import vn.vnpt.service.dto.PhieuChiDinhXetNghiemDTO;
import vn.vnpt.service.dto.PhieuChiDinhXetNghiemCriteria;
import vn.vnpt.service.PhieuChiDinhXetNghiemQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * REST controller for managing {@link vn.vnpt.domain.PhieuChiDinhXetNghiem}.
 */
@RestController
@RequestMapping("/api")
public class PhieuChiDinhXetNghiemResource {

    private final Logger log = LoggerFactory.getLogger(PhieuChiDinhXetNghiemResource.class);

    private static final String ENTITY_NAME = "khamchuabenhPhieuChiDinhXetNghiem";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PhieuChiDinhXetNghiemService phieuChiDinhXetNghiemService;

    private final PhieuChiDinhXetNghiemQueryService phieuChiDinhXetNghiemQueryService;

    public PhieuChiDinhXetNghiemResource(PhieuChiDinhXetNghiemService phieuChiDinhXetNghiemService, PhieuChiDinhXetNghiemQueryService phieuChiDinhXetNghiemQueryService) {
        this.phieuChiDinhXetNghiemService = phieuChiDinhXetNghiemService;
        this.phieuChiDinhXetNghiemQueryService = phieuChiDinhXetNghiemQueryService;
    }

    /**
     * {@code POST  /phieu-chi-dinh-xet-nghiems} : Create a new phieuChiDinhXetNghiem.
     *
     * @param phieuChiDinhXetNghiemDTO the phieuChiDinhXetNghiemDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new phieuChiDinhXetNghiemDTO, or with status {@code 400 (Bad Request)} if the phieuChiDinhXetNghiem has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/phieu-chi-dinh-xet-nghiems")
    public ResponseEntity<PhieuChiDinhXetNghiemDTO> createPhieuChiDinhXetNghiem(@Valid @RequestBody PhieuChiDinhXetNghiemDTO phieuChiDinhXetNghiemDTO) throws URISyntaxException {
        log.debug("REST request to save PhieuChiDinhXetNghiem : {}", phieuChiDinhXetNghiemDTO);
        if (phieuChiDinhXetNghiemDTO.getId() != null) {
            throw new BadRequestAlertException("A new phieuChiDinhXetNghiem cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PhieuChiDinhXetNghiemDTO result = phieuChiDinhXetNghiemService.save(phieuChiDinhXetNghiemDTO);
        return ResponseEntity.created(new URI("/api/phieu-chi-dinh-xet-nghiems/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /phieu-chi-dinh-xet-nghiems} : Updates an existing phieuChiDinhXetNghiem.
     *
     * @param phieuChiDinhXetNghiemDTO the phieuChiDinhXetNghiemDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated phieuChiDinhXetNghiemDTO,
     * or with status {@code 400 (Bad Request)} if the phieuChiDinhXetNghiemDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the phieuChiDinhXetNghiemDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/phieu-chi-dinh-xet-nghiems")
    public ResponseEntity<PhieuChiDinhXetNghiemDTO> updatePhieuChiDinhXetNghiem(@Valid @RequestBody PhieuChiDinhXetNghiemDTO phieuChiDinhXetNghiemDTO) throws URISyntaxException {
        log.debug("REST request to update PhieuChiDinhXetNghiem : {}", phieuChiDinhXetNghiemDTO);
        if (phieuChiDinhXetNghiemDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PhieuChiDinhXetNghiemDTO result = phieuChiDinhXetNghiemService.save(phieuChiDinhXetNghiemDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, phieuChiDinhXetNghiemDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /phieu-chi-dinh-xet-nghiems} : get all the phieuChiDinhXetNghiems.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of phieuChiDinhXetNghiems in body.
     */
    @GetMapping("/phieu-chi-dinh-xet-nghiems")
    public ResponseEntity<List<PhieuChiDinhXetNghiemDTO>> getAllPhieuChiDinhXetNghiems(PhieuChiDinhXetNghiemCriteria criteria, Pageable pageable) {
        log.debug("REST request to get PhieuChiDinhXetNghiems by criteria: {}", criteria);
        Page<PhieuChiDinhXetNghiemDTO> page = phieuChiDinhXetNghiemQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /phieu-chi-dinh-xet-nghiems/count} : count all the phieuChiDinhXetNghiems.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/phieu-chi-dinh-xet-nghiems/count")
    public ResponseEntity<Long> countPhieuChiDinhXetNghiems(PhieuChiDinhXetNghiemCriteria criteria) {
        log.debug("REST request to count PhieuChiDinhXetNghiems by criteria: {}", criteria);
        return ResponseEntity.ok().body(phieuChiDinhXetNghiemQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /phieu-chi-dinh-xet-nghiems/:id} : get the "id" phieuChiDinhXetNghiem.
     *
//     * @param id the id of the phieuChiDinhXetNghiemDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the phieuChiDinhXetNghiemDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/phieu-chi-dinh-xet-nghiems/{id}")
    public ResponseEntity<PhieuChiDinhXetNghiemDTO> getPhieuChiDinhXetNghiem(@MatrixVariable(pathVar = "id") Map<String, String> idMap) {
        final ObjectMapper mapper = new ObjectMapper();
        final PhieuChiDinhXetNghiemId id = mapper.convertValue(idMap, PhieuChiDinhXetNghiemId.class);
        log.debug("REST request to get PhieuChiDinhXetNghiem : {}", id);
        Optional<PhieuChiDinhXetNghiemDTO> phieuChiDinhXetNghiemDTO = phieuChiDinhXetNghiemService.findOne(id);
        return ResponseUtil.wrapOrNotFound(phieuChiDinhXetNghiemDTO);
    }

    /**
     * {@code DELETE  /phieu-chi-dinh-xet-nghiems/:id} : delete the "id" phieuChiDinhXetNghiem.
     *
//     * @param id the id of the phieuChiDinhXetNghiemDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/phieu-chi-dinh-xet-nghiems/{id}")
    public ResponseEntity<Void> deletePhieuChiDinhXetNghiem(@MatrixVariable(pathVar = "id") Map<String, String> idMap) {
        final ObjectMapper mapper = new ObjectMapper();
        final PhieuChiDinhXetNghiemId id = mapper.convertValue(idMap, PhieuChiDinhXetNghiemId.class);
        log.debug("REST request to delete PhieuChiDinhXetNghiem : {}", id);
        phieuChiDinhXetNghiemService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

package vn.vnpt.web.rest;

import vn.vnpt.service.ChanDoanHinhAnhService;
import vn.vnpt.web.rest.errors.BadRequestAlertException;
import vn.vnpt.service.dto.ChanDoanHinhAnhDTO;
import vn.vnpt.service.dto.ChanDoanHinhAnhCriteria;
import vn.vnpt.service.ChanDoanHinhAnhQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link vn.vnpt.domain.ChanDoanHinhAnh}.
 */
@RestController
@RequestMapping("/api")
public class ChanDoanHinhAnhResource {

    private final Logger log = LoggerFactory.getLogger(ChanDoanHinhAnhResource.class);

    private static final String ENTITY_NAME = "khamchuabenhChanDoanHinhAnh";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ChanDoanHinhAnhService chanDoanHinhAnhService;

    private final ChanDoanHinhAnhQueryService chanDoanHinhAnhQueryService;

    public ChanDoanHinhAnhResource(ChanDoanHinhAnhService chanDoanHinhAnhService, ChanDoanHinhAnhQueryService chanDoanHinhAnhQueryService) {
        this.chanDoanHinhAnhService = chanDoanHinhAnhService;
        this.chanDoanHinhAnhQueryService = chanDoanHinhAnhQueryService;
    }

    /**
     * {@code POST  /chan-doan-hinh-anhs} : Create a new chanDoanHinhAnh.
     *
     * @param chanDoanHinhAnhDTO the chanDoanHinhAnhDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new chanDoanHinhAnhDTO, or with status {@code 400 (Bad Request)} if the chanDoanHinhAnh has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/chan-doan-hinh-anhs")
    public ResponseEntity<ChanDoanHinhAnhDTO> createChanDoanHinhAnh(@Valid @RequestBody ChanDoanHinhAnhDTO chanDoanHinhAnhDTO) throws URISyntaxException {
        log.debug("REST request to save ChanDoanHinhAnh : {}", chanDoanHinhAnhDTO);
        if (chanDoanHinhAnhDTO.getId() != null) {
            throw new BadRequestAlertException("A new chanDoanHinhAnh cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ChanDoanHinhAnhDTO result = chanDoanHinhAnhService.save(chanDoanHinhAnhDTO);
        return ResponseEntity.created(new URI("/api/chan-doan-hinh-anhs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /chan-doan-hinh-anhs} : Updates an existing chanDoanHinhAnh.
     *
     * @param chanDoanHinhAnhDTO the chanDoanHinhAnhDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated chanDoanHinhAnhDTO,
     * or with status {@code 400 (Bad Request)} if the chanDoanHinhAnhDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the chanDoanHinhAnhDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/chan-doan-hinh-anhs")
    public ResponseEntity<ChanDoanHinhAnhDTO> updateChanDoanHinhAnh(@Valid @RequestBody ChanDoanHinhAnhDTO chanDoanHinhAnhDTO) throws URISyntaxException {
        log.debug("REST request to update ChanDoanHinhAnh : {}", chanDoanHinhAnhDTO);
        if (chanDoanHinhAnhDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ChanDoanHinhAnhDTO result = chanDoanHinhAnhService.save(chanDoanHinhAnhDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, chanDoanHinhAnhDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /chan-doan-hinh-anhs} : get all the chanDoanHinhAnhs.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of chanDoanHinhAnhs in body.
     */
    @GetMapping("/chan-doan-hinh-anhs")
    public ResponseEntity<List<ChanDoanHinhAnhDTO>> getAllChanDoanHinhAnhs(ChanDoanHinhAnhCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ChanDoanHinhAnhs by criteria: {}", criteria);
        Page<ChanDoanHinhAnhDTO> page = chanDoanHinhAnhQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /chan-doan-hinh-anhs/count} : count all the chanDoanHinhAnhs.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/chan-doan-hinh-anhs/count")
    public ResponseEntity<Long> countChanDoanHinhAnhs(ChanDoanHinhAnhCriteria criteria) {
        log.debug("REST request to count ChanDoanHinhAnhs by criteria: {}", criteria);
        return ResponseEntity.ok().body(chanDoanHinhAnhQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /chan-doan-hinh-anhs/:id} : get the "id" chanDoanHinhAnh.
     *
     * @param id the id of the chanDoanHinhAnhDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the chanDoanHinhAnhDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/chan-doan-hinh-anhs/{id}")
    public ResponseEntity<ChanDoanHinhAnhDTO> getChanDoanHinhAnh(@PathVariable Long id) {
        log.debug("REST request to get ChanDoanHinhAnh : {}", id);
        Optional<ChanDoanHinhAnhDTO> chanDoanHinhAnhDTO = chanDoanHinhAnhService.findOne(id);
        return ResponseUtil.wrapOrNotFound(chanDoanHinhAnhDTO);
    }

    /**
     * {@code DELETE  /chan-doan-hinh-anhs/:id} : delete the "id" chanDoanHinhAnh.
     *
     * @param id the id of the chanDoanHinhAnhDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/chan-doan-hinh-anhs/{id}")
    public ResponseEntity<Void> deleteChanDoanHinhAnh(@PathVariable Long id) {
        log.debug("REST request to delete ChanDoanHinhAnh : {}", id);
        chanDoanHinhAnhService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

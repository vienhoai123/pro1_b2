package vn.vnpt.web.rest;

import vn.vnpt.service.LoaiXetNghiemService;
import vn.vnpt.web.rest.errors.BadRequestAlertException;
import vn.vnpt.service.dto.LoaiXetNghiemDTO;
import vn.vnpt.service.dto.LoaiXetNghiemCriteria;
import vn.vnpt.service.LoaiXetNghiemQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link vn.vnpt.domain.LoaiXetNghiem}.
 */
@RestController
@RequestMapping("/api")
public class LoaiXetNghiemResource {

    private final Logger log = LoggerFactory.getLogger(LoaiXetNghiemResource.class);

    private static final String ENTITY_NAME = "khamchuabenhLoaiXetNghiem";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final LoaiXetNghiemService loaiXetNghiemService;

    private final LoaiXetNghiemQueryService loaiXetNghiemQueryService;

    public LoaiXetNghiemResource(LoaiXetNghiemService loaiXetNghiemService, LoaiXetNghiemQueryService loaiXetNghiemQueryService) {
        this.loaiXetNghiemService = loaiXetNghiemService;
        this.loaiXetNghiemQueryService = loaiXetNghiemQueryService;
    }

    /**
     * {@code POST  /loai-xet-nghiems} : Create a new loaiXetNghiem.
     *
     * @param loaiXetNghiemDTO the loaiXetNghiemDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new loaiXetNghiemDTO, or with status {@code 400 (Bad Request)} if the loaiXetNghiem has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/loai-xet-nghiems")
    public ResponseEntity<LoaiXetNghiemDTO> createLoaiXetNghiem(@Valid @RequestBody LoaiXetNghiemDTO loaiXetNghiemDTO) throws URISyntaxException {
        log.debug("REST request to save LoaiXetNghiem : {}", loaiXetNghiemDTO);
        if (loaiXetNghiemDTO.getId() != null) {
            throw new BadRequestAlertException("A new loaiXetNghiem cannot already have an ID", ENTITY_NAME, "idexists");
        }
        LoaiXetNghiemDTO result = loaiXetNghiemService.save(loaiXetNghiemDTO);
        return ResponseEntity.created(new URI("/api/loai-xet-nghiems/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /loai-xet-nghiems} : Updates an existing loaiXetNghiem.
     *
     * @param loaiXetNghiemDTO the loaiXetNghiemDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated loaiXetNghiemDTO,
     * or with status {@code 400 (Bad Request)} if the loaiXetNghiemDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the loaiXetNghiemDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/loai-xet-nghiems")
    public ResponseEntity<LoaiXetNghiemDTO> updateLoaiXetNghiem(@Valid @RequestBody LoaiXetNghiemDTO loaiXetNghiemDTO) throws URISyntaxException {
        log.debug("REST request to update LoaiXetNghiem : {}", loaiXetNghiemDTO);
        if (loaiXetNghiemDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        LoaiXetNghiemDTO result = loaiXetNghiemService.save(loaiXetNghiemDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, loaiXetNghiemDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /loai-xet-nghiems} : get all the loaiXetNghiems.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of loaiXetNghiems in body.
     */
    @GetMapping("/loai-xet-nghiems")
    public ResponseEntity<List<LoaiXetNghiemDTO>> getAllLoaiXetNghiems(LoaiXetNghiemCriteria criteria, Pageable pageable) {
        log.debug("REST request to get LoaiXetNghiems by criteria: {}", criteria);
        Page<LoaiXetNghiemDTO> page = loaiXetNghiemQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /loai-xet-nghiems/count} : count all the loaiXetNghiems.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/loai-xet-nghiems/count")
    public ResponseEntity<Long> countLoaiXetNghiems(LoaiXetNghiemCriteria criteria) {
        log.debug("REST request to count LoaiXetNghiems by criteria: {}", criteria);
        return ResponseEntity.ok().body(loaiXetNghiemQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /loai-xet-nghiems/:id} : get the "id" loaiXetNghiem.
     *
     * @param id the id of the loaiXetNghiemDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the loaiXetNghiemDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/loai-xet-nghiems/{id}")
    public ResponseEntity<LoaiXetNghiemDTO> getLoaiXetNghiem(@PathVariable Long id) {
        log.debug("REST request to get LoaiXetNghiem : {}", id);
        Optional<LoaiXetNghiemDTO> loaiXetNghiemDTO = loaiXetNghiemService.findOne(id);
        return ResponseUtil.wrapOrNotFound(loaiXetNghiemDTO);
    }

    /**
     * {@code DELETE  /loai-xet-nghiems/:id} : delete the "id" loaiXetNghiem.
     *
     * @param id the id of the loaiXetNghiemDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/loai-xet-nghiems/{id}")
    public ResponseEntity<Void> deleteLoaiXetNghiem(@PathVariable Long id) {
        log.debug("REST request to delete LoaiXetNghiem : {}", id);
        loaiXetNghiemService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

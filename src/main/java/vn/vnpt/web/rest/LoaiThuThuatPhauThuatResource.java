package vn.vnpt.web.rest;

import vn.vnpt.service.LoaiThuThuatPhauThuatService;
import vn.vnpt.web.rest.errors.BadRequestAlertException;
import vn.vnpt.service.dto.LoaiThuThuatPhauThuatDTO;
import vn.vnpt.service.dto.LoaiThuThuatPhauThuatCriteria;
import vn.vnpt.service.LoaiThuThuatPhauThuatQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link vn.vnpt.domain.LoaiThuThuatPhauThuat}.
 */
@RestController
@RequestMapping("/api")
public class LoaiThuThuatPhauThuatResource {

    private final Logger log = LoggerFactory.getLogger(LoaiThuThuatPhauThuatResource.class);

    private static final String ENTITY_NAME = "khamchuabenhLoaiThuThuatPhauThuat";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final LoaiThuThuatPhauThuatService loaiThuThuatPhauThuatService;

    private final LoaiThuThuatPhauThuatQueryService loaiThuThuatPhauThuatQueryService;

    public LoaiThuThuatPhauThuatResource(LoaiThuThuatPhauThuatService loaiThuThuatPhauThuatService, LoaiThuThuatPhauThuatQueryService loaiThuThuatPhauThuatQueryService) {
        this.loaiThuThuatPhauThuatService = loaiThuThuatPhauThuatService;
        this.loaiThuThuatPhauThuatQueryService = loaiThuThuatPhauThuatQueryService;
    }

    /**
     * {@code POST  /loai-thu-thuat-phau-thuats} : Create a new loaiThuThuatPhauThuat.
     *
     * @param loaiThuThuatPhauThuatDTO the loaiThuThuatPhauThuatDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new loaiThuThuatPhauThuatDTO, or with status {@code 400 (Bad Request)} if the loaiThuThuatPhauThuat has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/loai-thu-thuat-phau-thuats")
    public ResponseEntity<LoaiThuThuatPhauThuatDTO> createLoaiThuThuatPhauThuat(@Valid @RequestBody LoaiThuThuatPhauThuatDTO loaiThuThuatPhauThuatDTO) throws URISyntaxException {
        log.debug("REST request to save LoaiThuThuatPhauThuat : {}", loaiThuThuatPhauThuatDTO);
        if (loaiThuThuatPhauThuatDTO.getId() != null) {
            throw new BadRequestAlertException("A new loaiThuThuatPhauThuat cannot already have an ID", ENTITY_NAME, "idexists");
        }
        LoaiThuThuatPhauThuatDTO result = loaiThuThuatPhauThuatService.save(loaiThuThuatPhauThuatDTO);
        return ResponseEntity.created(new URI("/api/loai-thu-thuat-phau-thuats/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /loai-thu-thuat-phau-thuats} : Updates an existing loaiThuThuatPhauThuat.
     *
     * @param loaiThuThuatPhauThuatDTO the loaiThuThuatPhauThuatDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated loaiThuThuatPhauThuatDTO,
     * or with status {@code 400 (Bad Request)} if the loaiThuThuatPhauThuatDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the loaiThuThuatPhauThuatDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/loai-thu-thuat-phau-thuats")
    public ResponseEntity<LoaiThuThuatPhauThuatDTO> updateLoaiThuThuatPhauThuat(@Valid @RequestBody LoaiThuThuatPhauThuatDTO loaiThuThuatPhauThuatDTO) throws URISyntaxException {
        log.debug("REST request to update LoaiThuThuatPhauThuat : {}", loaiThuThuatPhauThuatDTO);
        if (loaiThuThuatPhauThuatDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        LoaiThuThuatPhauThuatDTO result = loaiThuThuatPhauThuatService.save(loaiThuThuatPhauThuatDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, loaiThuThuatPhauThuatDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /loai-thu-thuat-phau-thuats} : get all the loaiThuThuatPhauThuats.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of loaiThuThuatPhauThuats in body.
     */
    @GetMapping("/loai-thu-thuat-phau-thuats")
    public ResponseEntity<List<LoaiThuThuatPhauThuatDTO>> getAllLoaiThuThuatPhauThuats(LoaiThuThuatPhauThuatCriteria criteria, Pageable pageable) {
        log.debug("REST request to get LoaiThuThuatPhauThuats by criteria: {}", criteria);
        Page<LoaiThuThuatPhauThuatDTO> page = loaiThuThuatPhauThuatQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /loai-thu-thuat-phau-thuats/count} : count all the loaiThuThuatPhauThuats.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/loai-thu-thuat-phau-thuats/count")
    public ResponseEntity<Long> countLoaiThuThuatPhauThuats(LoaiThuThuatPhauThuatCriteria criteria) {
        log.debug("REST request to count LoaiThuThuatPhauThuats by criteria: {}", criteria);
        return ResponseEntity.ok().body(loaiThuThuatPhauThuatQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /loai-thu-thuat-phau-thuats/:id} : get the "id" loaiThuThuatPhauThuat.
     *
     * @param id the id of the loaiThuThuatPhauThuatDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the loaiThuThuatPhauThuatDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/loai-thu-thuat-phau-thuats/{id}")
    public ResponseEntity<LoaiThuThuatPhauThuatDTO> getLoaiThuThuatPhauThuat(@PathVariable Long id) {
        log.debug("REST request to get LoaiThuThuatPhauThuat : {}", id);
        Optional<LoaiThuThuatPhauThuatDTO> loaiThuThuatPhauThuatDTO = loaiThuThuatPhauThuatService.findOne(id);
        return ResponseUtil.wrapOrNotFound(loaiThuThuatPhauThuatDTO);
    }

    /**
     * {@code DELETE  /loai-thu-thuat-phau-thuats/:id} : delete the "id" loaiThuThuatPhauThuat.
     *
     * @param id the id of the loaiThuThuatPhauThuatDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/loai-thu-thuat-phau-thuats/{id}")
    public ResponseEntity<Void> deleteLoaiThuThuatPhauThuat(@PathVariable Long id) {
        log.debug("REST request to delete LoaiThuThuatPhauThuat : {}", id);
        loaiThuThuatPhauThuatService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

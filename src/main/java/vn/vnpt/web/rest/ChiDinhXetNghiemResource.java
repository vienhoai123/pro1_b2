package vn.vnpt.web.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import vn.vnpt.domain.ChiDinhXetNghiemId;
import vn.vnpt.domain.DotDieuTriId;
import vn.vnpt.service.ChiDinhXetNghiemService;
import vn.vnpt.service.dto.DotDieuTriDTO;
import vn.vnpt.web.rest.errors.BadRequestAlertException;
import vn.vnpt.service.dto.ChiDinhXetNghiemDTO;
import vn.vnpt.service.dto.ChiDinhXetNghiemCriteria;
import vn.vnpt.service.ChiDinhXetNghiemQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * REST controller for managing {@link vn.vnpt.domain.ChiDinhXetNghiem}.
 */
@RestController
@RequestMapping("/api")
public class ChiDinhXetNghiemResource {

    private final Logger log = LoggerFactory.getLogger(ChiDinhXetNghiemResource.class);

    private static final String ENTITY_NAME = "khamchuabenhChiDinhXetNghiem";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ChiDinhXetNghiemService chiDinhXetNghiemService;

    private final ChiDinhXetNghiemQueryService chiDinhXetNghiemQueryService;

    public ChiDinhXetNghiemResource(ChiDinhXetNghiemService chiDinhXetNghiemService, ChiDinhXetNghiemQueryService chiDinhXetNghiemQueryService) {
        this.chiDinhXetNghiemService = chiDinhXetNghiemService;
        this.chiDinhXetNghiemQueryService = chiDinhXetNghiemQueryService;
    }

    /**
     * {@code POST  /chi-dinh-xet-nghiems} : Create a new chiDinhXetNghiem.
     *
     * @param chiDinhXetNghiemDTO the chiDinhXetNghiemDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new chiDinhXetNghiemDTO, or with status {@code 400 (Bad Request)} if the chiDinhXetNghiem has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/chi-dinh-xet-nghiems")
    public ResponseEntity<ChiDinhXetNghiemDTO> createChiDinhXetNghiem(@Valid @RequestBody ChiDinhXetNghiemDTO chiDinhXetNghiemDTO) throws URISyntaxException {
        log.debug("REST request to save ChiDinhXetNghiem : {}", chiDinhXetNghiemDTO);
        if (chiDinhXetNghiemDTO.getId() != null) {
            throw new BadRequestAlertException("A new chiDinhXetNghiem cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ChiDinhXetNghiemDTO result = chiDinhXetNghiemService.save(chiDinhXetNghiemDTO);
        return ResponseEntity.created(new URI("/api/chi-dinh-xet-nghiems/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /chi-dinh-xet-nghiems} : Updates an existing chiDinhXetNghiem.
     *
     * @param chiDinhXetNghiemDTO the chiDinhXetNghiemDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated chiDinhXetNghiemDTO,
     * or with status {@code 400 (Bad Request)} if the chiDinhXetNghiemDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the chiDinhXetNghiemDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/chi-dinh-xet-nghiems")
    public ResponseEntity<ChiDinhXetNghiemDTO> updateChiDinhXetNghiem(@Valid @RequestBody ChiDinhXetNghiemDTO chiDinhXetNghiemDTO) throws URISyntaxException {
        log.debug("REST request to update ChiDinhXetNghiem : {}", chiDinhXetNghiemDTO);
        if (chiDinhXetNghiemDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ChiDinhXetNghiemDTO result = chiDinhXetNghiemService.save(chiDinhXetNghiemDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, chiDinhXetNghiemDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /chi-dinh-xet-nghiems} : get all the chiDinhXetNghiems.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of chiDinhXetNghiems in body.
     */
    @GetMapping("/chi-dinh-xet-nghiems")
    public ResponseEntity<List<ChiDinhXetNghiemDTO>> getAllChiDinhXetNghiems(ChiDinhXetNghiemCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ChiDinhXetNghiems by criteria: {}", criteria);
        Page<ChiDinhXetNghiemDTO> page = chiDinhXetNghiemQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /chi-dinh-xet-nghiems/count} : count all the chiDinhXetNghiems.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/chi-dinh-xet-nghiems/count")
    public ResponseEntity<Long> countChiDinhXetNghiems(ChiDinhXetNghiemCriteria criteria) {
        log.debug("REST request to count ChiDinhXetNghiems by criteria: {}", criteria);
        return ResponseEntity.ok().body(chiDinhXetNghiemQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /chi-dinh-xet-nghiems/:id} : get the "id" chiDinhXetNghiem.
     *
//     * @param id the id of the chiDinhXetNghiemDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the chiDinhXetNghiemDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/chi-dinh-xet-nghiems/{id}")
    public ResponseEntity<ChiDinhXetNghiemDTO> getChiDinhXetNghiem(@MatrixVariable(pathVar = "id") Map<String, String> idMap) {
        final ObjectMapper mapper = new ObjectMapper();
        final ChiDinhXetNghiemId id = mapper.convertValue(idMap, ChiDinhXetNghiemId.class);
        log.debug("REST request to get ChiDinhXetNghiem : {}", id);
        Optional<ChiDinhXetNghiemDTO> chiDinhXetNghiemDTO = chiDinhXetNghiemService.findOne(id);
        return ResponseUtil.wrapOrNotFound(chiDinhXetNghiemDTO);
    }

    /**
     * {@code DELETE  /chi-dinh-xet-nghiems/:id} : delete the "id" chiDinhXetNghiem.
     *
//     * @param id the id of the chiDinhXetNghiemDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/chi-dinh-xet-nghiems/{id}")
    public ResponseEntity<Void> deleteChiDinhXetNghiem(@MatrixVariable(pathVar = "id") Map<String, String> idMap) {
        final ObjectMapper mapper = new ObjectMapper();
        final ChiDinhXetNghiemId id = mapper.convertValue(idMap, ChiDinhXetNghiemId.class);
        log.debug("REST request to delete ChiDinhXetNghiem : {}", id);
        chiDinhXetNghiemService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

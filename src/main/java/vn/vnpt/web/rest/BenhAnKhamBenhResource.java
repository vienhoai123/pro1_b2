package vn.vnpt.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import vn.vnpt.service.BenhAnKhamBenhService;
import vn.vnpt.service.mapper.BenhNhanMapper;
import vn.vnpt.service.mapper.DonViMapper;
import vn.vnpt.web.rest.errors.BadRequestAlertException;
import vn.vnpt.service.dto.BenhAnKhamBenhDTO;
import vn.vnpt.service.dto.BenhAnKhamBenhCriteria;
import vn.vnpt.service.BenhAnKhamBenhQueryService;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import java.util.Map;import java.util.Optional;
import vn.vnpt.domain.BenhAnKhamBenhId;

/**
 * REST controller for managing {@link vn.vnpt.domain.BenhAnKhamBenh}.
 */
@RestController
@RequestMapping("/api")
public class BenhAnKhamBenhResource {

    private final Logger log = LoggerFactory.getLogger(BenhAnKhamBenhResource.class);

    private static final String ENTITY_NAME = "khamchuabenhBenhAnKhamBenh";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BenhAnKhamBenhService benhAnKhamBenhService;

    private final BenhAnKhamBenhQueryService benhAnKhamBenhQueryService;
    @Autowired
    private BenhNhanMapper benhNhanMapper;
    @Autowired
    private DonViMapper donViMapper;

    public BenhAnKhamBenhResource(BenhAnKhamBenhService benhAnKhamBenhService, BenhAnKhamBenhQueryService benhAnKhamBenhQueryService) {
        this.benhAnKhamBenhService = benhAnKhamBenhService;
        this.benhAnKhamBenhQueryService = benhAnKhamBenhQueryService;
    }

    /**
     * {@code POST  /benh-an-kham-benhs} : Create a new benhAnKhamBenh.
     *
     * @param benhAnKhamBenhDTO the benhAnKhamBenhDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new benhAnKhamBenhDTO, or with status {@code 400 (Bad Request)} if the benhAnKhamBenh has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/benh-an-kham-benhs")
    public ResponseEntity<BenhAnKhamBenhDTO> createBenhAnKhamBenh(@Valid @RequestBody BenhAnKhamBenhDTO benhAnKhamBenhDTO) throws URISyntaxException {
        log.debug("REST request to save BenhAnKhamBenh : {}", benhAnKhamBenhDTO);
        if (benhAnKhamBenhService.findOne(new BenhAnKhamBenhId(benhAnKhamBenhDTO.getId(),benhAnKhamBenhDTO.getBenhNhanId(),benhAnKhamBenhDTO.getDonViId())).isPresent()) {
            throw new BadRequestAlertException("This benhAnKhamBenh already exists", ENTITY_NAME, "idexists");
        }
        BenhAnKhamBenhDTO result = benhAnKhamBenhService.save(benhAnKhamBenhDTO);
        return ResponseEntity.created(new URI("/api/benh-an-kham-benhs/" + "benhNhanId=" + result.getBenhNhanId() + ";" + "donViId=" + result.getDonViId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, "benhNhanId=" + result.getBenhNhanId() + ";" + "donViId=" + result.getDonViId()))
            .body(result);
    }

    /**
     * {@code PUT  /benh-an-kham-benhs} : Updates an existing benhAnKhamBenh.
     *
     * @param benhAnKhamBenhDTO the benhAnKhamBenhDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated benhAnKhamBenhDTO,
     * or with status {@code 400 (Bad Request)} if the benhAnKhamBenhDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the benhAnKhamBenhDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/benh-an-kham-benhs")
    public ResponseEntity<BenhAnKhamBenhDTO> updateBenhAnKhamBenh(@Valid @RequestBody BenhAnKhamBenhDTO benhAnKhamBenhDTO) throws URISyntaxException {
        log.debug("REST request to update BenhAnKhamBenh : {}", benhAnKhamBenhDTO);
        if (!benhAnKhamBenhService.findOne(new BenhAnKhamBenhId(benhAnKhamBenhDTO.getId(),benhAnKhamBenhDTO.getBenhNhanId(),benhAnKhamBenhDTO.getDonViId())).isPresent()) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        BenhAnKhamBenhDTO result = benhAnKhamBenhService.save(benhAnKhamBenhDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, "benhNhanId=" + result.getBenhNhanId() + ";" + "donViId=" + result.getDonViId()))
            .body(result);
    }

    /**
     * {@code GET  /benh-an-kham-benhs} : get all the benhAnKhamBenhs.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of benhAnKhamBenhs in body.
     */
    @GetMapping("/benh-an-kham-benhs")
    public ResponseEntity<List<BenhAnKhamBenhDTO>> getAllBenhAnKhamBenhs(BenhAnKhamBenhCriteria criteria, Pageable pageable) {
        log.debug("REST request to get BenhAnKhamBenhs by criteria: {}", criteria);
        Page<BenhAnKhamBenhDTO> page = benhAnKhamBenhQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /benh-an-kham-benhs/count} : count all the benhAnKhamBenhs.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/benh-an-kham-benhs/count")
    public ResponseEntity<Long> countBenhAnKhamBenhs(BenhAnKhamBenhCriteria criteria) {
        log.debug("REST request to count BenhAnKhamBenhs by criteria: {}", criteria);
        return ResponseEntity.ok().body(benhAnKhamBenhQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /benh-an-kham-benhs/:id} : get the "id" benhAnKhamBenh.
     *
     * @param idMap a Map representation of the id of the benhAnKhamBenhDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the benhAnKhamBenhDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/benh-an-kham-benhs/{id}")
    public ResponseEntity<BenhAnKhamBenhDTO> getBenhAnKhamBenh(@MatrixVariable(pathVar = "id") Map<String, String> idMap) {
        final ObjectMapper mapper = new ObjectMapper();
        final BenhAnKhamBenhId id = mapper.convertValue(idMap, BenhAnKhamBenhId.class);
        log.debug("REST request to get BenhAnKhamBenh : {}", id);
        Optional<BenhAnKhamBenhDTO> benhAnKhamBenhDTO = benhAnKhamBenhService.findOne(id);
        return ResponseUtil.wrapOrNotFound(benhAnKhamBenhDTO);
    }

    /**
     * {@code DELETE  /benh-an-kham-benhs/:id} : delete the "id" benhAnKhamBenh.
     *
     * @param idMap a Map representation of the id of the benhAnKhamBenhDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/benh-an-kham-benhs/{id}")
    public ResponseEntity<Void> deleteBenhAnKhamBenh(@MatrixVariable(pathVar = "id") Map<String, String> idMap) {
        final ObjectMapper mapper = new ObjectMapper();
        final BenhAnKhamBenhId id = mapper.convertValue(idMap, BenhAnKhamBenhId.class);
        log.debug("REST request to delete BenhAnKhamBenh : {}", id);
        benhAnKhamBenhService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

package vn.vnpt.web.rest;

import vn.vnpt.service.ThongTinKhoaService;
import vn.vnpt.web.rest.errors.BadRequestAlertException;
import vn.vnpt.service.dto.ThongTinKhoaDTO;
import vn.vnpt.service.dto.ThongTinKhoaCriteria;
import vn.vnpt.service.ThongTinKhoaQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link vn.vnpt.domain.ThongTinKhoa}.
 */
@RestController
@RequestMapping("/api")
public class ThongTinKhoaResource {

    private final Logger log = LoggerFactory.getLogger(ThongTinKhoaResource.class);

    private static final String ENTITY_NAME = "khamchuabenhThongTinKhoa";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ThongTinKhoaService thongTinKhoaService;

    private final ThongTinKhoaQueryService thongTinKhoaQueryService;

    public ThongTinKhoaResource(ThongTinKhoaService thongTinKhoaService, ThongTinKhoaQueryService thongTinKhoaQueryService) {
        this.thongTinKhoaService = thongTinKhoaService;
        this.thongTinKhoaQueryService = thongTinKhoaQueryService;
    }

    /**
     * {@code POST  /thong-tin-khoas} : Create a new thongTinKhoa.
     *
     * @param thongTinKhoaDTO the thongTinKhoaDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new thongTinKhoaDTO, or with status {@code 400 (Bad Request)} if the thongTinKhoa has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/thong-tin-khoas")
    public ResponseEntity<ThongTinKhoaDTO> createThongTinKhoa(@Valid @RequestBody ThongTinKhoaDTO thongTinKhoaDTO) throws URISyntaxException {
        log.debug("REST request to save ThongTinKhoa : {}", thongTinKhoaDTO);
        if (thongTinKhoaDTO.getId() != null) {
            throw new BadRequestAlertException("A new thongTinKhoa cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ThongTinKhoaDTO result = thongTinKhoaService.save(thongTinKhoaDTO);
        return ResponseEntity.created(new URI("/api/thong-tin-khoas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /thong-tin-khoas} : Updates an existing thongTinKhoa.
     *
     * @param thongTinKhoaDTO the thongTinKhoaDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated thongTinKhoaDTO,
     * or with status {@code 400 (Bad Request)} if the thongTinKhoaDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the thongTinKhoaDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/thong-tin-khoas")
    public ResponseEntity<ThongTinKhoaDTO> updateThongTinKhoa(@Valid @RequestBody ThongTinKhoaDTO thongTinKhoaDTO) throws URISyntaxException {
        log.debug("REST request to update ThongTinKhoa : {}", thongTinKhoaDTO);
        if (thongTinKhoaDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ThongTinKhoaDTO result = thongTinKhoaService.save(thongTinKhoaDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, thongTinKhoaDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /thong-tin-khoas} : get all the thongTinKhoas.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of thongTinKhoas in body.
     */
    @GetMapping("/thong-tin-khoas")
    public ResponseEntity<List<ThongTinKhoaDTO>> getAllThongTinKhoas(ThongTinKhoaCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ThongTinKhoas by criteria: {}", criteria);
        Page<ThongTinKhoaDTO> page = thongTinKhoaQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /thong-tin-khoas/count} : count all the thongTinKhoas.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/thong-tin-khoas/count")
    public ResponseEntity<Long> countThongTinKhoas(ThongTinKhoaCriteria criteria) {
        log.debug("REST request to count ThongTinKhoas by criteria: {}", criteria);
        return ResponseEntity.ok().body(thongTinKhoaQueryService.countByCriteria(criteria));
    }

//    /**
//     * {@code GET  /thong-tin-khoas/:id} : get the "id" thongTinKhoa.
//     *
//     * @param id the id of the thongTinKhoaDTO to retrieve.
//     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the thongTinKhoaDTO, or with status {@code 404 (Not Found)}.
//     */
//    @GetMapping("/thong-tin-khoas/{id}")
//    public ResponseEntity<ThongTinKhoaDTO> getThongTinKhoa(@PathVariable Long id) {
//        log.debug("REST request to get ThongTinKhoa : {}", id);
//        Optional<ThongTinKhoaDTO> thongTinKhoaDTO = thongTinKhoaService.findOne(id);
//        return ResponseUtil.wrapOrNotFound(thongTinKhoaDTO);
//    }

//    /**
//     * {@code DELETE  /thong-tin-khoas/:id} : delete the "id" thongTinKhoa.
//     *
//     * @param id the id of the thongTinKhoaDTO to delete.
//     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
//     */
//    @DeleteMapping("/thong-tin-khoas/{id}")
//    public ResponseEntity<Void> deleteThongTinKhoa(@PathVariable Long id) {
//        log.debug("REST request to delete ThongTinKhoa : {}", id);
//        thongTinKhoaService.delete(id);
//        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
//    }
}

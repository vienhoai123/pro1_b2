package vn.vnpt.web.rest;

import vn.vnpt.service.ThongTinBhxhService;
import vn.vnpt.web.rest.errors.BadRequestAlertException;
import vn.vnpt.service.dto.ThongTinBhxhDTO;
import vn.vnpt.service.dto.ThongTinBhxhCriteria;
import vn.vnpt.service.ThongTinBhxhQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link vn.vnpt.domain.ThongTinBhxh}.
 */
@RestController
@RequestMapping("/api")
public class ThongTinBhxhResource {

    private final Logger log = LoggerFactory.getLogger(ThongTinBhxhResource.class);

    private static final String ENTITY_NAME = "khamchuabenhThongTinBhxh";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ThongTinBhxhService thongTinBhxhService;

    private final ThongTinBhxhQueryService thongTinBhxhQueryService;

    public ThongTinBhxhResource(ThongTinBhxhService thongTinBhxhService, ThongTinBhxhQueryService thongTinBhxhQueryService) {
        this.thongTinBhxhService = thongTinBhxhService;
        this.thongTinBhxhQueryService = thongTinBhxhQueryService;
    }

    /**
     * {@code POST  /thong-tin-bhxhs} : Create a new thongTinBhxh.
     *
     * @param thongTinBhxhDTO the thongTinBhxhDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new thongTinBhxhDTO, or with status {@code 400 (Bad Request)} if the thongTinBhxh has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/thong-tin-bhxhs")
    public ResponseEntity<ThongTinBhxhDTO> createThongTinBhxh(@Valid @RequestBody ThongTinBhxhDTO thongTinBhxhDTO) throws URISyntaxException {
        log.debug("REST request to save ThongTinBhxh : {}", thongTinBhxhDTO);
        if (thongTinBhxhDTO.getId() != null) {
            throw new BadRequestAlertException("A new thongTinBhxh cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ThongTinBhxhDTO result = thongTinBhxhService.save(thongTinBhxhDTO);
        return ResponseEntity.created(new URI("/api/thong-tin-bhxhs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /thong-tin-bhxhs} : Updates an existing thongTinBhxh.
     *
     * @param thongTinBhxhDTO the thongTinBhxhDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated thongTinBhxhDTO,
     * or with status {@code 400 (Bad Request)} if the thongTinBhxhDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the thongTinBhxhDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/thong-tin-bhxhs")
    public ResponseEntity<ThongTinBhxhDTO> updateThongTinBhxh(@Valid @RequestBody ThongTinBhxhDTO thongTinBhxhDTO) throws URISyntaxException {
        log.debug("REST request to update ThongTinBhxh : {}", thongTinBhxhDTO);
        if (thongTinBhxhDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ThongTinBhxhDTO result = thongTinBhxhService.save(thongTinBhxhDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, thongTinBhxhDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /thong-tin-bhxhs} : get all the thongTinBhxhs.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of thongTinBhxhs in body.
     */
    @GetMapping("/thong-tin-bhxhs")
    public ResponseEntity<List<ThongTinBhxhDTO>> getAllThongTinBhxhs(ThongTinBhxhCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ThongTinBhxhs by criteria: {}", criteria);
        Page<ThongTinBhxhDTO> page = thongTinBhxhQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /thong-tin-bhxhs/count} : count all the thongTinBhxhs.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/thong-tin-bhxhs/count")
    public ResponseEntity<Long> countThongTinBhxhs(ThongTinBhxhCriteria criteria) {
        log.debug("REST request to count ThongTinBhxhs by criteria: {}", criteria);
        return ResponseEntity.ok().body(thongTinBhxhQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /thong-tin-bhxhs/:id} : get the "id" thongTinBhxh.
     *
     * @param id the id of the thongTinBhxhDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the thongTinBhxhDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/thong-tin-bhxhs/{id}")
    public ResponseEntity<ThongTinBhxhDTO> getThongTinBhxh(@PathVariable Long id) {
        log.debug("REST request to get ThongTinBhxh : {}", id);
        Optional<ThongTinBhxhDTO> thongTinBhxhDTO = thongTinBhxhService.findOne(id);
        return ResponseUtil.wrapOrNotFound(thongTinBhxhDTO);
    }

    /**
     * {@code DELETE  /thong-tin-bhxhs/:id} : delete the "id" thongTinBhxh.
     *
     * @param id the id of the thongTinBhxhDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/thong-tin-bhxhs/{id}")
    public ResponseEntity<Void> deleteThongTinBhxh(@PathVariable Long id) {
        log.debug("REST request to delete ThongTinBhxh : {}", id);
        thongTinBhxhService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

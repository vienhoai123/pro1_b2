package vn.vnpt.web.rest;

import vn.vnpt.service.DoiTuongBhytService;
import vn.vnpt.web.rest.errors.BadRequestAlertException;
import vn.vnpt.service.dto.DoiTuongBhytDTO;
import vn.vnpt.service.dto.DoiTuongBhytCriteria;
import vn.vnpt.service.DoiTuongBhytQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link vn.vnpt.domain.DoiTuongBhyt}.
 */
@RestController
@RequestMapping("/api")
public class DoiTuongBhytResource {

    private final Logger log = LoggerFactory.getLogger(DoiTuongBhytResource.class);

    private static final String ENTITY_NAME = "khamchuabenhDoiTuongBhyt";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DoiTuongBhytService doiTuongBhytService;

    private final DoiTuongBhytQueryService doiTuongBhytQueryService;

    public DoiTuongBhytResource(DoiTuongBhytService doiTuongBhytService, DoiTuongBhytQueryService doiTuongBhytQueryService) {
        this.doiTuongBhytService = doiTuongBhytService;
        this.doiTuongBhytQueryService = doiTuongBhytQueryService;
    }

    /**
     * {@code POST  /doi-tuong-bhyts} : Create a new doiTuongBhyt.
     *
     * @param doiTuongBhytDTO the doiTuongBhytDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new doiTuongBhytDTO, or with status {@code 400 (Bad Request)} if the doiTuongBhyt has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/doi-tuong-bhyts")
    public ResponseEntity<DoiTuongBhytDTO> createDoiTuongBhyt(@Valid @RequestBody DoiTuongBhytDTO doiTuongBhytDTO) throws URISyntaxException {
        log.debug("REST request to save DoiTuongBhyt : {}", doiTuongBhytDTO);
        if (doiTuongBhytDTO.getId() != null) {
            throw new BadRequestAlertException("A new doiTuongBhyt cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DoiTuongBhytDTO result = doiTuongBhytService.save(doiTuongBhytDTO);
        return ResponseEntity.created(new URI("/api/doi-tuong-bhyts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /doi-tuong-bhyts} : Updates an existing doiTuongBhyt.
     *
     * @param doiTuongBhytDTO the doiTuongBhytDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated doiTuongBhytDTO,
     * or with status {@code 400 (Bad Request)} if the doiTuongBhytDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the doiTuongBhytDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/doi-tuong-bhyts")
    public ResponseEntity<DoiTuongBhytDTO> updateDoiTuongBhyt(@Valid @RequestBody DoiTuongBhytDTO doiTuongBhytDTO) throws URISyntaxException {
        log.debug("REST request to update DoiTuongBhyt : {}", doiTuongBhytDTO);
        if (doiTuongBhytDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DoiTuongBhytDTO result = doiTuongBhytService.save(doiTuongBhytDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, doiTuongBhytDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /doi-tuong-bhyts} : get all the doiTuongBhyts.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of doiTuongBhyts in body.
     */
    @GetMapping("/doi-tuong-bhyts")
    public ResponseEntity<List<DoiTuongBhytDTO>> getAllDoiTuongBhyts(DoiTuongBhytCriteria criteria) {
        log.debug("REST request to get DoiTuongBhyts by criteria: {}", criteria);
        List<DoiTuongBhytDTO> entityList = doiTuongBhytQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
     * {@code GET  /doi-tuong-bhyts/count} : count all the doiTuongBhyts.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/doi-tuong-bhyts/count")
    public ResponseEntity<Long> countDoiTuongBhyts(DoiTuongBhytCriteria criteria) {
        log.debug("REST request to count DoiTuongBhyts by criteria: {}", criteria);
        return ResponseEntity.ok().body(doiTuongBhytQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /doi-tuong-bhyts/:id} : get the "id" doiTuongBhyt.
     *
     * @param id the id of the doiTuongBhytDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the doiTuongBhytDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/doi-tuong-bhyts/{id}")
    public ResponseEntity<DoiTuongBhytDTO> getDoiTuongBhyt(@PathVariable Long id) {
        log.debug("REST request to get DoiTuongBhyt : {}", id);
        Optional<DoiTuongBhytDTO> doiTuongBhytDTO = doiTuongBhytService.findOne(id);
        return ResponseUtil.wrapOrNotFound(doiTuongBhytDTO);
    }

    /**
     * {@code DELETE  /doi-tuong-bhyts/:id} : delete the "id" doiTuongBhyt.
     *
     * @param id the id of the doiTuongBhytDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/doi-tuong-bhyts/{id}")
    public ResponseEntity<Void> deleteDoiTuongBhyt(@PathVariable Long id) {
        log.debug("REST request to delete DoiTuongBhyt : {}", id);
        doiTuongBhytService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

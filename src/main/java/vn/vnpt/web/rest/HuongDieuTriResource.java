package vn.vnpt.web.rest;

import vn.vnpt.service.HuongDieuTriService;
import vn.vnpt.web.rest.errors.BadRequestAlertException;
import vn.vnpt.service.dto.HuongDieuTriDTO;
import vn.vnpt.service.dto.HuongDieuTriCriteria;
import vn.vnpt.service.HuongDieuTriQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link vn.vnpt.domain.HuongDieuTri}.
 */
@RestController
@RequestMapping("/api")
public class HuongDieuTriResource {

    private final Logger log = LoggerFactory.getLogger(HuongDieuTriResource.class);

    private static final String ENTITY_NAME = "khamchuabenhHuongDieuTri";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final HuongDieuTriService huongDieuTriService;

    private final HuongDieuTriQueryService huongDieuTriQueryService;

    public HuongDieuTriResource(HuongDieuTriService huongDieuTriService, HuongDieuTriQueryService huongDieuTriQueryService) {
        this.huongDieuTriService = huongDieuTriService;
        this.huongDieuTriQueryService = huongDieuTriQueryService;
    }

    /**
     * {@code POST  /huong-dieu-tris} : Create a new huongDieuTri.
     *
     * @param huongDieuTriDTO the huongDieuTriDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new huongDieuTriDTO, or with status {@code 400 (Bad Request)} if the huongDieuTri has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/huong-dieu-tris")
    public ResponseEntity<HuongDieuTriDTO> createHuongDieuTri(@Valid @RequestBody HuongDieuTriDTO huongDieuTriDTO) throws URISyntaxException {
        log.debug("REST request to save HuongDieuTri : {}", huongDieuTriDTO);
        if (huongDieuTriDTO.getId() != null) {
            throw new BadRequestAlertException("A new huongDieuTri cannot already have an ID", ENTITY_NAME, "idexists");
        }
        HuongDieuTriDTO result = huongDieuTriService.save(huongDieuTriDTO);
        return ResponseEntity.created(new URI("/api/huong-dieu-tris/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /huong-dieu-tris} : Updates an existing huongDieuTri.
     *
     * @param huongDieuTriDTO the huongDieuTriDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated huongDieuTriDTO,
     * or with status {@code 400 (Bad Request)} if the huongDieuTriDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the huongDieuTriDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/huong-dieu-tris")
    public ResponseEntity<HuongDieuTriDTO> updateHuongDieuTri(@Valid @RequestBody HuongDieuTriDTO huongDieuTriDTO) throws URISyntaxException {
        log.debug("REST request to update HuongDieuTri : {}", huongDieuTriDTO);
        if (huongDieuTriDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        HuongDieuTriDTO result = huongDieuTriService.save(huongDieuTriDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, huongDieuTriDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /huong-dieu-tris} : get all the huongDieuTris.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of huongDieuTris in body.
     */
    @GetMapping("/huong-dieu-tris")
    public ResponseEntity<List<HuongDieuTriDTO>> getAllHuongDieuTris(HuongDieuTriCriteria criteria, Pageable pageable) {
        log.debug("REST request to get HuongDieuTris by criteria: {}", criteria);
        Page<HuongDieuTriDTO> page = huongDieuTriQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /huong-dieu-tris/count} : count all the huongDieuTris.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/huong-dieu-tris/count")
    public ResponseEntity<Long> countHuongDieuTris(HuongDieuTriCriteria criteria) {
        log.debug("REST request to count HuongDieuTris by criteria: {}", criteria);
        return ResponseEntity.ok().body(huongDieuTriQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /huong-dieu-tris/:id} : get the "id" huongDieuTri.
     *
     * @param id the id of the huongDieuTriDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the huongDieuTriDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/huong-dieu-tris/{id}")
    public ResponseEntity<HuongDieuTriDTO> getHuongDieuTri(@PathVariable Long id) {
        log.debug("REST request to get HuongDieuTri : {}", id);
        Optional<HuongDieuTriDTO> huongDieuTriDTO = huongDieuTriService.findOne(id);
        return ResponseUtil.wrapOrNotFound(huongDieuTriDTO);
    }

    /**
     * {@code DELETE  /huong-dieu-tris/:id} : delete the "id" huongDieuTri.
     *
     * @param id the id of the huongDieuTriDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/huong-dieu-tris/{id}")
    public ResponseEntity<Void> deleteHuongDieuTri(@PathVariable Long id) {
        log.debug("REST request to delete HuongDieuTri : {}", id);
        huongDieuTriService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

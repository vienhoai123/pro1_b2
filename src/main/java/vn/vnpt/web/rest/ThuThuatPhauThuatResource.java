package vn.vnpt.web.rest;

import vn.vnpt.service.ThuThuatPhauThuatService;
import vn.vnpt.web.rest.errors.BadRequestAlertException;
import vn.vnpt.service.dto.ThuThuatPhauThuatDTO;
import vn.vnpt.service.dto.ThuThuatPhauThuatCriteria;
import vn.vnpt.service.ThuThuatPhauThuatQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link vn.vnpt.domain.ThuThuatPhauThuat}.
 */
@RestController
@RequestMapping("/api")
public class ThuThuatPhauThuatResource {

    private final Logger log = LoggerFactory.getLogger(ThuThuatPhauThuatResource.class);

    private static final String ENTITY_NAME = "khamchuabenhThuThuatPhauThuat";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ThuThuatPhauThuatService thuThuatPhauThuatService;

    private final ThuThuatPhauThuatQueryService thuThuatPhauThuatQueryService;

    public ThuThuatPhauThuatResource(ThuThuatPhauThuatService thuThuatPhauThuatService, ThuThuatPhauThuatQueryService thuThuatPhauThuatQueryService) {
        this.thuThuatPhauThuatService = thuThuatPhauThuatService;
        this.thuThuatPhauThuatQueryService = thuThuatPhauThuatQueryService;
    }

    /**
     * {@code POST  /thu-thuat-phau-thuats} : Create a new thuThuatPhauThuat.
     *
     * @param thuThuatPhauThuatDTO the thuThuatPhauThuatDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new thuThuatPhauThuatDTO, or with status {@code 400 (Bad Request)} if the thuThuatPhauThuat has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/thu-thuat-phau-thuats")
    public ResponseEntity<ThuThuatPhauThuatDTO> createThuThuatPhauThuat(@Valid @RequestBody ThuThuatPhauThuatDTO thuThuatPhauThuatDTO) throws URISyntaxException {
        log.debug("REST request to save ThuThuatPhauThuat : {}", thuThuatPhauThuatDTO);
        if (thuThuatPhauThuatDTO.getId() != null) {
            throw new BadRequestAlertException("A new thuThuatPhauThuat cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ThuThuatPhauThuatDTO result = thuThuatPhauThuatService.save(thuThuatPhauThuatDTO);
        return ResponseEntity.created(new URI("/api/thu-thuat-phau-thuats/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /thu-thuat-phau-thuats} : Updates an existing thuThuatPhauThuat.
     *
     * @param thuThuatPhauThuatDTO the thuThuatPhauThuatDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated thuThuatPhauThuatDTO,
     * or with status {@code 400 (Bad Request)} if the thuThuatPhauThuatDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the thuThuatPhauThuatDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/thu-thuat-phau-thuats")
    public ResponseEntity<ThuThuatPhauThuatDTO> updateThuThuatPhauThuat(@Valid @RequestBody ThuThuatPhauThuatDTO thuThuatPhauThuatDTO) throws URISyntaxException {
        log.debug("REST request to update ThuThuatPhauThuat : {}", thuThuatPhauThuatDTO);
        if (thuThuatPhauThuatDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ThuThuatPhauThuatDTO result = thuThuatPhauThuatService.save(thuThuatPhauThuatDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, thuThuatPhauThuatDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /thu-thuat-phau-thuats} : get all the thuThuatPhauThuats.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of thuThuatPhauThuats in body.
     */
    @GetMapping("/thu-thuat-phau-thuats")
    public ResponseEntity<List<ThuThuatPhauThuatDTO>> getAllThuThuatPhauThuats(ThuThuatPhauThuatCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ThuThuatPhauThuats by criteria: {}", criteria);
        Page<ThuThuatPhauThuatDTO> page = thuThuatPhauThuatQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /thu-thuat-phau-thuats/count} : count all the thuThuatPhauThuats.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/thu-thuat-phau-thuats/count")
    public ResponseEntity<Long> countThuThuatPhauThuats(ThuThuatPhauThuatCriteria criteria) {
        log.debug("REST request to count ThuThuatPhauThuats by criteria: {}", criteria);
        return ResponseEntity.ok().body(thuThuatPhauThuatQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /thu-thuat-phau-thuats/:id} : get the "id" thuThuatPhauThuat.
     *
     * @param id the id of the thuThuatPhauThuatDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the thuThuatPhauThuatDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/thu-thuat-phau-thuats/{id}")
    public ResponseEntity<ThuThuatPhauThuatDTO> getThuThuatPhauThuat(@PathVariable Long id) {
        log.debug("REST request to get ThuThuatPhauThuat : {}", id);
        Optional<ThuThuatPhauThuatDTO> thuThuatPhauThuatDTO = thuThuatPhauThuatService.findOne(id);
        return ResponseUtil.wrapOrNotFound(thuThuatPhauThuatDTO);
    }

    /**
     * {@code DELETE  /thu-thuat-phau-thuats/:id} : delete the "id" thuThuatPhauThuat.
     *
     * @param id the id of the thuThuatPhauThuatDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/thu-thuat-phau-thuats/{id}")
    public ResponseEntity<Void> deleteThuThuatPhauThuat(@PathVariable Long id) {
        log.debug("REST request to delete ThuThuatPhauThuat : {}", id);
        thuThuatPhauThuatService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

package vn.vnpt.web.rest;

import vn.vnpt.service.DonViService;
import vn.vnpt.web.rest.errors.BadRequestAlertException;
import vn.vnpt.service.dto.DonViDTO;
import vn.vnpt.service.dto.DonViCriteria;
import vn.vnpt.service.DonViQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link vn.vnpt.domain.DonVi}.
 */
@RestController
@RequestMapping("/api")
public class DonViResource {

    private final Logger log = LoggerFactory.getLogger(DonViResource.class);

    private static final String ENTITY_NAME = "khamchuabenhDonVi";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DonViService donViService;

    private final DonViQueryService donViQueryService;

    public DonViResource(DonViService donViService, DonViQueryService donViQueryService) {
        this.donViService = donViService;
        this.donViQueryService = donViQueryService;
    }

    /**
     * {@code POST  /don-vis} : Create a new donVi.
     *
     * @param donViDTO the donViDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new donViDTO, or with status {@code 400 (Bad Request)} if the donVi has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/don-vis")
    public ResponseEntity<DonViDTO> createDonVi(@Valid @RequestBody DonViDTO donViDTO) throws URISyntaxException {
        log.debug("REST request to save DonVi : {}", donViDTO);
        if (donViDTO.getId() != null) {
            throw new BadRequestAlertException("A new donVi cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DonViDTO result = donViService.save(donViDTO);
        return ResponseEntity.created(new URI("/api/don-vis/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /don-vis} : Updates an existing donVi.
     *
     * @param donViDTO the donViDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated donViDTO,
     * or with status {@code 400 (Bad Request)} if the donViDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the donViDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/don-vis")
    public ResponseEntity<DonViDTO> updateDonVi(@Valid @RequestBody DonViDTO donViDTO) throws URISyntaxException {
        log.debug("REST request to update DonVi : {}", donViDTO);
        if (donViDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DonViDTO result = donViService.save(donViDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /don-vis} : get all the donVis.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of donVis in body.
     */
    @GetMapping("/don-vis")
    public ResponseEntity<List<DonViDTO>> getAllDonVis(DonViCriteria criteria, Pageable pageable) {
        log.debug("REST request to get DonVis by criteria: {}", criteria);
        Page<DonViDTO> page = donViQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /don-vis/count} : count all the donVis.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/don-vis/count")
    public ResponseEntity<Long> countDonVis(DonViCriteria criteria) {
        log.debug("REST request to count DonVis by criteria: {}", criteria);
        return ResponseEntity.ok().body(donViQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /don-vis/:id} : get the "id" donVi.
     *
     * @param id the id of the donViDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the donViDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/don-vis/{id}")
    public ResponseEntity<DonViDTO> getDonVi(@PathVariable Long id) {
        log.debug("REST request to get DonVi : {}", id);
        Optional<DonViDTO> donViDTO = donViService.findOne(id);
        return ResponseUtil.wrapOrNotFound(donViDTO);
    }

    /**
     * {@code DELETE  /don-vis/:id} : delete the "id" donVi.
     *
     * @param id the id of the donViDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/don-vis/{id}")
    public ResponseEntity<Void> deleteDonVi(@PathVariable Long id) {
        log.debug("REST request to delete DonVi : {}", id);
        donViService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

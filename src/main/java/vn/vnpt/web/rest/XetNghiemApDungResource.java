package vn.vnpt.web.rest;

import vn.vnpt.service.XetNghiemApDungService;
import vn.vnpt.web.rest.errors.BadRequestAlertException;
import vn.vnpt.service.dto.XetNghiemApDungDTO;
import vn.vnpt.service.dto.XetNghiemApDungCriteria;
import vn.vnpt.service.XetNghiemApDungQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link vn.vnpt.domain.XetNghiemApDung}.
 */
@RestController
@RequestMapping("/api")
public class XetNghiemApDungResource {

    private final Logger log = LoggerFactory.getLogger(XetNghiemApDungResource.class);

    private static final String ENTITY_NAME = "khamchuabenhXetNghiemApDung";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final XetNghiemApDungService xetNghiemApDungService;

    private final XetNghiemApDungQueryService xetNghiemApDungQueryService;

    public XetNghiemApDungResource(XetNghiemApDungService xetNghiemApDungService, XetNghiemApDungQueryService xetNghiemApDungQueryService) {
        this.xetNghiemApDungService = xetNghiemApDungService;
        this.xetNghiemApDungQueryService = xetNghiemApDungQueryService;
    }

    /**
     * {@code POST  /xet-nghiem-ap-dungs} : Create a new xetNghiemApDung.
     *
     * @param xetNghiemApDungDTO the xetNghiemApDungDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new xetNghiemApDungDTO, or with status {@code 400 (Bad Request)} if the xetNghiemApDung has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/xet-nghiem-ap-dungs")
    public ResponseEntity<XetNghiemApDungDTO> createXetNghiemApDung(@Valid @RequestBody XetNghiemApDungDTO xetNghiemApDungDTO) throws URISyntaxException {
        log.debug("REST request to save XetNghiemApDung : {}", xetNghiemApDungDTO);
        if (xetNghiemApDungDTO.getId() != null) {
            throw new BadRequestAlertException("A new xetNghiemApDung cannot already have an ID", ENTITY_NAME, "idexists");
        }
        XetNghiemApDungDTO result = xetNghiemApDungService.save(xetNghiemApDungDTO);
        return ResponseEntity.created(new URI("/api/xet-nghiem-ap-dungs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /xet-nghiem-ap-dungs} : Updates an existing xetNghiemApDung.
     *
     * @param xetNghiemApDungDTO the xetNghiemApDungDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated xetNghiemApDungDTO,
     * or with status {@code 400 (Bad Request)} if the xetNghiemApDungDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the xetNghiemApDungDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/xet-nghiem-ap-dungs")
    public ResponseEntity<XetNghiemApDungDTO> updateXetNghiemApDung(@Valid @RequestBody XetNghiemApDungDTO xetNghiemApDungDTO) throws URISyntaxException {
        log.debug("REST request to update XetNghiemApDung : {}", xetNghiemApDungDTO);
        if (xetNghiemApDungDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        XetNghiemApDungDTO result = xetNghiemApDungService.save(xetNghiemApDungDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, xetNghiemApDungDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /xet-nghiem-ap-dungs} : get all the xetNghiemApDungs.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of xetNghiemApDungs in body.
     */
    @GetMapping("/xet-nghiem-ap-dungs")
    public ResponseEntity<List<XetNghiemApDungDTO>> getAllXetNghiemApDungs(XetNghiemApDungCriteria criteria, Pageable pageable) {
        log.debug("REST request to get XetNghiemApDungs by criteria: {}", criteria);
        Page<XetNghiemApDungDTO> page = xetNghiemApDungQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /xet-nghiem-ap-dungs/count} : count all the xetNghiemApDungs.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/xet-nghiem-ap-dungs/count")
    public ResponseEntity<Long> countXetNghiemApDungs(XetNghiemApDungCriteria criteria) {
        log.debug("REST request to count XetNghiemApDungs by criteria: {}", criteria);
        return ResponseEntity.ok().body(xetNghiemApDungQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /xet-nghiem-ap-dungs/:id} : get the "id" xetNghiemApDung.
     *
     * @param id the id of the xetNghiemApDungDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the xetNghiemApDungDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/xet-nghiem-ap-dungs/{id}")
    public ResponseEntity<XetNghiemApDungDTO> getXetNghiemApDung(@PathVariable Long id) {
        log.debug("REST request to get XetNghiemApDung : {}", id);
        Optional<XetNghiemApDungDTO> xetNghiemApDungDTO = xetNghiemApDungService.findOne(id);
        return ResponseUtil.wrapOrNotFound(xetNghiemApDungDTO);
    }

    /**
     * {@code DELETE  /xet-nghiem-ap-dungs/:id} : delete the "id" xetNghiemApDung.
     *
     * @param id the id of the xetNghiemApDungDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/xet-nghiem-ap-dungs/{id}")
    public ResponseEntity<Void> deleteXetNghiemApDung(@PathVariable Long id) {
        log.debug("REST request to delete XetNghiemApDung : {}", id);
        xetNghiemApDungService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

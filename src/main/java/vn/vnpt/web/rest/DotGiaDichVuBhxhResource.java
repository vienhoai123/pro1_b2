package vn.vnpt.web.rest;

import vn.vnpt.service.DotGiaDichVuBhxhService;
import vn.vnpt.web.rest.errors.BadRequestAlertException;
import vn.vnpt.service.dto.DotGiaDichVuBhxhDTO;
import vn.vnpt.service.dto.DotGiaDichVuBhxhCriteria;
import vn.vnpt.service.DotGiaDichVuBhxhQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link vn.vnpt.domain.DotGiaDichVuBhxh}.
 */
@RestController
@RequestMapping("/api")
public class DotGiaDichVuBhxhResource {

    private final Logger log = LoggerFactory.getLogger(DotGiaDichVuBhxhResource.class);

    private static final String ENTITY_NAME = "khamchuabenhDotGiaDichVuBhxh";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DotGiaDichVuBhxhService dotGiaDichVuBhxhService;

    private final DotGiaDichVuBhxhQueryService dotGiaDichVuBhxhQueryService;

    public DotGiaDichVuBhxhResource(DotGiaDichVuBhxhService dotGiaDichVuBhxhService, DotGiaDichVuBhxhQueryService dotGiaDichVuBhxhQueryService) {
        this.dotGiaDichVuBhxhService = dotGiaDichVuBhxhService;
        this.dotGiaDichVuBhxhQueryService = dotGiaDichVuBhxhQueryService;
    }

    /**
     * {@code POST  /dot-gia-dich-vu-bhxhs} : Create a new dotGiaDichVuBhxh.
     *
     * @param dotGiaDichVuBhxhDTO the dotGiaDichVuBhxhDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new dotGiaDichVuBhxhDTO, or with status {@code 400 (Bad Request)} if the dotGiaDichVuBhxh has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/dot-gia-dich-vu-bhxhs")
    public ResponseEntity<DotGiaDichVuBhxhDTO> createDotGiaDichVuBhxh(@Valid @RequestBody DotGiaDichVuBhxhDTO dotGiaDichVuBhxhDTO) throws URISyntaxException {
        log.debug("REST request to save DotGiaDichVuBhxh : {}", dotGiaDichVuBhxhDTO);
        if (dotGiaDichVuBhxhDTO.getId() != null) {
            throw new BadRequestAlertException("A new dotGiaDichVuBhxh cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DotGiaDichVuBhxhDTO result = dotGiaDichVuBhxhService.save(dotGiaDichVuBhxhDTO);
        return ResponseEntity.created(new URI("/api/dot-gia-dich-vu-bhxhs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /dot-gia-dich-vu-bhxhs} : Updates an existing dotGiaDichVuBhxh.
     *
     * @param dotGiaDichVuBhxhDTO the dotGiaDichVuBhxhDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated dotGiaDichVuBhxhDTO,
     * or with status {@code 400 (Bad Request)} if the dotGiaDichVuBhxhDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the dotGiaDichVuBhxhDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/dot-gia-dich-vu-bhxhs")
    public ResponseEntity<DotGiaDichVuBhxhDTO> updateDotGiaDichVuBhxh(@Valid @RequestBody DotGiaDichVuBhxhDTO dotGiaDichVuBhxhDTO) throws URISyntaxException {
        log.debug("REST request to update DotGiaDichVuBhxh : {}", dotGiaDichVuBhxhDTO);
        if (dotGiaDichVuBhxhDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DotGiaDichVuBhxhDTO result = dotGiaDichVuBhxhService.save(dotGiaDichVuBhxhDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, dotGiaDichVuBhxhDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /dot-gia-dich-vu-bhxhs} : get all the dotGiaDichVuBhxhs.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of dotGiaDichVuBhxhs in body.
     */
    @GetMapping("/dot-gia-dich-vu-bhxhs")
    public ResponseEntity<List<DotGiaDichVuBhxhDTO>> getAllDotGiaDichVuBhxhs(DotGiaDichVuBhxhCriteria criteria, Pageable pageable) {
        log.debug("REST request to get DotGiaDichVuBhxhs by criteria: {}", criteria);
        Page<DotGiaDichVuBhxhDTO> page = dotGiaDichVuBhxhQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /dot-gia-dich-vu-bhxhs/count} : count all the dotGiaDichVuBhxhs.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/dot-gia-dich-vu-bhxhs/count")
    public ResponseEntity<Long> countDotGiaDichVuBhxhs(DotGiaDichVuBhxhCriteria criteria) {
        log.debug("REST request to count DotGiaDichVuBhxhs by criteria: {}", criteria);
        return ResponseEntity.ok().body(dotGiaDichVuBhxhQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /dot-gia-dich-vu-bhxhs/:id} : get the "id" dotGiaDichVuBhxh.
     *
     * @param id the id of the dotGiaDichVuBhxhDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the dotGiaDichVuBhxhDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/dot-gia-dich-vu-bhxhs/{id}")
    public ResponseEntity<DotGiaDichVuBhxhDTO> getDotGiaDichVuBhxh(@PathVariable Long id) {
        log.debug("REST request to get DotGiaDichVuBhxh : {}", id);
        Optional<DotGiaDichVuBhxhDTO> dotGiaDichVuBhxhDTO = dotGiaDichVuBhxhService.findOne(id);
        return ResponseUtil.wrapOrNotFound(dotGiaDichVuBhxhDTO);
    }

    /**
     * {@code DELETE  /dot-gia-dich-vu-bhxhs/:id} : delete the "id" dotGiaDichVuBhxh.
     *
     * @param id the id of the dotGiaDichVuBhxhDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/dot-gia-dich-vu-bhxhs/{id}")
    public ResponseEntity<Void> deleteDotGiaDichVuBhxh(@PathVariable Long id) {
        log.debug("REST request to delete DotGiaDichVuBhxh : {}", id);
        dotGiaDichVuBhxhService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

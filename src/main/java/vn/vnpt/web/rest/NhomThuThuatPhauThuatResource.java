package vn.vnpt.web.rest;

import vn.vnpt.service.NhomThuThuatPhauThuatService;
import vn.vnpt.web.rest.errors.BadRequestAlertException;
import vn.vnpt.service.dto.NhomThuThuatPhauThuatDTO;
import vn.vnpt.service.dto.NhomThuThuatPhauThuatCriteria;
import vn.vnpt.service.NhomThuThuatPhauThuatQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link vn.vnpt.domain.NhomThuThuatPhauThuat}.
 */
@RestController
@RequestMapping("/api")
public class NhomThuThuatPhauThuatResource {

    private final Logger log = LoggerFactory.getLogger(NhomThuThuatPhauThuatResource.class);

    private static final String ENTITY_NAME = "khamchuabenhNhomThuThuatPhauThuat";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final NhomThuThuatPhauThuatService nhomThuThuatPhauThuatService;

    private final NhomThuThuatPhauThuatQueryService nhomThuThuatPhauThuatQueryService;

    public NhomThuThuatPhauThuatResource(NhomThuThuatPhauThuatService nhomThuThuatPhauThuatService, NhomThuThuatPhauThuatQueryService nhomThuThuatPhauThuatQueryService) {
        this.nhomThuThuatPhauThuatService = nhomThuThuatPhauThuatService;
        this.nhomThuThuatPhauThuatQueryService = nhomThuThuatPhauThuatQueryService;
    }

    /**
     * {@code POST  /nhom-thu-thuat-phau-thuats} : Create a new nhomThuThuatPhauThuat.
     *
     * @param nhomThuThuatPhauThuatDTO the nhomThuThuatPhauThuatDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new nhomThuThuatPhauThuatDTO, or with status {@code 400 (Bad Request)} if the nhomThuThuatPhauThuat has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/nhom-thu-thuat-phau-thuats")
    public ResponseEntity<NhomThuThuatPhauThuatDTO> createNhomThuThuatPhauThuat(@Valid @RequestBody NhomThuThuatPhauThuatDTO nhomThuThuatPhauThuatDTO) throws URISyntaxException {
        log.debug("REST request to save NhomThuThuatPhauThuat : {}", nhomThuThuatPhauThuatDTO);
        if (nhomThuThuatPhauThuatDTO.getId() != null) {
            throw new BadRequestAlertException("A new nhomThuThuatPhauThuat cannot already have an ID", ENTITY_NAME, "idexists");
        }
        NhomThuThuatPhauThuatDTO result = nhomThuThuatPhauThuatService.save(nhomThuThuatPhauThuatDTO);
        return ResponseEntity.created(new URI("/api/nhom-thu-thuat-phau-thuats/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /nhom-thu-thuat-phau-thuats} : Updates an existing nhomThuThuatPhauThuat.
     *
     * @param nhomThuThuatPhauThuatDTO the nhomThuThuatPhauThuatDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated nhomThuThuatPhauThuatDTO,
     * or with status {@code 400 (Bad Request)} if the nhomThuThuatPhauThuatDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the nhomThuThuatPhauThuatDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/nhom-thu-thuat-phau-thuats")
    public ResponseEntity<NhomThuThuatPhauThuatDTO> updateNhomThuThuatPhauThuat(@Valid @RequestBody NhomThuThuatPhauThuatDTO nhomThuThuatPhauThuatDTO) throws URISyntaxException {
        log.debug("REST request to update NhomThuThuatPhauThuat : {}", nhomThuThuatPhauThuatDTO);
        if (nhomThuThuatPhauThuatDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        NhomThuThuatPhauThuatDTO result = nhomThuThuatPhauThuatService.save(nhomThuThuatPhauThuatDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, nhomThuThuatPhauThuatDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /nhom-thu-thuat-phau-thuats} : get all the nhomThuThuatPhauThuats.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of nhomThuThuatPhauThuats in body.
     */
    @GetMapping("/nhom-thu-thuat-phau-thuats")
    public ResponseEntity<List<NhomThuThuatPhauThuatDTO>> getAllNhomThuThuatPhauThuats(NhomThuThuatPhauThuatCriteria criteria, Pageable pageable) {
        log.debug("REST request to get NhomThuThuatPhauThuats by criteria: {}", criteria);
        Page<NhomThuThuatPhauThuatDTO> page = nhomThuThuatPhauThuatQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /nhom-thu-thuat-phau-thuats/count} : count all the nhomThuThuatPhauThuats.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/nhom-thu-thuat-phau-thuats/count")
    public ResponseEntity<Long> countNhomThuThuatPhauThuats(NhomThuThuatPhauThuatCriteria criteria) {
        log.debug("REST request to count NhomThuThuatPhauThuats by criteria: {}", criteria);
        return ResponseEntity.ok().body(nhomThuThuatPhauThuatQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /nhom-thu-thuat-phau-thuats/:id} : get the "id" nhomThuThuatPhauThuat.
     *
     * @param id the id of the nhomThuThuatPhauThuatDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the nhomThuThuatPhauThuatDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/nhom-thu-thuat-phau-thuats/{id}")
    public ResponseEntity<NhomThuThuatPhauThuatDTO> getNhomThuThuatPhauThuat(@PathVariable Long id) {
        log.debug("REST request to get NhomThuThuatPhauThuat : {}", id);
        Optional<NhomThuThuatPhauThuatDTO> nhomThuThuatPhauThuatDTO = nhomThuThuatPhauThuatService.findOne(id);
        return ResponseUtil.wrapOrNotFound(nhomThuThuatPhauThuatDTO);
    }

    /**
     * {@code DELETE  /nhom-thu-thuat-phau-thuats/:id} : delete the "id" nhomThuThuatPhauThuat.
     *
     * @param id the id of the nhomThuThuatPhauThuatDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/nhom-thu-thuat-phau-thuats/{id}")
    public ResponseEntity<Void> deleteNhomThuThuatPhauThuat(@PathVariable Long id) {
        log.debug("REST request to delete NhomThuThuatPhauThuat : {}", id);
        nhomThuThuatPhauThuatService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

package vn.vnpt.web.rest;

import vn.vnpt.service.TPhanNhomTTPTService;
import vn.vnpt.web.rest.errors.BadRequestAlertException;
import vn.vnpt.service.dto.TPhanNhomTTPTDTO;
import vn.vnpt.service.dto.TPhanNhomTTPTCriteria;
import vn.vnpt.service.TPhanNhomTTPTQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link vn.vnpt.domain.TPhanNhomTTPT}.
 */
@RestController
@RequestMapping("/api")
public class TPhanNhomTTPTResource {

    private final Logger log = LoggerFactory.getLogger(TPhanNhomTTPTResource.class);

    private static final String ENTITY_NAME = "khamchuabenhTPhanNhomTtpt";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TPhanNhomTTPTService tPhanNhomTTPTService;

    private final TPhanNhomTTPTQueryService tPhanNhomTTPTQueryService;

    public TPhanNhomTTPTResource(TPhanNhomTTPTService tPhanNhomTTPTService, TPhanNhomTTPTQueryService tPhanNhomTTPTQueryService) {
        this.tPhanNhomTTPTService = tPhanNhomTTPTService;
        this.tPhanNhomTTPTQueryService = tPhanNhomTTPTQueryService;
    }

    /**
     * {@code POST  /t-phan-nhom-ttpts} : Create a new tPhanNhomTTPT.
     *
     * @param tPhanNhomTTPTDTO the tPhanNhomTTPTDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new tPhanNhomTTPTDTO, or with status {@code 400 (Bad Request)} if the tPhanNhomTTPT has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/t-phan-nhom-ttpts")
    public ResponseEntity<TPhanNhomTTPTDTO> createTPhanNhomTTPT(@Valid @RequestBody TPhanNhomTTPTDTO tPhanNhomTTPTDTO) throws URISyntaxException {
        log.debug("REST request to save TPhanNhomTTPT : {}", tPhanNhomTTPTDTO);
        if (tPhanNhomTTPTDTO.getId() != null) {
            throw new BadRequestAlertException("A new tPhanNhomTTPT cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TPhanNhomTTPTDTO result = tPhanNhomTTPTService.save(tPhanNhomTTPTDTO);
        return ResponseEntity.created(new URI("/api/t-phan-nhom-ttpts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /t-phan-nhom-ttpts} : Updates an existing tPhanNhomTTPT.
     *
     * @param tPhanNhomTTPTDTO the tPhanNhomTTPTDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated tPhanNhomTTPTDTO,
     * or with status {@code 400 (Bad Request)} if the tPhanNhomTTPTDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the tPhanNhomTTPTDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/t-phan-nhom-ttpts")
    public ResponseEntity<TPhanNhomTTPTDTO> updateTPhanNhomTTPT(@Valid @RequestBody TPhanNhomTTPTDTO tPhanNhomTTPTDTO) throws URISyntaxException {
        log.debug("REST request to update TPhanNhomTTPT : {}", tPhanNhomTTPTDTO);
        if (tPhanNhomTTPTDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TPhanNhomTTPTDTO result = tPhanNhomTTPTService.save(tPhanNhomTTPTDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, tPhanNhomTTPTDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /t-phan-nhom-ttpts} : get all the tPhanNhomTTPTS.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of tPhanNhomTTPTS in body.
     */
    @GetMapping("/t-phan-nhom-ttpts")
    public ResponseEntity<List<TPhanNhomTTPTDTO>> getAllTPhanNhomTTPTS(TPhanNhomTTPTCriteria criteria, Pageable pageable) {
        log.debug("REST request to get TPhanNhomTTPTS by criteria: {}", criteria);
        Page<TPhanNhomTTPTDTO> page = tPhanNhomTTPTQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /t-phan-nhom-ttpts/count} : count all the tPhanNhomTTPTS.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/t-phan-nhom-ttpts/count")
    public ResponseEntity<Long> countTPhanNhomTTPTS(TPhanNhomTTPTCriteria criteria) {
        log.debug("REST request to count TPhanNhomTTPTS by criteria: {}", criteria);
        return ResponseEntity.ok().body(tPhanNhomTTPTQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /t-phan-nhom-ttpts/:id} : get the "id" tPhanNhomTTPT.
     *
     * @param id the id of the tPhanNhomTTPTDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the tPhanNhomTTPTDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/t-phan-nhom-ttpts/{id}")
    public ResponseEntity<TPhanNhomTTPTDTO> getTPhanNhomTTPT(@PathVariable Long id) {
        log.debug("REST request to get TPhanNhomTTPT : {}", id);
        Optional<TPhanNhomTTPTDTO> tPhanNhomTTPTDTO = tPhanNhomTTPTService.findOne(id);
        return ResponseUtil.wrapOrNotFound(tPhanNhomTTPTDTO);
    }

    /**
     * {@code DELETE  /t-phan-nhom-ttpts/:id} : delete the "id" tPhanNhomTTPT.
     *
     * @param id the id of the tPhanNhomTTPTDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/t-phan-nhom-ttpts/{id}")
    public ResponseEntity<Void> deleteTPhanNhomTTPT(@PathVariable Long id) {
        log.debug("REST request to delete TPhanNhomTTPT : {}", id);
        tPhanNhomTTPTService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

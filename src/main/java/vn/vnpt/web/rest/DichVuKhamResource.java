package vn.vnpt.web.rest;

import vn.vnpt.service.DichVuKhamService;
import vn.vnpt.web.rest.errors.BadRequestAlertException;
import vn.vnpt.service.dto.DichVuKhamDTO;
import vn.vnpt.service.dto.DichVuKhamCriteria;
import vn.vnpt.service.DichVuKhamQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link vn.vnpt.domain.DichVuKham}.
 */
@RestController
@RequestMapping("/api")
public class DichVuKhamResource {

    private final Logger log = LoggerFactory.getLogger(DichVuKhamResource.class);

    private static final String ENTITY_NAME = "khamchuabenhDichVuKham";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DichVuKhamService dichVuKhamService;

    private final DichVuKhamQueryService dichVuKhamQueryService;

    public DichVuKhamResource(DichVuKhamService dichVuKhamService, DichVuKhamQueryService dichVuKhamQueryService) {
        this.dichVuKhamService = dichVuKhamService;
        this.dichVuKhamQueryService = dichVuKhamQueryService;
    }

    /**
     * {@code POST  /dich-vu-khams} : Create a new dichVuKham.
     *
     * @param dichVuKhamDTO the dichVuKhamDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new dichVuKhamDTO, or with status {@code 400 (Bad Request)} if the dichVuKham has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/dich-vu-khams")
    public ResponseEntity<DichVuKhamDTO> createDichVuKham(@Valid @RequestBody DichVuKhamDTO dichVuKhamDTO) throws URISyntaxException {
        log.debug("REST request to save DichVuKham : {}", dichVuKhamDTO);
        if (dichVuKhamDTO.getId() != null) {
            throw new BadRequestAlertException("A new dichVuKham cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DichVuKhamDTO result = dichVuKhamService.save(dichVuKhamDTO);
        return ResponseEntity.created(new URI("/api/dich-vu-khams/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /dich-vu-khams} : Updates an existing dichVuKham.
     *
     * @param dichVuKhamDTO the dichVuKhamDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated dichVuKhamDTO,
     * or with status {@code 400 (Bad Request)} if the dichVuKhamDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the dichVuKhamDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/dich-vu-khams")
    public ResponseEntity<DichVuKhamDTO> updateDichVuKham(@Valid @RequestBody DichVuKhamDTO dichVuKhamDTO) throws URISyntaxException {
        log.debug("REST request to update DichVuKham : {}", dichVuKhamDTO);
        if (dichVuKhamDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DichVuKhamDTO result = dichVuKhamService.save(dichVuKhamDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /dich-vu-khams} : get all the dichVuKhams.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of dichVuKhams in body.
     */
    @GetMapping("/dich-vu-khams")
    public ResponseEntity<List<DichVuKhamDTO>> getAllDichVuKhams(DichVuKhamCriteria criteria, Pageable pageable) {
        log.debug("REST request to get DichVuKhams by criteria: {}", criteria);
        Page<DichVuKhamDTO> page = dichVuKhamQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /dich-vu-khams/count} : count all the dichVuKhams.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/dich-vu-khams/count")
    public ResponseEntity<Long> countDichVuKhams(DichVuKhamCriteria criteria) {
        log.debug("REST request to count DichVuKhams by criteria: {}", criteria);
        return ResponseEntity.ok().body(dichVuKhamQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /dich-vu-khams/:id} : get the "id" dichVuKham.
     *
     * @param id the id of the dichVuKhamDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the dichVuKhamDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/dich-vu-khams/{id}")
    public ResponseEntity<DichVuKhamDTO> getDichVuKham(@PathVariable Long id) {
        log.debug("REST request to get DichVuKham : {}", id);
        Optional<DichVuKhamDTO> dichVuKhamDTO = dichVuKhamService.findOne(id);
        return ResponseUtil.wrapOrNotFound(dichVuKhamDTO);
    }

    /**
     * {@code DELETE  /dich-vu-khams/:id} : delete the "id" dichVuKham.
     *
     * @param id the id of the dichVuKhamDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/dich-vu-khams/{id}")
    public ResponseEntity<Void> deleteDichVuKham(@PathVariable Long id) {
        log.debug("REST request to delete DichVuKham : {}", id);
        dichVuKhamService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

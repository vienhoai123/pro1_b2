package vn.vnpt.web.rest;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import vn.vnpt.service.DotThayDoiMaDichVuQueryService;
import vn.vnpt.service.DotThayDoiMaDichVuService;
import vn.vnpt.service.dto.DotThayDoiMaDichVuCriteria;
import vn.vnpt.service.dto.DotThayDoiMaDichVuDTO;
import vn.vnpt.web.rest.errors.BadRequestAlertException;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link vn.vnpt.domain.DotThayDoiMaDichVu}.
 */
@RestController
@RequestMapping("/api")
public class DotThayDoiMaDichVuResource {

    private static final String ENTITY_NAME = "khamchuabenhDotThayDoiMaDichVu";
    private final Logger log = LoggerFactory.getLogger(DotThayDoiMaDichVuResource.class);
    private final DotThayDoiMaDichVuService dotThayDoiMaDichVuService;
    private final DotThayDoiMaDichVuQueryService dotThayDoiMaDichVuQueryService;
    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    public DotThayDoiMaDichVuResource(DotThayDoiMaDichVuService dotThayDoiMaDichVuService, DotThayDoiMaDichVuQueryService dotThayDoiMaDichVuQueryService) {
        this.dotThayDoiMaDichVuService = dotThayDoiMaDichVuService;
        this.dotThayDoiMaDichVuQueryService = dotThayDoiMaDichVuQueryService;
    }

    /**
     * {@code POST  /dot-thay-doi-ma-dich-vus} : Create a new dotThayDoiMaDichVu.
     *
     * @param dotThayDoiMaDichVuDTO the dotThayDoiMaDichVuDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new dotThayDoiMaDichVuDTO, or with status {@code 400 (Bad Request)} if the dotThayDoiMaDichVu has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/dot-thay-doi-ma-dich-vus")
    public ResponseEntity<DotThayDoiMaDichVuDTO> createDotThayDoiMaDichVu(@Valid @RequestBody DotThayDoiMaDichVuDTO dotThayDoiMaDichVuDTO) throws URISyntaxException {
        log.debug("REST request to save DotThayDoiMaDichVu : {}", dotThayDoiMaDichVuDTO);
        if (dotThayDoiMaDichVuDTO.getId() != null) {
            throw new BadRequestAlertException("A new dotThayDoiMaDichVu cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DotThayDoiMaDichVuDTO result = dotThayDoiMaDichVuService.save(dotThayDoiMaDichVuDTO);
        return ResponseEntity.created(new URI("/api/dot-thay-doi-ma-dich-vus/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /dot-thay-doi-ma-dich-vus} : Updates an existing dotThayDoiMaDichVu.
     *
     * @param dotThayDoiMaDichVuDTO the dotThayDoiMaDichVuDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated dotThayDoiMaDichVuDTO,
     * or with status {@code 400 (Bad Request)} if the dotThayDoiMaDichVuDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the dotThayDoiMaDichVuDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/dot-thay-doi-ma-dich-vus")
    public ResponseEntity<DotThayDoiMaDichVuDTO> updateDotThayDoiMaDichVu(@Valid @RequestBody DotThayDoiMaDichVuDTO dotThayDoiMaDichVuDTO) throws URISyntaxException {
        log.debug("REST request to update DotThayDoiMaDichVu : {}", dotThayDoiMaDichVuDTO);
        if (dotThayDoiMaDichVuDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DotThayDoiMaDichVuDTO result = dotThayDoiMaDichVuService.save(dotThayDoiMaDichVuDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, dotThayDoiMaDichVuDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /dot-thay-doi-ma-dich-vus} : get all the dotThayDoiMaDichVus.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of dotThayDoiMaDichVus in body.
     */
    @GetMapping("/dot-thay-doi-ma-dich-vus")
    public ResponseEntity<List<DotThayDoiMaDichVuDTO>> getAllDotThayDoiMaDichVus(DotThayDoiMaDichVuCriteria criteria, Pageable pageable) {
        log.debug("REST request to get DotThayDoiMaDichVus by criteria: {}", criteria);
        Page<DotThayDoiMaDichVuDTO> page = dotThayDoiMaDichVuQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /dot-thay-doi-ma-dich-vus/count} : count all the dotThayDoiMaDichVus.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/dot-thay-doi-ma-dich-vus/count")
    public ResponseEntity<Long> countDotThayDoiMaDichVus(DotThayDoiMaDichVuCriteria criteria) {
        log.debug("REST request to count DotThayDoiMaDichVus by criteria: {}", criteria);
        return ResponseEntity.ok().body(dotThayDoiMaDichVuQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /dot-thay-doi-ma-dich-vus/:id} : get the "id" dotThayDoiMaDichVu.
     *
     * @param id the id of the dotThayDoiMaDichVuDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the dotThayDoiMaDichVuDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/dot-thay-doi-ma-dich-vus/{id}")
    public ResponseEntity<DotThayDoiMaDichVuDTO> getDotThayDoiMaDichVu(@PathVariable Long id) {
        log.debug("REST request to get DotThayDoiMaDichVu : {}", id);
        Optional<DotThayDoiMaDichVuDTO> dotThayDoiMaDichVuDTO = dotThayDoiMaDichVuService.findOne(id);
        return ResponseUtil.wrapOrNotFound(dotThayDoiMaDichVuDTO);
    }

    /**
     * {@code DELETE  /dot-thay-doi-ma-dich-vus/:id} : delete the "id" dotThayDoiMaDichVu.
     *
     * @param id the id of the dotThayDoiMaDichVuDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/dot-thay-doi-ma-dich-vus/{id}")
    public ResponseEntity<Void> deleteDotThayDoiMaDichVu(@PathVariable Long id) {
        log.debug("REST request to delete DotThayDoiMaDichVu : {}", id);
        dotThayDoiMaDichVuService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

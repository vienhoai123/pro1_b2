package vn.vnpt.web.rest;

import vn.vnpt.service.QuanHuyenService;
import vn.vnpt.web.rest.errors.BadRequestAlertException;
import vn.vnpt.service.dto.QuanHuyenDTO;
import vn.vnpt.service.dto.QuanHuyenCriteria;
import vn.vnpt.service.QuanHuyenQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link vn.vnpt.domain.QuanHuyen}.
 */
@RestController
@RequestMapping("/api")
public class QuanHuyenResource {

    private final Logger log = LoggerFactory.getLogger(QuanHuyenResource.class);

    private static final String ENTITY_NAME = "khamchuabenhQuanHuyen";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final QuanHuyenService quanHuyenService;

    private final QuanHuyenQueryService quanHuyenQueryService;

    public QuanHuyenResource(QuanHuyenService quanHuyenService, QuanHuyenQueryService quanHuyenQueryService) {
        this.quanHuyenService = quanHuyenService;
        this.quanHuyenQueryService = quanHuyenQueryService;
    }

    /**
     * {@code POST  /quan-huyens} : Create a new quanHuyen.
     *
     * @param quanHuyenDTO the quanHuyenDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new quanHuyenDTO, or with status {@code 400 (Bad Request)} if the quanHuyen has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/quan-huyens")
    public ResponseEntity<QuanHuyenDTO> createQuanHuyen(@Valid @RequestBody QuanHuyenDTO quanHuyenDTO) throws URISyntaxException {
        log.debug("REST request to save QuanHuyen : {}", quanHuyenDTO);
        if (quanHuyenDTO.getId() != null) {
            throw new BadRequestAlertException("A new quanHuyen cannot already have an ID", ENTITY_NAME, "idexists");
        }
        QuanHuyenDTO result = quanHuyenService.save(quanHuyenDTO);
        return ResponseEntity.created(new URI("/api/quan-huyens/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /quan-huyens} : Updates an existing quanHuyen.
     *
     * @param quanHuyenDTO the quanHuyenDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated quanHuyenDTO,
     * or with status {@code 400 (Bad Request)} if the quanHuyenDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the quanHuyenDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/quan-huyens")
    public ResponseEntity<QuanHuyenDTO> updateQuanHuyen(@Valid @RequestBody QuanHuyenDTO quanHuyenDTO) throws URISyntaxException {
        log.debug("REST request to update QuanHuyen : {}", quanHuyenDTO);
        if (quanHuyenDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        QuanHuyenDTO result = quanHuyenService.save(quanHuyenDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, quanHuyenDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /quan-huyens} : get all the quanHuyens.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of quanHuyens in body.
     */
    @GetMapping("/quan-huyens")
    public ResponseEntity<List<QuanHuyenDTO>> getAllQuanHuyens(QuanHuyenCriteria criteria) {
        log.debug("REST request to get QuanHuyens by criteria: {}", criteria);
        List<QuanHuyenDTO> entityList = quanHuyenQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
     * {@code GET  /quan-huyens/count} : count all the quanHuyens.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/quan-huyens/count")
    public ResponseEntity<Long> countQuanHuyens(QuanHuyenCriteria criteria) {
        log.debug("REST request to count QuanHuyens by criteria: {}", criteria);
        return ResponseEntity.ok().body(quanHuyenQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /quan-huyens/:id} : get the "id" quanHuyen.
     *
     * @param id the id of the quanHuyenDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the quanHuyenDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/quan-huyens/{id}")
    public ResponseEntity<QuanHuyenDTO> getQuanHuyen(@PathVariable Long id) {
        log.debug("REST request to get QuanHuyen : {}", id);
        Optional<QuanHuyenDTO> quanHuyenDTO = quanHuyenService.findOne(id);
        return ResponseUtil.wrapOrNotFound(quanHuyenDTO);
    }

    /**
     * {@code DELETE  /quan-huyens/:id} : delete the "id" quanHuyen.
     *
     * @param id the id of the quanHuyenDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/quan-huyens/{id}")
    public ResponseEntity<Void> deleteQuanHuyen(@PathVariable Long id) {
        log.debug("REST request to delete QuanHuyen : {}", id);
        quanHuyenService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

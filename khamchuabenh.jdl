
entity BenhAnKhamBenh {
	id Long required,
	/** Trạng thái bệnh nhân đã tiếp nhận
1: Bệnh nhân cũ
0: Bệnh nhân mới */
	benhNhanCu Boolean required,
	/** Cảnh báo còn thuộc khi bệnh nhân khám BHYT */
	canhBao Boolean required,
	/** Trạng thái cấp cứu của bệnh nhân
1: Cấp cứu
0: Bình thường */
	capCuu Boolean required,
	/** Trạng thái chỉ có năm sinh của bệnh nhân
1: Chỉ có năm sinh
0: Đủ thông tin */
	chiCoNamSinh Boolean required,
	/** Thông tin chứng minh nhân dân của bệnh nhân */
	cmndBenhNhan String maxlength(20),
	/** Thông tin chứng minh nhân dân của người nhà bệnh nhân */
	cmndNguoiNha String maxlength(20),
	/** Trạng thái có bảo hiểm của */
	coBaoHiem Boolean required,
	/** Thông tin địa chỉ của bệnh nhân */
	diaChi String required maxlength(500),
	/** Thông tin email của bệnh nhân */
	email String maxlength(255),
	/** Trạng thái giới tính của bệnh nhân:

1: Nam
.
0: Nữ. */
	gioiTinh Boolean required,
	/** Thông tin kháng thể của bệnh nhân */
	khangThe String maxlength(5),
	/** Lần khám trong ngày của bệnh nhân */
	lanKhamTrongNgay Boolean,
	/** Mã loại giấy tờ của trẻ em như giấy khai sinh, giấy chứng sinh */
	loaiGiayToTreEm Boolean,
	/** Thông tin ngày cấp CMND của bệnh nhân */
	ngayCapCmnd LocalDate,
	/** Thời gian miễn cùng chi trả của thẻ BHYT */
	ngayMienCungChiTra LocalDate,
	/** Thôi tin ngày sinh của bệnh nhân */
	ngaySinh LocalDate required,
	/** Thông tin nhóm máu cảu bệnh nhân */
	nhomMau String maxlength(5),
	/** Thông tin nơi cấp của bệnh nhân */
	noiCapCmnd String maxlength(500),
	/** Thông tin nơi làm việc của bệnh nhân */
	noiLamViec String maxlength(500),
	/** Thông tin số điện thoại của bệnh nhân */
	phone String maxlength(15),
	/** Thông tin số điện thoại người nhà */
	phoneNguoiNha String maxlength(15),
	/** Thông tin quốc tịch của bệnh nhân */
	quocTich String maxlength(200),
	/** Tên bệnh nhân */
	tenBenhNhan String required maxlength(200),
	/** Tên người nhà bệnh nhân */
	tenNguoiNha String maxlength(200),
	/** Thông tin tháng tuổi nếu là bệnh nhân nhi */
	thangTuoi Integer,
	/** Thông tin thời gian tiếp nhận bệnh nhân */
	thoiGianTiepNhan LocalDate required,
	/** Thông tin buổi của bệnh nhân */
	tuoi Integer,
	/** Trạng thái ưu tiên của bệnh nhân:
1: Ưu tiên
0: Không ưu tiên */
	uuTien Boolean required,
	/** Mã bệnh nhân theo công thức UUID */
	uuid String maxlength(255),
	/** Trạng thái của Bệnh án khám bệnh:
1: Chờ khám, mới vào khoa
2: Đang khám, đang điều trị
3: Đã khám, xuất viện
4: Chuyển khoa phòng khám
5: Chuyển viện, chuyển tuyến
6: Nhập viện,
7: Tử vong;
8: trốn viện;
*/
	trangThai Boolean required,
	/** Mã khoa hoàn tất khám */
	maKhoaHoanTatKham Integer,
	/** Mã phòng hoàn tất khám */
	maPhongHoanTatKham Integer,
	/** Tên khoa hoàn tất khám */
	tenKhoaHoanTatKham String maxlength(200),
	/** Tên phòng hoàn tất khám */
	tenPhongHoanTatKham String maxlength(200),
	/** Thời gian hoàn tất khám */
	thoiGianHoanTatKham LocalDate,
	/** Số bệnh án */
	soBenhAn String,
    /** Số bệnh án theo khoa */
    soBenhAnKhoa String,
	nam Integer required
}

/** NDB_TABLE=READ_BACKUP=1 Bảng chức thông tin bệnh lý */
entity BenhLy {
	id Long required,
	/** Trạng thái của 1 dòng thông tin bệnh lý

1: Có hiệu lực.

0: Không có hiệu lực. */
	enabled Boolean required,
	/** Mã icd */
	icd String required maxlength(200),
	/** Mô tả icd bằng tiếng Việt */
	moTa String required maxlength(500),
	/** Mô tả icd bằng tiếng Anh */
	moTaTiengAnh String maxlength(500),
	/** Tên không dấu của icd */
	tenKhongDau String maxlength(500),
	/** Mã viết tắt của icd */
	vietTat String maxlength(100),
	/** Mã bệnh theo VNCode */
	vncode String required
}

/** NDB_TABLE=READ_BACKUP=1 Đợt điều trị theo  */
entity DotDieuTri {
	id Long required,
	/** */
	soThuTu String,
	/** Giá tiền cận trên BHYT */
	bhytCanTren Integer,
	/** Giá tiền cận trên BHYT quy định cho các Dịch vụ kỹ thuật cao */
	bhytCanTrenKtc Integer,
	/** Thông tin mã khu vực của thẻ BHYT */
	bhytMaKhuVuc String,
	/** Thời gian có hiệu lực của BHYT */
	bhytNgayBatDau LocalDate,
	/** Thời gian được hưởng quyền lợi bảo hiểm 5 năm */
	bhytNgayDuNamNam LocalDate,
	/** Thời gian hết hạn của thẻ BHYT */
	bhytNgayHetHan LocalDate,
	/** Trạng thái nội tỉnh của thẻ BHYT:
1: Nội tỉnh
0: Ngoại tỉnh */
	bhytNoiTinh Boolean required,
	/** Mã thẻ BHYT */
	bhytSoThe String maxlength(30),
	/** Tỷ lệ miễn giảm của thẻ BHYT */
	bhytTyLeMienGiam Integer,
	/** Tỷ lệ miễn giảm cho các dịch vụ kỹ thuật cao của thẻ BHYT */
	bhytTyLeMienGiamKtc Integer,
	/** Trạng thái có bảo hiểm của */
	coBaoHiem Boolean required,
	/** Thông tin địa chỉ BHYT của bệnh nhân */
	bhytDiaChi String required maxlength(500),
	/** Tên đối tượng của thẻ BHYT */
	doiTuongBhytTen String maxlength(200),
	/** Trạng thái đúng tuyến của thẻ BHYT
1: Đúng tuyến
0: Không đúng tuyến */
	dungTuyen Boolean required,
	/** Mã giấy chứng sinh, giấy khai sinh.... */
	giayToTreEm String maxlength(200),
	/** Mã loại giấy tờ của trẻ em như giấy khai sinh, giấy chứng sinh */
	loaiGiayToTreEm Boolean,
	/** Thời gian miễn cùng chi trả của thẻ BHYT */
	ngayMienCungChiTra LocalDate,
	/** Thông tin thẻ TEKT */
	theTreEm Boolean required,
	/** Trạng thái thông tuyến theo bhxh xml 4210:
1: Thông tuyến
0: không thông tuyến */
	thongTuyenBhxhXml4210 Boolean required,
	/** Trạng thái của Bệnh án khám bệnh:
0: mới vào khoa, chờ khám;
1: Điều trị, khám có giường;
2: Điều trị, khám không giường;
3: Xuất viện, hoàn tất khám;
4: chuyển tuyến, chuyển viện;
5: Nhập viện;
6: tử vong;
7: trốn viện;
8: Kết thúc đợt ĐT, thêm đợt ĐT mới;

	*/
	trangThai Boolean required,
	/** Loại của bệnh án 1: ngoại trú. 2: Nội trú, 3: điều trị bệnh án ngoại trú*/
	loai Integer,
	/** Nơi đăng ký khám chữa bệnh ban đầu */
	bhytNoiDkKcbbd String maxlength(30),
	nam Integer required
}

/**
 * The ThongTinKhoa entity.
 */
entity ThongTinKhoa {
	id Long required,
	/** Thời gian Khoa nhận bệnh */
	thoiGianNhanBenh LocalDate,
	/** Thời gian chuyển bệnh nhân chuyển sang Khoa khác*/
	thoiGianChuyenKhoa LocalDate,
	/** Mã Khoa phòng hiện tại */
	 khoaId Integer,
	 /** khoaChuyenDen Mã khoa chuyển bệnh nhân đến */
	 khoaChuyenDen Integer,
	 /** khoaChuyenDi Mã khoa chuyển đi*/
	 khoaChuyenDi Integer,
	 /** nhanVienNhanBenh nhân viên nhận bệnh*/
	 nhanVienNhanBenh Integer,
	 /** nhanVienChuyenKhoa */
	 nhanVienChuyenKhoa Integer,
	 /** soNgay: số ngày nằm viện trong khoa */
	 soNgay BigDecimal,
	 /** trangThai:
	 0: mới vào khoa, chờ khám;
	 1: điều trị, khám bệnh có giường;
	 2: điều trị, khám bệnh không giường;
	 3: xuất viện, hoàn tất khám;
	 4: Chuyển khoa phòng khám
	 5: Chuyển viện, chuyển tuyến;
	 6: Nhập viện,
	 7: Tử vong
	 8: Kết thúc đợt ĐT, thêm đợt ĐT mới;
	  */
	 trangThai Integer,
	 nam Integer required
}

entity BenhNhan {
	id Long required,
	/** Trạng thái thông tin bệnh nhân chỉ có năm sinh:
1: Chỉ có năm sinh.
0: Đầy đủ ngày tháng năm sinh. */
	chiCoNamSinh Boolean required,
	/** Thông tin chứng minh nhân dân của bệnh nhân */
	cmnd String maxlength(20),
	/** Thông tin địa chỉ của bệnh nhân trên thẻ BHYT */
	diaChiBhyt String required maxlength(500),
	/** Thông tin địa chỉ email của bệnh nhân */
	email String maxlength(255),
	/** Trạng thái của 1 dòng thông tin bệnh nhân

1: Có hiệu lực.

0: Không có hiệu lực. */
	enabled Boolean required,
	/** Trạng thái giới tính của bệnh nhân:

1: Nam
.
0: Nữ. */
	gioiTinh Boolean required,
	/** Thông tin kháng thể của bệnh nhân */
	khangThe String maxlength(5),
	/** Thông tin mã số thuế của bệnh nhân */
	maSoThue String maxlength(100),
	/** Ngày cấp chứng minh nhân dân của bệnh nhân */
	ngayCapCmnd LocalDate,
	/** Thông tin ngày sinh của bệnh nhân */
	ngaySinh LocalDate required,
	/** Thông tin nhóm máu của bệnh nhân */
	nhomMau String maxlength(5),
	/** Thông tin nơi cấp chứng minh nhân dân */
	noiCapCmnd String maxlength(500),
	/** Thông tin nơi làm việc của bệnh nhân */
	noiLamViec String maxlength(500),
	/** Thông tin số điện thoại của bệnh nhân */
	phone String maxlength(15),
	/** Tên bệnh nhân */
	ten String required maxlength(200),
	/** Số nhà của bệnh nhân */
	soNhaXom String maxlength(255),
	/** Thông tin ấp, thôn, xóm của bệnh nhân */
	apThon String maxlength(255),
	/** Mã danh mục địa phương */
	diaPhuongId Long,
	/** Thông tin địa chỉ thường trú của bệnh nhân */
	diaChiThuongTru String required maxlength(500)
}

/** NDB_TABLE=READ_BACKUP=1 Danh mục tên bệnh lý theo Y học cổ truyền */
entity BenhYhct {
	id Long required,
	/** Mã bệnh lý YHCT */
	ma String required maxlength(50),
	/** Tên YHCT */
	ten String required maxlength(500)
}

/** NDB_TABLE=READ_BACKUP=1 Thông tin chức danh của user */
entity ChucDanh {
	id Integer required,
	/** Mô tả chức danh */
	moTa String maxlength(500),
	/** Tên chức danh */
	ten String required maxlength(500)
}

entity ChucVu {
	id Long required,
	moTa String maxlength(500),
	ten String required maxlength(500)
}

/** NDB_TABLE=READ_BACKUP=1 Thông tin các nước trên thế giới */
entity Country {
	id Integer required,
	/** Mã 2 ký tự của quốc gia theo ISO */
	isoCode String maxlength(255),
	/** Mã theo số của quốc gia theo ISO */
	isoNumeric String  maxlength(255),
	/** Tên quốc gia */
	name String maxlength(255),
	/** Mã cục thống kê */
	maCtk Integer,
	/** Tên cục thống kê */
	tenCtk String maxlength(255)
}

/** NDB_TABLE=READ_BACKUP=1 Thông tin theo tên dân tộc */
entity DanToc {
	id Integer required,
	/** Mã dân tộc */
	ma4069Byt Integer required,
	/** Mã dân tộc theo cục thống kê */
	maCucThongKe Integer required,
	/** Tên dân tộc */
	ten4069Byt String required maxlength(500),
	/** Tên dân tộc theo cục thống kê */
	tenCucThongKe String required maxlength(500)
}


entity DoiTuongBhyt {
	id Integer required,
	/** Cận trên BHYT */
	canTren Integer required,
	/** Cận trên kỹ thuật cao */
	canTrenKtc Integer required,
	/** Mã đối tượng BHYT */
	ma String required maxlength(10),
	/** Tên đối tượng */
	ten String required maxlength(500),
	/** Tỷ lệ miễn giảm */
	tyLeMienGiam Integer required,
	/** Tỷ lệ miển giảm kỹ thuật cao */
	tyLeMienGiamKtc Integer required
}


entity DonVi {
	id Integer required,
	/** Cấp đơn vị */
	cap Integer required,
	/** Thông tin chi nhánh ngân hàng */
	chiNhanhNganHang String maxlength(300),
	/** Địa chỉ của đơn vị */
	diaChi String maxlength(1000),
	/** Mã đơn vị quản lý */
	donViQuanLyId BigDecimal,
	/** Đơn vị trực thuộc */
	dvtt String maxlength(10),
	/** email */
	email String maxlength(50),
	/** Trạng thái hiệu lực của dòng dữ liệu: 1: Có hiệu lực. 0: Không có hiệu lực */
	enabled Boolean required,
	/** Ký hiệu của đơn vị */
	kyHieu String maxlength(100),
	/** Loại của đơn vị */
	loai Integer required,
	/** Mã tỉnh */
	maTinh String required maxlength(10),
	/** Số điện thoại */
	phone String maxlength(15),
	/** Số tài khoản của đơn vị */
	soTaiKhoan String maxlength(30),
	/** Tên của đơn vị */
	ten String required maxlength(300),
	/** Tên thông tin ngân hàng */
	tenNganHang String maxlength(300)
}

entity DotGiaDichVuBhxh {
	id Integer required,
	doiTuongApDung BigDecimal required,
	ghiChu String maxlength(1000),
	ngayApDung LocalDate required,
	ten String required maxlength(500)
}


entity HuongDieuTri {
	id Integer required,
	/** Trạng thái hướng điều trị: 0: Cả nội và ngoại trú. 1: Ngoại trú, 2: Nội trú */
	noiTru Boolean required,
	/** Tên hướng điều trị */
	ten String maxlength(200),
	/** Mã hướng điều trị */
	ma Boolean required
}

entity Khoa {
	id Integer required,
	/** Cấp khoa */
	cap Integer required,
	/** Trạng thái: 1: Còn hiệu lực. 0 Hết hiệu lực */
	enabled Boolean required,
	/** Ký hiệu khoa */
	kyHieu String required maxlength(50),
	/** Số điện thoại khoa */
	phone String maxlength(15),
	/** Tên khoa */
	ten String required maxlength(200)
}


entity LoaiBenhLy {
	id Integer required,
	/** Ký hiệu bệnh lý */
	kyHieu String maxlength(50),
	/** Mô tả bệnh lý */
	moTa String required maxlength(500),
	/** Tên bệnh lý */
	ten String maxlength(500),
	/** Tên tiếng anh của bệnh lý */
	tenTiengAnh String required maxlength(500)
}

entity Menu {
	id Integer required,
	/** Trạng thái có hiệu lực của dịch vụ:
1: Còn hiệu lực.
0: Đã ẩn. */
	enabled Boolean required,
	/** Cấp của menu */
	level Boolean required,
	/** Tên hiển thị của menu */
	name String required maxlength(255),
	/** Đường dẫn của menu */
	uri String maxlength(255),
	/** Mã menu cha */
	parent Integer,
	/** Mã đơn vị */
	donViId Long required
}

/** NDB_TABLE=READ_BACKUP=1 Thông tin nghề nghiệp theo công văn 4069 */
entity NgheNghiep {
	id Integer required,
	/** Mã nghề nghiệp 4069 */
	ma4069 Integer required,
	/** Tên nghề nghiệp 4069 */
	ten4069 String required maxlength(500)
}


entity NhanVien {
	id Integer required,
	/** Tên nhân vien */
	ten String required maxlength(300),
	/** Chứng chỉ hành nghề */
	chungChiHanhNghe String required maxlength(255),
	/** CMND của nhân viên */
	cmnd String required maxlength(30),
	/** 0: Nữ. 1 Nam */
	gioiTinh Boolean required,
	/** Ngày sinh */
	ngaySinh LocalDate required
}


entity NhomBenhLy {
	id Integer required,
	/** Mô tả nhóm bệnh lý */
	moTa String maxlength(500),
	/** Tên nhóm bệnh lý */
	ten String required maxlength(500)
}

entity NhomDoiTuongBhyt {
	id Integer required,
	/** Tên nhóm đối tượng BHYT */
	ten String required maxlength(500)
}


entity Phong {
	id Integer required,
	/** Trạng thái: 1: Còn hiệu lực. 0 Hết hiệu lực */
	enabled Boolean required,
	/** Ký hiệu phòng */
	kyHieu String required maxlength(500),
	/** Loại phòng */
	loai BigDecimal required,
	/** Số điện thoại của phòng */
	phone String maxlength(15),
	/** Tên phòng */
	ten String required maxlength(500),
	/** Vị trí phòng */
	viTri String maxlength(500)
}

/** NDB_TABLE=READ_BACKUP=1 Thông tin quận huyện */
entity PhuongXa {
	id Integer required,
	/** Cấp */
	cap Boolean required,
	/** Đoạn ký tự gợi ý */
	guessPhrase String maxlength(255),
	/** Tên xã */
	ten String required maxlength(255),
	/** Tên không dấu */
	tenKhongDau String required maxlength(255)
}


entity QuanHuyen {
	id Integer required,
	/** Cấp */
	cap String required maxlength(255),
	/** Đoạn cụm từ gợi ý */
	guessPhrase String maxlength(255),
	/** Tên quận huyện */
	ten String required maxlength(255),
	/** Tên không dấu */
	tenKhongDau String required maxlength(255)
}


entity TBenhLyKhamBenh {
	benhLyId Integer required,
	ttkbId Long required
}


entity TPhongNhanVien {
	enabled Boolean required
}


entity TheBhyt {
	id Long required,
	/** Trạng thái có hiệu lực của dịch vụ:
1: Còn hiệu lực.
0: Đã ẩn. */
	enabled Integer required,
	/** Mã khu vực */
	maKhuVuc String maxlength(500),
	/** Mã số BHXH */
	maSoBhxh String maxlength(30),
	/** Mã thẻ cũ */
	maTheCu String maxlength(30),
	/** Ngày bắt đầu có hiệu lực của thẻ */
	ngayBatDau LocalDate required,
	/** Ngày hưởng quyền lợi đủ 5 năm */
	ngayDuNamNam LocalDate required,
	/** Ngày thẻ hết hạn */
	ngayHetHan LocalDate required,
	/** Mã QR */
	qr String maxlength(1000),
	/** Mã số thẻ */
	soThe String required maxlength(30),
	/** Ngày miễn cũng chi trả */
	ngayMienCungChiTra LocalDate
}


entity ThongTinBhxh {
	id Long required,
	/** Mã đơn vị trực thuộc */
	dvtt String required maxlength(10),
	/** Trạng thái có hiệu lực: 1: Có hiệu lực: 0: Không có hiệu lực */
	enabled Boolean required,
	/** Ngày áp dụng BHXH */
	ngayApDungBhyt LocalDate required,
	/** Tên đơn vị theo BHXH */
	ten String required maxlength(500)
}


entity ThongTinKhamBenh {
	id Long required,
	/** Chỉ số BMI */
	bmi BigDecimal,
	/** Cân nặng */
	canNang BigDecimal,
	/** Chiều cao (tính bằng cm) */
	chieuCao Integer,
	/** Creatinin */
	creatinin BigDecimal,
	/** Dấu hiệu lâm sàng */
	dauHieuLamSang String maxlength(500),
	/** Độ thành thải Creatinin */
	doThanhThai BigDecimal,
	/** Huyết áp cao */
	huyetApCao Integer,
	/** Huyết áp thấp */
	huyetApThap Integer,
	/** ICD */
	icd String maxlength(50),
	/** Lần hẹn */
	lanHen Boolean,
	/** Lời dặn */
	loiDan String maxlength(500),
	/** Lý do chuyển */
	lyDoChuyen String maxlength(2000),
	/** Mã bệnh y học cổ truyền */
	maBenhYhct String maxlength(100),
	/** Mạch */
	mach Integer,
	/** Ngày ra toa tối đa */
	maxNgayRaToa Integer,
	/** Ngày hẹn */
	ngayHen LocalDate,
	/** Nhận định BMI */
	nhanDinhBmi String maxlength(500),
	/** Nhận định độ thanh thải */
	nhanDinhDoThanhThai String maxlength(500),
	/** Trận thái nhập viện. 0: Không nhập viện. 1: Nhập viện */
	nhapVien Boolean required,
	/** Nhiệt độ */
	nhietDo BigDecimal,
	/** Nhiệp thở */
	nhipTho Integer,
	/** Phương pháp điều trị y học cổ truyền */
	ppDieuTriYtct String maxlength(2000),
	/** Số lần in bảng kê */
	soLanInBangKe Integer,
	/** Số lần in toa thuốc */
	soLanInToaThuoc Integer,
	/** Tên bệnh theo icd */
	tenBenhIcd String maxlength(500),
	/** Tên bệnh theo bác sĩ */
	tenBenhTheoBacSi String maxlength(500),
	/** Tên bệnh y học cổ truyền */
	tenBenhYhct String maxlength(500),
	/** Trạng thái đã chẩn đoán hình ảnh: 0: Chưa thực hiện đủ: 1: Đã thực hiện đủ */
	trangThaiCdha Boolean required,
	/** Trạng thái đã thực hiện ttpt: 0: Chưa thực hiện đủ: 1: Đã thực hiện đủ */
	trangThaiTtpt Boolean required,
	/** Trạng thái đã xét nghiệm: 0: Chưa thực hiện đủ: 1: Đã thực hiện đủ */
	trangThaiXetNghiem Boolean required,
	/** Triệu chứng lâm sàng */
	trieuChungLamSang String maxlength(255),
	/** Vòng bụng */
	vongBung Integer,
	/** Trạng thái lần khám bệnh; 1 chờ khám; 2 đang khám, 3 đã khám, 4 kết thúc bắt buộc, 5 chuyển phòng khám, 6 chuyển viện, 7 nhập viện nội trú từ ngoại trú */
	trangThai Boolean required,
	/** Thời gian kết thúc khám */
	thoiGianKetThucKham LocalDate,
	/** Mã phòng chuyển từ */
	phongIdChuyenTu Long,
	/** Thời gian khám bệnh */
	thoiGianKhamBenh LocalDate,
	/** Nội dung y lệnh */
	yLenh String maxlength(255),
	/** Loại y lệnh: 0: Bình thường. 1: Tây y: 2 Đông Y */
	loaiYLenh Boolean,
	/** Chẩn đoán */
	chanDoan String maxlength(255),
	/** Loại chẩn đoán: 0: Bình thường. 1: Tây y: 2 Đông Y */
	loaiChanDoan Boolean,
	nam Integer required
}

entity TinhThanhPho {
	id Integer required,
	/** Cấp */
	cap String required maxlength(255),
	/** Đoạn gợi ý */
	guessPhrase String maxlength(255),
	/** Tên tỉnh thành phố */
	ten String required maxlength(255),
	/** Tên không dấu */
	tenKhongDau String required maxlength(255)
}


entity UserExtra {
	id Long required,
	/** Trạng thái: 1: Còn hiệu lực. 0 Hết hiệu lực */
	enabled Boolean required,
	/** username */
	username String required maxlength(100)
}

entity UserType {
	id Integer required,
	/** Mô tả loại user */
	moTa String maxlength(255)
}


// Relations
// NDB_TABLE=READ_BACKUP=1
relationship ManyToOne {
	BenhAnKhamBenh{benhNhan required} to BenhNhan
}

// NDB_TABLE=READ_BACKUP=1
relationship ManyToOne {
	BenhAnKhamBenh{donVi required} to DonVi
}

// NDB_TABLE=READ_BACKUP=1
relationship ManyToOne {
	/** Mã nhân viên tiếp nhận */
	BenhAnKhamBenh{nhanVienTiepNhan required} to NhanVien
}

// NDB_TABLE=READ_BACKUP=1
relationship ManyToOne {
	/** Id phòng hoàn tất khám */
	BenhAnKhamBenh{phongTiepNhan required} to Phong
}

// NDB_TABLE=READ_BACKUP=1 Bảng chức thông tin bệnh lý
relationship ManyToOne {
	/** Mã nhóm bệnh lý */
	BenhLy{nhomBenhLy required} to NhomBenhLy
}

// NDB_TABLE=READ_BACKUP=1
relationship ManyToOne {
	/** Mã dân tộc */
	BenhNhan{danToc} to DanToc
}

// NDB_TABLE=READ_BACKUP=1
relationship ManyToOne {
	/** Mã nghề nghiệp */
	BenhNhan{ngheNghiep} to NgheNghiep
}

// NDB_TABLE=READ_BACKUP=1
relationship ManyToOne {
	/** Mã quốc tịch */
	BenhNhan{quocTich required} to Country
}

// NDB_TABLE=READ_BACKUP=1
relationship ManyToOne {
	/** Mã phường xã */
	BenhNhan{phuongXa} to PhuongXa
}

// NDB_TABLE=READ_BACKUP=1
relationship ManyToOne {
	/** Mã quận huyện */
	BenhNhan{quanHuyen} to QuanHuyen
}

// NDB_TABLE=READ_BACKUP=1
relationship ManyToOne {
	/** Mã tỉnh thành phố */
	BenhNhan{tinhThanhPho} to TinhThanhPho
}

// NDB_TABLE=READ_BACKUP=1
relationship ManyToOne {
	BenhNhan{userExtra} to UserExtra
}

// NDB_TABLE=READ_BACKUP=1 Danh mục tên bệnh lý theo Y học cổ truyền
relationship ManyToOne {
	/** Mã bệnh lý */
	BenhYhct{benhLy} to BenhLy
}

// NDB_TABLE=READ_BACKUP=1
relationship ManyToOne {
	/** Mã nhóm đối tượng BHYT */
	DoiTuongBhyt{nhomDoiTuongBhyt required} to NhomDoiTuongBhyt
}

// NDB_TABLE=READ_BACKUP=1
relationship ManyToOne {
	/** Mã thông tin BHXH */
	DonVi{thongTinBhxh} to ThongTinBhxh
}

// NDB_TABLE=READ_BACKUP=1
relationship ManyToOne {
	/** Mã đơn vị */
	Khoa{donVi required} to DonVi
}

// NDB_TABLE=READ_BACKUP=1
relationship ManyToOne {
	/** Mã chức danh */
	NhanVien{chucDanh required} to ChucDanh
}

// NDB_TABLE=READ_BACKUP=1
relationship ManyToOne {
	/** Mã người dùng */
	NhanVien{userExtra} to UserExtra
}

// NDB_TABLE=READ_BACKUP=1
relationship ManyToOne {
	/** Mã loại bệnh lý */
	NhomBenhLy{loaiBenhLy required} to LoaiBenhLy
}

// NDB_TABLE=READ_BACKUP=1
relationship ManyToOne {
	/** Mã khoa */
	Phong{khoa required} to Khoa
}

// NDB_TABLE=READ_BACKUP=1 Thông tin quận huyện
relationship ManyToOne {
	/** Mã quận huyện */
	PhuongXa{quanHuyen required} to QuanHuyen
}

// NDB_TABLE=READ_BACKUP=1
relationship ManyToOne {
	/** Mã tỉnh thành phố */
	QuanHuyen{tinhThanhPho required} to TinhThanhPho
}

// TODO This is a pure ManyToMany relation (delete me and decide owner side)
// NDB_TABLE=READ_BACKUP=1
relationship ManyToMany {
	NhanVien{chucVu required} to ChucVu{nhanVien}
}


// NDB_TABLE=READ_BACKUP=1
relationship ManyToOne {
	TPhongNhanVien{phong required} to Phong
}

// NDB_TABLE=READ_BACKUP=1
relationship ManyToOne {
	TPhongNhanVien{nhanVien required} to NhanVien
}

// TODO This is a pure ManyToMany relation (delete me and decide owner side)
// NDB_TABLE=READ_BACKUP=1
relationship ManyToMany {
	Menu{user required} to UserExtra{menu}
}

// NDB_TABLE=READ_BACKUP=1
relationship ManyToOne {
	/** Mã bệnh nhân */
	TheBhyt{benhNhan} to BenhNhan
}

// NDB_TABLE=READ_BACKUP=1
relationship ManyToOne {
	/** Mã đối tượng BHYT */
	TheBhyt{doiTuongBhyt required} to DoiTuongBhyt
}

// NDB_TABLE=READ_BACKUP=1
relationship ManyToOne {
	/** Mã thông tin BHXH */
	TheBhyt{thongTinBhxh required} to ThongTinBhxh
}

// NDB_TABLE=READ_BACKUP=1
relationship ManyToOne {
	/** Mã đơn vị */
	ThongTinBhxh{donVi required} to DonVi
}

// NDB_TABLE=READ_BACKUP=1
relationship ManyToOne {
	DotDieuTri{bakb required} to BenhAnKhamBenh
}

// NDB_TABLE=READ_BACKUP=1
relationship ManyToOne {
	DotDieuTri{donVi required} to BenhAnKhamBenh
}

// NDB_TABLE=READ_BACKUP=1
relationship ManyToOne {
	DotDieuTri{benhNhan required} to BenhAnKhamBenh
}

// NDB_TABLE=READ_BACKUP=1
relationship ManyToOne {
	ThongTinKhoa{bakb required} to DotDieuTri
}

// NDB_TABLE=READ_BACKUP=1
relationship ManyToOne {
	ThongTinKhoa{donVi required} to DotDieuTri
}

// NDB_TABLE=READ_BACKUP=1
relationship ManyToOne {
	ThongTinKhoa{benhNhan required} to DotDieuTri
}


// NDB_TABLE=READ_BACKUP=1
relationship ManyToOne {
	ThongTinKhoa{dotDieuTri required} to DotDieuTri
}

// NDB_TABLE=READ_BACKUP=1
relationship ManyToOne {
	ThongTinKhamBenh{bakb required} to ThongTinKhoa
}

// NDB_TABLE=READ_BACKUP=1
relationship ManyToOne {
	ThongTinKhamBenh{donVi required} to ThongTinKhoa
}

// NDB_TABLE=READ_BACKUP=1
relationship ManyToOne {
	ThongTinKhamBenh{benhNhan required} to ThongTinKhoa
}


// NDB_TABLE=READ_BACKUP=1
relationship ManyToOne {
	ThongTinKhamBenh{dotDieuTri required} to ThongTinKhoa
}

// NDB_TABLE=READ_BACKUP=1
relationship ManyToOne {
	ThongTinKhamBenh{thongTinKhoa required} to ThongTinKhoa
}

// NDB_TABLE=READ_BACKUP=1
relationship ManyToOne {
	/** Mã nhân viên */
	ThongTinKhamBenh{nhanVien} to NhanVien
}

// NDB_TABLE=READ_BACKUP=1
relationship ManyToOne {
	/** Mã bệnh lý */
	ThongTinKhamBenh{benhLy} to BenhLy
}

// NDB_TABLE=READ_BACKUP=1
relationship ManyToOne {
	/** Mã hướng điều trị */
	ThongTinKhamBenh{huongDieuTri} to HuongDieuTri
}

// NDB_TABLE=READ_BACKUP=1
relationship ManyToOne {
	/** Mã phòng */
	ThongTinKhamBenh{phong required} to Phong
}

// NDB_TABLE=READ_BACKUP=1
relationship ManyToOne {
	/** uuid */
	UserExtra{uuid required} to User
}

// NDB_TABLE=READ_BACKUP=1
relationship ManyToOne {
	UserExtra{userType} to UserType
}

// NDB_TABLE=READ_BACKUP=1
relationship ManyToOne {
	DotDieuTri{dotDieuTri} to BenhAnKhamBenh
}

dto * with mapstruct
paginate * with pagination
service * with serviceImpl
microservice * with khamchuabenh
filter *
clientRootFolder

BenhAnKhamBenh,
BenhLy,
DotDieuTri,
ThongTinKhoa,
BenhNhan,
BenhYhct,
ChucDanh,
ChucVu,
Country,
DanToc,
DoiTuongBhyt,
DonVi,
DotGiaDichVuBhxh,
HuongDieuTri,
Khoa,
LoaiBenhLy,
Menu,
NgheNghiep,
NhanVien,
NhomBenhLy,
NhomDoiTuongBhyt,
Phong,
PhuongXa,
QuanHuyen,
TBenhLyKhamBenh,
TPhongNhanVien,
TheBhyt,
ThongTinBhxh,
ThongTinKhamBenh,
TinhThanhPho,
UserExtra,
UserType
with khamchuabenh
